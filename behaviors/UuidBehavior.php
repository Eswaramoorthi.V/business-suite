<?php

namespace app\behaviors;

use yii\db\BaseActiveRecord;
use yii\behaviors\AttributeBehavior;
use Ramsey\Uuid\Uuid;

class UuidBehavior extends AttributeBehavior
{
    public $uuidAttribute = 'id';

    public $value;

    public function init()
    {
        parent::init();

        if (empty($this->attributes)) {
            $this->attributes = [
                BaseActiveRecord::EVENT_BEFORE_INSERT => [$this->uuidAttribute],
            ];
        }
    }

    protected function getValue($event)
    {
        if ($this->value === null) {
            return Uuid::uuid1();
        }

        return parent::getValue($event);
    }
}
