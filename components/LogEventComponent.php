<?php

namespace app\components;

use Yii;
use dektrium\user\models\LoginForm;
use dektrium\user\models\User;

class LogEventComponent
{
    public static $smsLogActionList = ['login'];

    public static function init()
    {
      
       Yii::$app->layout = Yii::$app->user->isGuest ? '@app/views/layouts/guest.php' : '@app/views/layouts/main.php';
    }
}
