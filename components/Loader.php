<?php


namespace app\components;


abstract class Loader{

	/**
	 * @var ClassLoader the package auto loader
	 */
	private static $loader;
        

	/**
	 * Initiate and return the auto-loader
	 *
	 * @return ClassLoader
	 */
	protected function getLoader() {
            pr($loader);
		if (!isset(self::$loader)) {
			if (!class_exists('\Composer\Autoload\ClassLoader')) {
				$libraryDir = realpath(dirname(__FILE__) . '/../');
				require_once($libraryDir . '/extensions/vendors/composer/ClassLoader.php');
			}
			self::$loader = new ClassLoader();
			self::$loader->register();
		}
		return self::$loader;
	}

}