<?php

namespace app\components;

class Procurement {

    const MRV_STATUS_OPEN=1;
    const MRV_STATUS_PART=2;
    const MRV_STATUS_CLOSE=3;

    const DOCUMENT_DRAFT=203;

    const DOCUMENT_APPROVED=204;
    const DOCUMENT_UNAPPROVED=205;

    const PAYMENTTERM_ROOT=197;
    const DEFAULT_CURRENCY=1;
    public static  function getMRVStatus(){
        $mrvStatus=[];
        $mrvStatus[self::MRV_STATUS_OPEN]='Open';
        $mrvStatus[self::MRV_STATUS_PART]='Part';
        $mrvStatus[self::MRV_STATUS_CLOSE]='Close';
        return $mrvStatus;

    }
    public static  function getDocumentStatus(){
        $documentStatus=[];
        $documentStatus[self::DOCUMENT_DRAFT]='Draft';
        $documentStatus[self::DOCUMENT_APPROVED]='Approved';
        $documentStatus[self::DOCUMENT_UNAPPROVED]='Un Approved';
        return $documentStatus;

    }

    public static function getCurency(){
        $currency=[];
        $currency[1]='AED';
        return $currency;
    }

}
