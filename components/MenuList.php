<?php

namespace app\components;

use yii;
use yii\helpers\Url;
use app\models\Menu;
use app\models\RoleChild;

class MenuList {

    public static function getMenu($controllerId, $actionId, $sitemap = null) {


        $fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
        reset($fetchRole);
        /* @var $role \yii\rbac\Role */
        $role = current($fetchRole);
       // $userRole = 'admin'; //$role->name;
        $userRole = $role->name;
        
        $getauthList = RoleChild::find()->where(['parent' => $userRole])->asArray()->all();
        $getauthListIds = [];
        if (!empty($getauthList)) {
            foreach ($getauthList as $authData) {
                $idlist = explode("," ,$authData['associated_ids']);
                foreach ($idlist as $mid) {
                    $getauthListIds[$mid] = $mid;
                }
            }
        }
        $authIds = array_values($getauthListIds);

        $query = Menu::find()->where(['status' => 1])->andWhere(['menu_disp'=>1])->orderby([
                    'position' => 'asc'
                ])->asArray()->all();

        //pr($getauthListIds);
        $level3Data = self::formatMenu(3, $query, [], $controllerId, $actionId, $authIds, $userRole);
        $level2and3Data = self::formatMenu(2, $query, $level3Data, $controllerId, $actionId, $authIds, $userRole);
        $menuDataprepare = [];
        $temp = [];
        foreach ($query as $menuData) {
            if (!in_array($menuData['id'], $authIds) && $userRole!='admin') {
                            continue;
                }
            if ($menuData['parent_id'] == 0) {
                if (isset($level2and3Data[$menuData['id']]) && $menuData['name'] != 'Sitemap') {
                    $cntrl = explode(",", $menuData['controller']);
                    ksort($level2and3Data[$menuData['id']]);
                    $temp['items'] = array_values($level2and3Data[$menuData['id']]);
                    $temp['label'] = $menuData['name'];
                    $temp['url'] = [$menuData['url']];
                    if (($controllerId == 'school' && $actionId == 'index') || ($controllerId == 'schedule' && $actionId == 'scheduler')) {
                        $temp['options']['class'] = "xn-openable";
                    } else {
                        $temp['options']['class'] = (in_array($controllerId, $cntrl) ) ? "xn-openable active" : "xn-openable";
                    }
                    $icon = $menuData['icon'];
                    $temp['template'] = "<a href='{url}'><span class='$icon'></span><span class='xn-text'>{label}</span></a>";
                    $menuDataprepare[] = $temp;
                } else {
                    ksort($menuData);
                    $temp['label'] = $menuData['name'];
                    $temp['url'] = [$menuData['url']];
                    $temp['visible'] = true;
                    $temp['options']['class'] = ($controllerId == $menuData['controller'] && $actionId == $menuData['action']) ? "active" : "";
                    $icon = $menuData['icon'];
                    $temp['template'] = "<a href='{url}'><span class='$icon'></span><span class='xn-text'>{label}</span></a>";
                    //if ($temp['label'] == 'Sitemap') {
                        unset($temp['items']);
                    //}
                    $menuDataprepare[$menuData['position']] = $temp;
                }
            }
        }

        // pr($menuDataprepare);
        if ($sitemap == null) {
            $ulContainer['options'] = ['class' => 'x-navigation'];
            $additionalContainer['label'] = 'Estimate';
            $additionalContainer['url'] = Url::toRoute(['/']);
            $additionalContainer['options'] = ['class' => 'xn-logo'];

            $t[0] = $additionalContainer;
            $t[1] = ['label' => 'aaa'];
            $t[1] = ['options' => ['class' => 'x-navigation-control']];
        } else {
            $t = [];
        }
        $finalData['options'] = ['class' => 'x-navigation'];
        $finalData['items'] = array_merge($t, $menuDataprepare);

        //pr($finalData);
        return $finalData;
    }

    public static function getParentID($type) {
        $fetchMenus = Menu::find()->where(['status' => 1])->asArray()->all();
        return $chartSettings;
    }

    public static function formatMenu($level, $data, $level3 = [], $controllerId, $actionId, $authIds, $userRole) {
        $container = [];
        foreach ($data as $menuData) {
            
             if (!in_array($menuData['id'], $authIds) && $userRole!='admin') {
                            continue;
                }
                
            if ($menuData['level'] == $level) {
                if (isset($level3[$menuData['id']]) && !empty($level3)) {
                    $cntrl = explode(",", $menuData['controller']);
                    ksort($level3[$menuData['id']]);
                    $temp['items'] = array_values($level3[$menuData['id']]);
                    $temp['label'] = $menuData['name'];
                    $temp['url'] = [$menuData['url']];
                    //$temp['options']['class'] = ($controllerId == $menuData['controller']) ? "xn-openable active" : "xn-openable";
                    $temp['options']['class'] = (in_array($controllerId, $cntrl) ) ? "xn-openable active" : "xn-openable";
//                     if( ($controllerId=='school' && $actionId== 'index') || ($controllerId=='schedule' && $actionId== 'scheduler') ) {
//                        $temp['options']['class'] = "xn-openable";                    
//                    } else {
//                         $temp['options']['class'] = (in_array($controllerId, $cntrl) ) ? "xn-openable active" : "xn-openable";
//                    }
                    $icon = $menuData['icon'];
                    $temp['template'] = "<a href='{url}'><span class='$icon'></span><span class='xn-text'>{label}</span></a>";
                    $container[$menuData['parent_id']][$menuData['position']] = $temp;
                } else {
                    //pr($menuData['id']);
                    ksort($menuData);
                    $temp['label'] = $menuData['name'];
                    $temp['url'] = [$menuData['url']];
                    $temp['options']['class'] = ($controllerId == $menuData['controller'] && $actionId == $menuData['action']) ? "active" : "";
                    $icon = $menuData['icon'];
                    $temp['template'] = "<a href='{url}'><span class='$icon'></span><span class='xn-text'>{label}</span></a>";
                    //if ($temp['label'] == 'Menu Settings') {
                        unset($temp['items']);
                    //}
                    $container[$menuData['parent_id']][$menuData['position']] = $temp;
                }
            }
        }
        return $container;
    }

}
