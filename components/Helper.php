<?php

namespace app\components;

class Helper {

  public static function get_timeago($ptime) {
        $estimate_time = time() - $ptime;

        if ($estimate_time < 1) {
            return 'less than 1 second ago';
        }

        $condition = array(
            12 * 30 * 24 * 60 * 60 => 'year',
            30 * 24 * 60 * 60 => 'month',
            24 * 60 * 60 => 'day',
            60 * 60 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($condition as $secs => $str) {
            $d = $estimate_time / $secs;

            if ($d >= 1) {
                $r = round($d);
                return $r . ' ' . $str . ( $r > 1 ? 's' : '' ) . ' ago';
            }
        }
    }
    
    
    public static  function numberTowords($num) {

                $decones = array(
                    '00'=>" ",
            '01' => "one", 
            '02' => "two", 
            '03' => "three", 
            '04' => "four", 
            '05' => "five", 
            '06' => "six", 
            '07' => "seven", 
            '08' => "eight", 
            '09' => "nine", 
            10 => "ten", 
            11 => "eleven", 
            12 => "twelve", 
            13 => "thirteen", 
            14 => "fourteen", 
            15 => "fifteen", 
            16 => "sixteen", 
            17 => "seventeen", 
            18 => "eighteen", 
            19 => "nineteen" 
            );
$ones = array( 
            0 => " ",
            1 => "one",     
            2 => "two", 
            3 => "three", 
            4 => "four", 
            5 => "five", 
            6 => "six", 
            7 => "seven", 
            8 => "eight", 
            9 => "nine", 
            10 => "ten", 
            11 => "eleven", 
            12 => "twelve", 
            13 => "thirteen", 
            14 => "fourteen", 
            15 => "fifteen", 
            16 => "sixteen", 
            17 => "seventeen", 
            18 => "eighteen", 
            19 => "nineteen" 
            ); 
$tens = array( 
            0 => "",
            2 => "twenty", 
            3 => "thirty", 
            4 => "forty", 
            5 => "fifty", 
            6 => "sixty", 
            7 => "seventy", 
            8 => "eighty", 
            9 => "ninety" 
            ); 
$hundreds = array( 
            "hundred", 
            "thousand", 
            "million", 
            "billion", 
            "trillion", 
            "quadrillion" 
            ); //limit t quadrillion 
$num = number_format($num,2,".",","); 
$num_arr = explode(".",$num);

$wholenum = $num_arr[0]; 
$decnum = $num_arr[1];

$whole_arr = array_reverse(explode(",",$wholenum));
        //pr1($whole_arr);
krsort($whole_arr); 
$rettxt = "";
foreach($whole_arr as $key => $i){

    while(substr($i,0,1)=="0")
        $i=substr($i,1,5);
    if($i < 20){
        /* echo "getting:".$i; */
        $rettxt .= $ones[substr($i,0,1)];
    }elseif($i < 100){
        if(substr($i,0,1)!="0")  $rettxt .= $tens[substr($i,0,1)];
        if(substr($i,1,1)!="0") $rettxt .= " ".$ones[substr($i,1,1)];
    }else{
        if(substr($i,0,1)!="0") $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
        if(substr($i,1,1)!="0")$rettxt .= " ".$tens[substr($i,1,1)];
        if(substr($i,2,1)!="0")$rettxt .= " ".$ones[substr($i,2,1)];
    }
    if($key > 0){
        $rettxt .= " ".$hundreds[$key]." ";
    }
}
        $rettxt = $rettxt." Dirhams";

if($decnum > 0){ 
    $rettxt .= " And "; 
    if($decnum <= 20){
        $rettxt .= $decones[$decnum];
    }

    elseif($decnum < 100){ 
        $rettxt .= $tens[substr($decnum,0,1)]; 
        $rettxt .= " ".$ones[substr($decnum,1,1)]; 
    }
    $rettxt = $rettxt." Fils ";
} 
return $rettxt.' '.'Only';}


   public static function numtowords2($num){
$decones = array(
            '01' => "One",
            '02' => "Two",
            '03' => "Three",
            '04' => "Four",
            '05' => "Five",
            '06' => "Six",
            '07' => "Seven",
            '08' => "Eight",
            '09' => "Nine",
            10 => "Ten",
            11 => "Eleven",
            12 => "Twelve",
            13 => "Thirteen",
            14 => "Fourteen",
            15 => "Fifteen",
            16 => "Sixteen",
            17 => "Seventeen",
            18 => "Eighteen",
            19 => "Nineteen"
            );
$ones = array(
            0 => " ",
            1 => "One",
            2 => "Two",
            3 => "Three",
            4 => "Four",
            5 => "Five",
            6 => "Six",
            7 => "Seven",
            8 => "Eight",
            9 => "Nine",
            10 => "Ten",
            11 => "Eleven",
            12 => "Twelve",
            13 => "Thirteen",
            14 => "Fourteen",
            15 => "Fifteen",
            16 => "Sixteen",
            17 => "Seventeen",
            18 => "Eighteen",
            19 => "Nineteen"
            );
$tens = array(
            0 => "",
    1 => "",
            2 => "Twenty",
            3 => "Thirty",
            4 => "Forty",
            5 => "Fifty",
            6 => "Sixty",
            7 => "Seventy",
            8 => "Eighty",
            9 => "Ninety"
            );
$hundreds = array(
            "Hundred",
            "Thousand",
            "Million",
            "Billion",
            "Trillion",
            "Quadrillion"
            ); //limit t quadrillion
$num = number_format($num,2,".",",");
$num_arr = explode(".",$num);
$wholenum = $num_arr[0];
$decnum = $num_arr[1];
$whole_arr = array_reverse(explode(",",$wholenum));
krsort($whole_arr);
$rettxt = "";
foreach($whole_arr as $key => $i){
    if($i < 20){
        $rettxt .= $ones[substr($i,0,1)];
    }

    elseif($i < 100){
        $rettxt .= $tens[substr($i,0,1)];
        $rettxt .= " ".$ones[substr($i,1,1)];
    }
    else{
        $rettxt .= $ones[substr($i,0,1)]." ".$hundreds[0];
        $rettxt .= " ".$tens[substr($i,1,1)];
        $rettxt .= " ".$ones[substr($i,2,1)];
    }
    if($key > 0){
        $rettxt .= " ".$hundreds[$key]." ";
    }

}
$rettxt = $rettxt." Dirhams";

if($decnum > 0){
    $rettxt .= " And ";
    if($decnum < 20){
        $rettxt .= $decones[$decnum];
    }
    elseif($decnum < 100){
        $rettxt .= $tens[substr($decnum,0,1)];
        $rettxt .= " ".$ones[substr($decnum,1,1)];
    }
    $rettxt = $rettxt." Fils ";
}
return $rettxt.' '.'Only';}
}
