<?php

namespace app\components;

use app\models\AuthItemChild;
use dektrium\rbac\models\AuthItem;
use Yii;
use app\models\Menu;
use app\assets\AppAsset;
use app\modules\users\models\Profile;
use yii\authclient\widgets\AuthChoice;
use yii\helpers\ArrayHelper;

class MenuHelper
{

    public static function getMenu()
    {

        $result = static::getMenuRecrusive(0);
       // pr($result);
        return $result;
    }

//     private static function getMenuRecrusive($parent)
//     {

// // pr($_SESSION);
// // die();
// $fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
// reset($fetchRole);

// $role = current($fetchRole);

// $roleName = $role->name;
// pr($roleName);
// // $roleName=isset($_SESSION['user_role_name'])?$_SESSION['user_role_name']:0;

//         if ($roleName != 'Admin') {
//             $menuItems = \app\models\AuthItemChild::find()
//                 ->where(['!=', 'menu_id', ''])
//                 ->andWhere(['parent' => $roleName])
//                 ->asArray()->all();

//             $getauthListIds = [];
//             if (!empty($menuItems)) {
//                 foreach ($menuItems as $authData) {
//                     $idlist = explode(",", $authData['associated_ids']);
//                     foreach ($idlist as $mid) {
//                         $getauthListIds[$mid] = $mid;
//                     }
//                 }
//             }
//             $authIds = array_values($getauthListIds);

//             $items = \app\models\Menu::find()
//                 ->where(['IN', 'id', $authIds])
//                 ->andwhere(['parent_id' => $parent])
//                 ->andWhere(['status' => 1])
//                 ->orderBy(['position' => 'asc'])
//                 ->asArray()->all();

//         } else {
//             $items = \app\models\Menu::find()
//                 ->where(['parent_id' => $parent])
//                 ->andWhere(['status' => 1])
//                 //->where(['IN', 'id', $itemslist])
//                 ->orderBy(['position' => 'asc'])
//                 ->asArray()->all();

//         }


//         //pr($items);
//         $result = [];

//         foreach ($items as $item) {

//                 if($item['url']!='#'){
//                      $permission=$item['url'];
//                      $splitmenu=explode("/",$permission);
//                     array_shift($splitmenu);
//                     $joinValue=implode("/",$splitmenu);
//                      $authJoin = 'auth/' . $joinValue;

//                     if (Yii::$app->user->can($authJoin)) {

//                         $result[] = [
//                             'label' => $item['name'],
//                             'url' => \Yii::$app->params['domain_url'] . "/main/web/index.php?r=" . $item['url'],
//                             'icon' => $item['icon'],
//                             'item' => $item,
//                             'items' => static::getMenuRecrusive($item['id']),

//                         ];
//                     }

//                 }else{

//                     $items = static::getMenuRecrusive($item['id']);
//                     if(!empty($items) || $item['id']==1){
//                         $result[] = [
//                             'label' => $item['name'],
//                             'url' => \Yii::$app->params['domain_url'] . "/main/web/index.php?r=" . $item['url'],
//                             'icon' => $item['icon'],
//                             'item' => $item,
//                             'items' => $items,

//                         ];
//                     }

//                 }



//         }

//         //pr($result);

//         return $result;
//     }



    public static function getActionHelper($route)
    {
        // pr($route);
        $actionList = '';
        $actions = ['export-pdf','update','deletes','changepassword','assignproject','assigncompany','detailview','index','master','copy','convert-project'];
      
        foreach ($actions as $list) {

            if (Yii::$app->user->can($route . '/' . $list)) {
                $actionList .= '{' . $list . '}';
            }

        }
        // pr($actionList);
        return $actionList;
    }

    public static function getActionVisibleHelper($route)
    {

//   pr($route);
        $visibleList = [];
        $visible = ['create','export-excel','export-pdf','export-full-excel'];
        foreach ($visible as $Vlist) {
            if (Yii::$app->user->can($route . '/' . $Vlist)) {
                $visibleList[$Vlist] = "show";

            } else {
                $visibleList[$Vlist] = "hide";
            }
        }

        return $visibleList;
    }


    public static function getMapRole()
    {

        $result = static::getMenuRecursiveMapRole(NULL);
        return $result;
    }

    private static function getMenuRecursiveMapRole($parent)
    {


        // pr($parent);
        $items = \app\models\Menu::find()
            ->where(['parent_id' => $parent])
            ->andWhere(['role_list_disp' => 1])
            //->where(['IN', 'id', $itemslist])
            ->orderBy(['position' => 'asc'])
            ->asArray()->all();
        $result = [];
// pr($items);
        foreach ($items as $item) {

            $result[] = [
                'label' => $item['name'],
                'url' => \Yii::$app->params['domain_url'] . "main/web/index.php?r=" . $item['url'],
                'icon' => $item['icon'],
                'item' => $item,
                'items' => static::getMenuRecursiveMapRole($item['id']),

            ];

        }


        return $result;
    }




  }
