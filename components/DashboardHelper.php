<?php

namespace app\components;

class DashboardHelper {

    public static function monthDataPreparation($data, $total=false) {
        for ($m = 1; $m <= 12; $m++) {
            $month = date("M", strtotime(date("Y") . "-" . $m . "-01"));
            if($total==false) {
                if (array_key_exists($month, $data)) {
                    $monthlist[$month] = (array_sum($data[$month]));
                } else {
                    $monthlist[$month] = 0;
                }
            } else {
                $monthlist[$month] = self::arraySum($data, $month);
            }
        }
        return $monthlist;
    }

    public static function arraySum($input, $key) {
        $sum = 0;
        array_walk($input, function($item, $index, $params) {
            if (!empty($item[$params[1]]))
                $params[0] += $item[$params[1]];
        }, array(&$sum, $key)
        );
        return $sum;
    }

}
