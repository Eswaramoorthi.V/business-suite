var nvd3Charts = function() {
	
        var myColors = ["#00B000","#FFC000","#FF0000","#FF702A","#DA3610",
                        "#80CDC2","#A6D969","#D9EF8B","#FFFF99","#F7EC37","#F46D43",
                        "#E08215","#D73026","#A12235","#8C510A","#14514B","#4D9220",
                        "#542688", "#4575B4", "#74ACD1", "#B8E1DE", "#FEE0B6","#FDB863",                                                
                        "#C51B7D","#DE77AE","#EDD3F2"];
        d3.scale.myColors = function() {
            return d3.scale.ordinal().range(myColors);
        };
        
	
	
	
	
	
	var drawDountChart = function() {
		//Regular pie chart example
		nv.addGraph(function() {
			var chart = nv.models.pieChart().x(function(d) {
				return d.label;
			}).y(function(d) {
				return d.value;
			}).showLabels(true).color(d3.scale.myColors().range());;

			d3.select("#chart-9 svg").datum(exampleData()).transition().duration(350).call(chart);

			return chart;
		});

		//Donut chart example
		nv.addGraph(function() {
			var chart = nv.models.pieChart().x(function(d) {
				return d.label;
			}).y(function(d) {
				return d.value;
			}).showLabels(true)//Display pie labels
			.labelThreshold(.05)//Configure the minimum slice size for labels to show up
			.labelType("value")//Configure what type of data to show in the label. Can be "key", "value" or "percent"
			.donut(true)//Turn on Donut mode. Makes pie chart look tasty!
			.donutRatio(0.35)//Configure how big you want the donut hole size to be.
			.color(["#00B000","#FFC000","#FF0000"]);

			d3.select("#chart-11 svg").datum(exampleData()).transition().duration(350).call(chart).call(centerText());

			return chart;
		});

		//Pie chart example data. Note how there is only a single array of key-value pairs.
		function exampleData() {
			return [
			{
				"label" : "Normal",
				"value" : 280
			},
			{
				"label" : "Critical",
				"value" : 30
			}, {
				"label" : "Low Battery",
				"value" : 49
			}, ];
		}
		function centerText() {
			return function() {
        var svg = d3.select("svg");

    		var donut = svg.selectAll("g.nv-slice").filter(
          function (d, i) {
            return i == 0;
          }
        );
        
        // Insert first line of text into middle of donut pie chart
        donut.insert("text", "g")
            .text("Total : 359")
            .attr("font-family", "sans-serif")
            .attr("font-size", "40px")
            .attr("text-anchor", "middle")
            .attr("fill", "red")
			.style("fill", "#000");
        // Insert second line of text into middle of donut pie chart
        
      }
    }

	};
	

	
	function centerText() {
			return function() {
        var svg = d3.select("svg");

    		var donut = svg.selectAll("g.nv-slice").filter(
          function (d, i) {
            return i == 0;
          }
        );
        
        // Insert first line of text into middle of donut pie chart
        donut.insert("text", "g")
            .text("Total : 359")
            .attr("font-family", "sans-serif")
            .attr("font-size", "40px")
            .attr("text-anchor", "middle")
			.style("fill", "#000");
        // Insert second line of text into middle of donut pie chart
        
      }
    }
	return {		
		init : function() {
                    console.log("Draw Chart");
			
			drawDountChart();
			
		}
	};
}();

nvd3Charts.init();