$(function () {
   
    $(".export-to").on("click", function () {
        
        var searchdata = $(this).attr('data-searchdata');
        var url = $(this).attr('data-url');
        var exporttype = $(this).attr('data-section');
        
        $.ajax({
            type: "POST",
            url: url,
            data: {searchdata: searchdata,exporttype: exporttype},
            beforeSend: function () {
            },
            success: function (response) {
                window.open(response, '_blank');
            },
            error: function (response) {
                console.log(response);
            }
        });
    });
    


});



