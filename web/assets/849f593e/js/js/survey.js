var SURVEY = SURVEY || {};

$(function(){

  	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  	// global inicialization functions
  	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  	SURVEY.global = {

  		init: function() {

      		SURVEY.global.actions();
	    },

	    // device identification function
	    actions: function(){

	   		$(document).on('click', $surveyModalButton, function(){
	   		    /*if ($($surveyModal).data('bs.modal').isShown()) {
		            $($surveyModal).find($surveyModalContent)
		            	.load($(this).attr('value'));
		         	$surveyModalHeader.innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
		        } else { */
 		         	$($surveyModal).modal('show')
		         		.find($surveyModalContent)
		         		.load($(this).attr('value'));
		         	$($surveyModalHeader).html('<h4>' + $(this).attr('title') + '</h4>');
		        //}

		        SURVEY.global.step1();

		        //SURVEY.global.step1();
    		});

    		$(document).on('click', $MapCaptureButton, function(){
                var searchTextField = $($mapinput).val();
                var users = $($users).val();
                var name = $($name).val();
                var description = $($description).val();
                if(name ==''){
                    $($name).focus();
                }else if(users ==''){
                    $('.bootstrap-tagsinput input').focus();
                }else if(description ==''){
                    $($description).focus();
				}else if(searchTextField ==''){
                    $($mapinput).focus();
                }else {
                    SURVEY.global.step2();
                }
    		});

    		$(document).on('click', $resetCrop, function(){
    		   $($mapImage).cropper('clear');
             });

    		$(document).on('click', $saveCrop, function(){
    			SURVEY.global.step3();
             });

    		$(document).on('click', $saveimage, function(){
    			SURVEY.global.test();
             });

			/*$(document).on('click', '.listImg li', function(){
				var src = $(this).find('img').attr('src');
			});*/

	    },

	    step1: function()
	    {
	   	var staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap";

	  		setTimeout(function(){ 

				//$map_lat = parseFloat($('#SurveyModal #'+$map_element).attr('data-lat'));
				//$map_lng = parseFloat($('#SurveyModal #'+$map_element).attr('data-lng'));

				// $('#'+$map_element).width($map_width+'px');
				$('#'+$map_element).height($map_height+'px');

				SURVEY.custom.map($map_element, $map_lat, $map_lng, $map_zoom, $map_type);

			 }, 2000);
	    },

	    step2: function()
	    {
	    	$($step1).addClass('hide');
	    	$($step2).removeClass('hide');
	    	var imageUrl = "";
	    	imageUrl = SURVEY.global.map_image();
	    	
	    	SURVEY.custom.convertImage(imageUrl);

	    	setTimeout(function(){

	    		var canvas = localStorage.getItem( "savedImageData" );
		    	$($mapImage).attr('src', canvas);
		    	SURVEY.custom.cropper($mapImage);

	    	}, 1500)

	    	

	    },

	    step3: function()
	    {
			$($step2).addClass('hide');
	    	$($step3).removeClass('hide');

	    	var url = $($mapImage).cropper('getCroppedCanvas').toDataURL("image/png");
	    	$('#cropImage').attr('src', url);
			SURVEY.custom.canvas_start();
	      	$($step4).removeClass('hide');
	      	var itemURL = '';
		      document
		        .getElementById('drag-items')
		        .addEventListener('dragstart', function(e) {
		          itemURL = e.target.src;
		     });

		       var con = stage.container();
      			con.addEventListener('dragover', function(e) {
        		e.preventDefault(); // !important
      			});

			con.addEventListener('drop', function(e) {
			        e.preventDefault();
			        // now we need to find pointer position
			        // we can't use stage.getPointerPosition() here, because that event
			        // is not registered by Konva.Stage
			        // we can register it manually:
			        stage.setPointersPositions(e);

					SURVEY.custom.add_image(itemURL);

			        /*Konva.Image.fromURL(itemURL, function(image) {
			          layer.add(image);

			          image.position(stage.getPointerPosition());
			          image.draggable(true);

			          layer.draw();
			        });*/


			      });

		    },

	    map_image: function()
	    {

	    	var staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap";
 
            //Set the Google Map Center.
            staticMapUrl += "?center=" + $map.getCenter().lat() + "," + $map.getCenter().lng();
 
            //Set the Google Map Size.
            staticMapUrl += "&size=" + $map_width + "x" + $map_height;
 
            //Set the Google Map Zoom.
            staticMapUrl += "&zoom=" + $map.getZoom();
 
            //Set the Google Map Type.
            staticMapUrl += "&maptype=" + $map_type;

			//Set the Google Map Key.
			staticMapUrl += "&key=" + $map_key;

			return staticMapUrl;

	    },

	    test: function() {

		var canvas = document.querySelector('#myCanvas canvas');
		   
		var img = new Image;
		    
		   	img.crossOrigin = "Anonymous";
		 
		    img.onload = function() {
		      	canvas.width = img.width;
		      	canvas.height = img.height;
		      	ctx.drawImage( img, 0, 0 );

		     	//localStorage.setItem( "savedImageData", canvas.toDataURL("image/png") );
		   	}
		 
		   //img.src = src;
		   $('#finalimg').attr('src', canvas.toDataURL("image/png"));

		    var lat = $('#lat').val();
            var lng = $('#lng').val();
            var users = $('#users').val();
            var name = $('#name').val();
            var description = $('#description').val();
			$.ajax({

					type     :'POST',

					cache    : false,

					data: {recid: $('#recid').val(), iamgefile: canvas.toDataURL('image/png'),'lat':lat,'lng':lng,'users':users,'name':name,'description':description},

					url  : 'index.php?r=location/location/savesiteimage',

					success  : function(response) {

					self.location.reload();

					//$('#SurveyModal').css('display','none');	

					//$('.modal-backdrop').css('position','relative', 'important');

					},

					error: function(){

					  console.log('failure');

					}

				});


	    }

  	}; 

  	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  	// Custom functions
  	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  	SURVEY.custom = {

  		init: function() {

  		},

  		map: function(ele, lat, lng, zoom, type) {

			$map = new google.maps.Map(document.getElementById('googleMap'), {
		      center: {lat: -33.8688, lng: 151.2195},
   		      mapTypeId: type,
   		      disableDefaultUI: true,
		      zoom: 18,
		      tilt: 0,
			  zoomControl : true
		    });
		    var input = document.getElementById('searchTextField');
            var options = {
                // types: ['(cities)'],
                componentRestrictions: {country: 'SG'}
            };
		    $map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);

		    var autocomplete = new google.maps.places.Autocomplete(input,options);
		    autocomplete.bindTo('bounds', $map);

		    var infowindow = new google.maps.InfoWindow();
		    var marker = new google.maps.Marker({
		        map: $map,
		        anchorPoint: new google.maps.Point(0, -29)
		    });

autocomplete.addListener('place_changed', function() {
        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            window.alert("Autocomplete's returned place contains no geometry");
            return;
        }
  
        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            $map.fitBounds(place.geometry.viewport);

        } else {
            $map.setCenter(place.geometry.location);

            $map.setZoom(17);
        }
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();

    $('#lat').val(lat);
    $('#lng').val(lng);
        /*marker.setIcon(({
            url: place.icon,
            size: new google.maps.Size(100, 100),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(35, 35)
        }));
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        /*var address = '';
        if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }
    
        infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
        infowindow.open($map, marker);*/
 
		});
  			/*$map = new google.maps.Map(document.getElementById(ele), {
			    	zoom: zoom,
			    	center: {lat: lat, lng: lng},
			    	mapTypeId: type,
			    	disableDefaultUI: true,
			    	tilt: 0,
			    	zoomControl : true,
			  	});*/

  		},

  		convertImage: function (src) {

  			var img = new Image,
		    canvas = document.createElement("canvas"),
		    ctx = canvas.getContext("2d");
		    
		   	img.crossOrigin = "Anonymous";
		 
		    img.onload = function() {
		      	canvas.width = img.width;
		      	canvas.height = img.height;
		      	ctx.drawImage( img, 0, 0 );

		     	localStorage.setItem( "savedImageData", canvas.toDataURL("image/png") );
		   	}
		 
		   img.src = src;
		},

		cropper: function (ele) {

			$(ele).cropper({
	    		viewMode : 1,
	    		autoCrop: false,
	    		//zoomable: false
		    });

		},

		canvas_start: function() {
			var img = document.getElementById('cropImage');
			SURVEY.custom.get_img(img.src);

			stage = new Konva.Stage({
		        container: 'myCanvas',
		        width: localStorage.getItem('cwidth'),
		        height: localStorage.getItem('cheight'), 
		    });
			layer = new Konva.Layer();
	      	stage.add(layer);
	      	SURVEY.custom.add_image(img.src, 'no');
	    },

	    get_img: function (src, callback)
		{	
			var img = new Image();
			img.src = src;
			img.onload = function(){
			  var imgSize = {
			     w: img.width,
			     h: img.height
			  };
		    localStorage.setItem( "cwidth", imgSize.w );
  			localStorage.setItem( "cheight", imgSize.h );
			};
		},

		add_image: function(src, opt = '')
		{


	      	var fname = src.split("/").pop().split(".")[0];
			var drag = (opt) ? false : true;
			var darthVaderGroup = new Konva.Group({
							        draggable: drag
	      						});

	      	layer.add(darthVaderGroup);

	      	if ( fname != 'icon9' ) {

	      	var imageObj1 = new Image();
			imageObj1.src = src;

			var width = imageObj1.width;
			var height = imageObj1.height;
			var imname = imageObj1.name;

	      	var darthVaderImg = new Konva.Image({
		        width: width,
		        height: height,
		        imname: imname
		    });

	      	

	      	darthVaderGroup.add(darthVaderImg);
	      	//darthVaderGroup.add(text);
			


	      	if(drag)
	      	{
	      		/*SURVEY.custom.addAnchor(darthVaderGroup, 0, 0, 'topLeft');
	      		SURVEY.custom.addAnchor(darthVaderGroup, width, 0, 'topRight');
	      		SURVEY.custom.addAnchor(darthVaderGroup, width, height, 'bottomRight');
	      		SURVEY.custom.addAnchor(darthVaderGroup, 0, height, 'bottomLeft');	*/
	      	}

		    imageObj1.onload = function() {
		        darthVaderImg.image(imageObj1);
		        layer.draw();
		    };
}
		  	if ( drag ) {

			

if ( fname == 'icon9' ) {

var textNode = new Konva.Text({
        text: 'Some text here',
        x: 50,
        y: 50,
        fontSize: 20,
      });

      //layer.add(textNode);
      darthVaderGroup.add(textNode);
      //layer.draw();

      textNode.on('dblclick', () => {
        // create textarea over canvas with absolute position

        // first we need to find position for textarea
        // how to find it?

        // at first lets find position of text node relative to the stage:
        var textPosition = textNode.getAbsolutePosition();

        // then lets find position of stage container on the page:
        var stageBox = stage.container().getBoundingClientRect();

        // so position of textarea will be the sum of positions above:
        var areaPosition = {
          x: textPosition.x,
          y: textPosition.y
        };

        // create textarea and style it
        var textarea = document.createElement('input');
        document.getElementById("myCanvas").appendChild(textarea); 
        //document.body.appendChild(textarea);

        textarea.value = textNode.text();
        textarea.style.position = 'absolute';
        textarea.style.top = areaPosition.y + 'px';
        textarea.style.left = areaPosition.x + 'px';
        textarea.style.width = textNode.width();

        textarea.focus();

        textarea.addEventListener('keydown', function(e) {
          // hide on enter
          if (e.keyCode === 13) {
            textNode.text(textarea.value);
            layer.draw();
            document.getElementById("myCanvas").removeChild(textarea);
          }
        });
      });
 
	
  }


  var tr = new Konva.Transformer({
				anchorStroke: '#666',
		        anchorFill: '#666',
		        anchorSize: 5,
		        borderStroke: 'green',
		        borderDash: [1, 1],
		        draggable: true
			});

			layer.add(tr);
			tr.attachTo(darthVaderGroup);
			layer.draw();

			const deleteButton = new Konva.Circle({
			  x: tr.getWidth(),
			  y: 0,
			  radius: 5,
			  fill: 'red'
			});
			tr.add(deleteButton);

			tr.on('transform', () => {
			  deleteButton.x(tr.getWidth())
			})

			deleteButton.on('click', () => {
			  tr.destroy();
			  if ( fname != 'icon9' ) 
			  	darthVaderImg.destroy();
			  if ( fname == 'icon9' ) 
				  textNode.destroy();
			  layer.draw();
			})
			}



	/*stage.on('click dbltap', function(e) {


		 stage.find('Transformer').destroy();


	        // if click on empty area - remove all transformers
	        /*if (e.target === stage) {
	          stage.find('Transformer').destroy();
	          layer.draw();
	          return;
	        }*/
	        // do nothing if clicked NOT on our rectangles
	        //if (!e.target.hasName('rect')) {
	        //  return;
	        //}
	        // remove old transformers
	        // TODO: we can skip it if current rect is already selected
	        //stage.find('Transformer').destroy();

	        /*if ( e.target !== stage ) {
	        // create new transformer
  			var tr2 = new Konva.Transformer({
				anchorStroke: '#666',
		        anchorFill: '#666',
		        anchorSize: 5,
		        borderStroke: 'green',
		        borderDash: [1, 1],
		        draggable: true
			});

			layer.add(tr2);
			tr2.attachTo(darthVaderGroup);
			layer.draw();
				}
	      });

		darthVaderGroup.on('click tap', () => {
          var tr3 = new Konva.Transformer({
        anchorStroke: '#666',
            anchorFill: '#666',
            anchorSize: 5,
            borderStroke: 'green',
            borderDash: [1, 1],
            draggable: true
      });

      tr3.resizeEnabled(true);
      tr3.rotateEnabled(true);


      layer.add(tr3);
      tr3.attachTo(darthVaderGroup);
      layer.draw();
          });*/


		},

		addAnchor: function(group, x, y, name) {
	        var stage = group.getStage();
	        var layer = group.getLayer();

	        var anchor = new Konva.Circle({
	          x: x,
	          y: y,
	          stroke: '#666',
	          fill: '#ddd',
	          strokeWidth: 2,
	          radius: 2,
	          name: name,
	          draggable: true,
	          dragOnTop: false
	        });

	        anchor.on('dragmove', function() {
	          SURVEY.custom.update(this);
	          layer.draw();
	        });
	        anchor.on('mousedown touchstart', function() {
	          group.draggable(false);
	          this.moveToTop();
	        });
	        anchor.on('dragend', function() {
	          group.draggable(true);
	          layer.draw();
	        });
	        // add hover styling
	        anchor.on('mouseover', function() {
	          var layer = this.getLayer();
	          document.body.style.cursor = 'pointer';
	          this.strokeWidth(4);
	          layer.draw();
	        });
	        anchor.on('mouseout', function() {
	          var layer = this.getLayer();
	          document.body.style.cursor = 'default';
	          this.strokeWidth(2);
	          layer.draw();
	        });

	        group.add(anchor);
      },

      update: function(activeAnchor) {
        var group = activeAnchor.getParent();

        var topLeft = group.find('.topLeft')[0];
        var topRight = group.find('.topRight')[0];
        var bottomRight = group.find('.bottomRight')[0];
        var bottomLeft = group.find('.bottomLeft')[0];
        var image = group.find('Image')[0];

        var anchorX = activeAnchor.getX();
        var anchorY = activeAnchor.getY();

        // update anchor positions
        switch (activeAnchor.getName()) {
          case 'topLeft':
            topRight.y(anchorY);
            bottomLeft.x(anchorX);
            break;
          case 'topRight':
            topLeft.y(anchorY);
            bottomRight.x(anchorX);
            break;
          case 'bottomRight':
            bottomLeft.y(anchorY);
            topRight.x(anchorX);
            break;
          case 'bottomLeft':
            bottomRight.y(anchorY);
            topLeft.x(anchorX);
            break;
        }

        image.position(topLeft.position());

        var width = topRight.getX() - topLeft.getX();
        var height = bottomLeft.getY() - topLeft.getY();
        if (width && height) {
          image.width(width);
          image.height(height);
        }
      }

  	};


  	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  	// initialize when document ready
  	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  	SURVEY.documentOnReady = {

		init: function(){
      		SURVEY.global.init();
      	}

    }


  	var 	
			//map variables
      		$map_key = 'AIzaSyAwWq0jnxsbjA6Y5E_4JXI6Z3WRVzqx8yA',
      		$map,
      		$map_zoom = 21,
      		$map_type = 'satellite',
      		$map_lat,
      		$map_lng,
      		$map_width = '600';
      		$map_height = '500';
      		$map_element = 'googleMap',

      		//Step 1
      		$step1 = '.step1',
      		$surveyModalButton = '.SurveyModalButton',
      		$surveyModal = '#SurveyModal',
      		$surveyModalContent = '#SurveyModalContent',
      		$surveyModalHeader = '#SurveyModalHeader',
      		$MapCaptureButton = '.MapCaptureButton',
				$mapinput ='#searchTextField',
				$name ='#name',
				$description ='#description',
				$users ='#users',

      		//Step 2 
      		$step2 = '.step2',
      		$mapImage = '#mapImage',
      		$resetCrop = '.resetCrop',
      		$saveCrop = '.saveCrop';

      		//Step 3
      		$step3 = '.step3',
      		$step4 = '.step4',
      		$saveimage = '.saveImg',




    //!!!!!!!!!!!!!
	// initializing
	//!!!!!!!!!!!!!
	$(document).ready( SURVEY.documentOnReady.init );
    

      

});


//survey page scripts

function SurveyPage() {
	$(document).ready(function(){
		SURVEY.global.step1();		
	});
}