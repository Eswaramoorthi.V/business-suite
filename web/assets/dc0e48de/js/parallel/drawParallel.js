    var blue_to_brown = d3.scale.linear()
                        .domain([9, 50])
                        .range(["steelblue", "brown"])
                        .interpolate(d3.interpolateLab);

                var color = function (d) {
                    //https://designschool.canva.com/blog/100-color-combinations/
                    //Nr. 11, 90,92,96 etc kanske kan va bra
                    //Annars kanske denna sida: http://www.colorcombos.com/color-schemes-4.html
                    //console.log(d);
                    return d.colorcode;
                };

                var parcoords = d3.parcoords()("#example")
                        .color(color)
                        .alpha(0.6); //Hur starkt linjerna ska lysa i visualiseringen...higher = stronger (0.5 orginal)

                // Bra sida där man kan hovra över visualisering och se vägen: http://bl.ocks.org/syntagmatic/4020926

                // load csv file and create the chart
                d3.json("parallel", function (data) {
                    parcoords
                            .data(data)
                            .hideAxis(["Time Period","Location Id","colorcode","Meeter Id","Frequency"])
                            .render()
                            .reorderable()
                            .brushMode("1D-axes");  // enable brushing

                    // create data table, row hover highlighting
                    
                    for(var i = 0; i < data.length; i++) {
                        console.log(data[i]['colorcode']);
                           //delete data[i]['colorcode'];
                        }
                    var grid = d3.divgrid();
                    d3.select("#grid")
                            .datum(data.slice(0, 20))
                            .call(grid)
                            .selectAll(".row")
                            .on({
                                "mouseover": function (d) {
                                    parcoords.highlight([d])
                                },
                                "mouseout": parcoords.unhighlight
                            });

                    // update data table on brush event
                    parcoords.on("brush", function (d) {
                        d3.select("#grid")
                                .datum(d.slice(0, 10))
                                .call(grid)
                                .selectAll(".row")
                                .on({
                                    "mouseover": function (d) {
                                        parcoords.highlight([d])
                                    },
                                    "mouseout": parcoords.unhighlight
                                });
                    });
                });

                d3.select('#btnReset').on('click', function () {
                    parcoords.brushReset();
                })
                d3.select('#sltPredicate').on('change', function () {
                    parcoords.brushPredicate(this.value);
                });
            