$(function () {
    /* reportrange */
    if ($("#reportrange").length > 0) {

        var start = moment().subtract(29, 'days');
        var end = moment();
        $('#reportrange').daterangepicker({

            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 01,
            locale: {
                format: 'DD/MM/YYYY H:mm'
            },
            startDate: start,
            endDate: end,
            dateLimit: {days: 60},
            ranges: {
                'Daily': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'This Week': [moment().startOf('week'), moment().endOf('week')],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            separator: ' to ',
            startDate: moment().subtract('days', 29),
            endDate: moment()
        },
                function (start, end, label) {
                    if (label == 'Daily') {
                        return false;
                    }
                    if (label == 'Custom Range') {
                        $('#reportrange span').html(start.format('DD/MM/YYYY H:mm') + ' - ' + end.format('DD/MM/YYYY H:mm'));
                    } else {
                        $('#reportrange span').html(start.format('DD/MM/YYYY 00:00') + ' - ' + end.format('DD/MM/YYYY 23:59'));
                    }
                }
        );
        $("#reportrange span").html(moment().subtract('days', 29).format('DD/MM/YYYY 00:00') + ' - ' + moment().format('DD/MM/YYYY 23:59'));

    }

    $("#report-search").on("click", function () {
        $('button').prop('disabled', true);
        var url = $(this).attr('data-url');
        var date_range = $('#reportrange').text();
        var date_range_split = date_range.split('-');
        var startDate = $.trim(date_range_split[0]);
        var endDate = $.trim(date_range_split[1]);

        var activation_type = $('#activation_type').val();
        var report_type = $('#reportType').val();
        

        var location = [];
        $('#location_list').each(function () {
            location.push($(this).val());
        });

        $.ajax({
            type: "POST",
            url: url,
            data: {startDate: startDate, endDate: endDate, reportType: report_type, activationType: activation_type, locationList: location},
            beforeSend: function () {
                // setting a timeout
                $("#reportresult").html('<div id="loader_disp" class="loader col-md-12"></div>');
            },
            success: function (response) {
                $("#reportresult").html(response);
                $('button').prop('disabled', false);
            },
            error: function (response) {
                console.log(response);
            }
        });
    });
    


});

    $("#report-type").on("change", function () {
        var url = $(this).val();
        document.location.href = url;
    });


