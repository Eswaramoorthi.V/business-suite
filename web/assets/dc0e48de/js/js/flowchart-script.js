$(document).ready(function() {


  var $flowchart = $('#example');
  var $container = $flowchart.parent();

  var cx = $flowchart.width() / 2;
  var cy = $flowchart.height() / 2;

  // Panzoom initialization...
  $flowchart.panzoom();

  // Centering panzoom
  $flowchart.panzoom('pan', -cx + $container.width() / 2, -cy + $container.height() / 2);

  // Panzoom zoom handling...
  var possibleZooms = [0.5, 0.75, 1, 2, 3];
  var currentZoom = 2;
  $container.on('mousewheel.focal', function(e) {
    e.preventDefault();
    var delta = (e.delta || e.originalEvent.wheelDelta) || e.originalEvent.detail;
    var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
    currentZoom = Math.max(0, Math.min(possibleZooms.length - 1, (currentZoom + (zoomOut * 2 - 1))));
    $flowchart.flowchart('setPositionRatio', possibleZooms[currentZoom]);
    $flowchart.panzoom('zoom', possibleZooms[currentZoom], {
      animate: false,
      focal: e
    });
  });

    var data1 = $('#jsondata').val();
    var data = {
        "operators": {
            "operator1": {
                "top": 860,
                "left": 620,
                "properties": {
                    "title": "System Setup",
                    "inputs": {
                        "input_1": {
                            "label": "Code Type"
                        },
                        "input_2": {
                            "label": "Code Moster"
                        },
                        "input_3": {
                            "label": "Location"
                        },
                        "input_4": {
                            "label": "Organization"
                        }
                    },
                    "outputs": {
                        "output_1": {
                            "label": "Output"
                        }
                    }
                }
            },
            "operator2": {
                "top": 860,
                "left": 840,
                "properties": {
                    "title": "User Setup",
                    "inputs": {
                        "input_1": {
                            "label": "User Type"
                        },
                        "input_2": {
                            "label": "User Group"
                        },
                        "input_3": {
                            "label": "User Profile"
                        },
                        "input_4": {
                            "label": "User Role"
                        },
                        "input_5": {
                            "label": "User Authentication"
                        }
                    },
                    "outputs": {
                        "output_1": {
                            "label": "Output"
                        }
                    }
                }
            },
            "operator3": {
                "top": 860,
                "left": 1080,
                "properties": {
                    "title": "Project Setting",
                    "inputs": {
                        "input_1": {
                            "label": "input"
                        },
                        "input_2": {
                            "label": "input"
                        },
                        "input_3": {
                            "label": "input"
                        },
                        "input_4": {
                            "label": "input"
                        }
                    },
                    "outputs": {
                        "output_1": {
                            "label": "Output"
                        },
                        "output_2": {
                            "label": "Output"
                        },
                        "output_3": {
                            "label": "Output"
                        },
                        "output_4": {
                            "label": "Output"
                        }
                    }
                }
            },
            "operator4": {
                "top": 860,
                "left": 1340,
                "properties": {
                    "title": "Doucument Setting",
                    "inputs": {
                        "input_1": {
                            "label": "input"
                        },
                        "input_2": {
                            "label": "input"
                        },
                        "input_3": {
                            "label": "input"
                        },
                        "input_4": {
                            "label": "input"
                        }
                    },
                    "outputs": {
                        "output_1": {
                            "label": "Output"
                        },
                        "output_2": {
                            "label": "Output"
                        },
                        "output_3": {
                            "label": "Output"
                        },
                        "output_4": {
                            "label": "Output"
                        }
                    }
                }
            }
        },
        "links": {
            "0": {
                "fromOperator": "operator2",
                "fromConnector": "output_1",
                "fromSubConnector": 0,
                "toOperator": "operator3",
                "toConnector": "input_1",
                "toSubConnector": 0
            },
            "1": {
                "fromOperator": "operator3",
                "fromConnector": "output_1",
                "fromSubConnector": 0,
                "toOperator": "operator4",
                "toConnector": "input_1",
                "toSubConnector": 0
            },
            "link_1": {
                "fromOperator": "operator1",
                "fromConnector": "output_1",
                "toOperator": "operator2",
                "toConnector": "input_1",
                "color": "red"
            }
        },
        "operatorTypes": {}
    };
console.log(data1);
  // Apply the plugin on a standard, empty div...
  $flowchart.flowchart({
    data: data,
    linkWidth: 5
  });

    var $operatorProperties = $('#operator_properties');
    var $linkProperties = $('#link_properties');
    var $operatorTitle = $('#operator_title');
    var $linkColor = $('#link_color')

    var $flowchart = $('#example');
    $flowchart.flowchart({
        data: data,
        onOperatorSelect: function(operatorId) {
            $operatorProperties.show();
            $operatorTitle.val($flowchart.flowchart('getOperatorTitle', operatorId));
            return true;
        },
        onOperatorUnselect: function() {
            $operatorProperties.hide();
            return true;
        },
        onLinkSelect: function(linkId) {
            $linkProperties.show();
            $linkColor.val($flowchart.flowchart('getLinkMainColor', linkId));
            return true;
        },
        onLinkUnselect: function() {
            $linkProperties.hide();
            return true;
        }
    });

    $operatorTitle.keyup(function() {
        var selectedOperatorId = $flowchart.flowchart('getSelectedOperatorId');
        if (selectedOperatorId != null) {
            $flowchart.flowchart('setOperatorTitle', selectedOperatorId, $operatorTitle.val());
        }
    });

    $linkColor.change(function() {
        var selectedLinkId = $flowchart.flowchart('getSelectedLinkId');
        if (selectedLinkId != null) {
            $flowchart.flowchart('setLinkMainColor', selectedLinkId, $linkColor.val());
        }
    });
  $flowchart.parent().siblings('.delete_selected_button').click(function() {
    $flowchart.flowchart('deleteSelected');
  });


    $flowchart.parent().siblings('.get_data').click(function() {
        var data = $flowchart.flowchart('getData');
        $('#flowchart_data').val(JSON.stringify(data, null, 2));
    });
  var $draggableOperators = $('.draggable_operator');

  function getOperatorData($element) {
    var nbInputs = parseInt($element.data('nb-inputs'));
    var nbOutputs = parseInt($element.data('nb-outputs'));
    var data = {
      properties: {
        title: $element.text(),
        inputs: {},
        outputs: {}
      }
    };

    var i = 0;
    for (i = 0; i < nbInputs; i++) {
      data.properties.inputs['input_' + i] = {
        label: 'Input ' + (i + 1)
      };
    }
    for (i = 0; i < nbOutputs; i++) {
      data.properties.outputs['output_' + i] = {
        label: 'Output ' + (i + 1)
      };
    }

    return data;
  }

  var operatorId = 0;

  $draggableOperators.draggable({
    cursor: "move",
    opacity: 0.7,

    helper: 'clone',
    appendTo: 'body',
    zIndex: 1000,

    helper: function(e) {
      var $this = $(this);
      var data = getOperatorData($this);
      return $flowchart.flowchart('getOperatorElement', data);
    },
    stop: function(e, ui) {
      var $this = $(this);
      var elOffset = ui.offset;
      var containerOffset = $container.offset();
      if (elOffset.left > containerOffset.left &&
        elOffset.top > containerOffset.top &&
        elOffset.left < containerOffset.left + $container.width() &&
        elOffset.top < containerOffset.top + $container.height()) {

        var flowchartOffset = $flowchart.offset();

        var relativeLeft = elOffset.left - flowchartOffset.left;
        var relativeTop = elOffset.top - flowchartOffset.top;

        var positionRatio = $flowchart.flowchart('getPositionRatio');
        relativeLeft /= positionRatio;
        relativeTop /= positionRatio;

        var data = getOperatorData($this);
        data.left = relativeLeft;
        data.top = relativeTop;

        $flowchart.flowchart('addOperator', data);
      }
    }
  });



});