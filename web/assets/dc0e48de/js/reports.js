$(function(){
    /* reportrange */
    if($("#reportrange").length > 0){
        var url = $('#datepickerurl').attr('data-url');
        $("#reportrange").daterangepicker({
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM.DD.YYYY',
            separator: ' to ',
            startDate: moment().subtract('days', 29),
            endDate: moment()
        },function(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            var startDate=start.format('YYYY-MM-DD');
            var endDate=end.format('YYYY-MM-DD');
            console.log(url);
            $.ajax({
                type : "POST",
                url: url+'&startDate='+startDate+"&endDate="+endDate,
                beforeSend: function() {
                    // setting a timeout
                    $("#performancechart").html("");
                },
                success  : function(response) {

                    $("#performancechart").html(response);

                },
                error : function(response){
                    console.log(response);
                }
            });

            return false;
        });


        $("#reportrange span").html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));





    }


});

