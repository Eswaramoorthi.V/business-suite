<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();

function pr($d) {
    echo "<pre>", print_r($d), "</pre>";
    die();
}

function pr1($d) {
    echo "<pre>", print_r($d), "</pre>";
}

function sql($q,$off=0) {
    echo $q->createCommand()->rawsql;
    if($off==1){
        die();
    }
}


