$(function(){
    /* reportrange */
    if($("#reportrange").length > 0){
        
        var url = $('#faulturl').attr('data-url');
        console.log(url);
        $("#reportrange").daterangepicker({
            ranges: {
               'Today': [moment(), moment()],
               'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days': [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month': [moment().startOf('month'), moment().endOf('month')],
               'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM.DD.YYYY',
            separator: ' to ',
            startDate: moment().subtract('days', 29),
            endDate: moment()
          },function(start, end) {
              $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
              var startDate=start.format('YYYY-MM-DD');
              var endDate=end.format('YYYY-MM-DD');


            $.ajax({
                type : "POST",
                url: url+'?startDate='+startDate+"&endDate="+endDate,
                beforeSend: function() {
                    // setting a timeout
                    $("#faultsummary1").html("");
                },
                success  : function(response) {

                    $("#faultsummary1").html(response);

                },
                error : function(response){
                    console.log(response);
                }
            });

            return false;
        });


        $("#reportrange span").html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));





    }

    /* end reportrange */

    /* Rickshaw dashboard chart */
    // var seriesData = [ [], [] ];
    // var random = new Rickshaw.Fixtures.RandomData(1000);

    // for(var i = 0; i < 100; i++) {
        // random.addData(seriesData);
    // }

    // var rdc = new Rickshaw.Graph( {
            // element: document.getElementById("dashboard-chart"),
            // renderer: 'area',
            // width: $("#dashboard-chart").width(),
            // height: 250,
            // series: [{color: "#33414E",data: seriesData[0],name: 'New'},
                     // {color: "#1caf9a",data: seriesData[1],name: 'Returned'}]
    // } );

    // rdc.render();

    // var legend = new Rickshaw.Graph.Legend({graph: rdc, element: document.getElementById('dashboard-legend')});
    // var shelving = new Rickshaw.Graph.Behavior.Series.Toggle({graph: rdc,legend: legend});
    // var order = new Rickshaw.Graph.Behavior.Series.Order({graph: rdc,legend: legend});
    // var highlight = new Rickshaw.Graph.Behavior.Series.Highlight( {graph: rdc,legend: legend} );



    // var hoverDetail = new Rickshaw.Graph.HoverDetail({graph: rdc});

    // window.addEventListener('resize', rdc_resize);

    // rdc_resize();
    /* END Rickshaw dashboard chart */

    /* Donut dashboard chart */

    /* END Donut dashboard chart */


    /* Bar dashboard chart */
    Morris.Bar({
        element: 'dashboard-bar-1',
        data: [
            { y: 'Web Server', a: 95, b: 40,c: 45 },
            { y: 'App Server', a: 75, b: 35,c: 25 },
            { y: 'DB Server', a: 55, b: 85,c: 35 },

        ],
        xkey: 'y',
        ykeys: ['a', 'b','c'],
        labels: ['CPU', 'Memory','Storage'],
        barColors: ['#33414E', '#1caf9a','#c67343'],
        gridTextSize: '10px',
        hideHover: true,
        gridLineColor: '#E5E5E5',
        xLabelMargin: 10,
        xaxes:220,

    }).on('click', function(i, row){

    console.log(row.y);
    $("#healthmonitor").trigger("click");
});


    /* END Bar dashboard chart */





        /* End Moris Area Chart */
    /* Vector Map */
    var jvm_wm = new jvm.WorldMap({container: $('#dashboard-map-seles'),
                                    map: 'world_mill_en',
                                    backgroundColor: '#FFFFFF',
                                    regionsSelectable: true,
                                    regionStyle: {selected: {fill: '#B64645'},
                                                    initial: {fill: '#33414E'}},
                                    markerStyle: {initial: {fill: '#1caf9a',
                                                   stroke: '#1caf9a'}},
                                    markers: [{latLng: [50.27, 30.31], name: 'Kyiv - 1'},
                                              {latLng: [52.52, 13.40], name: 'Berlin - 2'},
                                              {latLng: [48.85, 2.35], name: 'Paris - 1'},
                                              {latLng: [51.51, -0.13], name: 'London - 3'},
                                              {latLng: [40.71, -74.00], name: 'New York - 5'},
                                              {latLng: [35.38, 139.69], name: 'Tokyo - 12'},
                                              {latLng: [37.78, -122.41], name: 'San Francisco - 8'},
                                              {latLng: [28.61, 77.20], name: 'New Delhi - 4'},
                                              {latLng: [39.91, 116.39], name: 'Beijing - 3'}]
                                });
    /* END Vector Map */


    $(".x-navigation-minimize").on("click",function(){
        setTimeout(function(){
            rdc_resize();
        },200);
    });

    $("#dashboard-bar-1").css("height","365")

});

