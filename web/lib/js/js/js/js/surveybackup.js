var SURVEY = SURVEY || {};

$(function(){

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // global inicialization functions
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    SURVEY.global = {

        init: function() {
            SURVEY.global.actions();
        },

        // device identification function
        actions: function(){

            $(document).on('click', $surveyModalButton, function(){
                /*if ($($surveyModal).data('bs.modal').isShown()) {
                 $($surveyModal).find($surveyModalContent)
                     .load($(this).attr('value'));
                  $surveyModalHeader.innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
             } else { */
                $($surveyModal).modal('show')
                    .find($surveyModalContent)
                    .load($(this).attr('value'));
                $($surveyModalHeader).html('<h4>' + $(this).attr('title') + '</h4>');
                //}

                SURVEY.global.step1();
            });

            $(document).on('click', $MapCaptureButton, function(){
                SURVEY.global.step2();
            });

            $(document).on('click', $resetCrop, function(){
                $($mapImage).cropper('clear');
            });

            $(document).on('click', $saveCrop, function(){
                SURVEY.global.step3();
            });

            $(document).on('click', $saveimage, function(){
                SURVEY.global.test();
            });

            /*$(document).on('click', '.listImg li', function(){
                var src = $(this).find('img').attr('src');
            });*/

        },

        step1: function()
        {
            setTimeout(function(){

                $map_lat = parseFloat($('#SurveyModal #'+$map_element).attr('data-lat'));
                $map_lng = parseFloat($('#SurveyModal #'+$map_element).attr('data-lng'));

                $('#'+$map_element).width($map_width+'px');
                $('#'+$map_element).height($map_height+'px');

                SURVEY.custom.map($map_element, $map_lat, $map_lng, $map_zoom, $map_type);

            }, 2000);
        },

        step2: function()
        {
            $($step1).addClass('hide');
            $($step2).removeClass('hide');

            var imageUrl = SURVEY.global.map_image();
            SURVEY.custom.convertImage(imageUrl);

            var canvas = localStorage.getItem( "savedImageData" );
            $($mapImage).attr('src', canvas);

            SURVEY.custom.cropper($mapImage);

        },

        step3: function()
        {
            $($step2).addClass('hide');
            $($step3).removeClass('hide');

            var url = $($mapImage).cropper('getCroppedCanvas').toDataURL("image/png");
            $('#cropImage').attr('src', url);
            SURVEY.custom.canvas_start();
            $($step4).removeClass('hide');
            var itemURL = '';
            document
                .getElementById('drag-items')
                .addEventListener('dragstart', function(e) {
                    itemURL = e.target.src;
                });

            var con = stage.container();
            con.addEventListener('dragover', function(e) {
                e.preventDefault(); // !important
            });

            con.addEventListener('drop', function(e) {
                e.preventDefault();
                // now we need to find pointer position
                // we can't use stage.getPointerPosition() here, because that event
                // is not registered by Konva.Stage
                // we can register it manually:
                stage.setPointersPositions(e);

                SURVEY.custom.add_image(itemURL);

                /*Konva.Image.fromURL(itemURL, function(image) {
                  layer.add(image);

                  image.position(stage.getPointerPosition());
                  image.draggable(true);

                  layer.draw();
                });*/


            });


        },

        map_image: function()
        {

            var staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap";

            //Set the Google Map Center.
            staticMapUrl += "?center=" + $map.getCenter().lat() + "," + $map.getCenter().lng();

            //Set the Google Map Size.
            staticMapUrl += "&size=" + $map_width + "x" + $map_height;

            //Set the Google Map Zoom.
            staticMapUrl += "&zoom=" + $map.getZoom();

            //Set the Google Map Type.
            staticMapUrl += "&maptype=" + $map_type;

            //Set the Google Map Key.
            staticMapUrl += "&key=" + $map_key;

            return staticMapUrl;

        },

        test: function() {

            var canvas = document.querySelector('#myCanvas canvas');

            var img = new Image;

            img.crossOrigin = "Anonymous";

            img.onload = function() {
                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage( img, 0, 0 );

                //localStorage.setItem( "savedImageData", canvas.toDataURL("image/png") );
            }

            //img.src = src;
            $('#finalimg').attr('src', canvas.toDataURL("image/png"));


            $.ajax({

                type     :'POST',

                cache    : false,

                data: {recid: $('#recid').val(), iamgefile: canvas.toDataURL('image/png')},

                url  : '/projecttracking/main/web/index.php?r=location/location/savesiteimage',

                success  : function(response) {

                    // self.location.reload();
                    // window.location = '/projecttracking/main/web/index.php?r=location/location/index';

                    //$('#SurveyModal').css('display','none');

                    //$('.modal-backdrop').css('position','relative', 'important');

                },

                error: function(){

                    console.log('failure');

                }

            });


        }

    };

    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // Custom functions
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    SURVEY.custom = {

        init: function() {

        },

        map: function(ele, lat, lng, zoom, type) {

            $map = new google.maps.Map(document.getElementById(ele), {
                zoom: zoom,
                center: {lat: lat, lng: lng},
                mapTypeId: type,
                disableDefaultUI: true,
                tilt: 0,
                zoomControl : true,
            });

        },

        convertImage: function (src) {

            var img = new Image,
                canvas = document.createElement("canvas"),
                ctx = canvas.getContext("2d");

            img.crossOrigin = "Anonymous";

            img.onload = function() {
                canvas.width = img.width;
                canvas.height = img.height;
                ctx.drawImage( img, 0, 0 );

                localStorage.setItem( "savedImageData", canvas.toDataURL("image/png") );
            }

            img.src = src;
        },

        cropper: function (ele) {

            $(ele).cropper({
                viewMode : 1,
                autoCrop: false,
                //zoomable: false
            });

        },

        canvas_start: function() {
            var img = document.getElementById('cropImage');
            SURVEY.custom.get_img(img.src);

            stage = new Konva.Stage({
                container: 'myCanvas',
                width: localStorage.getItem('cwidth'),
                height: localStorage.getItem('cheight'),
            });
            layer = new Konva.Layer();
            stage.add(layer);
            SURVEY.custom.add_image(img.src, 'no');
        },

        get_img: function (src, callback)
        {
            var img = new Image();
            img.src = src;
            img.onload = function(){
                var imgSize = {
                    w: img.width,
                    h: img.height
                };
                localStorage.setItem( "cwidth", imgSize.w );
                localStorage.setItem( "cheight", imgSize.h );
            };
        },

        add_image: function(src, opt = '')
        {
            var drag = (opt) ? false : true;

            var darthVaderGroup = new Konva.Group({
                draggable: drag
            });

            layer.add(darthVaderGroup);

            var imageObj1 = new Image();
            imageObj1.src = src;

            var width = imageObj1.width;
            var height = imageObj1.height;
            var imname = imageObj1.name;

            var darthVaderImg = new Konva.Image({
                width: width,
                height: height,
                imname: imname
            });

            darthVaderGroup.add(darthVaderImg);

            if(drag)
            {
                SURVEY.custom.addAnchor(darthVaderGroup, 0, 0, 'topLeft');
                SURVEY.custom.addAnchor(darthVaderGroup, width, 0, 'topRight');
                SURVEY.custom.addAnchor(darthVaderGroup, width, height, 'bottomRight');
                SURVEY.custom.addAnchor(darthVaderGroup, 0, height, 'bottomLeft');
            }

            imageObj1.onload = function() {
                darthVaderImg.image(imageObj1);
                layer.draw();
            };

            /*var tr = new Konva.Transformer();
            layer.add(tr);
            tr.attachTo(darthVaderGroup);
            layer.draw();*/


            /*stage.on('click tap', function(e) {
            // if click on empty area - remove all transformers
            if (e.target === stage) {
              stage.find('Transformer').destroy();
              layer.draw();
              return;
            }
            // do nothing if clicked NOT on our rectangles
            if (!e.target.hasName('rect')) {
              return;
            }
            // remove old transformers
            // TODO: we can skip it if current rect is already selected
            stage.find('Transformer').destroy();

            // create new transformer
            var tr = new Konva.Transformer();
            layer.add(tr);
            tr.attachTo(e.target);
            layer.draw();
          });*/


        },

        addAnchor: function(group, x, y, name) {
            var stage = group.getStage();
            var layer = group.getLayer();

            var anchor = new Konva.Circle({
                x: x,
                y: y,
                stroke: '#666',
                fill: '#ddd',
                strokeWidth: 2,
                radius: 2,
                name: name,
                draggable: true,
                dragOnTop: false
            });

            anchor.on('dragmove', function() {
                SURVEY.custom.update(this);
                layer.draw();
            });
            anchor.on('mousedown touchstart', function() {
                group.draggable(false);
                this.moveToTop();
            });
            anchor.on('dragend', function() {
                group.draggable(true);
                layer.draw();
            });
            // add hover styling
            anchor.on('mouseover', function() {
                var layer = this.getLayer();
                document.body.style.cursor = 'pointer';
                this.strokeWidth(4);
                layer.draw();
            });
            anchor.on('mouseout', function() {
                var layer = this.getLayer();
                document.body.style.cursor = 'default';
                this.strokeWidth(2);
                layer.draw();
            });

            group.add(anchor);
        },

        update: function(activeAnchor) {
            var group = activeAnchor.getParent();

            var topLeft = group.find('.topLeft')[0];
            var topRight = group.find('.topRight')[0];
            var bottomRight = group.find('.bottomRight')[0];
            var bottomLeft = group.find('.bottomLeft')[0];
            var image = group.find('Image')[0];

            var anchorX = activeAnchor.getX();
            var anchorY = activeAnchor.getY();

            // update anchor positions
            switch (activeAnchor.getName()) {
                case 'topLeft':
                    topRight.y(anchorY);
                    bottomLeft.x(anchorX);
                    break;
                case 'topRight':
                    topLeft.y(anchorY);
                    bottomRight.x(anchorX);
                    break;
                case 'bottomRight':
                    bottomLeft.y(anchorY);
                    topRight.x(anchorX);
                    break;
                case 'bottomLeft':
                    bottomRight.y(anchorY);
                    topLeft.x(anchorX);
                    break;
            }

            image.position(topLeft.position());

            var width = topRight.getX() - topLeft.getX();
            var height = bottomLeft.getY() - topLeft.getY();
            if (width && height) {
                image.width(width);
                image.height(height);
            }
        }

    };


    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    // initialize when document ready
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    SURVEY.documentOnReady = {

        init: function(){
            SURVEY.global.init();
        }

    }


    var
        //map variables
        $map_key = 'AIzaSyAwWq0jnxsbjA6Y5E_4JXI6Z3WRVzqx8yA',
        $map,
        $map_zoom = 21,
        $map_type = 'satellite',
        $map_lat,
        $map_lng,
        $map_width = '600';
    $map_height = '400';
    $map_element = 'googleMap',

        //Step 1
        $step1 = '.step1',
        $surveyModalButton = '.SurveyModalButton',
        $surveyModal = '#SurveyModal',
        $surveyModalContent = '#SurveyModalContent',
        $surveyModalHeader = '#SurveyModalHeader',
        $MapCaptureButton = '.MapCaptureButton',

        //Step 2
        $step2 = '.step2',
        $mapImage = '#mapImage',
        $resetCrop = '.resetCrop',
        $saveCrop = '.saveCrop';

    //Step 3
    $step3 = '.step3',
        $step4 = '.step4',
        $saveimage = '.saveImg',




        //!!!!!!!!!!!!!
        // initializing
        //!!!!!!!!!!!!!
        $(document).ready( SURVEY.documentOnReady.init );




});