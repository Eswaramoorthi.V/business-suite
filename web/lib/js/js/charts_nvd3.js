var nvd3Charts = function () {

    var myColors = ["#00B000", "#FFC000", "#FF0000", "#FF702A", "#DA3610",
        "#80CDC2", "#A6D969", "#D9EF8B", "#FFFF99", "#F7EC37", "#F46D43",
        "#E08215", "#D73026", "#A12235", "#8C510A", "#14514B", "#4D9220",
        "#542688", "#4575B4", "#74ACD1", "#B8E1DE", "#FEE0B6", "#FDB863",
        "#C51B7D", "#DE77AE", "#EDD3F2"];
    d3.scale.myColors = function () {
        return d3.scale.ordinal().range(myColors);
    };

    var drawDountChart = function (chartId, data,chartTotal,chartColor) {
        //Donut chart example
        nv.addGraph(function () {
            var width = 220, height = 150;
            var chart = nv.models.pieChart().x(function (d) {
                return d.label;
            }).y(function (d) {               
                return d.value;
            }).showLabels(true)//Display pie labels
                    .labelThreshold(.05)//Configure the minimum slice size for labels to show up
                    .labelType("value")//Configure what type of data to show in the label. Can be "key", "value" or "percent"
                    .donut(true)//Turn on Donut mode. Makes pie chart look tasty!
                    .donutRatio(0.35)//Configure how big you want the donut hole size to be.
                    .color(chartColor).width(width);
            chart.valueFormat(d3.format('d'));

             
            if (data !== 'null') {

                d3.select("#" + chartId + " svg").datum(parseData(data)).transition().duration(350).call(chart).call(centerText(chartId,chartTotal));
            } else {
                d3.select("#" + chartId + " svg").datum(parseData()).transition().duration(350).call(chart).call(centerText(chartId,chartTotal));
            }

                chart.pie.dispatch.on("elementClick", function(e) {
                    console.log(e.point.href);
                     window.location.href = e.point.href;
                });

            return chart;
        });

        //Pie chart example data. Note how there is only a single array of key-value pairs.
        function parseData(data = "") {
         
                return JSON.parse(data);
        }
        function centerText(chartId,chartTotal) {
            return function () {
                var svg = d3.select("#" + chartId + " svg");

                var donut = svg.selectAll("g.nv-slice").filter(
                        function (d, i) {
                            return i == 0;
                        }
                );

                // Insert first line of text into middle of donut pie chart
                donut.insert("text", "g")
                        .text("Total :"+chartTotal)                        
                         .attr("class", "css-label-class")
                         .attr("text-anchor", "middle")
                        .style("fill", "#000");
                // Insert second line of text into middle of donut pie chart

            }
        }

    };



    function centerText(chartId) {
        return function () {
            var svg = d3.select("#" + chartId + " svg");

            var donut = svg.selectAll("g.nv-slice").filter(
                    function (d, i) {
                        return i == 0;
                    }
            );

            // Insert first line of text into middle of donut pie chart
            donut.insert("text", "g")
                    .text("Total : 359")
                    .attr("font-family", "sans-serif")
                    .attr("font-size", "40px")
                    .attr("text-anchor", "middle")
                    .style("fill", "#000");
            // Insert second line of text into middle of donut pie chart

        }
    }
    return {
        init: function (chartID, chartData,chartTotal,chartColor) {
            var parsedJson = $.parseJSON(chartColor);
            drawDountChart(chartID, chartData,chartTotal,parsedJson);

        }
    };
}();

