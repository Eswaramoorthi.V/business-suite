<?php

$this->title = 'Create Menu';
$this->params['breadcrumbs'][] = ['label' => 'Menu Management', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-th-large';
?>
<div class="category-master-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
