<?php


use yii\helpers\Url;
use yii\web\JsExpression;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\web\View;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategoryMaster\Search */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Menu';
$this->params['breadcrumbs'][] = 'Menu';
//$this->params['title_icon'] = 'fa-list';
?>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-list" aria-hidden="true"></i> Menu
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
       <?= Html::button('Add Menu', ['value' => Url::to('create?r=menu/create'), 'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
    </div>
</div>

<?php
Modal::begin([
    'header' => '<h4>Add Menu</h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Menu</h4>',
    'id' => 'update',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalupdate'><div class='loader'></div></div>";
Modal::end();
?>
<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
          
            [
                'attribute' => 'name',
                'header' => '<div style="width:120px;">Menu Name</div>',
                'value' => 'name',
            ],
            
            [
                'attribute' => 'parent_id',
                'header' => '<div style="width:120px;">Parent</div>',
                'value' => function($model) {
                $category = ArrayHelper::map($model->getMenus(), 'id', 'name');
                return !empty($model->parent_id)? $category[$model->parent_id]:'-';            
                }
            ],
            [
                'attribute' => 'type',
                'header' => '<div style="width:120px;">Type</div>',
                'value' => function($model) {
                 return !empty($model->type)? $model->type:'-';            
                }
            ],
           
            // [
            //     'class' => 'yii\grid\ActionColumn',
            //     'contentOptions' => ['style' => 'width:80px;text-align:center','class'=>'skip-export'],
            //     'headerOptions' =>['class'=>'skip-export'],
            //     'header' => "Actions",
            //     'template' => ' {update}{deletes}',
            //     'buttons' => [
                  
            //         'update' => function($url) {
            //             return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'update', 'title' => 'Edit']
            //             );
            //         },
            //         'deletes' => function ($url) {
            //             return Html::a(
            //                             '<span class="glyphicon glyphicon-trash icons"></span>', $url, [
            //                         'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
            //                             ]
            //             );
            //         },
            //     ],
            // ],
            [
                
                'class' => '\kartik\grid\ActionColumn',
                'mergeHeader' => false,
                    'contentOptions' => ['style' => 'width:50px;text-align:center','class'=>'skip-export'],
                    'headerOptions' =>['class'=>'skip-export'],
                    'header'=>"Actions",
                'template'=>$actionList,
                // 'template'=>'{update}',
                'dropdown' => true,
                'dropdownOptions'=>['class'=>'pull-right' ],
                'options' => ['style' => 'max-width:20px;'],
                'buttons' => [
                   
                    'update' => function($url) {
                        return Html::a('Update',   $url,['class' => 'action-btn dropdown-item update ', 'title' => 'Update']
                        );
                    },
                    'deletes' => function($url) {
                       return Html::a('Delete',     $url,  ['class' => 'action-btn dropdown-item','data-method'=>'post', 'data-pjax'=>0,'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?']]
                       );
                   },
                  ]
                
                ],
        ],
        'summary'=>'',
        
       
        
    ]);
}
?>                       


<?= $this->registerJs("
    $(function(){
$('#modalButton').click(function() {
    $('#modal').modal('show')
    .find('#modalContent')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>

<?=
$this->registerJs(
        "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.update').click(function(e){
       e.preventDefault();      
       $('#update').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
?>

<style>
a.action-btn.dropdown-item {
  display: block;
  width: 100%;
  padding: .25rem 1.5rem;
  clear: both;
  font-weight: normal;
  text-align: inherit;
  white-space: nowrap;
  background-color: transparent;
  border: 0;
  color: navy;
  font-size: small;
}
</style>