<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-master-form form-vertical">

    <?php $form = ActiveForm::begin(); ?>

<div class="row">
<div class="col-md-4">
    <?=
    $form->field($model, 'name', [
        'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
    ])->textInput(['maxlength' => true,'placeholder' =>"Enter Menu"]);
    ?>

    <?php $category = ArrayHelper::map($model->getMenus(), 'id', 'name'); ?>
    <?=
    $form->field($model, 'parent_id', [
        'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
    ])->dropDownList($category, [
       // 'class' => 'select form-control',
        'prompt' => 'Choose parent category',
        'data-live-search' => 'true'
    ])
    ?>

    <?=
    $form->field($model, 'position', [
        'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
    ])->textInput(['maxlength' => true]);
    ?>
    
    <?=
    $form->field($model, 'level', [
        'template' => "{label}\n
  <div class='col-md-12 col-xs-12'>
        {input}\n
        {hint}\n
        {error}
  </div>",
        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
    ])->textInput(['maxlength' => true]);
    ?>

    <?php
        echo $form->field($model, 'url', [
            'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
        ])->textInput(['maxlength' => true,'placeholder' =>"Enter URL"]);
        ?>
</div>
   <div class="col-md-4">
        <?=
        $form->field($model, 'controller', [
            'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
        ])->textInput(['maxlength' => true,'placeholder' =>"Enter Controller Name"]);
        ?>

        <?=
        $form->field($model, 'action', [
            'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
        ])->textInput(['maxlength' => true,'placeholder' =>"Enter Action"]);
        ?>

        <?=
        $form->field($model, 'icon', [
            'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
        ])->textInput(['maxlength' => true,'placeholder' =>"Enter Icon"]);
        ?>

        <?=
        $form->field($model, 'role_list', [
            'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
           {input}\n
           <span class='clr'>Note: </span> Data enter in the above field by comma separated before <span class='clr'>|</span> will display to user to manage roles. 
           Data contains after <span class='clr'>|</span> by comma separated will automatically added to auth manager table when user activate <span class='clr'>Read Mode</span> from Map Roles Module</span>
            {hint}\n
            {error}
      </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
        ])->textArea(['maxlength' => true, 'rows' => 2, 'cols' => 100,'placeholder' =>"Enter Roles"]);
        ?>
</div>
    <div class="col-md-4">
        <?php
        $a = ['1' => 'Yes', '0' => 'No'];
        echo $form->field($model, 'role_list_disp', [
            'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
        ])->dropDownList($a, [
            //'class' => 'select form-control',
            'selected' => true,
        ]);
        ?>
        
    <?php
        $menu = ['1' => 'Yes', '0' => 'No'];
        echo $form->field($model, 'menu_disp', [
            'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
        ])->dropDownList($menu, [
           // 'class' => 'select form-control',
            'selected' => true,
        ]);
        ?>
   
    <?php
        $b = ['others' => 'Others', 'report' => 'Report'];
        echo $form->field($model, 'type', [
            'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
        ])->dropDownList($b, [
          //  'class' => 'select form-control',
            'selected' => true,
        ]);
        ?>
  
        <?php
        $s = ['1' => 'Yes', '0' => 'No'];
        echo $form->field($model, 'status', [
            'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label']
        ])->dropDownList($s, [
            //'class' => 'select form-control',
            'selected' => true,
        ]);
        ?>
</div>
    <div class="form-group">
        <label class="col-md-3 col-xs-12 control-label" for=""></label>
        <div class="col-md-12 col-xs-12">
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>            
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
</div>
<style>
    .clr {
        color:red;
        font-weight: bold;
    }
</style>