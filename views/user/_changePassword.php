<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ScheduleMaster */

$this->title = 'Commit Schedules';
$this->params['breadcrumbs'][] = ['label' => 'Schedule Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schedule-master-commit">

    <?= $this->render('_commitform', [
        'model' => $model,
        'lists' => $lists
    ]) ?>

</div>
