<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserGroup */

?>
<div class="user-group-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
