<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="page-content">
    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2> New User</h2>
    </div>
    <!-- END PAGE TITLE -->

    <!-- PAGE CONTENT WRAPPER -->
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-8">
                <div class="panel-body">
                    <!-- START JQUERY VALIDATION PLUGIN -->
                    <div class="register-user-form form-horizontal">

                        <?php $form = ActiveForm::begin([
                            'options' => ['enctype'=>'multipart/form-data']
                        ]); ?>

                        <?= $form->field($model, 'username', [
                          'template' => "{label}\n
                          <div class='col-md-9 '>
                                <div class='input-group'>
                                    <span class='input-group-addon'>
                                        <span class='fa fa-pencil'></span>
                                    </span>
                                    {input}\n
                                </div>
                                {hint}\n
                                {error}
                          </div>",
                          'labelOptions' => [ 'class' => 'col-md-3  control-label' ]
                        ])->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'nric', [
                          'template' => "{label}\n
                          <div class='col-md-9 '>
                                <div class='input-group'>
                                    <span class='input-group-addon'>
                                        <span class='fa fa-pencil'></span>
                                    </span>
                                    {input}\n
                                </div>
                                {hint}\n
                                {error}
                          </div>",
                          'labelOptions' => [ 'class' => 'col-md-3  control-label' ]
                        ])->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'email', [
                          'template' => "{label}\n
                          <div class='col-md-9 '>
                                <div class='input-group'>
                                    <span class='input-group-addon'>
                                        <span class='fa fa-pencil'></span>
                                    </span>
                                    {input}\n
                                </div>
                                {hint}\n
                                {error}
                          </div>",
                          'labelOptions' => [ 'class' => 'col-md-3  control-label' ]
                        ])->textInput(['maxlength' => true,'placeholder'=>'test@example.com']) ?>

                        <?= $form->field($model, 'phone', [
                          'template' => "{label}\n
                          <div class='col-md-9 '>
                                <div class='input-group'>
                                    <span class='input-group-addon'>
                                        <span class='fa fa-pencil'></span>
                                    </span>
                                    {input}\n
                                </div>
                                {hint}\n
                                {error}
                          </div>",
                          'labelOptions' => [ 'class' => 'col-md-3  control-label' ]
                        ])->textInput(['maxlength' => true,'placeholder'=>'+65-88889999']) ?>

                        <?php /* $form->field($model, 'birth_date', [
                          'template' => "{label}\n
                          <div class='col-md-9 '>
                                {input}\n
                                {hint}\n
                                {error}
                          </div>",
                          'labelOptions' => [ 'class' => 'col-md-3  control-label' ],
                          'inputOptions' => [ 'class' => 'form-control datepicker-rms' ],
                        ])->textInput() */?>

                        <?= $form->field($model, 'attachment', [
                          'template' => "{label}\n
                          <div class='col-md-9 '>
                                {input}\n
                                {hint}\n
                                {error}
                          </div>",
                          'labelOptions' => [ 'class' => 'col-md-3  control-label' ],
                          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false" ]
                        ])->fileInput() ?>

                        <div class="form-group">
                            <label class="col-md-3  control-label" for=""></label>
                            <div class="col-md-9 ">
                                <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
