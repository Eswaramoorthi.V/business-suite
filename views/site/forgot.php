<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="page-content">
    <!-- PAGE TITLE -->
    <div class="page-title">
        <h2> Forgot Password</h2>
    </div>
    <!-- END PAGE TITLE -->

    <!-- PAGE CONTENT WRAPPER -->
    <?= Yii::$app->session->getFlash('msg') ?>
    <div class="page-content-wrap">
        <div class="row">
            <div class="col-md-8">
                <div class="panel-body">
                    <!-- START JQUERY VALIDATION PLUGIN -->
                    <div class="register-user-form form-horizontal">

                        <?php $form = ActiveForm::begin([
                            'options' => ['enctype'=>'multipart/form-data']
                        ]); ?>

                        <?= $form->field($model, 'email', [
                            'template' => "{label}\n
                          <div class='col-md-6'>
                                <div class='input-group'>
                                    <span class='input-group-addon'>
                                        <span class='fa fa-pencil'></span>
                                    </span>
                                    {input}\n
                                </div>
                                {hint}\n
                                {error}
                          </div>",
                            'labelOptions' => [ 'class' => 'col-md-3  control-label' ]
                        ])->textInput(['maxlength' => true,'placeholder'=>'test@example.com']) ?>


                        <div class="form-group">
                            <label class="col-md-3  control-label" for=""></label>
                            <div class="col-md-9 ">
                                <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
