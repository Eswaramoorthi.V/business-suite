<?php

use yii\helpers\ArrayHelper;
use app\models\Permission;
use yii\web\View;
use yii\helpers\Url;
use kartik\select2\Select2;




$this->title = 'Role Permission ['.$role. ']';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Role Permission'), 'url' => ['/permission/index']];

$this->params['breadcrumbs'][] = $this->title;

$getRoles = ArrayHelper::map(\app\models\AuthItem::find()->where("type=1 AND name!='Admin'")->all(), 'name', 'name');

$getMaproles = \app\components\MenuHelper::getMapRole();
// pr($getMaproles);
?>

<style type="text/css">
  
ul {
    padding: 0;
    list-style-type: none;
}


.role-container {
    border: 2px solid rgba(153,178,183,0.2);
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    margin-bottom: 10px;
}

.role-subcontainer {
    margin-left: 10px;
    margin-right: 10px;
}

i {
    margin-right: 5px;
    margin-left: 10px;
}

.sitemap ul {
    margin: 1em 0 1em 0;
    padding: 0;
    list-style: none;
    line-height: 3;
}


.heading1 {
    font-weight: bold;
    font-size: 14px;
    margin-top: 10px;
    margin-bottom: 10px;
    color: #57b495;
}


.sitemap .subitems li {
    float: left;
    margin-left: 25px;
}

.main-heading {
    font-weight: bold;
    font-size: 18px;
}

.main-heading-clr {
    color: #FF4500;
}

.heading2 {
    font-weight: bold;
    font-size: 12px; 
    margin-top: 20px;
    color: #FF4500;
}

.heading3 {
    font-weight: bold;
    font-size: 12px; 
    margin-top: 20px;
    color: #9e2e43;
}
.heading4 {
    font-weight: bold;
    font-size: 12px; 
    margin-top: 20px;
    color: #60605f;
}

.inner-container {
   /* background-color: #f8f5f5;  
    border: 2px solid rgba(153,178,183,0.2);*/
    -webkit-border-radius: 10px;
    -moz-border-radius: 10px;
    border-radius: 10px;
    margin-bottom: 10px;
} 

.subsub-class {
    background-color: #f4f4f4;
}

</style> 

<div class="row">

    <div class="col-md-4 pull-right">
        <strong>Select Role : </strong>

        <?php
        echo Select2::widget([
            'name' => 'site_id',
            'value'=>isset($_GET['role'])?$_GET['role']:'',
            'data'=>$getRoles,
            'options' => [
                'prompt'=>'select',
                'class' => 'form-control contractor',
                'id'=>'roleType'
            ],
        ]);


        ?>

    </div>
</div>


 <div class="list-group border-bottom" id="role-list" data-url="<?= Url::toRoute('/permission/index') ?>">

    </div>
    <div class="ibox float-e-margins sitemap ">

    <div class="ibox-content box hide">
   
        <?php

        if($role !=''){
        
        foreach ($getMaproles as $mData) {


               
                //pr1($mData);
              $mainHeadclass = "show";
              if($mData['label']=='Project' || $mData['label']=='Client' || $mData['label']=='Suppliers' || $mData['label']=='User Role' || $mData['label']=='Subcontractors' || $mData['label']=='Employee')
                {
                    //continue;
     
                    //pr1("SDSDSDSD----".$mData['label']."----".$mData['item']['id']);
                    ?>
                    <div class="row role-container">
                    <div class="role-subcontainer">
                    <br>
                    <div class="col-md-12"><i class="<?=$mData['icon']; ?>"></i> 
                    <span class="heading1"><?=$mData['label']; ?> </span></div>

                    <?php
                                  $listcntrMain = '';  
                                  if($mData['item']['role_list']!='') { 
                                        $rolesAll = explode("|", $mData['item']['role_list']);

                                         $menuId = $mData['item']['id'];
                                         $child = 'auth/' . $mData['item']['controller'];
                                         $controller = $mData['item']['controller'];
                                         $pid = $mData['item']['parent_id'];
                                         $level = $mData['item']['level'];


                                        $roles = explode(",", $rolesAll[0]);
                                     //   pr($roles);
                                        $listcntrMain .= '<ul class="subitems col-md-12">';
                                     foreach ($roles as $roleData) {
                                            $childValDispMain = ($roleData == 'index') ? 'read' : $roleData;
                                            $childValmain = $child . '/' . $roleData;
                                            $childValDispMain = ucwords(str_replace('-', ' ', $childValDispMain));
                                        //    pr($childauthList);
                                        //    die();
                                            if (in_array($childValmain, $childauthList)) {
                                                $showhide = "checked";
                                            } else {
                                                $showhide = "";
                                            }
                       //if(is_tab) 
                                            // pr1($roleData);

                                            // if($roleData=='system-setup') {
                                            //     echo "HERE";
                                            //     pr($mData['item']);
                                            // }

                        $listcntrMain .= "<li><input type='checkbox'  $showhide data-parent='$role' data-child='$childValmain' "
                            . "data-menuid='$menuId' data-item='$roleData' data-level='$level' data-parentid='$pid' data-roleothers='$roleothers' data-controller='$controller' class='roleSettings'>" . $childValDispMain .
                            "</li>";
                  
                            } 
                        // $listcntrMain .= '</ul>';
                            echo $listcntrMain;
                            }
                  ?>
                  </div></div>
                    <?php          

                


              $mainHeadclass = "hide";
         }

         $mainLbl = ucwords(str_replace('-', ' ', $mData['label']));

        ?>
                <div class='row role-container <?php echo $mainHeadclass; ?>'>
                    <div class="role-subcontainer">   
                    <br>
                    <div class="col-md-12"><i class="<?=$mData['icon']; ?>"></i> 
                    <span class="heading1"><?=$mainLbl; ?> </span></div>
                        <?php if($mData['items']!='') { 
                            foreach ($mData['items'] as $mDataItem) { 
                              //  pr1($mDataItem);
                               //pr($mDataItem['item']['role_list']);

                                $menuId = $mDataItem['item']['id'];
                                $child = 'auth/' . $mDataItem['item']['controller'];
                                $controller = $mDataItem['item']['controller'];
                                $pid = $mDataItem['item']['parent_id'];
                                $level = $mDataItem['item']['level'];

                                $roleothers = '';
                                if(isset($rolesAll[1])) {
                                    $roleothers = $rolesAll[1];
                                }
                                // if($mDataItem['label']=='System Setup') {
                                //     pr1($mDataItem);
                                // } is_tab
                                
                                    $colmdClass ="col-md-12";
                                    // if($mData['label']=='My Profile') {
                                    //     $colmdClass ="col-md-3";
                                    // } 

                                    // if($mDataItem['label']=='Projects' || $mDataItem['label']=='Tasks'|| $mDataItem['label']=='Customer Organization Info' || (!empty($mDataItem['items']))) {
                                    //     $colmdClass ="col-md-12";
                                    // }
                                     $ulcolmdClass =""; 
                                     $liClass = "";
                                    if((!empty($mDataItem['items']))) {
                                       // pr1($mDataItem['label']);
                                        $ulcolmdClass ="col-md-12";
                                        // if($mDataItem['label']!="Projects")
                                        // $liClass ="heading3";
                                    }

                                ?>


                                    
                                    <div class="<?php echo $colmdClass; ?> inner-container">
                                         <span class="heading2"><i class="<?=$mDataItem['icon']; ?> heading2"></i> <?=ucwords($mDataItem['label']); ?></span>

                                  <?php 

                                  $listcntr = "";  
                                  if($mDataItem['item']['role_list']!='') { 
                                        $rolesAll = explode("|", $mDataItem['item']['role_list']);




                                        $roles = explode(",", $rolesAll[0]);

                                        // if($mDataItem['item']=='Projects') {
                                        //   // pr($roles); 
                                        // }
                                        // pr1($mDataItem['item']);
                                        // pr1($roles);
                                        $listcntr .= "<ul class='subitems col-md-12'>";
                                     foreach ($roles as $roleData) {
                                            $childValDisp = ($roleData == 'index') ? 'read' : $roleData;
                                            $childVal = $child . '/' . $roleData;
                                            $childValDisp = ucwords(str_replace('-', ' ', $childValDisp));
// pr($childauthList);
                                            if (in_array($childVal, $childauthList)) {
                                                $showhide = "checked";
                                            } else {
                                                $showhide = "";
                                            }
                     

                        $listcntr .= "<li class='$liClass'><input type='checkbox'  $showhide data-parent='$role' data-child='$childVal' "
                            . "data-menuid='$menuId' data-item='$roleData' data-level='$level' data-parentid='$pid'  data-controller='$controller' class='roleSettings'>" . $childValDisp .
                            "</li>";
                  
                            } 
                        $listcntr .= '</ul>';
                            echo $listcntr;
                            }




                         //<!-- SUB SUB STARTS HERE -->

                         if(!empty($mDataItem['items'])) { 
                            //echo  '<div class="">';

                            //pr1($mDataItem['items']);
                            foreach ($mDataItem['items'] as $mDataItemSub) { 
                               // pr1($mDataItem);
                               //pr($mDataItem['item']['role_list']);

                                $menuIdSub = $mDataItemSub['item']['id'];
                                $childSub = 'auth/' . $mDataItemSub['item']['controller'];
                                $controllerSub = $mDataItemSub['item']['controller'];
                                $pidSub = $mDataItemSub['item']['parent_id'];
                                $levelSub = $mDataItemSub['item']['level'];

                                $colmdClassSub ="";

                                $lblDisp = $mDataItemSub['label'];
                                
                                // if(($mDataItemSub['item']['is_tab']==1 && $mDataItem['label']=='Projects') OR ($mDataItemSub['item']['is_tab']==1 && $mDataItem['label']=='Tasks') OR  ($mDataItemSub['item']['is_tab']==1 && $mDataItem['label']=='Customer Organization Info')) {
                                //     $colmdClassSub ="col-md-6 subsub-class";
                                // }
                                   
                                ?>

                                    
                                    <div class="<?php echo $colmdClassSub; ?> inner-container">
                                         <span class="heading4"><i class="<?=$mDataItemSub['icon']; ?>"></i> <?=$lblDisp; ?></span>

                                  <?php 

                                  $listcntrSub = "";  
                                  if($mDataItemSub['item']['role_list']!='') { 
                                        $rolesAllSub = explode("|", $mDataItemSub['item']['role_list']);

                                        $rolesSub = explode(",", $rolesAllSub[0]);
                                        //pr1($rolesSub);
                                        $listcntrSub .= '<ul class="subitems col-md-12">';

                                     foreach ($rolesSub as $rSubkey=> $roleDataSub) {
                                             if(($rSubkey==0 && $mDataItem['label']=='Admin')) {
                                                $chkTabDisp = '';
                                            }
                                        else if(($rSubkey==0 && $mDataItem['label']=='User Settings')) {
                                            $chkTabDisp = '';
                                        }
                                        else if(($rSubkey==0 && $mDataItem['label']=='Map Roles')) {
                                            $chkTabDisp = '';
                                        }
                                        
                                        else {
                                                $chkTabDisp = '';
                                            }
                                            $childValDispSub = ($roleDataSub == 'index') ? 'read' : $roleDataSub;
                                            $childValSub = $childSub . '/' . $roleDataSub;
                                            $childValDispSub = ucwords(str_replace('-', ' ', $childValDispSub));

                                            if (in_array($childValSub, $childauthList)) {
                                                $showhideSub = "checked";
                                            } else {
                                                $showhideSub = "";
                                            }
                    //    //if(is_tab)

                        $listcntrSub .= "<li><input type='checkbox'  $showhideSub data-parent='$role' data-child='$childValSub' "
                            . "data-menuid='$menuIdSub' data-item='$roleDataSub' data-level='$levelSub' data-parentid='$pidSub' data-roleothers='$roleothers' data-controller='$controllerSub' class='roleSettings'>" . $childValDispSub .$chkTabDisp.
                            "</li>";
                  
                                     }
                        $listcntrSub .= '</ul>';
                            echo $listcntrSub;
                            } 

                             ?>

                               </div>

                        <?php  

                        } 
                        //echo  '</div>';
                        }  ?>

                        <!-- SUB SUB ENDS HERE -->

                               </div>

                        <?php } }  ?>

                </div>              
                </div>
        <?php } 
        } ?>       
    
</div>
</div>




<style>

   


    input:checked + .slider {
        background-color: #1caf9a !important;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

</style>

<?php
echo $this->registerJs('
  var role = $("#roleType").val();
if(role!=""){
$(".box").removeClass("hide");
}
    $("#roleType").on("change", function() {
        var role = $(this).val();

        getRoles(role);
    });
', View::POS_READY);


echo $this->registerJs('$(".roleSettings").on("click", function() {        
                $("#loader_disp").removeClass("hide");
                var action = (this.checked)==true?1:0;
                var child = $(this).attr("data-child");
                var parent = $(this).attr("data-parent");
                var menuid = $(this).attr("data-menuid");   
                var item = $(this).attr("data-item");
                var roleothers = $(this).attr("data-roleothers");
                var controller = $(this).attr("data-controller"); 
                var level = $(this).attr("data-level"); 
                var parentid = $(this).attr("data-parentid"); 
                processRole(action, child, parent, menuid, item, roleothers, controller, level, parentid);
            });
       ', View::POS_READY);
?>

<script>

    function getRoles(roleType)
    {
        var url = $('#role-list').data('url');

        if (url === undefined) {
            return false;
        }
        location.href = url + "?role=" + roleType;
    }

    function processRole(action, child, parent, menuid, item, roleothers, controller, level, parentid) {

        $.ajax({
            url: "<?php echo \Yii::$app->getUrlManager()->createUrl('permission/roleupdate') ?>",
            data: {action: action, parent: parent, child: child, menuid: menuid, item:item, roleothers:roleothers, controller:controller, level:level, parentid:parentid},
            type: "POST",
            success: function () {
                console.log("Success");
            }
        });
    }
</script>
