

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use app\models\Customer;
$this->params['breadcrumbs'][] = 'Tender';
?>
<style type="text/css">
/* body {
    padding-top: 50px;
} */
.dropdown.dropdown-lg .dropdown-menu {
    margin-top: -1px;
    padding: 6px 20px;
}
.input-group-btn .btn-group {
    display: flex !important;
}
.btn-group .btn {
    border-radius: 0;
    margin-left: -12px;
}
.btn-group .btn:last-child {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
.btn-group .form-horizontal .btn[type="submit"] {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.form-horizontal .form-group {
    margin-left: 0;
    margin-right: 0;
}
.form-group .form-control:last-child {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
}

@media screen and (min-width: 768px) {
    #adv-search {
        width: 500px;
        margin: 0 auto;
    }
    .dropdown.dropdown-lg {
        position: static !important;
    }
    .dropdown.dropdown-lg .dropdown-menu {
        min-width: 500px;
    }
}
</style>

<script src="<?= Url::base() ?>/web/lib/js/bootstrap/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div class="container">
<div class="row">
<div class="col-md-6 " style="font-size: 30px">
<i class="fa fa-product-hunt" aria-hidden="true"></i> Tender
</div>
</div>
	<div class="row">
    <div class="col-md-6 margin-right-10" style="margin-left: 981px;">

 <?= Html::button('Add', ['value' => Url::to('create'), 'class' => 'btn btn-success',$visibleList['create'], 'id' => 'addModalButton']) ?>

</div>


</div>
<br>
<div class="row">

    <div class="col-md-6  margin-right-10" style="margin-left: 540px;">
            <div class="input-group" id="adv-search">
                <input type="text" class="form-control" name="searchtext" placeholder="Search" />
                <div class="input-group-btn">
                    <div class="btn-group" role="group">
                        <div class="dropdown dropdown-lg">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu">
                                <form class="form-horizontal" role="form" method="post" action="<?php echo $searchurl;?>">
                                  <div class="form-group">
                                    <label for="filter">Tender Type</label>
                                    <select class="form-control" id ="tender_type" name="tender_type">
                                    <?php
                                     
                                     $tenderType = [];

foreach ($tenderTypeData as $value) {
    $pType =[];
    $pType['id'] = $value['id'];
    $pType=$value['name'];
    $tenderType=$pType;
?>
    <option value="<?php echo $value['id']; ?>"><?php echo ($tenderType); ?></option>
  <?php } ?>
                                    </select>

                                  </div>
                                  <div class="form-group">
                                    <label for="filter">Building Type</label>
                                    <select class="form-control" id ="building_type" name="building_type">
                                    <?php
                                     
                                     $building_type = [];

foreach ($buildingTypeData as $value) {
    $pType =[];
    $pType['id'] = $value['id'];
    $pType=$value['name'];
    $building_type=$pType;
?>
    <option value="<?php echo $value['id']; ?>"><?php echo ($building_type); ?></option>
  <?php } ?>
                                    </select>
                                    
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Total Area</label>
                                    <input class="form-control" type="text" id =total_area name =total_area />
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Total Amount</label>
                                    <input class="form-control" type="text" id = total_amount name = total_amount />
                                  </div>
                                  
                                  <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>

                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
	</div>
</div>
<!-- </html> -->
<div class="row">
    <!-- <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-wrench" aria-hidden="true"></i> Project
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
       <?= Html::button('Add', ['value' => Url::to('tender/tender/create'), 'class' => 'btn btn-success',$visibleList['create'], 'id' => 'addModalButton']) ?>
    </div> -->
</div>

<?php
Modal::begin([
    'header' => '<h4>Add Tender</h4>',
    'id' => 'addModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalAddContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Tender</h4>',
    'id' => 'updateModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalUpdateContent'><div class='loader'></div></div>";
Modal::end();
?>


<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'tender_code',
                'header' => '<div style="width:120px;">Tender Code</div>',
                'value' => function($model, $key, $index) {
                    return $model->tender_code;
                    // return Html::a($model->project_code, ['/estimate/project/master', 'id' => $model->id], [ 'data-pjax'=>"0"]);
                },
                'vAlign' => 'middle',
                'format' => 'raw',
                'noWrap' => true
            ],
            [
                'attribute' => 'tender_name',
                'header' => '<div style="width:120px;">Tender Name</div>',
                'value' => function($model, $key, $index) {
                    return Html::a($model->tender_name, ['/tender/tender/master', 'id' => $model->id], [ 'data-pjax'=>"0"]);
                },
                'vAlign' => 'middle',
                'format' => 'raw',
                'noWrap' => true
            ],
         
            [
                'attribute' => 'client_name',
                'header' => '<div style="width:120px;">Client  Name</div>',
                'value' => function($model, $key, $index) {
                    $assign_clientData =Customer::find()->asArray()->all();
                    $assign_ClientData = [];
                    
                    foreach ($assign_clientData as $value) {
                        $eType =[];
                        $eType['id'] = $value['id'];
                        $eType['customer_fname'] = $value['customer_fname'].' '.$value['customer_lname'];
                        // $eType['customer_lname'] = $value['customer_lname'];
                        $assign_ClientData[]=$eType;
                    }
                    
                    $assign_client = ArrayHelper::map($assign_ClientData, 'id', 'customer_fname');
                    return !empty($assign_client[$model['client_name']])?$assign_client[$model['client_name']]:'-';
                       
                    },
                
            ],
            [
                'attribute' => 'tender_due_date',
                'header' => '<div style="width:180px;">Tender Due Date</div>',
                 'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->tender_due_date, 'php:d-m-Y');
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:200px;text-align:center','class'=>'skip-export'],
                'headerOptions' =>['class'=>'skip-export'],
                'header' => "Action",
                // 'template' => ' {update}{deletes}',
                'template'=>$actionList,
                'buttons' => [
                  
                    'update' => function($url) {
                        return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'updateModalButton', 'title' => 'Edit']);
                    },
                    'deletes' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', $url, [ 'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],]);
                    },
                    'convert-project' => function($url) {
                        return Html::a('<span class="fa fa-copy"></span> ', $url, [ 'title' => 'Convert Project']);
                    },
                    
                   
                ],
            ],
        ],
    ]);
}
?>                                   
<?= $this->registerJs("
$(function(){
$('#addModalButton').click(function() {
    $('#addModal').modal('show')
    .find('#modalAddContent')
    .load($(this).attr('value'));
});
});
", View::POS_READY); ?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updateModalButton').click(function(e){
        e.preventDefault();      
        $('#updateModal').modal('show')
        .find('#modalUpdateContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.copyModalButton').click(function(e){
        e.preventDefault();      
        $('#copyModal').modal('show')
        .find('#modalUpdateContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>