
<?php

use yii\helpers\Html;
// pr($productInfo);
?> 
<div >
<div>&nbsp;</div>
<div class="new_head">
<table class="info-table">
            <tr>
                <td>Tender Name<br></td>
                <td>:&nbsp;</td>
                <td valign="bottom"> <?php echo $tenderName?></td>
                   
                <td>Client Name</td> <td>:&nbsp;</td><td> <?php echo $clientName?> </td>
                <td> </td>
                <td > </td>

            </tr>
            <tr>

                <td>Reference</td>
                <td>:&nbsp;</td>
                <td > <?php echo $tender['reference']?></td>
                <
                <td>Due Date </td>
                <td>:&nbsp;</td>
                <td><?php $getdate =$tender['tender_due_date'];
                    echo Yii::$app->formatter->asDate($getdate, 'd-MMM-Y'); ?></td>

            </tr>
           
</table>

</div>
<div>&nbsp;</div>
<table class="table table-bordered " cellspacing="3" cellpadding="15" >
<tbody>
<tr class="rowheader">
<td width="5%" class="text-center text-bold" style="padding: 2px"><b><h4>Item </h4></b> </td>
    <td width="5%" class="text-center text-bold" style="padding: 2px"><b> <h4>Description </h4></b></td>
   <td width="5%" class="text-center text-bold" style="padding: 2px"><b>Unit</b></td>
    <td width="7%" class="text-center text-bold" style="padding: 2px"><b>Quantity</b></td>
  <td width="5%" class="text-center text-bold" style="padding: 2px"><b> Unit Rate (AED)</b></td>
    <td width="5%" class="text-center text-bold" style="padding: 2px"><b>Amount (AED)</b></td>
        </tr>
        <?php 
  $i=65;
  foreach($dataList as $key => $data) { ?>
  
    <tr>
      <td class="text-center"> <b><?php echo chr($i); ?> </b></td> <br>
      <td class="text-left"> <b><u><?php echo $key; ?></u> </b> </td><br>
      <td colspan="4"></td>
    </tr>
    <?php foreach($data as $key1 => $val) { ?>
          <tr>
          <td class="text-center"> <?php echo $key1+1;?> </td>
          <td class="text-left"> <?php echo $val[0];?></td>
          <td class="text-center"> <?php echo $val[1];?></td>
          <td class="text-center"> <?php echo $val[2];?></td>
          <td class="text-center"> <?php echo $val[3];?></td>
          <td class="text-center"> <?php echo $val[4];?></td>
          </tr>
     <?php 
         } 
      $i++; 
    } 
  ?>
  <tr class="rowfooter">
    <td colspan="4"></td>
    <td class="text-center"> Grand Total </td>
    <td class="text-center"> <?php echo $totalAmount;?></td>
  </tr> 
       
</tbody>
</table>


    <div class="ignore_page_brack">
        <table class="table">
            <tr>
                <td>Date </td>
                <td><?php
                    echo $tender['tender_due_date'];
                    ?></td>
                <td>Signature </td>
                <td>&nbsp;</td>
                <td>
        </table>
        <p>Please acknowledge receipt of documents, complete as listed, by signing the copy annexed.</p>

        <table class="table ">
            <tr>
                <td >Acknowledge by : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ </td>
            </tr>
            <tr>
                <td style="height: 100px">&nbsp;</td>
            </tr>

        </table>
        <table class="table ">
            <tr>
                <td>On behalf of  : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _<br>
                    (Company Stamp)</td>
                <td>Date: _ _ _ _ _ _ _ _ _ _ _</td>
            </tr>
        </table>
    </div>
  
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
           
