<?php

use yii\helpers\Html;

$this->title = 'Create Tender';
$this->params['breadcrumbs'][] = ['label' => 'Tender', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="project-create">
    <?= $this->render('_form', [
        'model' => $model,
        'tenderTypeValue'=>$tenderTypeValue,
        'tenderStatusValue'=>$tenderStatusValue,
        'assign_client'=>$assign_client,
        'buildingTypeValue'=>$buildingTypeValue,
        'companyList'=>$companyList,
        'accountTypeValue'=>$accountTypeValue
    ]) ?>

</div>
