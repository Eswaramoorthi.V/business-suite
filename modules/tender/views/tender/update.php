<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use app\models\Employee;
use app\models\Tender;
use app\models\Customer;




$model->tender_due_date = date('d-m-Y', strtotime($model['tender_due_date']));
$assign_employee = ArrayHelper::map(Employee::find()->select(['username'])->asArray()->all(),'username','username');
?>
<div class="tender-form form-vertical">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'tender_code', [
                'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Tender Code",'class' => "tender_code form-control"])->label(' Tender Code')
            ?>
             <h5 class="error-msg2 hide text-danger"> </h5>
        </div>
       
        <div class="col-md-4">
            <?=
            $form->field($model, 'tender_name', [
                'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => " Tender Name"])->label(' Tender Name')
            ?>

        </div>
        <div class="col-md-4">
            <!-- <?=
            $form->field($model, 'client_name', [
                'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Client Name"])->label('Client  Name')
            ?> -->
             <?=
            $form->field($model, 'client_name', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($assign_client, [
                'prompt' => 'Choose Client',
                'class' => 'select from control',
                'value'=>isset($tenderData['client_name']) ? $tenderData['client_name'] : '',
            ])->label("Client")
            ?>
        </div>

    </div>
    <div class="row">
        
        <div class="col-md-4">
            <?=
            $form->field($model, 'tender_type', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($tenderTypeValue, [
                'prompt' => 'Choose status',
                'class' => 'select from control',
                'value'=>isset($tenderData['tender_type']) ? $tenderData['tender_type'] : '',
            ])->label("Tender Type")
            ?>
        </div>
  
    <div class="col-md-4">
            <?=
            $form->field($model, 'tender_status', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($tenderStatusValue, [
                'prompt' => 'Choose status',
                'class' => 'select from control',
                isset($tenderData['tender_status']) ? $tenderData['tender_status'] : '',
            ])->label("Tender Status")
            ?>
        </div>
        <div class="col-md-4">
            
             <?=
            $form->field($model, 'building_type', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($buildingTypeValue, [
                'prompt' => 'Choose status',
                'class' => 'select from control',
                isset($tenderData['building_type']) ? $tenderData['building_type'] : '',
            ])->label("Building Type")
            ?>
        </div>
        </div>
    <div class="row">
        <div class="col-md-4">
                    <?=
                    $form->field($model, 'consultant', [
                        'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Enter consultant"])->label('Consultant')
                    ?>
                </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'total_area', [
                        'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Enter Total Area"])->label('Total Area')
                    ?>
                </div>
             <div class="col-md-4">
                    <?=
                    $form->field($model, 'reference', [
                        'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Enter Reference"])->label('Reference')
                    ?>
                </div>
                </div>
                <div class="row">
                <div class="col-md-4">
                <?=
            $form->field($model, 'tender_due_date', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
            ])->textInput([
                
                ])->label('Tender Due Date')?>
                </div>
           
                
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'country', [
                        'template' => "{label}\n
                <div class='col-md-12 col-xs-12'>
                        {input}\n
                        {hint}\n
                        {error}
                </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Country"])->label('Country')
                    ?>
                </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'amount', [
                        'template' => "{label}\n
                <div class='col-md-12 col-xs-12'>
                        {input}\n
                        {hint}\n
                        {error}
                </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Tender Amount"])->label(' Tender  Amount')
                    ?>
                </div>
            </div>
            <div class="row">
               
            
                <div class="col-md-4 hide">
           
           <?=
          $form->field($model, 'company_id', [
              'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
              {input}\n
              {hint}\n
              {error}
      </div>",
              'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
          ])->dropDownList($companyList, [
              'prompt' => 'Choose Company',
              'class' => 'select from control',
              'value'=> isset($tenderData['company_id']) ? $tenderData['company_id'] : '',
          ])->label("Company")
          ?>
      </div>
             <br> 
     <div class="row">
                <div class="form-group">
                    <label class="col-md-6 col-xs-12 control-label" for=""></label>
                    <div class="col-md-6 col-xs-12">
                        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

       
 <?= $this->registerJs("
    
    $('#tender-tender_due_date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
   

", View::POS_READY); ?>

<script >
$(function () {
    $('.tender_code').blur(function () {
        var tender_code = $(this).val();
        $.ajax({
            url: "./tender/tender/get-tender-code-check",
            data: {'tender_code': tender_code},
            type: "post",
            success: function (data) {
                $('#tender_code').val(data);
                if (data != '') {
                    $('.error-msg2').removeClass("hide");
                    $('.error-msg2').html("Tender Code Already Exists");
                }
                else{
                    $('.error-msg2').addClass("hide");
                }
            }
        });
    });
});
</script>