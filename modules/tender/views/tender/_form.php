<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Tender;

$getLastRecord=Tender::find()->orderBy(['id' => SORT_DESC])->asArray()->one();


if(!empty($getLastRecord)){
    $autoId=$getLastRecord['id']+1;
}else{
    $autoId=1;
}


?>
<div class="tender-form form-vertical">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'tender_code', [
                'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Tender Code",'class' => "tender_code form-control",'value'=>'T00'.$autoId])->label(' Tender Code')
            ?>
             <h5 class="error-msg2 hide text-danger"> </h5>
        </div>
       
        <div class="col-md-4">
            <?=
            $form->field($model, 'tender_name', [
                'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => " Tender Name"])->label(' Tender Name')
            ?>

        </div>
        <div class="col-md-4">
           
             <?=
        
            $form->field($model, 'client_name', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($assign_client, [
                'prompt' => 'Choose Client',
                'class' => 'select from control',
                // 'value'=>$model->project_type
            ])->label("Client")
            ?>
        </div>

    </div>
    <div class="row">
        
        <div class="col-md-4">
            <?=
            $form->field($model, 'tender_type', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($tenderTypeValue, [
                'prompt' => 'Choose status',
                'class' => 'select from control',
                'value'=>$model->tender_type
            ])->label("Tender Type")
            ?>
        </div>
  
    <div class="col-md-4">
            <?=
            $form->field($model, 'tender_status', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($tenderStatusValue, [
                'prompt' => 'Choose status',
                'class' => 'select from control',
                'value'=>$model->tender_status
            ])->label("Tender Status")
            ?>
        </div>
        <div class="col-md-4">
           
             <?=
            $form->field($model, 'building_type', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($buildingTypeValue, [
                'prompt' => 'Choose Buiding Type',
                'class' => 'select from control',
                'value'=>$model->building_type
            ])->label("Building Type")
            ?>
        </div>
        </div>
    <div class="row">
        <div class="col-md-4">
                    <?=
                    $form->field($model, 'consultant', [
                        'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Enter consultant"])->label('Consultant')
                    ?>
                </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'total_area', [
                        'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Enter Total Area"])->label('Total Area')
                    ?>
                </div>
             <div class="col-md-4">
                    <?=
                    $form->field($model, 'reference', [
                        'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Enter Reference"])->label('Reference')
                    ?>
                </div>
                </div>
                <div class="row">
                <div class="col-md-4">
                <?=
            $form->field($model, 'tender_due_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Tender Due Date')
            ?>
                </div>
           <div class="col-md-4">
                    <?=
                    $form->field($model, 'country', [
                        'template' => "{label}\n
                <div class='col-md-12 col-xs-12'>
                        {input}\n
                        {hint}\n
                        {error}
                </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Country"])->label('Country')
                    ?>
                </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'amount', [
                        'template' => "{label}\n
                <div class='col-md-12 col-xs-12'>
                        {input}\n
                        {hint}\n
                        {error}
                </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Tender Amount"])->label(' Tender  Amount')
                    ?>
                </div>
            </div>
            <div class="row">
               

                <div class="col-md-4 hide">
           
           <?=
          $form->field($model, 'company_id', [
              'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
              {input}\n
              {hint}\n
              {error}
      </div>",
              'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
          ])->dropDownList($companyList, [
              'prompt' => 'Choose Company',
              'class' => 'select from control',
              'value'=>$model->company_id
          ])->label("Company")
          ?>
      </div>
                </div>
                <br>
     <div class="row">
                <div class="form-group">
                    <label class="col-md-6 col-xs-12 control-label" for=""></label>
                    <div class="col-md-6 col-xs-12">
                        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                        <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

        <?= $this->registerJs("
    
        $('#tender-tender_due_date').datetimepicker({
            defaultDate: new Date(),
            format: 'DD-MM-YYYY',
          });
        
", View::POS_READY); ?>

<script >
$(function () {
    $('.tender_code').blur(function () {
        var tender_code = $(this).val();
        $.ajax({
            url: "./tender/tender/get-tender-code-check",
            data: {'tender_code': tender_code},
            type: "post",
            success: function (data) {
                $('#tender_code').val(data);
                if (data != '') {
                    $('.error-msg2').removeClass("hide");
                    $('.error-msg2').html("Tender Code Already Exists");
                }
                else{
                    $('.error-msg2').addClass("hide");
                }
            }
        });
    });
});
</script>