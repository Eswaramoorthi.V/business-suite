<?php

namespace app\modules\tender;

class tender extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\tender\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
