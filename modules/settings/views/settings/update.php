<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\date\DatePicker;
$activeList=[1=>'Yes',2=>'No'];
?>
<div class="company-form form-vertical">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-md-6">
    <?=
            $form->field($model, 'company_name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " Please enter company name"])->label('Company Name')
            ?>
    </div>
    <div class="col-md-6">
        <?=
            $form->field($model, 'email_id', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " Please enter email"])->label('Email')
            ?>
             <h5 class="error-msg2 hide text-danger"> </h5>
        </div>
        </div>
        <br>
        <div class="row">
        <div class="col-md-6">
            <?=
            $form->field($model, 'tel_no', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " Please enter Tel No"])->label('Tel No')
            ?>
        </div>
    
 
    <div class="col-md-6">
    <?=
            $form->field($model, 'fax_no', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " Please enter fax no"])->label('Fax no')
            ?>
    </div>
    </div>
    <div class="row">
    <div class="col-md-6">
    <?=
            $form->field($model, 'po_box_no', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " Please enter po box no"])->label('PO Box No')
            ?>
    </div>
  
  
    <div class="col-md-6">
        <?php $logopath="/pricingcalculator/web/lib/img/general/".$model->email_id."/logo"."/".$model->logo;?>
    <?php if(is_null($model->logo) || empty($model->logo)): ?>
        <?= $form->field($model, 'logo', [
          'template' => "{label}\n
          <div class='col-md-9'>
                {input}\n
                {hint}\n
                {error}
          </div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php else: ?>
  
 
        <?= $form->field($model, 'logo', [
          'template' => "{label}\n
          <div class='col-md-9 col-xs-9'>
                {input}\n
                <div class='file-preview'>
                  <strong> Current File </strong> : 
                    <div class='clearfix'></div>
                     <a base href='$logopath'target='_blank'> $model->logo</a>
                
                {hint}\n
                {error}
          </div></div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php endif; ?>
  
                </div>
                </div>
                <br>
                <div class="row">
                <div class="col-md-4">
     <?=
         $form->field($model, 'active', [
             'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
             'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
         ])->dropDownList($activeList, [
             'prompt' => 'Choose Active',
             'class' => 'project_name select from control',
            //  'value'=>$delivery->project_id
         ])->label("Active")
         ?>
 </div>
    </div>
    <br>
     <div class="row">
                <div class="form-group">
                    <label class="col-md-6 col-xs-12 control-label" for=""></label>
                    <div class="col-md-6 col-xs-12">
                        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                        <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

  