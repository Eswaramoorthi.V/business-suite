

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

$this->params['breadcrumbs'][] = 'General Settings';
?>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-wrench" aria-hidden="true"></i> General Settings
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
       <?= Html::button('Add', ['value' => Url::to('create'), 'class' => 'btn btn-success', 'id' => 'addModalButton']) ?>
    </div>
</div>
<?php
Modal::begin([
    'header' => '<h4>Add General Settings</h4>',
    'id' => 'addModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalAddContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update General Settings</h4>',
    'id' => 'updateModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalUpdateContent'><div class='loader'></div></div>";
Modal::end();
?>


<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            
            [
                'attribute' => 'company_name',
               'header' => '<div style="width:120px;">Company Name</div>',
                'value' => function($model, $key, $index) {
                   return !empty($model->company_name)?$model->company_name:'-';
                            
                },
                'vAlign' => 'middle',
                'format' => 'raw',
                //'width' => '150px',
                'noWrap' => true
            ],
            [
                'attribute' => 'email_id',
                'header' => '<div style="width:120px;">Email </div>',
                'value' => function($model, $key, $index) {
                    return !empty($model->email_id)?$model->email_id:'-';
                },
                'vAlign' => 'middle',
                'format' => 'raw',
               // 'width' => '150px',
                'noWrap' => true
          ],

         
            [
                
                'class' => '\kartik\grid\ActionColumn',
                'mergeHeader' => false,
                    'contentOptions' => ['style' => 'width:50px;text-align:center','class'=>'skip-export'],
                    'headerOptions' =>['class'=>'skip-export'],
                    'header'=>"Action",
                'template'=>$actionList,
                'dropdown' => true,
                'dropdownOptions'=>['class'=>'pull-right' ],
                'options' => ['style' => 'max-width:20px;'],
                'buttons' => [
                   
                    'update' => function($url) {
                        return Html::a('Update',   $url,['class' => 'action-btn dropdown-item updateModalButton ', 'title' => 'Update']
                        );
                    },
                    'deletes' => function($url) {
                       return Html::a('Delete',   $url,['class' => 'action-btn dropdown-item','data-method'=>'post', 'data-pjax'=>0,'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?']]
                       );
                   },
                    ]
                
                ],
        ],
    ]);
}
?>                                   
<?= $this->registerJs("
$(function(){
$('#addModalButton').click(function() {
    $('#addModal').modal('show')
    .find('#modalAddContent')
    .load($(this).attr('value'));
});
});
", View::POS_READY); ?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updateModalButton').click(function(e){
        e.preventDefault();      
        $('#updateModal').modal('show')
        .find('#modalUpdateContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>


<style>
a.action-btn.dropdown-item {
  display: block;
  width: 100%;
  padding: .25rem 1.5rem;
  clear: both;
  font-weight: normal;
  text-align: inherit;
  white-space: nowrap;
  background-color: transparent;
  border: 0;
  color: navy;
  font-size: small;
}
</style>