<?php

use yii\helpers\Html;

$this->title = 'Create Role';
$this->params['breadcrumbs'][] = ['label' => 'Role', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="project-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
