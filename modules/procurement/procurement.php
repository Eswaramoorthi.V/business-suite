<?php

namespace app\modules\procurement;

class procurement extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\procurement\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
