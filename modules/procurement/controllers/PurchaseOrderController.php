<?php

namespace app\modules\procurement\controllers;

use app\components\Procurement;
use app\models\PurchaseRequestMaster;
use app\models\PurchaseRequestDetails;
use app\models\PurchaseRequestMaterial;
use app\models\PurchaseDelivery;
use app\models\PurchaseDeliverySearch;
use app\models\PurchaseDeliveryStatus;


// purchaseDelivery

use Yii;
use app\models\Purchase;
use app\models\Search\PurchaseSearch;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\mpdf\Pdf;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\controllers\BaseController;
use app\modules\estimate\models\Settings;
use app\models\SettingsTree;
use app\models\EstimateDetails;
use app\models\EstimateMaster;
use app\models\MaterialMaster;
use app\models\MaterialUnitCost;
use app\models\LabourMaster;
use app\models\LabourUnitCost;
use app\modules\estimate\models\Equipment;
use app\models\ProjectEstimateMaster;
use app\models\ProjectEstimate;
use app\models\Tree;
use app\models\Project;
use app\components\MenuHelper;
use app\models\Customer;
use app\models\GeneralSettings;
use app\models\ProjectResource;
use app\models\PurchaseOrderDetails;
use app\models\Company;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class PurchaseOrderController extends BaseController
{
    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {

        $route = $this->route;

        $actionList = MenuHelper::getActionHelper($route);
        // pr($actionList);
        $visibleList = MenuHelper::getActionVisibleHelper($route);

        $searchModel = new PurchaseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize' => 50];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionList' => $actionList,
            'visibleList' => $visibleList
        ]);

    }

    public function actionIndex1()
    {

        $route = $this->route;

        $actionList = MenuHelper::getActionHelper($route);
        // pr($actionList);
        $visibleList = MenuHelper::getActionVisibleHelper($route);

        $searchModel = new PurchaseDeliverySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize' => 50];
        return $this->render('index1', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionList' => $actionList,
            'visibleList' => $visibleList
        ]);

    }


    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGetReferenceName()
    {
        $project_id = $_REQUEST['project_id'];
        $codeListData = PurchaseRequestMaster::find()->select(['reference_name', 'id'])->where(['project_id' => $project_id])->asArray()->all();

        $codeData = [];

        foreach ($codeListData as $value) {
            $eType = [];
            $eType['id'] = $value['id'];
            $eType['reference_name'] = $value['reference_name'];
            $codeData[] = $eType;
        }
        $codeList = ArrayHelper::map($codeData, 'id', 'reference_name');
// pr($codeList);
        echo "<option value=''>Select Area</option>";
        if (count($codeList) > 0) {
            foreach ($codeList as $id => $reference_name) {
                echo "<option value='$id'>$reference_name</option>";
            }
        }

    }

//     public function actionGetMaterialList()
//     {
//         $project_id = $_REQUEST['project_id'];
//         $projectTypeData = Project::find()->where(['id' => $project_id])->asArray()->one();

//         $projectAccountType=$projectTypeData['property_account_type'];


//         $getAcName = SettingsTree::find()->where(['id' => $projectAccountType])->asArray()->one();

//         $Atype = $getAcName['name'];

//         $accountType = strtolower($Atype);
//        if(($accountType =='cost center')) {


//         $itemType = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1]);


//      }else{
//             // pr("else");
//             $resource_type = '1';
//             $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->andwhere(['project_id' => $project_id])->groupBy('material_id')->asArray()->all();
//             //  pr($projectResourceMasterData);
//             $resourceNameListData = [];
//             foreach ($projectResourceMasterData as $value) {

//                 $cat = [];
//                 $itemType = Tree::find()->where(['id' => $value['material_id']]);

//             }


//         }
// // pr($itemType);
//         return $this->renderPartial('material-tree', [
//             'item'=>$itemType

//         ]);


//     }
    public function actionGetMaterialList()
    {

        $project_id = $_REQUEST['project_id'];
        $projectTypeData = Project::find()->where(['id' => $project_id])->asArray()->one();

        $projectAccountType = $projectTypeData['property_account_type'];


        $getAcName = SettingsTree::find()->where(['id' => $projectAccountType])->asArray()->one();

        $Atype = $getAcName['name'];

        $accountType = strtolower($Atype);
        if (($accountType == 'cost center')) {


            $itemType = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();

            $itemTypeList = [];
            foreach ($itemType as $value) {
                $itemTypeList[$value['id']] = $value['name'];
            }
            echo "<option value=''>Select</option>";
            foreach ($itemTypeList as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }
        } else {
            // pr("else");
            $resource_type = '1';
            $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->andwhere(['project_id' => $project_id])->groupBy('material_id')->asArray()->all();
            //  pr($projectResourceMasterData);
            $resourceNameListData = [];
            foreach ($projectResourceMasterData as $value) {

                $cat = [];
                $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();
                foreach ($getActivityName as $value1) {
                    $resourceNameListData[$value['material_id']] = $value1['name'];
                    // $cat['name'] = $value1['name'];
                }
            }

            echo "<option value=''>Select</option>";
            foreach ($resourceNameListData as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }
        }
    }


    public function actionGetMaterialListDrop()
    {

//        $project_id = $_REQUEST['project_id'];
        $count = $_REQUEST['counter'];


        $itemType = Tree::find();


        $mrvStatus = Procurement::getMRVStatus();
        $itemType = $itemType;
        return $this->renderPartial('material-list', [
            'item' => $itemType,
            'mrvStatus' => $mrvStatus,
            'count' => $count

        ]);


    }

    protected function findModelRequest($id)
    {
        if (($model = PurchaseRequestMaster::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionGetTotalMaterialData()
    {


        $request_ids = $_REQUEST['mrv'];
        $arrayData = [];
        if ($request_ids != '') {
            foreach ($request_ids as $value) {

                $purchaseMasterData = PurchaseRequestDetails::find()->with('purchaseOrderDetails')
                    ->where(['purchase_request_master_id' => $value])
                    ->asArray()->all();
                // pr($purchaseMasterData);
                foreach ($purchaseMasterData as $value1) {

                    $resData = [];
                    $resData['id'] = $value1['id'];
                    $resData['purchase_request_master_id'] = $value1['purchase_request_master_id'];
                    $resData['material_id'] = $value1['material_id'];
                    $resData['description'] = $value1['description'];

                    $resData['unit'] = $value1['unit'];
                    $resData['expected_date'] = $value1['expected_date'];
                    $resData['document'] = isset($value1['document']) ? $value1['document'] :'';
                    $resData['material_request_quantity'] = $value1['material_request_quantity'];
                    $resData['order_quantity'] = isset($value1['purchaseOrderDetails']['order_quantity']) ? $value1['purchaseOrderDetails']['order_quantity'] : 0;
                    $resData['mrv_status'] = isset($value1['purchaseOrderDetails']['mrv_status']) ? $value1['purchaseOrderDetails']['mrv_status'] : 1;
                    $resData['remaining_quantity'] = isset($value1['purchaseOrderDetails']['remaining_quantity']) ? $value1['purchaseOrderDetails']['remaining_quantity'] : $value1['material_total_quantity'];


                    $resData['material_total_quantity'] = $value1['material_total_quantity'];
                    if ($resData['mrv_status'] != Procurement::MRV_STATUS_CLOSE) {
                        $arrayData[] = $resData;
                    }
// pr($arrayData);
                }

            }


        } else {
            $arrayData = [];
        }

        $materailData = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();
        $materialNameListData = ArrayHelper::map($materailData, 'id', 'name');
        $mrvStatus = Procurement::getMRVStatus();
        $getUnitTypeList = SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
        $unitType = ArrayHelper::map($getUnitTypeList, 'id', 'name');


        return $this->renderPartial('request', [
                'purchaseMaster' => $arrayData,
                'arrayData' => $arrayData,
                'resourceNameListData' => $materialNameListData,
                'unitType' => $unitType,
                'mrvSatusData' => $mrvStatus


            ]
        );

    }


    public function actionUpdatedelivery($id)
    {

        $purchaseDeliveryData = PurchaseDelivery::find()->where(['purchase_id' => $id])->asArray()->all();

        $arrayData = [];
        foreach ($purchaseDeliveryData as $value1) {

            $resData = [];
            $resData['id'] = $value1['id'];
            $resData['purchase_id'] = $value1['purchase_id'];
            $resData['material_id'] = $value1['material_id'];
            $resData['description'] = $value1['description'];
            $resData['material_status'] = $value1['material_status'];
            $resData['delivery_date'] = $value1['delivery_date'];
            $resData['total_quantity'] = $value1['total_quantity'];
            $resData['purchased_quantity'] = $value1['purchased_quantity'];
            $resData['remaining_quantity'] = $value1['remaining_quantity'];
            $arrayData[] = $resData;
        }

        $resource_type = '1';
        $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->groupBy('material_id')->asArray()->all();
        $resourceNameListData = [];
        foreach ($projectResourceMasterData as $value) {

            $cat = [];
            $cat['id'] = $value['material_id'];
            $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();
            foreach ($getActivityName as $value1) {
                //    pr($value1);
                $cat['name'] = $value1['name'];
            }
            $resourceNameListData[] = $cat;
        }

        $materialStatusList = \app\models\SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 207])
            ->orderBy('root, lft')->asArray()->all();

        $materialStatusdata = [];
        $parent = "";
        foreach ($materialStatusList as $d) {
            // pr($d);
            if ($d['lvl'] == 1) {
                $materialStatusdata[$d['id']] = $d;
                $parent = $d['id'];
            } else {
                $materialStatusdata[$parent]['sublevel'][] = $d;
            }
        }

        return $this->renderPartial('updatedelivery', [
                'purchaseDelivery' => $arrayData,
                'arrayData' => $arrayData,
                'resourceNameListData' => $resourceNameListData,
                'materialStatusdata' => $materialStatusdata,


            ]
        );

    }

    public function actionDeliveryStatusUpdates()
    {
        pr($_REQUEST);
    }


    public function actionDelivery($id)
    {


        if ($_POST) {

            $postData = Yii::$app->request->post();

            // pr($postData);
            $deliveryData = $this->purchaseReformat($postData['PurchaseDelivery']);


            foreach ($deliveryData as $value) {

                $purchaseDetailsId = $value['podetails'];
                $projectId = $value['project_id'];
                $materialId = $value['material_id'];

                $purchaseDelivery = PurchaseDelivery::find()->where(['po_details_id' => $purchaseDetailsId])->one();
                if (empty($purchaseDelivery)) {
                    $purchaseDelivery = new PurchaseDelivery();
                }

                //   pr($value);
                $purchaseDelivery->project_id = $projectId;
                $purchaseDelivery->po_id = $value['poorder'];
                $purchaseDelivery->material_id = $value['material_id'];
                $purchaseDelivery->po_details_id = $value['podetails'];
                $purchaseDelivery->description = $value['description'];
                $purchaseDelivery->material_status = $value['material_status'];
                $purchaseDelivery->delivery_date = date('Y-m-d', strtotime($value['delivery_date']));
                $purchaseDelivery->order_quantity = $value['order_quantity'];
                //$purchaseDelivery->delivered_quantity = $value['delivered_quantity'];
                if (!$purchaseDelivery->save()) {
                    pr($purchaseDelivery->getErrors());
                }

                $deliveryId = $purchaseDelivery->id;
                $pdStatus = new PurchaseDeliveryStatus();
                $pdStatus->purchase_delivery_id = $deliveryId;
                $pdStatus->received_date = $value['delivery_date'];
                $pdStatus->received_quantity = $value['received_quantity'];

                if (!$pdStatus->save()) {
                    pr($pdStatus->getErrors());
                }

                $deliveredStatus = PurchaseDeliveryStatus::find()->where(['purchase_delivery_id' => $deliveryId]);
                $deliveredQuantity = $deliveredStatus->sum('received_quantity');
                $purchaseDelivery->delivered_quantity = $deliveredQuantity;
                $purchaseDelivery->save();

                $purchaseOderDetails = PurchaseOrderDetails::find()->where(['id' => $purchaseDelivery->po_details_id])->one();
                $purchaseOderDetails->purchase_delivery_id = $deliveryId;
                $purchaseOderDetails->save();
                $purchaseDelivery = PurchaseDelivery::find()->where(['project_id' => $projectId, 'material_id' => $materialId]);
                $projectMatrailsQuantiry = $purchaseDelivery->sum('delivered_quantity');
                $projectResource = ProjectResource::find()->where(['project_id' => $projectId, 'material_id' => $materialId])->one();

                $projectResource->purchased_quantity = $projectMatrailsQuantiry;
                $projectResource->save();

            }

            return $this->redirect(['index']);


        } else {

            $purchaseDelivery = new PurchaseDelivery();

            $POdata = PurchaseOrderDetails::find()->with('delivery')
                ->where(['purchase_id' => $id])
                ->asArray()->all();

            //    pr($POdata);
            $project_id = '';
            $arrayData = [];
            if (!empty($POdata)) {
                foreach ($POdata as $value1) {
//   pr($value1);
                    $resData = [];
                    $resData['id'] = $value1['id'];
                    $resData['project_id'] = $value1['project_id'];
                    $resData['material_id'] = $value1['material_id'];
                    $resData['description'] = $value1['description'];
                    $resData['mrv_status'] = $value1['mrv_status'];
                    $resData['delivery_date'] = $value1['expected_date'];
                    $resData['order_quantity'] = $value1['order_quantity'];
                    $resData['delivered_quantity'] = isset($value1['delivery']['delivered_quantity']) ? $value1['delivery']['delivered_quantity'] : 0;
                    $resData['po_id'] = $value1['purchase_id'];
                    $project_id = $value1['project_id'];
                    // $resData['remaining_quantity'] = $value1['total_quantity'] - $value1['purchased_quantity'];

                    // pr($resData);
                    $arrayData[] = $resData;
                }
// pr($arrayData);


            } else {

            }


            $resource_type = ('1');
            $materialNameListData = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();

            // $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->groupBy('material_id')->asArray()->all();
            $resourceNameListData = [];
            foreach ($materialNameListData as $value) {

                $cat = [];
                $cat['id'] = $value['id'];
                $cat['name'] = $value['name'];
                $resourceNameListData[] = $cat;
            }

            $purchaseDetail = Purchase::find()->where(['id' => $id])->one();
            $reference = $purchaseDetail['reference'];
            $date = $purchaseDetail['date_created'];
            // pr($purchaseDetail);
            $delivery_date = $purchaseDetail['due_date'];
            $package = $purchaseDetail['package'];
            $amount = $purchaseDetail['amount'];
            $recommendation = $purchaseDetail['recommendation'];
            $discount = $purchaseDetail['discount'];
            $supplier_ref = $purchaseDetail['supplier_ref'];
            $provision_amount = $purchaseDetail['provision_amount'];
            $po_number = $purchaseDetail['po_number'];
            $originator = $purchaseDetail['originator'];

            $purchaseData = Purchase::find()->where(['id' => $project_id])->one();
            $currency_id = $purchaseData['currency'];
            $currencyData = Company::find()->where(['id' => $currency_id])->asArray()->one();
            $currency = $currencyData['currency'];
            // $companyListData = Company::find()->where(['id'=>$supplierID])->asArray()->one();
            // $companyName = $companyListData['c'];
            // pr($currencyData);
            // foreach ($companyListData as $value) {
            //     $purchaseDetail = Purchase::find()->where(['id' => $value['company_name']])->one();

            //     $currency = $value['currency'];

            // }

            $supplierId = $purchaseData['supplier_name'];
            $supplierData = Customer::find()->where(['id' => $supplierId])->asArray()->one();
            $supplierName = $supplierData['company_name'];


            $projectListData = Project::find()->where(['id' => $project_id])->asArray()->one();
            $ProjectName = $projectListData['project_name'];
            $paymentTermsList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 197])
                ->orderBy('root, lft')->asArray()->all();

            foreach ($paymentTermsList as $value2) {
                //    pr($value2);
                $purchaseDetail = Purchase::find()->where(['id' => $value2['id']])->one();
                $paymentTerms = $value2['name'];
                //    pr($paymentTerms);
            }
            $statusList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 202])
                ->orderBy('root, lft')->asArray()->all();
            foreach ($statusList as $value3) {

                $purchaseDetail = Purchase::find()->where(['id' => $value3['id']])->one();
                $lpostatus = $value3['name'];
                //    pr($lpostatus);
            }
            $OrderType = [1 => 'Direct', 2 => 'MRV'];

            foreach ($OrderType as $value4) {
                $order_type = $value4;
            }

            $mrvStatusList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 214])
                ->orderBy('root, lft')->asArray()->all();

//             $MaterialData = [];

// foreach ($materialStatusList as $value) {
//     $eType =[];
//     $eType['id'] = $value['id'];
//     $eType['name'] = $value['name'];
//     $MaterialData[]=$eType;
// }
            $mrvSatusData = [];
            $parent = "";
            foreach ($mrvStatusList as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $mrvSatusData[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $mrvSatusData[$parent]['sublevel'][] = $d;
                }
            }


            return $this->render('delivery', [
                    'purchaseDelivery' => $purchaseDelivery,
                    'arrayData' => $arrayData,
                    'project_id' => $project_id,
                    'resourceNameListData' => $resourceNameListData,
                    'mrvSatusData' => $mrvSatusData,
                    // 'companyName' => $companyName,
                    'order_type' => $order_type,
                    'supplierName' => $supplierName,
                    'ProjectName' => $ProjectName,
                    'originator' => $originator,
                    'reference' => $reference,
                    'date' => $date,
                    'delivery_date' => $delivery_date,
                    'currency' => $currency,
                    'amount' => $amount,
                    'package' => $package,
                    'paymentTerms' => $paymentTerms,
                    'recommendation' => $recommendation,
                    'discount' => $discount,
                    'supplier_ref' => $supplier_ref,
                    'provision_amount' => $provision_amount,
                    'po_number' => $po_number,
                    'lpostatus' => $lpostatus,


                ]
            );


        }

    }

    public function actionCreate1()
    {

        $purchaseMaster = new Purchase();
        $purchase = new PurchaseOrderDetails();


        if ($purchaseMaster->load(Yii::$app->request->post())) {
            $postData = Yii::$app->request->post();
            pr($_REQUEST);

            $purchaseMaster->project_name = $postData['Purchase']['project_name'];
            $purchaseMaster->supplier_name = $postData['Purchase']['supplier_name'];
            $purchaseMaster->date_created = date('Y-m-d', strtotime($postData['Purchase']['date_created']));
            $purchaseMaster->due_date = date('Y-m-d', strtotime($postData['Purchase']['due_date']));
            $purchaseMaster->sub_total = $postData['Purchase']['sub_total'];
            $purchaseMaster->total_amount = $postData['Purchase']['total_amount'];
            $purchaseMaster->vat = $postData['Purchase']['vat'];
            $purchaseMaster->company_name = 1;
            $purchaseMaster->originator = $postData['Purchase']['originator'];
            $purchaseMaster->reference = $postData['Purchase']['reference'];
            $purchaseMaster->amount = $postData['Purchase']['amount'];
            $purchaseMaster->currency = $postData['Purchase']['currency'];
            $purchaseMaster->package = $postData['Purchase']['package'];
            $purchaseMaster->payment_terms = $postData['Purchase']['payment_terms'];
            $purchaseMaster->discount = $postData['Purchase']['discount'];
            $purchaseMaster->recommendation = $postData['Purchase']['recommendation'];
            $purchaseMaster->supplier_ref = $postData['Purchase']['supplier_ref'];
            $purchaseMaster->provision_amount = $postData['Purchase']['provision_amount'];
            $purchaseMaster->po_number = $postData['Purchase']['po_number'];
            $purchaseMaster->lpo_status = $postData['Purchase']['lpo_status'];
            $purchaseMaster->order_type = $postData['Purchase']['order_type'];

            if (!$purchaseMaster->save()) {
                pr($purchaseMaster->getErrors());
            }
            $requestList = isset($_REQUEST['requestList']) ? ($_REQUEST['requestList']) : '';

            $projectNameID = $postData['Purchase']['project_name'];
            $purchaseRequestId = $purchaseMaster->id;

            $purchaseContainer = $this->purchaseReformat($postData['PurchaseMaster']);
            // pr($purchaseContainer);

            $projectNameID = $postData['Purchase']['project_name'];

            $purchaseId = $purchaseMaster->id;
            if (!empty($purchaseContainer)) {
                foreach ($purchaseContainer as $purchaseMasterData) {

                    $purchase = new PurchaseOrderDetails();
                    $purchase->purchase_id = $purchaseId;
                    $purchase->material_id = $purchaseMasterData['material_id'];
                    $purchase->description = $purchaseMasterData['description'];

                    $purchase->unit = isset($purchaseMasterData['unit']) ? $purchaseMasterData['unit'] : "";
                    $purchase->expected_date = date('Y-m-d', strtotime($purchaseMasterData['expected_date']));
                    $purchase->order_quantity = $purchaseMasterData['order_quantity'];
                    $purchase->remaining_quantity = isset($purchaseMasterData['remaining_quantity']) ? $purchaseMasterData['remaining_quantity'] : "";
                    $purchase->request_id = isset($purchaseMasterData['requestid']) ? $purchaseMasterData['requestid'] : "";
                    $purchase->price = $purchaseMasterData['price'];
                    $purchase->total_amount = $purchaseMasterData['total_amount'];
                    $purchase->project_id = $projectNameID;
                    if ($purchase->remaining_quantity == $purchaseMasterData['order_quantity']) {
                        $purchase->mrv_status = Procurement::MRV_STATUS_OPEN;
                    } else if ($purchase->remaining_quantity == 0) {
                        $purchase->mrv_status = Procurement::MRV_STATUS_CLOSE;
                    } else {
                        $purchase->mrv_status = Procurement::MRV_STATUS_PART;
                    }
                    if (!$purchase->save()) {
                        pr($purchase->getErrors());

                    } else {
                        $purchasedetailsId = $purchase->id;
                        $request = PurchaseRequestDetails::find()
                            ->where(['id' => $purchase->request_id])->one();
                        if (!empty($request)) {
                            $request->request_order_id = $purchasedetailsId;
                            //pr($request);
                            $request->save();
                        }

                    }


                }


            }
            // }


            return $this->redirect(['index']);
        } else {
            $projectListData = Project::find()->select(['project_name', 'id', 'project_code'])->asArray()->all();
            $projectList = ArrayHelper::map($projectListData, 'id', 'project_code');
            $currencylist = Procurement::getCurency();

            $typeOneData = Customer::find()->where(['customer_type' => 3])->asArray()->all();
            $supplierData = ArrayHelper::map($typeOneData, 'id', 'company_name');

            $paymentTermsList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 197])
                ->orderBy('root, lft')->asArray()->all();

            $termsTypeValue = ArrayHelper::map($paymentTermsList, 'id', 'name');
            $resource_type = '1';
            $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->groupBy('material_id')->asArray()->all();
            $resourceNameListData = [];
            foreach ($projectResourceMasterData as $value) {
                $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->one();
                $cat = [];
                $cat['id'] = $value['material_id'];
                $cat['name'] = $getActivityName['name'];
                $resourceNameListData[] = $cat;
            }
            $getUnitTypeList = SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
            $unitType = ArrayHelper::map($getUnitTypeList, 'id', 'name');

            $purchaseMaster->currency = Procurement::DEFAULT_CURRENCY;
            $purchaseMaster->lpo_status = Procurement::DOCUMENT_DRAFT;


            $LpoStatus = Procurement::getDocumentStatus();
            $mrvStatus = Procurement::getMRVStatus();
            return $this->render('_form2', ['purchaseMaster' => $purchaseMaster,
                    'purchase' => $purchase,
                    'projectList' => $projectList,
                    'supplierData' => $supplierData,
                    'currencylist' => $currencylist,
                    'termsTypeValue' => $termsTypeValue,
                    'LpoStatus' => $LpoStatus,
                    'mrvStatus' => $mrvStatus,
                    'unitType' => $unitType,
                    'resourceNameListData' => $resourceNameListData

                ]
            );

        }
    }

    public function actionCreate()
    {

        $purchaseMaster = new Purchase();
        $purchase = new PurchaseOrderDetails();


        if ($purchaseMaster->load(Yii::$app->request->post())) {
            $postData = Yii::$app->request->post();
            //    pr($postData);

            $purchaseMaster->project_name = $postData['project_id'];
            $purchaseMaster->supplier_name = $postData['supplier_name'];
            $purchaseMaster->date_created = date('Y-m-d', strtotime($postData['Purchase']['date_created']));
            $purchaseMaster->due_date = date('Y-m-d', strtotime($postData['Purchase']['due_date']));
            $purchaseMaster->sub_total = $postData['Purchase']['sub_total'];
            $purchaseMaster->total_amount = $postData['Purchase']['total_amount'];
            $purchaseMaster->vat = $postData['Purchase']['vat'];
            $purchaseMaster->company_name = 1;
            $purchaseMaster->originator = $postData['Purchase']['originator'];
            $purchaseMaster->reference = $postData['Purchase']['reference'];
            $purchaseMaster->amount = $postData['Purchase']['amount'];
            $purchaseMaster->currency = $postData['currency'];
            $purchaseMaster->package = $postData['Purchase']['package'];
            $purchaseMaster->payment_terms = $postData['payment_terms'];
            $purchaseMaster->discount = $postData['Purchase']['discount'];
            $purchaseMaster->recommendation = $postData['Purchase']['recommendation'];
            $purchaseMaster->supplier_ref = $postData['Purchase']['supplier_ref'];
            $purchaseMaster->provision_amount = $postData['Purchase']['provision_amount'];
            $purchaseMaster->po_number = $postData['Purchase']['po_number'];
            $purchaseMaster->lpo_status = $postData['lpo_status'];
            $purchaseMaster->order_type = $postData['order_type'];

            if (!$purchaseMaster->save()) {
                pr($purchaseMaster->getErrors());
            }
            $requestList = isset($_REQUEST['requestList']) ? ($_REQUEST['requestList']) : '';

            $projectNameID = $postData['project_id'];
            $purchaseRequestId = $purchaseMaster->id;

            $purchaseContainer = $this->purchaseReformat($postData['PurchaseMaster']);
            // pr($purchaseContainer);

            $projectNameID = $postData['project_id'];

            $purchaseId = $purchaseMaster->id;
            if (!empty($purchaseContainer)) {
                foreach ($purchaseContainer as $purchaseMasterData) {

                    $purchase = new PurchaseOrderDetails();
                    $purchase->purchase_id = $purchaseId;
                    $purchase->material_id = isset($purchaseMasterData['material_id']) ? $purchaseMasterData['material_id'] :'';
                    $purchase->description = $purchaseMasterData['description'];

                    $purchase->unit = isset($purchaseMasterData['unit']) ? $purchaseMasterData['unit'] : "";
                    $purchase->expected_date = date('Y-m-d', strtotime($purchaseMasterData['expected_date']));
                    $purchase->order_quantity = $purchaseMasterData['order_quantity'];
                    $purchase->remaining_quantity = isset($purchaseMasterData['remaining_quantity']) ? $purchaseMasterData['remaining_quantity'] : "";
                    $purchase->request_id = isset($purchaseMasterData['requestid']) ? $purchaseMasterData['requestid'] : "";
                    $purchase->price = $purchaseMasterData['price'];
                    $purchase->total_amount = $purchaseMasterData['total_amount'];
                    $purchase->project_id = $projectNameID;
                    // pr($_FILES);
                    if ($purchase->remaining_quantity == $purchaseMasterData['order_quantity']) {
                        $purchase->mrv_status = Procurement::MRV_STATUS_OPEN;
                    } else if ($purchase->remaining_quantity == 0) {
                        $purchase->mrv_status = Procurement::MRV_STATUS_CLOSE;
                    } else {
                        $purchase->mrv_status = Procurement::MRV_STATUS_PART;
                    }
                    // pr($_FILES);
                    $check=array_filter($_FILES);
                   
                    $emptyCheck=count($check);
                  
                    if ($emptyCheck >0 ) {
                        for ($i = 0; $i < count($_FILES['PurchaseMaster']['tmp_name']['document']); $i++) {
                            $timeVal = time();
                            $file = $timeVal . '-' . $_FILES['PurchaseMaster']['name']['document'][$i];
            
                            $baseurl = Yii::getAlias('@app');
                            // pr($baseurl);
                            // $folder = '../web/observationimages/';
                            // $minifolder = '../web/observationimages/mini/';
                            $folder = $baseurl . '/web/purchaseorder/';
                            // pr($folder);
                            $minifolder = $baseurl . '/web/purchaseorder/attachment/';
                            $Savedfile_name = $file;
                            if (file_exists($folder)) {
                                // pr("idf");
                                // mkdir($baseurl . 'web/mrv/', 0777, true);
                                // mkdir($baseurl . 'web/mrv/attachment/', 0777, true);
                                $path = $minifolder . $Savedfile_name;
            
                            } else {
                                // pr("else");
                                mkdir($baseurl . '/web/purchaseorder/', 0777, true);
                                mkdir($baseurl . '/web/purchaseorder/attachment/', 0777, true);
                                $path = $minifolder . $Savedfile_name;
            
            
                            }
                            move_uploaded_file($_FILES["PurchaseMaster"]["tmp_name"]['document'][$i], $path);
            
                            $filesName[] = $Savedfile_name;

                            foreach($filesName as $value ){
                                $purchase->document = $value;
                            }
                           
                        }
            
                    }else{
                        $purchase->document ='';
                    }

                    if (!$purchase->save()) {
                        pr($purchase->getErrors());

                    } else {
                        $purchasedetailsId = $purchase->id;
                        $request = PurchaseRequestDetails::find()
                            ->where(['id' => $purchase->request_id])->one();
                        if (!empty($request)) {
                            $request->request_order_id = $purchasedetailsId;
                            //pr($request);
                            $request->save();
                        }

                    }


                }


            }
            // }


            return $this->redirect(['index']);
        } else {
            $projectListData = Project::find()->select(['project_name', 'id', 'project_code'])->asArray()->all();
            $projectList = ArrayHelper::map($projectListData, 'id', 'project_code');
            $currencylist = Procurement::getCurency();

            $typeOneData = Customer::find()->where(['customer_type' => 3])->asArray()->all();
            $supplierData = ArrayHelper::map($typeOneData, 'id', 'company_name');

            $paymentTermsList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 197])
                ->orderBy('root, lft')->asArray()->all();

            $termsTypeValue = ArrayHelper::map($paymentTermsList, 'id', 'name');
            $resource_type = '1';
            $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->groupBy('material_id')->asArray()->all();
            $resourceNameListData = [];
            foreach ($projectResourceMasterData as $value) {
                $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->one();
                $cat = [];
                $cat['id'] = $value['material_id'];
                $cat['name'] = $getActivityName['name'];
                $resourceNameListData[] = $cat;
            }
            $getUnitTypeList = SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
            $unitType = ArrayHelper::map($getUnitTypeList, 'id', 'name');
            $materialList = ArrayHelper::map($resourceNameListData, 'id', 'name');
            // pr($materialList);
            $purchaseMaster->currency = Procurement::DEFAULT_CURRENCY;
            $purchaseMaster->lpo_status = Procurement::DOCUMENT_DRAFT;


            $LpoStatus = Procurement::getDocumentStatus();
            $mrvStatus = Procurement::getMRVStatus();
            // pr($resourceNameListData);
            return $this->render('create', ['purchaseMaster' => $purchaseMaster,
                    'purchase' => $purchase,
                    'projectList' => $projectList,
                    'supplierData' => $supplierData,
                    'currencylist' => $currencylist,
                    'termsTypeValue' => $termsTypeValue,
                    'LpoStatus' => $LpoStatus,
                    'mrvStatus' => $mrvStatus,
                    'unitType' => $unitType,
                    'resourceNameListData' => $resourceNameListData,
                    'materialList' => $materialList

                ]
            );

        }
    }

    protected function purchaseReformat($purchaseDatas)
    {

        $purchaseKeysvalue = array_keys($purchaseDatas);
        // pr($purchaseKeysvalue);
        $purchaseData = array_values($purchaseDatas);
        $purchaseTotalCount = count(array_values($purchaseData[0]));

        $purchaseContainer = [];
        for ($i = 0; $i < $purchaseTotalCount; $i++) {
            $purchaseTemp = [];
            foreach ($purchaseKeysvalue as $key) {
                $purchaseTemp[$key] = isset($purchaseDatas[$key][$i]) ? $purchaseDatas[$key][$i] : '';
            }
            $purchaseContainer[] = $purchaseTemp;
        }
        return $purchaseContainer;
    }


    public function actionUpdate($id)
    {


        $order_type = Yii::$app->request->get('order_type');
        $purchaseMaster = $this->findModel($id);

        if ($order_type == 1) {

            $originalPurchaseIds = [];
            $projectId = '';
            foreach ($purchaseMaster->purchase as $purchaseData) {

                $originalPurchaseIds[$purchaseData['id']] = $purchaseData['id'];

                $projectId = $purchaseData['project_id'];
                $request_id = $purchaseData['request_id'];
            }


            $refernceData = PurchaseOrderDetails::find()->select('request_id')->where(['request_id' => $id])->andWhere(['project_id' => $projectId])->asArray()->all();
            // pr($refernceData);
            $requestedName = [];
            foreach ($refernceData as $value) {
                $getActivityName = PurchaseRequestMaster::find()->where(['id' => $value['request_id']])->asArray()->all();
                foreach ($getActivityName as $value1) {
                    $requestedName[$value['request_id']] = $value1['reference_name'];
                }

            }

            $codeListData = PurchaseRequestMaster::find()->select(['reference_name', 'id'])->where(['project_id' => $projectId])->asArray()->all();
// pr($request_id);
            $codeData = [];
            $refernceData = PurchaseOrderDetails::find()->select('request_id')->where(['request_id' => $id])->andWhere(['project_id' => $projectId])->asArray()->all();

            foreach ($codeListData as $value) {
                foreach ($refernceData as $value1) {
                    $eType = [];
                    $eType['id'] = $value1['request_id'];
                    $eType['reference_name'] = $value['reference_name'];
                    $codeData[] = $eType;
                }

            }
            $codeList = ArrayHelper::map($codeData, 'id', 'reference_name');


            // $resource_type = '1';
            // $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->andwhere(['project_id' => $projectId])->groupBy('material_id')->asArray()->all();
            // $resourceNameListData = [];
            // foreach ($projectResourceMasterData as $value) {

            //     $cat = [];
            //     $cat['id'] = $value['material_id'];
            //     $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();
            //     foreach ($getActivityName as $value1) {
            //         //    pr($value1);
            //         $cat['name'] = $value1['name'];
            //     }
            //     $resourceNameListData[] = $cat;
            // }
            $resource_type = ('1');
            $materialNameListData = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();

            // $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->groupBy('material_id')->asArray()->all();
            $resourceNameListData = [];
            foreach ($materialNameListData as $value) {

                $cat = [];
                $cat['id'] = $value['id'];
                $cat['name'] = $value['name'];
                $resourceNameListData[] = $cat;
            }

            if ($purchaseMaster->load(Yii::$app->request->post())) {

                $request = Yii::$app->request->post();
             // pr($request);
                $PurchaseData = $request['Purchase'];

                $purchaseMaster->project_name = $request['project_name'];
                $purchaseMaster->supplier_name = $request['supplier_name'];

                $purchaseMaster->sub_total = $request['Purchase']['sub_total'];
                $purchaseMaster->vat = $request['Purchase']['vat'];
                $purchaseMaster->total_amount = $request['Purchase']['total_amount'];
                $purchaseMaster->notes = $request['Purchase']['notes'];
                $purchaseMaster->date_created = date('Y-m-d', strtotime($request['Purchase']['date_created']));
                $purchaseMaster->due_date = date('Y-m-d', strtotime($request['Purchase']['due_date']));
                $purchaseMaster->company_name = $request['Purchase']['company_name'];
                $purchaseMaster->originator = $request['Purchase']['originator'];
                $purchaseMaster->reference = $request['Purchase']['reference'];
                $purchaseMaster->amount = $request['Purchase']['amount'];
                $purchaseMaster->currency = $request['currency'];
                $purchaseMaster->package = $request['Purchase']['package'];
                $purchaseMaster->payment_terms = $request['payment_terms'];
                $purchaseMaster->discount = $request['Purchase']['discount'];
                $purchaseMaster->recommendation = $request['Purchase']['recommendation'];
                $purchaseMaster->supplier_ref = $request['Purchase']['supplier_ref'];
                $purchaseMaster->provision_amount = $request['Purchase']['provision_amount'];
                $purchaseMaster->po_number = $request['Purchase']['po_number'];
                $purchaseMaster->lpo_status = $request['lpo_status'];

                if (!$purchaseMaster->save()) {
                    pr($purchaseMaster->getErrors());
                }
                $projectNameID = $request['project_name'];
                // pr($projectNameID);

                // print_r($request);
                $currentPurchaseIds = (array_flip($request['PurchaseMaster']['id']));
                // print_r($currentPurchaseIds);
                // die();
                $deletedIds = [];
                foreach ($originalPurchaseIds as $ids) {
                    if (!array_key_exists($ids, $currentPurchaseIds)) {
                        // echo "first if ";
                        // print_r($ids);
                        // pr('ggvjh');
                        $deletedIds[] = $ids;
                    }
                    if ($deletedIds) {
                        // echo "seconfdif ";
                        // print_r($deletedIds);
                        PurchaseOrderDetails::deleteAll(['id' => $deletedIds]);
                    }
                }
                $purchaseContainer = $this->purchaseReformat($request['PurchaseMaster']);
                // print_r($purchaseContainer);
                foreach ($purchaseContainer as $purchaseData) {

                    if ($purchaseData['id'] == '') {
                        $purchase = new PurchaseOrderDetails();
                    } else {
                        $purchase = PurchaseOrderDetails::findOne($purchaseData['id']);
                    }
                    // pr($purchase);
                    //pr($purchaseData);
                    $purchase->purchase_id = $id;
                    $purchase->material_id = $purchaseData['material_id'];
                    $purchase->description = $purchaseData['description'];
                    $purchase->mrv_status = $purchaseData['mrv_status'];
                    $purchase->expected_date = date('Y-m-d', strtotime($purchaseData['expected_date']));
                    $purchase->quantity = $purchaseData['total_quantity'];
                    $purchase->order_quantity = $purchaseData['order_quantity'];
                    // $purchase->unit = $purchaseData['unit'];
                    $purchase->price = $purchaseData['price'];
                    $purchase->total_amount = $purchaseData['total_amount'];
                    $purchase->project_id = $projectNameID;
                    //pr($purchase);
                    $purchase->document = isset($purchaseData['document']) ? $purchaseData['document'] :'';
                   
                    $check=array_filter($_FILES);
                    // pr($check);
                    $emptyCheck=count($check);
                  
                    if ( $emptyCheck >0) {
                        for ($i = 0; $i < count($_FILES['PurchaseMaster']['tmp_name']['document']); $i++) {
                            $timeVal = time();
                            $file = $timeVal . '-' . $_FILES['PurchaseMaster']['name']['document'][$i];
            
                            $baseurl = Yii::getAlias('@app');
                           
                            $folder = $baseurl . '/web/purchaseorder/';
                         
                            $minifolder = $baseurl . '/web/purchaseorder/attachment/';
                            $Savedfile_name = $file;
                            if (file_exists($folder)) {
                                // pr("idf");
                                // mkdir($baseurl . 'web/mrv/', 0777, true);
                                // mkdir($baseurl . 'web/mrv/attachment/', 0777, true);
                                $path = $minifolder . $Savedfile_name;
            
                            } else {
                                // pr("else");
                                mkdir($baseurl . '/web/purchaseorder/', 0777, true);
                                mkdir($baseurl . '/web/purchaseorder/attachment/', 0777, true);
                                $path = $minifolder . $Savedfile_name;
            
            
                            }
                            move_uploaded_file($_FILES["PurchaseMaster"]["tmp_name"]['document'][$i], $path);
            
                            $filesName[] = $Savedfile_name;

                            foreach($filesName as $value ){
                                $purchase->document = $value;
                            }
                           
                        }
            
                    }
                }
                


                if (!$purchase->save()) {
                    pr($purchase->getErrors());
                }
                // pr($invoiceContainer);

                return $this->redirect(['index']);
            } else {
                // $companyId=$_SESSION['default_company_id'];
                // $projectListData = Project::find()->select(['project_name', 'id'])->where(['company_id'=>$companyId])->asArray()->all();

                //     $assign_project = [];

                //     foreach ($projectListData as $value) {
                //         $eType = [];
                //         $eType['id'] = $value['id'];
                //         $eType['project_name'] = $value['project_name'];
                //         $assign_project[] = $eType;
                //     }
                //     $projectList = ArrayHelper::map($assign_project, 'id', 'project_name');
                $projectListData = Project::find()->select(['project_name', 'id', 'project_code'])->asArray()->all();
                $projectList = ArrayHelper::map($projectListData, 'id', 'project_code');

                $projectData = Project::find()->where(['id' => $purchaseMaster['project_name']])->one();
                $typeOneData = Customer::find()->where(['customer_type' => 3])->asArray()->all();
                $assign_ClientData = [];
                foreach ($typeOneData as $value) {

                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['company_name'] = $value['company_name'];
                    $assign_ClientData[] = $eType;
                }
                //  pr($assign_ClientData);
                $supplierData = ArrayHelper::map($assign_ClientData, 'id', 'company_name');
                $companyListData = Company::find()->select(['company_name', 'currency', 'id'])->asArray()->all();

                $companyList = [];

                foreach ($companyListData as $value) {
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['company_name'] = $value['company_name'];
                    $companyList[] = $eType;
                }
                $companylist = ArrayHelper::map($companyList, 'id', 'company_name');


                $currencyList = [];

                foreach ($companyListData as $value) {
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['currency'] = $value['currency'];
                    $currencyList[] = $eType;
                }
                $currencylist = ArrayHelper::map($currencyList, 'id', 'currency');
                $paymentTermsList = \app\models\SettingsTree::find()
                    ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                    ->where(['!=', 'lvl', 0])->andWhere(['root' => 197])
                    ->orderBy('root, lft')->asArray()->all();

                $termsType = [];
                $parent = "";
                foreach ($paymentTermsList as $d) {
                    // pr($d);
                    if ($d['lvl'] == 1) {
                        $termsType[$d['id']] = $d;
                        $parent = $d['id'];
                    } else {
                        $termsType[$parent]['sublevel'][] = $d;
                    }
                }

                $termsTypeValue = ArrayHelper::map($termsType, 'id', 'name');
                $statusList = \app\models\SettingsTree::find()
                    ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                    ->where(['!=', 'lvl', 0])->andWhere(['root' => 202])
                    ->orderBy('root, lft')->asArray()->all();

                $statusLPO = [];
                $parent = "";
                foreach ($statusList as $d) {
                    // pr($d);
                    if ($d['lvl'] == 1) {
                        $statusLPO[$d['id']] = $d;
                        $parent = $d['id'];
                    } else {
                        $statusLPO[$parent]['sublevel'][] = $d;
                    }
                }

                $LpoStatus = ArrayHelper::map($statusLPO, 'id', 'name');
                $materialStatusList = \app\models\SettingsTree::find()
                    ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                    ->where(['!=', 'lvl', 0])->andWhere(['root' => 207])
                    ->orderBy('root, lft')->asArray()->all();

                $materialStatusdata = [];
                $parent = "";
                foreach ($materialStatusList as $d) {
                    // pr($d);
                    if ($d['lvl'] == 1) {
                        $materialStatusdata[$d['id']] = $d;
                        $parent = $d['id'];
                    } else {
                        $materialStatusdata[$parent]['sublevel'][] = $d;
                    }
                }

                $materialStatus = ArrayHelper::map($materialStatusdata, 'id', 'name');

                $projectData = PurchaseOrderDetails::find()->where(['purchase_id' => $id])->asArray()->all();
                // pr($purchaseMaster);
                $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
                $unitType = [];
                foreach ($getUnitTypeList as $value) {
                    $eTypeList = [];
                    $eTypeList['id'] = $value['id'];
                    $eTypeList['name'] = $value['name'];
                    $unitType[] = $eTypeList;
                }

                $mrvStatusList = \app\models\SettingsTree::find()
                    ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                    ->where(['!=', 'lvl', 0])->andWhere(['root' => 214])
                    ->orderBy('root, lft')->asArray()->all();

                $mrvStatus = [];
                $parent = "";
                foreach ($mrvStatusList as $d) {
                    // pr($d);
                    if ($d['lvl'] == 1) {
                        $mrvStatus[$d['id']] = $d;
                        $parent = $d['id'];
                    } else {
                        $mrvStatus[$parent]['sublevel'][] = $d;
                    }
                }

                return $this->render('update', [

                        'purchaseMaster' => $purchaseMaster,
                        'projectList' => $projectList,
                        'projectData' => $projectData,
                        'supplierData' => $supplierData,
                        'resourceNameListData' => $resourceNameListData,
                        'currencylist' => $currencylist,
                        'companylist' => $companylist,
                        'termsTypeValue' => $termsTypeValue,
                        'LpoStatus' => $LpoStatus,
                        'materialStatus' => $materialStatus,
                        'materialStatusdata' => $materialStatusdata,
                        'requestedName' => $requestedName,
                        'codeList' => $codeList,
                        'unitType' => $unitType,
                        'mrvStatus' => $mrvStatus
                    ]
                );

            }
        } else if ($order_type == 2) {

            $originalPurchaseIds = [];
            $projectId = '';
            $reference_id = '';
            $request_id = '';
// pr($purchaseMaster);
            foreach ($purchaseMaster->purchase as $purchaseData) {
                //  pr($purchaseData);

                $originalPurchaseIds[$purchaseData['id']] = $purchaseData['id'];
                // $originalPurchaseIds[$purchaseData['id']] = $purchaseData['purchase_id'];
                $projectId = $purchaseData['project_id'];
                $request_id = $purchaseData['request_id'];
                // $reference_id = $purchaseData['reference_id'];


            }


            $refernceData = Purchase::find()->where(['project_name' => $projectId, 'id' => $id])->asArray()->all();
            // pr($)
            foreach ($refernceData as $PoNumber) {
                $requestedName[$PoNumber['id']] = $PoNumber['id'];
            }
            // $requestedName = ArrayHelper::getColumn($refernceData, 'id');
            //    pr($requestedName);
            $codeListData = PurchaseRequestMaster::find()->select(['reference_name', 'id'])->where(['project_id' => $projectId])->asArray()->all();

            $codeData = [];

            // pr($refernceData);
            foreach ($codeListData as $value) {

                $eType = [];
                $eType['id'] = $value['id'];
                $eType['reference_name'] = $value['reference_name'];
                $codeData[] = $eType;
            }

            $codeList = ArrayHelper::map($codeData, 'id', 'reference_name');
//  pr($codeList);
            $resource_type = '1';
            $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->andwhere(['project_id' => $projectId])->groupBy('material_id')->asArray()->all();

            $resourceNameListData = [];
            foreach ($projectResourceMasterData as $value) {

                $cat = [];
                $cat['id'] = $value['material_id'];
                $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();
                foreach ($getActivityName as $value1) {
                    //    pr($value1);
                    $cat['name'] = $value1['name'];
                }
                $resourceNameListData[] = $cat;
            }


            if ($purchaseMaster->load(Yii::$app->request->post())) {

                $request = Yii::$app->request->post();
                //    pr($request);
                $PurchaseData = $request['Purchase'];

                $purchaseMaster->project_name = $request['Purchase']['project_name'];
                $purchaseMaster->supplier_name = $request['Purchase']['supplier_name'];

                $purchaseMaster->sub_total = $request['Purchase']['sub_total'];
                $purchaseMaster->vat = $request['Purchase']['vat'];
                $purchaseMaster->total_amount = $request['Purchase']['total_amount'];
                $purchaseMaster->notes = $request['Purchase']['notes'];
                $purchaseMaster->date_created = date('Y-m-d', strtotime($request['Purchase']['date_created']));
                $purchaseMaster->due_date = date('Y-m-d', strtotime($request['Purchase']['due_date']));
                $purchaseMaster->company_name = $request['Purchase']['company_name'];
                $purchaseMaster->originator = $request['Purchase']['originator'];
                $purchaseMaster->reference = $request['Purchase']['reference'];
                $purchaseMaster->amount = $request['Purchase']['amount'];
                $purchaseMaster->currency = $request['Purchase']['currency'];
                $purchaseMaster->package = $request['Purchase']['package'];
                $purchaseMaster->payment_terms = $request['Purchase']['payment_terms'];
                $purchaseMaster->discount = $request['Purchase']['discount'];
                $purchaseMaster->recommendation = $request['Purchase']['recommendation'];
                $purchaseMaster->supplier_ref = $request['Purchase']['supplier_ref'];
                $purchaseMaster->provision_amount = $request['Purchase']['provision_amount'];
                $purchaseMaster->po_number = $request['Purchase']['po_number'];
                $purchaseMaster->lpo_status = $request['Purchase']['lpo_status'];
                $purchaseMaster->order_type = $request['Purchase']['order_type'];

                if (!$purchaseMaster->save()) {
                    pr($purchaseMaster->getErrors());
                }

                $projectNameID = $request['Purchase']['project_name'];
// pr($request);
                $currentPurchaseIds = (array_flip($request['PurchaseMaster']['id']));

                $deletedIds = [];
                foreach ($originalPurchaseIds as $ids) {
                    if (!array_key_exists($ids, $currentPurchaseIds)) {

                        $deletedIds[] = $ids;
                    }
                    if ($deletedIds) {

                        PurchaseOrderDetails::deleteAll(['id' => $deletedIds]);
                    }
                }
                $purchaseContainer = $this->purchaseReformat($request['PurchaseMaster']);


                if ($purchaseData['id'] == '') {
                    $purchase = new PurchaseOrderDetails();
                } else {
                    $purchase = PurchaseOrderDetails::findOne($purchaseData['id']);
                }

// pr($purchase);
                $purchase->purchase_id = $id;
                $purchase->material_id = $purchaseData['material_id'];
                $purchase->description = $purchaseData['description'];
                $purchase->mrv_status = $purchaseData['mrv_status'];
                $purchase->unit = $purchaseData['unit'];
                $purchase->expected_date = date('Y-m-d', strtotime($purchaseData['expected_date']));

                $purchase->order_quantity = $purchaseData['order_quantity'];
                $purchase->remaining_quantity = $purchaseData['remaining_quantity'];
// $purchase->unit = $purchaseData['unit'];
                $purchase->price = $purchaseData['price'];
                $purchase->total_amount = $purchaseData['total_amount'];
                $purchase->project_id = $projectNameID;

                return $this->redirect(['index']);
            } else {
             
                $projectListData = Project::find()->select(['project_name', 'id'])->asArray()->all();

                $assign_project = [];

                foreach ($projectListData as $value) {
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['project_name'] = $value['project_name'];
                    $assign_project[] = $eType;
                }
                $projectList = ArrayHelper::map($assign_project, 'id', 'project_name');

                $projectData = Project::find()->where(['id' => $purchaseMaster['project_name']])->one();
                $typeOneData = Customer::find()->where(['customer_type' => 3])->asArray()->all();
                $assign_ClientData = [];
                foreach ($typeOneData as $value) {

                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['company_name'] = $value['company_name'];
                    $assign_ClientData[] = $eType;
                }
                //  pr($assign_ClientData);
                $supplierData = ArrayHelper::map($assign_ClientData, 'id', 'company_name');
                $companyListData = Company::find()->select(['company_name', 'currency', 'id'])->asArray()->all();

                $companyList = [];

                foreach ($companyListData as $value) {
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['company_name'] = $value['company_name'];
                    $companyList[] = $eType;
                }

                $companylist = ArrayHelper::map($companyList, 'id', 'company_name');
                $currencyList = [];

                foreach ($companyListData as $value) {
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['currency'] = $value['currency'];
                    $currencyList[] = $eType;
                }

                $currencylist = ArrayHelper::map($currencyList, 'id', 'currency');
                $paymentTermsList = \app\models\SettingsTree::find()
                    ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                    ->where(['!=', 'lvl', 0])->andWhere(['root' => 197])
                    ->orderBy('root, lft')->asArray()->all();

                $termsType = [];
                $parent = "";
                foreach ($paymentTermsList as $d) {
                    // pr($d);
                    if ($d['lvl'] == 1) {
                        $termsType[$d['id']] = $d;
                        $parent = $d['id'];
                    } else {
                        $termsType[$parent]['sublevel'][] = $d;
                    }
                }

                $termsTypeValue = ArrayHelper::map($termsType, 'id', 'name');
                $statusList = \app\models\SettingsTree::find()
                    ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                    ->where(['!=', 'lvl', 0])->andWhere(['root' => 202])
                    ->orderBy('root, lft')->asArray()->all();

                $statusLPO = [];
                $parent = "";
                foreach ($statusList as $d) {
                    // pr($d);
                    if ($d['lvl'] == 1) {
                        $statusLPO[$d['id']] = $d;
                        $parent = $d['id'];
                    } else {
                        $statusLPO[$parent]['sublevel'][] = $d;
                    }
                }

                $LpoStatus = ArrayHelper::map($statusLPO, 'id', 'name');
                $materialStatusList = \app\models\SettingsTree::find()
                    ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                    ->where(['!=', 'lvl', 0])->andWhere(['root' => 207])
                    ->orderBy('root, lft')->asArray()->all();

                $materialStatusdata = [];
                $parent = "";
                foreach ($materialStatusList as $d) {
// pr($d);
                    if ($d['lvl'] == 1) {
                        $materialStatusdata[$d['id']] = $d;
                        $parent = $d['id'];
                    } else {
                        $materialStatusdata[$parent]['sublevel'][] = $d;
                    }
                }
                $mrvStatusList = \app\models\SettingsTree::find()
                    ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                    ->where(['!=', 'lvl', 0])->andWhere(['root' => 214])
                    ->orderBy('root, lft')->asArray()->all();

                $mrvStatus = [];
                $parent = "";
                foreach ($mrvStatusList as $d) {
// pr($d);
                    if ($d['lvl'] == 1) {
                        $mrvStatus[$d['id']] = $d;
                        $parent = $d['id'];
                    } else {
                        $mrvStatus[$parent]['sublevel'][] = $d;
                    }
                }

                $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
                $unitType = [];
                foreach ($getUnitTypeList as $value) {
                    $eTypeList = [];
                    $eTypeList['id'] = $value['id'];
                    $eTypeList['name'] = $value['name'];
                    $unitType[] = $eTypeList;
                }

                $projectData = PurchaseOrderDetails::find()->where(['purchase_id' => $id])->asArray()->all();

// pr($purchaseMaster);
// $requestedName=[];
// $refernceData=[];
                return $this->render('rupdate', [

                        'purchaseMaster' => $purchaseMaster,
                        'projectList' => $projectList,
                        'projectData' => $projectData,
                        'supplierData' => $supplierData,
                        'resourceNameListData' => $resourceNameListData,
                        'currencylist' => $currencylist,
                        'companylist' => $companylist,
                        'termsTypeValue' => $termsTypeValue,
                        'LpoStatus' => $LpoStatus,
                        // 'materialStatus' => $materialStatus,
                        // 'materialStatusdata' => $materialStatusdata,
                        'requestedName' => $requestedName,
                        'codeList' => $codeList,
                        'refernceData' => $refernceData,
                        'mrvStatus' => $mrvStatus,
                        'unitType' => $unitType

                    ]
                );

            }
        }
    }


    public function actionGetTotalQuantity()
    {
        $material_id = $_REQUEST['material_id'];

        $project_id = $_REQUEST['project_id'];

        $projectResourceMasterData = ProjectResource::find()->where(['material_id' => $material_id])->andwhere(['project_id' => $project_id])->one();
        // pr($projectResourceMasterData);
        if (!empty($projectResourceMasterData)) {
            echo $projectResourceMasterData['total_quantity'];
        } else {
            echo 0;
        }


    }

    public function actionGetPurchaseQuantity()
    {
        $material_id = $_REQUEST['material_id'];

        $project_id = $_REQUEST['project_id'];

        $projectResourceMasterData = PurchaseOrderDetails::find()->select(['order_quantity'])->where(['material_id' => $material_id, 'project_id' => $project_id]);

        $quantitySum = $projectResourceMasterData->sum('order_quantity');

        $quantity = isset($quantitySum) ? $quantitySum : 0;
        echo $quantity;
    }


    public function actionExportPdf($id)
    {
        $order_type = Yii::$app->request->get('order_type');

        $purchase = Purchase::find()->where(['id' => $id])->asArray()->one();

        $po_number = $purchase['po_number'];
        $reference = $purchase['reference'];
        $projectID = $purchase['project_name'];
        $Name = Project::find()->where(['id' => $projectID])->one();
        $projectName = $Name['project_name'];
        $supplierId = $purchase['supplier_name'];
        $supplierData = Customer::find()->where(['id' => $supplierId])->one();
        $supplierName = $supplierData['company_name'];
        $TRN_number = $supplierData['trn_number'];
        $salutation = ['1' => 'Mr', '2' => 'Mrs', '3' => 'Ms'];

        $salutionData = $salutation[$supplierData['salutation']];


        $supplierAddress = $supplierData['address'];
        $supplierphone = $supplierData['phone_number'];
        $companyID = $purchase['company_name'];
        $companyData = Company::find()->where(['id' => $companyID])->one();
        $companyLogo = $companyData['logo'];
//        $webRoot =Yii::$app->params['domain_url'];
//        $filename = $webRoot . 'pricingcalculator/web/uploads/company/';

        $webRoot = Yii::getAlias('@webroot');
        $filename = $webRoot . '/uploads/company/';
        $logo = $filename . $companyLogo;
        $companyName = $companyData['company_name'];
        $companyAddress = $companyData['address'];
        $postboxName = $companyData['po_box_no'];
        $telphone = $companyData['tel_no'];
        $faxno = $companyData['fax_no'];


        $getdate = $purchase['date_created'];
        $date = Yii::$app->formatter->asDate($getdate, 'd-MMM-Y');
        $lpo_status = $purchase['lpo_status'];
        $payment_terms = $purchase['payment_terms'];
        $lpo_statusData = \app\models\SettingsTree::find()->where(['id' => $lpo_status])->asArray()->one();
        $lpo_name = $lpo_statusData['name'];
        $payment_termsData = \app\models\SettingsTree::find()->where(['id' => $payment_terms])->asArray()->one();
        $paymentName = $payment_termsData['name'];

        // pr($email_id);
        // pr($logo);
        if ($order_type == 1) {

            $query = Purchase::find()
                ->joinWith('purchase')
                ->where(['purchase_order.id' => $id])
                ->asArray()->one();

            $purchaseId = $query['id'];

            $productInfo = PurchaseOrderDetails::find()
                ->where(['purchase_order_details.purchase_id' => $purchaseId])
                ->asArray()->all();
            // pr($productInfo);
            $word = $query['notes'];


            $dataList = [];
            $totalAmount = 0;
            foreach ($productInfo as $value) {
                // pr($value);
                $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();


                foreach ($getActivityName as $value1) {
                    $dataVal = [];
                    // $resource = $value1['name'];
                    if (!empty($value['total_amount'])) {
                        $dataVal['material_id'] = $value1['name'];
                        $dataVal['description'] = $value['description'];
                        $dataVal['order_quantity'] = $value['order_quantity'];
                        $dataVal['price'] = $value['price'];
                        $dataVal['total_amount'] = $value['total_amount'];
                        $totalAmount += $value['total_amount'];

                    }
                    $dataList[] = $dataVal;

                }


            }

            $filename = $projectName . '.pdf';
            $total_amount = isset($purchase['total_amount']) ? $purchase['total_amount'] : 0;
            $subtotalAmount = isset($purchase['sub_total']) ? $purchase['sub_total'] : 0;
            $VatAmount = isset($purchase['vat']) ? $purchase['vat'] : 0;
            $Discount = isset($purchase['discount']) ? $purchase['discount'] : 0;


            $homeurl = Yii::$app->params['domain_url'];
            // pr($companyID);

            $content = $this->renderPartial('pdf', [
                'homeurl' => $homeurl,
                'productInfo' => $dataList,
                'word' => $word,
                'total_amount' => $total_amount,
                'subtotalAmount' => $subtotalAmount,
                'VatAmount' => $VatAmount,
                'projectName' => $projectName,
                'supplierName' => $supplierName,
                'logo' => $logo,
                'supplierAddress' => $supplierAddress,
                'supplierphone' => $supplierphone,
                'reference' => $reference,
                'purchase' => $purchase,
                'TRN_number' => $TRN_number,
                'Discount' => $Discount,

                'salutionData' => $salutionData,
                'supplierphone' => $supplierphone,
                'companyAddress' => $companyAddress,
                'companyName' => $companyName,
                'postboxName'=>$postboxName,
                'telphone'=>$telphone,
                'faxno'=>$faxno


            ]);

        } else if ($order_type == 2) {
            $query = Purchase::find()
                ->joinWith('purchase')
                ->where(['purchase_order.id' => $id])
                ->asArray()->one();

            $purchaseId = $query['id'];

            $productInfo = PurchaseOrderDetails::find()
                ->where(['purchase_order_details.purchase_id' => $purchaseId])
                ->asArray()->all();
            // pr($productInfo);
            $requestId = '';

            foreach ($productInfo as $request_id) {

                $requestId = $request_id['purchase_id'];

            }
// pr($requestId);

            $word = $query['notes'];
            $dataList = [];
            $totalAmount = 0;
            $arrayData = [];
            $purchaseRequestData = PurchaseRequestDetails::find()
                ->where(['request_order_id' => $requestId])
                ->asArray()->all();

// pr($purchaseRequestData);
            foreach ($productInfo as $value) {

                foreach ($purchaseRequestData as $value1) {
                    // pr($value1);
                    $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();
                    foreach ($getActivityName as $value2) {

                        $dataVal = [];

                        if (!empty($value['total_amount'])) {
                            $dataVal['material_id'] = $value2['name'];
                            $dataVal['description'] = $value['description'];
                            // $dataVal['mrv_'] = $value2['name'];
                            $dataVal['total_quantity'] = $value1['material_total_quantity'];
                            $dataVal['request_quantity'] = $value1['material_request_quantity'];

                            $dataVal['order_quantity'] = $value['order_quantity'];

                            $dataVal['price'] = $value['price'];
                            $dataVal['total_amount'] = $value['total_amount'];
                            $totalAmount += $value['total_amount'];

                        }
                        $dataList[] = $dataVal;

                    }

                }
            }

            //    pr($dataList);
            $filename = $projectName . '.pdf';
            $total_amount = isset($purchase['total_amount']) ? $purchase['total_amount'] : 0;
            $subtotalAmount = isset($purchase['sub_total']) ? $purchase['sub_total'] : 0;
            $VatAmount = isset($purchase['vat']) ? $purchase['vat'] : 0;
            $Discount = isset($purchase['discount']) ? $purchase['discount'] : 0;


            $homeurl = Yii::$app->params['domain_url'];
            // pr($companyID);
            $content = $this->renderPartial('requestpdf', [
                'homeurl' => $homeurl,
                'productInfo' => $dataList,
                'word' => $word,
                'total_amount' => $total_amount,
                'subtotalAmount' => $subtotalAmount,
                'VatAmount' => $VatAmount,
                'Discount' => $Discount,
                'projectName' => $projectName,
                'supplierName' => $supplierName,
                'logo' => $logo,
                'supplierAddress' => $supplierAddress,
                'supplierphone' => $supplierphone,
                'reference' => $reference,
                'purchase' => $purchase, 'lpo_name' => $lpo_name,
                'paymentName' => $paymentName,
                'po_number' => $po_number,
                'date' => $date,
                'TRN_number' => $TRN_number,
                'salutionData' => $salutionData,
                'supplierphone' => $supplierphone,
                'companyAddress' => $companyAddress,
                'companyName' => $companyName

            ]);
        }
        $destination = Pdf::DEST_DOWNLOAD;

//
//echo $logo;
//die();
        $po_pdf = new Pdf([
            // 'mimeType' => 'application/pdf',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/pdf.css',
            // any css to be embedded if required
            'defaultFont' => 'Lato',
            'marginTop' => 40,
            'cssInline' => ' table,.new_head, .borderbox {font-size:13px !important; font-family:"Lato", sans-serif;} .text-bold{  font-family:"Lato", sans-serif;}',
            // 'cssInline' => 'table,.new_head, .borderbox {font-size:13px !important; font-family:"century";} .text-bold{  font-family:"century-b";}',
            // set mPDF properties on the fly
            'format' => Pdf::FORMAT_A4,
            'destination' => Pdf::DEST_DOWNLOAD,
            'filename' => $projectName . '.' . 'pdf',
            'options' => [
                'defaultheaderline' => 0,  //for header
            ],


            'methods' => [

                'SetHeader' => [$companyName.'||Generated On: ' . date("r")],
                'SetSubject' => ("
<table class='table' style='margin-bottom: 0px' border='1'>
<tr>
<td  colspan='2' align='center'   style=\"font - size: 25px;font-weight: bold;\" >$companyName</td>
</tr>
<tr>
<td width='50%'><img src='$logo' width='110px'/></td>
<td width='25%'><table class='table'>

<tr>

<td  align='right'  style=\"font-size: 15px;\" >

</td><td>Post Box No : $postboxName
<br>
Tel No : $telphone
<br>
Fax No : $faxno
</td>


</tr>

<tr>

</tr>
<tr>

</tr></table></td>

</tr>
</table>
<div style='clear: both;'></div>


"),
                'SetFooter' => ('
<table class=\'table\'> <tr>
<td width="20%"  style="font-weight:bold; text-align:center"><span style=" color: white">&nbsp; Contenthide</span> P.O.Box - ' . $companyData['po_box_no'] . ', Tel: ' . $companyData['tel_no'] . ', Fax: ' . $companyData['fax_no'] . ', E-mail: ' . $companyData['email_id'] . '</td>
<td width="10%" style="text-align: right;font-weight:bold;">{PAGENO}</td>
</tr>
</table>')
            ],
        ]);

//        pr($po_pdf->methods);
//
//        die();

        return $po_pdf->render();


    }


    public function actionDeletes($id)
    {
        $this->findModel($id)->delete();
        PurchaseOrderDetails::deleteAll(['purchase_id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Purchase::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    protected function findModelDelivery($id)
    {
        if (($model = PurchaseDelivery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDetailview()
    {
        $id = $_GET['id'];
        return $this->render('detail_view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionAmount()
    {

        $amount = $_REQUEST['data'];

        $word1 = \app\components\Helper::numtowords2($amount);
        echo $word1;

    }


    protected function deleteRecords($previousIds, $currentIds, $table)
    {
        $deletedIds = array_diff_key($previousIds, $currentIds);
        if ($deletedIds) {
            if ($table == "items") {

                //Get all the Sections IDs before Delete the Items for the respective
                $sectionIdsbeforeDel = QuotationItems::find()->select('section_id')->where(['id' => $deletedIds])->asArray()->all();
                $sectionnIdsBD = [];
                foreach ($sectionIdsbeforeDel as $sectionIdsBDData) {
                    $sectionIdsBD[$sectionIdsBDData['section_id']] = $sectionIdsBDData['section_id'];
                }
                QuotationItems::deleteAll(['id' => $deletedIds]);

                //Get all the Sections IDs again again after Delete the Items now by section to see atleast one entry for each section
                $sectionIdsafterDel = QuotationItems::find()->select('section_id')->where(['section_id' => $sectionIdsBD])->asArray()->all();
                $sectionIdsAD = [];
                foreach ($sectionIdsafterDel as $sectionIdsADData) {
                    $sectionIdsAD[$sectionIdsADData['section_id']] = $sectionIdsADData['section_id'];
                }
                //Compare before after Sections ids to find the section ids for delete
                $sectionIdstoDelete = array_diff_key($sectionIdsBD, $sectionIdsAD);
                if ($sectionIdstoDelete) {
                    QuotationSection::deleteAll(['id' => $sectionIdstoDelete]);
                }

            } else if ($table == "section") {
                QuotationSection::deleteAll(['id' => $deletedIds]);
            }
        }
    }


}