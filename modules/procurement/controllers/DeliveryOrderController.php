<?php

namespace app\modules\procurement\controllers;
use app\components\Procurement;
use yii\helpers\Html;
use app\models\PurchaseRequestMaster;
use app\models\PurchaseRequestDetails;
use app\models\PurchaseRequestMaterial;
use app\models\PurchaseDelivery;
use app\models\PurchaseDeliverySearch;
use app\models\Search\DeliverySearch;
use app\models\PurchaseDeliveryStatus;
// use app\models\Delivery;

use app\models\Delivery;
use app\models\DeliveryOrderDetails;
use app\models\DeliveryOrder;
// purchaseDelivery

use Yii;
use app\models\Purchase;
use app\models\PurchaseSearch;
use yii\web\NotFoundHttpException;

use kartik\export\ExportMenu;

use yii\helpers\ArrayHelper;

use kartik\mpdf\Pdf;
use app\controllers\BaseController;
use app\models\SettingsTree;

use app\models\Tree;
use app\models\Project;
use app\components\MenuHelper;
use app\models\Customer;

use app\models\ProjectResource;
use app\models\PurchaseOrderDetails;
use app\models\Company;


/**
 * ProjectController implements the CRUD actions for Project model.
 */
class DeliveryOrderController extends BaseController
{
    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {

        $route = $this->route;

        $actionList = MenuHelper::getActionHelper($route);
        // pr($actionList);
        $visibleList = MenuHelper::getActionVisibleHelper($route);

        $searchModel = new DeliverySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize' => 50];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionList' => $actionList,
            'visibleList' => $visibleList
        ]);

    }


    


    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGetReferenceName()
    {
        $project_id = $_REQUEST['project_id'];
        $codeListData = PurchaseRequestMaster::find()->select(['reference_name', 'id'])->where(['project_id' => $project_id])->asArray()->all();

        $codeData = [];

        foreach ($codeListData as $value) {
            $eType = [];
            $eType['id'] = $value['id'];
            $eType['reference_name'] = $value['reference_name'];
            $codeData[] = $eType;
        }
        $codeList = ArrayHelper::map($codeData, 'id', 'reference_name');
// pr($codeList);
        echo "<option value=''>Select Area</option>";
        if (count($codeList) > 0) {
            foreach ($codeList as $id => $reference_name) {
                echo "<option value='$id'>$reference_name</option>";
            }
        }

    }

    public function actionGetMaterialList()
    {
        $project_id = $_REQUEST['project_id'];
        $projectTypeData = Project::find()->where(['id' => $project_id])->asArray()->one();

        $projectType = $projectTypeData['property_account_type'];

        $getAcName = SettingsTree::find()->where(['id' => $projectType])->asArray()->one();

        $Atype = $getAcName['name'];

        $accountType = strtolower($Atype);
// pr($accountType);
        if ($accountType == 'profit center') {
            $resource_type = '1';
            $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->andwhere(['project_id' => $project_id])->groupBy('material_id')->asArray()->all();
            //  pr($projectResourceMasterData);
            $resourceNameListData = [];
            foreach ($projectResourceMasterData as $value) {

                $cat = [];
                $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();
                foreach ($getActivityName as $value1) {
                    $resourceNameListData[$value['material_id']] = $value1['name'];
                    // $cat['name'] = $value1['name'];
                }
            }
// pr($resourceNameListData);
            echo "<option value=''>Select</option>";
            foreach ($resourceNameListData as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }

        } else if ($accountType == 'cost center') {

            $itemType = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();

            $itemTypeList = [];
            foreach ($itemType as $value) {
                $itemTypeList[$value['id']] = $value['name'];
            }
            echo "<option value=''>Select</option>";
            foreach ($itemTypeList as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }

        }
    }


    

    public function actionGetDeliveryData()
    {
        $po_number = $_REQUEST['po_number'];

        $arrayData = [];
        if ($po_number != '') {

            $purchaseDeliveryData = PurchaseOrderDetails::find()->with('delivery')
                ->where(['purchase_id' => $po_number])->asArray()->all();
            //pr($purchaseDeliveryData);

            foreach ($purchaseDeliveryData as $value1) {

                $deliveredQuantity=isset($value1['delivery']['delivered_quantity'])?$value1['delivery']['delivered_quantity']:0;
                $deliveryStatus=isset($value1['delivery']['delivery_status'])?$value1['delivery']['delivery_status']:0;

                $resData = [];
                $resData['id'] = $value1['id'];
                $resData['po_id'] = $value1['purchase_id'];

                $resData['material_id'] = $value1['material_id'];
                $resData['description'] = $value1['description'];
                $resData['delivery_status'] = $deliveryStatus;
                $resData['order_quantity'] = $value1['order_quantity'];
                $resData['delivered_quantity'] =$deliveredQuantity;
                $arrayData[] = $resData;
            }
        } else {
            $arrayData = [];
        }


        $resource_type = ('1');
        $materialNameListData = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();

        // $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->groupBy('material_id')->asArray()->all();
        $resourceNameListData = [];
        foreach ($materialNameListData as $value) {

            $cat = [];
            $cat['id'] = $value['id'];
            $cat['name'] = $value['name'];
            $resourceNameListData[] = $cat;
        }

        $statusList = \app\models\SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 202])
            ->orderBy('root, lft')->asArray()->all();

        $statusLPO = [];
        $parent = "";
        foreach ($statusList as $d) {
            // pr($d);
            if ($d['lvl'] == 1) {
                $statusLPO[$d['id']] = $d;
                $parent = $d['id'];
            } else {
                $statusLPO[$parent]['sublevel'][] = $d;
            }
        }

        $LpoStatus = ArrayHelper::map($statusLPO, 'id', 'name');
        $mrvStatusList = \app\models\SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 214])
            ->orderBy('root, lft')->asArray()->all();


        $mrvSatusData = [];
        $parent = "";
        foreach ($mrvStatusList as $d) {
            // pr($d);
            if ($d['lvl'] == 1) {
                $mrvSatusData[$d['id']] = $d;
                $parent = $d['id'];
            } else {
                $mrvSatusData[$parent]['sublevel'][] = $d;
            }
        }
        // }


        return $this->renderPartial('request', [
                'purchaseMaster' => $arrayData,
                'arrayData' => $arrayData,
                'resourceNameListData' => $resourceNameListData,
                'LpoStatus' => $LpoStatus,
                'mrvSatusData' => $mrvSatusData


            ]
        );
    }


    public function actionGetSupplier()
    {
        $project_id = $_REQUEST['project_id'];


        $purchaseDataList = Purchase::find()->where(['project_name' => $project_id])->asArray()->all();

        $supplierData = [];
        foreach ($purchaseDataList as $value1) {

            $customerData = Customer::find()->where(['id' => $value1['supplier_name']])->asArray()->all();
            // pr($value1);


            foreach ($customerData as $value) {
                $eType = [];
                $eType['id'] = $value['id'];
                $eType['company_name'] = $value['company_name'];
                $supplierData[] = $eType;
            }
        }
        $supplierName = ArrayHelper::map($supplierData, 'id', 'company_name');
// pr($supplierName);
        echo "<option value=''>Select Area</option>";
        if (count($supplierName) > 0) {
            foreach ($supplierName as $id => $company_name) {
                echo "<option value='$id'>$company_name</option>";
            }
        }


    }


    public function actionGetDocumentStatus()
    {
        $project_id = $_REQUEST['project_id'];
        $purchaseDataList = Purchase::find()->where(['project_name' => $project_id])->asArray()->all();
        $statusName = ArrayHelper::map($purchaseDataList, 'id', 'name');

        $options['id']='purchase-master-material';
        $options['disabled']='disabled';
        $options['class']='form-control required material dropdownclass';
        $options['data-row']=0;

        echo Html::dropDownList('po_id', "", $statusName,[]);
 }

    
    public function actionSuplierList(){
        $projectId = $_REQUEST['depdrop_all_params']['project_id'];
        $out=[];
        $purchaseDataList = Purchase::find()->with('suplierList')->where(['project_name' => $projectId])->asArray()->all();
        if(!empty($purchaseDataList)){
            $supplierList = ArrayHelper::map($purchaseDataList, 'suplierList.id', 'suplierList.company_name');
            foreach ($supplierList as $id=>$name){

                    $out[] = ['id' => $id, 'name' => $name];

                }

            echo json_encode(['output'=>$out, 'selected'=>''], JSON_FORCE_OBJECT);
            die();
        }

        echo json_encode(['output'=>$out, 'selected'=>''], JSON_FORCE_OBJECT);
        die();
    }
    public function actionPoList(){
        $projectId = $_REQUEST['depdrop_all_params']['project_id'];
        $supplierId = $_REQUEST['depdrop_all_params']['supplier_name'];
        $out=[];
        $purchaseDataList = Purchase::find()->with('suplierList')->where(['project_name' => $projectId,'supplier_name'=>$supplierId])->asArray()->all();

        if(!empty($purchaseDataList)){
            $supplierList = ArrayHelper::map($purchaseDataList, 'id', 'po_number');
            foreach ($supplierList as $id=>$name){
                $out[] = ['id' => $id, 'name' => $name];

            }

            echo json_encode(['output'=>$out, 'selected'=>''], JSON_FORCE_OBJECT);
            die();
        }

        echo json_encode(['output'=>$out, 'selected'=>''], JSON_FORCE_OBJECT);
        die();


    }
    public function actionLpoStatus(){
        $poId = $_REQUEST['depdrop_all_params']['po_id'];
        $LpoStatus = Procurement::getDocumentStatus();
        $delivery = PurchaseDelivery::find()->where(['po_details_id'=>$poId])->one();
            if(!empty($delivery)){
                    $lpoValue=2;
            }else{
                $lpoValue=1;
            }

            foreach ($LpoStatus as $id=>$name) {
                $out[] = ['id' => $id, 'name' => $name];
            }
            // pr($out);
            echo json_encode(['output'=>$out, 'selected'=>$lpoValue], JSON_FORCE_OBJECT);
            die();

    }
    public function actionCurrency(){
        $poId = $_REQUEST['depdrop_all_params']['po_id'];
        $currencyList = Procurement::getCurency();
        $delivery = PurchaseDelivery::find()->where(['po_details_id'=>$poId])->one();
        if(!empty($delivery)){
            $currencyValue=2;
        }else{
            $currencyValue=1;
        }

        foreach ($currencyList as $id=>$name) {
            $out[] = ['id' => $id, 'name' => $name];
        }
        echo json_encode(['output'=>$out, 'selected'=>$currencyValue], JSON_FORCE_OBJECT);
        die();

    }
    public function actionPaymentTerms(){
        $poId = $_REQUEST['depdrop_all_params']['po_id'];
        $paymentTermsList = SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 197])
            ->orderBy('root, lft')->asArray()->all();
        $paymentTermList = ArrayHelper::map($paymentTermsList, 'id', 'name');

        $delivery = PurchaseDelivery::find()->where(['po_details_id'=>$poId])->one();
        if(!empty($delivery)){
            $paymentId=2;
        }else{
            $paymentId=1;
        }

        foreach ($paymentTermList as $id=>$name) {
            $out[] = ['id' => $id, 'name' => $name];
        }
        echo json_encode(['output'=>$out, 'selected'=>$paymentId], JSON_FORCE_OBJECT);
        die();

    }
    public function actionDeliveryOrderNumber(){
        $poId=$_REQUEST['po_number'];
        $poValue=$_REQUEST['po_value'];
        if($poId!='') {


            $deliveryorderCount = DeliveryOrder::find()->where(['po_id' => $poId])->count();
            $deliveryorderCount = $deliveryorderCount + 1;
            $strpadnumber = str_pad($deliveryorderCount, 2, '0', STR_PAD_LEFT);
            echo $poValue . "/" . $strpadnumber;
            die();
        }

    }


    public function actionGetPaymentTerms()
    {
        $poId = $_REQUEST['depdrop_all_params']['po_id'];
        $purchaseDataList = Purchase::find()->with('paymentTerms')->where(['project_name' => $project_id])->asArray()->all();
        pr($purchaseDataList);
        $statusName = ArrayHelper::map($purchaseDataList, 'id', 'name');

        $options['id']='purchase-master-material';
        $options['disabled']='disabled';
        $options['class']='form-control required material dropdownclass';
        $options['data-row']=0;

        echo Html::dropDownList('po_id', "", $statusName,[]);


    }

    public function actionGetCurrency()
    {
        $project_id = $_REQUEST['project_id'];
        $purchaseDataList = Purchase::find()->where(['project_name' => $project_id])->asArray()->all();

        $supplierData = [];
        foreach ($purchaseDataList as $value1) {
            //    pr($value1);
            $customerData = Company::find()->where(['id' => $value1['currency']])->asArray()->all();
            // pr($value1);


            foreach ($customerData as $value) {
                $eType = [];
                $eType['id'] = $value['id'];
                $eType['currency'] = $value['currency'];
                $supplierData[] = $eType;
            }
        }
        $supplierName = ArrayHelper::map($supplierData, 'id', 'currency');

        echo "<option value=''>Select currency</option>";
        if (count($supplierName) > 0) {

            foreach ($supplierName as $id => $currency) {
                echo "<option value='$id'>$currency</option>";
            }
        }


    }


    public function actionCreate()
    {


        $deliveryOrder = new PurchaseDeliveryStatus();
        $deliveryNumber = new DeliveryOrder();
        $delivery = new PurchaseDelivery();
        $mrvStatusList =Procurement::getMRVStatus();


        if ($_POST) {
            $requesttData = Yii::$app->request->post();
            // pr($requesttData);
            $deliveryContainer = $this->purchaseReformat($requesttData['DeliveryDetails']);
            $postData = $requesttData['PurchaseDelivery'];
            // pr($postData);
            foreach ($deliveryContainer as $value) {

                $poDetailsId=$value['podetails'];

                $delivery = PurchaseDelivery::find()->where(['po_details_id'=>$poDetailsId])->one();
                if(empty($delivery)){
                    $delivery = new PurchaseDelivery();
                    $delivery->delivered_quantity = $value['received_quantity'];
                }else{
                    $delivery->delivered_quantity = $delivery->delivered_quantity + $value['received_quantity'];
                }
                if($delivery->delivered_quantity==0){
                    $delivery->delivery_status = 215;
                }else if($delivery->delivered_quantity<$value['order_quantity']){
                    $delivery->delivery_status = 217;
                }
                if($value['order_quantity']==$delivery->delivered_quantity){
                    $delivery->delivery_status = 218;
                }

                $delivery->po_id = $postData['po_id'];
                $delivery->po_details_id = $value['podetails'];
                $delivery->project_id = $requesttData['project_id'];
                $delivery->supplier_name = $postData['supplier_name'];
                $delivery->lpo_status = $postData['lpo_status'];
                $delivery->currency = $postData['currency'];
                $delivery->payment_terms = $postData['payment_terms'];
                $delivery->delivery_order_number = $postData['delivery_order_number'];
                $delivery->delivery_date = date('Y-m-d', strtotime($postData['delivery_date']));
                $delivery->material_id = $value['material_id'];
                $delivery->description = $value['description'];

                $delivery->order_quantity = $value['order_quantity'];

                if (!$delivery->save()) {
                    pr($delivery->getErrors());
                }
                $deliveryId = $delivery->id;
                $pdStatus = new PurchaseDeliveryStatus();
                $pdStatus->purchase_delivery_id = $deliveryId;
                $pdStatus->received_date = date('Y-m-d', strtotime($postData['delivery_date']));
                $pdStatus->received_quantity = $value['received_quantity'];

                if (!$pdStatus->save()) {
                    pr($pdStatus->getErrors());
                }

            }

            $deliveryNumber->project_id = $requesttData['project_id'];
            $deliveryNumber->po_id = $postData['po_id'];
            $deliveryNumber->supplier_delivery_number = $requesttData['DeliveryOrder']['supplier_delivery_number'];
            if (!$deliveryNumber->save()) {
                pr($deliveryNumber->getErrors());
            }


            return $this->redirect(['index']);
        }
        else {

            $projectListData = Project::find()->select(['project_code', 'id'])->asArray()->all();

            $projectList = ArrayHelper::map($projectListData, 'id', 'project_code');


            $resource_type = '1';
            $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->groupBy('material_id')->asArray()->all();
            $resourceNameListData = [];
            foreach ($projectResourceMasterData as $value) {

                $cat = [];
                $cat['id'] = $value['material_id'];
                $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();
                foreach ($getActivityName as $value1) {
                    //    pr($value1);
                    $cat['name'] = $value1['name'];
                }
                $resourceNameListData[] = $cat;
            }






            $poListData = Purchase::find()->select(['po_number', 'id'])->asArray()->all();

            $poDataList = [];
            foreach ($poListData as $value) {

                $eType = [];
                $eType['id'] = $value['id'];
                $eType['po_number'] = $value['po_number'];
                $poDataList[] = $eType;
            }

            $poList = ArrayHelper::map($poDataList, 'id', 'po_number');
            // $purchaseSupplier=Purchase::find()->where('project_name'=>$)->asArray()->all();

            $typeOneData = Customer::find()->where(['customer_type' => 3])->asArray()->all();
            $assign_ClientData = [];
            foreach ($typeOneData as $value) {

                $eType = [];
                $eType['id'] = $value['id'];
                $eType['company_name'] = $value['company_name'];
                $assign_ClientData[] = $eType;
            }
            //  pr($assign_ClientData);
            $supplierData = ArrayHelper::map($assign_ClientData, 'id', 'company_name');


            $LpoStatus = Procurement::getDocumentStatus();
            $mrvStatus=Procurement::getMRVStatus();
            $currencylist = Procurement::getCurency();

            $paymentTermsList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 197])
                ->orderBy('root, lft')->asArray()->all();

            $termsType = [];
            $parent = "";
            foreach ($paymentTermsList as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $termsType[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $termsType[$parent]['sublevel'][] = $d;
                }
            }

            $termsTypeValue = ArrayHelper::map($termsType, 'id', 'name');
            return $this->render('create', ['delivery' => $delivery,
                    'deliveryOrder' => $deliveryOrder,
                    'projectList' => $projectList,
                    'mrvStatus' => $mrvStatus,
                    'resourceNameListData' => $resourceNameListData,
                    'poList' => $poList,
                    'supplierData' => $supplierData,
                    'LpoStatus' => $LpoStatus,
                    'currencylist' => $currencylist,
                    'termsTypeValue' => $termsTypeValue,
                    'deliveryNumber' => $deliveryNumber

                ]
            );

        }
    }


    protected function purchaseReformat($purchaseDatas)
    {

        $purchaseKeysvalue = array_keys($purchaseDatas);

        $purchaseData = array_values($purchaseDatas);
        // pr($purchaseKeysvalue);
        $purchaseTotalCount = count(array_values($purchaseData[0]));

        $purchaseContainer = [];
        for ($i = 0; $i < $purchaseTotalCount; $i++) {
            $purchaseTemp = [];
            foreach ($purchaseKeysvalue as $key) {
                $purchaseTemp[$key] = isset($purchaseDatas[$key][$i]) ? $purchaseDatas[$key][$i] : '';
            }
            $purchaseContainer[] = $purchaseTemp;
        }
        return $purchaseContainer;
    }


    public function actionUpdate($id)
    {


        $purchaseDelivery = $this->findModel($id);
        // pr($purchaseDelivery);
        $deliveryNumber = new DeliveryOrder();

        $poId = $purchaseDelivery['po_id'];
        // pr($poId);
        $project_id = $purchaseDelivery['project_id'];
        $deliveryNumber = DeliveryOrder::findOne($id);

        if ($_POST) {
            //    pr("fgf");
            $postData = Yii::$app->request->post();
            // pr($postData);
            $deliveryContainer = $this->purchaseReformat($postData['DeliveryDetails']);

            foreach ($deliveryContainer as $value) {
                // pr($deliveryContainer);
                if ($id == '') {
                    $delivery = new PurchaseDelivery();
                } else {
                    $delivery = PurchaseDelivery::findOne($id);
                }
                $projectId = $postData['project_id'];


                $deliveryQuantityData = PurchaseDelivery::find()->where(['project_id' => $projectId, 'material_id' => $value['material_id']])->one();
                $quantitySum = !empty($deliveryQuantityData['delivered_quantity']) ? (int)$deliveryQuantityData['delivered_quantity'] : 0;

                $delivery->po_id = $postData['PurchaseDelivery']['po_id'];
                $delivery->po_details_id = $value['podetails'];
                $delivery->project_id = $postData['project_id'];

                $delivery->supplier_name = $postData['PurchaseDelivery']['supplier_name'];
                $delivery->lpo_status = $postData['PurchaseDelivery']['lpo_status'];
                $delivery->currency = $postData['PurchaseDelivery']['currency'];
                $delivery->payment_terms = $postData['PurchaseDelivery']['payment_terms'];
                $delivery->delivery_order_number = $postData['PurchaseDelivery']['delivery_order_number'];
                $delivery->delivery_date = date('Y-m-d', strtotime($postData['PurchaseDelivery']['delivery_date']));
                $delivery->material_id = $value['material_id'];
                $delivery->description = $value['description'];
                $delivery->delivery_status = $value['delivery_status'];
                $delivery->order_quantity = $value['order_quantity'];
                $delivery->delivered_quantity = $value['delivered_quantity'] + $quantitySum;
// $deliveredQuantity=$value['delivered_quantity'];


                if (!$delivery->save()) {
                    pr($delivery->getErrors());
                }
                $deliveryId = $delivery->id;
                if ($deliveryId == '') {
                    $pdStatus = new PurchaseDeliveryStatus();
                } else {
                    $pdStatus = PurchaseDeliveryStatus::findOne($deliveryId);
                }

                $deliveryQuantityData = PurchaseDeliveryStatus::find()->where(['purchase_delivery_id' => $deliveryId])->one();
                $receivedSum = !empty($deliveryQuantityData['received_quantity']) ? (int)$deliveryQuantityData['received_quantity'] : 0;
                $pdStatus->purchase_delivery_id = $deliveryId;
                $pdStatus->received_date = date('Y-m-d', strtotime($postData['PurchaseDelivery']['delivery_date']));
                $pdStatus->received_quantity = $value['received_quantity'] + $receivedSum;

                if (!$pdStatus->save()) {
                    pr($pdStatus->getErrors());
                }

            }


            //  pr($deliveryNumber);
            //  $model = $this->findModel($id);
            $deliveryNumber->project_id = $postData['project_id'];
            $deliveryNumber->po_id = $postData['PurchaseDelivery']['po_id'];
            $deliveryNumber->supplier_delivery_number = $postData['DeliveryOrder']['supplier_delivery_number'];
            if (!$deliveryNumber->save()) {
                pr($deliveryNumber->getErrors());
            }


            return $this->redirect(['index']);
        } else {


// pr("fhgfhfgh");
            $deliveryData = PurchaseDelivery::find()->where(['po_id' => $poId, 'project_id' => $project_id])->asArray()->all();
// pr($deliveryData);
            $arrayData = [];
            $supplier_name=[];
            foreach ($deliveryData as $value1) {

                $resData = [];
                $resData['id'] = $value1['id'];
                $resData['po_id'] = $value1['po_id'];
                $resData['po_details_id'] = $value1['po_details_id'];
                $resData['material_id'] = $value1['material_id'];
                $resData['description'] = $value1['description'];
                $resData['delivery_status'] = $value1['delivery_status'];
                // $resData['delivery_date'] = $value1['delivery_date'];
                $resData['order_quantity'] = $value1['order_quantity'];
                $resData['delivered_quantity'] = $value1['delivered_quantity'];
                $supplier_name = $value1['supplier_name'];
                $arrayData[] = $resData;
            }

           
            $projectListData = Project::find()->select(['project_code', 'id'])->asArray()->all();
            // pr($projectListData);
            $assign_project = [];

            foreach ($projectListData as $value) {
                $eType = [];
                $eType['id'] = $value['id'];
                $eType['project_code'] = $value['project_code'];
                $assign_project[] = $eType;
            }
            $projectList = ArrayHelper::map($assign_project, 'id', 'project_code');
         
            $poListData = Purchase::find()->select(['po_number', 'id'])->where(['project_name' => $project_id])->asArray()->all();
            // pr($poListData);
            $poDataList = [];
            foreach ($poListData as $value) {

                $eType = [];
                $eType['id'] = $value['id'];
                $eType['po_number'] = $value['po_number'];
                $poDataList[] = $eType;
            }


            $poList = ArrayHelper::map($poDataList, 'id', 'po_number');
            $customerData = Customer::find()->select(['company_name', 'id'])->where(['id'=>$supplier_name])->asArray()->all();
// pr($customerData);
            $supplierDataList = [];
            foreach ($customerData as $value) {

                $eType = [];
                $eType['id'] = $value['id'];
                $eType['company_name'] = $value['company_name'];
                $supplierDataList[] = $eType;
            }

            $supplierData = ArrayHelper::map($supplierDataList, 'id', 'company_name');
// pr($supplierData);
            $currencyData = Company::find()->select(['currency', 'id'])->asArray()->all();
            // pr($poListData);
            $currencyDataList = [];
            foreach ($currencyData as $value) {

                $eType = [];
                $eType['id'] = $value['id'];
                $eType['currency'] = $value['currency'];
                $currencyDataList[] = $eType;
            }

            $currencyList = ArrayHelper::map($currencyDataList, 'id', 'currency');


            $documentStatusList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 202])
                ->orderBy('root, lft')->asArray()->all();


            $documentData = [];
            $parent = "";
            foreach ($documentStatusList as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $documentData[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $documentData[$parent]['sublevel'][] = $d;
                }
            }
            $documentStatus = ArrayHelper::map($documentData, 'id', 'name');
            $paymentList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 197])
                ->orderBy('root, lft')->asArray()->all();


            $paymentData = [];
            $parent = "";
            foreach ($paymentList as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $paymentData[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $paymentData[$parent]['sublevel'][] = $d;
                }
            }
            $paymentTerms = ArrayHelper::map($paymentData, 'id', 'name');
// pr($paymentTerms);

// pr($poList);

            $projectData = Project::find()->where(['id' => $purchaseDelivery['project_id']])->one();

            // $projectData = PurchaseOrderDetails::find()->where(['purchase_id' => $id])->asArray()->all();

            $deliveryStatusList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 214])
                ->orderBy('root, lft')->asArray()->all();

            $deliveryStatus = [];
            $parent = "";
            foreach ($deliveryStatusList as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $deliveryStatus[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $deliveryStatus[$parent]['sublevel'][] = $d;
                }
            }

            $resource_type = ('1');
            $materialNameListData = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();

            // $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->groupBy('material_id')->asArray()->all();
            $resourceNameListData = [];
            foreach ($materialNameListData as $value) {

                $cat = [];
                $cat['id'] = $value['id'];
                $cat['name'] = $value['name'];
                $resourceNameListData[] = $cat;
            }
            //        pr($resourceNameListData);

            $refernceData = PurchaseDelivery::find()->select('po_id', 'id')->where(['po_id' => $id])->asArray()->one();
            $po_id = $refernceData['po_id'];

            $getPONumber = Purchase::find()->select(['po_number', 'id'])->where(['id' => $poId])->asArray()->one();

            $po_number = $getPONumber['po_number'];
            //    pr($getPONumber);

            $poDataList = PurchaseDelivery::find()->select('po_id')->where(['po_id' => $id])->asArray()->all();
            $poName = ArrayHelper::getColumn($poDataList, 'po_id');
            //    pr($requestedName);

            return $this->render('update', [

                    'purchaseDelivery' => $purchaseDelivery,
                    'projectList' => $projectList,
                    'projectData' => $projectData,
                    'resourceNameListData' => $resourceNameListData,
                    'deliveryStatus' => $deliveryStatus,
                    'poList' => $poList,
                    'arrayData' => $arrayData,
                    'poName' => $poName,
                    'deliveryNumber' => $deliveryNumber,
                    'documentStatus' => $documentStatus,
                    'paymentTerms' => $paymentTerms,
                    'currencyList' => $currencyList,
                    'supplierData' => $supplierData
                ]
            );

        }

    }

    public function actionPonumber_view($id)
    {
        $DeliveryData = $this->findModel($id);

        $poId = $DeliveryData['po_id'];
        $project_id = $DeliveryData['project_id'];
        $id = $_GET['id'];
        $purchaseDelivery = PurchaseDelivery::find()->where(['po_id' => $poId, 'project_id' => $project_id])->asArray()->all();

        $projectName = '';
        $po_number = '';
        foreach ($purchaseDelivery as $poData) {
            $projectData = Project::find()->where(['id' => $poData['project_id']])->one();
            $purchaseData = Purchase::find()->where(['id' => $poData['po_id']])->one();
            $projectName = $projectData['project_name'];
            $po_number = $purchaseData['po_number'];
            $supplier_name = $poData['supplier_name'];
            $delivery_order_number = $poData['delivery_order_number'];
            $delivery_date = $poData['delivery_date'];
        }


        // pr($po_number);


        $arrayData = [];


        $purchaseDeliveryData = PurchaseDelivery::find()->where(['po_id' => $poId, 'project_id' => $project_id])->asArray()->all();
        foreach ($purchaseDeliveryData as $value1) {

            $resData = [];
            $resData['id'] = $value1['id'];
            $resData['material_id'] = $value1['material_id'];
            $resData['description'] = $value1['description'];

            $resData['delivery_status'] = $value1['delivery_status'];

            $resData['order_quantity'] = $value1['order_quantity'];

            $resData['delivered_quantity'] = $value1['delivered_quantity'];
            $arrayData[] = $resData;

        }

        $resource_type = ('1');
        $materialNameListData = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();


        $resourceNameListData = [];
        foreach ($materialNameListData as $value) {

            $cat = [];
            $cat['id'] = $value['id'];
            $cat['name'] = $value['name'];
            $resourceNameListData[] = $cat;
        }
        $mrvStatusList = \app\models\SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 214])
            ->orderBy('root, lft')->asArray()->all();


        $mrvStatusData = [];
        $parent = "";
        foreach ($mrvStatusList as $d) {
            // pr($d);
            if ($d['lvl'] == 1) {
                $mrvStatusData[$d['id']] = $d;
                $parent = $d['id'];
            } else {
                $mrvStatusData[$parent]['sublevel'][] = $d;
            }
        }
        // pr($mrvSatusData);

        $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
        $unitType = [];
        foreach ($getUnitTypeList as $value) {
            $eTypeList = [];
            $eTypeList['id'] = $value['id'];
            $eTypeList['name'] = $value['name'];
            $unitType[] = $eTypeList;
        }

        return $this->render('ponumber_view', [
            'model' => $this->findModel($id),
            'purchaseDelivery' => $purchaseDelivery,
            'projectName' => $projectName,
            'supplier_name' => $supplier_name,
            'delivery_order_number' => $delivery_order_number,
            'delivery_date' => $delivery_date,
            'arrayData' => $arrayData,
            'resourceNameListData' => $resourceNameListData,
            'mrvStatusData' => $mrvStatusData,
            'po_number' => $po_number


        ]);
    }

    public function actionDelivery_date_view($id)
    {
        $DeliveryData = $this->findModel($id);
        // pr($purchaseDelivery);

        $poId = $DeliveryData['po_id'];
        $project_id = $DeliveryData['project_id'];

        $id = $_GET['id'];
        // pr($id);
        $purchaseDelivery = PurchaseDelivery::find()->where(['po_id' => $poId, 'project_id' => $project_id])->asArray()->all();
        // pr($purchaseDelivery);
        $projectName = '';
        $po_number = '';
        foreach ($purchaseDelivery as $poData) {
            $projectData = Project::find()->where(['id' => $poData['project_id']])->one();
            $purchaseData = Purchase::find()->where(['id' => $poData['po_id']])->one();
            $projectName = $projectData['project_name'];
            $po_number = $purchaseData['po_number'];
            $supplier_name = $poData['supplier_name'];
            $delivery_order_number = $poData['delivery_order_number'];
            $delivery_date = $poData['delivery_date'];
        }


        // pr($po_number);


        $arrayData = [];
        $purchaseDeliveryData = PurchaseDelivery::find()->where(['po_id' => $poId, 'project_id' => $project_id])->asArray()->all();

        foreach ($purchaseDeliveryData as $value1) {

            $resData = [];
            $resData['id'] = $value1['id'];
            $resData['material_id'] = $value1['material_id'];
            $resData['description'] = $value1['description'];
            $resData['delivery_date'] = $delivery_date;
            $resData['delivery_status'] = $value1['delivery_status'];


            $resData['order_quantity'] = $value1['order_quantity'];

            $DeliveryStatusData = PurchaseDeliveryStatus::find()->where(['purchase_delivery_id' => $id])->asArray()->all();

            foreach ($DeliveryStatusData as $value2) {
                $resData['received_date'] = $value2['received_date'];
                $resData['received_quantity'] = $value2['received_quantity'];
            }


            $arrayData[] = $resData;

        }

        $resource_type = ('1');
        $materialNameListData = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();


        $resourceNameListData = [];
        foreach ($materialNameListData as $value) {

            $cat = [];
            $cat['id'] = $value['id'];
            $cat['name'] = $value['name'];
            $resourceNameListData[] = $cat;
        }
        $mrvStatusList = \app\models\SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 214])
            ->orderBy('root, lft')->asArray()->all();


        $mrvStatusData = [];
        $parent = "";
        foreach ($mrvStatusList as $d) {
            // pr($d);
            if ($d['lvl'] == 1) {
                $mrvStatusData[$d['id']] = $d;
                $parent = $d['id'];
            } else {
                $mrvStatusData[$parent]['sublevel'][] = $d;
            }
        }
        // pr($mrvSatusData);

        $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
        $unitType = [];
        foreach ($getUnitTypeList as $value) {
            $eTypeList = [];
            $eTypeList['id'] = $value['id'];
            $eTypeList['name'] = $value['name'];
            $unitType[] = $eTypeList;
        }

        return $this->render('delivery_date_view', [
            'model' => $this->findModel($id),
            'purchaseDelivery' => $purchaseDelivery,
            'projectName' => $projectName,
            'supplier_name' => $supplier_name,
            'delivery_order_number' => $delivery_order_number,
            'delivery_date' => $delivery_date,
            'arrayData' => $arrayData,
            'resourceNameListData' => $resourceNameListData,
            'mrvStatusData' => $mrvStatusData,
            'po_number' => $po_number


        ]);
    }



    public function actionGetTotalQuantity()
    {
        $material_id = $_REQUEST['material_id'];

        $project_id = $_REQUEST['project_id'];

        $projectResourceMasterData = ProjectResource::find()->where(['material_id' => $material_id])->andwhere(['project_id' => $project_id])->one();
        // pr($projectResourceMasterData);
        if (!empty($projectResourceMasterData)) {
            echo $projectResourceMasterData['total_quantity'];
        } else {
            echo 0;
        }


    }


    public function actionGetPonumberList()
    {


        $project_id = $_REQUEST['project_id'];

        $POorderData = Purchase::find()->select(['po_number', 'id'])->where(['project_name' => $project_id])->asArray()->all();

        $POList = ArrayHelper::map($POorderData, 'id', 'po_number');
        // pr($POList);
        echo "<option value=''>Select PO Number</option>";
        if (count($POList) > 0) {
            foreach ($POList as $id => $po_number) {
                echo "<option value='$id'>$po_number</option>";
            }
        }
    }


    public function actionGetReceivedQuantity()
    {
        $deliveryId = $_REQUEST['deliveryId'];
        $received = $_REQUEST['received'];


        $receicedData = PurchaseDeliveryStatus::find()->where(['purchase_delivery_id' => $deliveryId])->asArray()->one();
        $existingReceived = $receicedData['received_quantity'] + $received;

        echo $existingReceived;
// pr($existingReceived);

    }


    public function actionGetPurchaseQuantity()
    {
        $material_id = $_REQUEST['material_id'];

        $project_id = $_REQUEST['project_id'];

        $projectResourceMasterData = PurchaseOrderDetails::find()->select(['order_quantity'])->where(['material_id' => $material_id, 'project_id' => $project_id]);

        $quantitySum = $projectResourceMasterData->sum('order_quantity');

        $quantity = isset($quantitySum) ? $quantitySum : 0;
        echo $quantity;
    }


    public function actionExportPdf($id)
    {
        $order_type = Yii::$app->request->get('order_type');

        $purchase = Purchase::find()->where(['id' => $id])->asArray()->one();
        // pr($purchase);
        $po_number = $purchase['po_number'];
        $reference = $purchase['reference'];
        $projectID = $purchase['project_name'];
        $Name = Project::find()->where(['id' => $projectID])->one();
        $projectName = $Name['project_name'];
        $supplierId = $purchase['supplier_name'];
        $supplierData = Customer::find()->where(['id' => $supplierId])->one();
        $supplierName = $supplierData['company_name'];
        $supplierAddress = $supplierData['address'];
        $supplierphone = $supplierData['phone_number'];
        $companyID = $purchase['company_name'];
        $companyData = Company::find()->where(['id' => $companyID])->one();
        $logo = $companyData['logo'];
        $companyName = $companyData['company_name'];
        //    pr($logo);
        $getdate = $purchase['date_created'];
        $date = Yii::$app->formatter->asDate($getdate, 'd-MMM-Y');
        // pr($date);
        if ($order_type == 1) {
            $query = Purchase::find()
                ->joinWith('purchase')
                ->where(['purchase_order.id' => $id])
                ->asArray()->one();

            $purchaseId = $query['id'];

            $productInfo = PurchaseOrderDetails::find()
                ->where(['purchase_order_details.purchase_id' => $purchaseId])
                ->asArray()->all();
            // pr($productInfo);
            $word = $query['notes'];


            $dataList = [];
            $totalAmount = 0;
            foreach ($productInfo as $value) {
                // pr($value);
                $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();


                foreach ($getActivityName as $value1) {
                    $dataVal = [];
                    // $resource = $value1['name'];
                    if (!empty($value['total_amount'])) {
                        $dataVal['material_id'] = $value1['name'];
                        $dataVal['description'] = $value['description'];
                        $dataVal['order_quantity'] = $value['order_quantity'];
                        $dataVal['price'] = $value['price'];
                        $dataVal['total_amount'] = $value['total_amount'];
                        $totalAmount += $value['total_amount'];

                    }
                    $dataList[] = $dataVal;

                }


            }
//   pr($dataList);
            $filename = $projectName . '.pdf';
            $total_amount = isset($purchase['total_amount']) ? $purchase['total_amount'] : 0;
            $subtotalAmount = isset($purchase['sub_total']) ? $purchase['sub_total'] : 0;
            $VatAmount = isset($purchase['vat']) ? $purchase['vat'] : 0;
            $Discount = isset($purchase['discount']) ? $purchase['discount'] : 0;


            $homeurl = Yii::$app->params['domain_url'];
            // pr($companyID);
            $content = $this->renderPartial('pdf', [
                'homeurl' => $homeurl,
                'productInfo' => $dataList,
                'word' => $word,
                'total_amount' => $total_amount,
                'subtotalAmount' => $subtotalAmount,
                'VatAmount' => $VatAmount,
                'projectName' => $projectName,
                'supplierName' => $supplierName,
                'logo' => $logo,
                'supplierAddress' => $supplierAddress,
                'supplierphone' => $supplierphone,
                'reference' => $reference,
                'purchase' => $purchase,
                'Discount' => $Discount

            ]);

        } else if ($order_type == 2) {
            $query = Purchase::find()
                ->joinWith('purchase')
                ->where(['purchase_order.id' => $id])
                ->asArray()->one();

            $purchaseId = $query['id'];


            $productInfo = PurchaseOrderDetails::find()
                ->where(['purchase_order_details.purchase_id' => $purchaseId])
                ->asArray()->all();
            // pr($productInfo);
            $requestId = '';

            foreach ($productInfo as $request_id) {
                $requestId = $request_id['request_id'];

            }

            //  pr($requestId);
            // $requestId = $productInfo['request_id'];
            $word = $query['notes'];


            $dataList = [];
            $totalAmount = 0;
            $arrayData = [];
            $purchaseRequestData = PurchaseRequestDetails::find()
                ->where(['request_order_id' => $requestId])
                ->asArray()->all();
            foreach ($purchaseRequestData as $value1) {
                $resData = [];
                $resData['total_quantity'] = $value1['material_total_quantity'];
                $resData['purchased_quantity'] = $value1['material_received_quantity'];
                $arrayData[] = $resData;
            }

// pr($arrayData);
            foreach ($arrayData as $arrayValue) {
                foreach ($productInfo as $value) {

                    $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();
                    foreach ($getActivityName as $value1) {

                        $dataVal = [];

                        if (!empty($value['total_amount'])) {
                            $dataVal['material_id'] = $value1['name'];
                            $dataVal['description'] = $value['description'];
                            $dataVal['total_quantity'] = $arrayValue['total_quantity'];
                            $dataVal['purchased_quantity'] = $arrayValue['purchased_quantity'];
                            $dataVal['order_quantity'] = $value['order_quantity'];

                            $dataVal['price'] = $value['price'];
                            $dataVal['total_amount'] = $value['total_amount'];
                            $totalAmount += $value['total_amount'];

                        }
                        $dataList[] = $dataVal;

                    }


                }
            }
            //    pr($dataList);
            $filename = $projectName . '.pdf';
            $total_amount = isset($purchase['total_amount']) ? $purchase['total_amount'] : 0;
            $subtotalAmount = isset($purchase['sub_total']) ? $purchase['sub_total'] : 0;
            $VatAmount = isset($purchase['vat']) ? $purchase['vat'] : 0;
            $Discount = isset($purchase['discount']) ? $purchase['discount'] : 0;


            $homeurl = Yii::$app->params['domain_url'];
            // pr($companyID);
            $content = $this->renderPartial('requestpdf', [
                'homeurl' => $homeurl,
                'productInfo' => $dataList,
                'word' => $word,
                'total_amount' => $total_amount,
                'subtotalAmount' => $subtotalAmount,
                'VatAmount' => $VatAmount,
                'Discount' => $Discount,
                'projectName' => $projectName,
                'supplierName' => $supplierName,
                'logo' => $logo,
                'supplierAddress' => $supplierAddress,
                'supplierphone' => $supplierphone,
                'reference' => $reference,
                'purchase' => $purchase

            ]);
        }
        $destination = Pdf::DEST_DOWNLOAD;

        // pr($content);
        $pdf = new Pdf([
            // 'mimeType' => 'application/pdf',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/pdf.css',
            // any css to be embedded if required
            'defaultFont' => 'Lato',
            'marginTop' => 40,
            'cssInline' => ' table,.new_head, .borderbox {font-size:13px !important; font-family:"Lato", sans-serif;} .text-bold{  font-family:"Lato", sans-serif;}',
            // 'cssInline' => 'table,.new_head, .borderbox {font-size:13px !important; font-family:"century";} .text-bold{  font-family:"century-b";}',
            // set mPDF properties on the fly
            'format' => Pdf::FORMAT_A4,
            'destination' => Pdf::DEST_DOWNLOAD,
            'filename' => $projectName . '.' . 'pdf',
            'options' => [
                'defaultheaderline' => 0,  //for header
            ],

            'methods' => [
                'SetHeader' => ("<div style='width:100%; text-align:center;'>
        <table class='table' style='margin-bottom: 0px'>
        <tr>
        <td width='50%'><img src='" . $homeurl . "/lib/img/company/" . $companyName . "/logo/" . $logo . "' width='110px'/></td>
        <td width='50%'><table class='table'><tr>
        <td align='right'>PO.No : </td><td>$po_number</td>
        </tr><tr>
        <td align='right'>Date : </td><td> $date</td>
        </tr></table></td>
        </tr>
        </table>
        <table class='table'>
          <tr>
                        <td class=\"text-center text-bold\" style=\"font-size: 20px;text-decoration:underline\" >
                              PURCHASE ORDER
                        </td>
                    </tr>
    </table>
    </div>"),
                'SetFooter' => ('
        <table> <tr>
    <td width="20%"  style="font-weight:bold; text-align:center"><span style=" color: white">&nbsp; Contenthide</span> P.O.Box - 123456, Tel: 04-123456, Fax: 04-12345, E-mail: info@jeyamsys.com</td>
    <td width="10%" style="text-align: right;font-weight:bold;">{PAGENO}</td>
    </tr>
    </table>')
            ],
        ]);

        return $pdf->render();


    }


    public function actionDeletes($id)
    {
        $this->findModel($id)->delete();
        PurchaseDelivery::deleteAll(['id' => $id]);
        PurchaseDeliveryStatus::deleteAll(['purchase_delivery_id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PurchaseDelivery::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}