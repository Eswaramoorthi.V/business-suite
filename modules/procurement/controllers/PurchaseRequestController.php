<?php

namespace app\modules\procurement\controllers;

use Yii;
use app\models\PurchaseRequestMaster;
use app\models\PurchaseRequestDetails;
use app\models\Search\PurchaseRequestMasterSearch;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\mpdf\Pdf;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\controllers\BaseController;
use app\modules\estimate\models\Settings;
use app\models\SettingsTree;
use app\models\EstimateDetails;
use app\models\EstimateMaster;
use app\models\MaterialMaster;
use app\models\MaterialUnitCost;
use app\models\LabourMaster;
use app\models\LabourUnitCost;
use app\modules\estimate\models\Equipment;
use app\models\ProjectEstimateMaster;
use app\models\ProjectEstimate;
use app\models\Tree;
use app\models\Project;
use app\components\MenuHelper;
use app\models\Customer;
use app\models\Employee;
use app\models\ProjectResource;
use app\models\PurchaseOrderDetails;
use app\models\Company;
use app\components\Procurement;
use yii\web\UploadedFile;
// use app\models\PurchaseMaster;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class PurchaseRequestController extends BaseController
{
    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {

        $route = $this->route;

        $actionList = MenuHelper::getActionHelper($route);
        // pr($actionList);
        $visibleList = MenuHelper::getActionVisibleHelper($route);

        $searchModel = new PurchaseRequestMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        //    pr($dataProvider);
        $dataProvider->pagination = ['PageSize' => 50];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionList' => $actionList,
            'visibleList' => $visibleList
        ]);

    }


    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionGetProjectType()
    {
        $project_id = $_REQUEST['project_id'];
        $projectTypeData = Project::find()->where(['id' => $project_id])->asArray()->one();

        $projectType = $projectTypeData['property_account_type'];

        $getAcName = SettingsTree::find()->where(['id' => $projectType])->asArray()->one();

        $Atype = $getAcName['name'];

        $accountType = strtolower($Atype);
        echo $accountType;
    }

    public function actionGetMaterialList()
    {


        $project_id = $_REQUEST['project_id'];
        $projectTypeData = Project::find()->where(['id' => $project_id])->asArray()->one();

        $projectAccountType = $projectTypeData['property_account_type'];
        // pr($projectAccountType);

        $getAcName = SettingsTree::find()->where(['id' => $projectAccountType])->asArray()->one();

        $Atype = $getAcName['name'];

        $accountType = strtolower($Atype);
// pr($accountType);
        if (($accountType == 'cost center')) {
            // pr("if");

            $itemType = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();

            $itemTypeList = [];
            foreach ($itemType as $value) {
                $itemTypeList[$value['id']] = $value['name'];
            }
            echo "<option value=''>Select</option>";
            foreach ($itemTypeList as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }
        } else {
            // pr("else");
            $resource_type = '1';
            $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->andwhere(['project_id' => $project_id])->groupBy('material_id')->asArray()->all();
            //  pr($projectResourceMasterData);
            $resourceNameListData = [];
            foreach ($projectResourceMasterData as $value) {

                $cat = [];
                $getActivityName = Tree::find()->where(['id' => $value['material_id']])->asArray()->all();
                foreach ($getActivityName as $value1) {
                    $resourceNameListData[$value['material_id']] = $value1['name'];
                    // $cat['name'] = $value1['name'];
                }
            }

            echo "<option value=''>Select</option>";
            foreach ($resourceNameListData as $key => $value) {
                echo "<option value='$key'>$value</option>";
            }
        }
    }

    public function actionGetProjectCode()
    {

        $getLastRecord = PurchaseRequestMaster::find()->orderBy(['id' => SORT_DESC])->asArray()->one();

        if (!empty($getLastRecord)) {
            $autoId = $getLastRecord['id'] + 1;
        } else {
            $autoId = 1;
        }

        $project_id = $_REQUEST['project_id'];

        $getProjectCode = Project::find()->where(['id' => $project_id])->one();

        echo $getProjectCode['project_code'] . '-R00' . $autoId;

    }

    public function actionGetTotalQuantity()
    {
        $material_id = $_REQUEST['material_id'];

        $project_id = $_REQUEST['project_id'];

        $projectResourceMasterData = ProjectResource::find()->where(['material_id' => $material_id])->andwhere(['project_id' => $project_id])->one();

        echo $projectResourceMasterData['total_quantity'];

    }

    public function actionGetPurchaseQuantity()
    {
        $material_id = $_REQUEST['material_id'];

        $project_id = $_REQUEST['project_id'];

        $projectResourceMasterData = PurchaseOrderDetails::find()->select(['quantity'])->where(['material_id' => $material_id, 'project_id' => $project_id]);

        $quantitySum = $projectResourceMasterData->sum('quantity');

        $quantity = isset($quantitySum) ? $quantitySum : 0;
        echo $quantity;
    }


    public function actionCreate()
    {

        $requestMaster = new PurchaseRequestMaster();
        $requestDetails = new PurchaseRequestDetails();

        if ($requestMaster->load(Yii::$app->request->post())) {

            // pr(Yii::$app->request->post());
            $postData = Yii::$app->request->post();
            // pr($postData);
            $requestMaster->project_id = $postData['project_id'];
            $requestMaster->reference_name = $postData['PurchaseRequestMaster']['reference_name'];

            $requestMaster->requested_date = date('Y-m-d', strtotime($postData['PurchaseRequestMaster']['requested_date']));

            $requestMaster->requested_by = $postData['PurchaseRequestMaster']['requested_by'];
            $requestMaster->request_status = 4;
            // $vatAmount = $subtotal *($vat/100);
            if (!$requestMaster->save()) {
                pr($requestMaster->getErrors());
            }
            $projectNameID = $postData['project_id'];
            $requestContainer = $this->requestReformat($postData['RequestDetails']);
//             pr($_FILES);
// pr($requestContainer);
            $reqDetailId = $requestMaster->id;
            if (!empty($requestContainer)) {

                foreach ($requestContainer as $requestDetailData) {

                    $requestDetails = new PurchaseRequestDetails();
                    $requestDetails->purchase_request_master_id = $reqDetailId;
                    $requestDetails->project_id = $projectNameID;
                    $requestDetails->material_id = $requestDetailData['material_id'];

                    $requestDetails->description = $requestDetailData['description'];
                    $requestDetails->remarks = $requestDetailData['remarks'];
                    $requestDetails->unit = $requestDetailData['unit'];
                    $requestDetails->mrv_status = $requestDetailData['mrv_status'];
                    $requestDetails->expected_date = date('Y-m-d', strtotime($requestDetailData['expected_date']));
                    $requestDetails->material_request_quantity = $requestDetailData['request_quantity'];
                    // pr($_FILES);
                    $check=array_filter($_FILES);
                    // pr($check);
                    $emptyCheck=count($check);
                  
                    if ($emptyCheck >0 ) {
                        for ($i = 0; $i < count($_FILES['RequestDetails']['tmp_name']['document']); $i++) {
                            $timeVal = time();
                            $file = $timeVal . '-' . $_FILES['RequestDetails']['name']['document'][$i];
            
                            $baseurl = Yii::getAlias('@app');
                            // pr($baseurl);
                            // $folder = '../web/observationimages/';
                            // $minifolder = '../web/observationimages/mini/';
                            $folder = $baseurl . '/web/mrv/';
                            // pr($folder);
                            $minifolder = $baseurl . '/web/mrv/attachment/';
                            $Savedfile_name = $file;
                            if (file_exists($folder)) {
                                // pr("idf");
                                // mkdir($baseurl . 'web/mrv/', 0777, true);
                                // mkdir($baseurl . 'web/mrv/attachment/', 0777, true);
                                $path = $minifolder . $Savedfile_name;
            
                            } else {
                                // pr("else");
                                mkdir($baseurl . '/web/mrv/', 0777, true);
                                mkdir($baseurl . '/web/mrv/attachment/', 0777, true);
                                $path = $minifolder . $Savedfile_name;
            
            
                            }
                            move_uploaded_file($_FILES["RequestDetails"]["tmp_name"]['document'][$i], $path);
            
                            $filesName[] = $Savedfile_name;

                            foreach($filesName as $value ){
                                $requestDetails->document = $value;
                            }
                           
                        }
            
                    }else{
                        $requestDetails->document ='';
                    }
                    if (!$requestDetails->save()) {
                        pr($requestDetails->getErrors());
                    }


                }


            }
            return $this->redirect(['index']);
        } else {

            $query = Project::find()->select(['project_name', 'id', 'project_code']);
            $projectListData = $query->asArray()->all();
            $assign_project = [];
            $projectList = ArrayHelper::map($projectListData, 'id', 'project_code');

            $requstedByNameList = [];
            $currentUser = Yii::$app->user->getId();
            // pr($currentUser);
            $employeeData = Employee::find()->where(['id' => $currentUser])->asArray()->one();
          $requestedByName=$employeeData['first_name'] . ' ' . $employeeData['last_name'];
           

            $mrvStatus = Procurement::getMRVStatus();

            $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();

            $unitType = ArrayHelper::map($getUnitTypeList, 'id', 'name');


            //    pr($mrvStatus);
            return $this->render('create', ['requestMaster' => $requestMaster,
                    'unitType' => $unitType,
                    'requestDetails' => $requestDetails,
                    'projectList' => $projectList,
                    'requestedByName' => $requestedByName,

                    'mrvStatus' => $mrvStatus
                ]
            );

        }
    }


    protected function requestReformat($requestDatas)
    {

        $requestDataKeysvalue = array_keys($requestDatas);
        $requestData = array_values($requestDatas);
        $requestTotalCount = count(array_values($requestData[0]));

        $requestContainer = [];
        for ($i = 0; $i < $requestTotalCount; $i++) {
            $requestTemp = [];
            foreach ($requestDataKeysvalue as $key) {
                $requestTemp[$key] = isset($requestDatas[$key][$i]) ? $requestDatas[$key][$i] : '';
            }
            $requestContainer[] = $requestTemp;
        }
        return $requestContainer;
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */


    public function actionUpdate($id)
    {
        $requestMaster = $this->findModel($id);
        $originalRequestIds = [];
        $projectId = '';
        foreach ($requestMaster->request as $requestDetailsData) {
            // pr($requestDetailsData);
            $originalRequestIds[$requestDetailsData['id']] = $requestDetailsData['id'];
            $projectId = $requestDetailsData['project_id'];
        }

        $resource_type = ('1');
        $materialNameListData = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();

        // $projectResourceMasterData = ProjectResource::find()->where(['resource_type' => $resource_type])->groupBy('material_id')->asArray()->all();
        $resourceNameListData = [];
        foreach ($materialNameListData as $value) {

            $cat = [];
            $cat['id'] = $value['id'];
            $cat['name'] = $value['name'];
            $resourceNameListData[] = $cat;
        }

        // pr($resourceNameListData);
        //  print_r($requestMaster);
        if ($requestMaster->load(Yii::$app->request->post())) {

            $request = Yii::$app->request->post();
            //   pr($request);
            $RequestData = $request['PurchaseRequestMaster'];

            $requestMaster->project_id = $request['PurchaseRequestMaster']['project_id'];
            $requestMaster->reference_name = $request['PurchaseRequestMaster']['reference_name'];

            $requestMaster->requested_date = date('Y-m-d', strtotime($request['PurchaseRequestMaster']['requested_date']));

            $requestMaster->requested_by = $request['PurchaseRequestMaster']['requested_by'];
            $requestMaster->request_status = 5;

            if (!$requestMaster->save()) {
                pr($requestMaster->getErrors());
            }
            $projectNameID = $request['PurchaseRequestMaster']['project_id'];
            $currentRequestIds = (array_flip($request['RequestDetails']['id']));

            $deletedIds = [];
            foreach ($originalRequestIds as $ids) {
                if (!array_key_exists($ids, $currentRequestIds)) {

                    $deletedIds[] = $ids;
                }
                if ($deletedIds) {

                    PurchaseRequestDetails::deleteAll(['id' => $deletedIds]);
                }
            }
            $requestContainer = $this->requestReformat($request['RequestDetails']);


            foreach ($requestContainer as $requestData) {

                if ($requestData['id'] == '') {
                    $requestDetails = new PurchaseRequestDetails();
                } else {
                    $requestDetails = PurchaseRequestDetails::findOne($requestData['id']);
                }
// pr($requestData);
                $requestDetails->purchase_request_master_id = $id;
                $requestDetails->project_id = $projectNameID;
                $requestDetails->material_id = $requestData['material_id'];
                $requestDetails->description = $requestData['description'];
                $requestDetails->remarks = $requestData['remarks'];
                $requestDetails->unit = $requestData['unit'];
                $requestDetails->mrv_status = $requestData['mrv_status'];
                $requestDetails->expected_date = date('Y-m-d', strtotime($requestData['expected_date']));
                // $requestDetails->material_total_quantity = $requestData['total_quantity'];
                $requestDetails->material_request_quantity = $requestData['request_quantity'];
                // $requestDetails->material_received_quantity = $requestData['purchased-quantity'];
                $requestDetails->document = $requestData['document'];
                // pr($_FILES);
                $check=array_filter($_FILES);
                   
                    $emptyCheck=count($check);
                  
                    if ($emptyCheck >0 ) {
                    for ($i = 0; $i < count($_FILES['RequestDetails']['tmp_name']['document']); $i++) {
                        $timeVal = time();
                        $file = $timeVal . '-' . $_FILES['RequestDetails']['name']['document'][$i];
        
                        $baseurl = Yii::getAlias('@app');
                        // pr($baseurl);
                        // $folder = '../web/observationimages/';
                        // $minifolder = '../web/observationimages/mini/';
                        $folder = $baseurl . '/web/mrv/';
                        // pr($folder);
                        $minifolder = $baseurl . '/web/mrv/attachment/';
                        $Savedfile_name = $file;
                        if (file_exists($folder)) {
                            // pr("idf");
                            // mkdir($baseurl . 'web/mrv/', 0777, true);
                            // mkdir($baseurl . 'web/mrv/attachment/', 0777, true);
                            $path = $minifolder . $Savedfile_name;
        
                        } else {
                            // pr("else");
                            mkdir($baseurl . '/web/mrv/', 0777, true);
                            mkdir($baseurl . '/web/mrv/attachment/', 0777, true);
                            $path = $minifolder . $Savedfile_name;
        
        
                        }
                        move_uploaded_file($_FILES["RequestDetails"]["tmp_name"]['document'][$i], $path);
        
                        $filesName[] = $Savedfile_name;

                        foreach($filesName as $value ){
                            $requestDetails->document = $value;
                        }
                       
                    }
        
                }
                if (!$requestDetails->save()) {
                    pr($requestDetails->getErrors());
                }
                // pr($requestDetails);

            }

            // pr($invoiceContainer);

            return $this->redirect(['index']);
        } else {
            $projectListData = Project::find()->select(['project_name', 'id'])->asArray()->all();

            $assign_project = [];

            foreach ($projectListData as $value) {
                $eType = [];
                $eType['id'] = $value['id'];
                $eType['project_name'] = $value['project_name'];
                $assign_project[] = $eType;
            }
            $projectList = ArrayHelper::map($assign_project, 'id', 'project_name');

            $projectData = Project::find()->where(['id' => $requestMaster['project_id']])->one();

            $requstedByNameList = [];
            $currentUser = Yii::$app->user->getId();
            // pr($currentUser);
            $employeeData = Employee::find()->where(['id' => $currentUser])->asArray()->one();
          $requestedByName=$employeeData['first_name'] . ' ' . $employeeData['last_name'];


            $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
            $unitType = [];
            foreach ($getUnitTypeList as $value) {
                $eTypeList = [];
                $eTypeList['id'] = $value['id'];
                $eTypeList['name'] = $value['name'];
                $unitType[] = $eTypeList;
            }
            $mrvStatusList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 214])
                ->orderBy('root, lft')->asArray()->all();

            $mrvStatus = [];
            $parent = "";
            foreach ($mrvStatusList as $d) {
// pr($d);
                if ($d['lvl'] == 1) {
                    $mrvStatus[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $mrvStatus[$parent]['sublevel'][] = $d;
                }
            }

            return $this->render('update', [

                    'requestMaster' => $requestMaster,
                    'projectList' => $projectList,
                    'resourceNameListData' => $resourceNameListData,
                    'projectData' => $projectData,
                    'requestedByName' => $requestedByName,
                    'unitType' => $unitType,
                    'mrvStatus' => $mrvStatus

                ]
            );

        }
    }


    public function actionDeletes($id)
    {
        $this->findModel($id)->delete();
        PurchaseRequestDetails::deleteAll(['purchase_request_master_id' => $id]);

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        // pr($id);
        if (($model = PurchaseRequestMaster::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDetailview($id)
    {
        $id = $_GET['id'];
        // pr($id);
        $purchaseRequest = PurchaseRequestMaster::find()->where(['id' => $id])->asArray()->one();
        $projectData = Project::find()->where(['id' => $purchaseRequest['project_id']])->one();
        $projectName = $projectData['project_name'];
        $referenceName = $purchaseRequest['reference_name'];
        $requestedDate = $purchaseRequest['requested_date'];
        $employeeData = Employee::find()->where(['id' => $purchaseRequest['requested_by']])->asArray()->one();
        // pr($employeeData);
        $requestedBy = $employeeData['first_name'] . '' . $employeeData['last_name'];
        // pr($requestedBy);


        $arrayData = [];


        $purchaseRequestData = PurchaseRequestDetails::find()
            ->where(['purchase_request_master_id' => $id])
            ->asArray()->all();
        foreach ($purchaseRequestData as $value1) {
            $projectResourceMasterData = ProjectResource::find()->where(['material_id' => $value1['material_id']])->asArray()->one();
            $totalQuantity = (!empty($projectResourceMasterData['total_quantity'])) ? $projectResourceMasterData['total_quantity'] : 0;
            // pr($totalQuantity);
            $resData = [];
            $resData['id'] = $value1['id'];
            $resData['material_id'] = $value1['material_id'];
            $resData['description'] = $value1['description'];
            $resData['remarks'] = $value1['remarks'];
            $resData['mrv_status'] = $value1['mrv_status'];
            $resData['unit'] = $value1['unit'];
            $resData['expected_date'] = $value1['expected_date'];
            $resData['material_request_quantity'] = $value1['material_request_quantity'];

            $resData['material_total_quantity'] = $totalQuantity;
            $arrayData[] = $resData;

        }

        $resource_type = ('1');
        $materialNameListData = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();


        $resourceNameListData = [];
        foreach ($materialNameListData as $value) {

            $cat = [];
            $cat['id'] = $value['id'];
            $cat['name'] = $value['name'];
            $resourceNameListData[] = $cat;
        }
        $mrvStatusList = \app\models\SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 214])
            ->orderBy('root, lft')->asArray()->all();


        $mrvStatus=Procurement::getMRVStatus();
        //pr($mrvStatus);

        $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
        $unitType = ArrayHelper::map($getUnitTypeList, 'id', 'name');


        return $this->render('detailview', [
            'model' => $this->findModel($id),
            'purchaseRequest' => $purchaseRequest,
            'projectName' => $projectName,
            'referenceName' => $referenceName,
            'requestedDate' => $requestedDate,
            'requestedBy' => $requestedBy,
            'arrayData' => $arrayData,
            'resourceNameListData' => $resourceNameListData,
            'unitType' => $unitType,
            'mrvSatusData' => $mrvStatus
        ]);
    }

}