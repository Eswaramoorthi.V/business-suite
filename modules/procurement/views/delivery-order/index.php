

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use app\models\Project;
use app\models\Purchase;
use app\models\Customer;
use yii\helpers\ArrayHelper;
use app\models\Company;
$this->title = Yii::t('user', 'GRN');
$this->params['breadcrumbs'][] = 'GRN';
//$this->params['title_icon'] = 'fa-users';
?>

<?php Pjax::begin() ?>
<div class="purchase">
<div class="col-md-6 text-left add-label" style="font-size: 30px">
      
            <i class="fa fa-product-hunt" aria-hidden="true"></i> GRN (Goods Receipt Note)
    </div>
<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'project_id',
                'header' => '<div style="width:100px;">Project  Name</div>',
                'value' => function($model, $key, $index) {
                    $projectListData =Project::find()->select(['project_name','id'])->asArray()->all();

                    $assign_project = [];
                    
                    foreach ($projectListData as $value) {
                        $eType =[];
                        $eType['id'] = $value['id'];
                        $eType['project_name'] = $value['project_name'];
                        $assign_project[]=$eType;
                    }
                    $projectList = ArrayHelper::map($assign_project, 'id', 'project_name');
                    return !empty($projectList[$model['project_id']])?$projectList[$model['project_id']]:'-';
                       
                    },
                
            ],
            [
                'attribute' => 'po_number',
                'header' => '<div style="width:120px;">PO  Number</div>',
                'value' => function($model, $key, $index) {
                    $poListData = Purchase::find()->select(['po_number', 'id'])->asArray()->all();
                // pr($poListData);
                $poDataList = [];
                foreach ($poListData as $value) {
    
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['po_number'] = $value['po_number'];
                    $poDataList[] = $eType;
                }
                
                $poList = ArrayHelper::map($poDataList, 'id', 'po_number');
                    // return !empty($poList[$model['po_id']])?$poList[$model['po_id']]:'-';
                    return Html::a($poList[$model['po_id']], ['/delivery/delivery/ponumber_view', 'id' => $model->id]);
                    },
                    'vAlign' => 'middle',
                'format' => 'raw',
                'noWrap' => true
               
            ],
            [
                'attribute' => 'supplier_name',
                'header' => '<div style="width:120px;">Supplier Name</div>',
                'value' => function($model, $key, $index) {
                    $customerData= Customer::find()->asArray()->all();
                // pr($poListData);
                $supplierDataList = [];
                foreach ($customerData as $value) {
    
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['company_name'] = $value['company_name'];
                    $supplierDataList[] = $eType;
                }
                
                $supplierList = ArrayHelper::map($supplierDataList, 'id', 'company_name');
                    // return !empty($poList[$model['po_id']])?$poList[$model['po_id']]:'-';
                    return ($supplierList[isset($model['supplier_name']) ? $model['supplier_name'] :'-' ]);
                    },
                    'vAlign' => 'middle',
                'format' => 'raw',
                'noWrap' => true
               
            
            ],
            [
                'attribute' => 'delivery_date',
                'header' => '<div style="width:180px;">Delivery Date</div>',
                 'value' => function($model, $key, $index) {
               return Html::a(Yii::$app->formatter->asDate($model->delivery_date, 'php:d-m-Y'),['/delivery/delivery/delivery_date_view', 'id' => $model->id]);
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ]),
                'vAlign' => 'middle',
                'format' => 'raw',
                'noWrap' => true
            ],
            

            
            [
                
                'class' => '\kartik\grid\ActionColumn',
                'mergeHeader' => false,
                    'contentOptions' => ['style' => 'width:50px;text-align:center','class'=>'skip-export'],
                    'headerOptions' =>['class'=>'skip-export'],
                    'header'=>"Actions",
                'template'=>$actionList,
                'dropdown' => true,
                'dropdownOptions'=>['class'=>'pull-right' ],
                'options' => ['style' => 'max-width:20px;'],
                'buttons' => [
                   
                    'update' => function($url, $model) {
                        return Html::a('Update',  $url,['class' => 'action-btn dropdown-item ', 'title' => 'Update']
                        );
                    },
                    'deletes' => function($url) {
                       return Html::a('Delete',   $url,['class' => 'action-btn dropdown-item','data-method'=>'post', 'data-pjax'=>0,'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?']]
                       );
                   },
                   
                    ]
                
                ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'pjax' => false, // pjax is set to always true for this demo
       
            'toolbar' => [
                [
                    'content'=>

                        Html::a(Yii::t('app', ' {modelClass}', [
                            'modelClass' => '<i class="glyphicon glyphicon-bell icons"></i>GRN',
                        ]), ['create'], ['class' => 'btn btn-info btn-rounded', ]),

                ],

            ],

            
            //'showPageSummary' => true,
            'panel' => [

                // 'type' => GridView::TYPE_DEFAULT,
                'heading' => false,
            ],
            'persistResize' => true,

    ]);
}
?>                                   
</div>
<?php Pjax::end() ?>

<style>
a.action-btn.dropdown-item {
  display: block;
  width: 100%;
  padding: .25rem 1.5rem;
  clear: both;
  font-weight: normal;
  text-align: inherit;
  white-space: nowrap;
  background-color: transparent;
  border: 0;
  color: navy;
  font-size: small;
}
</style>