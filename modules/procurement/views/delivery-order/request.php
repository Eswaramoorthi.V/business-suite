<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
//  $purchaseMaster->date_created = date('d-m-Y', strtotime($purchaseMaster['date_created']));
//  $purchaseMaster->due_date = date('d-m-Y', strtotime($purchaseMaster['due_date']));
 

$vatcategory=\Yii::$app->params['VATRate'];

$currentUser=Yii::$app->user->getId();

$fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

$payment_list= [1=>'Payment after 30 days',2=>'Payment after 90 days'];

$role = current($fetchRole);
$roleName = $role->name;
$this->title = 'Update Purchase Order';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/purchase/purchase/index']];
$this->params['breadcrumbs'][] = $this->title;
// pr($purchaseMaster);
$purchaseStatus= [1=>'Purchase Created',2=>'Purchase Issued',3=>'Waiting for delivery',4=>'Delivered'];
// pr($arrayData);
?>

<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
       <div class="col-md-12 "><legend class="text-info"><small>Material</small></legend></div>
<div class ="row">
<div class="col-md-3" style="font-weight:bold">Material</div>
    <div class="col-md-2" style="font-weight:bold">Description</div>
    <div class="col-md-2" style="font-weight:bold">Delivery Status</div>
    <div class="col-md-1" style="font-weight:bold">Order Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Delivered Quantity</div>
    <div class="col-md-1" style="font-weight:bold"> Received Quantity</div>
   <!-- <div class="col-md-1" style="font-weight:bold">&nbsp;</div> -->
</div>
<div id="product">

<?php foreach ($arrayData as $request) { ?>



<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-3"><select  id="purchase-delivery-material-0<?php echo $request['id']; ?>" class="form-control required material" name="DeliveryDetails[material_id][]"  >
    
    <option value="">Please select material</option>
        <?php foreach ($resourceNameListData as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$request['material_id']){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
</select>
    
                                     
    </div>
    <div class="col-md-2"><textarea data-row ="<?php echo $request['id']; ?>"   id="purchase-delivery-description" class="form-control  description" name="DeliveryDetails[description][]"><?php echo $request['description'];?></textarea></div>
    <div class="col-md-2"><select data-row ="<?php echo $request['id']; ?>" id="purchase-delivery-deliverystatus-<?php echo $request['id']; ?>" class="form-control  deliverystatus" name="DeliveryDetails[delivery_status][]" onchange="calc(this.id);">
    <!-- <option value="">select</option> -->
                        <?php
                        $selected="";
                        $mrvStatus = [];
                        foreach ($mrvSatusData as $value) {
                            $pType = [];
                            $pType['id'] = $value['id'];
                            if($value['id']==$request['delivery_status']){
                                $selected="selected='selected'";
                            }
                            $pType = $value['name'];
                            $mrvStatus = $pType;
                            ?>
                            <option <?php echo $selected;?> value="<?php echo $value['id']; ?>"><?php echo($mrvStatus); ?></option>
                        <?php } ?>
   
    </select></div>
    
<div class="col-md-1"><input type="text" value="<?php echo $request['order_quantity']; ?>"data-row ="<?php echo $request['id']; ?>" id="order-quantity-<?php echo $request['id']; ?>" class="form-control order-qnty" name="DeliveryDetails[order_quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" value="<?php echo $request['delivered_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="delivered-quantity-<?php echo $request['id']; ?>" class="form-control delivered-qnty" name="DeliveryDetails[delivered_quantity][]"onchange="calc(this.id);" >
    <h6 class="error-msg hide text-danger"> </h6>
    </div>
    <div class="col-md-1"><input type="text" value="0" data-row ="<?php echo $request['id']; ?>" id="received-quantity-<?php echo $request['id']; ?>" class="form-control received-qnty" name="DeliveryDetails[received_quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input    type="hidden" value=<?php echo $request['id']; ?> data-row ="<?php echo $request['id']; ?>" id="podetails-<?php echo $request['id']; ?>" class="form-control qnty" name="DeliveryDetails[podetails][]" onchange="calc(this.id);" ></div>
</div>
</div>

<div class="row">&nbsp;</div> 
<?php } ?>
</div>

<div>&nbsp;</div>

<div class="col-md-12"><legend class="text-info"><small></small></legend></div>
<div class="col-md-12"> 
    <div class="form-group">
        <div class="col-md-12 col-xs-12">

        </div>
    </div>
</div>
</div>
</div>

 </div>



<script>
 function calc(id) {

    $(function(){
        var idPosition = id.split('-');

        $("#received-quantity-"+idPosition[2]).on("blur", function () {
        // alert();
    var received=$("#received-quantity-"+idPosition[2]).val();
    var order=$("#order-quantity-"+idPosition[2]).val();

if(Number(received) == Number(order)){
    $("#purchase-delivery-deliverystatus-" +idPosition[2]).val(218);
}

           
  });



$("#delivered-quantity-"+idPosition[2]).on("blur", function () {
    var delivered=$("#delivered-quantity-"+idPosition[2]).val();
    
    var order=$("#order-quantity-"+idPosition[2]).val();
 
    
    if (Number(delivered) > Number(order)) {
alert('Greater than deliverd quantity');
}else if(Number(delivered) <= Number(order)){
       
        $("#purchase-delivery-deliverystatus-" +idPosition[2]).val(217);
        }
  });

  $("#received-quantity-"+idPosition[2]).on("blur", function () {
    var received=$("#received-quantity-"+idPosition[2]).val();
    // var received=$("#received-quantity-"+idPosition[2]).val();
    var order=$("#order-quantity-"+idPosition[2]).val();
 
    
    if (Number(received) > Number(order)) {
        alert('Greater than received quantity');
    }else if(Number(received) < Number(order)){
        // alert('ff');
        $("#purchase-delivery-deliverystatus-" +idPosition[2]).val(217);
  }
  });




});


 }

$(function () {
        $('#mrv').on('change',function (){

            var mrv = $(this).val();
            $.ajax({
                url: './get-total-material-data',
                data: {'mrv':mrv},
                type: 'post',
                success: function (data) {
                    // console.log('data',data);
                    $('#product').empty().append(data);

                }
            });

        });
    });

    </script>
    

 
  