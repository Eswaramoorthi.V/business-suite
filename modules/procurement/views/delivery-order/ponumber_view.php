<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

 

$this->title = 'Delivery Order';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/purchase/purchase/index']];
$this->params['breadcrumbs'][] = $this->title;
// pr($unitType);
// $project_id=$_GET;
// pr($project_id);

?>

<?php $home = Yii::$app->params['domain_url']; ?>


<div class="purchase-form form-vertical">
<div class ="row">

</div>
<div class="table-responsive">

<table class="table">
                <colgroup>
                    <col style="width:15%">
                    <col style="width:15%">
                    <col style="width:15%">
                    <col style="width:15%">
                </colgroup>
                <tbody><tr>

                <th> Project Name</th>
                    <td>
                        <?php echo $projectName?>
                    </td>
                    <th> PO Number</th>
                    <td>
                        <?php echo $po_number?>
                    </td>
                    
                     </tr>
                    
                <tr>
                <th> Supplier Name</th>
                    <td>
                    <?php echo $supplier_name?>
                    </td>
                    <th> Delivery Order Number</th>
                    <td>
                    <?php echo $delivery_order_number?>
                    </td>
                    

                </tr>
                <tr>
                <th> Delivery Date</th>
                    <td>
                    <?php echo $delivery_date?>
                    </td>
                  
                    

                </tr>
               </tbody></table>

 

</div>
<div>&nbsp</div>
<div>&nbsp</div>
<div>&nbsp</div>
<?php $form = ActiveForm::begin(['id'=>'purchase-delivery-form','method'=>'post','action'=>'delivery?id='.$_GET['id']]); ?>


       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
      
<div class ="row">
<div class="col-md-4" style="font-weight:bold">Material</div>
        <div class="col-md-3" style="font-weight:bold">Description</div>
        <div class="col-md-2" style="font-weight:bold">Delivery Status</div>
    <div class="col-md-1" id="total" style="font-weight:bold">Order Quantity</div>
      <div class="col-md-1" id="request" style="font-weight:bold">Delivered Quantity</div>
  
</div>
<div id="product">
<?php foreach ($arrayData as $request) { ?>
<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-4"><select  disabled ="disabled" id="purchase-delivery-material-0<?php echo $request['id']; ?>" class="form-control required material" name="PurchaseDelivery[material_id][]"  >
    
    <option value="">Please select material</option>
        <?php foreach ($resourceNameListData as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$request['material_id']){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
</select>
    
                                     
    </div>
    <div class="col-md-3"><textarea  readonly="true"  data-row ="<?php echo $request['id']; ?>"   id="purchase-delivery-description" class="form-control  description" name="PurchaseDelivery[description][]"><?php echo $request['description'];?></textarea></div>
    
    <div class="col-md-2"><select  disabled ="disabled" data-row ="<?php echo $request['id']; ?>" id="purchase-delivery-materialstatus" class="form-control  materialstatus" name="PurchaseDelivery[material_status][]">
    
    <option value="">Please select status</option>
        <?php foreach ($mrvStatusData as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$request['delivery_status']){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
    </select></div>
    <div class="col-md-1"><input  readonly="true"  type="text" value="<?php echo $request['order_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="total-quantity-<?php echo $request['id']; ?>" class="form-control totalqnty" name="PurchaseDelivery[total_quantity][]" ></div>
    <div class="col-md-1"><input   readonly="true" type="text" value="<?php echo $request['delivered_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="request-quantity-<?php echo $request['id']; ?>" class="form-control requestqnty" name="PurchaseDelivery[request_quantity][]" ></div>
    
    
    


    
</div>
</div>

<div class="row">&nbsp;</div> 
<?php } ?>
</div>

<div>&nbsp;</div>


    </div>
</div>
<?php ActiveForm::end(); ?>
 </div>

