<?php


use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\web\View;
use app\models\Purchase;
use app\models\DeliveryOrder;

use kartik\depdrop\DepDrop;


$vatcategory = \Yii::$app->params['VATRate'];
$currentUser = Yii::$app->user->getId();

$fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

$OrderType = [1 => 'Direct', 2 => 'MRV'];
$role = current($fetchRole);
$roleName = $role->name;
// pr($roleName);
$purchaseStatus = [1 => 'Purchase Created', 2 => 'Purchase Issued', 3 => 'Waiting for delivery', 4 => 'Delivered'];
$getLastRecord = DeliveryOrder::find()->orderBy(['id' => SORT_DESC])->asArray()->one();

// pr($getLastRecord);
if (!empty($getLastRecord)) {
    $DOnumber = $getLastRecord['id'] + 1;
} else {
    $DOnumber = 1;
}
// pr($arrayData);
?>
<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
    <?php $form = ActiveForm::begin(['id' => 'purchase-delivery-form']); ?>
    <div class="row hide" id="error-message">
        <div class="col-md-12 warning"></div>
    </div>
    <div class="row">
        <div class="row">


            <div class="col-md-3">

                <label> Project Code</label>
                <?php

                echo Select2::widget([
                    'model' => $delivery,
                    'name' => 'project_id',
                    'id' => 'project_id',
                    'data' => $projectList,
                    'theme' => Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'Select Project ...', 'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);

                ?>
            </div>

            <div class="col-md-3">


                <?php
                echo $form->field($delivery, 'supplier_name')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => [],
                    'options' => ['id' => 'supplier_name', 'placeholder' => 'Select ...'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],

                    'pluginOptions' => [
                        'depends' => ['project_id'],
                        'url' => Url::to(['suplier-list']),
                        'params' => ['input-type-1', 'input-type-2']
                    ]
                ]);
                ?>


            </div>

            <div class="col-md-3">

                <?php
                echo $form->field($delivery, 'po_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => [],
                    'options' => ['id' => 'po_id', 'placeholder' => 'Select ...'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['supplier_name'],
                        'url' => Url::to(['po-list']),
                        'params' => ['project_id', 'supplier_name']
                    ]
                ]);
                ?>

            </div>

            <div class="col-md-3">
                <?= $form->field($delivery, 'originator')->textInput(['value' => $roleName, 'readonly' => true])->label('Originator') ?>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3">

                <?= $form->field($delivery, 'delivery_order_number')->textInput(['options'=>['id'=>'delivery_order_number']])->label('Delivery Order Number') ?>
            </div>
            <div class="col-md-3">
                <?=
                $form->field($delivery, 'delivery_date', [
                    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
                ])->textInput([])->label(' Delivery Date')
                ?>


            </div>
            <div class="col-md-3">

                <?php
                echo $form->field($delivery, 'lpo_status')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => [],
                    'options' => ['id' => 'lpo_status', 'placeholder' => 'Select ...'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['po_id'],
                        'url' => Url::to(['lpo-status']),
                        'params' => ['po_id'],

                    ]
                ]);
                ?>
            </div>


            <!-- <div class="col-md-4"> -->
            <div class="col-md-3">

                <?php
                echo $form->field($delivery, 'currency')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => [],
                    'options' => ['id' => 'currency', 'placeholder' => 'Select ...'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['po_id'],
                        'url' => Url::to(['currency']),
                        'params' => ['po_id'],

                    ]
                ]);
                ?>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3">

                <?php


                echo $form->field($delivery, 'payment_terms')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => [],
                    'options' => ['id' => 'payment_terms', 'placeholder' => 'Select ...'],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['po_id'],
                        'url' => Url::to(['payment-terms']),
                        'params' => ['po_id'],

                    ]
                ]);

                ?>

            </div>
            <div class="col-md-3">

                <?= $form->field($deliveryNumber, 'supplier_delivery_number')->textInput(['id' => 'sd_number'])->label('Supplier Delivery Number') ?>
            </div>
        </div>


    </div>
    <br>

</div>
<br>


<div class="row hide" id="headingdiv">
    <div class="col-md-3" style="font-weight:bold">Material</div>
    <div class="col-md-2" style="font-weight:bold">Description</div>
    <div class="col-md-2" style="font-weight:bold">Delivery Status</div>
    <div class="col-md-1" style="font-weight:bold">Order Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Delivered Quantity</div>
    <div class="col-md-1" style="font-weight:bold"> Received Quantity</div>
    <div class="col-md-1" style="font-weight:bold">&nbsp;</div>
</div>
<div id="product">
    <div class="col-md-12">
        <legend class="text-info"><small>Material</small></legend>
    </div>
    <div class="row copyinvitems details after-add-more">
        <div class="control-group">
            <div class="col-md-3"><select data-row="0" id="purchase-delivery-material"
                                          class="form-control required material" name="DeliveryDetails[material_id][]">
                    <option value="">Please select material</option>

                </select></div>
            <div class="col-md-2"><textarea id="purchase-delivery-description" class="form-control  descrip"
                                            name="DeliveryDetails[description][]"></textarea></div>
            <div class="col-md-1">

                <?php
                $mrvStatusVal = 1;
                $options['id'] = 'purchase-master-mrvstatus';
                $options['disabled'] = 'disabled';
                $options['class'] = 'dropdownclass';
                $options['data-row'] = 0;

                echo Html::dropDownList('PurchaseMaster[mrv_status][]', $mrvStatusVal, $mrvStatus, $options); ?>

            </div>
            <div class="col-md-1"><input type="text" value="0" data-row="0" id="order-quantity-0"
                                         class="form-control orderqnty" name="DeliveryDetails[order_quantity][]"></div>
            <div class="col-md-1"><input type="text" value="0" data-row="0" id="delivered-quantity-0"
                                         class="form-control deliveredqnty"
                                         name="DeliveryDetails[delivered_quantity][]"></div>
            <div class="col-md-1"><input type="text" value="0" data-row="0" id="received-quantity-0"
                                         class="form-control receivedqnty" name="DeliveryDetails[received_quantity][]">
            </div>


        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-12 col-xs-12">
                <?= Html::submitButton($delivery->isNewRecord ? 'Submit' : 'Update', ['class' => $delivery->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ', 'data-dismiss' => 'modal']) ?>
            </div>
        </div>
    </div>
</div>
</div>


<div class="clearfix"></div>
</div>

<?php ActiveForm::end(); ?>
</div>

<?= $this->registerJs("
    
    $('#purchasedelivery-delivery_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });
      
       $(\"form\").submit(function() {
        $(\".dropdownclass\").prop(\"disabled\", false);
    });
    
     $(function () {
        $('#po_id').on('change',function (){
            var po_number = $(this).val();
           
            var optionSelected = $('#po_id').find(\":selected\").text();
             
            
            
            $.ajax({
                url: './get-delivery-data',
                data: {'po_number':po_number},
                type: 'post',
                success: function (data) {
                    $('#product').empty().append(data).appendTo('#purchase-delivery-form');
                    
                }
            });
             $.ajax({
               url: './delivery-order-number',
                data: {'po_number':po_number,'po_value':optionSelected},
                type: 'post',
                success: function (d) {
                console.log(optionSelected);
                   
                   $('#purchasedelivery-delivery_order_number').val(d);
                    
                }
            });
        });
    });

   

 
   

    ", View::POS_READY); ?>



   

 
 