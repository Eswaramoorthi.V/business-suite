<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use kartik\select2\Select2;
use app\models\PurchaseRequestDetails;
use app\models\Purchase;
use app\models\DeliveryOrder;
//  $delivery->delivery_date = date('d-m-Y', strtotime($delivery['delivery_date']));
use kartik\depdrop\DepDrop;
 $this->title = 'Update Delivery Status';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/delivery/delivery/index']];
$this->params['breadcrumbs'][] = $this->title;
$currentUser=Yii::$app->user->getId();

$fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

$payment_list= [1=>'Payment after 30 days',2=>'Payment after 90 days'];

$role = current($fetchRole);
$roleName = $role->name;
$purchaseStatus= [1=>'Purchase Created',2=>'Purchase Issued',3=>'Waiting for delivery',4=>'Delivered'];
$getLastRecord=DeliveryOrder::find()->orderBy(['id' => SORT_DESC])->asArray()->one();

// pr($purchaseDelivery);
if(!empty($getLastRecord)){
    $DOnumber=$getLastRecord['id']+1;
}else{
    $DOnumber=1;
}
// pr($paymentTerms);
?>

<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
    <?php $form = ActiveForm::begin(['id'=>'purchase-delivery-form']); ?>
       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
         
         <div class="row">
         <div class="col-md-3">

                <label> Project Code</label>
                <?php

                echo Select2::widget([
                    'model' => $purchaseDelivery,
                    'name' => 'project_id',
                    'id' => 'project_id',
                    'value'=>$purchaseDelivery->project_id,
                    'data' => $projectList,
                    'theme' => Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'Select Project ...', 'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);

                ?>
            </div>
    <div class="col-md-3">
    <?php
                echo $form->field($purchaseDelivery, 'supplier_name')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => $supplierData,
                    'options' => ['id' => 'supplier_name', 'placeholder' => 'Select ...','value'=>$purchaseDelivery->supplier_name],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],

                    'pluginOptions' => [
                        'depends' => ['project_id'],
                        'url' => Url::to(['suplier-list']),
                        'params' => ['input-type-1', 'input-type-2']
                    ]
                ]);
                ?>
       
  
 </div>
       
    <div class="col-md-3" >
    <?php
                echo $form->field($purchaseDelivery, 'po_id')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => $poList,
                    'options' => ['id' => 'po_number', 'placeholder' => 'Select ...', 'value'=>$purchaseDelivery->po_id,],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['supplier_name'],
                        'url' => Url::to(['po-list']),
                        'params' => ['project_id', 'supplier_name']
                    ]
                ]);
                ?>
         </div>
            <div class="col-md-3">
 <?= $form->field($purchaseDelivery, 'originator')->textInput(['value'=>$roleName,'readonly'=>true])->label('Originator') ?>
     
    </div>
    </div>
    <br>
    <div class="row">
    
    <div class="col-md-3">
     
     <?= $form->field($purchaseDelivery, 'delivery_order_number')->textInput()->label('Delivery Order Number') ?>
 </div>

 <div class="col-md-3">
    <?=
            $form->field($purchaseDelivery, 'delivery_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label(' Delivery Date')
            ?>
     
        
    </div>
    <div class="col-md-3">
    <?php
                echo $form->field($purchaseDelivery, 'lpo_status')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => $documentStatus,
                    'options' => ['id' => 'lpo_status', 'placeholder' => 'Select ...', 'value'=>$purchaseDelivery->lpo_status],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['po_id'],
                        'url' => Url::to(['lpo-status']),
                        'params' => ['po_id'],

                    ]
                ]);
                ?>

   
            </div>
 <!-- <div class="col-md-4"> -->
 <div class="col-md-3">
 <?php
                echo $form->field($purchaseDelivery, 'currency')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => $currencyList,
                    'options' => ['id' => 'currency', 'placeholder' => 'Select ...','value'=>$purchaseDelivery->currency],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['po_id'],
                        'url' => Url::to(['currency']),
                        'params' => ['po_id'],

                    ]
                ]);
                ?>

 
       
    </div>
    </div>
    <br>
    <div class="row">
    <div class="col-md-3">
    <?php


                echo $form->field($purchaseDelivery, 'payment_terms')->widget(DepDrop::classname(), [
                    'type' => DepDrop::TYPE_SELECT2,
                    'data' => $paymentTerms,
                    'options' => ['id' => 'payment_terms', 'placeholder' => 'Select ...','value'=>$purchaseDelivery->payment_terms],
                    'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                    'pluginOptions' => [
                        'depends' => ['po_id'],
                        'url' => Url::to(['payment-terms']),
                        'params' => ['po_id'],

                    ]
                ]);

                ?>

</div>
    <div class="col-md-3">
     
     <?= $form->field($deliveryNumber, 'supplier_delivery_number')->textInput(['id'=>'sd_number'])->label('Supplier Delivery Number') ?>
 </div>
    </div>
   
   
    </div>
    <br>
    

</div>
<br>


<div>&nbsp;</div>  
<div class="col-md-12 "><legend class="text-info"><small>Material</small></legend></div>
<div class ="row ">
<div class="col-md-3" style="font-weight:bold">Material</div>
    <div class="col-md-2" style="font-weight:bold">Description</div>
    <div class="col-md-2" style="font-weight:bold">Delivery Status</div>
    <div class="col-md-1" style="font-weight:bold">Order Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Delivered Quantity</div>
    <div class="col-md-1" style="font-weight:bold"> Received Quantity</div>
    <div class="col-md-1" style="font-weight:bold">&nbsp;</div>
</div>

<div id="product">
<?php foreach ($arrayData as $delivery) { ?>
    <?php foreach ($purchaseDelivery->deliveryDetails as $key =>$deliveryDetails) { ?>
<div class="col-md-3"><select  id="purchase-delivery-material-0<?php echo $delivery['id']; ?>" class="form-control required material" name="DeliveryDetails[material_id][]"  >
    
    <option value="">Please select material</option>
        <?php foreach ($resourceNameListData as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$delivery['material_id']){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
</select>
    
                                     
    </div>
    <div class="col-md-2"><textarea data-row ="<?php echo $delivery['id']; ?>"  id="purchase-delivery-description<?php echo $delivery['id']; ?>" class="form-control  description" name="DeliveryDetails[description][]"><?php echo $delivery['description'];?></textarea></div>
    <div class="col-md-2"><select data-row ="<?php echo $delivery['id']; ?>" id="purchase-delivery-deliverystatus-<?php echo $delivery['id']; ?>" class="form-control  deliverystatus" name="DeliveryDetails[delivery_status][]">
    <!-- <option value="">select</option> -->
                        <?php

                        $mrvStatus = [];
                        foreach ($deliveryStatus as $value) {
                            $pType = [];
                            $pType['id'] = $value['id'];
                            $pType = $value['name'];
                            $mrvStatus = $pType;
                            ?>
                            <option value="<?php echo $value['id']; ?>"><?php echo($mrvStatus); ?></option>
                        <?php } ?>
   
    </select></div>
    
<div class="col-md-1"><input type="text" value="<?php echo $delivery['order_quantity']; ?>"data-row ="<?php echo $delivery['id']; ?>" id="order-quantity-<?php echo $delivery['id']; ?>" class="form-control order-qnty" name="DeliveryDetails[order_quantity][]"onchange="calc(this.id);" ></div>
    <div class="col-md-1"><input type="text" value="<?php echo $delivery['delivered_quantity']; ?>"data-row ="<?php echo $delivery['id']; ?>" id="delivered-quantity-<?php echo $delivery['id']; ?>" class="form-control delivered-qnty" name="DeliveryDetails[delivered_quantity][]" onchange="calc(this.id);" ></div>
    <div class="col-md-1"><input type="text" value="<?php echo $deliveryDetails->received_quantity; ?>" data-row ="<?php echo $delivery['id']; ?>" id="received-quantity-<?php echo $delivery['id']; ?>" class="form-control received-qnty" name="DeliveryDetails[received_quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="hidden" value=<?php echo $delivery['id']; ?> data-row ="<?php echo $delivery['id']; ?>" id="podetails-<?php echo $delivery['id']; ?>" class="form-control qnty" name="DeliveryDetails[podetails][]"></div>
    
    <div class="row">&nbsp;</div> 
<?php } }?>
</div>
   </div>

<div class = "row">
<div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                <?= Html::submitButton($purchaseDelivery->isNewRecord ? 'Submit' : 'Update', ['class' => $purchaseDelivery->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                    <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
        </div>
        </div>
        </div>

        
<?php
ActiveForm::end();
?>
 </div>
<?= $this->registerJs("
    
    $('#purchasedelivery-delivery_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });
     
    $(function () {
        $('body').on('change','#purchasedelivery-project_id',function (){ 
     
            var project_id = $(this).val();
            // alert(project_id);
            $.ajax({
                url: './get-ponumber-list',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    console.log('data',data);
                    $('#po_number').empty().append(data);
                 
                }
            });
        });
    });


", View::POS_READY); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
  
  function calc(id) {
    //   alert(id);
     
      var idPosition = id.split('-');
      $("#received-quantity-"+idPosition[2]).on("blur", function () {
        // alert();
    var received=$("#received-quantity-"+idPosition[2]).val();
    var order=$("#order-quantity-"+idPosition[2]).val();
var counterVal=$(this).attr('data-row');

           $.ajax({
                url: './get-received-quantity',
                data: {'deliveryId': counterVal,'received':received},
                type: 'post',
                success: function (data) {
                    console.log('data',data);
                    if(Number(data)==Number(order)){
              $("#purchase-delivery-deliverystatus-" +idPosition[2]).val(218);
                       }
                     }
                   });
  });
    
  }
  

  



</script>

<script>
  
 


    $(function () {
        $('#purchasedelivery-project_id').on('change',function (){
            var project_id = $(this).val();
            // console.log('project_id',project_id);
            $.ajax({
                url: './get-supplier',
                data: {'project_id':project_id},
                type: 'post',
                success: function (data) {
                    // console.log('data supplier',data);
                    // var data1=[];
                    $('#supplier').empty().append(data);

                }
            });

        });
    });

    $(function () {
        $('#purchasedelivery-project_id').on('change',function (){
            var project_id = $(this).val();
            // console.log('project_id',project_id);
            $.ajax({
                url: './get-document-status',
                data: {'project_id':project_id},
                type: 'post',
                success: function (data) {
                    // console.log('data supplier',data);
                    // var data1=[];
                    $('#lpo_status').empty().append(data);

                }
            });

        });
    });

    $(function () {
        $('#purchasedelivery-project_id').on('change',function (){
            var project_id = $(this).val();
            // console.log('project_id',project_id);
            $.ajax({
                url: './get-currency',
                data: {'project_id':project_id},
                type: 'post',
                success: function (data) {
                    // console.log('data supplier',data);
                    // var data1=[];
                    $('#currency').empty().append(data);
                  
                }
            });

        });
    });


    $(function () {
    $("#po_number").change(function () {
        var doId =  <?php echo json_encode($DOnumber) ?>;
// console.log('doId',doId);
    var poNumberText= ($("#po_number :selected").text()) ;
    $('#sd_number').val(poNumberText + '-'+ doId);
});
});
    $(function () {
        $('#purchasedelivery-project_id').on('change',function (){
            var project_id = $(this).val();
            // console.log('project_id',project_id);
            $.ajax({
                url: './get-payment-terms',
                data: {'project_id':project_id},
                type: 'post',
                success: function (data) {
                    // console.log('data supplier',data);
                    // var data1=[];
                    $('#payment_terms').empty().append(data);

                }
            });

        });
    });

    $(function () {
        $('#po_number').on('change',function (){
            var po_number = $(this).val();
            // console.log('po_number',po_number);
            $.ajax({
                url: './get-delivery-data',
                data: {'po_number':po_number},
                type: 'post',
                success: function (data) {
                  
                    $('#product').empty().append(data).appendTo('#purchase-delivery-form');

                }
            });

        });
    });
</script>

 
  