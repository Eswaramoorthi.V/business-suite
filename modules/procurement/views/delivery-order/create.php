<?php

use yii\helpers\Html;

$this->title = 'Create  GRN ' ;
$this->params['breadcrumbs'][] = ['label' => 'GRN', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="purchase">
    <?= $this->render('_form', [
       'delivery' => $delivery,
       'deliveryOrder' => $deliveryOrder,
       'projectList'=>$projectList,
        'mrvStatus'=>$mrvStatus,
    'resourceNameListData'=>$resourceNameListData,
         'poList'=>$poList,
         'supplierData'=>$supplierData,
         'LpoStatus'=>$LpoStatus,
         'currencylist'=>$currencylist,
         'termsTypeValue'=>$termsTypeValue,
         'deliveryNumber'=>$deliveryNumber

                
    ]) ?>

</div>
