<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
//  $purchaseMaster->requested_date = date('d-m-Y', strtotime($requestMaster['requested_date']));
 
 

$this->title = 'Update Purchase Request';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/request/purchase-request/index']];
$this->params['breadcrumbs'][] = $this->title;

$purchaseStatus= [1=>'Purchase Created',2=>'Purchase Issued',3=>'Waiting for delivery',4=>'Delivered'];
// pr($requestMaster);
?>

<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
    <?php $form = ActiveForm::begin(['id'=>'purchase-request-form','options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
    <div class="row">
         
    <div class="col-md-3">
     
       
     <?=
         $form->field($requestMaster, 'project_id', [
             'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
             'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
         ])->dropDownList($projectList, [
             'prompt' => 'Choose Project',
             'class' => 'project_id select from control',
             'value'=>$requestMaster->project_id
         ])->label("Project Name")
         ?>
 </div>
 <div class="col-md-3">
     
       
 <?= $form->field($requestMaster, 'reference_name')->textInput([])->label('Reference Name') ?>
 </div>
 <div class="col-md-3">
     
 <?= $form->field($requestMaster, 'requested_by')->textInput(['value'=>$requestedByName])->label('Requested By') ?>
     </div>
 
 <div class="col-md-3">
    <?=
            $form->field($requestMaster, 'requested_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Requested Date')
            ?>
     
        
    </div>
    
    </div>
    <br>
    
</div>
<br>

<div>&nbsp;</div>  
<div class="col-md-12"><legend class="text-info"><small>Material</small></legend></div>
<div class ="row">
<div class="col-md-2" style="font-weight:bold">Material</div>
<div class="col-md-2" style="font-weight:bold">Description</div>
<div class="col-md-1" style="font-weight:bold">Remarks</div>
<div class="col-md-1" style="font-weight:bold">MRV Status</div>
<div class="col-md-1" style="font-weight:bold">Unit </div>
<div class="col-md-2" style="font-weight:bold">Expected Date</div>
    <!-- <div class="col-md-1" id ="total"  style="font-weight:bold">Total Quantity</div> -->

    <div class="col-md-1"  id ="request" style="font-weight:bold">Request Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Document</div>
</div>
<div id="product">
<?php foreach ($requestMaster->request as $key =>$request) { ?>



<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-2"><select id="request-material-0<?php echo $request->id; ?>" class="form-control required material" name="RequestDetails[material_id][]"  >
    
    <option value="">Please select material</option>
        <?php foreach ($resourceNameListData as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$request->material_id){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
</select>
    
                                     
    </div>
    <div class="col-md-2"><textarea id="request-description" class="form-control  descrip" data-row ="0"  name="RequestDetails[description][]"><?php echo $request->description;?></textarea></div>
    <div class="col-md-1"><textarea id="request-remarks" class="form-control  remarks" data-row ="0"  name="RequestDetails[remarks][]"><?php echo $request->remarks;?></textarea></div>
    <div class="col-md-1"><select  id="request-mrvstatus-0<?php echo $request->id;?>" class="form-control  mrvstatus" name="RequestDetails[mrv_status][]"  onchange="calc(this.id);" >
    
    <option value=""> select </option>
        <?php foreach ($mrvStatus as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$request->mrv_status){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
    </select></div>
    <div class="col-md-1"><select data-row ="0" id="request-unit-0<?php echo $request->id;?>" class="form-control  unit" name="RequestDetails[unit][]"  onchange="calc(this.id);" >
    
    <option value="">Please select unit</option>
        <?php foreach ($unitType as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$request->unit){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
    </select></div>
    <div class="col-md-2"><input type="date"  value ="<?php echo $request->expected_date;?>" data-row ="<?php echo $request->id; ?>" id="request-expected-date" class="form-control  expected-date" name="RequestDetails[expected_date][]" placeholder ="dd-mm-yyyy"></div>

    
    <div class="col-md-1"  id= "requestfirst"><input type="text" data-row ="<?php echo $request->id; ?>"  value="<?php echo $request->material_request_quantity; ?>" id="request-quantity-<?php echo $request->id; ?>" class="form-control qntyrequest" name="RequestDetails[request_quantity][]"  onchange="calc(this.id);" >
    <h6 class="error-msg hide text-danger"> </h6>
    </div>
    <div class="col-md-1" ><input type="file"  accept="image/*"  data-row="<?php echo $request->id; ?>" value="<?php echo $request->document;?>"  id="document-<?php echo $request->id; ?>"  name="RequestDetails[document][]">
                 </div>
    
    <?php if($key!=0){ ?>
    <div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn  new-item-remove remove" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>
    <?php } ?>
    <input type="hidden" value="<?php echo $request->id;?>" name="RequestDetails[id][]"> 
  </div>
</div>

<div class="row">&nbsp;</div> 
<?php } ?>
</div>




  <div class="col-md-12" input-group-btn style="margin-top:21px">
       <div class="row">
 <div class="col-md-2">
        <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> ADD</button>
 </div>
            
       </div> 
    </div>
<br>
<div class="col-md-12"><legend class="text-info"><small></small></legend></div>
<div class="col-md-12"> 
    <div class="form-group">
        <div class="col-md-12 col-xs-12">
<?= Html::submitButton($requestMaster->isNewRecord ? 'Submit' : 'Update', ['class' => $requestMaster->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
<?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
</div>
</div>
</div>
<?php
ActiveForm::end();
?>
       </div>
 <?= $this->registerJs("
  
    $('#purchaserequestmaster-requested_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });


      $('body').on('click','.remove',function (){ 
        // console.log('remove',('.remove'));
    $(this).parents('.control-group').parent().remove();
    calc('remove');
    
  });

  $(function () {
    $('body').on('change','#purchaserequestmaster-project_id',function (){ 
 
        var project_id = $(this).val();
        // alert(project_id);
        $.ajax({
            url: './get-project-code',
            data: {'project_id': project_id},
            type: 'post',
            success: function (data) {
                // console.log('data',data);
                $('#purchaserequestmaster-reference_name').val(data);
             
            }
        });
    });
    
});


  $(function () {
    $('body').on('change','#purchaserequestmaster-project_id',function (){ 
 
        var project_id = $(this).val();
        // alert(project_id);
        $.ajax({
            url: './get-material-list',
            data: {'project_id': project_id},
            type: 'post',
            success: function (data) {
                // console.log('data',data);
                $('.material').empty().append(data);
             
            }
        });
    });
    
});


$(function () {
    $('body').on('change','#purchaserequestmaster-project_id',function (){ 
     
        var project_id = $(this).val();
        
        $.ajax({
            url: './get-project-type',
            data: {'project_id': project_id},
            type: 'post',
            success: function (data) {
                console.log('data',data);
                
                if(data =='cost center'){
               
                  $('#total').hide();
                  $('#purchased').hide();
                $('#totalfirst').hide();
                $('#purchasefirst').hide();
                   
                }else {
                   $('.qntyrequest').show();
                    $('.qntytotal').show();
                    $('.qntypurchase').show();
                }
                
             
            }
        });
    });
});
    
", View::POS_READY); ?>


<?php
$js = <<< JS
        

    var counter = 1;

    $(".add-more").click(function(){

                    
        var optionsValues = $('#request-material-0'+counter+'>option').map(function() {
        
        return $(this).val();
                }).get();
   var optionsText = $('#request-material-0'+counter+'>option').map(function() {return $(this).text();}).get();
   
    var optionsValueText = "";
    $.each(optionsText , function(index, val) { 
        
       optionsValueText=optionsValueText+'<option value="'+optionsValues[index]+'">'+val+'</option>';
});
var values = $('#request-mrvstatus-0'+counter+'>option').map(function() { return $(this).val(); });
var text = $('#request-mrvstatus-0'+counter+'>option').map(function() { return $(this).text(); });

var statusoptionsValueText = "";
    $.each(text , function(index, val) { 
        
        statusoptionsValueText=statusoptionsValueText+'<option value="'+values[index]+'">'+val+'</option>';
});
console.log('statusoptionsValueText',statusoptionsValueText);
var unitoptionsValues = $("#request-unit option").map(function() {
                   return $(this).val();
                  }).get();
                 
        
        var unitoptionsText = $("#request-unit option").map(function() {return $(this).text();}).get();
       
         var unitoptionsValueText = "";
         $.each(unitoptionsText , function(index, val) { 
             
            unitoptionsValueText=unitoptionsValueText+'<option value="'+unitoptionsValues[index]+'">'+val+'</option>';
});
        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp</div><div class="control-group col-md-12 padding-0">' +
        '<div class="col-md-2"><select  data-row = "'+counter+'" id="request-material-' + counter +'" class="form-control required material" name="RequestDetails[material_id][] " onchange="calc(this.id);">'+optionsValueText+'</select></div>'+
        '<div class="col-md-2"><textarea data-row = "descripion-' + counter + '" id="description-' + counter + '" class="form-control required description" name="RequestDetails[description][]"></textarea></div>'+
        '<div class="col-md-1"><textarea data-row = "remarks-' + counter + '" id="remarks-' + counter + '" class="form-control remarks" name="RequestDetails[remarks][]"></textarea></div>'+
        '<div class="col-md-1"><select  data-row = "'+counter+'" id="request-mrvstatus-' + counter +'" class="form-control  mrvstatus" name="RequestDetails[mrv_status][] " onchange="calc(this.id);">'+statusoptionsValueText+'</select></div>'+
        '<div class="col-md-1"><select  data-row = "'+counter+'" id="request-unit-' + counter +'" class="form-control  unit" name="RequestDetails[unit][] " onchange="calc(this.id);">'+unitoptionsValueText+'</select></div>'+
        
        '<div class="col-md-2"><input type="date" value="0"  data-row = "expected_date-' + counter + '" id="expected_date-' + counter + '" class="form-control expected_date" name="RequestDetails[expected_date][]"></div>'+
        
        '<div class="col-md-1"><input type="text" value="0"   data-row = "'+counter+'" id="request-quantity-' + counter + '" class="form-control qntyrequest" name="RequestDetails[request_quantity][]" ><h6 class="error-msg hide text-danger"> </h6></div>'+
        '<div class="col-md-1"><input type="file" accept="image/*"  data-row = "'+counter+'"  value="0"  id="document-' + counter + '"  name="RequestDetails[document][]"></div>'+
        '<div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn  new-item-remove remove" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>'+
        '<div class="clear">&nbsp</div>'+
            '</div>');
        newContainer.appendTo("#product");
              counter++;
                   
               
        });
JS;
$this->registerJs($js);


?>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>


    function calc(id) {

        if (id != "remove") {

            var idPosition = id.split('-');


            $('body').on('blur','.qntyrequest',function (){ 
            var counterVal=$(this).attr('data-row');
             var requestQuantity=$(this).val();
  

       var totalQuantity = parseInt($("#total-quantity-"+counterVal).val());
console.log('totalQuantity',totalQuantity);
  var requestQuntity = parseInt($("#request-quantity-"+counterVal).val());
  if (totalQuantity >= requestQuntity) {
    return false;
  } else if(totalQuantity <= requestQuntity) {
    $('.error-msg').removeClass("hide");
   
    $('.error-msg').html("greater than total quantity");
    setTimeout(function() {
             $('.error-msg').hide();
        }, 3000); 
  }
  else{
        $('.error-msg').addClass("hide");
        
    } 
  });


            $('body').on('change', '.material', function () {
                var material_id = $(this).val();
                var counterVal = $(this).attr('data-row');
                // console.log('counterVal', counterVal);

                var project_id = $('#project_id').val();
                $.ajax({
                    url: './get-total-quantity',
                    data: {'project_id': project_id, 'material_id': material_id},
                    type: 'post',
                    success: function (data) {

                        console.log('data',data);
                        $("#total-quantity-" + counterVal).val(data);

                    }
                });
                $.ajax({
                    url: './get-purchase-quantity',
                    data: {'project_id': project_id, 'material_id': material_id},
                    type: 'post',
                    success: function (data) {
                        console.log($("#purchased-quantity-" + counterVal));
                        $("#purchased-quantity-" + counterVal).val(data);

                    }
                });

            });

        }
    }


</script>
   
<script>

$('form').submit(function() {
    // alert();
   
                var request_quantity = document.getElementsByName('RequestDetails[request_quantity][]');
               
              var material = document.getElementsByName('RequestDetails[material_id][]');
            
              var expected_date = document.getElementsByName('RequestDetails[expected_date][]');
                $("#error-message").removeClass("hide");
                for (k=0; k<request_quantity.length; k++) {
                   
                    $(request_quantity[k]).removeClass("error-highlight");
                    $(material[k]).removeClass("error-highlight");
                    $(expected_date[k]).removeClass("error-highlight");
                }
                
                    for (i=0; i<request_quantity.length; i++) {
                      
                          if (request_quantity[i].value == 0) {
                            alert(request_quantity);
                            $("#error-message").html("<p>Request Quantity can not be 0 value</p>");
                            $("#error-message").removeClass("hide").addClass("show");
                            $(request_quantity[i]).addClass("error-highlight");
                            $('form').bind('submit',function(e){e.preventDefault();});
                        }else if (material[i].value == 0) {
                            $("#error-message").html("<p>Material cannot be blank</p>");
                            $("#error-message").removeClass("hide").addClass("show");
                            $(material[i]).addClass("error-highlight");
                            $('form').bind('submit',function(e){e.preventDefault();});
                        }
                        else if (expected_date[i].value == 0) {
                            $("#error-message").html("<p>Expected Date cannot be blank</p>");
                            $("#error-message").removeClass("hide").addClass("show");
                            $(expected_date[i]).addClass("error-highlight");
                            $('form').bind('submit',function(e){e.preventDefault();});
                        }else{
                            $('form').unbind('submit');
                        }
                      
                      }
                $("#error-message").addClass("hide");
               
   });

  
</script>