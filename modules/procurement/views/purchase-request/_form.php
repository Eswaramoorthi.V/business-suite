<?php


use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\select2\Select2;

$purchaseStatus = [1 => 'Purchase Created', 2 => 'Purchase Issued', 3 => 'Waiting for delivery', 4 => 'Delivered'];
$itemNo=10;

?>
<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
    <?php $form = ActiveForm::begin(['id' => 'purchase-request-form','options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row hide" id="error-message"><div class="col-md-12 warning"></div>
    </div>
    <div class="row">
        <div class="row">

            <div class="col-md-3">


            <label> Project Code</label>
<?php

echo Select2::widget([
    'model' => $requestMaster,
    'name' => 'project_id',
    'id' => 'project_id',
    'data' => $projectList,
    'theme' => Select2::THEME_DEFAULT,
    'options' => ['placeholder' => 'Select Project Code ...', 'autocomplete' => 'off'],
    'pluginOptions' => [
        'allowClear' => true,
        'initialize' => true,
    ],
    // 'required'=>true
]);

?>
            </div>
            <div class="col-md-3">


                <?= $form->field($requestMaster, 'reference_name')->textInput([])->label('MRV Code') ?>
            </div>
            <div class="col-md-3">
            <?= $form->field($requestMaster, 'requested_by')->textInput(['value'=>$requestedByName])->label('Requested By') ?>

                
            </div>

            <div class="col-md-3">
                <?=
                $form->field($requestMaster, 'requested_date', [
                    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
                ])->textInput([])->label('Requested Date')
                ?>


            </div>

        </div>
        <br>

    </div>
    <br>


    <div class="col-md-12">
   
        <legend class="text-info"><small>Material</small></legend>
    </div>
    <div class="row">
        <div class="col-md-2" style="font-weight:bold">Material</div>
        <div class="col-md-2" style="font-weight:bold">Description</div>
        <div class="col-md-1" style="font-weight:bold">Remarks</div>
        <div class="col-md-1" style="font-weight:bold">MRV Status</div>
        <div class="col-md-1" style="font-weight:bold">Unit</div>
        <div class="col-md-2" style="font-weight:bold">Expected Date</div>
        <!-- <div class="col-md-1 hide" id="total" style="font-weight:bold">Total Quantity</div> -->
      
        <div class="col-md-1" id="request" style="font-weight:bold">Request Quantity</div>
        <div class="col-md-1" style="font-weight:bold">Document</div>
        <!-- <div class="col-md-1" style="font-weight:bold">&nbsp;</div> -->
    </div>
    <div id="product">
 
        <div class="row copyinvitems details after-add-more">
            <div class="control-group">
                <div class="col-md-2"><select data-row="0" id="request-material" class="form-control required material"
                                              name="RequestDetails[material_id][]" onchange="calc(this.id);">
                        <option value="">Please select material</option>
                    </select><h6  id ="error-message1" class="error-message1 hide text-danger" style="font-size:10px;"> </h6></div>
                <div class="col-md-2"><textarea  data-row="0" id="request-description" class="form-control  descrip"
                                                name="RequestDetails[description][]"></textarea></div>
                                                <div class="col-md-1"><textarea id="request-remarks" class="form-control required remarks"
                                                name="RequestDetails[remarks][]"></textarea></div>
                                                <div class="col-md-1"><select data-row ="0" id="request-mrvstatus" class="form-control required mrvstatus" name="RequestDetails[mrv_status][]">
    
                        <?php

                        $mrvstatus = [];

                        foreach ($mrvStatus as $id=>$value) {

                            ?>
                            <option value="<?php echo $id?>"><?php echo $value; ?></option>
                        <?php } ?>
   
    </select></div>
                <div class="col-md-1"><select data-row="0" id="request-unit"
                                              class="form-control required unit"
                                              name="RequestDetails[unit][]">

                        <?php

                        $unit_type = [];
                        foreach ($unitType as $unit=>$unitvalue) {

                            ?>
                            <option value="<?php echo $unit; ?>"><?php echo $unitvalue; ?></option>
                        <?php } ?>
                    </select></div>
                <div class="col-md-2"><input type="date" data-row="0" id="request-expected_date"
                                             class="form-control required expected_date" name="RequestDetails[expected_date][]">
                                             <h6  id ="error-message2" class="error-message2 hide text-danger" style="font-size:10px;"> </h6>
                </div>
                
            
                <div class="col-md-1" id="requestfirst"><input type="text" data-row="0" value="0"
                                                               id="request-quantity-0" class="form-control required qntyrequest"
                                                               name="RequestDetails[request_quantity][]"
                                                               onchange="calc(this.id);">
                                                               <h6 class="error-msg hide text-danger"> </h6>
                                                               <h6  id ="error-message3" class="error-message3 hide text-danger" style="font-size:10px;"> </h6>
                </div>
                <div class="col-md-1" ><input type="file" accept="image/*"  data-row="0" value="0"  id="document-0"  name="RequestDetails[document][]">
                                                              </div>
               

            </div>
        </div>
       
    </div>
     <br>
     <div class="col-md-12" input-group-btn style="margin-top:21px">
        <div class="row">
            <div class="col-md-2">
                <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> ADD
                </button>
            </div>

        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($requestMaster->isNewRecord ? 'Submit' : 'Update', ['class' => $requestMaster->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                    <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ', 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="clearfix"></div>
</div>

<?php ActiveForm::end(); ?>
</div>
<?= $this->registerJs("
  
    $('#purchaserequestmaster-requested_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });


      $('body').on('click','.remove',function (){ 
        // console.log('remove',('.remove'));
    $(this).parents('.control-group').parent().remove();
    calc('remove');
    
  });

   





    $(function () {
        $('body').on('change','#project_id',function (){ 
     
            var project_id = $(this).val();
            // alert(project_id);
            $.ajax({
                url: './get-material-list',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    // console.log('data',data);
                    $('.material').empty().append(data);
                 
                }
            });
        });
        
    });
    
 

    $(function () {
        $('body').on('change','#project_id',function (){ 
     
            var project_id = $(this).val();
            // alert(project_id);
            $.ajax({
                url: './get-project-code',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    // console.log('data',data);
                    $('#purchaserequestmaster-reference_name').val(data);
                 
                }
            });
        });
        
    });

    $(function () {
        $('body').on('change','#project_id',function (){ 
         
            var project_id = $(this).val();
            // alert(project_id);
            $.ajax({
                url: './get-project-type',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    // console.log('data',data);
                    
                    if(data =='cost center'){
                   
                      $('#total').hide();
                      $('#purchased').hide();
                    $('#totalfirst').hide();
                    $('#purchasefirst').hide();
                       
                    }else {
                       $('.qntyrequest').show();
                        $('.qntytotal').show();
                        $('.qntypurchase').show();
                    }
                    
                 
                }
            });
        });
    });

   
    
", View::POS_READY); ?>

<?php
$js = <<< JS
        

    var counter = 1;

    $(".add-more").click(function(){

                    
                   var optionsValues = $("#request-material option").map(function() {
         return $(this).val();
              }).get();
        var optionsText = $("#request-material option").map(function() {return $(this).text();}).get();
        
         var optionsValueText = "";
         $.each(optionsText , function(index, val) { 
             
            optionsValueText=optionsValueText+'<option value="'+optionsValues[index]+'">'+val+'</option>';
});
var statusoptionsValues = $("#request-unit option").map(function() {
                   return $(this).val();
                  }).get();
                 
        
        var statusoptionsText = $("#request-unit option").map(function() {return $(this).text();}).get();
       
         var unitoptionsValueText = "";
         $.each(statusoptionsText , function(index, val) { 
             
            unitoptionsValueText=unitoptionsValueText+'<option value="'+statusoptionsValues[index]+'">'+val+'</option>';
});
var statusoptionsValues = $("#request-mrvstatus option").map(function() {
                   return $(this).val();
                  }).get();
                  
        
        var statusoptionsText = $("#request-mrvstatus option").map(function() {return $(this).text();}).get();
       
         var mrvstatusoptionsValueText = "";
         $.each(statusoptionsText , function(index, val) { 
             
            mrvstatusoptionsValueText=mrvstatusoptionsValueText+'<option value="'+statusoptionsValues[index]+'">'+val+'</option>';
});

        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp</div><div class="control-group col-md-12 padding-0">' +
        '<div class="col-md-2"><select  data-row = "'+counter+'" id="request-material-' + counter +'" class="form-control required material" name="RequestDetails[material_id][] " onchange="calc(this.id);">'+optionsValueText+'</select><h6  id ="error-message1" class="error-message1 hide text-danger" style="font-size:10px;"> </h6></div>'+
        '<div class="col-md-2"><textarea data-row = "' + counter + '" id="description-' + counter + '" class="form-control  description" name="RequestDetails[description][]"></textarea></div>'+
        '<div class="col-md-1"><textarea data-row = "' + counter + '" id="remarks-' + counter + '" class="form-control remarks" name="RequestDetails[remarks][]"></textarea></div>'+
        '<div class="col-md-1"><select  data-row = "'+counter+'" id="request-mrvstatus-' + counter +'" class="form-control  mrvstatus" name="RequestDetails[mrv_status][] " onchange="calc(this.id);">'+mrvstatusoptionsValueText+'</select></div>'+
        '<div class="col-md-1"><select  data-row = "'+counter+'" id="request-unit-' + counter +'" class="form-control  unit" name="RequestDetails[unit][] " onchange="calc(this.id);">'+unitoptionsValueText+'</select></div>'+
        
        '<div class="col-md-2"><input type="date" value="0"  data-row = "expected_date-' + counter + '" id="expected_date-' + counter + '" class="form-control expected_date" name="RequestDetails[expected_date][]"><h6  id ="error-message2" class="error-message2 hide text-danger" style="font-size:10px;"> </h6></div>'+
        
        '<div class="col-md-1"><input type="text" value="0"   data-row = "'+counter+'" id="request-quantity-' + counter + '" class="form-control qntyrequest" name="RequestDetails[request_quantity][]" ><h6  id ="error-message3" class="error-message3 hide text-danger" style="font-size:10px;"> </h6><h6 class="error-msg hide text-danger"> </h6></div>'+
        '<div class="col-md-1"><input type="file" accept="image/*"  data-row = "'+counter+'"  value="0"  id="document-' + counter + '"  name="RequestDetails[document][]"></div>'+
        
        '<div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn  new-item-remove remove" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>'+
        '<div class="clear">&nbsp</div>'+
            '</div>');
        newContainer.appendTo("#product");
              counter++;
                   
               
        });
JS;
$this->registerJs($js);


?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>


    function calc(id) {

        if (id != "remove") {

            var idPosition = id.split('-');


            $('body').on('blur','.qntyrequest',function (){ 
            var counterVal=$(this).attr('data-row');
             var requestQuantity=$(this).val();
  

       var totalQuantity = parseInt($("#total-quantity-"+counterVal).val());
console.log('totalQuantity',totalQuantity);
  var requestQuntity = parseInt($("#request-quantity-"+counterVal).val());
  if (totalQuantity >= requestQuntity) {
    return false;
  } else if(totalQuantity <= requestQuntity) {
    $('.error-msg').removeClass("hide");
   
    $('.error-msg').html("greater than total quantity");
    setTimeout(function() {
             $('.error-msg').hide();
        }, 3000); 
  }
  else{
        $('.error-msg').addClass("hide");
        
    } 
  });


            $('body').on('change', '.material', function () {
                var material_id = $(this).val();
                var counterVal = $(this).attr('data-row');
                // console.log('counterVal', counterVal);

                var project_id = $('#project_id').val();
                $.ajax({
                    url: './get-total-quantity',
                    data: {'project_id': project_id, 'material_id': material_id},
                    type: 'post',
                    success: function (data) {

                        console.log('data',data);
                        $("#total-quantity-" + counterVal).val(data);

                    }
                });
                $.ajax({
                    url: './get-purchase-quantity',
                    data: {'project_id': project_id, 'material_id': material_id},
                    type: 'post',
                    success: function (data) {
                        console.log($("#purchased-quantity-" + counterVal));
                        $("#purchased-quantity-" + counterVal).val(data);

                    }
                });

            });

        }
    }


</script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<script>

$('form').submit(function() {
    // alert();
   
                var request_quantity = document.getElementsByName('RequestDetails[request_quantity][]');
               
              var material = document.getElementsByName('RequestDetails[material_id][]');
            
              var expected_date = document.getElementsByName('RequestDetails[expected_date][]');
                $(".error-message1").removeClass("hide");
                $(".error-message2").removeClass("hide");
                $(".error-message3").removeClass("hide");
                for (k=0; k<request_quantity.length; k++) {
                   
                    $(request_quantity[k]).removeClass("error-highlight");
                    $(material[k]).removeClass("error-highlight");
                    $(expected_date[k]).removeClass("error-highlight");
                }
                
                    for (i=0; i<request_quantity.length; i++) {
                      
                          if (request_quantity[i].value == 0) {
                            // alert(request_quantity);
                            $(".error-message3").html("<p>Request Quantity can not be 0 value</p>");
                            $("#error-message3").removeClass("hide").addClass("show");
                            $(request_quantity[i]).addClass("error-highlight");
                            $('form').bind('submit',function(e){e.preventDefault();});
                        }else if (material[i].value == 0) {
                            $(".error-message1").html("<p>Material cannot be blank</p>");
                            $(".error-message1").removeClass("hide").addClass("show");
                            $(material[i]).addClass("error-highlight");
                            $('form').bind('submit',function(e){e.preventDefault();});
                        }
                        else if (expected_date[i].value == 0) {
                            $(".error-message2").html("<p>Expected Date cannot be blank</p>");
                            $(".error-message2").removeClass("hide").addClass("show");
                            $(expected_date[i]).addClass("error-highlight");
                            $('form').bind('submit',function(e){e.preventDefault();});
                        }else{
                            $('form').unbind('submit');
                        }
                      
                      }
                $(".error-message1").addClass("hide");
                $(".error-message2").addClass("hide");

                $(".error-message3").addClass("hide");
               
   });

  
</script>
   

 
 