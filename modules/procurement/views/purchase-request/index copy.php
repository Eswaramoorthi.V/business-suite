<?php



use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use app\modules\project\models\Project;
use app\modules\customer\models\Customer;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('user', 'Purchase Order');
$this->params['breadcrumbs'][] = 'Purchase';
//$this->params['title_icon'] = 'fa-users';


?>

<?php Pjax::begin() ?>
<div class="purchase">

    <?php
     if ($dataProvider) {
        echo GridView::widget([
            //'id' => 'kv-grid-demo',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'responsiveWrap' =>false,
            'columns' => [

                ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false],
                
                [
                    'attribute' => 'project_name',
                    'header' => '<div style="width:100px;">Project  Name</div>',
                    'value' => function($model, $key, $index) {
                        $projectListData =Project::find()->select(['project_name','id'])->asArray()->all();
    
                        $assign_project = [];
                        
                        foreach ($projectListData as $value) {
                            $eType =[];
                            $eType['id'] = $value['id'];
                            $eType['project_name'] = $value['project_name'];
                            $assign_project[]=$eType;
                        }
                        $projectList = ArrayHelper::map($assign_project, 'id', 'project_name');
                        return !empty($projectList[$model['project_name']])?$projectList[$model['project_name']]:'-';
                           
                        },
                    
                ],
                [
                    'attribute' => 'supplier_name',
                    'header' => '<div style="width:100px;">Supplier  Name</div>',
                    'value' => function($model, $key, $index) {
                        $assign_clientData =Customer::find()->asArray()->all();
                        $assign_ClientData = [];
                        
                        foreach ($assign_clientData as $value) {
                            $eType =[];
                            $eType['id'] = $value['id'];
                            $eType['customer_fname'] = $value['customer_fname'].' '.$value['customer_lname'];
                            // $eType['customer_lname'] = $value['customer_lname'];
                            $assign_ClientData[]=$eType;
                        }
                        
                        $assign_client = ArrayHelper::map($assign_ClientData, 'id', 'customer_fname');
                        return !empty($assign_client[$model['supplier_name']])?$assign_client[$model['supplier_name']]:'-';
                           
                        },
                    
                ],
                [
                    'attribute' => 'due_date',
                    'header' => '<div style="width:100px;">Delivery Date</div>',
                     'value' => function($model, $key, $index) {
                   return Yii::$app->formatter->asDate($model->due_date, 'php:d-m-Y');
               },
                    //'format' => 'date',
                    'filterType' => GridView::FILTER_DATE,
                    'filterWidgetOptions' => ([
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                ]
                    ])
                ],
                //    [
                //  'class' => 'kartik\grid\ActionColumn',
                //     'mergeHeader' => false,
                //     'contentOptions' => ['style' => 'width:50px;text-align:center','class'=>'skip-export'],
                //     'headerOptions' =>['class'=>'skip-export'],
                //     'header'=>"Actions",
                //     'template' => $actionList,
                //     //'template' => '{update}{delete}',
                //     'buttons' => [
                //       'update' => function($url) {
                //             return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'update', 'title' => 'Edit']
                //             );
                //         },
                //         'deletes' => function ($url) {
                //             return Html::a(
                //                 '<span class="glyphicon glyphicon-trash icons"></span>', $url, [
                //                     'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                //                 ]
                //             );
                //         },
                //         // 'export-pdf' => function($url) {
                //         //     return Html::a('<span class="fa fa-print icons"></span>', $url, ['class' => 'export-pdf', 'title' => 'PDF']
                //         //     );
                //         // },
                //         'export-pdf' => function ($url, $model, $key) {
                //             return Html::a('<span class="fa fa-print icons"></span>', Url::to(['purchase/export-pdf', 'id' => $model->id]),
                //                                 [
                //                                     'title' => Yii::t('app', 'PDF'),
                //                                     'data-pjax' => '0',
                
                //                                 ]
                //                             );
                                        
                //                             },
                //     ],
                // ],
                [
                
                    'class' => '\kartik\grid\ActionColumn',
                    'mergeHeader' => false,
                        'contentOptions' => ['style' => 'width:50px;text-align:center','class'=>'skip-export'],
                        'headerOptions' =>['class'=>'skip-export'],
                        'header'=>"Actions",
                    'template'=>$actionList,
                    'dropdown' => true,
                    'dropdownOptions'=>['class'=>'pull-right' ],
                    'options' => ['style' => 'max-width:20px;'],
                    'buttons' => [
                       
                        'update' => function($url) {
                            return Html::a('Update',   $url,['class' => 'action-btn dropdown-item ', 'title' => 'Update']
                            );
                        },
                        'deletes' => function($url) {
                           return Html::a('Delete',   $url,['class' => 'action-btn dropdown-item','data-method'=>'post', 'data-pjax'=>0,'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?']]
                           );
                       },
                       
    
    
                    ]
                    
                    ],
                

            ],
            'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'pjax' => false, // pjax is set to always true for this demo
       
            'toolbar' => [
                [
                    'content'=>

                        Html::a(Yii::t('app', ' {modelClass}', [
                            'modelClass' => '<i class="glyphicon glyphicon-bell icons"></i>Add New Purchase',
                        ]), ['create'], ['class' => 'btn btn-info btn-rounded', ]),

                ],

            ],

            
            //'showPageSummary' => true,
            'panel' => [

                // 'type' => GridView::TYPE_DEFAULT,
                'heading' => false,
            ],
            'persistResize' => true,

        ]);
    }

    ?>


</div>
<?php Pjax::end() ?>


<style>
a.action-btn.dropdown-item {
  display: block;
  width: 100%;
  padding: .25rem 1.5rem;
  clear: both;
  font-weight: normal;
  text-align: inherit;
  white-space: nowrap;
  background-color: transparent;
  border: 0;
  color: navy;
  font-size: small;
}
</style>