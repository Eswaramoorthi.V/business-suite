<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;



$this->title = 'Delivery Order';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/purchase/purchase/index']];
$this->params['breadcrumbs'][] = $this->title;
// pr($unitType);
// $project_id=$_GET;
// pr($project_id);

?>

<?php $home = Yii::$app->params['domain_url']; ?>


<div class="purchase-form form-vertical">
    <div class ="row">

    </div>
    <div class="table-responsive">

        <table class="table">
            <colgroup>
                <col style="width:15%">
                <col style="width:15%">
                <col style="width:15%">
                <col style="width:15%">
            </colgroup>
            <tbody><tr>

                <th> Project Name</th>
                <td>
                    <?php echo $projectName?>
                </td>
                <th> Reference Name</th>
                <td>
                    <?php echo $referenceName?>
                </td>

            </tr>

            <tr>
                <th> Requested Date</th>
                <td>
                    <?php echo $requestedDate?>
                </td>
                <th> Reference</th>
                <td>
                    <?php echo $requestedBy?>
                </td>


            </tr>
            </tbody></table>



    </div>
    <div>&nbsp</div>
    <div>&nbsp</div>
    <div>&nbsp</div>
    <?php $form = ActiveForm::begin(['id'=>'purchase-delivery-form','method'=>'post','action'=>'delivery?id='.$_GET['id']]); ?>


    <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>

    <div class ="row">
        <div class="col-md-2" style="font-weight:bold">Material</div>
        <div class="col-md-2" style="font-weight:bold">Description</div>
        <div class="col-md-1" style="font-weight:bold">Remarks</div>
        <div class="col-md-1" style="font-weight:bold">MRV Status</div>
        <div class="col-md-1" style="font-weight:bold">Unit</div>
        <div class="col-md-2" style="font-weight:bold">Expected Date</div>


        <div class="col-md-1" id="request" style="font-weight:bold">Request Quantity</div>

        <div class="col-md-1" id="total" style="font-weight:bold">Order Quantity</div>
        <div class="col-md-1" id="total" style="font-weight:bold">Delivered Quantity</div>
    </div>
    <div id="product">
        <?php foreach ($arrayData as $request) { ?>
            <div class="row copyinvitems details after-add-more">
                <div class="control-group">
                    <div class="col-md-2"><select  disabled ="disabled" id="purchase-delivery-material-0<?php echo $request['id']; ?>" class="form-control required material" name="PurchaseDelivery[material_id][]" disabled="disabled" >

                            <option value="">Please select material</option>
                            <?php foreach ($resourceNameListData as $result): ?>
                                <option value="<?php echo $result['id'];?>"<?php
                                if($result['id']==$request['material_id']){
                                    echo 'selected ';
                                } ?>><?php echo $result['name']; ?></option>
                                }
                            <?php endforeach; ?>
                        </select>


                    </div>
                    <div class="col-md-2"><textarea  readonly="true"  data-row ="<?php echo $request['id']; ?>"   id="purchase-delivery-description" class="form-control  description" name="PurchaseDelivery[description][]"><?php echo $request['description'];?></textarea></div>
                    <div class="col-md-1"><textarea  readonly="true"  id="request-remarks" class="form-control  remarks" data-row ="0"  name="RequestDetails[remarks][]"><?php echo $request['remarks']; ?></textarea></div>
                    <div class="col-md-1">

                        <?php
                        $mrvStatus=1;
                        $options['id']='purchase-master-mrvstatus';
                        $options['disabled']='disabled';
                        $options['class']='dropdownclass';
                        $options['data-row']=0;

                        echo Html::dropDownList('PurchaseMaster[mrv_status][]', $mrvStatus, $mrvSatusData,$options);?>

                    </div>
                    <div class="col-md-1">

                        <?php
                        $unitId=1;
                        $options['id']='purchase-delivery-unit';
                        $options['disabled']='disabled';
                        $options['class']='dropdownclass';
                        $options['data-row']=0;

                        echo Html::dropDownList('PurchaseDelivery[unit][]', $unitId, $unitType,$options);?>

                    </div>


                    <div class="col-md-2"><input   readonly="true" type="date"  value ="<?php echo $request['expected_date'];?>" data-row ="<?php echo $request['id']; ?>" id="purchase-delivery-delivery-date-<?php echo $request['id']; ?>" class="form-control  delivery-date" name="PurchaseDelivery[delivery_date][]"></div>

                    <div class="col-md-1"><input   readonly="true" type="text" value="<?php echo $request['material_request_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="request-quantity-<?php echo $request['id']; ?>" class="form-control requestqnty" name="PurchaseDelivery[request_quantity][]" ></div>
                    <div class="col-md-1"><input  readonly="true"  type="text" value="<?php echo $request['material_total_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="total-quantity-<?php echo $request['id']; ?>" class="form-control totalqnty" name="PurchaseDelivery[order_quantity][]" ></div>
                    <div class="col-md-1"><input  readonly="true"  type="text" value="<?php echo $request['material_total_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="total-quantity-<?php echo $request['id']; ?>" class="form-control totalqnty" name="PurchaseDelivery[delivered_quantity][]"></div>





                </div>
            </div>

            <div class="row">&nbsp;</div>
        <?php } ?>
    </div>

    <div>&nbsp;</div>


</div>
</div>
<?php ActiveForm::end(); ?>
</div>

