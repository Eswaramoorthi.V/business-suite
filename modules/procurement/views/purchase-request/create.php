<?php

use yii\helpers\Html;

$this->title = 'Create Purchase Request';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="purchase">
    <?= $this->render('_form', [
        'requestMaster' => $requestMaster,
        'unitType' => $unitType,
        'requestDetails' => $requestDetails,
        'projectList' => $projectList,
        'requestedByName' => $requestedByName,
        'mrvStatus' => $mrvStatus

    ]) ?>

</div>
