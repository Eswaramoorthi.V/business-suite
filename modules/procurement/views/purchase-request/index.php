

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use app\models\Project;
use app\models\Customer;
use app\models\Employee;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('user', 'Purchase Request');
$this->params['breadcrumbs'][] = 'Purchase';
//$this->params['title_icon'] = 'fa-users';
?>

<?php Pjax::begin() ?>
<div class="purchase">
<div class="col-md-6 text-left add-label" style="font-size: 30px">
      
            <i class="fa fa-product-hunt" aria-hidden="true"></i> MRV (Material Request Voucher)
    </div>
<?php
if ($dataProvider) {
    // pr($searchModel);
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'project_code',
                'header' => '<div style="width:100px;">Project  Name</div>',
                'value' => function($model, $key, $index) {
                                   return isset($model->project->project_code)?$model->project->project_code:"";

                    },
                
            ],
            [
                'attribute' => 'reference_name',
                'header' => '<div style="width:120px;">Reference Name</div>',
                'value' => function($model, $key, $index) {
                    // return $model->reference_name;
                    return Html::a($model->reference_name, ['/procurement/purchase-request/detailview', 'id' => $model->id], [ 'data-pjax'=>"0"]);
                },
                'vAlign' => 'middle',
                'format' => 'raw',
                'noWrap' => true
            ],
            
            
            [
                'attribute' => 'requested_date',
                'header' => '<div style="width:100px;">Delivery Date</div>',
                 'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->requested_date, 'php:d-m-Y');
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],

            
            [
                
                'class' => '\kartik\grid\ActionColumn',
                'mergeHeader' => false,
                    'contentOptions' => ['style' => 'width:50px;text-align:center','class'=>'skip-export'],
                    'headerOptions' =>['class'=>'skip-export'],
                    'header'=>"Actions",
                'template'=>$actionList,
                'dropdown' => true,
                'dropdownOptions'=>['class'=>'pull-right' ],
                'options' => ['style' => 'max-width:20px;'],
                'buttons' => [
                   
                    'update' => function($url) {
                        return Html::a('Update',   $url,['class' => 'action-btn dropdown-item ', 'title' => 'Update']
                        );
                    },
                    'deletes' => function($url) {
                       return Html::a('Delete',   $url,['class' => 'action-btn dropdown-item','data-method'=>'post', 'data-pjax'=>0,'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?']]
                       );
                   },
                ]
                
                ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'pjax' => false, // pjax is set to always true for this demo
       
            'toolbar' => [
                [
                    'content'=>

                        Html::a(Yii::t('app', ' {modelClass}', [
                            'modelClass' => '<i class="glyphicon glyphicon-bell icons"></i>Add New Purchase Request',
                        ]), ['create'], ['class' => 'btn btn-info btn-rounded', ]),

                ],

            ],

            
            //'showPageSummary' => true,
            'panel' => [

                // 'type' => GridView::TYPE_DEFAULT,
                'heading' => false,
            ],
            'persistResize' => true,

    ]);
}
?>                                   
</div>
<?php Pjax::end() ?>

<style>
a.action-btn.dropdown-item {
  display: block;
  width: 100%;
  padding: .25rem 1.5rem;
  clear: both;
  font-weight: normal;
  text-align: inherit;
  white-space: nowrap;
  background-color: transparent;
  border: 0;
  color: navy;
  font-size: small;
}
</style>