<?php


use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\View;
use app\models\Purchase;
use app\components\Procurement;

$vatcategory=\Yii::$app->params['VATRate'];
$currentUser=Yii::$app->user->getId();
// pr($currentUser);
$fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

$OrderType= [1=>'Direct',2=>'MRV'];
$role = current($fetchRole);
$roleName = $role->name;
// pr($roleName);
$purchaseStatus= [1=>'Purchase Created',2=>'Purchase Issued',3=>'Waiting for delivery',4=>'Delivered'];
$getLastRecord=Purchase::find()->orderBy(['id' => SORT_DESC])->asArray()->one();
$itemNo=10;
$lpo='LOP/';
if(!empty($getLastRecord)){
    $ponumer1=$getLastRecord['id']+1;
}else{
    $ponumer1=1;
}
$strpadnumber=str_pad($ponumer1, 5, '0', STR_PAD_LEFT);
$last_number=$lpo.$strpadnumber;

?>
<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
    <input type="hidden" name ="last_ponumber" value="<?php echo $last_number;?>" id="last_ponumber">
    <?php $form = ActiveForm::begin(['id'=>'purchase-master-form']); ?>
    <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
        <div class="row">

            <div class="col-md-3">


                <?=
                $form->field($purchaseMaster, 'project_name', [
                    'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                ])->dropDownList($projectList, [
                    'prompt' => 'Choose Project',
                    'class' => 'project_name select from control',
                    'value'=>$purchaseMaster->project_name
                ])->label("Project Name")
                ?>
            </div>

            <div class="col-md-3">

                <?=
                $form->field($purchaseMaster, 'supplier_name', [
                    'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                ])->dropDownList($supplierData, [
                    'prompt' => 'Choose Supplier',
                    'class' => 'select from control',
                    'value'=>$purchaseMaster->supplier_name
                ])->label("Supplier Name")
                ?>

            </div>

            <div class="col-md-3">

                <?= $form->field($purchaseMaster, 'originator')->textInput(['value'=>$roleName,'readonly'=>true])->label('Originator') ?>
            </div>
            <div class="col-md-3">
                <?=
                $form->field($purchaseMaster, 'lpo_status', [
                    'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                ])->dropDownList($LpoStatus, [
                    'prompt' => 'Choose purchase status',
                    'class' => 'select from control',
                    'value'=>$purchaseMaster->lpo_status,
                ])->label("Document Status")
                ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3">
                <?= $form->field($purchaseMaster, 'reference')->textInput()->label('Reference') ?>

            </div>


            <div class="col-md-3">
                <?=
                $form->field($purchaseMaster, 'date_created', [
                    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
                ])->textInput([])->label('Date')
                ?>


            </div>
            <div class="col-md-3">
                <?= $form->field($purchaseMaster, 'amount')->textInput(['class' => "amount form-control",'readonly'=>true])->label('Amount') ?>

            </div>
            <div class="col-md-3">
                <?=
                $form->field($purchaseMaster, 'due_date', [
                    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
                ])->textInput([])->label('Delivery Expect Date')
                ?>

            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-3">
                <?=
                $form->field($purchaseMaster, 'currency', [
                    'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                ])->dropDownList($currencylist, [
                    'prompt' => 'Choose currency',
                    'class' => 'select from control',
                    'value'=>$purchaseMaster->currency
                ])->label("Currency")
                ?>

            </div>
            <div class="col-md-3">
                <?= $form->field($purchaseMaster, 'package')->textInput()->label('Package') ?>
            </div>
            <div class="col-md-3">
                <?=
                $form->field($purchaseMaster, 'payment_terms', [
                    'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                ])->dropDownList($termsTypeValue, [
                    'prompt' => 'Choose payment terms',
                    'class' => 'select from control',
                    'value'=>$purchaseMaster->payment_terms
                ])->label("Payment Terms")
                ?>

            </div>
            <div class="col-md-3">
                <?= $form->field($purchaseMaster, 'recommendation')->textInput()->label('Recommendation') ?>
            </div>

        </div>
        <br>
        <div class="row">

            <div class="col-md-3">
                <?= $form->field($purchaseMaster, 'supplier_ref')->textInput()->label('Supplier ref') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($purchaseMaster, 'provision_amount')->textInput()->label('Provision Amount') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($purchaseMaster, 'po_number')->textInput(['id'=>'po_number'])->label('PO Number') ?>
            </div>
            <div class="col-md-3">
                <?=
                $form->field($purchaseMaster, 'order_type', [
                    'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                ])->dropDownList($OrderType, [
                    //  'prompt' => 'Choose order type',
                    'class' => 'order_type select from control',
                    'value'=>$purchaseMaster->order_type,
                    //  'id'=>'OrderType',
                    //  'multiple'=>'multiple',
                ])->label("Order Type")
                ?>
            </div>
        </div>
        <br>
        <div class="row">


            <div class="col-md-3" id ='typeHide'>
                <label> MRV</label>
                <?php
                echo Select2::widget([
                    'name' => 'requestList',
                    'id'=>'mrv',
                    // 'data' => $codeList,
                    // 'value'=>$userProjectList,
                    'theme' => Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'Select mrv ...', 'multiple' => true, 'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
            </div>
        </div>

    </div>

    <br>


    <div class="col-md-12"><legend class="text-info"><small>Material</small></legend></div>
    <div class ="row" id="headingdiv">
        <div class="col-md-3" style="font-weight:bold">Material</div>
        <div class="col-md-2" style="font-weight:bold">Description</div>
        <div class="col-md-1" style="font-weight:bold">MRV Status</div>
        <div class="col-md-2" style="font-weight:bold">Expected Date</div>

        <div class="col-md-1" style="font-weight:bold"> Order Quantity</div>
        <!-- <div class="col-md-1" style="font-weight:bold">Unit</div> -->
        <div class="col-md-1" style="font-weight:bold">Price</div>
        <div class="col-md-1" style="font-weight:bold">Total</div>
        <div class="col-md-1" style="font-weight:bold">&nbsp;</div>
    </div>
    <div id="product">
    <?php for($i = 1; $i <= $itemNo; $i++) {?>
        <div class="row copyinvitems details after-add-more" id="copy">
            <div class="control-group drag">

                <div class="col-md-3 material-drop">

                    <?php



                    echo \kartik\tree\TreeViewInput::widget([
                        'name' => 'PurchaseMaster[material_id][]',
                        'value' => true,
                        'query' => \app\models\Tree::find()->addOrderBy('root, lft'),
                        'headingOptions' => ['label' => 'Store'],
                        'rootOptions' => ['label'=>'<i class="fas fa-tree text-success"></i>'],
                        'fontAwesome' => true,
                        'asDropdown' => true,
                        'multiple' => false,
                        'cacheSettings' => ['enableCache' => true],
                        'options' => ['disabled' => false]
                    ]);

                    ?>
                    </div>
                <div class="col-md-2"><textarea id="purchase-master-description" class="form-control  descrip" name="PurchaseMaster[description][]"></textarea></div>
                <div class="col-md-1"><select data-row ="0" id="purchase-master-mrvstatus" class="form-control  mrvstatus" name="PurchaseMaster[mrv_status][]">
                        <!-- <option value="">Please select MRV StauSs</option> -->
                        <?php

                        $mrv = [];

                        foreach ($mrvStatus as $mrvID=>$mrvValue) {

                            ?>
                            <option value="<?php echo $mrvID; ?>"><?php echo $mrvValue; ?></option>
                        <?php } ?>
                    </select></div>
                <div class="col-md-2"><input type="date" data-row ="0" id="purchase-master-expected-date" class="form-control  expected-date" name="PurchaseMaster[expected_date][]"></div>

                <div class="col-md-1"><input type="text" value="0" data-row="0" id="order-quantity-0" class="form-control orderqnty" name="PurchaseMaster[order_quantity][]" onchange="calc(this.id);"></div>
                <!-- <div class="col-md-1"><input type="text" id="purchase-master-unit" class="form-control required unit" name="PurchaseMaster[unit][]"></div> -->
                <div class="col-md-1"><input type="text" value="0" id="price-amount-0" class="form-control" name="PurchaseMaster[price][]" onchange="calc(this.id);"></div>
                <div class="col-md-1"><input type="text" class="form-control totalamount" readOnly = "true" value="0.00" id="total-amount-0" name="PurchaseMaster[total_amount][]" onchange="calc(this.id);"></div>
                <div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn  new-item-remove remove" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>
            </div>
        </div>
        <?php } ?>
    </div>

    <div class="purchase-box">
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class='col-md-12  padding-0'>
                    <div class='col-md-3 col-xs-12 padding-0'>

                    </div>
                    <div class='col-md-7 col-xs-12'>
                        <?=
                        $form->field($purchaseMaster, 'sub_total', [
                            'template' => "{label}\n
                <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                            'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                        ])->textInput(
                            [
                                'id' => 'subtotal',
                                'readOnly' => true,
                                'placeholder' => '0.00',
                                'class' => 'form-control subtotaltotal',
                                'onchange' => 'calc(this.id);',
                            ])->label('Sub Total')
                        ?>
                    </div>
                </div>
                <div class="col-md-12"><legend class="text-info"><small></small></legend></div>
                <?=
                $form->field($purchaseMaster, 'vat', [
                    'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                    'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                ])->textInput([
                    // 'class'=>'select from control',
                    'value'=>$vatcategory,
                    'readOnly' => true,
                    'placeholder' => '',
                    'id' => 'vat',
                    'onchange' => 'calc(this.id);',
                ])->label('VAT(%)')
                ?>
                <div class="col-md-12"><legend class="text-info"><small></small></legend></div>
                <?=
                $form->field($purchaseMaster, 'discount', [
                    'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                    'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                ])->textInput([

                    'placeholder' => '',
                    'id' => 'discount',
                    'onchange' => 'calc(this.id);',
                ])->label('Discount')
                ?>
                <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

                <?=
                $form->field($purchaseMaster, 'total_amount', [
                    'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                    'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                ])->textInput([
                    'readOnly' => true,
                    'placeholder' => '0.00',
                    'id' => 'totalamount',
                    //'class' => 'form-control total'

                ])->label('Grand Total')
                ?>
            </div>
            <div>&nbsp;</div>

        </div>
    </div>
    <div>&nbsp;</div>
    <div class = "row">
        <div class= "col-md-8">
            <?=
            $form->field($purchaseMaster, 'notes', [
                'template' => "{label}\n
            <div class='col-md-9 col-xs-12'>
            {input}\n
             </div>",
                'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
            ])->textarea([
                'readOnly' => true,
//            'placeholder' => '0.00',
                'id' => 'amountwords',
                'class' => 'form-control total text-bold'
            ])->label('Amount In Words')
            ?>
        </div>
    </div>
    <br>
    <div class = "row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($purchaseMaster->isNewRecord ? 'Submit' : 'Update', ['class' => $purchaseMaster->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                    <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="clearfix"></div>
</div>

<?php ActiveForm::end(); ?>
</div>

<?= $this->registerJs("


    
    $('#purchase-date_created').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });
      $('#purchase-due_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });

      $('body').on('click','.remove',function (){ 
         console.log('remove',('.remove'));
    $(this).parents('.control-group').parent().remove();
    calc('remove');
    
  });
 

      $(function () {
        $('body').on('change','#purchase-project_name',function (){ 
     
            var project_id = $(this).val();
            // alert(project_id);
            $.ajax({
                url: './get-material-list',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    console.log('data',data);
                    $('.material-drop').empty().html(data);
                 
                }
            });
        });
    });

    $(function () {
        $('body').on('change','#purchase-project_name',function (){ 
     
            var project_id = $(this).val();
            // alert(project_id);
            $.ajax({
                url: './get-reference-name',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    console.log('data',data);
                    $('#mrv').empty().append(data);
                 
                }
            });
        });
    });

    $(function () {
    $('body').on('click','#typeHide',function (){ 
       console.log('typeHide');
        $('#headingdiv').hide();
        $('#addButton').hide();
       
});
});
    $(function () {
        $('body').on('change','#purchase-order_type',function (){ 
     
             var orderType = $(this).val();
        if(orderType==='2'){
               $('#headingdiv').hide();
                $('#addButton').hide();
               }
            else{
              
                $('#headingdiv').show();
                $('#addButton').show();
                $('#product').show();
            }
           
        });
    });
    
    $(function () {
        $('body').on('change','.project_name',function (){ 
            
             var optionSelected = $(this).find(\"option:selected\");;
              var valueSelected  = optionSelected.val();
                var textSelected   = optionSelected.text();
                var lopId=$('#last_ponumber').val();
                var lponumber=lopId+'/'+textSelected;
                $('#po_number').val(lponumber);
                
            
           
        });
    });

    
   

    $('body').on('change','.material',function (){
        var material_id = $(this).val();
       
        var counterVal=$(this).attr('data-row');
        console.log('counterVal',counterVal);

       var project_id = $('#purchase-project_name').val();
      $.ajax({
           url: './get-total-quantity',
           data: {'project_id': project_id,'material_id':material_id},
           type: 'post',
           success: function (data) {
           
                $('#total-quantity-' + counterVal).val(data);
               
           }
       });
              
       });


    
", View::POS_READY); ?>

<?php
//$js = <<< JS
//
//
//        var counter = 1;
//    $(".add-more").click(function () {
//         var optionsValues = $("#purchase-master-material option").map(function() {return $(this).val();}).get();
//        var optionsText = $("#purchase-master-material option").map(function() {return $(this).text();}).get();
//
//         var optionsValueText = "";
//         $.each(optionsText , function(index, val) {
//
//            optionsValueText=optionsValueText+'<option value="'+optionsValues[index]+'">'+val+'</option>';
//});
//var statusoptionsValues = $("#purchase-master-mrvstatus option").map(function() {
//                   return $(this).val();
//                  }).get();
//                   console.log("Material");
//
//        var statusoptionsText = $("#purchase-master-mrvstatus option").map(function() {return $(this).text();}).get();
//
//         var mrvoptionsValueText = "";
//         $.each(statusoptionsText , function(index, val) {
//
//            mrvoptionsValueText=mrvoptionsValueText+'<option value="'+statusoptionsValues[index]+'">'+val+'</option>';
//});
//
//        var newContainer = $(document.createElement('div'));
//        newContainer.after().html('<div class="clear">&nbsp</div><div class="control-group col-md-12 padding-0">' +
//        '<div class="col-md-2"><select data-row = "'+counter+'"  id="purchase-master-material-' + counter +'" class="form-control required material" name="PurchaseMaster[material_id][] ">'+optionsValueText+'</select></div>'+
//        '<div class="col-md-2"><textarea id="purchase-master description" class="form-control" name="PurchaseMaster[description][]"></textarea></div>'+
//        '<div class="col-md-1"> <select  data-row = "'+counter+'" id="purchase-master-mrvstatus-' + counter +'" class="form-control  mrvstatus" name="PurchaseMaster[mrvstatus][] " onchange="calc(this.id);">'+mrvoptionsValueText+'</select></div>'+
//
//        '<div class="col-md-2"><input type="date" value="0" data-row = "purchase-master-expected-date-' + counter + '" id="purchase-master-expected-date-' + counter + '" class="form-control expected-date" name="PurchaseMaster[expected_date][]"></div>'+
//        '<div class="col-md-1"><input type="text" value="0" id="total-quantity-' + counter + '" class="form-control totalqnty" name="PurchaseMaster[total_quantity][]" onchange="calc(this.id);"></div>'+
//        '<div class="col-md-1"><input type="text" value="0" id="order-quantity-' + counter + '" class="form-control orderqnty" name="PurchaseMaster[order_quantity][]" onchange="calc(this.id);"></div>'+
//        // '<div class="col-md-1"><input type="text" id="purchase-master-unit" class="form-control required unit" name="PurchaseMaster[unit][]"></div>'+
//        '<div class="col-md-1"><input type="text" value="0" id="price-amount-' + counter + '" class="form-control" name="PurchaseMaster[price][]" onchange="calc(this.id);"></div>'+
//        '<div class="col-md-1"><input type="text" class="form-control totalamount" value="0.00" id="total-amount-' + counter + '" class="form-control" name="PurchaseMaster[total_amount][]" onchange="calc(this.id);"></div>'+
//        '<div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn  new-item-remove remove" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>'+
//        '<div class="clear">&nbsp</div>'+
//            '</div>');
//        newContainer.appendTo("#product");
//
//
//        counter++;
//    });
//
//
//
//
//JS;
//$this->registerJs($js);
//
//
//
//?>
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdn.materialdesignicons.com/5.0.45/css/materialdesignicons.min.css">
<link rel="stylesheet" href="/pricingcalculator/lib/css/comboTree-style.css">
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>


<script>
    $(function(){
        var counter=1;


    });
    $(function () {
        $('body').on('click','.kv-node-label',function (){
            alert($(this).text());
        $('.kv-carets span').text($(this).text());


        });
        $('#mrv').on('change',function (){
            var mrv = $(this).val();
            console.log('mrv',mrv);
            $.ajax({
                url: './get-total-material-data',
                data: {'mrv':mrv},
                type: 'post',
                success: function (data) {
                    // console.log('data',data);
                    $('#product').empty().append(data);

                }
            });

        });
    });


    $(function(){
        $("#typeHide").hide();
        $("#purchase-order_type").on("change", function(){
            var order = $(this).val();
            if(order==1){
                $("#typeHide").hide();
            }else{
                $("#typeHide").show();
            }


        });
    });


    function calc(id) {

        if(id!="remove"){
            var idPosition = id.split('-');
            // console.log('idPosition',idPosition[2]);
            var price = ($("#price-amount-"+idPosition[2]).val());

            // console.log('price',$("#order-quantity-"+idPosition[2]).val());
            $("#price-amount-"+idPosition[2]).val(price);
            var total = $("#order-quantity-"+idPosition[2]).val()*price;
            var tot = Number(total).toFixed(2);
            // console.log('total',total);
            $("#total-amount-"+idPosition[2]).val(tot)
        }



        if($('.performa-Invoice').is(':checked')) {
            var type = $("#customInvoicetype").val();
            var forpreamount = 0;
            $(".totalamount").each(function () {
                forpreamount += parseFloat($(this).val());
            });
            var presubtotal = Number(forpreamount).toFixed(2);

            var progressPercentage = $("#progress_percentage").val();
            var progressAmount = presubtotal * progressPercentage / 100;

            var advancePercentage = $("#advance_percentage").val();
            var advancAmount = presubtotal * advancePercentage / 100;

            var workdoneAmount = Number($("#workdone_amount").val()).toFixed(2);
            var certifiedAmount = Number($("#certified_amount").val()).toFixed(2);
            var contraAmount = Number($("#contra_charges").val()).toFixed(2);
            var rententionAmount = workdoneAmount * $("#retention_amount").val() / 100;
            var advancerecoveryAmount = workdoneAmount * $("#advance_recovery").val() / 100;
            var total = parseFloat(certifiedAmount) + parseFloat(contraAmount) + parseFloat(rententionAmount) + parseFloat(advancerecoveryAmount);
            var netAmount = workdoneAmount - total;
            if (type == 0){
                $("#subtotal").val(netAmount);
            }else if (type == 1){
                $("#subtotal").val(advancAmount);
            } else {
                $("#subtotal").val(progressAmount);
            }
        }
        else{
            var subTotal = 0;
            $(".totalamount").each(function () {
                subTotal += parseFloat($(this).val());
            });
            var subTotalFormatted = Number(subTotal).toFixed(2);
            $("#subtotal").val(subTotalFormatted);
        }

        var subtotal = Number($("#subtotal").val()).toFixed(2);
        var discountamount = Number($("#discount").val());
// console.log('discountamount',discountamount);
        $("#subtotal").val(subtotal);
        var totamntbeforeVat;
        totamntbeforeVat = subtotal;
        var discrate = $("#discountrate").val();
        if(discrate>0) {
            var discamount = $("#discountrate").val()*subtotal/100;
            var discountformatted = Number(discamount).toFixed(2)
            $("#discountamount").val(discountformatted);
            totamntbeforeVat = subtotal- discountformatted;
        }


        //console.log("Before vat amout",totamntbeforeVat);
        var vat = Number($("#vat").val()).toFixed(2)
        $("#vat").val(vat);
        var total, vatAmount;

        if(vat>0) {
            vatAmount = totamntbeforeVat*(vat)/100;
        } else {
            vatAmount = 0;
        }


        total = parseFloat(totamntbeforeVat) + parseFloat(vatAmount) -discountamount;
        //console.log("Vat value",vat);
        //console.log("Total amount",total);
        var tot = Number(total).toFixed(2);
        $("#totalamount").on("change", function(){
            var data = $(this).val();
            // console.log('data',data);
            $.ajax({
                url: './amount',
                data: {'data': data},
                type: 'POST',
                success: function (response) {
// console.log('response',response);
                    $('#amountwords').val(response);
                    $('#purchase-amount').val(data);
                    console.log(response);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });

        $("#totalamount").val(tot).trigger('change');
        // var amountWords = \app\components\Helper::numtowords2(tot);



    }



</script>





