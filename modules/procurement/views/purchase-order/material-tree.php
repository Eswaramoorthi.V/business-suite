<?php



echo \kartik\tree\TreeViewInput::widget([
    'name' => 'project_directory_id2',
    'value' => 'true', // preselected values
    'query' => $item,
    'headingOptions' => ['label' => 'Store'],
    'rootOptions' => ['label'=>'<i class="fas fa-tree text-success"></i>'],
    'fontAwesome' => true,
    'asDropdown' => true,
    'multiple' => false,
    'cacheSettings' => ['enableCache' => true],
    'options' => ['disabled' => false]
]);

?>