<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
//  $purchaseMaster->date_created = date('d-m-Y', strtotime($purchaseMaster['date_created']));
//  $purchaseMaster->due_date = date('d-m-Y', strtotime($purchaseMaster['due_date']));
 

$this->title = 'Delivery Order';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/purchase/purchase/index']];
$this->params['breadcrumbs'][] = $this->title;
//pr($purchaseDelivery);
?>

<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
<?php $form = ActiveForm::begin(['id'=>'purchase-delivery-form','method'=>'post','action'=>'delivery?id='.$_GET['id']]); ?>
       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
   
<div class ="row">
<div class="col-md-2" style="font-weight:bold">Material</div>
    <div class="col-md-2" style="font-weight:bold">Description</div>
    <div class="col-md-1" style="font-weight:bold">Material Status</div>
    <div class="col-md-2" style="font-weight:bold">Delivery Date</div>
    <div class="col-md-1" style="font-weight:bold">Total Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Purchased Quantity</div>
    <div class="col-md-1" style="font-weight:bold"> Remaining Quantity</div>
    <!-- <div class="col-md-1" style="font-weight:bold">Unit</div> -->
    <!-- <div class="col-md-1" style="font-weight:bold">Price</div>
    <div class="col-md-1" style="font-weight:bold">Total</div> -->
    <!-- <div class="col-md-1" style="font-weight:bold">&nbsp;</div> -->
</div>
<div id="product">
<?php foreach ($purchaseDelivery as $request) { ?>



<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-2"><select disabled id="purchase-delivery-material-0<?php echo $request['purchase_id']; ?>" class="form-control required material" name="PurchaseDelivery[resource_id][]"  >
    
   
</select>
    
                                     
    </div>
    <div class="col-md-2"><textarea  readonly="true"  data-row ="<?php echo $request['id']; ?>"   id="purchase-delivery-description" class="form-control  description" name="PurchaseDelivery[description][]"><?php echo $request['description'];?></textarea></div>
    <div class="col-md-1"><select data-row ="<?php echo $request['id']; ?>" id="purchase-delivery-materialstatus" class="form-control  materialstatus" name="PurchaseDelivery[material_status][]">
    
    <option value="">Please select status</option>
        <?php foreach ($materialStatusdata as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$request['material_status']){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
    </select></div>
    <div class="col-md-2"><input  type="date"  value ="<?php echo $request['delivery_date'];?>" data-row ="<?php echo $request['id']; ?>" id="purchase-delivery-delivery-date-<?php echo $request['id']; ?>" class="form-control  delivery-date" name="PurchaseDelivery[delivery_date][]"></div>
    <div class="col-md-1"><input   readonly="true" type="text" value="<?php echo $request['total_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="total-quantity-<?php echo $request['id']; ?>" class="form-control totalqnty" name="PurchaseDelivery[total-quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input  readonly="true"  type="text" value="<?php echo $request['purchased_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="purchased-quantity-<?php echo $request['id']; ?>" class="form-control purchanseqnty" name="PurchaseDelivery[purchased-quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input   readonly="true" type="text" value="<?php echo $request['remaining_quantity']; ?>"data-row ="<?php echo $request['id']; ?>" id="quantity-<?php echo $request['id']; ?>" class="form-control qnty" name="PurchaseDelivery[remaining-quantity][]" onchange="calc(this.id);"></div>
    <input type="hidden" value="<?php echo $request['purchase_id'];?>" name="PurchaseDelivery[PO_id][]"> 
   
</div>
</div>

<div class="row">&nbsp;</div> 
<?php } ?>
</div>

<div>&nbsp;</div>

<div class = "row">
<div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                <?= Html::submitButton($purchaseDelivery->isNewRecord ? 'Submit' : 'Update', ['class' => $purchaseDelivery->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                    <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
 </div>
 




  