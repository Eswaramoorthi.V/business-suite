<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\select2\Select2;
use app\models\PurchaseRequestDetails;
 $purchaseMaster->date_created = date('d-m-Y', strtotime($purchaseMaster['date_created']));
 $purchaseMaster->due_date = date('d-m-Y', strtotime($purchaseMaster['due_date']));
 

$vatcategory=\Yii::$app->params['VATRate'];

$currentUser=Yii::$app->user->getId();

$fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

$payment_list= [1=>'Payment after 30 days',2=>'Payment after 90 days'];

$role = current($fetchRole);
$roleName = $role->name;
$this->title = 'Update Purchase Order';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/purchase/purchase/index']];
$this->params['breadcrumbs'][] = $this->title;
// pr($purchaseMaster);
$purchaseStatus= [1=>'Purchase Created',2=>'Purchase Issued',3=>'Waiting for delivery',4=>'Delivered'];
$OrderType= [1=>'Direct',2=>'MRV'];



?>

<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
    <?php $form = ActiveForm::begin(['id'=>'purchase-master-form']); ?>
       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
         <div class="row">
        <!-- <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> Purchase Oder </small></legend></div> -->
         </div>
         <div class="row">
         <div class="col-md-3 hide">
     
       
        <?=
            $form->field($purchaseMaster, 'company_name', [
                'template' => "{label}\n
        <div class=''>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($companylist, [
                'prompt' => 'Choose company',
                'class' => 'project_name select from control',
                'value'=>$purchaseMaster->company_name
            ])->label("Company Name")
            ?>
    </div>
       
    <div class="col-md-3">
     
       
    <label> Project Code</label>
                <?php
                echo Select2::widget([
                    'name' => 'project_name',
                    'id'=>'project_name',
                    'data' => $projectList,
                    'value'=>$purchaseMaster->project_name,
                    'theme' => Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'Select project code ...',  'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
 </div>
    <div class="col-md-3">
    <label> Supplier Name</label>
                <?php
                echo Select2::widget([
                    'name' => 'supplier_name',
                    'id'=>'supplier_name',
                    'data' => $supplierData,
                    'value'=>$purchaseMaster->supplier_name,
                    'theme' => Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'Select supplier name ...',  'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

  </div>
 
    <div class="col-md-3">
     
     <?= $form->field($purchaseMaster, 'originator')->textInput(['value'=>$roleName])->label('Originator') ?>
 </div>
 <div class="col-md-3">
 <label> Document Status</label>
                <?php
                echo Select2::widget([
                    'name' => 'lpo_status',
                    'id'=>'lpo_status',
                    'data' => $LpoStatus,
                    'value'=>$purchaseMaster->lpo_status,
                    'theme' => Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'Select document status ...',  'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

</div>
    </div>
    <br>
    <div class="row">
    <div class="col-md-3">
    <?= $form->field($purchaseMaster, 'reference')->textInput()->label('Reference') ?>
        
    </div>


    <div class="col-md-3">
    <?=
            $form->field($purchaseMaster, 'date_created', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Date')
            ?>
     
        
    </div>
    <div class="col-md-3">
    <?= $form->field($purchaseMaster, 'amount')->textInput()->label('Amount') ?>
   
    </div>
    <div class="col-md-3">
    <?=
            $form->field($purchaseMaster, 'due_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Delivery Date')
            ?>
   
    </div>
    </div>
    <br>
    <div class="row">
    <div class="col-md-3">

    <label> Currency</label>
                <?php
                echo Select2::widget([
                    'name' => 'currency',
                    'id'=>'currency',
                    'data' => $currencylist,
                    'value'=>$purchaseMaster->currency,
                    'theme' => Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'Select currency ...',  'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

      
       
    </div>
    <div class="col-md-3">
    <?= $form->field($purchaseMaster, 'package')->textInput()->label('Package') ?>
    </div>
    <div class="col-md-3">
    <label> Payment Terms</label>
                <?php
                echo Select2::widget([
                    'name' => 'payment_terms',
                    'id'=>'payment_terms',
                    'data' => $termsTypeValue,
                    'value'=>$purchaseMaster->payment_terms,
                    'theme' => Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'Select Payment Terms ...',  'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

  
    </div>
    <div class="col-md-3">
    <?= $form->field($purchaseMaster, 'recommendation')->textInput()->label('Recommendation') ?>
    </div>

</div>
<br>
<div class="row">

<div class="col-md-3">
<?= $form->field($purchaseMaster, 'supplier_ref')->textInput()->label('Supplier ref') ?>
</div>
<div class="col-md-3">
<?= $form->field($purchaseMaster, 'provision_amount')->textInput()->label('Provision Amount') ?>
</div>
<div class="col-md-3">
<?= $form->field($purchaseMaster, 'po_number')->textInput()->label('PO Number') ?>
</div>
<div class="col-md-3">
<label> Order Type</label>
                <?php
                echo Select2::widget([
                    'name' => 'order_type',
                    'id'=>'order_type',
                    'data' => $OrderType,
                    'value'=>$purchaseMaster->order_type,
                    'theme' => Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'Select Order Type ...',  'autocomplete' => 'off'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>


</div>
</div>
<div class="row">
<div class="col-md-3" id ='typeHide'>
<label>Assign MRV</label>
        <?php
            echo Select2::widget([
                'name' => 'codeList',
                'id'=>'mrv',
                'data' => $codeList,
                'value'=>$requestedName,
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => 'Request order...', 'multiple' => true, 'autocomplete' => 'off'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
</div>
</div>
<br>


<div>&nbsp;</div>  
<div class="col-md-12"><legend class="text-info"><small>Material</small></legend></div>
<div class ="row">
<div class="col-md-2" style="font-weight:bold">Material</div>
    <div class="col-md-1" style="font-weight:bold">Description</div>
    <div class="col-md-1" style="font-weight:bold">Mrv Status</div>
    <div class="col-md-2" style="font-weight:bold">Expected Date</div>
   <div class="col-md-1" style="font-weight:bold">Order Quantity</div>
  
    <div class="col-md-1" style="font-weight:bold">Price</div>
    <div class="col-md-1" style="font-weight:bold">Total</div>
    <div class="col-md-1" style="font-weight:bold">Document</div>
    <!-- <div class="col-md-1" style="font-weight:bold">&nbsp;</div> -->
</div>
<div id="product">
<?php foreach ($purchaseMaster->purchase as $key =>$purchase) { ?>



<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-2"><select id="purchase-master-material-0<?php echo $purchase->id; ?>" class="form-control required material" name="PurchaseMaster[material_id][]"  >
    
    <option value="">Please select material</option>
        <?php foreach ($resourceNameListData as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$purchase->material_id){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
</select>
    
                                     
    </div>
    <div class="col-md-1"><textarea id="purchase-master-description" class="form-control  descrip" name="PurchaseMaster[description][]"><?php echo $purchase->description;?></textarea></div>
    <div class="col-md-1"><select data-row ="0" id="purchase-master-mrvstatus<?php echo $purchase->id; ?>" class="form-control  mrvstatus" name="PurchaseMaster[mrv_status][]">
    
    <!-- <option value=""> select </option> -->
        <?php foreach ($mrvStatus as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$purchase->mrv_status){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
    </select></div>
    <div class="col-md-2"><input type="date"  value ="<?php echo $purchase->expected_date;?>" data-row ="0" id="purchase-master-expected-date" class="form-control  expected-date" name="PurchaseMaster[expected_date][]"></div>
    <div class="col-md-1"><input type="text" value="<?php echo $purchase->quantity; ?>" id="total-quantity-0<?php echo $purchase->id; ?>" class="form-control totalqnty" name="PurchaseMaster[total_quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" value="<?php echo $purchase->order_quantity; ?>" id="order-quantity-0<?php echo $purchase->id; ?>" class="form-control orderqnty" name="PurchaseMaster[order_quantity][]" onchange="calc(this.id);"></div>
    <!-- <div class="col-md-1"><input type="text" id="invoice-unit" class="form-control required unit" name="PurchaseMaster[unit][]"></div> -->
    <div class="col-md-1"><input type="text" value="<?php echo number_format($purchase->price, 2, '.' ,''); ?>" id="price-amount-0<?php echo $purchase->id; ?>" class="form-control" name="PurchaseMaster[price][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" class="form-control totalamount" readOnly = "true" value="<?php echo number_format($purchase->total_amount, 2, '.', '');?>" id="total-amount-0<?php echo $purchase->id; ?>" name="PurchaseMaster[total_amount][]" onchange="calc(this.id);"></div>
    <div class="col-md-1" ><input type="file" accept="image/*"  data-row="0" value="<?php echo $purchase->document; ?>"  id="document-0<?php echo $purchase->id; ?>"  name="PurchaseMaster[document][]">
                                                              </div>
    <?php if($key!=0){ ?>
    <div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn  new-item-remove remove" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>
    <?php } ?>
    <input type="hidden" value="<?php echo $purchase->id;?>" name="PurchaseMaster[id][]"> 
  </div>
</div>

<div class="row">&nbsp;</div> 
<?php } ?>
</div>



   <div class="col-md-12" input-group-btn style="margin-top:21px">
       <div class="row">
 <div class="col-md-2">
        <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> ADD</button>
 </div>
            
       </div> 
    </div>




<div class="status-box hide">
    <div class="row">
       
        <div>&nbsp;</div>

    </div>
</div>
<div>&nbsp;</div>




        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>


        <div class="purchase-box">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class='col-md-12  padding-0'>
                <div class='col-md-3 col-xs-12 padding-0'>
               
            </div>
            <div class='col-md-7 col-xs-12'>
            <?=
            $form->field($purchaseMaster, 'sub_total', [
                'template' => "{label}\n
                <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
             'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
            ])->textInput(
                    [   
                        'id' => 'subtotal',
                        'readOnly' => true, 
                        'placeholder' => '0.00',
                        'class' => 'form-control subtotaltotal',
                        'onchange' => 'calc(this.id);',
                    ])->label('Sub Total')
            ?>
                </div>
            </div>
            <div class="col-md-12"><legend class="text-info"><small></small></legend></div> 
            <?=
                    $form->field($purchaseMaster, 'vat', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        // 'class'=>'select from control',
                        'value'=>$vatcategory,
                        'readOnly' => true,
                        'placeholder' => '',
                        'id' => 'vat',
                        'onchange' => 'calc(this.id);',
                    ])->label('VAT(%)')
                    ?>
                     <div class="col-md-12"><legend class="text-info"><small></small></legend></div> 
            <?=
                    $form->field($purchaseMaster, 'discount', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                       
                        'placeholder' => 'discount',
                        'id' => 'discount',
                        'onchange' => 'calc(this.id);',
                    ])->label('Discount')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

            <?=
            $form->field($purchaseMaster, 'total_amount', [
                'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
            ])->textInput([
                'readOnly' => true, 
                'placeholder' => '0.00',
                'id' => 'totalamount',
                 //'class' => 'form-control total'
                
                ])->label('Grand Total')
            ?>
        </div>
        <div>&nbsp;</div>

    </div>
</div>
<div>&nbsp;</div>
<div class = "row">
<div class= "col-md-8">
        <?=
        $form->field($purchaseMaster, 'notes', [
            'template' => "{label}\n
            <div class='col-md-9 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
        ])->textarea([
           'readOnly' => true,
//            'placeholder' => '0.00',
            'id' => 'amountwords',
            'class' => 'form-control total text-bold'
        ])->label('Amount In Words')
        ?>
        </div>
</div>
<br>
<div class="col-md-12"><legend class="text-info"><small></small></legend></div>
<div class="col-md-12"> 
    <div class="form-group">
        <div class="col-md-12 col-xs-12">
<?= Html::submitButton($purchaseMaster->isNewRecord ? 'Submit' : 'Update', ['class' => $purchaseMaster->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
<?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
</div>
</div>
</div>
<?php
ActiveForm::end();
?>
 </div>
<?= $this->registerJs("
    
    $('#purchase-date_created').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });
      $('#purchase-due_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });

      $('body').on('click','.remove',function (){ 
        // console.log('remove',('.remove'));
    $(this).parents('.control-group').parent().remove();
    calc('remove');
    
  });

  $(function () {
    $('body').on('change','#purchase-project_name',function (){ 
 
        var project_id = $(this).val();
        // alert(project_id);
        $.ajax({
            url: './get-reference-name',
            data: {'project_id': project_id},
            type: 'post',
            success: function (data) {
                console.log('data',data);
                $('#mrv').empty().append(data);
             
            }
        });
    });
});

$(function () {
$('body').on('click','#typeHide',function (){ 
  
    $('#headingdiv').hide();
    $('#addButton').hide();
   
});
});
$(function () {
    $('body').on('change','#purchase-order_type',function (){ 
 
           var orderType = $(this).val();
       
        if(orderType==='2'){
           $('#headingdiv').hide();
            $('#addButton').hide();
           
        }
        else{
            
            $('#headingdiv').show();
            $('#addButton').show();
            $('#product').show();
        }
       
    });
});


$(function () {
    $('body').on('change','#mrv',function (){ 
 
        var mrv = $(this).val();
     $.ajax({
        url: './get-total-material-data',
        data: {'mrv':mrv},
        type: 'post',
        success: function (data) {
        console.log('data',data);
        $('#product').empty().append(data);
            
        }
    });

    });
});

$(function(){
    $('#typeHide').hide();
    $('body').on('change','#purchase-order_type',function (){ 
    
        var order = $(this).val();
        console.log('order',order);
        if(order==1){
       $('#typeHide').hide();
         }else{
       $('#typeHide').show();
           }
             });
             });


      $(function () {
        $('body').on('change','#purchase-project_name',function (){ 
     
            var project_id = $(this).val();
            // alert(project_id);
            $.ajax({
                url: './get-material-list',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    console.log('data',data);
                    $('.material').empty().append(data);
                 
                }
            });
        });
    });
    
", View::POS_READY); ?>

<?php 
$js = <<< JS

      
    var counter = 1;
    $(".add-more").click(function () {
        var optionsValues = $('#purchase-master-material-0'+counter+'>option').map(function() {return $(this).val();}).get();
        var optionsText = $('#purchase-master-material-0'+counter+'>option' ).map(function() {return $(this).text();}).get();
         var optionsValueText = "";
         $.each(optionsText , function(index, val) { 
optionsValueText=optionsValueText+'<option value="'+optionsValues[index]+'">'+val+'</option>';
});
var values = $('#purchase-master-mrvstatus'+counter+'>option').map(function() { return $(this).val(); });
var text = $('#purchase-master-mrvstatus'+counter+'>option').map(function() { return $(this).text(); });

var statusoptionsValueText = "";
         $.each(text , function(index, val) { 
             
            statusoptionsValueText=statusoptionsValueText+'<option value="'+values[index]+'">'+val+'</option>';
});


        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp</div><div class="control-group col-md-12 padding-0">' +
        '<div class="col-md-2"><select id="purchase-master-material-0' + counter +'" class="form-control required material" name="PurchaseMaster[material_id][] ">'+optionsValueText+'</select></div>'+
        '<div class="col-md-1"><textarea id="purchase-master description" class="form-control" name="PurchaseMaster[description][]"></textarea></div>'+
        '<div class="col-md-1"><select  data-row = "'+counter+'" id="purchase-master-mrvstatus' + counter +'" class="form-control  mrvstatus" name="PurchaseMaster[mrvstatus][] " onchange="calc(this.id);">'+statusoptionsValueText+'</select></div>'+
        
        '<div class="col-md-2"><input type="date"  data-row = "' + counter + '" id="purchase-master-expected-date-' + counter + '" class="form-control expected_date" name="PurchaseMaster[expected-date][]"></div>'+
        '<div class="col-md-1"><input type="text" value="0" id="total-quantity-' + counter + '" class="form-control" name="PurchaseMaster[total-quantity][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1"><input type="text" value="0" id="order-quantity-' + counter + '" class="form-control" name="PurchaseMaster[order-quantity][]" onchange="calc(this.id);"></div>'+
        // '<div class="col-md-1"><input type="text" id="purchase-master-unit" class="form-control required unit" name="PurchaseMaster[unit][]"></div>'+
        '<div class="col-md-1"><input type="text" value="0.00" id="price-amount-' + counter + '" class="form-control" name="PurchaseMaster[price][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1"><input type="text" class="form-control totalamount" value="0.00" id="total-amount-' + counter + '" class="form-control" name="PurchaseMaster[total_amount][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1"><input type="file" data-row = "'+counter+'"  value="0"  id="document-' + counter + '"  name="PurchaseMaster[document][]"></div>'+
        '<div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn  new-item-remove remove" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>'+
        '<div class="clear">&nbsp</div>'+
            '</div>');
        newContainer.appendTo("#product");
        counter++;
    });

    
 
                
JS;
$this->registerJs($js);

?>

<script>
    function calc(id) {
       // alert('here');
       if(id!="remove"){
        var idPosition = id.split('-');
            // console.log('idPosition',idPosition[2]);
            var price = ($("#price-amount-"+idPosition[2]).val());

            console.log('price',$("#order-quantity-"+idPosition[2]).val());
            $("#price-amount-"+idPosition[2]).val(price);
            var total = $("#order-quantity-"+idPosition[2]).val()*price;
            var tot = Number(total).toFixed(2);
            console.log('total',total);
            $("#total-amount-"+idPosition[2]).val(tot)
        }
        
        
    

         if($('.performa-Invoice').is(':checked')) {
           var type = $("#customInvoicetype").val();
             var forpreamount = 0;
             $(".totalamount").each(function () {
                 forpreamount += parseFloat($(this).val());
             });
             var presubtotal = Number(forpreamount).toFixed(2);

             var progressPercentage = $("#progress_percentage").val();
             var progressAmount = presubtotal * progressPercentage / 100;

             var advancePercentage = $("#advance_percentage").val();
             var advancAmount = presubtotal * advancePercentage / 100;

             var workdoneAmount = Number($("#workdone_amount").val()).toFixed(2);
             var certifiedAmount = Number($("#certified_amount").val()).toFixed(2);
             var contraAmount = Number($("#contra_charges").val()).toFixed(2);
             var rententionAmount = workdoneAmount * $("#retention_amount").val() / 100;
             var advancerecoveryAmount = workdoneAmount * $("#advance_recovery").val() / 100;
             var total = parseFloat(certifiedAmount) + parseFloat(contraAmount) + parseFloat(rententionAmount) + parseFloat(advancerecoveryAmount);
             var netAmount = workdoneAmount - total;
             if (type == 0){
                 $("#subtotal").val(netAmount);
             }else if (type == 1){
                 $("#subtotal").val(advancAmount);
             } else {
                 $("#subtotal").val(progressAmount);
             }
         }
         else{
            var subTotal = 0;
            $(".totalamount").each(function () {
                subTotal += parseFloat($(this).val());
            });
            var subTotalFormatted = Number(subTotal).toFixed(2); 
            $("#subtotal").val(subTotalFormatted);
        }
            
            var subtotal = Number($("#subtotal").val()).toFixed(2)
            $("#subtotal").val(subtotal);
            var discountamount = Number($("#discount").val());
            var totamntbeforeVat;
            totamntbeforeVat = subtotal;
            var discrate = $("#discountrate").val();
            if(discrate>0) {
                var discamount = $("#discountrate").val()*subtotal/100;
                var discountformatted = Number(discamount).toFixed(2)
                $("#discountamount").val(discountformatted);
                totamntbeforeVat = subtotal- discountformatted;
            } 
            

            //console.log("Before vat amout",totamntbeforeVat);
            var vat = Number($("#vat").val()).toFixed(2)
            $("#vat").val(vat);
            var total, vatAmount;
            
            if(vat>0) {
                   vatAmount = totamntbeforeVat*(vat)/100;
            } else {
                   vatAmount = 0;
            }
            
            total = parseFloat(totamntbeforeVat) + parseFloat(vatAmount)-discountamount;
            //console.log("Vat value",vat);
            //console.log("Total amount",total);
            var tot = Number(total).toFixed(2);
        $("#totalamount").on("change", function(){
            var data = $(this).val();

            $.ajax({
                url: './amount',
                data: {'data': data},
                type: 'POST',
                success: function (response) {

                    $('#amountwords').val(response);
                    console.log(response);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
        $("#totalamount").val(tot).trigger('change');
        // var amountWords = \app\components\Helper::numtowords2(tot);

            
        }

    
    
   </script> 
   

 
  