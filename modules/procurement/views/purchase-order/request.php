<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
//  $purchaseMaster->date_created = date('d-m-Y', strtotime($purchaseMaster['date_created']));
//  $purchaseMaster->due_date = date('d-m-Y', strtotime($purchaseMaster['due_date']));
 

$vatcategory=\Yii::$app->params['VATRate'];

$currentUser=Yii::$app->user->getId();

$fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

$payment_list= [1=>'Payment after 30 days',2=>'Payment after 90 days'];

$role = current($fetchRole);
$roleName = $role->name;
$this->title = 'Update Purchase Order';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/purchase/purchase/index']];
$this->params['breadcrumbs'][] = $this->title;
// pr($purchaseMaster);
$purchaseStatus= [1=>'Purchase Created',2=>'Purchase Issued',3=>'Waiting for delivery',4=>'Delivered'];
$baseurl = Yii::$app->params['domain_url'];
// pr($baseurl);
?>

<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
   
<div class ="row">
<div class="col-md-1" style="font-weight:bold">Material</div>
    <div class="col-md-1" style="font-weight:bold">Description</div>
    <div class="col-md-1" style="font-weight:bold">MRV Status</div>
    <div class="col-md-1" style="font-weight:bold"> Unit</div>
    <div class="col-md-2" style="font-weight:bold">Expected Date</div>
    <div class="col-md-1" style="font-weight:bold">Document</div>
    <div class="col-md-1" style="font-weight:bold"> Request Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Remaining Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Order Quantity</div>

    <div class="col-md-1" style="font-weight:bold">Price</div>
    <div class="col-md-1" style="font-weight:bold">Total</div>
  
</div>
<div id="product">
<?php foreach ($arrayData as $request) {
       $materailId=$request['material_id'];
       $mrvStatus=$request['mrv_status'];
       $uintTypeId=$request['unit'];
    ?>



<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-1">

        <?php

        $options['id']='purchase-master-material';
        $options['disabled']='disabled';
        $options['class']='form-control required material dropdownclass';
        $options['data-row']=0;

        echo Html::dropDownList('PurchaseMaster[material_id][]', $materailId, $resourceNameListData,$options);?>
    </div>
    <div class="col-md-1">
        <textarea  readonly = "true" data-row ="<?php echo $request['id']; ?>"   id="purchase-master-description" class="form-control  description" name="PurchaseMaster[description][]"><?php echo $request['description'];?></textarea></div>
    <div class="col-md-1">

        <?php
            $options['id']='purchase-master-mrvstatus';
            $options['disabled']='disabled';
            $options['class']='dropdownclass';
            $options['data-row']=0;

         echo Html::dropDownList('PurchaseMaster[mrv_status][]', $mrvStatus, $mrvSatusData,$options);?>

    </div>
    <div class="col-md-1">
        <?php

        $options['id']='purchase-master-unit';
        $options['disabled']='disabled';
        $options['class']='form-control  unit dropdownclass';
        $options['data-row']=0;

        echo Html::dropDownList('PurchaseMaster[unit][]', $uintTypeId, $unitType,$options);?>

    </div>
    
    <div class="col-md-2"><input type="date"  readonly value ="<?php echo $request['expected_date'];?>" data-row ="<?php echo $request['id']; ?>" id="purchase-master-expected-date-<?php echo $request['id']; ?>" class="form-control  expected-date" name="PurchaseMaster[expected_date][]"></div>
<?php if($request['document']!=''){ ?>
    <div class="col-md-1" ><a href="<?php echo $baseurl.'web/mrv/attachment/'.$request['document'];?>" target="_blank" data-pjax="0"><img class="mrvAttachment" src="<?php echo $baseurl.'web/mrv/attachment/'.$request['document'];?>"  id ="document-<?php echo $request['id'];?>" width="30px" height="30px"></a>
                 </div>
                 <?php } ?>
                 <?php if($request['document'] ==''){ ?>
    <div class="col-md-1" >
                 </div>
                 <?php } ?>
    
    <div class="col-md-1"><input type="text" readonly = "true" value="<?php echo $request['material_request_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="request-quantity-<?php echo $request['id']; ?>" class="form-control request-qnty" name="PurchaseMaster[request_quantity][]" ></div>
        <div class="col-md-1"><input type="text"  value="<?php echo $request['remaining_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="remaining-quantity-<?php echo $request['id']; ?>" class="form-control remaining-qnty" name="PurchaseMaster[remaining_quantity][]" ></div>

        <div class="col-md-1"><input type="text" value="0" data-row ="<?php echo $request['id']; ?>" id="order-quantity-<?php echo $request['id']; ?>" class="form-control order-qnty" name="PurchaseMaster[order_quantity][]" onblur="validate(this.id);"></div>

    <!-- <div class="col-md-1"><input type="text" id="purchase-master-unit" class="form-control required unit" name="PurchaseMaster[unit][]"></div> -->
    <div class="col-md-1"><input type="text" value="0" data-row ="<?php echo $request['id']; ?>" id="price-amount-<?php echo $request['id']; ?>" class="form-control" name="PurchaseMaster[price][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" data-row ="<?php echo $request['id']; ?>" class="form-control totalamount" readOnly = "true" value="0.00" id="total-amount-<?php echo $request['id']; ?>" name="PurchaseMaster[total_amount][]" onchange="calc(this.id);"></div>
        <input type="hidden" value="<?php echo $request['id']; ?>"data-row ="<?php echo $request['id']; ?>" id="request-<?php echo $request['id']; ?>" class="form-control order-qnty" name="PurchaseMaster[requestid][]">
</div>
</div>

<div class="row">&nbsp;</div> 
<?php } ?>
</div>

<div>&nbsp;</div>

<div class="col-md-12"><legend class="text-info"><small></small></legend></div>
<div class="col-md-12"> 
    <div class="form-group">
        <div class="col-md-12 col-xs-12">

        </div>
    </div>
</div>
</div>
</div>

 </div>
<?= $this->registerJs("
    
    $('#purchase-date_created').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });
      $('#purchase-due_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });

      $('body').on('click','.remove',function (){ 
        // console.log('remove',('.remove'));
    $(this).parents('.control-group').parent().remove();
    calc('remove');
    
  });

      $(function () {
        $('body').on('change','#purchase-project_name',function (){ 
     
            var project_id = $(this).val();
            // alert(project_id);
            $.ajax({
                url: './get-material-list',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    console.log('data',data);
                    $('.material').empty().append(data);
                 
                }
            });
        });
    });

    $(function () {
        $('body').on('change','#purchase-project_name',function (){ 
     
            var project_id = $(this).val();
            // alert(project_id);
            $.ajax({
                url: './get-reference-name',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    console.log('data',data);
                    $('#mrv').empty().append(data);
                 
                }
            });
        });
    });

    $(function () {
        $('body').on('change','#purchase-order_type',function (){ 
     
            var orderType = $(this).val();
            // alert(project_id);
            if(orderType==='1'){
                $('#typeHide').show();
            }
            else{
                $('#typeHide').hide();
            }
           
        });
    });

    
    // $(function () {
    //     $('body').on('change','#mrv',function (){ 
     
    //         var mrv = $(this).val();
    //       console.log('mrv',mrv);
    //       $.ajax({
    //         url: './get-total-material-data',
    //         data: {'mrv':mrv},
    //         type: 'post',
    //         success: function (data) {
    //         console.log('data',data);
    //             //  $('#total-quantity-' + counterVal).val(data);
                
    //         }
    //     });
           
    //     });
    // });

    
", View::POS_READY); ?>


<script>


$(function () {
    $("form").submit(function() {
        $(".dropdownclass").prop("disabled", false);
    });
        $('#mrv').on('change',function (){

            var mrv = $(this).val();
            $.ajax({
                url: './get-total-material-data',
                data: {'mrv':mrv},
                type: 'post',
                success: function (data) {
                    console.log('data',data);
                    $('#product').empty().append(data);

                }
            });

        });
    });

    function validate(id) {
        var idPosition = id.split('-');
        var idValue=idPosition[2];
        console.log(idValue);

        var orderQuantity = Number($("#remaining-quantity-"+idValue).val());
        var requestQuantity = Number($("#order-quantity-"+idValue).val());
        var remainingQuantityValue = orderQuantity-requestQuantity;
        console.log(remainingQuantityValue);
        if(remainingQuantityValue<0){
          //  var request=0;
          console.log('if');
            $("#order-quantity-"+idValue).val(0);

        }else {
            console.log('else');
            $("#remaining-quantity-"+idValue).val(remainingQuantityValue);
        }

    }


    function calc(id) {
      
     
            var idPosition = id.split('-');
          
            var price = ($("#price-amount-"+idPosition[2]).val());
            $("#price-amount-"+idPosition[1]).val(price);

            var total = $("#order-quantity-"+idPosition[2]).val()*price;
            console.log('total',total);
            var tot = Number(total).toFixed(2); 
            // console.log('tot',tot);
            $("#total-amount-"+idPosition[2]).val(tot)
        
        
        
         if($('.performa-Invoice').is(':checked')) {
           var type = $("#customInvoicetype").val();
             var forpreamount = 0;
             $(".totalamount").each(function () {
                 forpreamount += parseFloat($(this).val());
             });
             var presubtotal = Number(forpreamount).toFixed(2);

             var progressPercentage = $("#progress_percentage").val();
             var progressAmount = presubtotal * progressPercentage / 100;

             var advancePercentage = $("#advance_percentage").val();
             var advancAmount = presubtotal * advancePercentage / 100;

             var workdoneAmount = Number($("#workdone_amount").val()).toFixed(2);
             var certifiedAmount = Number($("#certified_amount").val()).toFixed(2);
             var contraAmount = Number($("#contra_charges").val()).toFixed(2);
             var rententionAmount = workdoneAmount * $("#retention_amount").val() / 100;
             var advancerecoveryAmount = workdoneAmount * $("#advance_recovery").val() / 100;
             var total = parseFloat(certifiedAmount) + parseFloat(contraAmount) + parseFloat(rententionAmount) + parseFloat(advancerecoveryAmount);
             var netAmount = workdoneAmount - total;
             if (type == 0){
                 $("#subtotal").val(netAmount);
             }else if (type == 1){
                 $("#subtotal").val(advancAmount);
             } else {
                 $("#subtotal").val(progressAmount);
             }
         }
         else{
            var subTotal = 0;
            $(".totalamount").each(function () {
                subTotal += parseFloat($(this).val());
            });
            var subTotalFormatted = Number(subTotal).toFixed(2); 
            $("#subtotal").val(subTotalFormatted);
        }
        var discountamount = Number($("#discount").val());
        // console.log('discountamount',discountamount);
            var subtotal = Number($("#subtotal").val()).toFixed(2)
            $("#subtotal").val(subtotal);
            var totamntbeforeVat;
            totamntbeforeVat = subtotal;
            var discrate = $("#discountrate").val();
            if(discrate>0) {
                var discamount = $("#discountrate").val()*subtotal/100;
                var discountformatted = Number(discamount).toFixed(2)
                $("#discountamount").val(discountformatted);
                totamntbeforeVat = subtotal- discountformatted;
            } 
            

            //console.log("Before vat amout",totamntbeforeVat);
            var vat = Number($("#vat").val()).toFixed(2)
            $("#vat").val(vat);
            var total, vatAmount;
            
            if(vat>0) {
                   vatAmount = totamntbeforeVat*(vat)/100;
            } else {
                   vatAmount = 0;
            }
            
            total = parseFloat(totamntbeforeVat) + parseFloat(vatAmount)-discountamount;
            //console.log("Vat value",vat);
            //console.log("Total amount",total);
            var tot = Number(total).toFixed(2);
        $("#totalamount").on("change", function(){
            var data = $(this).val();

            $.ajax({
                url: './amount',
                data: {'data': data},
                type: 'POST',
                success: function (response) {

                    $('#amountwords').val(response);
                    $('#purchase-amount').val(data);
                    console.log(response);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
        $("#totalamount").val(tot).trigger('change');
        

            
        }

        $(document).ready(function(){
            // alert();
        $('.mrvAttachment[src=""]').hide();
        $('.mrvAttachment:not([src=""])').show();
    });
    
   </script> 
   

 
  