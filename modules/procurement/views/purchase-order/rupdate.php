<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\select2\Select2;
use app\models\PurchaseRequestDetails;
//  $purchaseMaster->date_created = date('d-m-Y', strtotime($purchaseMaster['date_created']));
//  $purchaseMaster->expected_date = date('d-m-Y', strtotime($purchase['expected_date']));
 

$vatcategory=\Yii::$app->params['VATRate'];

$currentUser=Yii::$app->user->getId();

$fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());

$payment_list= [1=>'Payment after 30 days',2=>'Payment after 90 days'];

$role = current($fetchRole);
$roleName = $role->name;
$this->title = 'Update Purchase Order';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/purchase/purchase/index']];
$this->params['breadcrumbs'][] = $this->title;
// pr($purchaseMaster);
$purchaseStatus= [1=>'Purchase Created',2=>'Purchase Issued',3=>'Waiting for delivery',4=>'Delivered'];
$OrderType= [1=>'Direct',2=>'MRV'];
$request_id='';
$arrayData=[];
foreach ($purchaseMaster->purchase as $key =>$purchaseData) {
    $request_id=$purchaseData['purchase_id'];

}
$purchaseRequestData = PurchaseRequestDetails::find()
->where(['request_order_id' =>$request_id])
->asArray()->all();
// pr($purchaseRequestData);
foreach($purchaseRequestData as $value1){
    $resData=[];
    $resData['material_request_quantity']=$value1['material_request_quantity'];
    $resData['document']=$value1['document'];
    
    $arrayData[]=$resData;
}
$baseurl = Yii::$app->params['domain_url'];
// pr($requestedName);
?>

<?php $home = Yii::$app->params['domain_url']; ?>
<div class="purchase-form form-vertical">
    <?php $form = ActiveForm::begin(['id'=>'purchase-master-form']); ?>
       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
         <div class="row">
        <!-- <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> Purchase Oder </small></legend></div> -->
         </div>
         <div class="row">
         <div class="col-md-3 hide">
     
       
        <?=
            $form->field($purchaseMaster, 'company_name', [
                'template' => "{label}\n
        <div class=''>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($companylist, [
                'prompt' => 'Choose company',
                'class' => 'project_name select from control',
                'value'=>$purchaseMaster->company_name
            ])->label("Company Name")
            ?>
    </div>
 
    <div class="col-md-3">
     
       
     <?=
         $form->field($purchaseMaster, 'project_name', [
             'template' => "{label}\n
     <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
             'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
         ])->dropDownList($projectList, [
             'prompt' => 'Choose Project',
             'class' => 'project_name select from control',
             'value'=>$purchaseMaster->project_name
         ])->label("Project Name")
         ?>
 </div>
    <div class="col-md-3">
    <?=
            $form->field($purchaseMaster, 'supplier_name', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($supplierData, [
                'prompt' => 'Choose Supplier',
                'class' => 'select from control',
                'value'=>$purchaseMaster->supplier_name
            ])->label("Supplier Name")
            ?>
     
    </div>
  
    <div class="col-md-3">
     
     <?= $form->field($purchaseMaster, 'originator')->textInput(['value'=>$roleName])->label('Originator') ?>
 </div>
 <div class="col-md-3">
<?=
         $form->field($purchaseMaster, 'lpo_status', [
             'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
             'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
         ])->dropDownList($LpoStatus, [
             'prompt' => 'Choose purchase status',
             'class' => 'select from control',
            //  'value'=>$purchaseMaster->purchase_status
         ])->label("Document Status")
         ?>
</div>
    </div>
    <br>
    <div class="row">
    <div class="col-md-3">
    <?= $form->field($purchaseMaster, 'reference')->textInput()->label('Reference') ?>
        
    </div>


    <div class="col-md-3">
    <?=
            $form->field($purchaseMaster, 'date_created', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Date')
            ?>
     
        
    </div>
    <div class="col-md-3">
    <?= $form->field($purchaseMaster, 'amount')->textInput()->label('Amount') ?>
   
    </div>
    <div class="col-md-3">
    <?=
            $form->field($purchaseMaster, 'due_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Delivery Date')
            ?>
   
    </div>
    </div>
    <br>
    <div class="row">
    <div class="col-md-3">
       <?=
         $form->field($purchaseMaster, 'currency', [
             'template' => "{label}\n
     <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
             'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
         ])->dropDownList($currencylist, [
             'prompt' => 'Choose currency',
             'class' => 'select from control',
             'value'=>$purchaseMaster->currency
         ])->label("Currency")
         ?>
       
    </div>
    <div class="col-md-3">
    <?= $form->field($purchaseMaster, 'package')->textInput()->label('Package') ?>
    </div>
    <div class="col-md-3">
    <?=
         $form->field($purchaseMaster, 'payment_terms', [
             'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
             'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
         ])->dropDownList($termsTypeValue, [
             'prompt' => 'Choose payment terms',
             'class' => 'select from control',
             'value'=>$purchaseMaster->payment_terms
         ])->label("Payment Terms")
         ?>
    </div>
    <div class="col-md-3">
    <?= $form->field($purchaseMaster, 'recommendation')->textInput()->label('Recommendation') ?>
    </div>

</div>
<br>
<div class="row">
<div class="col-md-3">
<?= $form->field($purchaseMaster, 'discount')->textInput()->label('Discount') ?>
</div>
<div class="col-md-3">
<?= $form->field($purchaseMaster, 'supplier_ref')->textInput()->label('Supplier ref') ?>
</div>
<div class="col-md-3">
<?= $form->field($purchaseMaster, 'provision_amount')->textInput()->label('Provision Amount') ?>
</div>
<div class="col-md-3">
<?= $form->field($purchaseMaster, 'po_number')->textInput()->label('PO Number') ?>
</div>
</div>
<br>
<div class="row">

<div class="col-md-3">
<?=
         $form->field($purchaseMaster, 'order_type', [
             'template' => "{label}\n
             <div class=''>
             {input}\n
             {hint}\n
             {error}
     </div>",
             'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
         ])->dropDownList($OrderType, [
            //  'prompt' => 'Choose order type',
             'class' => 'order_type select from control',
             'value'=>$purchaseMaster->order_type,
            //  'id'=>'OrderType',
            //  'multiple'=>'multiple',
         ])->label("Order Type")
         ?>
</div>
<div class="col-md-3" id ='typeHide'>
<label>Assign MRV</label>
        <?php
            echo Select2::widget([
                'name' => 'requestList',
                'id'=>'mrv',
                'data' => $codeList,
                'value'=>$requestedName,
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => 'Request ...', 'multiple' => true, 'autocomplete' => 'off'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
</div>
</div>
<br>


<div>&nbsp;</div>  
<div class="col-md-12"><legend class="text-info"><small>Material</small></legend></div>
<div class ="row" id ="headingdiv">
<div class="col-md-1" style="font-weight:bold">Material</div>
    <div class="col-md-1" style="font-weight:bold">Description</div>
    <div class="col-md-1" style="font-weight:bold">MRV Status</div>
    <div class="col-md-1" style="font-weight:bold"> Unit</div>
    <div class="col-md-2" style="font-weight:bold">Expected Date</div>
    <div class="col-md-1" style="font-weight:bold">Document</div>
    <!-- <div class="col-md-1" style="font-weight:bold">Purchased Quantity</div> -->
    <div class="col-md-1" style="font-weight:bold"> Request Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Order Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Remaining Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Price</div>
    <div class="col-md-1" style="font-weight:bold">Total</div>
</div>
<div id="product">

<?php foreach ($purchaseMaster->purchase as $key =>$purchase) { ?>

    <?php foreach ($arrayData as $requestData) { ?>


    <div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-1"><select id="purchase-master-material-0<?php echo $purchase->id; ?>" class="form-control required material" name="PurchaseMaster[material_id][]" disabled="disabled" >
    
    <option value="">Please select material</option>
        <?php foreach ($resourceNameListData as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$purchase->material_id){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
</select>
    
                                     
    </div>
    <div class="col-md-1"><textarea data-row ="<?php echo $purchase->id; ?>"  id="purchase-master-description" class="form-control  descrip" name="PurchaseMaster[description][]"><?php echo $purchase->description;?></textarea></div>

    <div class="col-md-1"><select data-row ="<?php echo $purchase->id; ?>" id="purchase-master-mrvstatus" class="form-control  mrvstatus" name="PurchaseMaster[mrv_status][]">
    <!-- <option value="">Please select status</option> -->
    <option value=""> select </option>
        <?php foreach ($mrvStatus as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$purchase->mrv_status){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
                                     
    </select></div>
    <div class="col-md-1"><select data-row ="<?php echo $purchase->id; ?>" id="purchase-master-unit" class="form-control  unit" name="PurchaseMaster[unit][]">
    <!-- <option value="">Please select status</option> -->
    <option value=""> select </option>
        <?php foreach ($unitType as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$purchase->unit){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
                                     
    </select></div>
    <div class="col-md-2"><input type="date"  value ="<?php echo $purchase->expected_date;?>" data-row ="<?php echo $purchase->id; ?>" id="purchase-master-expected-date-<?php echo $purchase->id; ?>" class="form-control  expected-date" name="PurchaseMaster[expected_date][]"></div>
    <?php if($requestData['document']!=''){ ?>
    <div class="col-md-1" ><a href="<?php echo $baseurl.'web/mrv/attachment/'.$requestData['document'];?>" target="_blank" data-pjax="0"><img class="mrvAttachment" src="<?php echo $baseurl.'web/mrv/attachment/'.$requestData['document'];?>"  id ="document-<?php echo $purchase->id;?>" width="30px" height="30px"></a>
                 </div>
                 <?php } ?>
                 <?php if($requestData['document'] ==''){ ?>
    <div class="col-md-1" >
                 </div>
                 <?php } ?>
    <div class="col-md-1"><input type="text" value=" <?php echo $requestData['material_request_quantity']; ?>"data-row ="<?php echo $purchase->id; ?>" id="request-quantity-<?php echo $purchase->id; ?>" class="form-control request-quantity" name="PurchaseMaster[request_quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" value=" <?php echo $purchase->order_quantity; ?>"data-row ="<?php echo $purchase->id; ?>" id="order-quantity-<?php echo $purchase->id; ?>" class="form-control order-quantity" name="PurchaseMaster[order_quantity][]" onchange="calc(this.id);"></div>
    
    
    <div class="col-md-1"><input type="text"  readonly ="true" value="<?php echo $purchase->remaining_quantity;  ?>" data-row ="<?php echo $purchase->id; ?>" id="purchased-quantity-<?php echo $purchase->id; ?>" class="form-control remaining-quantity" name="PurchaseMaster[remaining_quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" value="<?php echo $purchase->price; ?>" data-row ="<?php echo $purchase->id; ?>" id="price-amount-<?php echo $purchase->id; ?>" class="form-control" name="PurchaseMaster[price][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" data-row ="<?php echo $purchase->id; ?>" class="form-control totalamount" readOnly = "true" value="<?php echo $purchase->total_amount; ?>" id="total-amount-<?php echo $purchase->id; ?>" name="PurchaseMaster[total_amount][]" onchange="calc(this.id);"></div>
  <input type="hidden" value="<?php echo $purchase->id;?>" name="PurchaseMaster[id][]"> 
  
</div>
</div>
<div class="row">&nbsp;</div> 
<?php }} ?>
</div>

<div>&nbsp;</div>

        <div class="purchase-box">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class='col-md-12  padding-0'>
                <div class='col-md-3 col-xs-12 padding-0'>
               
            </div>
            <div class='col-md-7 col-xs-12'>
            <?=
            $form->field($purchaseMaster, 'sub_total', [
                'template' => "{label}\n
                <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
             'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
            ])->textInput(
                    [   
                        'id' => 'subtotal',
                        'readOnly' => true, 
                        'placeholder' => '0.00',
                        'class' => 'form-control subtotaltotal',
                        'onchange' => 'calc(this.id);',
                    ])->label('Sub Total')
            ?>
                </div>
            </div>
            <div class="col-md-12"><legend class="text-info"><small></small></legend></div> 
            <?=
                    $form->field($purchaseMaster, 'vat', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        // 'class'=>'select from control',
                        'value'=>$vatcategory,
                        'readOnly' => true,
                        'placeholder' => '',
                        'id' => 'vat',
                        'onchange' => 'calc(this.id);',
                    ])->label('VAT(%)')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div> 
            <?=
                    $form->field($purchaseMaster, 'discount', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                       
                        'placeholder' => '',
                        'id' => 'discount',
                        'onchange' => 'calc(this.id);',
                    ])->label('Discount')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

            <?=
            $form->field($purchaseMaster, 'total_amount', [
                'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
            ])->textInput([
                'readOnly' => true, 
                'placeholder' => '0.00',
                'id' => 'totalamount',
                 //'class' => 'form-control total'
                
                ])->label('Grand Total')
            ?>
        </div>
        <div>&nbsp;</div>

    </div>
</div>
<div>&nbsp;</div>
<div class = "row">
<div class= "col-md-8">
        <?=
        $form->field($purchaseMaster, 'notes', [
            'template' => "{label}\n
            <div class='col-md-9 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
        ])->textarea([
           'readOnly' => true,
//            'placeholder' => '0.00',
            'id' => 'amountwords',
            'class' => 'form-control total text-bold'
        ])->label('Amount In Words')
        ?>
        </div>
</div>
<br>
<div class="col-md-12"><legend class="text-info"><small></small></legend></div>
<div class="col-md-12"> 
    <div class="form-group">
        <div class="col-md-12 col-xs-12">
<?= Html::submitButton($purchaseMaster->isNewRecord ? 'Submit' : 'Update', ['class' => $purchaseMaster->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
<?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
</div>
</div>
</div>
<?php
ActiveForm::end();
?>
 </div>
<?= $this->registerJs("
    
    $('#purchase-date_created').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });
      $('#purchase-due_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY',
      });

      $('body').on('click','.remove',function (){ 
        // console.log('remove',('.remove'));
    $(this).parents('.control-group').parent().remove();
    calc('remove');
    
  });

  $(function () {
    $('body').on('change','#purchase-project_name',function (){ 
 
        var project_id = $(this).val();
        // alert(project_id);
        $.ajax({
            url: './get-reference-name',
            data: {'project_id': project_id},
            type: 'post',
            success: function (data) {
                console.log('data',data);
                $('#mrv').empty().append(data);
             
            }
        });
    });
});

$(function(){
    // $('#typeHide').hide();
    $('body').on('change','#purchase-order_type',function (){ 
    
        var order = $(this).val();
        console.log('order',order);
        if(order==1){
       $('#typeHide').hide();
         }else{
       $('#typeHide').show();
           }
             });
             });


$(function () {
$('body').on('click','#typeHide',function (){ 
  
    $('#headingdiv').hide();
    $('#addButton').hide();
   
});
});
$(function () {
    $('body').on('change','#purchase-order_type',function (){ 
 

        var orderType = $(this).val();
      
        if(orderType==='2'){
         $('#headingdiv').show();
            $('#addButton').hide();
           }
        else{
           $('#headingdiv').show();
            $('#addButton').show();
            $('#product').show();
        }
       
    });
});

      $(function () {
        $('body').on('change','#purchase-project_name',function (){ 
     
            var project_id = $(this).val();
            
            $.ajax({
                url: './get-material-list',
                data: {'project_id': project_id},
                type: 'post',
                success: function (data) {
                    console.log('data',data);
                    $('.material').empty().append(data);
                 
                }
            });
        });
    });
    
", View::POS_READY); ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>

$( document ).ready(function() {

});
$(function () {
    $('#mrv').on('change',function (){
      
        var mrv = $(this).val();
        $.ajax({
            url: './get-total-material-data',
            data: {'mrv':mrv},
            type: 'post',
            success: function (data) {
                console.log('data',data);
                $('#product').empty().append(data);

            }
        });

    });
});
    function calc(id) {
       // alert('here');
    
       var idPosition = id.split('-');
          
          var price = Number($("#price-"+idPosition[1]).val());
          var quantity = Number($("#quantity-"+idPosition[1]).val());
         
          console.log('quantity',quantity);
          console.log('price',price);
          $("#price-"+idPosition[1]).val(price);

          var total =quantity *price ;
console.log('total',total);
          var tot = Number(total).toFixed(2); 
          console.log('tot',tot);
          $("#totalamount-"+idPosition[1]).val(tot)
      
    if($('.performa-Invoice').is(':checked')) {
         var type = $("#customInvoicetype").val();
           var forpreamount = 0;
           $(".totalamount").each(function () {
               forpreamount += parseFloat($(this).val());
           });
           var presubtotal = Number(forpreamount).toFixed(2);

           var progressPercentage = $("#progress_percentage").val();
           var progressAmount = presubtotal * progressPercentage / 100;

           var advancePercentage = $("#advance_percentage").val();
           var advancAmount = presubtotal * advancePercentage / 100;

           var workdoneAmount = Number($("#workdone_amount").val()).toFixed(2);
           var certifiedAmount = Number($("#certified_amount").val()).toFixed(2);
           var contraAmount = Number($("#contra_charges").val()).toFixed(2);
           var rententionAmount = workdoneAmount * $("#retention_amount").val() / 100;
           var advancerecoveryAmount = workdoneAmount * $("#advance_recovery").val() / 100;
           var total = parseFloat(certifiedAmount) + parseFloat(contraAmount) + parseFloat(rententionAmount) + parseFloat(advancerecoveryAmount);
           var netAmount = workdoneAmount - total;
           if (type == 0){
               $("#subtotal").val(netAmount);
           }else if (type == 1){
               $("#subtotal").val(advancAmount);
           } else {
               $("#subtotal").val(progressAmount);
           }
       }
       else{
          var subTotal = 0;
          $(".totalamount").each(function () {
              subTotal += parseFloat($(this).val());
          });
          var subTotalFormatted = Number(subTotal).toFixed(2); 
          $("#subtotal").val(subTotalFormatted);
      }
          
          var subtotal = Number($("#subtotal").val()).toFixed(2)
          $("#subtotal").val(subtotal);
          var discountamount = Number($("#discount").val());
          
          var totamntbeforeVat;
          totamntbeforeVat = subtotal;
          var discrate = $("#discountrate").val();
          if(discrate>0) {
              var discamount = $("#discountrate").val()*subtotal/100;
              var discountformatted = Number(discamount).toFixed(2)
              $("#discountamount").val(discountformatted);
              totamntbeforeVat = subtotal- discountformatted;
          } 
          

          //console.log("Before vat amout",totamntbeforeVat);
          var vat = Number($("#vat").val()).toFixed(2)
          $("#vat").val(vat);
          var total, vatAmount;
          
          if(vat>0) {
                 vatAmount = totamntbeforeVat*(vat)/100;
          } else {
                 vatAmount = 0;
          }
          
          total = parseFloat(totamntbeforeVat) + parseFloat(vatAmount) - discountamount;
        //   console.log('discountamount',discountamount);
          
        //   console.log('vatAmount',vatAmount);
        //   console.log('total',total);

          var tot = Number(total).toFixed(2);
      $("#totalamount").on("change", function(){
          var data = $(this).val();

          $.ajax({
              url: './amount',
              data: {'data': data},
              type: 'POST',
              success: function (response) {

                  $('#amountwords').val(response);
                  $('#purchase-amount').val(data);
                  console.log(response);
              },
              error: function (error) {
                  console.log(error);
              }
          });
      });
      $("#totalamount").val(tot).trigger('change');
            
        }

    
    
   </script> 
   

 
  