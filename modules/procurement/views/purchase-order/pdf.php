<?php
use yii\helpers\Html;
$supliername='M/S. JUNAID SAN & ELE.MAT.TRDG.L.L.C';
$suplier_postbox='Post Box No. 34862';
$suplier_tel='Tel:  04‐3201694';
$suplier_country='DUBAI ‐ U.A.E';
$suplier_attn='Attb : Mr. ASIF'
?>
<table class='table' style='margin-bottom: 0px' border='1'>
    <tr>
        <td  colspan='2' align='center'   style="font-size: 15px;" ><?php echo $companyName;?></td>
    </tr>
    <tr>
        <td width='50%'><img src='<?php echo $logo;?>' width='110px'/></td>
        <td width='25%'><table class='table'>

                <tr>
                    <td  align='right'  style=\"font-size: 15px;\" >

                    </td><td>Post Box No : <?php echo $postboxName;?>
                        <br>
                        Tel No : <?php echo $telphone;?>
                        <br>
                        Fax No : <?php echo $faxno;?>
                    </td>


                </tr>

                <tr>

                </tr>
                <tr>

                </tr></table></td>

    </tr>
</table>
<br/>

<table class='table' border='1' style='padding-top: 2px'>

    <tr>

        <td class=\"localheading\" style=\"font-size: 13px;\" >
            LOCAL PURCHASE ORDER
        </td>
        <td class=\"localheading\" style=\"font-size: 13px;\" >
        <?php echo $TRN_number;?>
        </td>
    </tr>
</table>
<br>

<table class='table' border='1' style='padding-top: 2px'>
    <tr><td>To</td><td style='border:none;'></td><td>LPO Details</td></tr>
    <tr>



        <td class=\"localheading\" style=\"font-size: 13px;\" >
        <?php echo $supliername?>
        <br/>
        <?php echo $suplier_postbox?>
        <br/>
        <?php echo $suplier_tel?>
        <br/>
        <?php echo $suplier_country?>
        <br/>
        <?php echo $suplier_attn?>
        <br/>

        </td>
        <td style='border:none;'></td>
        <td>
            <?php echo $supliername?>
            <br/>
            <?php echo $suplier_postbox?>
            <br/>
            <?php echo $suplier_tel?>
            <br/>
            <?php echo $suplier_country?>
            <br/>
            <?php echo $suplier_attn?>
            <br/>
        </td>
    </tr>
</table>
<br>


<div>&nbsp;</div>
<table class="table table-bordered " cellspacing="3" cellpadding="15" >
<tbody>
<tr class="rowheader">
            <td width="10%" class="text-center text-bold" style="padding: 2px"><h4>SNO</h4></td>
            <td width="40%" class="text-center text-bold" ><h4>Material</h4></td>
            <td width="30%" class="text-center text-bold" ><h4>Description</h4></td>
            <td width="15%" class="text-center text-bold" style="padding: 2px"><h4>Quantity </h4></td>
            <td width="15%" class="text-center text-bold" style="padding: 2px"><h4>Price</h4></td>
            <td width="20%" class="text-center text-bold" style="padding: 2px"><h4>Total</h4></td>
        </tr>
        </tr>
                  <?php foreach($productInfo as $key => $value){
                    // pr($product);
                     ?>
           <tr>
                <td class="text-center"style="font-size:17px" > <?php echo $key+1;?> </td>
                <td class="text-left"style="font-size:17px" > <?php echo ($value['material_id']);?></td>
                <td class="text-left"style="font-size:17px"> <?php echo ($value['description']);?></td>
                <td class="text-right"style="font-size:17px"> <?php echo ($value['order_quantity']);?></td>
               
                <td class="text-right"style="font-size:17px" > <?php echo number_format($value['price'], 2, '.', ',');?></td>
                <td class="text-right"style="font-size:17px" > <?php echo number_format($value['total_amount'], 2, '.', ',');?></td>
                 </tr> 
                 
                 <?php } ?>
                 <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2">Sub Total</td>
                    <td class="text-right"><?php echo $subtotalAmount; ?></td>
                </tr>
                <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2">VAT</td>
                    <td class="text-right"><?php echo $VatAmount; ?></td>
                </tr>
                <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2">Discount</td>
                    <td class="text-right"><?php echo $Discount; ?></td>
                </tr>
  
                <tr class="rowfooter">
                     <td class="text-center text-bold" colspan="3" style="padding: 15px"><?php echo ucfirst($word);?></td>  
                      <td class="text-center text-bold" colspan="2" style="padding: 15px"> Grand Total </td>  
                      <td class="text-right text-bold" style="padding: 15px"><?php echo $total_amount; ?></td>
                </tr>
                
</tbody>
</table>
       

    <div class="ignore_page_brack">
        <table class="table">
            <tr>
                <td>Date </td>
                <td><?php
                    echo $purchase['date_created'];
                    ?></td>
                <td>Signature </td>
                <td>&nbsp;</td>
                <td>
        </table>
        <p>Please acknowledge receipt of documents, complete as listed, by signing the copy annexed.</p>

        <table class="table ">
            <tr>
                <td >Acknowledge by : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ </td>
            </tr>
            <tr>
                <td style="height: 100px">&nbsp;</td>
            </tr>

        </table>
        <table class="table ">
            <tr>
                <td>On behalf of  : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _<br>
                    (Company Stamp)</td>
                <td>Date: _ _ _ _ _ _ _ _ _ _ _</td>
            </tr>
        </table>
    </div>
  
    </div>



    
    
    
           
