<?php

use yii\helpers\Html;

$this->title = 'Create LPOM';
$this->params['breadcrumbs'][] = ['label' => 'LPOM', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="purchase">
    <?= $this->render('_form', [
       'purchaseMaster' => $purchaseMaster, 
    'purchase' => $purchase,
       'projectList'=>$projectList,
                'supplierData'=>$supplierData,

                'currencylist'=>$currencylist,
                'termsTypeValue'=>$termsTypeValue,
                'LpoStatus'=>$LpoStatus,
                'mrvStatus'=>$mrvStatus,
                'unitType'=>$unitType,
                'materialList'=>$materialList,
                'resourceNameListData' => $resourceNameListData,
                
    ]) ?>

</div>
