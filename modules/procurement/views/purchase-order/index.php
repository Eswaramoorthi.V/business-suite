

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use app\models\Project;
use app\models\Customer;
use yii\helpers\ArrayHelper;
use app\models\Company;
$this->title = Yii::t('user', 'LPOM');
$this->params['breadcrumbs'][] = 'LPOM';
//$this->params['title_icon'] = 'fa-users';
?>

<?php Pjax::begin() ?>
<div class="purchase">
<div class="col-md-6 text-left add-label" style="font-size: 30px">
      
            <i class="fa fa-etsy" aria-hidden="true"></i> LPOM (Local Purchase Order)
    </div>

<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'project_name',
                'header' => '<div style="width:100px;">Project  Name</div>',
                'value' => function($model, $key, $index) {
                    $projectListData =Project::find()->select(['project_name','id'])->asArray()->all();

                    $assign_project = [];
                    
                    foreach ($projectListData as $value) {
                        $eType =[];
                        $eType['id'] = $value['id'];
                        $eType['project_name'] = $value['project_name'];
                        $assign_project[]=$eType;
                    }
                    $projectList = ArrayHelper::map($assign_project, 'id', 'project_name');
                    return !empty($projectList[$model['project_name']])?$projectList[$model['project_name']]:'-';
                       
                    },
                
            ],
            [
                'attribute' => 'po_number',
                'header' => '<div style="width:120px;">PO Number</div>',
                 'value' => function($model, $key, $index) {
                        return Html::a($model->po_number, ['/procurement/purchase-order/delivery', 'id' => $model->id], [ 'data-pjax'=>"0"]);
                    },
                'vAlign' => 'middle',
                'format' => 'raw',
                'noWrap' => true
            ],
            [
                'attribute' => 'company_name',
                'header' => '<div style="width:100px;">Company  Name</div>',
                'value' => function($model, $key, $index) {
                    $companyDataList =Company::find()->asArray()->all();
                    $companyData = [];
                    
                    foreach ($companyDataList as $value) {
                        $eType =[];
                        $eType['id'] = $value['id'];
                        $eType['company_name'] = $value['company_name'];
                        // $eType['customer_lname'] = $value['customer_lname'];
                        $companyData[]=$eType;
                    }
                    
                    $assign_company = ArrayHelper::map($companyData, 'id', 'company_name');
                    return !empty($assign_company[$model['company_name']])?$assign_company[$model['company_name']]:'-';
                       
                    },
                
            ],
            [
                'attribute' => 'supplier_name',
                'header' => '<div style="width:100px;">Supplier  Name</div>',
                'value' => function($model, $key, $index) {
                    $assign_clientData =Customer::find()->asArray()->all();
                    $assign_ClientData = [];
                    
                    foreach ($assign_clientData as $value) {
                        $eType =[];
                        $eType['id'] = $value['id'];
                        $eType['company_name'] = $value['company_name'];
                        // $eType['customer_lname'] = $value['customer_lname'];
                        $assign_ClientData[]=$eType;
                    }
                    
                    $assign_client = ArrayHelper::map($assign_ClientData, 'id', 'company_name');
                    return !empty($assign_client[$model['supplier_name']])?$assign_client[$model['supplier_name']]:'-';
                       
                    },
                
            ],
            // [
            //     'attribute' => 'lpo_status',
            //     'header' => '<div style="width:100px;">Purchase Status</div>',
            //     'value' => function($model, $key, $index) {
                    
            //         $purchaseStatus= [1=>'Purchase Created',2=>'Purchase Issued',3=>'Waiting for delivery',4=>'Delivered'];
            //         return !empty($purchaseStatus[$model['purchase_status']])?$purchaseStatus[$model['purchase_status']]:'-';
                       
            //         },
                
            // ],

            [
                'attribute' => 'due_date',
                'header' => '<div style="width:100px;">Delivery Expect Date</div>',
                 'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->due_date, 'php:d-m-Y');
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],

           
            [
                
                'class' => '\kartik\grid\ActionColumn',
                'mergeHeader' => false,
                    'contentOptions' => ['style' => 'width:50px;text-align:center','class'=>'skip-export'],
                    'headerOptions' =>['class'=>'skip-export'],
                    'header'=>"Actions",
                'template'=>$actionList,
                'dropdown' => true,
                'dropdownOptions'=>['class'=>'pull-right' ],
                'options' => ['style' => 'max-width:20px;'],
                'buttons' => [
                    'export-pdf' => function($url, $model) {
                        return Html::a('PDF' ,$url."&order_type=$model->order_type",['class' => 'action-btn dropdown-item ', 'title' => 'PDF',]
                        );
                    },
                    'update' => function($url, $model) {
                        return Html::a('Update',  $url."&order_type=$model->order_type",['class' => 'action-btn dropdown-item ', 'title' => 'Update']
                        );
                    },
                    'deletes' => function($url) {
                       return Html::a('Delete',   $url,['class' => 'action-btn dropdown-item','data-method'=>'post', 'data-pjax'=>0,'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?']]
                       );
                   },
                   
                    ]
                
                ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
            
            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'pjax' => false, // pjax is set to always true for this demo
       
            'toolbar' => [
                [
                    'content'=>

                        Html::a(Yii::t('app', ' {modelClass}', [
                            'modelClass' => '<i class="glyphicon glyphicon-bell icons"></i>Add New LPOM',
                        ]), ['create'], ['class' => 'btn btn-info btn-rounded', ]),

                ],

            ],

            
            //'showPageSummary' => true,
            'panel' => [

                // 'type' => GridView::TYPE_DEFAULT,
                'heading' => false,
            ],
            'persistResize' => true,

    ]);
}
?>                                   
</div>
<?php Pjax::end() ?>

<style>
a.action-btn.dropdown-item {
  display: block;
  width: 100%;
  padding: .25rem 1.5rem;
  clear: both;
  font-weight: normal;
  text-align: inherit;
  white-space: nowrap;
  background-color: transparent;
  border: 0;
  color: navy;
  font-size: small;
}
</style>