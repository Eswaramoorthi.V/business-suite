<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

 

$this->title = 'Delivery Order';
$this->params['breadcrumbs'][] = ['label' => 'Purchase', 'url' => ['/purchase/purchase/index']];
$this->params['breadcrumbs'][] = $this->title;
// pr($ProjectName);
// $project_id=$_GET;
// pr($project_id);

?>

<?php $home = Yii::$app->params['domain_url']; ?>


<div class="purchase-form form-vertical">
<div class ="row">

</div>
<div class="table-responsive">

<table class="table">
                <colgroup>
                    <col style="width:15%">
                    <col style="width:15%">
                    <col style="width:15%">
                    <col style="width:15%">
                </colgroup>
                <tbody><tr>

                <th> Project Name</th>
                    <td>
                        <?php echo $ProjectName?>
                    </td>
                    <th> Supplier Name</th>
                    <td>
                        <?php echo $supplierName?>
                    </td>
                    <th> Originator</th>
                    <td>
                    <?php echo $originator?>
                    </td>
                     </tr>
                    
                <tr>
                
                    <th> Reference</th>
                    <td>
                    <?php echo $reference?>
                    </td>
                    <th> Date</th>
                    <td>
                        <?php echo $date?>
                    </td>
                   
                    <th> Amount</th>
                    <td>
                    <?php echo $amount?>
                    </td>

                </tr>
                <tr>
                
                <th> Delivery Date</th>
                    <td>
                        <?php echo $delivery_date?>
                    </td>
                    <th> Currency</th>
                    <td>
                    
                    <?php echo $currency?>
                    </td>
                    <th> Package</th>
                    <td>
                    <?php echo $package?>
                    </td>
</tr>
                    <tr>
                    
                    <th> Payment Terms</th>
                    <td>
                        <?php echo $paymentTerms?>
                    </td>
                    <th> Recommentation</th>
                    <td>
                    <?php echo $recommendation?>
                    </td>
                    <th>Discount</th>
                    <td>
                    <?php echo $discount?>
                    </td>
</tr>
                
                <tr>
                   
                    <th> Supplier Ref</th>
                    <td>
                    <?php echo $supplier_ref?>
                    </td>
                    <th> Provision Amount</th>
                    <td>
                    <?php echo $provision_amount?>
                    </td>
                    <th> PO Number</th>
                    <td>
                    <?php echo $po_number?>
                    </td>

                </tr>
                <tr>
               
                    <th> LPO Status</th>
                    <td>
                        <?php echo $lpostatus?>
                    </td>
                    <th> Order Type</th>
                    <td>
                    <?php echo $order_type?>
                    </td>
                    </tr>
                </tbody></table>

 

</div>
<div>&nbsp</div>
<div>&nbsp</div>
<div>&nbsp</div>
<?php $form = ActiveForm::begin(['id'=>'purchase-delivery-form','method'=>'post','action'=>'delivery?id='.$_GET['id']]); ?>


       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
      
<div class ="row">
<div class="col-md-2" style="font-weight:bold">Material</div>
    <div class="col-md-2" style="font-weight:bold">Description</div>
    <div class="col-md-1" style="font-weight:bold">MRV Status</div>
    <div class="col-md-2" style="font-weight:bold">Delivery Date</div>
    <div class="col-md-1" style="font-weight:bold">Ordered Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Delivered Quantity</div>
    <div class="col-md-1" style="font-weight:bold"> Received Quantity</div>
  
</div>
<div id="product">
<?php foreach ($arrayData as $request) { ?>
<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-2"><select  id="purchase-delivery-material-0<?php echo $request['id']; ?>" class="form-control required material" name="PurchaseDelivery[material_id][]" disabled="disabled" >
    
    <option value="">Please select material</option>
        <?php foreach ($resourceNameListData as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$request['material_id']){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
</select>
    
                                     
    </div>
    <div class="col-md-2"><textarea  readonly="true"  data-row ="<?php echo $request['id']; ?>"   id="purchase-delivery-description" class="form-control  description" name="PurchaseDelivery[description][]"><?php echo $request['description'];?></textarea></div>
    <div class="col-md-1"><select data-row ="<?php echo $request['id']; ?>" id="purchase-delivery-materialstatus" class="form-control  materialstatus" name="PurchaseDelivery[material_status][]">
    
    <option value="">Please select status</option>
        <?php foreach ($mrvSatusData as $result): ?>
        <option value="<?php echo $result['id'];?>"<?php 
           if($result['id']==$request['mrv_status']){
               echo 'selected ';
           } ?>><?php echo $result['name']; ?></option>
                                        }
        <?php endforeach; ?>
    </select></div>
    <div class="col-md-2"><input  type="date"  value ="<?php echo $request['delivery_date'];?>" data-row ="<?php echo $request['id']; ?>" id="purchase-delivery-delivery-date-<?php echo $request['id']; ?>" class="form-control  delivery-date" name="PurchaseDelivery[delivery_date][]"></div>
    <div class="col-md-1"><input   readonly="true" type="text" value="<?php echo $request['order_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="order-quantity-<?php echo $request['id']; ?>" class="form-control totalqnty" name="PurchaseDelivery[order_quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input   type="text" value="<?php echo $request['delivered_quantity']; ?>" data-row ="<?php echo $request['id']; ?>" id="purchased-quantity-<?php echo $request['id']; ?>" class="form-control purchanseqnty" name="PurchaseDelivery[delivered_quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input    type="text" value="0"data-row ="<?php echo $request['id']; ?>" id="received-quantity-<?php echo $request['id']; ?>" class="form-control qntyreceived" name="PurchaseDelivery[received_quantity][]" onchange="calc(this.id);"><h6 class="error-msg hide text-danger"> </h6></div>
     <div class="col-md-1"><input    type="hidden" value=<?php echo $request['id']; ?> data-row ="<?php echo $request['id']; ?>" id="podetails-<?php echo $request['id']; ?>" class="form-control qnty" name="PurchaseDelivery[podetails][]"></div>
        <div class="col-md-1"><input    type="hidden" value=<?php echo $request['po_id']; ?>  id="podetails-<?php echo $request['id']; ?>" class="form-control qnty" name="PurchaseDelivery[poorder][]"></div>
        <div class="col-md-1"><input    type="hidden" value=<?php echo $request['project_id']; ?>  id="projectdetails-<?php echo $request['id']; ?>" class="form-control qnty" name="PurchaseDelivery[project_id][]"></div>


    
</div>
</div>

<div class="row">&nbsp;</div> 
<?php } ?>
</div>

<div>&nbsp;</div>

<div class = "row">
<div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                <?= Html::submitButton($purchaseDelivery->isNewRecord ? 'Submit' : 'Update', ['class' => $purchaseDelivery->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                    <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
 </div>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <script >
  function calc(id) {
    var idPosition = id.split('-');
    // console.log('idPosition',idPosition[1]);
    // var purchased = ($("#purchased-quantity-"+idPosition[2]).val())
        //    var total = $("#order-quantity-"+idPosition[1]).val();
        //    console.log('total',total);
    //  $('#quantity-'+idPosition[2]).val(total);
   
    $('body').on('blur','.qntyreceived',function (){ 
            var counterVal=$(this).attr('data-row');
             var receivedQuantity=$(this).val();
             
console.log('counterVal',counterVal);
       var orderQuantity = parseInt($("#order-quantity-"+counterVal).val());
//   var requestQuntity = parseInt($("#request-quantity-"+counterVal).val());
  if (orderQuantity >= receivedQuantity) {
    return false;
  } else if(orderQuantity <= receivedQuantity) {
    $('.error-msg').removeClass("hide");
   
    $('.error-msg').html("greater than total quantity");
    setTimeout(function() {
             $('.error-msg').hide();
        }, 3000); 
  }
  else{
        $('.error-msg').addClass("hide");
        
    } 
  });

  }
  jQuery(function ($) {
      $('form').bind('submit', function () {
          $(this).find(':input').prop('disabled', false);
      });
  });

</script>
