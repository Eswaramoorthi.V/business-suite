<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use yii\web\View;
use app\models\Purchase;
use app\components\Procurement;
?>
<div id="product">
    <div class="row copyinvitems details after-add-more" id="copy">
        <div class="control-group drag">

            <div class="col-md-4 material-drop-<?php echo $count;?>">


                <?php






                echo \kartik\tree\TreeViewInput::widget([
                    'name' => 'PurchaseMaster[material_id][]',
                    'value' => true,
                    'query' => \app\models\Tree::find()->addOrderBy('root, lft'),
                    'headingOptions' => ['label' => 'Store'],
                    'rootOptions' => ['label' => '<i class="fas fa-tree text-success"></i>'],
                    'fontAwesome' => true,
                    'asDropdown' => true,
                    'multiple' => false,
                    'cacheSettings' => ['enableCache' => true],
                    'options' => ['disabled' => false,'id'=>'tree123']
                ]);


                ?></div>
            <div class="col-md-2"><textarea id="purchase-master-description-<?php echo $count;?>" class="form-control  descrip-<?php echo $count;?>" name="PurchaseMaster[description][]"></textarea></div>
            <div class="col-md-1"><select data-row ="<?php echo $count;?>" id="purchase-master-mrvstatus-<?php echo $count;?>" class="form-control  mrvstatus-<?php echo $count;?>" name="PurchaseMaster[mrv_status][]">
                    <!-- <option value="">Please select MRV StauSs</option> -->
                    <?php

                    $mrv = [];

                    foreach ($mrvStatus as $mrvID=>$mrvValue) {

                        ?>
                        <option value="<?php echo $mrvID; ?>"><?php echo $mrvValue; ?></option>
                    <?php } ?>
                </select></div>
            <div class="col-md-2"><input type="date" data-row ="<?php echo $count;?>" id="purchase-master-expected-date-<?php echo $count;?>" class="form-control  expected-date-<?php echo $count;?>" name="PurchaseMaster[expected_date][]"></div>

            <div class="col-md-1"><input type="text" value="<?php echo $count;?>" data-row="<?php echo $count;?>" id="order-quantity-<?php echo $count;?>" class="form-control orderqnty" name="PurchaseMaster[order_quantity][]" onchange="calc(this.id);"></div>
            <!-- <div class="col-md-1"><input type="text" id="purchase-master-unit" class="form-control required unit" name="PurchaseMaster[unit][]"></div> -->
            <div class="col-md-1"><input type="text" value="<?php echo $count;?>" id="price-amount-<?php echo $count;?>" class="form-control" name="PurchaseMaster[price][]" onchange="calc(this.id);"></div>
            <div class="col-md-1"><input type="text" class="form-control totalamount-<?php echo $count;?>" readOnly = "true" value="0.00" id="total-amount-<?php echo $count;?>" name="PurchaseMaster[total_amount][]" onchange="calc(this.id);"></div>

        </div>
    </div>
</div>