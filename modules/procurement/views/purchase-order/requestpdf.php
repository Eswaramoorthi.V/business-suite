<?php

use yii\helpers\Html;

?> 
<div class ="tableborderheader" >
  
<div>&nbsp;</div>
<div class="">
<table class='table'>
        
        <tr>
                      <td class="companyheader" style="font-size: 13px;" >
                      TO:
                      <div>&nbsp;</div>
                      <?php echo $salutionData?> :

                    
                      <?php echo $supplierName?>
                      <br>
                      <div>&nbsp;</div>
                     <?php echo $supplierAddress?> 
                     <br>
                     <div>&nbsp;</div>
                     Phone.No:  <?php echo $supplierphone?>
                      <br>
                      <!-- <div>&nbsp;</div> -->
                     
                      <br></td>

                      <td class="companyheader" style="font-size: 13px;" >
                      <!-- LPO Details: -->
                      <div>&nbsp;</div>
                      Project Name :
                      <?php echo $projectName?> 

                    <br>
                    <div>&nbsp;</div>

                      LPO.NO :
                      <?php echo $po_number?> 

                    <br>
                    <div>&nbsp;</div>
                    Date:
              <?php echo $date?>
                      <br>
                      <div>&nbsp;</div>
                      Your.Ref:
                      <?php echo $reference?>
                      </td>
                    </tr>
  </table>
</div>
<div>&nbsp;</div>
<table class="table table-bordered " cellspacing="3" cellpadding="15" >
<tbody>
<tr class="rowheader">
            <td width="10%" class="text-center text-bold" style="padding: 2px"><h4>SN</h4></td>
            <td width="40%" class="text-center text-bold" ><h4>Material</h4></td>
            <td width="30%" class="text-center text-bold" ><h4>Description</h4></td>
            <td width="15%" class="text-center text-bold" style="padding: 2px"><h4>Total Qty </h4></td>
            <td width="15%" class="text-center text-bold" style="padding: 2px"><h4>Request</h4></td>
            <td width="15%" class="text-center text-bold" style="padding: 2px"><h4>Order</h4></td>
            <td width="15%" class="text-center text-bold" style="padding: 2px"><h4>Price</h4></td>
            <td width="20%" class="text-center text-bold" style="padding: 2px"><h4>Total</h4></td>
        </tr>
        </tr>
                  <?php foreach($productInfo as $key => $value){
                    // pr($product);
                     ?>
           <tr>
                <td class="text-center"style="font-size:17px" > <?php echo $key+1;?> </td>
                <td class="text-left"style="font-size:17px" > <?php echo ($value['material_id']);?></td>
                <td class="text-left"style="font-size:17px"> <?php echo ($value['description']);?></td>
                <td class="text-right"style="font-size:17px"> <?php echo ($value['total_quantity']);?></td>
                <td class="text-right"style="font-size:17px"> <?php echo ($value['request_quantity']);?></td>
                <td class="text-right"style="font-size:17px"> <?php echo ($value['order_quantity']);?></td>
               
                <td class="text-right"style="font-size:17px" > <?php echo number_format($value['price'], 2, '.', ',');?></td>
                <td class="text-right"style="font-size:17px" > <?php echo number_format($value['total_amount'], 2, '.', ',');?></td>
                 </tr> 
                 
                 <?php } ?>
                 <tr class="">
                    <td style="border: 0" colspan="4"> </td>
                    <td class="text-center" colspan="3">Sub Total</td>
                    <td class="text-right"><?php echo $subtotalAmount; ?></td>
                </tr>
                <tr class="">
                    <td style="border: 0" colspan="4"> </td>
                    <td class="text-center" colspan="3">VAT</td>
                    <td class="text-right"><?php echo $VatAmount; ?></td>
                </tr>
                <tr class="">
                    <td style="border: 0" colspan="4"> </td>
                    <td class="text-center" colspan="3">Discount</td>
                    <td class="text-right"><?php echo $Discount; ?></td>
                </tr>
  
                <tr class="rowfooter">
                <td style="border: 0" colspan="4"> </td>
                      <td class="text-center text-bold" colspan="3" style="padding: 15px"> Grand Total </td>  
                      <td class="text-right text-bold" style="padding: 15px"><?php echo $total_amount; ?></td>
                </tr>
                
</tbody>
</table>
<table class='table'>
        
        <tr>
                      <td class="text-center-words  text-bold " style="font-size: 13px;"  >
                      Amount in words(<?php echo ucfirst($word);?>) 
                      </td>
                     
                  </tr>
  </table>

  <table class="info-table">
   
            <tr>
          
                <td>Payment terms<br></td>
                <td>:&nbsp;</td>
                <td valign="bottom"> <?php echo $paymentName?></td>
                   <br>
                   <td> </td>
                   <td > </td>
               
                <td> </td>
                <td > </td>

            </tr>
            
            
</table>

<br>
    <div class="ignore_page_brack">
    

        <table class="table ">
            <tr>
                <td class= "text-company">
               For  <?php echo $companyName?> 
                  </td>
                  </tr>
            <tr>
                <td style="height: 100px">&nbsp;</td>
            </tr>

        </table>
        <table class="table ">
            <tr>
                <td>Prepared By  : _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _<br>
                    </td>
                <td>Approved By: _ _ _ _ _ _ _ _ _ _ _</td>
            </tr>
        </table>
    </div>
  
    </div>
    
    
    
    
    
    
    
    
    
    
    
    
    
           
