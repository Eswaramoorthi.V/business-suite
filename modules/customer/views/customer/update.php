<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$type = $_GET['type'];
//pr($type);
$clientType=[1=>'Company',2=>'Individual'];
$customertype=['1'=>'client','2'=>'sub-contractor'];
$salutation=['1'=>'Mr','2'=>'Mrs','3'=>'Ms'];

?>
<div id='msg' class='msg'></div>
<div class="customer-form form-vertical">
    <?php $form = ActiveForm::begin(['id'=>'active-form']); ?>
     <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
<div class="row">

        <legend class="text-info"><small>Update</small></legend>
        <div class="col-md-4">
            <?=
            $form->field($model, 'code', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                   // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " Code"])->label('Code')
            ?>
        
        </div>
        <div class="col-md-4 type">
            <?=
            $form->field($model, 'type', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->dropDownList($clientType, [
                'class' => 'select form-control',
                'id'=>'clientType',
               
            ])->label("Type")
            ?>
        </div>
 
            <?=
            $form->field($model, 'customer_type', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
          ])->hiddenInput($customertype, [
                'class' => 'select form-control',
               
            ])->label('')
            ?>
   
    
        <div class="col-md-4" id ='typeHide'>
            <?=
            $form->field($model, 'company_name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " Company name"])->label(' Company Name')
            ?>
        
    </div>
   
</div>
    <div class="row">
        <legend class="text-info"><small>Primary Contact</small></legend>
         <div class="col-md-2">
            <?=
            $form->field($model, 'salutation', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    //'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
           ])->dropDownList($salutation, [
                'class' => 'select from-control',
               // 'id'=>'clientType',
               
            ])->label("salutation")
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'customer_fname', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    //'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " First name"])->label('First Name')
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'customer_lname', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    //'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " Last name"])->label('Last Name')
            ?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'customer_mid_name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    //'inputOptions' => ['class' => 'sdsdsd'],
            ])->textInput(['placeholder' => " Middle name"])->label('Middle Name')
            ?>
        </div>


        <div class="col-md-4">
            <?=
            $form->field($model, 'email', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Enter email"])->label('Email')
            ?>
        </div>
        <div class="col-md-4">
<?=
$form->field($model, 'phone_number', [
    'template' => "{label}\n
          <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
          </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
        // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
])->textInput(['placeholder' => "Enter Phonenumber"])->label('Phone Number')
?>
        </div>
        <div class="col-md-4">
<?=
$form->field($model, 'website', [
    'template' => "{label}\n
          <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
          </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
        // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
])->textInput(['placeholder' => "Enter website"])->label('WebSite')
?>
        </div>
    </div>
    <div class="row">
        <legend class="text-info"><small>Address</small></legend>


        <div class="col-md-6">
            <?=
            $form->field($model, 'address', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textarea(['placeholder' => "line1"])->label('Billing Address')
            ?>
        </div>
        <div class="col-md-6">
<?=
$form->field($model, 'address_line2', [
    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
        //'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
])->textarea(['placeholder' => "line2"])->label('Address Line2')
?>
        </div>

        <div class="col-md-6">
            <?=
            $form->field($model, 'trn_number', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textinput(['placeholder' => " TRN No."])->label('TRN Number')
            ?>
        </div>
        <div class="col-md-6">
            <?=
            $form->field($model, 'description', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textarea(['placeholder' => "Notes"])->label('Notes')
            ?>
        </div>
    </div>

  <div class="col-md-12" input-group-btn style="margin-top:21px">
          

        <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label" for=""></label>
            <div class="col-md-12 col-xs-6">
                        <input type="hidden" name="type" value="<?php echo $model->customer_type; ?>">
<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
            </div>
        </div>
         </div>
<?php ActiveForm::end(); ?>  
    </div>
</div>
 


<?= $this->registerJs("

    $('.add-more').click(function(){ 
        $('.rmbtn').removeClass('hide');
        var html = $('.copyinvitems').html();
        $('.after-add-more').after(html);
         $('.copyinvitems  .rmbtn').addClass('hide');
    });
    
    $('body').on('click','.remove',function(){ 
       $(this).parents('.control-group').remove();
     });

    


", View::POS_READY); ?>

<script> 
$("form#active-form").submit(function(e) {
    e.preventDefault();
    event.stopPropagation();

    $("#customer-customer_fname").removeClass("error-highlight");
    $("#customer-customer_lname").removeClass("error-highlight");
    $("#customer-phone_number").removeClass("error-highlight");
    $("#customer-email").removeClass("error-highlight");
    $("#customer-address").removeClass("error-highlight");
    $("#customer-city").removeClass("error-highlight");
    $("#customer-state").removeClass("error-highlight");
    $("#customer-country").removeClass("error-highlight");


   var formData = new FormData(this);
//      if($('#Type').val()==='') {
//           //alert("Name cannot be Empty ");
//            $("#error-message").removeClass("hide");
//            $("#error-message").html("<p>Type cannot be Empty</p>");
//            $("#Type").addClass("error-highlight");
//            return false;
//       }
//       else if($('#clientType').val()==='') {
//            $("#error-message").removeClass("hide");
//            $("#error-message").html("<p>Client type cannot be Empty</p>");
//            $("#clientType").addClass("error-highlight");
//            return false;
//       }
       if($('#customer-customer_fname').val()===''){
           $("#error-message").removeClass("hide");
            $("#error-message").html("<p>First Name cannot be Null</p>");
            $("#customer-customer_fname").addClass("error-highlight");
            return false;
       }
       else if($('#customer-customer_lname').val()==='') {
           $("#error-message").removeClass("hide");
           $("#error-message").html("<p>Last Name Cannot be Null</p>");
           $("#customer-customer_lname").addClass("error-highlight");
            return false;
       }
       else if($('#customer-phone_number').val()==='') {
          $("#error-message").removeClass("hide");
           $("#error-message").html("<p>Phone Number Cannot be Null</p>");
           $("#customer-phone_number").addClass("error-highlight");
            return false;
       }
       else if($('#customer-email').val()==='') {
          $("#error-message").removeClass("hide");
           $("#error-message").html("<p>Email Cannot be Null</p>");
           $("#customer-email").addClass("error-highlight");
            return false;
       }
       else if(!ValidateEmail($("#customer-email").val())) {
           $("#error-message").removeClass("hide");
           $("#error-message").html("<p>Enter Valid E-mail ID</p>");
           $("#customer-email").addClass("error-highlight");
            return false;
        }
         else if($('#customer-address').val()==='') {
          $("#error-message").removeClass("hide");
           $("#error-message").html("<p>Enter address</p>");
           $("#customer-address").addClass("error-highlight");
            return false;
       }
        else if($('#customer-city').val()==='') {
          $("#error-message").removeClass("hide");
           $("#error-message").html("<p>Enter City</p>");
           $("#customer-city").addClass("error-highlight");
            return false;
       }
        else if($('#customer-state').val()==='') {
          $("#error-message").removeClass("hide");
           $("#error-message").html("<p>Enter State</p>");
           $("#customer-state").addClass("error-highlight");
            return false;
       }
       else if($('#customer-country').val()==='') {
          $("#error-message").removeClass("hide");
           $("#error-message").html("<p>Enter Country</p>");
           $("#customer-country").addClass("error-highlight");
            return false;
       }
       else {
           $("#error-message").addClass("hide");
         }
    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('Client Created');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();

            },
        cache: false,
        contentType: false,
        processData: false
    });
     
});

 function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };
</script>

<?php
$js = <<< JS
$(document).ready(function(){
     var type='$type';
          if(type==3){
              $('.type').hide();
          }
          else{
              $('.type').show();
          }
            
});
JS;
$this->registerJs($js);
?>