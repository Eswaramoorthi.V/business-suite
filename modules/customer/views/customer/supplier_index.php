<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;

//$this->title = 'Clients';
$this->params['breadcrumbs'][] ='Suppliers';
//$this->params['title_icon'] = 'fa fa-users';

?>

<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-handshake-o" aria-hidden="true"></i> Suppliers
    </div>
    
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
        <?= Html::button('Add Suppliers',['value'=>Url::to('create?r=customer/create&type=3'),'class'=>'btn btn-success','id'=>'modalButton'])?>
    </div>
</div>
<?php
            
    Modal::begin([
        'header' => '<h4>Supplier Information</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalContent'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
            
    Modal::begin([
        'header' => '<h4>Supplier Update</h4>',
        'id' => 'update',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalupdate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
if ($dataProvider) {


    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false, 'hiddenFromExport' => false],
        [
            'attribute' => 'code',
            'header' => '<div style="width:120px;">Code</div>',
            'value' => function($model, $key, $index) {
                return !empty($model->code)?$model->code:'-';
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        [
            'attribute' => 'coustomer_fname',
            'header' => '<div style="width:120px;">Contact Name</div>',
            'value' => function($model, $key, $index) {
               return  Html::a($model->customer_fname .' '.$model->customer_lname, ['view', 'id' => $model->id], [ 'data-pjax'=>"0"]);
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        
       [
            'attribute' => 'company_name',
           'header' => '<div style="width:120px;">Company Name</div>',
            'value' => function($model, $key, $index) {
               return !empty($model->company_name)?$model->company_name:'-';
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        [
            'attribute' => 'type',
            'header' => '<div style="width:120px;">Type</div>',
            'value' => function($model, $key, $index) {
                 $type=[1=>'Company',2=>'Individual'];
                 return  $type[$model->type];
               
            },
            'vAlign' => 'middle',
            'format' => 'raw',
           // 'width' => '150px',
            'noWrap' => true
        ],
       
        [
            'attribute' => 'email',
            'header' => '<div style="width:120px;">Email</div>',
            'value' => function($model, $key, $index) {
               return$model->email;
            },
            'vAlign' => 'middle',
            'format' => 'raw',
           // 'width' => '150px',
            'noWrap' => true
      ],
      [
            'attribute' => 'phone_number',
          'header' => '<div style="width:120px;">Contact Number</div>',
            'value' => function($model, $key, $index) {
               return $model->phone_number; 
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
      

    [
    
    'class' => 'yii\grid\ActionColumn',
    'contentOptions' => ['style' => 'width:100px; text-align:center;','class'=>'skip-export'],
    'headerOptions' =>['class'=>'skip-export'],
    'header'=>"Actions",
    'template' => '{view}{update}{deletes}',
    'buttons' => [
              
      'view' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-th-list icons"></span>',
                $url, 
                [
                    'title' => 'View',
                ]
            );
        },
                
        'update' => function($url) {
                    return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url."&type=3", ['class' => 'update','title'=>'Edit']
                    );
                },
                
        'deletes' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-trash icons"></span>',
                $url."&type=2", 
                [
                    'title' => 'delete','data' => ['confirm' => 'Are you sure want to delete?'],
                ]
            );
        },
    ],
],
    ];


     echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'responsiveWrap' =>false,
    'columns' => $gridColumns, // check the configuration for grid columns by clicking button above
    
    'persistResize' => false,
         
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
    'beforeHeader' => [
    [
        'columns' => [
            ['content' => 'List Of Subcontractors', 'options' => ['colspan' => 5, 'class' => 'hide','style' => 'font-size:18px']],
                ],],
    ],
    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => false, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar' =>  [
      '{export}',
   ],
    // set export properties
    'export' => [
        'fontAwesome' => true,
        'showConfirmAlert' => false
    ],
    // parameters from the demo form
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    //'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '',
    ],
    'exportConfig' => [GridView::EXCEL => [
    'filename' => 'Client List',
    ],
            GridView::PDF => [
                'filename' => 'Subcontractor Lists',
                'showHeader' => true,
                //'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'config' => [
                    //'cssInline' =>'table{direction:0}' ,
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                ],
          ]
        
  
        ],
]);
    
   
}
?>                                   
<?= $this->registerJs("
    $(function(){
$('#modalButton').click(function() {
    $('#modal').modal('show')
    .find('#modalContent')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>

<?= $this->registerJs(

  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.update').click(function(e){
       e.preventDefault();      
       $('#update').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);