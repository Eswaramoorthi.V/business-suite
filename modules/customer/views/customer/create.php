<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PreventiveMaintenance */

$this->title = 'Clients';
//$this->params['breadcrumbs'][] = ['label' => 'Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-user';


?>
<div class="customer-create">



    <?= $this->render('_form', [
        'model' => $model,
        
    ]) ?>

</div>
