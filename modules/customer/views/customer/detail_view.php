<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use app\models\Country;

/* @var $this yii\web\View */
/* @var $model app\models\AssetMaster */

//$this->title = 'Client';
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = !empty($model->company_name)?($model->company_name):($model->customer_fname);

$companyName= $model['company_name'];
$clientType = $model['customer_type'];
$individualName= $model['customer_fname']." ".$model['customer_lname'];
 $type=[1=>'Company',2=>'Individual'];
 Modal::begin([
    'header' => '<h4>Update</h4>',
    'id' => 'update',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalupdate'><div class='loader'></div></div>";
Modal::end();
?>
<div class="row">
   <div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-user" aria-hidden="true"></i> Client |  <small><?php echo !empty($companyName)?($companyName):$individualName;  ?></small> </h1>
        <ul class="breadcrumb">

        </ul>    </section>
</div>                

    
   <div class="nav-tab-custom">
 <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      
                                           
                <section class="content edusec-user-profile">
                  
                        <div class="table-responsive">
                              <p class="text-right" >
                    <?=
                    Html::a(Yii::t('app', ' {modelClass}', [
                                'modelClass' => 'Edit',
                            ]), ['customer/update', 'id' => $model->id,'type'=>$clientType], ['class' => 'btn btn-success','id'=>'clientupdate']);
                    ?>  </p>   
                            <table class="table">
                                <colgroup>
                                    <col style="width:15%">
                                    <col style="width:35%">
                                    <col style="width:15%">
                                    <col style="width:35%">
                                </colgroup>
                                <tbody><tr>
                                        <th> Type</th>
                                        <td><?php echo $type[$model->type];
                                       ?></td>
                                        <?php if($model->type == 1){ ?>
                                       <th> Company Name</th>
                                        <td><?php echo
                                       !empty($model->company_name)?($model->company_name):' - '; ?></td>
                                        <?php } ?>
                                   </tr>
                                    <tr>
                                        <th>Contact Person</th>
                                        <td><?php echo  !empty($model->customer_fname)?($model->customer_fname)." ".($model->customer_lname)." ".($model->customer_mid_name):' - ' ; ?></td>
                                        <th>Contact Number</th>
                                        <td><?php echo $model->phone_number ?></td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td><?php echo $model->email ?></td>
                                        <th>Remarks</th>
                                        <td><?php echo $model->description ?></td>
                                    </tr>

                                </tbody>
                            </table>
                            <div></div>
                            <table class="table">
                                <colgroup>
                                    <col style="width:3%">
                                    <col style="width:35%">
                                </colgroup>
                                <tbody><th style="font-size: 20px" >Address</th ><tr>
                                        <th> Line1 </th>
                                        <td><?php echo $model->address;
                                       ?></td>
                                        
                                    </tr>
                                   
                           
                                </tbody>
                            </table>
                        </div>
                </section>               
            </div>
        </div>
   </div>
</div>
<?php
$this->registerJs("$(function() {
   $('#clientupdate').click(function(e) {
     e.preventDefault();
     $('#update').modal('show').find('#modalupdate')
     .load($(this).attr('href'));
   });
   

});");
?>
   
