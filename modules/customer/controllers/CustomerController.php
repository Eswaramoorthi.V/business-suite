<?php

namespace app\modules\customer\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use app\models\Customer;
use app\models\Search\CustomerSearch;
use app\models\Search\SubcontractorSearch;
use app\models\Search\SupplierSearch;
use app\models\Contacts;
use app\controllers\BaseController;


/**
 * EquipmentGroupController implements the CRUD actions for EquipmentGroup model.
 */
class CustomerController extends BaseController {
    
    
     
    public function actionIndex() {
   
          $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $dataProvider->pagination = ['PageSize'=>50];
        return $this->render('index', [
          
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }
    public function actionSubcontractorIndex() {
   
          $searchModel = new SubcontractorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $dataProvider->pagination = ['PageSize'=>50];
        return $this->render('subcontractor_index', [
          
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }
     public function actionSupplierIndex() {
   
          $searchModel = new SupplierSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          $dataProvider->pagination = ['PageSize'=>50];
        return $this->render('supplier_index', [
          
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }
    
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate() {
        
        $model = new Customer();
        $type = Yii::$app->request->get('type');
        if ($model->load(Yii::$app->request->post())) {
            // pr(Yii::$app->request->post());
            $postData=Yii::$app->request->post();
            $model->code=$postData['Customer']['code'];
             $type = Yii::$app->request->post('type');  
             $model->save();
             
            if ($type == 1) {
                return $this->redirect(['index']);
            } if ($type == 2) {
                return $this->redirect(['subcontractor-index']);
            } else {
                return $this->redirect(['supplier-index']);
            }    
            } else {
                $model->customer_type = $type;
                return $this->renderAjax('create', [
                    'type' => $type,
                    'model' => $model]
                );
            
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $postData=Yii::$app->request->post();
            // pr($postData);
            $model->code=$postData['Customer']['code'];
              $model->save();
            $type = Yii::$app->request->post('type');
             if ($type == 1) {
                return $this->redirect(['index']);
            } if ($type == 2) {
                return $this->redirect(['subcontractor-index']);
            } else {
                return $this->redirect(['supplier-index']);
            }
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }
   public function actionDeletes($id)
    {
       $type = Yii::$app->request->get('type');
        $this->findModel($id)->delete();
        if ($type == 1) {
            return $this->redirect(['index']);
        } else {
            return $this->redirect(['subcontractor-index']);
        }
    }


    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionAddressGroupServices(){
        $customerId=$_REQUEST['id'];
        $list = Customer::find()->where(['id'=>$customerId])->one();
        //pr($list);
        $address=$list['address'];
        $line2=$list['address_line2'];
        $city=$list['city'];
        $state=$list['state'];
        $code=$list['zip_code'];
        $country=$list['country'];
        
         echo $address .','. $line2.','.$city.','.$state.','.$code.','.$country;
            
    }
    
    
    
    public function actionContactGroup(){
        $customerId = $_REQUEST['id'];
        $list = Contacts::find()->where(['customer_id' => $customerId])->asArray()->all();
        $details = ArrayHelper::map($list, 'id', 'first_name');
        
       // pr($details);
        echo "<option value=''>Select Contact</option>";
        foreach ($details as $id => $name) {
            echo "<option value='$id'>$name</option>";
        }
    }
    
    public function actionDeliveryAddressGroup(){
        $customerId=$_REQUEST['id'];
        $list = Customer::find()->where(['id'=>$customerId])->one();
        //pr($list);
        $address=isset($list['same_address'==0])?$list['delivery_address']:$list['address'];
        $line2=isset($list['same_address'==0])?$list['delivery_address_line2']:$list['address_line2'];
        $city=isset($list['same_address'==0])?$list['delivery_city']:$list['city'];
        $state=isset($list['same_address'==0])?$list['delivery_state']:$list['state'];
        $code=isset($list['same_address'==0])?$list['delivery_zip_code']:$list['zip_code'];
        $country=isset($list['same_address'==0])?$list['delivery_country']:$list['country'];
        
        
         echo $address .','. $line2.','.$city.','.$state.','.$code.','.$country;
            
    }
    
    
}