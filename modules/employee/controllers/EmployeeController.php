<?php

namespace app\modules\employee\controllers;

use Yii;
use app\models\Employee;
use app\models\Project;
use app\models\EmployeeProject;
use app\models\EmployeeSearch;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\mpdf\Pdf;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\Company;
use yii\web\UploadedFile;
use app\models\AuthAssignment;
use \dektrium\user\models\User;
use app\components\MenuHelper;
use app\models\EmployeeCompany;
use app\controllers\BaseController;
/**
 * ProjectController implements the CRUD actions for Project model.
 */
class EmployeeController extends BaseController
{
    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        $route=$this->route;
        
        $actioList = MenuHelper::getActionHelper($route);
        // pr($actioList);
        $visibleList = MenuHelper::getActionVisibleHelper($route);
    //   pr($visibleList);
        $searchModel = new EmployeeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize' => 10];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actioList' => $actioList,
            'visibleList' => $visibleList
        ]);

    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Employee();
        $model2 = new AuthAssignment();

        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();

            $EmployeeData = $request['Employee'];
            $employeeUniqueId = $EmployeeData['employee_id'];


            $filenameDoc = '';

            $directoryPath = 'uploads/';
            if (!file_exists($directoryPath)) {
                mkdir($directoryPath);
            }
            $document = UploadedFile::getInstance($model, 'document');
            if (!empty($document)) {
                if ($model->document = $document) {

                    $dirname = $employeeUniqueId;
                    // pr($dirname);
                    $subfolder = "personal-documents";
                    $folder = 'uploads/employee/' . $dirname . '/';
                    $filename = 'uploads/employee/' . $dirname . '/' . $subfolder . '/';
                    if (file_exists($filename)) {

                        $filenameDoc = $filename . $model->document->baseName . '_' . time() . '.' . $model->document->extension;

                    } else {
                        if (file_exists($folder)) {
                            mkdir('uploads/employee/' . $dirname . '/' . $subfolder . '/');
                            $filenameDoc = $filename . $model->document->baseName . '_' . time() . '.' . $model->document->extension;

                        } else {
                            mkdir('uploads/employee/' . $dirname . '/');
                            mkdir('uploads/employee/' . $dirname . '/' . $subfolder . '/');
                            $filenameDoc = $filename . $model->document->baseName . '_' . time() . '.' . $model->document->extension;

                        }
                    }
                }
                $model->document->saveAs($filenameDoc);
                $model->document = $model->document->baseName . '_' . time() . '.' . $model->document->extension;
            }
            $model->email = $request['Employee']['email'];
            $model->username = $request['Employee']['username'];
            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
            $model->role = $request['Employee']['role'];
            $model->first_name = $request['Employee']['first_name'];
            $model->last_name = $request['Employee']['last_name'];
            $model->trade = $request['Employee']['trade'];
            $model->gender = $request['Employee']['gender'];
            $model->basic_salary = $request['Employee']['basic_salary'];
            $model->allowance = $request['Employee']['allowance'];
            $model->phone_number = $request['Employee']['phone_number'];
            $model->mobile_number = $request['Employee']['mobile_number'];
            $model->notes = $request['Employee']['notes'];
            // $model->company_name = $request['Employee']['company_name'];
            $model->employee_id = $request['Employee']['employee_id'];
            $model->hire_date = date('Y-m-d', strtotime($EmployeeData['hire_date']));
            $model->date_of_birth = date('Y-m-d', strtotime($EmployeeData['date_of_birth']));
            $model->visa_expirydate = date('Y-m-d', strtotime($EmployeeData['visa_expirydate']));
            $model->auth_key = \Yii::$app->security->generateRandomString();
            $model->confirmed_at = time();
            $model->created_at = time();
            $model->updated_at = time();
            // foreach ($companylist as $companyName){
            //     $model->company_name = $companyName;
            // }
            $model->save();
            

            if (!$model->save()) {
                pr($model->getErrors());
            }
            $model2->user_id = $model->id;
            $model2->item_name = $model->role;
            $model2->created_at = time();
            $model2->save();

            if (!$model2->save()) {
                pr($model2->getErrors());
            }
            return $this->redirect('index');
        } else {
           
            // pr($userProjectList);
            return $this->renderAjax('create', [
                'model' => $model,
                'model2' => $model2,
                
            ]);
        }
    }


    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {

        $model = Employee::find()->where(['id' => $id])->one();

        $prevPwd = $model['password_hash'];

        $model2 = AuthAssignment::find()->where(['=', 'user_id', $id])->one();

        $employUniqueId = $model['employee_id'];
        if (!empty($model->password_hash)) {
            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
        } else {
            $model->password_hash = $prevPwd;
        }
        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();
            $EmployeeData = $request['Employee'];

            $model->document = $request['Employee']['document'];


            $documentUpload = UploadedFile::getInstance($model, 'document');
            $filePath = '';
            if (!empty($documentUpload)) {
                if ($model->document = $documentUpload) {

                    $directoryName = $employUniqueId;
                    $subfolderName = "personal-documents";
                    $folderName = 'uploads/employee/' . $directoryName . '/';
                    $filePath = 'uploads/employee/' . $directoryName . '/' . $subfolderName . '/';
                    if (file_exists($filePath)) {

                        $filename = $filePath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;

                    } else {
                        if (file_exists($folderName)) {
                            mkdir('uploads/employee/' . $directoryName . '/' . $subfolderName . '/');
                            $filename = $filePath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;

                        } else {
                            mkdir('uploads/employee/' . $directoryName . '/');
                            mkdir('uploads/employee/' . $directoryName . '/' . $subfolderName . '/');
                            $filename = $filePath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;

                        }
                    }
                }
                $model->document->saveAs($filename);
                $model->document = $model->document->baseName . '_' . time() . '.' . $model->document->extension;
            }

            $model->hire_date = date('Y-m-d', strtotime($EmployeeData['hire_date']));
            $model->date_of_birth = date('Y-m-d', strtotime($EmployeeData['date_of_birth']));
            $model->visa_expirydate = date('Y-m-d', strtotime($EmployeeData['visa_expirydate']));

            $model->email = $request['Employee']['email'];
            $model->username = $request['Employee']['username'];
            $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
            $model->employee_id = $request['Employee']['employee_id'];
            $model->role = $request['Employee']['role'];
            $model->first_name = $request['Employee']['first_name'];
            $model->last_name = $request['Employee']['last_name'];
            $model->trade = $request['Employee']['trade'];
            $model->gender = $request['Employee']['gender'];

            $model->basic_salary = $request['Employee']['basic_salary'];
            $model->allowance = $request['Employee']['allowance'];
            $model->phone_number = $request['Employee']['phone_number'];
            $model->mobile_number = $request['Employee']['mobile_number'];
            $model->notes = $request['Employee']['notes'];
            // $model->company_name = $request['Employee']['company_name'];
            $model->auth_key = \Yii::$app->security->generateRandomString();
            $model->confirmed_at = time();
            $model->created_at = time();
            $model->updated_at = time();
            $model->save();
            if (!$model->save()) {
                pr($model->getErrors());
            }
            $model2->user_id = $model->id;
            $model2->item_name = $model->role;
            $model2->created_at = time();
            $model2->save();

            if (!$model2->save()) {
                pr($model2->getErrors());
            }
            return $this->redirect('index');
        } else {
            
            return $this->renderAjax('update', [
                'model' => $model,
                'model2' => $model2,
               
            ]);
        }
    }

    public function actionChangepassword($id)
    {

        $model = Employee::find()->where(['id' => $id])->one();

        // $prevPwd = $model['password_hash'];
        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();
            if (!empty($model->password_hash)) {
                $model->password_hash = Yii::$app->security->generatePasswordHash($model->password_hash);
                $model->confirm_password = Yii::$app->security->generatePasswordHash($model->confirm_password);
            }
            $model->save();

            if (!$model->save()) {
                pr($model->getErrors());
            }
            return $this->redirect('index');
        } else {
            return $this->renderAjax('changepassword', [
                'model' => $model,

            ]);
        }
    }

    public function actionAssignproject($id)
    {
        $updateProject = new EmployeeProject();
        if (isset($_REQUEST['projectlist']) and !empty($_REQUEST['projectlist'])) {
            $projectList= $_REQUEST['projectlist'];
            $updateProject = EmployeeProject::deleteAll(['user_id' => $id]);
            foreach ($projectList as $projectId){
                $updateProject = new EmployeeProject();
                $updateProject->user_id = $id;
                $updateProject->project_id = $projectId;
                $updateProject->created_at = time();
                $updateProject->updated_at = time();
                if (!$updateProject->save()) {
                    pr($updateProject->getErrors());
                }
            }

            return $this->redirect('index');
        } else {
            $projectListData =Project::find()->select(['project_name','id'])->asArray()->all();

$assign_project = [];

foreach ($projectListData as $value) {
    $eType =[];
    $eType['id'] = $value['id'];
    $eType['project_name'] = $value['project_name'];
    $assign_project[]=$eType;
}
$projectList = ArrayHelper::map($assign_project, 'id', 'project_name');

$userProject= EmployeeProject::find()->select('project_id')->where(['user_id'=>$id])->asArray()->all();

$userProjectList = ArrayHelper::getColumn($userProject, 'project_id');
// pr($userProjectList);
            return $this->renderAjax('assignproject', [
                'model3' => $updateProject,
                'projectList'=>$projectList,
                'userProjectList'=>$userProjectList

            ]);
        }
    }

    public function actionAssigncompany($id)
    {
        $updateCompany = new EmployeeCompany();
        if (isset($_REQUEST['companylist']) and !empty($_REQUEST['companylist'])) {
            $companyList= $_REQUEST['companylist'];
            $updateCompany = EmployeeCompany::deleteAll(['user_id' => $id]);
            foreach ($companyList as $companyId){
                $updateCompany = new EmployeeCompany();
                $updateCompany->user_id = $id;
                $updateCompany->company_id = $companyId;
                $updateCompany->created_at = time();
                $updateCompany->updated_at = time();
                if (!$updateCompany->save()) {
                    pr($updateCompany->getErrors());
                }
            }

            return $this->redirect('index');
        } else {
            $companyListData =Company::find()->select(['company_name','id'])->asArray()->all();

            $assign_company= [];
            
            foreach ($companyListData as $value) {
                $eType =[];
                $eType['id'] = $value['id'];
                $eType['company_name'] = $value['company_name'];
                $assign_company[]=$eType;
            }
            $companyList = ArrayHelper::map($assign_company, 'id', 'company_name');
         
            $userCompany= EmployeeCompany::find()->select('company_id')->where(['user_id'=>$id])->asArray()->all();
            $userCompanyList = ArrayHelper::getColumn($userCompany, 'company_id');
                        return $this->renderAjax('assigncompany', [
                            'model4' => $updateCompany,
                            'companyList'=>$companyList,
                            'userCompanyList'=>$userCompanyList
            
                        ]);
                    }
                }



    public function actionDownload($id)
    {

        $download = User::findOne($id);
        $dirname = $download['employee_id'];
        //$filename = $download['document'];
        $path = Yii::getAlias('@webroot') . '/uploads/employee/' . $dirname . '/personal-documents/' . $download->document;
        //pr($path);
        if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        } else {
            throw new NotFoundHttpException("can't find {$download->document} file");
        }
    }


    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionDeletes($id)
    {
        Employee::deleteAll(['id' => $id]);
        AuthAssignment::deleteAll(['user_id' => $id]);;

        return $this->redirect('index');
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Employee::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDetailview()
    {
        $id = $_GET['id'];
        return $this->render('detail_view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionEmployeeIdCheck()
    {
        $employee_id = $_REQUEST['employee_id'];
        $codeList = User::find()->where(['employee_id' => $employee_id])->one();
        echo $codeList['employee_id'];
    }

    public function actionGetUsernameCheck()
    {
        $username = $_REQUEST['username'];
        $codeList = User::find()->where(['username' => $username])->one();
        echo $codeList['username'];
    }

    public function actionGetEmailCheck()
    {
        $email = $_REQUEST['email'];
        $codeList = User::find()->where(['email' => $email])->one();
        echo $codeList['email'];
    }

}