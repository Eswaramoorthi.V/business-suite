<?php

use yii\helpers\Html;

$this->title = 'Create Employee';
$this->params['breadcrumbs'][] = ['label' => 'Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="project-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
