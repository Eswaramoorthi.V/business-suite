<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Permission;
$model->hire_date=date('d-m-Y',strtotime($model->hire_date));
$model->date_of_birth=date('d-m-Y',strtotime($model->date_of_birth));
$model->visa_expirydate=date('d-m-Y',strtotime($model->visa_expirydate));
$gender = ['1' => 'Male', '2' => 'Female'];
$trade = ['1' => 'Painter', '2' => 'Manager','3' => 'Admin', '4' => 'Supervisor','5' => 'Driver', '6' => 'Gypsum Carpentor','7' => 'Mason', '8' => 'Helper','9' => 'Plumber', '10' => 'Furniture Carpentor','11' => 'Electrician', '12' => 'Operator'];
$role = ArrayHelper::map(Permission::find()->where("type=1 AND (name!='admin' and name!='Facility Manager') ")->all(), 'name', 'name');
?>
<div class="employee-form form-vertical">
<?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <!-- <legend class="text-info"><small>Update</small></legend> -->
    <div class="col-md-3">
    <?=
            $form->field($model, 'username', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Username",'class' => "username form-control"])->label('Username')
            ?>
       
       <h5 class="error-msg2 hide text-danger" style="font-size: 12px;"> </h5>
      
       </div>
        <div class="col-md-3">
        <!-- <?=
            $form->field($model, 'password_hash', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 password control-label', 'style' => 'color:black'],
            ])->passwordInput(['placeholder' => "Password", 'value'=>''])->label('Password')
            ?> -->
             <?=
            $form->field($model, 'email', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Email",'class' => "email form-control"])->label('Email')
            ?>
       
       <h5 class="error-msg3 hide text-danger" style="font-size: 12px;"> </h5>

              </div>
              <div class="col-md-3">
              <?=
            $form->field($model, 'role', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($role,[
                'prompt' => 'Select Role',
                'class'=>'select from control',
            ])->label("Role")
            ?>
                           </div>
              
        <div class="col-md-3">
        <?=
            $form->field($model, 'first_name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
             ])->textInput(['placeholder' => "First name"])->label('First Name')
            ?>
                </div>
        </div>
       <br>
    <div class="row">
        <div class="col-md-3">
        <?=
            $form->field($model, 'last_name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Last name"])->label('Last Name')
            ?>
            
        </div>
       
        <div class="col-md-3">
        <?=
            $form->field($model, 'employee_id', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Employee Id",'class' => "employee_id form-control"])->label('Employee Id')
            ?>
       
       <h5 class="error-msg4 hide text-danger" style="font-size: 12px;"> </h5>
        </div>
        <div class="col-md-3">
        <?=
            $form->field($model, 'trade', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($trade,[
                'prompt' => 'Select Designation',
                'class'=>'select from control',
            ])->label("Designation")
            ?>
        </div>
        <div class="col-md-3">
        <?php $model->isNewRecord ? $model->gender = 'Male' : $model->gender = $model->gender ;  ?>
    <?=
            $form->field($model, 'gender', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->radioList($gender,[
               // 'prompt' => '  Select Gender',
                'class'=>'select from control',
            ])->label("Gender")
            ?>
            </div>
                </div>
                <br>
                <div class ="row">
                <div class="col-md-3">
                <?=
            $form->field($model, 'date_of_birth', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Date Of Birth')
            ?>
                </div>

    <div class="col-md-3">
    <?=
            $form->field($model, 'basic_salary', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
             ])->textinput(['placeholder' => "Basic Slalary"])->label('Basic Slalary')
            ?>
                </div>
                <div class="col-md-3">
                <?=
            $form->field($model, 'allowance', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
             ])->textinput(['placeholder' => "Allowance"])->label('Allowance')
            ?>
        </div>
        <div class="col-md-3">   
        <?=
            $form->field($model, 'phone_number', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Enter Phone Number"])->label('Phone'); ?>
 </div> 
 </div> 

<br>
                <div class="row">
                
  
    <div class="col-md-3">
    <?=
            $form->field($model, 'mobile_number', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Enter Mobile Number"])->label('Mobile'); ?>
        </div>
        <div class="col-md-3">
        <?=
            $form->field($model, 'hire_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput([])->label('Hire Date')
            ?>      
        </div>
        <div class="col-md-3">
        <?=
            $form->field($model, 'visa_expirydate', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput([])->label('Visa Expiry')
            ?>   
        </div>
        <div class="col-md-3">
               
        <?=
            $form->field($model, 'notes', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
             ])->textarea(['rows' =>3,'placeholder' => "Enter Notes"])->label('Notes')
            ?>
                </div>

            </div>
        
        <br>
    <div class="row">
        <div class="col-md-6">
        <?php $documentpath="/pricingcalculator/web/uploads/employee/".$model->employee_id."/personal-documents"."/".$model->document;?>
    <?php if(is_null($model->document) || empty($model->document)): ?>
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9'>
                {input}\n
                {hint}\n
                {error}
          </div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php else: ?>
  
 
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9 col-xs-9'>
                {input}\n
                <div class='file-preview'>
                  <strong> Current File </strong> : 
                    <div class='clearfix'></div>
                     <a base href='$documentpath'target='_blank'> $model->document</a>
                
                {hint}\n
                {error}
          </div></div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php endif; ?>
  
                </div>
               
</div>
<br>        
          
     <div class="row">
                <div class="form-group">
                    <label class="col-md-6 col-xs-12 control-label" for=""></label>
                    <div class="col-md-6 col-xs-12">
                        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                        <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>   
                 </div>

                 <?= $this->registerJs("
    
    $('#employee-date_of_birth').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    

    $('#employee-hire_date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    

    $('#employee-visa_expirydate').datetimepicker({
        format: 'DD-MM-YYYY'
    });
    
", View::POS_READY); ?>

<script >
$(function () {
    $('.username').blur(function () {
        var username = $(this).val();
        $.ajax({
            url: "./get-username-check",
            data: {'username': username},
            type: "post",
            success: function (data) {
                $('#username').val(data);
                if (data != '') {
                    $('.error-msg2').removeClass("hide");
                    $('.error-msg2').html("Username Already Exists");
                }
                else{
                    $('.error-msg2').addClass("hide");
                }
            }
        });
    });
});

$(function () {
    $('.email').blur(function () {
        var email = $(this).val();
        $.ajax({
            url: "./get-email-check",
            data: {'email': email},
            type: "post",
            success: function (data) {
                $('#email').val(data);
                if (data != '') {
                    $('.error-msg3').removeClass("hide");
                    $('.error-msg3').html("Email Already Exists");
                }
                else{
                    $('.error-msg3').addClass("hide");
                }
            }
        });
    });
});


$(function () {
    $('.employee_id').blur(function () {
        var employee_id = $(this).val();
        $.ajax({
            url: "./employee-id-check",
            data: {'employee_id': employee_id},
            type: "post",
            success: function (data) {
                $('#employee_id').val(data);
                if (data != '') {
                    $('.error-msg4').removeClass("hide");
                    $('.error-msg4').html("Employee Id  Already Exists");
                }
                else{
                    $('.error-msg4').addClass("hide");
                }
            }
        });
    });
});

</script>