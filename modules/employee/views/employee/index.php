

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use app\modules\project\models\Project;
use app\models\Permission;
$this->params['breadcrumbs'][] = 'Employee';

// $trade = ['1' => 'Painter', '2' => 'Manager','3' => 'Admin', '4' => 'Supervisor','5' => 'Driver', '6' => 'Gypsum Carpentor','7' => 'Mason', '8' => 'Helper','9' => 'Plumber', '10' => 'Furniture Carpentor','11' => 'Electrician', '12' => 'Operator'];
?>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-wrench" aria-hidden="true"></i> Employee
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
       <?= Html::button('Add', ['value' => Url::to('create'), 'class' => 'btn btn-success',$visibleList['create'], 'id' => 'addModalButton']) ?>
       
    </div>
</div>
<?php
Modal::begin([
    'header' => '<h4>Add Employee</h4>',
    'id' => 'addModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalAddContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Employee</h4>',
    'id' => 'updateModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalUpdateContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Change Password</h4>',
    'id' => 'passwordModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalUpdatePasswordContent'><div class='loader'></div></div>";
Modal::end();
?>

<?php
Modal::begin([
    'header' => '<h4>Assign Project</h4>',
    'id' => 'assignprojectModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalUpdateAssignProjectContent'><div class='loader'></div></div>";
Modal::end();
// pr($actioList);
?>

<?php
Modal::begin([
    'header' => '<h4>Assign Company</h4>',
    'id' => 'assigncompanyModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalUpdateAssignCompanyContent'><div class='loader'></div></div>";
Modal::end();
// pr($actioList);
?>

<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'first_name',
                'header' => '<div style="width:120px;">Employee Name</div>',
                'value' => function($model, $key, $index) {
                $employeename = $model->first_name . ' ' . $model->last_name;
                return Html::a($employeename, ['detailview', 'id' => $model->id]);
                   
                },
                'format' => 'raw',
            ],
           [
                'attribute' => 'employee_id',
                'header' => '<div style="width:90px;">Employee ID</div>',
                'value' => 'employee_id',
            ],
            [
                'attribute' => 'trade',
                'header' => '<div style="width:120px;">Designation</div>',
                'value' => function($model, $key, $index) {
                    $trade = ['1' => 'Painter', '2' => 'Manager','3' => 'Admin', '4' => 'Supervisor','5' => 'Driver', '6' => 'Gypsum Carpentor','7' => 'Mason', '8' => 'Helper','9' => 'Plumber', '10' => 'Furniture Carpentor','11' => 'Electrician', '12' => 'Operator'];
                    return !empty($trade[$model['trade']])?$trade[$model['trade']]:'-';;
                       
                    },
                
            ],
            [
                'attribute' => 'role',
                'header' => '<div style="width:120px;">Role</div>',
                'value' => function($model, $key, $index) {
                    $role = ArrayHelper::map(Permission::find()->where("type=1 AND (name!='admin' and name!='Facility Manager') ")->all(), 'name', 'name');
                    return !empty($role[$model['role']])?$role[$model['role']]:'-';;
                       
                    },
                
            ],
            [
                'attribute' => 'mobile_number',
                'header' => '<div style="width:120px;">Mobile Number</div>',
                'value' => 'mobile_number',
            ],
            // [
            //     'attribute' => 'Project Name',
            //     'header' => '<div style="width:120px;">Project Name</div>',
            //     'value' => function($model3, $key, $index) {
            //         $assign_project = ArrayHelper::map(Project::find()->select(['project_name'])->asArray()->all(),'project_name','project_name');
            //         return !empty($assign_project[$model3['project_name']])?$assign_project[$model3['project_name']]:'-';;
                       
            //         },
                
            // ],
            
            [
                'attribute' => 'hire_date',
                'header' => '<div style="width:180px;">Hire Date</div>',
              
                   'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->hire_date, 'php:d-m-Y');
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
            ]
                ])
            ],
            [
                'attribute' => 'visa_expirydate',
                'header' => '<div style="width:180px;">Visa Expiry</div>',
              
                   'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->visa_expirydate, 'php:d-m-Y');
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'dd-mm-yyyy',
            ]
                ])
            ],
            ['attribute' => 'Attachment',
            'contentOptions'=> ['class'=>'skip-export'],
            'headerOptions'=> ['class'=>'skip-export'],
            'header' => '<div style="width:80px;">Attachment</div>',
            'format' => 'raw',
            'value' => function($model) {
                return !empty($model->document) ?
                        Html::a('<div style="text-align:center; font-size:18px;"><span class="fa fa-download"></span></div>', ['employee/download', 'id' => $model->id]) : '';
            }
        ],
                   [
                
                'class' => '\kartik\grid\ActionColumn',
                'mergeHeader' => false,
                    'contentOptions' => ['style' => 'width:50px;text-align:center','class'=>'skip-export'],
                    'headerOptions' =>['class'=>'skip-export'],
                    'header'=>"Actions",
                'template'=>$actioList,
                'dropdown' => true,
                'dropdownOptions'=>['class'=>'pull-right' ],
                'options' => ['style' => 'max-width:20px;'],
                'buttons' => [
                   
                    'update' => function($url) {
                        return Html::a('Update',   $url,['class' => 'action-btn dropdown-item updateModalButton ', 'title' => 'Update']
                        );
                    },
                    'deletes' => function($url) {
                       return Html::a('Delete',   $url,['class' => 'action-btn dropdown-item','data-method'=>'post', 'data-pjax'=>0,'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?']]
                       );
                   },
                   'changepassword' => function($url) {
                    return Html::a('Change Password',   $url,['class' => 'action-btn dropdown-item  passwordModalButton', 'title' => 'Change Password']
                    );
                },
                'assigncompany' => function($url) {
                    return Html::a('Assign Company',   $url,['class' => 'action-btn dropdown-item  assigncompanyModalButton', 'title' => 'Assign Company']
                    );
                },
                'assignproject' => function($url) {
                    return Html::a('Assign Project',   $url,['class' => 'action-btn dropdown-item  assignprojectModalButton', 'title' => 'Assign Project']
                    );
                },
                'detailview' => function($url) {
                    return Html::a('Detail View',   $url,['class' => 'action-btn dropdown-item ', 'title' => 'Detail View']
                    );
                },
                   


                ]
                
                ],
            
        ],
        'containerOptions' => ['style' => 'overflow: auto'],
    ]);
}
?>                                   
<?= $this->registerJs("
$(function(){
$('#addModalButton').click(function() {
    $('#addModal').modal('show')
    .find('#modalAddContent')
    .load($(this).attr('value'));
});
});
", View::POS_READY); ?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updateModalButton').click(function(e){
        e.preventDefault();      
        $('#updateModal').modal('show')
        .find('#modalUpdateContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.passwordModalButton').click(function(e){
        e.preventDefault();      
        $('#passwordModal').modal('show')
        .find('#modalUpdatePasswordContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.assignprojectModalButton').click(function(e){
        e.preventDefault();      
        $('#assignprojectModal').modal('show')
        .find('#modalUpdateAssignProjectContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>
<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.assigncompanyModalButton').click(function(e){
        e.preventDefault();      
        $('#assigncompanyModal').modal('show')
        .find('#modalUpdateAssignCompanyContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>
<style>
a.action-btn.dropdown-item {
  display: block;
  width: 100%;
  padding: .25rem 1.5rem;
  clear: both;
  font-weight: normal;
  text-align: inherit;
  white-space: nowrap;
  background-color: transparent;
  border: 0;
  color: navy;
  font-size: small;
}
</style>