<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

use yii\helpers\Url;
use kartik\select2\Select2;

?>
<div class="employee-form form-vertical">

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <!-- <legend class="text-info"><small>Update</small></legend> -->
    

        <div class="col-sm-6">
            <label>Assign Project</label>
            <?php
            echo Select2::widget([
                'name' => 'projectlist',
                'data' => $projectList,
                'value'=>$userProjectList,
                'theme' => Select2::THEME_DEFAULT,
                'options' => ['placeholder' => 'Assign Projects ...', 'multiple' => true, 'autocomplete' => 'off'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>


    </div>
       <br>
            
          
     <div class="row">
                <div class="form-group">
                    <label class="col-md-6 col-xs-12 control-label" for=""></label>
                    <div class="col-md-6 col-xs-12">
                        <?= Html::submitButton($model3->isNewRecord ? 'Submit' : 'Submit', ['class' => $model3->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                        <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>   
                 </div>

     
