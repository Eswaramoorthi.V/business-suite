<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\date\DatePicker;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Permission;

?>
<div class="employee-form form-vertical">

<?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <!-- <legend class="text-info"><small>Update</small></legend> -->
    
        <div class="col-md-6">
        <?=
            $form->field($model, 'password_hash', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 password control-label', 'style' => 'color:black'],
            ])->passwordInput(['placeholder' => " New Password", 'value'=>''])->label('New Password')
            ?>
            </div>
             <div class="col-md-6">
              <?=
            $form->field($model, 'password_hash', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 password control-label', 'style' => 'color:black'],
            ])->passwordInput(['placeholder' => " Confirm Password", 'value'=>''])->label('Confirm Password')
            ?>       
      

              </div>
              
            </div>
       <br>
            
          
     <div class="row">
                <div class="form-group">
                    <label class="col-md-6 col-xs-12 control-label" for=""></label>
                    <div class="col-md-6 col-xs-12">
                        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Submit', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                        <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>   
                 </div>

     
