<?php


use yii\helpers\Html;

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
 

//$this->title = 'Project'.'     >     '. $model->project_name;
$this->params['breadcrumbs'][] =['label'=>'Employee List','url'=>['index']] ;
$firstName = $model->first_name;
$lastName = $model->last_name;
$this->params['breadcrumbs'][] = $firstName.' '.$lastName;
$gender = ['1' => 'Male', '2' => 'Female'];
$trade = ['1' => 'Painter', '2' => 'Manager','3' => 'Admin', '4' => 'Supervisor','5' => 'Driver', '6' => 'Gypsum Carpentor','7' => 'Mason', '8' => 'Helper','9' => 'Plumber', '10' => 'Furniture Carpentor','11' => 'Electrician', '12' => 'Operator'];
// $role = ArrayHelper::map(Permission::find()->where("type=1 AND (name!='admin' and name!='Facility Manager') ")->all(), 'name', 'name');
Modal::begin([
        'header' => '<h4>Update Employee</h4>',
        'id' => 'updateemply',
        'size' => 'modal-lg',
    ]);

    echo "<div id ='modalemplyupdate'><div class='loader'></div></div>";
    Modal::end();
?>

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-eye" aria-hidden="true"></i> Views | <small> <?php echo  $model['first_name'].' '.$model['last_name'];?></small>        </h1>
        <ul class="breadcrumb">

</ul>    </section>
</div>
<div class="nav-tab-custom">
                    <ul class="nav nav-tabs">
                        <li class="active" id="summarytab"><a href="#summary" data-toggle="tab">Employee Details</a></li>
                        </ul>
                       <div class="tab-content">
        <div class="tab-pane summary active" id="summary">

            <div class="box-body table-responsive">
                <p class="text-right" >
                            <?=
                            Html::a(Yii::t('app', ' {modelClass}', [
                                        'modelClass' => 'Edit',
                                    ]), ['employee/update', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalEmplyUpdate']);
                            ?>  </p>                           
<section class="content edusec-user-profile">
<div class="row">
    <div class="table-responsive">
	<table class="table">
		<colgroup>
			<col style="width:15%">
			<col style="width:35%">
			<col style="width:15%">
			<col style="width:35%">
		</colgroup>
		<tbody><tr>
			<th>Employee Name</th>
            <td><?php echo  $model['first_name'].' '.$model['last_name'];?></td>
            <th>User Name</th>
			
            <td><?php echo  $model['username'].' '.$model['username'];?></td>
		</tr>
                <tr>
			<th>Employee ID</th>
			<td><?php echo $model['employee_id'];?></td>
			<th>Trade</th>
			<td><?php echo  isset($trade[$model->trade])?$trade[$model->trade]:'-';?></td>
		</tr>
                <tr>
			<th>Date of Birth</th>
			<td><?php $rDate = explode(" ",$model['date_of_birth']);  echo $rDate[0];?></td>
			<th>Gender</th>
            <td><?php echo  isset($gender[$model->gender])?$gender[$model->gender]:'-';?></td>
		</tr>
                 <tr>
			<th>Mobile Number</th>
			<td><?php  echo $model['mobile_number'];?></td>
			<th>Phone Number</th>
			<td><?php  echo isset($model['phone_number'])?$model['phone_number']:'-';?></td>
		</tr>
                 <tr>
			<th>Basic Salary</th>
            <td><?php  echo isset($model['basic_salary'])?$model['basic_salary']:'-';?></td>
			<th>Allowance</th>
		
            <td><?php  echo isset($model['allowance'])?$model['allowance']:'-';?></td>
		</tr>
		<tr>
			<th>Hire Date</th>
			<td><?php $rDate = explode(" ",$model['hire_date']);  echo $rDate[0]; ?></td>
			<th>Visa Expiry</th>
			<td><?php $rDate = explode(" ",$model['visa_expirydate']);  echo $rDate[0]; ?></td>
		</tr>
        <tr>
			<th>Email </th>
			<td><?php  echo isset($model['email'])?$model['email']:'-';?></td>
			
		</tr>
		
		
	</tbody></table>
</div>
    </section>               
</div>
  </div>
                          
             
              </div>

<?php
$this->registerJs("$(function() {
   $('#modalEmplyUpdate').click(function(e) {
     e.preventDefault();
     $('#updateemply').modal('show').find('#modalemplyupdate')
     .load($(this).attr('href'));
   });
});");
?>
<?php
$js = <<< JS
$(document).ready(function(){
    
   var url_parts = location.href.split('#');
        
        
    var last_segment = url_parts[url_parts.length-1];
    if(url_parts.length==2) {
        try {
            $('.nav-tabs #summarytab').removeClass('active');
            $('.tab-content #summary').removeClass('active');
            $('.tab-content #'+last_segment).addClass('active');
            $('.nav-tabs #'+last_segment+'tab').addClass('active');
         } catch (e) {
            $('.nav-tabs #summarytab').addClass('active');
         }   
    }
            
});
JS;
$this->registerJs($js);
?>    