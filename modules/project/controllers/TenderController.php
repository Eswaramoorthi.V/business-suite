<?php

namespace app\modules\tender\controllers;

use app\modules\employee\models\EmployeeProject;
use Yii;
use app\modules\tender\models\Tender;
use app\modules\project\models\Project;
use app\modules\tender\models\TenderSearch;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\mpdf\Pdf;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\modules\tender\controllers\BaseController;
use app\modules\estimate\models\Settings;
use app\models\SettingsTree;
use app\models\EstimateDetails;
use app\models\EstimateMaster;
use app\models\MaterialMaster;
use app\models\MaterialUnitCost;
use app\models\LabourMaster;
use app\models\LabourUnitCost;
use app\modules\estimate\models\Equipment;
use app\modules\tender\models\TenderEstimateMaster;
use app\modules\tender\models\TenderEstimate;
use app\models\ProjectEstimateMaster;
use app\models\ProjectEstimate;
use app\models\Tree;
use app\modules\tender\models\TenderResourceMaster;
use app\models\Company;
use app\modules\customer\models\Customer;
use app\components\MenuHelper;
use app\models\ProjectResource;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class TenderController extends BaseController
{
    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->post()) {
            $searchModel = new TenderSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->post());

        } else {
            $searchModel = new TenderSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $dataProvider->pagination = ['PageSize' => 10];
        }
        $route = $this->route;
        $actionList = MenuHelper::getActionHelper($route);
        // pr($actionList);
        $visibleList = MenuHelper::getActionVisibleHelper($route);
        //  pr($visibleList);
        $list = \app\models\SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 178])
            ->orderBy('root, lft')->asArray()->all();

        $tenderTypeData = [];
        $parent = "";
        foreach ($list as $d) {
            // pr($d);
            if ($d['lvl'] == 1) {
                $tenderTypeData[$d['id']] = $d;
                $parent = $d['id'];
            } else {
                $tenderTypeData[$parent]['sublevel'][] = $d;
            }
        }
        $buildingTypeList = \app\models\SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 187])
            ->orderBy('root, lft')->asArray()->all();

        $buildingTypeData = [];
        $parent = "";
        foreach ($buildingTypeList as $d) {
            // pr($d);
            if ($d['lvl'] == 1) {
                $buildingTypeData[$d['id']] = $d;
                $parent = $d['id'];
            } else {
                $buildingTypeData[$parent]['sublevel'][] = $d;
            }
        }

        //$searchurl=\Yii::app()->createUrl('/project/project/index', []);
        $searchurl = Yii::$app->urlManager->createUrl(['/tender/tender/index']);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'actionList' => $actionList,
            'visibleList' => $visibleList,
            'buildingTypeData' => $buildingTypeData,
            'tenderTypeData' => $tenderTypeData,
            'searchurl' => $searchurl

        ]);


    }

    /**
     * Displays a single Project model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Project model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tender();

        if ($model->load(Yii::$app->request->post())) {
            //pr($_REQUEST);
            $request = Yii::$app->request->post();
            $model->tender_due_date = date('Y-m-d', strtotime($model['tender_due_date']));
            // pr(Yii::$app->request->post());
            $model->tender_code = $request['Tender']['tender_code'];
            $model->tender_name = $request['Tender']['tender_name'];
            $model->client_name = $request['Tender']['client_name'];
            $model->tender_type = $request['Tender']['tender_type'];
            $model->tender_status = $request['Tender']['tender_status'];
            $model->building_type = $request['Tender']['building_type'];
            $model->consultant = $request['Tender']['consultant'];
            $model->total_area = $request['Tender']['total_area'];
            $model->reference = $request['Tender']['reference'];
            // $model->tender_account_type = $request['Tender']['tender_account_type'];
            $model->country = $request['Tender']['country'];
            $model->amount = $request['Tender']['amount'];
            $model->company_id = $request['Tender']['company_id'];
            $model->save();
            if (!$model->save()) {
                pr($model->getErrors());
            }
            // $tenderId = $model->id;
            // $userId = Yii::$app->user->id;
            // $updateProject = new EmployeeProject();
            // $updateProject->user_id = $userId;
            // $updateProject->project_id = $projectId;
            // $updateProject->created_at = time();
            // $updateProject->updated_at = time();
            // if (!$updateProject->save()) {
            //     pr($updateProject->getErrors());
            // }
            return $this->redirect(['index']);
        } else {
            $list = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 178])
                ->orderBy('root, lft')->asArray()->all();

            $dataformatChange = [];
            $parent = "";
            foreach ($list as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $dataformatChange[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $dataformatChange[$parent]['sublevel'][] = $d;
                }
            }

            $tenderTypeValue = ArrayHelper::map($dataformatChange, 'id', 'name');
            $list = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 181])
                ->orderBy('root, lft')->asArray()->all();

            $dataformatChangeStatus = [];
            $parent = "";
            foreach ($list as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $dataformatChangeStatus[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $dataformatChangeStatus[$parent]['sublevel'][] = $d;
                }
            }
            $tenderStatusValue = ArrayHelper::map($dataformatChangeStatus, 'id', 'name');
            $buildingTypeList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 187])
                ->orderBy('root, lft')->asArray()->all();

            $buildingType = [];
            $parent = "";
            foreach ($buildingTypeList as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $buildingType[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $buildingType[$parent]['sublevel'][] = $d;
                }
            }

            $buildingTypeValue = ArrayHelper::map($buildingType, 'id', 'name');
            $accountTypeList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 211])
                ->orderBy('root, lft')->asArray()->all();

            $accountType = [];
            $parent = "";
            foreach ($accountTypeList as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $accountType[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $accountType[$parent]['sublevel'][] = $d;
                }
            }

            $accountTypeValue = ArrayHelper::map($accountType, 'id', 'name');

            $typeOneData = Customer::find()->where(['type' => 1])->asArray()->all();
            $typeTwoData = Customer::find()->where(['type' => 2])->asArray()->all();
            $assign_clientData = array_merge($typeOneData, $typeTwoData);

            $assign_ClientData = [];

            foreach ($assign_clientData as $value) {
                if ($value['company_name'] != '') {
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['customer_fname'] = $value['customer_fname'] . ' ' . $value['customer_lname'] . ' (' . $value['company_name'] . ')';
                    $assign_ClientData[] = $eType;
                } else {
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['customer_fname'] = $value['customer_fname'] . ' ' . $value['customer_lname'];
                    $assign_ClientData[] = $eType;
                }

            }
            $assign_client = ArrayHelper::map($assign_ClientData, 'id', 'customer_fname');
            $companyListData =Company::find()->select(['company_name','id'])->asArray()->all();

            $assign_company= [];
            
            foreach ($companyListData as $value) {
                $eType =[];
                $eType['id'] = $value['id'];
                $eType['company_name'] = $value['company_name'];
                $assign_company[]=$eType;
            }
            $companyList = ArrayHelper::map($assign_company, 'id', 'company_name');
            return $this->renderAjax('create', [
                'model' => $model,
                'tenderTypeValue' => $tenderTypeValue,
                'tenderStatusValue' => $tenderStatusValue,
                'assign_client' => $assign_client,
                'buildingTypeValue' => $buildingTypeValue,
                'companyList'=>$companyList,
                'accountTypeValue'=>$accountTypeValue
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();
            // pr($request);
            $tenderData = $request['Tender'];
            $model->tender_code = $request['Tender']['tender_code'];
            $model->tender_name = $request['Tender']['tender_name'];
            $model->client_name = $request['Tender']['client_name'];
            $model->tender_type = $request['Tender']['tender_type'];
            $model->tender_status = $request['Tender']['tender_status'];
            $model->building_type = $request['Tender']['building_type'];
            $model->consultant = $request['Tender']['consultant'];
            $model->total_area = $request['Tender']['total_area'];
            $model->reference = $request['Tender']['reference'];
            // $model->tender_due_date= $request['Project']['tender_due_date'];
            $model->tender_due_date = date('Y-m-d', strtotime($tenderData['tender_due_date']));
            // $model->tender_account_type = $request['Tender']['tender_account_type'];
            $model->country = $request['Tender']['country'];
            $model->amount = $request['Tender']['amount'];
            $model->company_id = $request['Tender']['company_id'];

            $model->save();
            if (!$model->save()) {
                pr($model->getErrors());
            }
            return $this->redirect(['index']);
        } else {
            $list = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 178])
                ->orderBy('root, lft')->asArray()->all();

            $dataformatChange = [];
            $parent = "";
            foreach ($list as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $dataformatChange[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $dataformatChange[$parent]['sublevel'][] = $d;
                }
            }

            $tenderTypeValue = ArrayHelper::map($dataformatChange, 'id', 'name');

            $tenderData = Tender::find()->where(['id' => $id])->one();

            $listStatus = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 181])
                ->orderBy('root, lft')->asArray()->all();

            $dataformatChangeStatus = [];
            $parent = "";
            foreach ($listStatus as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $dataformatChangeStatus[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $dataformatChangeStatus[$parent]['sublevel'][] = $d;
                }
            }

            $tenderStatusValue = ArrayHelper::map($dataformatChangeStatus, 'id', 'name');
            $buildingTypeList = \app\models\SettingsTree::find()
                ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root' => 187])
                ->orderBy('root, lft')->asArray()->all();

            $buildingType = [];
            $parent = "";
            foreach ($buildingTypeList as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $buildingType[$d['id']] = $d;
                    $parent = $d['id'];
                } else {
                    $buildingType[$parent]['sublevel'][] = $d;
                }
            }

            $buildingTypeValue = ArrayHelper::map($buildingType, 'id', 'name');
            $typeOneData = Customer::find()->where(['type' => 1])->asArray()->all();
            $typeTwoData = Customer::find()->where(['type' => 2])->asArray()->all();
            $assign_clientData = array_merge($typeOneData, $typeTwoData);

            $assign_ClientData = [];

            foreach ($assign_clientData as $value) {
                if ($value['company_name'] != '') {
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['customer_fname'] = $value['customer_fname'] . ' ' . $value['customer_lname'] . ' (' . $value['company_name'] . ')';
                    $assign_ClientData[] = $eType;
                } else {
                    $eType = [];
                    $eType['id'] = $value['id'];
                    $eType['customer_fname'] = $value['customer_fname'] . ' ' . $value['customer_lname'];
                    $assign_ClientData[] = $eType;
                }

            }

            $assign_client = ArrayHelper::map($assign_ClientData, 'id', 'customer_fname');
            $companyListData =Company::find()->select(['company_name','id'])->asArray()->all();

            $assign_company= [];
            
            foreach ($companyListData as $value) {
                $eType =[];
                $eType['id'] = $value['id'];
                $eType['company_name'] = $value['company_name'];
                $assign_company[]=$eType;
            }
            $companyList = ArrayHelper::map($assign_company, 'id', 'company_name');
            $accountTypeList = \app\models\SettingsTree::find()
            ->select(['id', 'root', 'lft', 'rgt', 'lvl', 'name', 'code', 'description'])
            ->where(['!=', 'lvl', 0])->andWhere(['root' => 211])
            ->orderBy('root, lft')->asArray()->all();

        $accountType = [];
        $parent = "";
        foreach ($accountTypeList as $d) {
            // pr($d);
            if ($d['lvl'] == 1) {
                $accountType[$d['id']] = $d;
                $parent = $d['id'];
            } else {
                $accountType[$parent]['sublevel'][] = $d;
            }
        }

        $accountTypeValue = ArrayHelper::map($accountType, 'id', 'name');

            return $this->renderAjax('update', [
                'model' => $model,
                'tenderTypeValue' => $tenderTypeValue,
                'tenderData' => $tenderData,
                'tenderStatusValue' => $tenderStatusValue,
                'assign_client' => $assign_client,
                'buildingTypeValue' => $buildingTypeValue,
                 'companyList'=>$companyList,
                 'accountTypeValue'=>$accountTypeValue
            ]);
        }
    }

    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionDeletes($id)
    {

        $this->findModel($id)->delete();
        TenderEstimateMaster::deleteAll(['tender_id' => $id]);
        TenderEstimate::deleteAll(['tender_id' => $id]);

        return $this->redirect(['index']);
    }

    public function actionConvertProject($id)
    {
        $model = $this->findModel($id);
        $tenderData = Tender::find()->where(['id' => $id])->asArray()->all();
        // pr($tenderData);
        $tenderMasterData = TenderEstimateMaster::find()->where(['tender_id' => $id])->asArray()->all();
    //   pr($tenderMasterData);
        $estimatedata = TenderEstimate::find()->select(['resource_type', 'resource_unit_cost', 'resource_total_cost'])->where(['tender_id' => $id])->asArray()->all();
        // pr($estimatedata);

        $getLastRecord=Project::find()->orderBy(['id' => SORT_DESC])->asArray()->one();


if(!empty($getLastRecord)){
    $autoId=$getLastRecord['id']+1;
}else{
    $autoId=1;
}

        foreach ($tenderData as $valueData) {
            // pr($valueData);
            $project = new Project();
            $project->project_name = $valueData['tender_name'];
            $project->project_code = 'P00' . $autoId;
            $project->project_type = $valueData['tender_type'];
            $project->amount = $valueData['amount'];
            // $project->description = $valueData['description'];
            $project->client_name = $valueData['client_name'];
            $project->country = $valueData['country'];
            $project->tender_due_date = $valueData['tender_due_date'];
            $project->building_type = $valueData['building_type'];
            $project->consultant = $valueData['consultant'];
            $project->total_area = $valueData['total_area'];
            $project->reference = $valueData['reference'];
            // $project->property_account_type = $valueData['tender_account_type'];
            $project->project_status = $valueData['tender_status'];
            if (!$project->save()) {
                pr($project->getErrors());
            }
        }
        $projectId = $project->id;
        // pr($projectId);
        foreach ($tenderMasterData as $valueData1) {
            // pr($valueData1);
            // die();
            $projectEstimateMaster = new ProjectEstimateMaster();
            $projectEstimateMaster->project_id = $projectId;
            $projectEstimateMaster->activity_id = $valueData1['activity_id'];
            $projectEstimateMaster->sub_activity_id = $valueData1['sub_activity_id'];
            $projectEstimateMaster->description = $valueData1['description'];
            $projectEstimateMaster->unit_id = $valueData1['unit_id'];
            $projectEstimateMaster->quantity = $valueData1['quantity'];
            $projectEstimateMaster->unit_rate = $valueData1['unit_rate'];
            $projectEstimateMaster->total_cost = $valueData1['total_cost'];
            // pr($projectEstimateMaster);
            if (!$projectEstimateMaster->save()) {
                pr($projectEstimateMaster->getErrors());
            }
            $projectEstimateMasterId = $projectEstimateMaster->id;

            $estimateDetails = TenderEstimate::find()
                ->where(['tender_id' => $id])
                ->andWhere(['tender_master_id' => $valueData1['id']])->asArray()->all();
            $estimateDetailsResult = ArrayHelper::index($estimateDetails, 'resource_type');


            foreach ($estimateDetails as $valueData3) {

                $projectEstimate = new ProjectEstimate();
                $projectEstimate->project_id = $projectId;
                $projectEstimate->project_master_id = $projectEstimateMasterId;
                $projectEstimate->resource_type = $valueData3['resource_type'];
                $projectEstimate->resource_unit_cost = $valueData3['resource_unit_cost'];
                $projectEstimate->resource_total_cost = $valueData3['resource_total_cost'];

                if (!$projectEstimate->save()) {
                    pr($projectEstimate->getErrors());
                }
            }
        }

        ProjectResource::deleteAll(['project_id' => $projectId]);
// pr($id);
        $tenderDataMaster = TenderEstimateMaster::find()->where(['tender_id' => $id])->asArray()->all();
// pr($tenderDataMaster);
        foreach ($tenderDataMaster as $tenderDataInfo){

            // pr($projectInfo);
            $activityId=$tenderDataInfo['activity_id'];
            $subActivityId=$tenderDataInfo['sub_activity_id'];
            $quantity = $tenderDataInfo['quantity'];
            $getestimateDetails = EstimateMaster::find()->with('estimateDetails')->where(['activity_id' => $activityId,'sub_activity_id'=>$subActivityId])->asArray()->one();
            $estimateDetails=isset($getestimateDetails['estimateDetails'])?$getestimateDetails['estimateDetails']:[];
           $count=sizeof($estimateDetails);

        
            if($count>0){
                // pr($estimateDetails);
                foreach ($estimateDetails as $estimateData) {
                    // pr($estimateData);
                  
                    $resourcetypeid = $estimateData['resource_type'];
                    $resourceId =  $estimateData['resource_id'];
                    // pr($resourceId);
                    $projectResource = ProjectResource::find()->where(['project_id' => $projectId, 'material_id' => $resourceId])->one();
                    // pr($projectResource);
                 
                    if (empty($projectResource)) {
                        // pr("fxgbd");
                        $projectResource = new ProjectResource();
                        $quantity1=$quantity;
                    } else {
                        // pr("vbghfgh");

                        $quantity1 = $projectResource->total_quantity + $quantity;
                    }
                    $projectResource->project_id = $projectId;
                    $projectResource->total_quantity = $quantity1;
                    $projectResource->material_id = $resourceId;
                    $projectResource->resource_type = $resourcetypeid;
                    // $projectResource->save();
                    // pr($projectResource);
                    
                    if (!$projectResource->save()) {
                        pr($projectResource->getErrors());
                    }

                }
            }
        }

        $projectId = $projectId;
        $userId = Yii::$app->user->id;
        $updateProject = new EmployeeProject();
        $updateProject->user_id = $userId;
        $updateProject->project_id = $projectId;
        $updateProject->created_at = time();
        $updateProject->updated_at = time();
        if (!$updateProject->save()) {
            pr($updateProject->getErrors());
        }

        return $this->redirect(['/project/project/index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tender::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    

    public function actionMaster($id)
    {

        $queryDataList = TenderEstimateMaster::find()->where(['tender_id' => $id])->asArray()->all();
        $tender = Tender::find()->where(['id' => $id])->asArray()->one();
        $tenderName = $tender['tender_name'];
        $tenderCode = isset($tender['tender_code']) ? $tender['tender_code'] : '';
        $arrayData = [];
        $totalCost = 0;
        $materialCostData = 0;
        $labourCostData = 0;
        $equipmentCostData = 0;
        $profitCostData = 0;
        $overheadCostData = 0;
        if ($queryDataList != '') {
            foreach ($queryDataList as $KeyData => $value1) {
                $formula = ($KeyData + 1);
                // $formula= ($KeyData);
                // pr($formula);
                //        die();
                $estimateDetails = TenderEstimate::find()
                    ->where(['tender_id' => $id])
                    ->andWhere(['tender_master_id' => $value1['id']])->asArray()->all();
                $estimateDetailsResult = ArrayHelper::index($estimateDetails, 'resource_type');

                $materialCost = isset($estimateDetailsResult['1']) ? (isset($estimateDetailsResult['1']['resource_unit_cost']) ? $estimateDetailsResult['1']['resource_unit_cost'] : 0) : 0;
                $materialTotalCost = isset($estimateDetailsResult['1']) ? (isset($estimateDetailsResult['1']['resource_total_cost']) ? $estimateDetailsResult['1']['resource_total_cost'] : 0) : 0;
                $labourCost = isset($estimateDetailsResult['2']) ? (isset($estimateDetailsResult['2']['resource_unit_cost']) ? $estimateDetailsResult['2']['resource_unit_cost'] : 0) : 0;
                $labourTotalCost = isset($estimateDetailsResult['2']) ? (isset($estimateDetailsResult['2']['resource_total_cost']) ? $estimateDetailsResult['2']['resource_total_cost'] : 0) : 0;
                $equipCost = isset($estimateDetailsResult['3']) ? (isset($estimateDetailsResult['3']['resource_unit_cost']) ? $estimateDetailsResult['3']['resource_unit_cost'] : 0) : 0;
                $ohCost = isset($estimateDetailsResult['4']) ? (isset($estimateDetailsResult['4']['resource_unit_cost']) ? $estimateDetailsResult['4']['resource_unit_cost'] : 0) : 0;
                $profitCost = isset($estimateDetailsResult['5']) ? (isset($estimateDetailsResult['5']['resource_unit_cost']) ? $estimateDetailsResult['5']['resource_unit_cost'] : 0) : 0;
                $resDataList = [];


                $resDataList[0] = $value1['activity_id'];
                $resDataList[1] = $value1['sub_activity_id'];
                $resDataList[2] = $value1['description'];
                $resDataList[3] = $value1['quantity'];
                $resDataList[4] = $value1['unit_id'];
                $resDataList[7] = isset($materialCost) ? ($materialCost) : 0;
                $resDataList[8] = '=ROUND((H' . ($formula) . '*D' . $formula . '),2)';
                $resDataList[9] = isset($labourCost) ? ($labourCost) : 0;
                $resDataList[10] = '=ROUND((J' . ($formula) . '*D' . $formula . '),2)';
                $resDataList[11] = isset($equipCost) ? ($equipCost) : 0;
                $resDataList[12] = '=ROUND((I' . ($formula) . '+K' . $formula . '+L' . $formula . '),2)';
                $resDataList[13] = isset($ohCost) ? ($ohCost) : 0;
                $resDataList[14] = ('=ROUND(((M' . $formula . '*N' . $formula . ')/100),2)');
                $resDataList[15] = '=ROUND(((M' . $formula . '+O' . $formula . ')),2)';
                $resDataList[16] = isset($profitCost) ? ($profitCost) : 0;
                $resDataList[17] = '=ROUND(((P' . $formula . '*Q' . $formula . ')/100),2)';
                $resDataList[18] = '=ROUND((P' . ($formula) . '+R' . $formula . '),2)';
                $resDataList[19] = '=ROUND((S' . ($formula) . '/D' . $formula . '),2)';
                $resDataList[5] = '=ROUND((S' . ($formula) . '/D' . $formula . '),2)';
                $resDataList[6] = '=ROUND((P' . ($formula) . '+R' . $formula . '),2)';
                $arrayData[] = $resDataList;
                $materialCostData += ($materialCost * $value1['quantity']);
                $labourCostData += ($labourCost * $value1['quantity']);

                $equipmentCostData += isset($equipCost) ? ($equipCost) : 0;
                $profitCostData += ($resDataList[15] * $resDataList[16]);
                $overheadCostData += ($resDataList[12]) * ($ohCost);
                $totalCost += $resDataList[18];
            }
        }
        $list = empty($arrayData) ? [[]] : $arrayData;
        $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
        $unitTypeList = ArrayHelper::map($getUnitTypeList, 'id', 'name');
        $unitTypeListData = array_values($unitTypeList);
        
        $getActivityData = EstimateMaster::find()->groupBy(['activity_id'])->asArray()->all();
        
        $getSubActivityData = EstimateMaster::find()->groupBy(['sub_activity_id'])->asArray()->all();

        $activity_list = [];
                foreach ($getActivityData as $value) {
              
                        $cat = [];
                        $cat['id'] = $value['activity_id'];
                    $getActivityName =SettingsTree::find()->where(['id'=>$value['activity_id']])->asArray()->all();
                   foreach ($getActivityName as $value1) {
                    //    pr($value1);
                        $cat['name'] = $value1['name'];
                    }
                       $activity_list[] = $cat;
                     }
        
       
        $subActivityListData = [];
        $subActivityList=[];
       
        foreach($getSubActivityData as $value ){
            $getActivityName =SettingsTree::find()->where(['id'=>$value['sub_activity_id']])->asArray()->all();
            $cat=[];
            $cat['id']=$value['sub_activity_id'];
            foreach($getActivityName as $value1){
                $cat['name']=$value1['name'];
            }
          
            $subActivityListData[]=$cat;
        }
      
        $sub_activity_list=[];
        foreach ($getSubActivityData as $key => $valueData) {
           $cat = [];
           $cat['id'] = $valueData['sub_activity_id'];
           $getActivityName =SettingsTree::find()->where(['id'=>$valueData['sub_activity_id']])->asArray()->all();
      
           foreach ($getActivityName as $value) {
                        $cat['name'] = $value['name'];
                           $sub_activity_list[$valueData['activity_id']][]=$cat;
                               }
   }

     $route = $this->route;
        $visibleList = MenuHelper::getActionVisibleHelper($route);
        return $this->render('master', ['arrayData' => $list,
            'overheadCost' => $overheadCostData, 'profitCost' => $profitCostData, 'equipmentCost' => $equipmentCostData,
            'labourCost' => $labourCostData, 'materialCost' => $materialCostData, 'totalCost' => $totalCost,
            'unitTypeListData' => $unitTypeListData, 'subActivityListData' => $subActivityListData, 'sub_activity_list' => $sub_activity_list, 'category' => $activity_list,
            'tenderName' => $tenderName, 'tenderCode' => $tenderCode, 'visibleList' => $visibleList
        ]);
    }

    public function actionGetActivityList()
    {
        $activity_id = $_REQUEST['activity_id'];
        // print_r($activity_id);
        $activity_list = EstimateMaster::find()->where(['activity_id' => $activity_id])->asArray()->one();
        //    pr($activity_list);
        echo $activity_list['activity_id'];
        // echo 'activity_list';
        // echo 177;
    }

    public function actionGetEstimateData()
    {
        $subactivityId = $_REQUEST['sub_activity_id'];
        $activity_id = $_REQUEST['activity_id'];
        // print_r($activity_id);
        $activity_list = EstimateMaster::find()->where(['activity_id' => $activity_id, 'sub_activity_id' => $subactivityId])->asArray()->one();
        echo $activity_list['quantity'];
    }

    public function actionGetTotalQuantity()
    {
        $resource_id = $_REQUEST['material_id'];
        $tender_id = $_REQUEST['tender_id'];
       
        $totalQuantityList = ProjectResource::find()->where(['material_id' => $resource_id, 'tender_id' => $project_id])->asArray()->one();
    
        echo $totalQuantityList['resource_quantity'];
    }

    public function actionGetDescription()
    {
        $subActivityId = $_REQUEST['sub_activity'];
        $settingList = SettingsTree::find()->where(['id' => $subActivityId])->asArray()->one();
        echo $settingList['description'];
    }

    public function actionGetMaterialData()
    {

        $activityId = $_REQUEST['activity_id'];
        $subActivityId = $_REQUEST['sub_activity_id'];
        $activity_list_unit = EstimateMaster::find()->where(['activity_id' => $activityId, 'sub_activity_id' => $subActivityId])->asArray()->one();
        $estimateDetails = EstimateDetails::find()->select(['estmate_id', 'resource_type', 'actual_cost'])->where(['estmate_id' => $activity_list_unit['id']])->asArray()->all();
        $resource = ArrayHelper::getcolumn($estimateDetails, 'resource_type');

        $dataList = [];
        // pr($estimateDetails);
        // die();
        foreach ($estimateDetails as $value) {
            if (!empty($dataList) && !empty($dataList[$value['resource_type']])) {
                $dataList[$value['resource_type']] = $dataList[$value['resource_type']] + $value['actual_cost'];
            } else {
                $dataList[$value['resource_type']] = $value['actual_cost'];
            }
        }
        // pr($dataList);
        // die();

        // PR($dataList);

        if (empty($dataList)) {
            $resourceMaster = Tree::find()->select('name')->where(['lvl' => 0])->asArray()->all();
            foreach ($resourceMaster as $d) {
                $dataList[$d['name']] = 0;
            }
        }


        echo json_encode(array_values($dataList));
        // exit;
    }

    public function actionCreateEstimate()
    {
        $estimateType = Yii::$app->params['estimateType'];
        if (Yii::$app->request->post()) {
            $request = Yii::$app->request->post();

            $tenderId = $request['tender_id'];
            $formdata = $request['formdata'];
            TenderEstimateMaster::deleteAll(['tender_id' => $tenderId]);
            TenderEstimate::deleteAll(['tender_id' => $tenderId]);
            foreach ($formdata as $valueData) {

                //    pr($valueData);

                if (!empty($valueData[5] && $valueData[6])) {

                    $tenderEstimateMaster = new TenderEstimateMaster();
                    $tenderEstimateMaster->tender_id = $tenderId;
                    $tenderEstimateMaster->activity_id = $valueData[0];
                    $tenderEstimateMaster->sub_activity_id = $valueData[1];
                    $tenderEstimateMaster->description = $valueData[2];
                    $tenderEstimateMaster->quantity = $valueData[3];
                    $tenderEstimateMaster->unit_id = $valueData[4];

                    $quantityValue = $valueData[3];
                    $totalmatLabEquValue = ($valueData[3] * $valueData[7]) + ($valueData[3] * ($valueData[9]) + ($valueData[11]));
                    $overHeadAmount = ($totalmatLabEquValue * $valueData[13]) / 100;
                    $netCostAmount = ($totalmatLabEquValue) + ($overHeadAmount);
                    $profitAmount = ($netCostAmount * $valueData[16]) / 100;
                    $totalAmountData = ($netCostAmount + $profitAmount);
                    $unitRateAmount = ($totalAmountData / $quantityValue);

                    $tenderEstimateMaster->total_cost = $totalAmountData;
                    $tenderEstimateMaster->unit_rate = $unitRateAmount;

                    //    pr( $projectEstimateMaster);
                    if ($tenderEstimateMaster->save()) {
                        $tenderEstimateMasterId = $tenderEstimateMaster->id;
                        $arryDataVal['1'] = [0 => $valueData[7], 1 => $valueData[8], 2 => 1, 3 => $valueData[3]];
                        $arryDataVal['2'] = [0 => $valueData[9], 1 => $valueData[10], 2 => 2, 3 => $valueData[3]];
                        $arryDataVal['3'] = [0 => $valueData[11], 1 => $valueData[11], 2 => 3, 3 => $valueData[3]];
                        $arryDataVal['4'] = [0 => $valueData[13], 1 => $valueData[14], 2 => 4, 3 => $valueData[3]];
                        $arryDataVal['5'] = [0 => $valueData[16], 1 => $valueData[17], 2 => 5, 3 => $valueData[3]];
                        $mergeArray = array_values($arryDataVal);
                        foreach ($mergeArray as $value12) {
                            // pr($value12);
                            $tenderEstimate = new TenderEstimate();
                            $tenderEstimate->tender_id = $tenderId;
                            $tenderEstimate->tender_master_id = $tenderEstimateMasterId;
                            $tenderEstimate->resource_type = $value12[2];
                            $tenderEstimate->resource_unit_cost = $value12[0];
                            $tenderEstimate->resource_total_cost = ($value12[0] * $value12[3]);
                            // pr($projectEstimate);
                            if (!$tenderEstimate->save()) {
                                pr($tenderEstimate->getErrors());
                            }
                        }

                    } else {
                        pr($tenderEstimateMaster->getErrors());
                    }
                }
            }
            TenderResourceMaster::deleteAll(['tender_id' => $tenderId]);

            $tenderMasterData = TenderEstimateMaster::find()->where(['tender_id' => $tenderId])->all();

            foreach ($tenderMasterData as $tenderInfo){

                //pr($projectInfo);
                $activityId=$tenderInfo['activity_id'];
                $subActivityId=$tenderInfo['sub_activity_id'];
                $quantity = $tenderInfo['quantity'];
                $getestimateDetails = EstimateMaster::find()->with('estimateDetails')->where(['activity_id' => $activityId,'sub_activity_id'=>$subActivityId])->asArray()->one();
                $estimateDetails=isset($getestimateDetails['estimateDetails'])?$getestimateDetails['estimateDetails']:[];
                if(!empty($estimateDetails)){
                    foreach ($estimateDetails as $estimateData) {
                        $resourcetypeid = $estimateData['resource_type'];
                        $resourceId =  $estimateData['resource_id'];
                        
                        $tenderResource = TenderResourceMaster::find()->where(['tender_id' => $tenderId, 'material_id' => $resourceId])->one();
                        if (empty($tenderResource)) {
                            $tenderResource = new TenderResourceMaster();
                            $quantity1=$quantity;
                        } else {

                            $quantity1 = $tenderResource->total_quantity + $quantity;
                        }
                        $tenderResource->tender_id = $tenderId;
                        $tenderResource->total_quantity = $quantity1;
                        $tenderResource->material_id = $resourceId;
                        $tenderResource->resource_type = $resourcetypeid;
                        if (!$tenderResource->save()) {
                            pr($tenderResource->getErrors());
                        }

                    }
                }
            }



           return $this->redirect(['master?id=' . $tenderId]);
        }
    }

    public function actionExportFullExcel($id)
    {
        $tender = Tender::find()->where(['id' => $id])->asArray()->one();
        $tenderName = $tender['tender_name'];
        $tenderName1 = substr($tenderName, 0, 30);
        $data = TenderEstimateMaster::find()->where(['tender_id' => $id])->asArray()->all();
        $estimatedata = TenderEstimate::find()->select(['resource_type', 'resource_unit_cost', 'resource_total_cost'])->where(['tender_id' => $id])->asArray()->all();
        $settingList = SettingsTree::find()->asArray()->all();
        $subActivityListData = ArrayHelper::map($settingList, 'id', 'description');
        $listData = ArrayHelper::map($settingList, 'id', 'name');
        $dataList = [];
        $totalAmount = 0;
        foreach ($data as $value) {
            $estimateDetails = TenderEstimate::find()
                ->where(['tender_id' => $id])
                ->andWhere(['tender_master_id' => $value['id']])->asArray()->all();


            $estimateDetailsResult = ArrayHelper::index($estimateDetails, 'resource_type');
            $dataVal = [];
            $activity = $listData[$value['activity_id']];
            if (!empty($value['total_cost'])) {
                $activity = $listData[$value['activity_id']];
                $subActivity = $subActivityListData[$value['sub_activity_id']];
                $dataVal['sub_activity_id'] = $subActivity;
                $dataVal['unit_type'] = $value['unit_id'];
                $dataVal['quantity'] = $value['quantity'];
                $dataVal['unit_rate'] = ($value['total_cost'] / $value['quantity']);
                $dataVal['total_cost'] = $value['total_cost'];
                $dataVal['material_rate'] = isset($estimateDetailsResult['1']) ? (isset($estimateDetailsResult['1']['resource_unit_cost']) ? $estimateDetailsResult['1']['resource_unit_cost'] : 0) : 0;
                $dataVal['material_amount'] = isset($estimateDetailsResult['1']) ? (isset($estimateDetailsResult['1']['resource_total_cost']) ? $estimateDetailsResult['1']['resource_total_cost'] : 0) : 0;
                $dataVal['labour_rate'] = isset($estimateDetailsResult['2']) ? (isset($estimateDetailsResult['2']['resource_unit_cost']) ? $estimateDetailsResult['2']['resource_unit_cost'] : 0) : 0;
                $dataVal['labour_amount'] = isset($estimateDetailsResult['2']) ? (isset($estimateDetailsResult['2']['resource_total_cost']) ? $estimateDetailsResult['2']['resource_total_cost'] : 0) : 0;
                $dataVal['equipment'] = isset($estimateDetailsResult['3']) ? (isset($estimateDetailsResult['3']['resource_unit_cost']) ? $estimateDetailsResult['3']['resource_unit_cost'] : 0) : 0;
                $dataVal['overhead_percent'] = isset($estimateDetailsResult['4']) ? (isset($estimateDetailsResult['4']['resource_unit_cost']) ? $estimateDetailsResult['4']['resource_unit_cost'] : 0) : 0;
                $dataVal['profit_percent'] = isset($estimateDetailsResult['5']) ? (isset($estimateDetailsResult['5']['resource_unit_cost']) ? $estimateDetailsResult['5']['resource_unit_cost'] : 0) : 0;

                $dataVal['mat_lap_equ'] = ($dataVal['material_rate'] * $value['quantity']) + ($dataVal['labour_rate'] * $value['quantity']) + $dataVal['equipment'];
                $dataVal['overhead_amount'] = ($dataVal['mat_lap_equ'] * $dataVal['overhead_percent']) / 100;

                $dataVal['netcost'] = ($dataVal['mat_lap_equ'] + $dataVal['overhead_amount']);
                $dataVal['profit_amount'] = ($dataVal['netcost'] * $dataVal['profit_percent']) / 100;
                $dataVal['total_amount'] = ($value['total_cost'] / $value['quantity']);
                $dataVal['unit_rate_amount'] = $value['total_cost'];
                $totalAmount += $value['total_cost'];
            }
            if (!empty($dataList) && !empty($dataList[$activity])) {
                $dataList[$activity] = array_merge($dataList[$activity], [array_values($dataVal)]);
            } else {
                $dataList[$activity] = [array_values($dataVal)];
            }
        }
        $listData = [];
        $listData[0] = ['Tender Name : ' . $tenderName, '', '', '', '', '', 'Material', '', 'Labour', '', 'Equipment', 'Mat +Lap+Equ', 'OverHead', '', 'Netcost', 'Profit', '', 'Selling Price', ''];
        $listData[1] = ['Item', 'Description', 'Unit', 'Quantity', 'Unit Rate', 'Amount', 'Rate', 'Total', 'Rate', 'Total', 'Cost', 'Cost', '%', 'Amount', '%', '%', ' Amount', 'Total', 'Unit Rate'];
        $listData[2] = ['', '', '', '', '(AED)', '(AED)'];
        $i = 65;
        $boldData = [];
        $j = 5;

        // die();
        foreach ($dataList as $key => $value1) {
            $boldData['B' . $j] = [
                'font' => [
                    'bold' => true,
                    'size' => 11,
                    'name' => 'Calibri',
                    'underline' => true,
                ],
            ];
            $listData[] = [chr($i), $key];
            // pr($listData);
            foreach ($value1 as $key1 => $value2) {
                // pr($key1+1);
                if (!empty($value2)) {
                    $listData[] = array_merge([$key1 + 1], $value2);

                }
            }
            $i++;
            $j += count($value1) + 1;
        }
        $styleData = ['A1:F4' => [
            'font' => [
                'bold' => true,
                //'color' => ['rgb' => 'FF0000'],
                'size' => 11,
                'name' => 'Calibri'
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ],
            'borders' => [
                'top' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                ],
            ],
//        'fill' => [
//            'type' => \PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
//            'rotation' => 180,
//            'startcolor' => [
//                'argb' => 'FFA0A0A0',
//            ],
//            'endcolor' => [
//                'argb' => 'FFFFFFFF',
//            ],
//        ],
        ]];
        $style = array_merge($styleData, $boldData);
        $totalArray = [[], ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'Total Amount', $totalAmount]];
        $arrayDataList = array_merge($listData, $totalArray);
        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                $tenderName1 => [// Name of the excel sheet
                    'data' => $arrayDataList,
                    'titles' => ['', '', 'BILLS OF QUANTITIES'],
                    'styles' => $style,
                ],
            ]
        ]);


        if ($file->send($tenderName . '.xlsx')) {
            return $this->redirect('index');
        }
    }


    public function actionExportPdf($id)
    {
        $tender = Tender::find()->where(['id' => $id])->asArray()->one();
        $tenderName = $tender['tender_name'];
        $companyID= $tender['company_id'];
       $companyData =Company::find()->where(['id'=>$companyID])->one();
       $logo =$companyData['logo'];
       $companyName =$companyData['company_name'];
// pr($companyName);
        $allClientData = Customer::find()->where(['type' => 1])->asArray()->all();

        $assign_ClientData = [];

        foreach ($allClientData as $value) {
            $eType = [];
            $eType['id'] = $value['id'];
            $eType['customer_fname'] = $value['customer_fname'] . ' ' . $value['customer_lname'];

            $assign_ClientData[] = $eType;
        }

        $assign_client = ArrayHelper::map($assign_ClientData, 'id', 'customer_fname');
        // pr($assign_client);
        $clientName = isset($assign_client) ? ($assign_client[$tender['client_name']]) : "";

        $data = TenderEstimateMaster::find()->where(['tender_id' => $id])->asArray()->all();
        $settingList = SettingsTree::find()->asArray()->all();
        $subActivityListData = ArrayHelper::map($settingList, 'id', 'description');
        $listData = ArrayHelper::map($settingList, 'id', 'name');

        $dataList = [];
        $totalAmount = 0;
        foreach ($data as $value) {
            $dataVal = [];
            $activity = $listData[$value['activity_id']];
            if (!empty($value['total_cost'])) {
                $activity = $listData[$value['activity_id']];
                $subActivity = $subActivityListData[$value['sub_activity_id']];
                $dataVal['sub_activity_id'] = $subActivity;
                $dataVal['unit_type'] = $value['unit_id'];
                $dataVal['quantity'] = $value['quantity'];
                $dataVal['unit_rate'] = ($value['total_cost'] / $value['quantity']);
                $dataVal['total_cost'] = $value['total_cost'];
                $totalAmount += $value['total_cost'];
            }
            if (!empty($dataList) && !empty($dataList[$activity])) {
                $dataList[$activity] = array_merge($dataList[$activity], [array_values($dataVal)]);
            } else {
                $dataList[$activity] = [array_values($dataVal)];
            }
            
        }
        // pr($totalAmount);
        $filename = $tender['tender_name'] . '.pdf';
        $homeurl = Yii::$app->params['domain_url'];
        $content = $this->renderPartial('pdf', [
            'homeurl' => $homeurl,
            'dataList' => $dataList,
            'totalAmount' => $totalAmount,
            'tenderName' => $tenderName,
            'clientName' => $clientName,
            'tender'=>$tender
        ]);
        $destination = Pdf::DEST_DOWNLOAD;

        // pr($content);
        $pdf = new Pdf([
            // 'mimeType' => 'application/pdf',
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/pdf.css',
            'defaultFont' => 'Lato',
            'marginTop' => 40,
            'cssInline' => ' table,.new_head, .borderbox {font-size:13px !important; font-family:"Lato", sans-serif;} .text-bold{  font-family:"Lato", sans-serif;}',
            'format' => Pdf::FORMAT_A4,
            'destination' => Pdf::DEST_DOWNLOAD,
            'filename' => $tender['tender_name'] . '.' . 'pdf',
            'options' => [
                'defaultheaderline' => 0,  //for header
            ],
       
        'methods' => [
            'SetHeader' => ("<div style='width:100%; text-align:center;'>
            <table class='table' style='margin-bottom: 0px'>
            <tr>
            <td width='50%'><img src='".$homeurl."/lib/img/company/".$companyName."/logo/".$logo."' width='110px'/></td>
            <td width='50%'><table class='table'><tr>
            <td align='right'>Tender Code : </td><td>$tender[tender_code]</td>
            </tr><tr>
            
            </tr></table></td>
            </tr>
            </table>
            <table class='table'>
              <tr>
                            <td class=\"text-center text-bold\" style=\"font-size: 20px;text-decoration:underline\" >
                                 Bills Of Quantities
                            </td>
                        </tr>
        </table>
        </div>"),
        'SetFooter' => ('
            <table> <tr>
        <td width="20%"  style="font-weight:bold; text-align:center"><span style=" color: white">&nbsp; Contenthide</span> P.O.Box - 123456, Tel: 04-123456, Fax: 04-12345, E-mail: info@jeyamsys.com</td>
        <td width="10%" style="text-align: right;font-weight:bold;">{PAGENO}</td>
        </tr>
        </table>')
        ],
    ]);
//  pr($pdf);
        return $pdf->render();

    }


    public function actionExportExcel($id)
    {
        $tender = Tender::find()->where(['id' => $id])->asArray()->one();
        $tenderName = $tender['tender_name'];
        $tenderName1 = substr($tenderName, 0, 30);
        $data = TenderEstimateMaster::find()->where(['tender_id' => $id])->asArray()->all();

        $settingList = SettingsTree::find()->asArray()->all();
        $subActivityListData = ArrayHelper::map($settingList, 'id', 'description');
        $listData = ArrayHelper::map($settingList, 'id', 'name');
        $dataList = [];
        $totalAmount = 0;
        foreach ($data as $value) {

            $dataVal = [];
            $activity = $listData[$value['activity_id']];
            if (!empty($value['total_cost'])) {
                $activity = $listData[$value['activity_id']];
                $subActivity = $subActivityListData[$value['sub_activity_id']];
                $dataVal['sub_activity_id'] = $subActivity;
                $dataVal['unit_type'] = $value['unit_id'];
                $dataVal['quantity'] = $value['quantity'];
                $dataVal['unit_rate'] = ($value['total_cost'] / $value['quantity']);
                $dataVal['total_cost'] = $value['total_cost'];

                $totalAmount += $value['total_cost'];
            }
            if (!empty($dataList) && !empty($dataList[$activity])) {
                $dataList[$activity] = array_merge($dataList[$activity], [array_values($dataVal)]);
            } else {
                $dataList[$activity] = [array_values($dataVal)];
            }
        }
        $listData = [];
        $listData[0] = ['Tender Name : ' . $tenderName];
        $listData[1] = ['Item', 'Description', 'Unit', 'Quantity', 'Unit Rate', 'Amount'];
        $listData[2] = ['', '', '', '', '(AED)', '(AED)'];
        $i = 65;
        $boldData = [];
        $j = 5;

        // die();
        foreach ($dataList as $key => $value1) {
            $boldData['B' . $j] = [
                'font' => [
                    'bold' => true,
                    'size' => 11,
                    'name' => 'Calibri',
                    'underline' => true,
                ],
            ];
            $listData[] = [chr($i), $key];
            // pr($listData);
            foreach ($value1 as $key1 => $value2) {
                // pr($key1+1);
                if (!empty($value2)) {
                    $listData[] = array_merge([$key1 + 1], $value2);

                }
            }
            $i++;
            $j += count($value1) + 1;
        }
        $styleData = ['A1:F4' => [
            'font' => [
                'bold' => true,
                'color' => ['rgb' => 'FF0000'],
                'size' => 11,
                'name' => 'Calibri'
            ],
            'alignment' => [
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ],
            'borders' => [
                'top' => [
                    'style' => \PHPExcel_Style_Border::BORDER_THIN,
                ],
            ],
//        'fill' => [
//            'type' => \PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
//            'rotation' => 180,
//            'startcolor' => [
//                'argb' => 'FFA0A0A0',
//            ],
//            'endcolor' => [
//                'argb' => 'FFFFFFFF',
//            ],
//        ],
        ]];
        $style = array_merge($styleData, $boldData);
        $totalArray = [[], ['', '', '', '', 'Total Amount', $totalAmount]];
        $arrayDataList = array_merge($listData, $totalArray);
        $file = \Yii::createObject([
            'class' => 'codemix\excelexport\ExcelFile',
            'sheets' => [
                $tenderName1 => [// Name of the excel sheet
                    'data' => $arrayDataList,
                    'titles' => ['', '', 'BILLS OF QUANTITIES'],
                    'styles' => $style,
                ],
            ]
        ]);


        if ($file->send($tenderName . '.xlsx')) {
            return $this->redirect('index');
        }
    }


    public function actionGetTenderCodeCheck()
    {
        $tender_code = $_REQUEST['tender_code'];
        $codeList = Tender::find()->where(['tender_code' => $tender_code])->one();
        echo $codeList['tender_code'];
    }

}