<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\Project;

$getLastRecord=Project::find()->orderBy(['id' => SORT_DESC])->asArray()->one();


if(!empty($getLastRecord)){
    $autoId=$getLastRecord['id']+1;
}else{
    $autoId=1;
}


?>
<div class="project-form form-vertical">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'project_code', [
                'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Project Code",'class' => "project_code form-control",'value'=>'P00'.$autoId])->label(' Project Code')
            ?>
             <h5 class="error-msg2 hide text-danger"> </h5>
        </div>
       
        <div class="col-md-4">
            <?=
            $form->field($model, 'project_name', [
                'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => "Proposed Project Name"])->label('Proposed Project Name')
            ?>

        </div>
        <div class="col-md-4">
           
             <?=
        
            $form->field($model, 'client_name', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($assign_client, [
                'prompt' => 'Choose Client',
                'class' => 'select from control',
                // 'value'=>$model->project_type
            ])->label("Client")
            ?>
        </div>

    </div>
    <div class="row">
        
        <div class="col-md-4">
            <?=
            $form->field($model, 'project_type', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($projectTypeValue, [
                'prompt' => 'Choose status',
                'class' => 'select from control',
                // 'value'=>$model->project_type
            ])->label("Project Type")
            ?>
        </div>
  
    <div class="col-md-4">
            <?=
            $form->field($model, 'project_status', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($projectStatusValue, [
                'prompt' => 'Choose status',
                'class' => 'select from control',
                'value'=>$model->project_status
            ])->label("Project Status")
            ?>
        </div>
        <div class="col-md-4">
           
             <?=
            $form->field($model, 'building_type', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($buildingTypeValue, [
                'prompt' => 'Choose Buiding Type',
                'class' => 'select from control',
                'value'=>$model->building_type
            ])->label("Building Type")
            ?>
        </div>
        </div>
    <div class="row">
        <div class="col-md-4">
                    <?=
                    $form->field($model, 'consultant', [
                        'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Enter consultant"])->label('Consultant')
                    ?>
                </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'total_area', [
                        'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Enter Total Area"])->label('Total Area')
                    ?>
                </div>
             <div class="col-md-4">
                    <?=
                    $form->field($model, 'reference', [
                        'template' => "{label}\n
                    <div class='col-md-12 col-xs-12'>
                            {input}\n
                            {hint}\n
                            {error}
                    </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Enter Reference"])->label('Reference')
                    ?>
                </div>
                </div>
                <div class="row">
                <div class="col-md-4">
                <?=
            $form->field($model, 'tender_due_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Tender Due Date')
            ?>
                </div>
           
           
                <div class="col-md-4">
           
             <?=
            $form->field($model, 'property_account_type', [
                'template' => "{label}\n
        <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
        </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($accountTypeValue, [
                'prompt' => 'Choose Account Type',
                'class' => 'select from control',
                'value'=>$model->property_account_type
            ])->label("Account Type")
            ?>
        </div>
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'country', [
                        'template' => "{label}\n
                <div class='col-md-12 col-xs-12'>
                        {input}\n
                        {hint}\n
                        {error}
                </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Country"])->label('Country')
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?=
                    $form->field($model, 'amount', [
                        'template' => "{label}\n
                <div class='col-md-12 col-xs-12'>
                        {input}\n
                        {hint}\n
                        {error}
                </div>",
                        'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    ])->textInput(['placeholder' => "Project Amount"])->label(' Project  Amount')
                    ?>
                </div>

                <div class="col-md-4 hide">
           
           <?=
          $form->field($model, 'company_id', [
              'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
              {input}\n
              {hint}\n
              {error}
      </div>",
              'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
          ])->dropDownList($companyList, [
              'prompt' => 'Choose Company',
              'class' => 'select from control',
              'value'=>$model->company_id
          ])->label("Company")
          ?>
      </div>
                </div>
     <div class="row">
                <div class="form-group">
                    <label class="col-md-6 col-xs-12 control-label" for=""></label>
                    <div class="col-md-6 col-xs-12">
                        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                        <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

        <?= $this->registerJs("
    
        $('#project-tender_due_date').datetimepicker({
            defaultDate: new Date(),
            format: 'DD-MM-YYYY',
          });
        
", View::POS_READY); ?>

<script >
$(function () {
    $('.project_code').blur(function () {
        var project_code = $(this).val();
        $.ajax({
            url: "./project/project/company",
            data: {'project_code': project_code},
            type: "post",
            success: function (data) {
                $('#project_code').val(data);
                if (data != '') {
                    $('.error-msg2').removeClass("hide");
                    $('.error-msg2').html("Project Code Already Exists");
                }
                else{
                    $('.error-msg2').addClass("hide");
                }
            }
        });
    });
});
</script>