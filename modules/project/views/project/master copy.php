<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use execut\widget\TreeView;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;
use kartik\editable\Editable;
use kartik\builder\Form;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\data\ActiveDataProvider;

$getCategoryData = \app\models\SettingsTree::find()->where(['parent_id' => 2])
    ->asArray()
    ->all();
$category = [];
foreach ($getCategoryData as $value)
{
    $cat = [];
    $cat['id'] = $value['id'];
    $cat['name'] = $value['name'];
    $category[] = $cat;
}
$getSubActivityId = \app\models\SettingsTree::find()->asArray()->all();
$subActivityList = [];
foreach ($getSubActivityId as $value)
{
    if($value['parent_id'] != null && $value['parent_id'] != 1 && $value['parent_id'] != 2){
        $cat = [];
        $cat['id'] = $value['id'];
        $cat['name'] = $value['name'];
        $subActivityList[] = $cat;
    }
}


$getUnitTypeList = \app\models\SettingsTree::find()->where(['name' => 'Sq.Mtr'])
    ->asArray()
    ->all();
  
$unitTypeList = [];
foreach ($getUnitTypeList as $value)
{
    $cat = [];
    $cat['id'] = $value['id'];
    $cat['name'] = $value['name'];
    $unitTypeList[] = $cat;
}

$settingList = \app\models\SettingsTree::find()->asArray()->all();
$subActivityListData = [];
foreach ($settingList as $value1)
{
    if($value1['lvl'] != null && $value1['parent_id'] != 1 && $value1['parent_id'] != 2){
            $cat = [];
            $cat['id'] = $value1['id'];
            $cat['name'] = $value1['name'];
            if(!empty($subActivityListData) && !empty($subActivityListData[$value1['parent_id']])){
                $subActivityListData[$value1['parent_id']] = array_merge($subActivityListData[$value1['parent_id']], [$cat]);
            } else{
                $subActivityListData[$value1['parent_id']] = [$cat]; 
            }
    }
}
$projectId = $_GET['id'];
$project = app\modules\estimate\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
// pr($project);
$projectName = $project['project_name'];
$projectCode = $project['project_code'];
$location = $project['location'];
$status = $project['status'];
$startDate = $project['start_date'];
$endDate = $project['end_date'];
$tenderDueDate = $project['tender_due_date'];
$projectType="";
$projectTypeName = $project['project_type'];
if($projectTypeName==1){
    $projectType="Commercial";
}else if($projectTypeName==2){
   $projectType="Hospital";
    }

    $projectStatus="";
    $projectTypeStatus = $project['project_status'];
    if($projectTypeStatus==1){
        $projectStatus="Estimation";
    }else if($projectTypeStatus==2){
       $projectStatus="Job in Hand";
        }
        else if($projectTypeStatus==3){
            $projectStatus="Awarded";
             }else if($projectTypeStatus==4){
                $projectStatus="Design";
                 }
             $projectValue = $project['client_name'];
             $buildingType = $project['building_type'];
             $consultant = $project['consultant'];
             $totalArea = $project['total_area'];
             $reference = $project['reference'];
             $propertyAccountType = $project['property_account_type'];
$projectValue = $project['amount'];
$description = $project['description'];
$clientName = $project['client_name'];
$amount = $project['amount'];
$country = $project['country'];
$this->title = $projectName;
$this->params['breadcrumbs'][] = ['label' => 'Project', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
.table-responsive {
    min-height: 0.01%;
    overflow-x: auto;
}
</style>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
        <i class="fa fa-wrench" aria-hidden="true"></i> Estimate
    </div>
</div>
<div>&nbsp</div>
<div class="row">
<html>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://bossanova.uk/jexcel/v3/jexcel.js"></script>
<script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jexcel/v3/jexcel.css" type="text/css" />
<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jexcel/2.1.0/js/jquery.jdropdown.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jexcel/2.1.0/css/jquery.jdropdown.min.css" type="text/css" />
<script src="https://bossanova.uk/jexcel/v4/jexcel.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jexcel/v4/jexcel.css" type="text/css" />
<div class="row">
                        <div class="table-responsive">
                            <table class="table">
                                <colgroup>
                                    <col style="width:15%">
                                    <col style="width:35%">
                                    <col style="width:15%">
                                    <col style="width:35%">
                                </colgroup>
                                <tbody><tr>
                                        <th> Project Name</th>
                                        <td>
                                        <?php echo $projectName?>  
                        </td>
                        <th> Project Code</th>
                                        <td>
                                        <?php echo $projectCode?>    
                        </td>
                          </tr>
                                    <tr>
                                        <th>Client Name</th>
                                        <td>
                                        <?php echo $clientName?> 
                                         </td>
                                        <th>Building Type</th>
                                        <td>
                                        <?php echo $buildingType?> 
                                      </td>

                                    </tr>
                                    <tr>
                                        <th>Consultant</th>
                                        <td>
                                        <?php echo $consultant?> 
                                        </td>
                                        <th>Country</th>
                                        <td>
                                        <?php echo $country?> 
                        </td>
                                        
                                    </tr>
                                    <tr>
                                    
                                        <th>Reference</th>
                                        <td>
                                        <?php echo $reference?> 
                                        </td>
                                        <th>Total Area</th>
                                        <td>
                                        <?php echo $totalArea?> 
                                        </td>
                                    </tr>
                                    <tr>
                                    
                                    <th>Project Status</th>
                                    <td>
                                    <?php echo $projectStatus?> 
                                    </td>
                                    <th>Project Account Type</th>
                                    <td>
                                    <?php echo $propertyAccountType?> 
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <th>Project Type</th>
                                    <td>
                                    <?php echo ($projectType)?> 
                                    </td>
                                    <th>Project Amount</th>
                                    <td>
                                    <?php echo ($amount)?> 
                                    </td>
                                </tr>
                                </tbody></table>
                        </div>

                        <div>&nbsp</div>

    <div class="row">
        <div class="col-md-2">
            <label class="control-label">Total Cost</label>
            <input type="text" value="0" class="form-control totalcost" readOnly="true" id = "totalcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">Material Cost</label>
            <input type="text" value="0" class="form-control materialcost" readOnly="true" id = "materialcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">Labour Cost</label>
            <input type="text" value="0" class="form-control labourcost" readOnly="true" id = "labourcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">Profit</label>
            <input type="text" value="0" class="form-control profitcost" readOnly="true" id = "profitcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">OverHead</label>
            <input type="text" value="0" class="form-control overheadcost" readOnly="true" id = "overheadcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">Equipment Cost</label>
            <input type="text" value="0" class="form-control equipmentcost" readOnly="true" id = "equipmentcost">
        </div>
    </div>

<div>&nbsp</div>

<div class="row" style="padding-top: 12px">  
<div class="col-md-6 text-left add-label">
    <a onclick="table.insertRow()" class="btn btn-success btn-sm ">Add New Row</a>
    <a onclick="table.deleteRow();" class="btn btn-success btn-sm ">Delete Last Row</a>
</div>
<div class="col-md-6 text-right add-label">
<a href="export-excel?id=<?=$_GET['id']?>" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Download Excel</a> 
    <!-- <button id='exportData' class="btn btn-primary btn-sm"> <i class="fa fa-file-excel-o"></i> Download Excel</button> -->
<a href="export-pdf?id=<?=$_GET['id']?>" class="btn btn-success btn-sm"><i class="fa fa-file-pdf-o"></i> Download Pdf</a>
</div>
</div>

<div class="table-responsive">
    <div id="my"></div>
</div>
</html>
</div>
<div class="row">
<div class="col-md-12 text-right add-label" style="padding-top: 12px">
<input type='button' value='Save' id="getDataButton" class="btn btn-success btn-sm margin-right-10">
    </div>
</div>
<script>

dropdownFilter = function(instance, cell, c, r, source) {
    var value = instance.jexcel.getValueFromCoords(c - 1, r);
    if(value && value != ''){
        var dataVal = <?=json_encode($subActivityListData)?>;
        var subActivity = (dataVal[value]) ? dataVal[value] : [];
        return subActivity;
    } else {
        return source;
    }     
}

var data = <?= json_encode($arrayData) ?>;


var SUMCOL = function(instance, columnId) {
    // console.log('columnId',columnId);
    var total = 0;
    for (var j = 0; j < instance.options.data.length; j++) {
        if (Number(instance.records[j][columnId-1].innerHTML)) {
            // total += Number(instance.records[j][columnId-1].innerHTML);
            total += Math.ceil((Number(instance.records[j][columnId-1].innerHTML))*100)/100;
        }
    }
    if(columnId == 8){
      $('.materialcost').val(total);  
    } else if(columnId == 10){
      $('.labourcost').val(total);  
    }else if(columnId == 17){
      $('.profitcost').val(total.toFixed(2));  
    }else if(columnId == 14){
      $('.overheadcost').val(total.toFixed(2));  
    }else if(columnId == 11){
      $('.equipmentcost').val(total);  
    }else if(columnId == 6){
      $('.totalcost').val(total.toFixed(2));  
    }
    return total;
}
var table = jexcel(document.getElementById('my'), {
    data:data,
    
    columns: [
        {
            title: 'Activity',
            type:'dropdown',
            class: 'category',
            width:'200',
            source: <?=json_encode($category) ?>,
            autocomplete:true,
        },
        {
            title: 'Sub Activity',
            type:'dropdown',
            class: 'sub_activity_id',
            width:'400',
            filter:dropdownFilter,
            source:<?=json_encode($subActivityList) ?>,
            autocomplete:true,            
        },
        {
            title: 'Quantity',
            type: 'number',
            width:'100',
        },
        {
            title: 'Unit Type',
            type: 'hidden',
            class: 'unit_id',
            source: <?=json_encode(array_values($unitTypeList)) ?>,
            autocomplete:true,
            width:'100',
        },
        {
            title: '(AED)',
            type: 'number',
            width:'100',
        },
        {
            title: '(AED)',
            type: 'number',
            width:'100',
        },
        {
            title: 'Rate ',
            // class: 'unit_id',
            type: 'number',
            width:'100',
        },
        {
            title: 'Total',
            type: 'number',
            width:'100',
            // readOnly:true,
        },
        {
            title: 'Rate ',
            type: 'number',
            width:'100px',
        },
        {
            title: 'Total',
            type: 'number',
            width:'100px',
            // readOnly:true,
        },
        {
            title: 'Cost ',
            type: 'number',
            width:'100px',
            // readOnly:true,
        },
        {
            title: 'Cost',
            type: 'number',
            width:'150px',
        },
        {
            title: '% ',
            type: 'number',
            width:'100px',
        },
        {
            title: 'Amount ',
            type: 'number',
            width:'100px',
            // readOnly:true,
        },
        // {
        //     title: 'Net Cost O ',
        //     type: 'number',
        //     width:'100px',
        //     // readOnly:true,
        // },
        {
            title: '%  ',
            type: 'number',
            width:'100px',
        },
        {
            title: '% ',
            type: 'number',
            width:'100px',
            // readOnly:true,
        },
        {
            title: 'Amount  ',
            type: 'number',
            width:'100px',
            // readOnly:true,
        },
        {
            title: 'Total  ',
            type: 'number',
            width:'100px',
            // readOnly:true,
        },
        {
            title: 'Unit Rate',
            type: 'number',
            width:'100px',
            // readOnly:true,
        },
        
    ],

    minDimensions:[10,5],
    rowResize: true,
    columnDrag: true,
    nestedHeaders:[
        [
            {
                title: 'Estimate Master Details',
                colspan: '18',
            },
        ],
        [
            {
                colspan: '3',
            },
            {
                title: 'Unit Rate',
                colspan: '1'
            },
            {
                title: 'Amount',
                colspan: '1'
            },
            {
                title: 'Material',
                colspan: '2'
            },
            {
                title: 'Labour',
                colspan: '2'
            },
            {
                title: 'Equipment',
                colspan: '1'
            },
            {
                title: 'Mat + Lab + Equ',
                colspan: '1'
            },
            {
                title: 'OverHead',
                colspan:2,
            },
            {
                title: 'Net cost',
                colspan:1,
            },
            {
                title: 'Profit',
                colspan:2,
            },
            {
                title: 'Selling Price',
                colspan:2,
            }
        ],
    ],
    updateTable:function(instance, cell, col, row, val, label, cellName) {
       
       if (col == 0) {
        
            $(cell).addClass('category');
        }
    },
    footers: [['','','Total','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)']],
    onchange:function(instance, cell, c, r, value) {
        if(c==0){      
        var activity_id = instance.jexcel.getValueFromCoords(c, r);
        var columnLPC= parseInt(c) + parseInt(1);
        var columnName = jexcel.getColumnNameFromId([columnLPC, r]);
        var resData = 0;
        var url;
        url = "./get-activity-list";
        dataVal= {  'activity_id': activity_id };
        $.ajax({
                url: url,
                data: dataVal,
                type: "post",
                success: function (data) {
                resData = data;
                instance.jexcel.setValue(columnName, resData);                  
                }                
            });
        }
        if(c==1){
        var activityId = instance.jexcel.getValueFromCoords(c - 1, r);
        var subActivityId = instance.jexcel.getValueFromCoords(c, r);
        var columnLPC= parseInt(c) + parseInt(1);
        var columnName = jexcel.getColumnNameFromId([columnLPC, r]);
        var resData = 0;
          $.ajax({
                url: "./get-estimate-data",
                data: {  'activity_id': activityId, 'sub_activity_id': subActivityId },
                type: "post",
                success: function (data) {
                resData = data;
                instance.jexcel.setValue(columnName, resData);
                }               
            });
        }
        if(c==2){
            var activityId = instance.jexcel.getValueFromCoords(0, r);
            var subActivityId =instance.jexcel.getValueFromCoords(1, r);
            var quantityVal =instance.jexcel.getValueFromCoords(2, r);
            
            
            var UnitRateC= parseInt(c) + parseInt(2);
            var TotalRateC= parseInt(c) + parseInt(3);
            var MaterialRateC= parseInt(c) + parseInt(4);
            var MaterialTotalC= parseInt(c) + parseInt(5);

            var LabourRateC= parseInt(c) + parseInt(6);
            var LabourTotalC= parseInt(c) + parseInt(7);
            var EquipmentCostC= parseInt(c) + parseInt(8);
            var MatLabEquTotalC= parseInt(c) + parseInt(9);
            var OhPercentC= parseInt(c) + parseInt(10);
            var OhAmountC= parseInt(c) + parseInt(11);
            var netCostC= parseInt(c) + parseInt(12);
            // console.log('netCostC',netCostC);
            var profitPercentC= parseInt(c) + parseInt(13);
            var profitAmountC= parseInt(c) + parseInt(14);
            var sellingTotalC= parseInt(c) + parseInt(15);
            var sellingUnitC= parseInt(c) + parseInt(16);
            var UnitRateColumn = jexcel.getColumnNameFromId([UnitRateC, r]);
            var TotalRateColumn = jexcel.getColumnNameFromId([TotalRateC, r]);


            var MaterialRateColumn = jexcel.getColumnNameFromId([MaterialRateC, r]);
           
            var MaterialTotalColumn = jexcel.getColumnNameFromId([MaterialTotalC, r]);
            var LabourRateColumn = jexcel.getColumnNameFromId([LabourRateC, r]);
            var LabourTotalColumn = jexcel.getColumnNameFromId([LabourTotalC, r]);
            var EquipmentCostColumn = jexcel.getColumnNameFromId([EquipmentCostC, r]);
            var MatLapEquColumn = jexcel.getColumnNameFromId([MatLabEquTotalC, r]);
            var OhPercentColumn = jexcel.getColumnNameFromId([OhPercentC, r]);
            var OhAmountColumn = jexcel.getColumnNameFromId([OhAmountC, r]);
            var netCostColumn = jexcel.getColumnNameFromId([netCostC, r]);
            // console.log('netCostColumn',netCostColumn);
            var profitPercentColumn = jexcel.getColumnNameFromId([profitPercentC, r]);
            var profitAmountColumn = jexcel.getColumnNameFromId([profitAmountC, r]);
            var sellingTotalColumn = jexcel.getColumnNameFromId([sellingTotalC, r]);
            var sellingUnitColumn = jexcel.getColumnNameFromId([sellingUnitC, r]);
            var resData = 0;
            console.log('total',('=ROUND((G'+(++r)+ '*C'+(r)+'),2)')); 
            // console.log('total',('=ROUND((L'+(r+1)+ '+N'+(r+1)+'),2)')); 
            console.log('netcost amount',('=ROUND((L'+(r)+ '+N'+(r)+'),2)'));
            $.ajax({
            url: "./get-material-data",
            data: {  'activity_id' :activityId,'sub_activity_id':subActivityId},
            type: "post",
            success: function (data) {
                var valData = JSON.parse(data);
               var  materialDataRate =0;
               var labourDataRate =0;
               var EquipmentCostData = 0;
               var OhPercentData =0;
               var ProfitPercentData = 0;
               var ProfitPercentData =0;
               var materialDataTotal =0;
               var labourDataTotal =0;
               var MatLapEquData = 0;
               var OhAmountData = 0;
               var netCostAmount =0;
               var ProfitAmountData =0;
               var sellingTotalData =0;
               var sellingUnitData =0;
                for(var i = 0; i < valData.length; i++) {
            //  console.log('valData',valData);
                    materialDataRate = (valData[0]) ? valData[0] : 0 ;
                    
                    labourDataRate = valData[1] ? valData[1] : 0;
                    EquipmentCostData= valData[2] ? valData[2] : 0;
                    OhPercentData =valData[3] ? valData[3] : 0;
                    ProfitPercentData =valData[4]  ? valData[4] : 0;
                    materialDataTotal = ((quantityVal) * (materialDataRate));
                    labourDataTotal = valData[1]*quantityVal;
                    MatLapEquData=(parseInt(materialDataTotal) +parseInt(labourDataTotal) + parseInt(EquipmentCostData) );
                //    console.log('MatLapEquData',MatLapEquData);
                    OhAmountData =(MatLapEquData * OhPercentData)/100;
                    // console.log('OhAmountData',OhAmountData);
                    netCostAmount =MatLapEquData + OhAmountData;
                    // console.log('netCostAmount',netCostAmount);
                    ProfitAmountData =(netCostAmount *ProfitPercentData)/100;
                    sellingTotalData =netCostAmount + ProfitAmountData;
                    sellingUnitData =sellingTotalData /quantityVal;
                }
        instance.jexcel.setValue(MaterialRateColumn, materialDataRate);
        instance.jexcel.setValue(MaterialTotalColumn, ('=ROUND((G'+(r)+ '*C'+(r)+'),2)'));   
         instance.jexcel.setValue(LabourRateColumn, labourDataRate);
                instance.jexcel.setValue(LabourTotalColumn, ('=ROUND((I'+(r)+ '*C'+(r)+'),2)'));
                instance.jexcel.setValue(EquipmentCostColumn, EquipmentCostData);    
                instance.jexcel.setValue(MatLapEquColumn, ('=ROUND((H'+(r)+ '+J'+(r)+ '+K'+(r)+'),2)')); 
                instance.jexcel.setValue(OhPercentColumn, OhPercentData);  
                instance.jexcel.setValue(OhAmountColumn, ('=ROUND(((L'+(r)+ '*M'+(r)+(')/100')+'),2)'));  
                
                // instance.jexcel.setValue(netCostColumn,netCostAmount.toFixed(2));
                instance.jexcel.setValue(netCostColumn,('=ROUND((L'+(r)+ '+N'+(r)+'),2)'));
                instance.jexcel.setValue(profitPercentColumn, ProfitPercentData); 
                instance.jexcel.setValue(profitAmountColumn, ProfitAmountData.toFixed(2)); 
                instance.jexcel.setValue(sellingTotalColumn, sellingTotalData.toFixed(2)); 
                instance.jexcel.setValue(sellingUnitColumn, sellingUnitData.toFixed(2));  
                instance.jexcel.setValue(UnitRateColumn, sellingUnitData.toFixed(2));   
                instance.jexcel.setValue(TotalRateColumn, (sellingTotalData.toFixed(2))); 
         
        }
            });
        }
    }
});

// document.getElementById('exportData').onclick = function () {
//     table.download(true);
// return false;
// }

$('#getDataButton').on('click', function ()
    {
        var data = $('#my').jexcel("getData");
        
        $('.error-msg').addClass("disable");
        var project_id =  <?php echo json_encode($_GET['id']) ?>;
        var formdata = data;
        $.ajax({
            url: './create-estimate',
            data: {  'formdata': data, 'project_id':project_id, },
            type: 'POST',
            success: function (response) {
                console.log(response);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
</script>
<style type="text/css">
 thead.resizable tr:nth-child(1) td:nth-child(n){
   /* background: #66d651; */
   font-weight: bold;
   font-size: 18px;
 }
 thead.resizable tr:nth-child(2) td:nth-child(n){
   /* background: #ff470094; */
   font-weight: bold;
   font-size: 15px;
 }
 thead.resizable tr:nth-child(3) td:nth-child(n){
   /* background: #ffb100c7; */
   font-weight: bold;
   font-size: 13px;
 }
tbody.resizable tr:nth-child(n) td:nth-child(1){
  background:#ffffff;  
}
tbody.resizable tr:nth-child(n) td:nth-child(2){
 background:#ffffff; 
}
tbody.resizable tr:nth-child(n) td:nth-child(n){
  font-weight: 600;
  /* background:#a9a6a6bf;   */
}

 </style>