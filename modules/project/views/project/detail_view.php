<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use execut\widget\TreeView;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;
use kartik\editable\Editable;
use kartik\builder\Form;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\data\ActiveDataProvider;


$this->title = $projectName;
$this->params['breadcrumbs'][] = ['label' => 'Project', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .table-responsive {
        min-height: 0.01%;
        overflow-x: auto;
    }
</style>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
        <i class="fa fa-wrench" aria-hidden="true"></i> Project Resource
    </div>
</div>
<div>&nbsp</div>
<div class="row">
    <html>
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/jquery.min.js"></script>
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/jexcel.js"></script>
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/jsuites.js"></script>
    <link rel="stylesheet" href="https://bossanova.uk/jexcel/v3/jexcel.css" type="text/css" />
    <link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/numeral.min.js"></script>
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/jquery.jdropdown.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jexcel/2.1.0/css/jquery.jdropdown.min.css" type="text/css" />
    <script src="https://bossanova.uk/jexcel/v4/jexcel.js"></script>
    <link rel="stylesheet" href="https://bossanova.uk/jexcel/v4/jexcel.css" type="text/css" />

   
    <div class="row">
        <div class="table-responsive">
            <table class="table">
                <colgroup>
                    <col style="width:15%">
                    <col style="width:35%">
                    <col style="width:15%">
                    <col style="width:35%">
                </colgroup>
                <tbody><tr>
                    <th> <b>Project Code</b></th>
                    <td>
                        <b> <?php echo $projectCode?></b>
                    </td>
                    <th> <b>Project Name</b></th>
                    <td>
                        <b> <?php echo $projectName?></b>
                    </td>

                </tr>

                </tbody></table>
        </div>

        <div>&nbsp</div>


      
        <div class="table-responsive">
            <div id="my"></div>
        </div>
    </html>
</div>

<script>

    

var data = <?= json_encode($arrayData) ?>;

 var table = jexcel(document.getElementById('my'), {
        data:data,
        columns: [
            {
                title: 'Name',
                type:'text',
                class: 'category',
                width:'500',
                
                autocomplete:true,
            },
            {
                title: 'Total',
                type:'numeric',
                width:'225',
                autocomplete:true,
            },
            {
                title: 'Purchased',
                type: 'numeric',
                width:'225',
                wordWrap:true
            },
            {
                title: 'Remaining',
                type: 'numeric',
                width:'225',
            },
            
            

        ],
        minDimensions:[4,15],
        rowResize: true,
        columnDrag: true,
        tableWidth: "600px",


     updateTable:function(instance, cell, col, row, val, label, cellName) {

            if (col == 0 || col == 1 || col == 2 || col ==3 || col == 4 || col == 5 || col == 6) {
                if(col == 0) {
                    $(cell).addClass('category');
                }
                $(cell).css('background-color', 'white');
            } else {
                $(cell).css('background-color','rgb(247 247 246 / 0.29)');
            }
            $(cell).css('font-weight',400);
        },
        // footers: [['','','Total','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)']],
        onchange:function(instance, cell, c, r, value) {
         
            if(c==0){
            var resource_id = instance.jexcel.getValueFromCoords(c, r);
       
            var columnLPC= parseInt(c) + parseInt(1);
            var columnName = jexcel.getColumnNameFromId([columnLPC, r]);
            var resData = 0;
            var url;
            var project_id =  <?php echo json_encode($_GET['id']) ?>;
           
            url = "./get-total-quantity";
            dataVal= {  'material_id': resource_id,'project_id':project_id };
            $.ajax({
                    url: url,
                    data: dataVal,
                    type: "post",
                    success: function (data) {
                    resData = data;
                    instance.jexcel.setValue(columnName, resData);
                    }
                });
            }
            
           
        },
    });

    // document.getElementById('exportData').onclick = function () {
    //     table.download(true);
    // return false;
    // }

    $('#getDataButton').on('click', function ()
    {
        var data = $('#my').jexcel("getData");

        $('.error-msg').addClass("disable");
        var project_id =  <?php echo json_encode($_GET['id']) ?>;
        var formdata = data;
        $.ajax({
            url: './create-estimate',
            data: {  'formdata': data, 'project_id':project_id, },
            type: 'POST',
            success: function (response) {
                console.log(response);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
</script>
<style type="text/css">
    thead.resizable tr:nth-child(1) td:nth-child(n){
        font-weight: bold;
        font-size: 13px;
    }
    thead.resizable tr:nth-child(2) td:nth-child(n){
        font-weight: bold;
        font-size: 13px;
    }
    thead.resizable tr:nth-child(3) td:nth-child(n){
        font-weight: bold;
        font-size: 10px;
    }
</style>