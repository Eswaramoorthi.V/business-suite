<?php
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use execut\widget\TreeView;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;
use kartik\editable\Editable;
use kartik\builder\Form;
use kartik\select2\Select2;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\data\ActiveDataProvider;


$this->title = $projectName;
$this->params['breadcrumbs'][] = ['label' => 'Project', 'url' => ['project/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .table-responsive {
        min-height: 0.01%;
        overflow-x: auto;
    }
</style>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
        <i class="fa fa-wrench" aria-hidden="true"></i> Estimate
    </div>
</div>
<div>&nbsp</div>
<div class="row">
    <html>
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/jquery.min.js"></script>
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/jexcel.js"></script>
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/jsuites.js"></script>
    <link rel="stylesheet" href="https://bossanova.uk/jexcel/v3/jexcel.css" type="text/css" />
    <link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/numeral.min.js"></script>
    <script src="<?= Url::base() ?>/web/lib/js/jexcel/jquery.jdropdown.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jexcel/2.1.0/css/jquery.jdropdown.min.css" type="text/css" />
    <script src="https://bossanova.uk/jexcel/v4/jexcel.js"></script>
    <link rel="stylesheet" href="https://bossanova.uk/jexcel/v4/jexcel.css" type="text/css" />
    <div class="row">
        <div class="table-responsive">
            <table class="table">
                <colgroup>
                    <col style="width:15%">
                    <col style="width:35%">
                    <col style="width:15%">
                    <col style="width:35%">
                </colgroup>
                <tbody><tr>
                    <th> Project Code</th>
                    <td>
                        <?php echo $projectCode?>
                    </td>
                    <th> Project Name</th>
                    <td>
                        <?php echo $projectName?>
                    </td>

                </tr>

                </tbody></table>
        </div>

        <div>&nbsp</div>

        <div class="row">
            <div class="col-md-2">
                <label class="control-label">Total Cost</label>
                <input type="text" value="0" class="form-control totalcost" readOnly="true" id = "totalcost">
            </div>
            <div class="col-md-2">
                <label class="control-label">Material Cost</label>
                <input type="text" value="0" class="form-control materialcost" readOnly="true" id = "materialcost">
            </div>
            <div class="col-md-2">
                <label class="control-label">Labour Cost</label>
                <input type="text" value="0" class="form-control labourcost" readOnly="true" id = "labourcost">
            </div>
            <div class="col-md-2">
                <label class="control-label">Profit</label>
                <input type="text" value="0" class="form-control profitcost" readOnly="true" id = "profitcost">
            </div>
            <div class="col-md-2">
                <label class="control-label">OverHead</label>
                <input type="text" value="0" class="form-control overheadcost" readOnly="true" id = "overheadcost">
            </div>
            <div class="col-md-2">
                <label class="control-label">Equipment Cost</label>
                <input type="text" value="0" class="form-control equipmentcost" readOnly="true" id = "equipmentcost">
            </div>
        </div>

        <div>&nbsp</div>

        <div class="row" style="padding-top: 12px">
            <div class="col-md-6 text-left add-label">
                <a onclick="table.insertRow()" class="btn btn-success btn-sm ">Add New Row</a>
                <a onclick="table.deleteRow();" class="btn btn-success btn-sm ">Delete Last Row</a>
            </div>
            <div class="col-md-6 text-right add-label">
            <a href="export-full-excel?id=<?=$_GET['id']?>" class="btn btn-primary btn-sm"<?php $visibleList['export-full-excel']?>><i class="fa fa-file-excel-o"></i> Download  Full Excel</a>
            <a href="export-excel?id=<?=$_GET['id']?>" class="btn btn-primary btn-sm"<?php $visibleList['export-excel']?>><i class="fa fa-file-excel-o"></i> Download  Excel</a>
                <!-- <a href="export-excel?id=<?=$_GET['id']?>" class="btn btn-success btn-sm"><i class="fa fa-file-excel-o"></i> Download Excel</a> -->
                <!-- <button id='exportData' class="btn btn-primary btn-sm"> <i class="fa fa-file-excel-o"></i> Download Excel</button> -->
                <a href="export-pdf?id=<?=$_GET['id']?>" class="btn btn-success btn-sm"<?php $visibleList['export-pdf']?>><i class="fa fa-file-pdf-o"></i> Download Pdf</a>
            </div>
        </div>

        <div class="table-responsive">
            <div id="my"></div>
        </div>
    </html>
</div>
<div class="row">
    <div class="col-md-12 text-right add-label" style="padding-top: 12px">
        <input type='button' value='Save' id="getDataButton" class="btn btn-success btn-sm margin-right-10">
    </div>
</div>
<script>

    dropdownFilter = function(instance, cell, c, r, source) {
        var value = instance.jexcel.getValueFromCoords(c - 1, r);
       
        if(value && value != ''){
            var dataVal = <?=json_encode($sub_activity_list)?>;
            
            var subActivity = (dataVal[value]) ? dataVal[value] : [];
          
            return subActivity;
        } else {
            return source;
        }
    }

    var data = <?= json_encode($arrayData) ?>;


    var SUMCOL = function(instance, columnId) {
        var total = 0;
        for (var j = 0; j < instance.options.data.length; j++) {
            if (Number(instance.records[j][columnId-1].innerHTML)) {
                // total += Number(instance.records[j][columnId-1].innerHTML);
                total += Math.ceil((Number(instance.records[j][columnId-1].innerHTML))*100)/100;
            }
        }
        if(columnId == 9){
            $('.materialcost').val(total);
        } else if(columnId == 11){
            $('.labourcost').val(total);
        }else if(columnId == 18){
            $('.profitcost').val(total.toFixed(2));
        }else if(columnId == 15){
            $('.overheadcost').val(total.toFixed(2));
        }else if(columnId == 12){
            $('.equipmentcost').val(total);
        }else if(columnId == 7){
            $('.totalcost').val(total.toFixed(2));
        }
        return total;
    }
    var table = jexcel(document.getElementById('my'), {
        data:data,
        columns: [
            {
                title: 'Activity',
                type:'dropdown',
                class: 'category',
                width:'200',
                source: <?=json_encode($category) ?>,
                autocomplete:true,
            },
            {
                title: 'Sub Activity',
                type:'dropdown',
                class: 'sub_activity_id',
                width:'200',
                filter:dropdownFilter,
                source:<?=json_encode($subActivityListData) ?>,
                autocomplete:true,
            },
            {
                title: 'Description',
                type: 'text',
                width:'250',
                wordWrap:true
            },
            {
                title: 'Quantity',
                type: 'number',
                width:'100',
            },
            {
                title: 'Unit Type',
                type: 'dropdown',
                class: 'unit_id',
                source: <?=json_encode($unitTypeListData) ?>,
                autocomplete:true,
                width:'100',
            },
            {
                title: '(AED)',
                type: 'number',
                width:'100',
                //readOnly:true,
            },
            {
                title: '(AED)',
                type: 'number',
                width:'100',
               // readOnly:true
            },
            {
                title: 'Rate ',
                // class: 'unit_id',
                type: 'number',
                width:'100',
            },
            {
                title: 'Total',
                type: 'number',
                width:'100',
               // readOnly:true,
            },
            {
                title: 'Rate ',
                type: 'number',
                width:'100px',
            },
            {
                title: 'Total',
                type: 'number',
                width:'100px',
                 //readOnly:true,
            },
            {
                title: 'Cost ',
                type: 'number',
                width:'100px',

            },
            {
                title: 'Cost',
                type: 'number',
                width:'150px',
                //readOnly:true,
            },
            {
                title: '% ',
                type: 'number',
                width:'100px',
            },
            {
                title: 'Amount ',
                type: 'number',
                width:'100px',
               // readOnly:true,
            },
            {
                title: '%  ',
                type: 'number',
                width:'100px',
            },
            {
                title: '% ',
                type: 'number',
                width:'100px',
                //readOnly:true,
            },
            {
                title: 'Amount  ',
                type: 'number',
                width:'100px',
                //readOnly:true,
            },
            {
                title: 'Total  ',
                type: 'number',
                width:'100px',
               // readOnly:true,
            },
            {
                title: 'Unit Rate',
                type: 'number',
                width:'100px',
                //readOnly:true,
            },

        ],
        minDimensions:[10,5],
        rowResize: true,
        columnDrag: true,
        nestedHeaders:[
            [
                {
                    title: 'Estimate Master Details',
                    colspan: '20',
                },
            ],
            [
                {
                    colspan: '5',
                },
                {
                    title: 'Unit Rate',
                    colspan: '1'
                },
                {
                    title: 'Amount',
                    colspan: '1'
                },
                {
                    title: 'Material',
                    colspan: '2'
                },
                {
                    title: 'Labour',
                    colspan: '2'
                },
                {
                    title: 'Equipment',
                    colspan: '1'
                },
                {
                    title: 'Mat + Lab + Equ',
                    colspan: '1'
                },
                {
                    title: 'OverHead',
                    colspan:2,
                },
                {
                    title: 'Net cost',
                    colspan:1,
                },
                {
                    title: 'Profit',
                    colspan:2,
                },
                {
                    title: 'Selling Price',
                    colspan:2,
                }
            ],
        ],
        updateTable:function(instance, cell, col, row, val, label, cellName) {

            if (col == 0 || col == 1 || col == 2 || col ==3 || col == 4 || col == 5 || col == 6) {
                if(col == 0) {
                    $(cell).addClass('category');
                }
                $(cell).css('background-color', 'white');
            } else {
                $(cell).css('background-color','rgb(247 247 246 / 0.29)');
            }
            $(cell).css('font-weight',600);
        },
        footers: [['','','Total','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','=ROUND((SUMCOL(TABLE(), COLUMN())),2)']],
        onchange:function(instance, cell, c, r, value) {
            console.log('c',c);
            // if(c==0){
            // var activity_id = instance.jexcel.getValueFromCoords(c, r);
            // var columnLPC= parseInt(c) + parseInt(1);
            // var columnName = jexcel.getColumnNameFromId([columnLPC, r]);
            // var resData = 0;
            // var url;
            // url = "./get-activity-list";
            // dataVal= {  'activity_id': activity_id };
            // $.ajax({
            //         url: url,
            //         data: dataVal,
            //         type: "post",
            //         success: function (data) {
            //         resData = data;
            //         instance.jexcel.setValue(columnName, resData);
            //         }
            //     });
            // }
            if(c==1){
                var activityId = instance.jexcel.getValueFromCoords(c - 1, r);
                var subActivityId = instance.jexcel.getValueFromCoords(c, r);
                var columnLPC= parseInt(c) + parseInt(2);
                var descriptionC= parseInt(c) + parseInt(1);
                var columnName = jexcel.getColumnNameFromId([columnLPC, r]);
                var descriptionColumn = jexcel.getColumnNameFromId([descriptionC, r]);
                // var resData = 0;
                var description="";
                $.ajax({
                    url: "./get-estimate-data",
                    data: {  'activity_id': activityId, 'sub_activity_id': subActivityId },
                    type: "post",
                    success: function (data) {
                        // console.log('data',data);
                        resData = 1;
                        instance.jexcel.setValue(columnName, resData);
                    }
                });
                $.ajax({
                    url: "./get-description",
                    data: { 'sub_activity': subActivityId },
                    type: "post",

                    success: function (data) {
                        description = data;
                        instance.jexcel.setValue(descriptionColumn, description);
                    }
                });
            }
            if(c==1) {
                c=4;
                var activityId = instance.jexcel.getValueFromCoords(0, r);
                var subActivityId =instance.jexcel.getValueFromCoords(1, r);
                var quantityVal =instance.jexcel.getValueFromCoords(3, r);
                var UnitRateC= parseInt(c) + parseInt(1);
                var TotalRateC= parseInt(c) + parseInt(2);
                var MaterialRateC= parseInt(c) + parseInt(3);
                var MaterialTotalC= parseInt(c) + parseInt(4);
                var LabourRateC= parseInt(c) + parseInt(5);
                var LabourTotalC= parseInt(c) + parseInt(6);
                var EquipmentCostC= parseInt(c) + parseInt(7);
                var MatLabEquTotalC= parseInt(c) + parseInt(8);
                var OhPercentC= parseInt(c) + parseInt(9);
                var OhAmountC= parseInt(c) + parseInt(10);
                var netCostC= parseInt(c) + parseInt(11);
                var profitPercentC= parseInt(c) + parseInt(12);
                var profitAmountC= parseInt(c) + parseInt(13);
                var sellingTotalC= parseInt(c) + parseInt(14);
                var sellingUnitC= parseInt(c) + parseInt(15);
                var UnitRateColumn = jexcel.getColumnNameFromId([UnitRateC, r]);
                var TotalRateColumn = jexcel.getColumnNameFromId([TotalRateC, r]);
                var MaterialRateColumn = jexcel.getColumnNameFromId([MaterialRateC, r]);
                var MaterialTotalColumn = jexcel.getColumnNameFromId([MaterialTotalC, r]);
                var LabourRateColumn = jexcel.getColumnNameFromId([LabourRateC, r]);
                var LabourTotalColumn = jexcel.getColumnNameFromId([LabourTotalC, r]);
                var EquipmentCostColumn = jexcel.getColumnNameFromId([EquipmentCostC, r]);
                var MatLapEquColumn = jexcel.getColumnNameFromId([MatLabEquTotalC, r]);
                var OhPercentColumn = jexcel.getColumnNameFromId([OhPercentC, r]);
                var OhAmountColumn = jexcel.getColumnNameFromId([OhAmountC, r]);
                var netCostColumn = jexcel.getColumnNameFromId([netCostC, r]);
                var profitPercentColumn = jexcel.getColumnNameFromId([profitPercentC, r]);
                var profitAmountColumn = jexcel.getColumnNameFromId([profitAmountC, r]);
                var sellingTotalColumn = jexcel.getColumnNameFromId([sellingTotalC, r]);
                var sellingUnitColumn = jexcel.getColumnNameFromId([sellingUnitC, r]);
                // var resData = 0;
                $.ajax({
                    url: "./get-material-data",
                    data: {  'activity_id' :activityId,'sub_activity_id':subActivityId},
                    type: "post",
                    success: function (data) {
                        var materialDataRate =0;
                        var labourDataRate =0;
                        var EquipmentCostData =0;
                        var OhPercentData =0;
                        var ProfitPercentData =0;
                        var valData = JSON.parse(data);

                        for(var i = 0; i < valData.length; i++) {
                            materialDataRate = (valData[0]) ? valData[0] : 0 ;
                            labourDataRate = valData[1] ? valData[1] : 0;
                            EquipmentCostData= valData[2] ? valData[2] : 0;
                            OhPercentData =valData[3] ? valData[3] : 0;
                            ProfitPercentData =valData[4]  ? valData[4] : 0;

                        }
                        var totalValue = parseInt(materialDataRate) + parseInt(labourDataRate) + parseInt(EquipmentCostData) + parseInt(OhPercentData) + parseInt(ProfitPercentData);
                        instance.jexcel.setValue(MaterialRateColumn, materialDataRate);

                        console.log("Total"+totalValue);

                        console.log('total','=I'+(++r)+ '*C'+(+r));
                        console.log('total amount',('=L'+(r)+ '*M'+(r)+('/100')));


                        if(totalValue!=0){
                            instance.jexcel.setValue(MaterialTotalColumn, ('=ROUND((H'+(r)+ '*D'+(r)+'),2)'));
                            instance.jexcel.setValue(LabourRateColumn, labourDataRate);
                            instance.jexcel.setValue(LabourTotalColumn, ('=ROUND((J'+(r)+ '*D'+(r)+'),2)'));
                            instance.jexcel.setValue(EquipmentCostColumn, EquipmentCostData);
                            instance.jexcel.setValue(MatLapEquColumn, ('=ROUND((I'+(r)+ '+K'+(r)+ '+L'+(r)+'),2)'));
                            instance.jexcel.setValue(OhPercentColumn, OhPercentData);
                            instance.jexcel.setValue(OhAmountColumn, ('=ROUND(((M'+(r)+ '*N'+(r)+(')/100')+'),2)'));

                            instance.jexcel.setValue(netCostColumn, ('=ROUND((M'+(r)+ '+O'+(r)+'),2)'));
                            instance.jexcel.setValue(profitPercentColumn, ProfitPercentData);
                            instance.jexcel.setValue(profitAmountColumn, ('=ROUND(((P'+(r)+ '*Q'+(r)+(')/100')+'),2)') );
                            instance.jexcel.setValue(sellingTotalColumn, ('=ROUND((P'+(r)+ '+R'+(r)+'),2)'));
                            instance.jexcel.setValue(sellingUnitColumn, ('=ROUND((S'+(r)+ '/D'+(r)+'),2)'));
                            instance.jexcel.setValue(UnitRateColumn, ('=ROUND((S'+(r)+ '/D'+(r)+'),2)'));
                            instance.jexcel.setValue(TotalRateColumn, ('=ROUND((P'+(r)+ '+R'+(r)+'),2)'));
                        }else{

                            instance.jexcel.setValue(MaterialTotalColumn, ('=(H'+(r)+ '*D'+(r)+')'));
                            instance.jexcel.setValue(LabourRateColumn, labourDataRate);
                            instance.jexcel.setValue(LabourTotalColumn, ('=(J'+(r)+ '*D'+(r)+')'));
                            instance.jexcel.setValue(EquipmentCostColumn, EquipmentCostData);
                            instance.jexcel.setValue(MatLapEquColumn, ('=(I'+(r)+ '+K'+(r)+ '+L'+(r)+')'));
                            instance.jexcel.setValue(OhPercentColumn, OhPercentData);
                            instance.jexcel.setValue(OhAmountColumn, ('=((M'+(r)+ '*N'+(r)+(')/100')+')'));



                            instance.jexcel.setValue(netCostColumn, ('=(M'+(r)+ '+O'+(r)+')'));
                     
                            instance.jexcel.setValue(profitPercentColumn, ProfitPercentData);
                            instance.jexcel.setValue(profitAmountColumn, ('=((P'+(r)+ '*Q'+(r)+(')/100')+')'));
                            instance.jexcel.setValue(sellingTotalColumn, ('=(P'+(r)+ '+R'+(r)+')'));
                            instance.jexcel.setValue(sellingUnitColumn, ('=(S'+(r)+ '/D'+(r)+')'));
                            // instance.jexcel.setValue(sellingUnitColumn, NaN('=(S'+(r)+ '/D'+(r)+')') ? 0 : ('=(S'+(r)+ '/D'+(r)+')'));
                            // var value1 = instance.jexcel.getData('S'+(r));
                            // console.log("Value1+",value1);
                            // instance.jexcel.setValue(UnitRateColumn, ('=(S'+(r)+ '/D'+(r)+')'));
                            instance.jexcel.setValue(UnitRateColumn, ('=(S'+(r)+ '/D'+(r)+')'));
                            instance.jexcel.setValue(TotalRateColumn, ('=(P'+(r)+ '+R'+(r)+')'));
                            console.log("LLL"+instance.jexcel.getValue(sellingTotalColumn));
                        }

                        console.log("LLL"+instance.jexcel.getValue(MatLapEquColumn));


                    }
                });
            }
        },
    });

    // document.getElementById('exportData').onclick = function () {
    //     table.download(true);
    // return false;
    // }

    $('#getDataButton').on('click', function ()
    {
        var data = $('#my').jexcel("getData");

        $('.error-msg').addClass("disable");
        var project_id =  <?php echo json_encode($_GET['id']) ?>;
        var formdata = data;
        $.ajax({
            url: './create-estimate',
            data: {  'formdata': data, 'project_id':project_id, },
            type: 'POST',
            success: function (response) {
                console.log(response);
            },
            error: function (error) {
                console.log(error);
            }
        });
    });
</script>
<style type="text/css">
    thead.resizable tr:nth-child(1) td:nth-child(n){
        font-weight: bold;
        font-size: 18px;
    }
    thead.resizable tr:nth-child(2) td:nth-child(n){
        font-weight: bold;
        font-size: 15px;
    }
    thead.resizable tr:nth-child(3) td:nth-child(n){
        font-weight: bold;
        font-size: 13px;
    }
</style>