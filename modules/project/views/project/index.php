

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use app\models\Customer;
$this->params['breadcrumbs'][] = 'Project';
?>
<style type="text/css">
/* body {
    padding-top: 50px;
} */
.dropdown.dropdown-lg .dropdown-menu {
    margin-top: -1px;
    padding: 6px 20px;
}
.input-group-btn .btn-group {
    display: flex !important;
}
.btn-group .btn {
    border-radius: 0;
    margin-left: -12px;
}
.btn-group .btn:last-child {
    border-top-right-radius: 4px;
    border-bottom-right-radius: 4px;
}
.btn-group .form-horizontal .btn[type="submit"] {
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
}
.form-horizontal .form-group {
    margin-left: 0;
    margin-right: 0;
}
.form-group .form-control:last-child {
    border-top-left-radius: 4px;
    border-bottom-left-radius: 4px;
}

@media screen and (min-width: 768px) {
    #adv-search {
        width: 500px;
        margin: 0 auto;
    }
    .dropdown.dropdown-lg {
        position: static !important;
    }
    .dropdown.dropdown-lg .dropdown-menu {
        min-width: 500px;
    }
}
</style>

<!-- Custom Theme Style Start Here -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/style.bundle.css">
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/login-6.css">
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/app-table.css">
<!-- <link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/custom-them-default.css"> -->
<!-- Custom Theme Style End Here -->

<script src="<?= Url::base() ?>/web/lib/js/bootstrap/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<div class="">
<div class="row">
<div class="col-md-6 " style="font-size: 30px">
<i class="fa fa-product-hunt" aria-hidden="true"></i> Project
</div>
</div>
<div class="custom-row-view-table">
	<div class="row">
    <div class="" style="    margin-right: 0;">

 <?= Html::button('Add', ['value' => Url::to('project/project/create'), 'class' => 'btn btn-success custom-main-btn',$visibleList['create'], 'id' => 'addModalButton']) ?>

</div>


</div>

<div class="row" style="margin-right: 100px;">

    <div class="col-md-6  margin-right-10" style="">
            <div class="input-group" id="adv-search">
                <input type="text" class="form-control" name="searchtext" placeholder="Search" />
                <div class="input-group-btn">
                    <div class="btn-group" role="group">
                        <div class="dropdown dropdown-lg" style="position: relative;">
                            <button type="button" class="custom-main-btn btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu" style="position: absolute;
    left: -463px;
    top: 40px;
    max-width: 500px;">
                                <form class="form-horizontal" role="form" method="post" action="<?php echo $searchurl;?>">
                                  <div class="form-group">
                                    <label for="filter">Project Type</label>
                                    <select class="form-control" id ="project_type" name="project_type">
                                    <?php
                                     
                                     $projectType = [];

foreach ($projectTypeData as $value) {
    $pType =[];
    $pType['id'] = $value['id'];
    $pType=$value['name'];
    $projectType=$pType;
?>
    <option value="<?php echo $value['id']; ?>"><?php echo ($projectType); ?></option>
  <?php } ?>
                                    </select>

                                  </div>
                                  <div class="form-group">
                                    <label for="filter">Building Type</label>
                                    <select class="form-control" id ="building_type" name="building_type">
                                    <?php
                                     
                                     $building_type = [];

foreach ($buildingTypeData as $value) {
    $pType =[];
    $pType['id'] = $value['id'];
    $pType=$value['name'];
    $building_type=$pType;
?>
    <option value="<?php echo $value['id']; ?>"><?php echo ($building_type); ?></option>
  <?php } ?>
                                    </select>
                                    
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Total Area</label>
                                    <input class="form-control" type="text" id =total_area name =total_area />
                                  </div>
                                  <div class="form-group">
                                    <label for="contain">Total Amount</label>
                                    <input class="form-control" type="text" id = total_amount name = total_amount />
                                  </div>
                                  
                                  <button type="submit" style="border-radius: .25rem;" class="btn btn-primary custom-main-btn"><span class="glyphicon glyphicon-search" style="margin-right: 0" aria-hidden="true"></span></button>

                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary custom-main-btn"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
	</div>
</div>
</div>
<!-- </html> -->
<div class="row">
    <!-- <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-wrench" aria-hidden="true"></i> Project
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
       <?= Html::button('Add', ['value' => Url::to('project/project/create'), 'class' => 'btn btn-success',$visibleList['create'], 'id' => 'addModalButton']) ?>
    </div> -->
</div>

<?php
Modal::begin([
    'header' => '<h4>Add Project</h4>',
    'id' => 'addModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalAddContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Project</h4>',
    'id' => 'updateModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalUpdateContent'><div class='loader'></div></div>";
Modal::end();
?>

<div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile ">
<div class="kt-portlet__body kt-portlet__body--fit">
<div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded">

<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'project_code',
                'header' => '<div style="width:120px;">Project Code</div>',
                'value' => function($model, $key, $index) {
                    return $model->project_code;
                    // return Html::a($model->project_code, ['/estimate/project/master', 'id' => $model->id], [ 'data-pjax'=>"0"]);
                },
                'vAlign' => 'middle',
                'format' => 'raw',
                'noWrap' => true
            ],
            [
                'attribute' => 'project_name',
                'header' => '<div style="width:120px;">Project Name</div>',
                'value' => function($model, $key, $index) {
                    return Html::a($model->project_name, ['/project/project/master', 'id' => $model->id], [ 'data-pjax'=>"0"]);
                },
                'vAlign' => 'middle',
                'format' => 'raw',
                'noWrap' => true
            ],
         
            [
                'attribute' => 'client_name',
                'header' => '<div style="width:120px;">Client  Name</div>',
                'value' => function($model, $key, $index) {
                    $assign_clientData =Customer::find()->asArray()->all();
                    $assign_ClientData = [];
                    
                    foreach ($assign_clientData as $value) {
                        $eType =[];
                        $eType['id'] = $value['id'];
                        $eType['customer_fname'] = $value['customer_fname'].' '.$value['customer_lname'];
                        // $eType['customer_lname'] = $value['customer_lname'];
                        $assign_ClientData[]=$eType;
                    }
                    
                    $assign_client = ArrayHelper::map($assign_ClientData, 'id', 'customer_fname');
                    return !empty($assign_client[$model['client_name']])?$assign_client[$model['client_name']]:'-';
                       
                    },
                
            ],
            [
                'attribute' => 'tender_due_date',
                'header' => '<div style="width:180px;">Tender Due Date</div>',
                 'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->tender_due_date, 'php:d-m-Y');
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:200px;','class'=>'skip-export'],
                'headerOptions' =>['class'=>'skip-export'],
                'header' => "Action",
                // 'template' => ' {update}{deletes}',
                'template'=>$actionList,
                'buttons' => [
                  
                    'update' => function($url) {
                        return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'updateModalButton', 'title' => 'Edit']);
                    },
                    'deletes' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', $url, [ 'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],]);
                    },
                    'copy' => function($url) {
                        return Html::a('<span class="fa fa-copy"></span> ', $url, [ 'title' => 'Copy']);
                    },
                    
                    'detailview' => function ($url) {
                        return Html::a(
                                        '<i class="fa fa-eye fa-lg" aria-hidden="true"></i>&nbsp&nbsp;', $url, [
                                    'title' => 'Purchase',
                                    'data-pjax' => 0,
                                        ]
                        );
                    },
                ],
            ],
        ],
    ]);
}
?>        
</div>
</div>
</div>                           
<?= $this->registerJs("
$(function(){
    $('table').addClass('kt-datatable__table');
    $('tr').addClass('kt-datatable__row');
    $('th').addClass('kt-datatable__cell kt-datatable__cell--sort');
    $('td').addClass('kt-datatable__cell');

$('#addModalButton').click(function() {
    $('#addModal').modal('show')
    .find('#modalAddContent')
    .load($(this).attr('value'));
});
});
", View::POS_READY); ?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updateModalButton').click(function(e){
        e.preventDefault();      
        $('#updateModal').modal('show')
        .find('#modalUpdateContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.copyModalButton').click(function(e){
        e.preventDefault();      
        $('#copyModal').modal('show')
        .find('#modalUpdateContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>