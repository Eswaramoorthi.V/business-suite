<?php

use yii\helpers\Html;

$this->title = 'Create Project';
$this->params['breadcrumbs'][] = ['label' => 'Project', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="project-create">
    <?= $this->render('_form', [
        'model' => $model,
        'projectTypeValue'=>$projectTypeValue,
        'projectStatusValue'=>$projectStatusValue,
        'assign_client'=>$assign_client,
        'buildingTypeValue'=>$buildingTypeValue,
        'companyList'=>$companyList,
        'accountTypeValue'=>$accountTypeValue
    ]) ?>

</div>
