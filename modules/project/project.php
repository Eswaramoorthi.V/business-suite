<?php

namespace app\modules\project;

class project extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\project\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
