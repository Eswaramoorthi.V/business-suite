<?php


namespace app\modules\adhocproject\controllers;


use Yii;
use app\models\Document;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use app\controllers\BaseController;

class DocumentController extends BaseController {

public function actionIndex() {
     
          $dataProvider = new ActiveDataProvider([
               
                'query' => Document::find()  
          ]);
            $dataProvider->pagination  = false;
        
            return $this->render('index', ['dataProvider' => $dataProvider,]);
 
    }

    public function actionCreate($id) {

        $projectId = $_REQUEST['id'];

        $model = new Document();
        if ($model->load(Yii::$app->request->post())) {

            $postData = Yii::$app->request->post();

            $filenameDoc = '';
            $uploads = UploadedFile::getInstances($model, 'document');
            if (!empty($uploads)) {
                $path=Yii::$app->params['domain_path']."/web/";
                $documentList = [];
                $dirname = $projectId;
                $Subfolder = "document";
                $folder = 'uploads/project/' . $dirname . '/';
                $filename = 'uploads/project/' . $dirname . '/'.$Subfolder.'/';

                  $directorypath=$path.'uploads/project/' . $dirname . '/'.$Subfolder.'/';
                // mkdir($directorypath,0777,true);

                foreach ($uploads as $key => $upload) {
                    $description = $postData['Document']['description'][$key];
                    if (file_exists($filename)) {
                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                    } else {
                        if (file_exists($folder)) {
                        

                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                        }
                        else{

                       // mkdir('uploads/project/' . $dirname . '/');
                       // mkdir(,true);

                        } 
                        
                    }

                    $upload->saveAs($filenameDoc);
                    $document = new \app\models\Document();

                    $document->project_id = $projectId;
                    $document->description = $description;
                    $document->document = $upload->baseName . '_' . time() . '.' . $upload->extension;

                    if (!$document->save()) {
                        pr($document->getErrors());
                    }
                }
            }

        } else {

            return $this->renderAjax('create', [
                        'model' => $model,
            ]);
        }
    }

   
    public function actionUpdate($id) {
        
        $ID = $_REQUEST['id'];
       
        $DocumentData = Document::find()->where(['id' => $ID])->asArray()->one();
        $projectID = $DocumentData['project_id'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();

            $model->document = $request['current_document'];
            $uploadDocument = UploadedFile::getInstance($model, 'document');
            if (!empty($uploadDocument)) {
                
                if ($model->document = $uploadDocument) {

                    $directoryName = $projectID;
                    $SubfolderName = "document";
                    $folderName = 'uploads/project/' . $directoryName . '/';
                    $filePath = 'uploads/project/' . $directoryName . '/'.$SubfolderName.'/';
                    // pr($filename);
                    if (file_exists($filePath)) {
                          
                        $fileName = $filePath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;
                       
                    } 
                    else {
                        if (file_exists($folderName)) {
                            
                        mkdir('uploads/project/' . $directoryName . '/'.$SubfolderName.'/');
                        $fileName = $filePath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;
                       
                        }
                        
                        else{
                        //mkdir('uploads/project/' . $directoryName . '/');
                        mkdir('uploads/project/' . $directoryName . '/'.$SubfolderName.'/',true);
                        $fileName = $filePath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;
                         
                        } 
                    }
                    $model->document->saveAs($fileName);
                    $model->document = $model->document->baseName . '_' . time() . '.' . $model->document->extension; 
                }
            
            }
            
           
            if (!$model->save()) {
                pr($model->getErrors());
            }
            return $this->redirect(yii::$app->request->referrer."#documents");
        } 
        else {
            return $this->renderAjax('update', [
                        'model' => $model,
            ]);
        }
    }

    protected function findModel($id) {
        if (($model = Document::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDeletes($id) {
        //pr($id);
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer."#documents");
    }

    public function actionDownload($id)
    {

        $download = Document::findOne($id);
        $dirname = $download['project_id'];
        //$filename = $download->document;
       // pr($filename);
        $path = Yii::getAlias('@webroot').'/uploads/project/' . $dirname . '/document/'.$download->document;
        //pr($path);
         if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        } else {
            throw new NotFoundHttpException("can't find {$download->document} file");
        }
    }

}
