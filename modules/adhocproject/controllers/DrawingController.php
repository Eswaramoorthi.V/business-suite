<?php


namespace app\modules\adhocproject\controllers;
use app\controllers\BaseController;

use Yii;
use app\models\Drawing;
use app\models\Project;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

class DrawingController extends BaseController {

    public function actionIndex() {

        return $this->render('index');
    }

    public function actionCreate($id) {

        $projectId = $_REQUEST['id'];

        $model = new Drawing();
        if ($model->load(Yii::$app->request->post())) {

            $postData = Yii::$app->request->post();

            $filenameDoc = '';
            $uploads = UploadedFile::getInstances($model, 'document');
            if (!empty($uploads)) {
                $documentList = [];
                $dirname = $projectId;
                $Subfolder = "drawing";
                $folder = 'uploads/project/' . $dirname . '/';
                $filename = 'uploads/project/' . $dirname . '/'.$Subfolder.'/';
                foreach ($uploads as $key => $upload) {
                    $description = $postData['Drawing']['description'][$key];
                    if (file_exists($filename)) {
                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                    } else {
                        if (file_exists($folder)) {
                        
                        mkdir('uploads/project/' . $dirname . '/'.$Subfolder.'/');
                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                        }
                        else{
                        mkdir('uploads/project/' . $dirname . '/');
                        mkdir('uploads/project/' . $dirname . '/'.$Subfolder.'/');
                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                        } 
                    }

                    $upload->saveAs($filenameDoc);
                    $drawing = new \app\models\Drawing();

                    $drawing->project_id = $projectId;
                    $drawing->description = $description;
                    $drawing->document = $upload->baseName . '_' . time(). '.' . $upload->extension;

                    if (!$drawing->save()) {
                        pr($drawing->getErrors());
                    }
                }
            }
            //return $this->redirect(yii::$app->request->referrer."#drawings");
        } else {

            return $this->renderAjax('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        
        $ID = $_REQUEST['id'];
        $drawingData = Drawing::find()->where(['id'=>$ID])->asArray()->one();
        $projectID = $drawingData['project_id'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();
            $model->document = $request['current_document'];
              $documentUpload = UploadedFile::getInstance($model, 'document');
               if (!empty($documentUpload)) {
                if ($model->document = $documentUpload) {

                    $directoryName = $projectID;
                    $SubfolderName = "drawing";
                    $folderName = 'uploads/project/' . $directoryName . '/';
                    $filepath = 'uploads/project/' . $directoryName . '/'.$SubfolderName.'/';
                    // pr($filename);
                    if (file_exists($filepath)) {

                        $fileName = $filepath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;
                    } 
                    else {
                        if (file_exists($folderName)) {
                            
                        mkdir('uploads/project/' . $directoryName . '/'.$SubfolderName.'/');
                        $fileName = $filepath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;
                        }
                        else{
                        mkdir('uploads/project/' . $directoryName . '/');
                        mkdir('uploads/project/' . $directoryName . '/'.$SubfolderName.'/');
                        $fileName = $filepath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;
                        } 
                    }
                }
            $model->document->saveAs($fileName);
            $model->document = $model->document->baseName . '_' . time() . '.' . $model->document->extension;
            }
            

            if (!$model->save()) {
                pr($model->getErrors());
            }
            return $this->redirect(yii::$app->request->referrer."#drawings");
        } 
        else {
            return $this->renderAjax('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDeletes($id) {
        //pr($id);
        $this->findModel($id)->delete();

      return $this->redirect(Yii::$app->request->referrer."#drawings");
    }
    public function actionDownload($id) 
    { 
         
        $download = Drawing::findOne($id); 
        $dirname = $download['project_id'];
        //$filename = $download['document'];
        $path = Yii::getAlias('@webroot').'/uploads/project/' . $dirname . '/drawing/'.$download->document;
        //pr($path);
         if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        } else {
            throw new NotFoundHttpException("can't find {$download->document} file");
        }
    }
    protected function findModel($id) {
        if (($model = Drawing::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
