<?php


namespace app\modules\adhocproject\controllers;


use Yii;
use app\models\SitePhotos;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use app\controllers\BaseController;

class SitePhotosController extends BaseController {

public function actionIndex() {
     
          $dataProvider = new ActiveDataProvider([
               
                'query' => SitePhotos::find()  
          ]);
            $dataProvider->pagination  = false;
        
            return $this->render('index', ['dataProvider' => $dataProvider,]);
 
    }

    public function actionCreate($id) {

        $projectId = $_REQUEST['id'];

        $model = new SitePhotos();
        if ($model->load(Yii::$app->request->post())) {

            $postData = Yii::$app->request->post();

            $filenameDoc = '';
            $uploads = UploadedFile::getInstances($model, 'document');
            if (!empty($uploads)) {
                $documentList = [];
                $dirname = $projectId;
                $Subfolder = "sitephotos";
                $folder = 'uploads/project/' . $dirname . '/';
                $filename = 'uploads/project/' . $dirname . '/'.$Subfolder.'/';
                //pr($filename);
                foreach ($uploads as $key => $upload) {
                    $description = $postData['SitePhotos']['description'][$key];
                    if (file_exists($filename)) {
                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                    } else {
                        if (file_exists($folder)) {
                        
                        mkdir('uploads/project/' . $dirname . '/'.$Subfolder.'/');
                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                        }
                        else{
                        mkdir('uploads/project/' . $dirname . '/');
                        mkdir('uploads/project/' . $dirname . '/'.$Subfolder.'/');
                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                        } 
                        
                    }

                    $upload->saveAs($filenameDoc);
                    $document = new \app\models\SitePhotos();

                    $document->project_id = $projectId;
                    $document->description = $description;
                    $document->document = $upload->baseName . '_' . time() . '.' . $upload->extension;

                    if (!$document->save()) {
                        pr($document->getErrors());
                    }
                }
            }
            //return $this->redirect(yii::$app->request->referrer."#sitephotos");
        } else {

            return $this->renderAjax('create', [
                        'model' => $model,
            ]);
        }
    }

   
    public function actionUpdate($id) {
        
        $ID = $_REQUEST['id'];
       
        $photosData = SitePhotos::find()->where(['id' => $ID])->asArray()->one();
        $projectID = $photosData['project_id'];
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();

            $model->document = $request['current_document'];
            $uploadDocument = UploadedFile::getInstance($model, 'document');
            if (!empty($uploadDocument)) {
                
                if ($model->document = $uploadDocument) {

                    $directoryName = $projectID;
                    $SubfolderName = "sitephotos";
                    $folderName = 'uploads/project/' . $directoryName . '/';
                    $filePath = 'uploads/project/' . $directoryName . '/'.$SubfolderName.'/';
                    // pr($filename);
                    if (file_exists($filePath)) {
                          
                        $fileName = $filePath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;
                       
                    } 
                    else {
                        if (file_exists($folderName)) {
                            
                        mkdir('uploads/project/' . $directoryName . '/'.$SubfolderName.'/');
                        $fileName = $filePath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;
                       
                        }
                        
                        else{
                        mkdir('uploads/project/' . $directoryName . '/');
                        mkdir('uploads/project/' . $directoryName . '/'.$SubfolderName.'/');
                        $fileName = $filePath . $model->document->baseName . '_' . time() . '.' . $model->document->extension;
                         
                        } 
                    }
                    $model->document->saveAs($fileName);
                    $model->document = $model->document->baseName . '_' . time() . '.' . $model->document->extension; 
                }
            
            }
            
           
            if (!$model->save()) {
                pr($model->getErrors());
            }
            return $this->redirect(yii::$app->request->referrer."#sitephotos");
        } 
        else {
            return $this->renderAjax('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDeletes($id) {
        $this->findModel($id)->delete();
        return $this->redirect(Yii::$app->request->referrer."#sitephotos");
    }
    public function actionDownload($id) 
    { 
         
        $download = SitePhotos::findOne($id); 
        $dirname = $download['project_id'];
        //$filename = $download->document;
       // pr($filename);
        $path = Yii::getAlias('@webroot').'/uploads/project/' . $dirname . '/sitephotos/'.$download->document;
        //pr($path);
         if (file_exists($path)) {
            return Yii::$app->response->sendFile($path);
        } else {
            throw new NotFoundHttpException("can't find {$download->document} file");
        }
    }
    protected function findModel($id) {
        if (($model = SitePhotos::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
