<?php


namespace app\modules\adhocproject\controllers;


use Mpdf\Tag\P;
use Yii;
use yii\web\NotFoundHttpException;
use app\models\QuotationItems;
use app\models\QuotationMaster;
use app\models\QuotationSection;
use app\models\Customer;
use kartik\mpdf\Pdf;
use app\controllers\BaseController;


class QuotationController extends BaseController
{

    /**
     * Lists all QuotationMaster models.
     * @return mixed
     */
//    public function actionIndex() {
//     
//          $dataProvider = new ActiveDataProvider([
//               
//                'query' => QuotationMaster::find()  
//          ]);
//          
//            $dataProvider->pagination  = false;
//        
//            return $this->render('index', ['dataProvider' => $dataProvider,]);
// 
//    }
    /**
     * Displays a single QuotationMaster model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new QuotationMaster model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     */
    public function actionCreate($id)
    {

        $quotationMaster = new QuotationMaster();
        $quotationsection = new QuotationSection();
        $quotationitems = new QuotationItems();
        $projectId = isset($_GET['id']) ? $_GET['id'] : "";
        $projectinfo = \app\models\AdhocProject::find()->where(['id' => $projectId])->asArray()->one();
        $clientId = $projectinfo['customer_id'];
        $getLastRecord = $quotationMaster->find()->orderBy(['id' => SORT_DESC])->asArray()->one();

        if (!empty($getLastRecord)) {
            $quotationId = $getLastRecord['id'] + 1;
        } else {
            $quotationId = 1;
        }

        $quotationNmuber = 'QUO' . '/' . $quotationId . '/' . 'CI' . '/' . date('Y');


        if ($quotationMaster->load(Yii::$app->request->post())) {


            $postData = Yii::$app->request->post();

            pr($postData);

            $quotationMaster->quotation_date = date('Y-m-d', strtotime($postData['QuotationMaster']['quotation_date']));
             $quotationMaster->name = $postData['QuotationMaster']['name'];
            $quotationMaster->project_id = $projectId;
            $quotationMaster->client_id = $clientId;
            $quotationMaster->quotation_number = $quotationNmuber;
            $sectionDatas = $postData['quotationsection'];
            if(!$quotationMaster->save()){
                pr($quotationMaster->getErrors());
            }

            echo $quotationMaster->id;
            die();
            $quotationmasterId = $quotationMaster->id;

            //pr($sectionDatas);

            foreach ($sectionDatas['items'] as $key => $sectItemsDatas) {

                $section = new QuotationSection();
                $section->quotation_master_id = $quotationmasterId;
                $section->section_name = $sectionDatas['section_name'][$key];
                $section->item_total_amount = $sectionDatas['item_total_amount'][$key];
                $section->multiple_item = $sectionDatas['multiple_item'][$key];

                if (!$section->save()) {
                    pr($section->getErrors());
                }

                $sectionItemsCount = (count($sectItemsDatas['description']));
                for ($i = 0; $i < $sectionItemsCount; $i++) {
                    $items = new QuotationItems();
                    $items->master_id = $quotationmasterId;
                    $items->section_id = $section->id;
                    $items->description = $sectItemsDatas['description'][$i];
                    $items->quantity = $sectItemsDatas['quantity'][$i];
                    $items->unit = $sectItemsDatas['unit'][$i];
                    $items->price = $sectItemsDatas['price'][$i];
                    $items->total_amount = $sectItemsDatas['total_amount'][$i];
                    $items->options = isset($sectItemsDatas['options'][$i]) ? $sectItemsDatas['options'][$i] : 0;
//                    isset($sectionDatas['options'][$i])?  $items->options = $sectionDatas['options'][$i]:$items->options=0;

                    if (!$items->save()) {
                        pr($items->getErrors());
                    }
                }
            }
            return Yii::$app->response->redirect(Yii::$app->request->referrer . "#quotation");
        } else {
            return $this->renderAjax('create', ['projectId' => $projectId, 'quotationMaster' => $quotationMaster, 'quotationsection' => $quotationsection, 'quotationitems' => $quotationitems]
            );
        }
    }


    public function actionCopyQuote()
    {

        $quoteId = $_REQUEST['id'];
        $quoteMaster = QuotationMaster::find()->where(['id' => $quoteId])->one();
        $cloneQuote = new QuotationMaster();
        $getLastRecord = QuotationMaster::find()->orderBy(['id' => SORT_DESC])->asArray()->one();
        if (!empty($getLastRecord)) {
            $quotationId = $getLastRecord['id'] + 1;
        } else {
            $quotationId = 1;
        }

        $quotationNmuber = 'QUO' . '/' . $quotationId . '/' . 'CI' . '/' . date('Y');
        foreach ($quoteMaster->attributes as $key => $val) {
            if ($key == 'quotation_number') {
                $cloneQuote->quotation_number = $quotationNmuber;
            } else {
                $cloneQuote->$key = $val;
            }

        }
        // $cloneQuote->attributes = $quoteMaster->attributes;
        $cloneQuote->isNewRecord = true;
        $cloneQuote->id = "";
        //pr($cloneQuote);
        $cloneQuote->save();
        $cloneMasterId = $cloneQuote->id;

        $quoteSection = QuotationSection::find()->where(['quotation_master_id' => $quoteId])->all();
        if (!empty($quoteSection)) {
            foreach ($quoteSection as $quoteSectionVal) {
                $qsecObj = new QuotationSection();
                $sectionId = $quoteSectionVal->id;
                foreach ($quoteSectionVal->attributes as $qkey => $qvalue) {
                    if ($qkey == 'quotation_master_id') {
                        $qsecObj->quotation_master_id = $cloneMasterId;
                    } else {
                        $qsecObj->$qkey = $qvalue;
                    }

                }
                $qsecObj->isNewRecord = true;
                $qsecObj->id = Null;
                $qsecObj->save();
                $clonesectionId = $qsecObj->id;

                $quoteItems = QuotationItems::find()->where(['master_id' => $quoteId, 'section_id' => $sectionId])->all();
                // pr($quoteItems);
                if (!empty($quoteItems)) {
                    foreach ($quoteItems as $quoteItemsVal) {
                        $qItemsObj = new QuotationItems();
                        foreach ($quoteItemsVal->attributes as $qkey => $qvalue) {
                            if ($qkey == 'master_id') {
                                $qItemsObj->master_id = $cloneMasterId;
                            } else if ($qkey == 'section_id') {
                                $qItemsObj->section_id = $clonesectionId;
                            } else {
                                $qItemsObj->$qkey = $qvalue;
                            }

                        }
                        $qItemsObj->isNewRecord = true;
                        $qItemsObj->id = Null;
                        $qItemsObj->save();

                    }

                }

            }
        }


        return Yii::$app->response->redirect(Yii::$app->request->referrer . "#quotation");


    }

    public function updateNew($postData)
    {
 // pr($postData);
        $quotationId = $postData['id'];
        $quotationMaster = new QuotationMaster();
        $projectId = isset($postData['project_id'])? $postData['project_id'] :"";
        $clientId = isset($postData['client_id'])? $postData['client_id'] :"";
        $count = QuotationMaster::find()->where(['parent_master' => $quotationId])->orderBy(['id' => SORT_DESC])->count();
        if (empty($count)) {
            $version = 1;
        } else {
            $version = $count + 1;
        }
        $quotationNmuber = 'QUO' . '/' . $quotationId . "-" . $version . '/' . 'CI' . '/' . date('Y');


        $quotationMaster->quotation_date = date('Y-m-d', strtotime($postData['QuotationMaster']['quotation_date']));
         $quotationMaster->name = $postData['QuotationMaster']['name'];
        $quotationMaster->project_id = $projectId;
        $quotationMaster->client_id = $clientId;
        $quotationMaster->quotation_number = $quotationNmuber;
        $quotationMaster->parent_master = $quotationId;
        $quotationMaster->version_id = $version;
        $quotationMaster->description = $postData['QuotationMaster']['description'];
        $quotationMaster->exclusion_text = $postData['QuotationMaster']['exclusion_text'];
        $quotationMaster->completion_period = $postData['QuotationMaster']['completion_period'];
        $quotationMaster->notes = $postData['QuotationMaster']['notes'];
        $quotationMaster->payment_terms = $postData['QuotationMaster']['payment_terms'];
        $quotationMaster->validity = $postData['QuotationMaster']['validity'];
        $quotationMaster->price = $postData['QuotationMaster']['price'];
        $quotationMaster->signature = $postData['QuotationMaster']['signature'];
        $quotationMaster->subtotal = $postData['QuotationMaster']['subtotal'];
        $quotationMaster->discount_rate = $postData['QuotationMaster']['discount_rate'];
        $quotationMaster->discount_amount = $postData['QuotationMaster']['discount_amount'];
        $quotationMaster->total_amount = $postData['QuotationMaster']['total_amount'];
      //  pr($quotationMaster);

        $sectionDatas = isset($postData['quotationsection'])? $postData['quotationsection'] :"";

        if(!$quotationMaster->save()){
            pr($quotationMaster->getErrors());
        }
        $quotationmasterId = $quotationMaster->id;
        foreach ($sectionDatas['items'] as $key => $sectItemsDatas) {

            $section = new QuotationSection();
            $section->quotation_master_id = $quotationmasterId;
            $section->section_name = $sectionDatas['section_name'][$key];
            $section->item_total_amount = $sectionDatas['item_total_amount'][$key];
            $section->multiple_item = $sectionDatas['multiple_item'][$key];

            if (!$section->save()) {
                pr($section->getErrors());
            }

            $sectionItemsCount = (count($sectItemsDatas['description']));
            for ($i = 0; $i < $sectionItemsCount; $i++) {
                $items = new QuotationItems();
                $items->master_id = $quotationmasterId;
                $items->section_id = $section->id;
                $items->description = $sectItemsDatas['description'][$i];
                $items->quantity = $sectItemsDatas['quantity'][$i];
                $items->unit = $sectItemsDatas['unit'][$i];
                $items->price = $sectItemsDatas['price'][$i];
                $items->total_amount = $sectItemsDatas['total_amount'][$i];
                $items->options = isset($sectItemsDatas['options'][$i]) ? $sectItemsDatas['options'][$i] : 0;
//                    isset($sectionDatas['options'][$i])?  $items->options = $sectionDatas['options'][$i]:$items->options=0;

                if (!$items->save()) {
                    pr($items->getErrors());
                }
            }
        }
        
        return Yii::$app->response->redirect(Yii::$app->request->referrer . "#quotation");

    }


    /**
     * Updates an existing QuotationMaster model.
     * If update is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $quotationmasterId = $id;
        $quotationMaster = $this->findModel($quotationmasterId);
        // pr($quotationMaster);
        // $quotationMaster=QuotationMaster::find()->where(['id'=>$id])->asArray()->all();
        // pr($quotationMaster);
        $prevSectnIds = [];

        foreach ($quotationMaster->section as $secData) {
            pr($section);
            $prevSectnIds[$secData['id']] = $secData['id'];
        }
       
        foreach ($quotationMaster->items as $itemData) {
            $prevItemsIds[$itemData['id']] = $itemData['id'];
        }

        if ($quotationMaster->load(Yii::$app->request->post())) {
            if ($_REQUEST['update'] == 'update_new') {
                // pr("ghgh");
                $this->updateNew($_REQUEST);
            } else {
// pr("bvb");
                $request = Yii::$app->request->post();
                $quotationMaster->quotation_date = date('Y-m-d', strtotime($request['QuotationMaster']['quotation_date']));
               // pr($request);
         $quotationMaster->name = $request['QuotationMaster']['name'];
                if (!$quotationMaster->save()) {
                    pr($quotationMaster->getErrors());
                }
                $sectionDatas = $request['quotationsection'];
                // pr1($sectionDatas);
                $currentSectnIds = (array_flip($sectionDatas['id']));
                // pr1($prevItemsIds);
                //Delete the removed sections from table
                $this->deleteRecords($prevSectnIds, $currentSectnIds, 'section');

// pr($prevSectnIds);
                $updatedItemIds = [];
                foreach ($sectionDatas['items'] as $key => $sectItemsDatas) {

                    $sectionId = isset($sectionDatas['id'][$key]) ? $sectionDatas['id'][$key] : '';

                    if ($sectionId) {
                        $section = QuotationSection::findOne($sectionId);
                    } else {
                        $section = new QuotationSection();
                    }

                    $section->quotation_master_id = $quotationmasterId;
                    $section->section_name = $sectionDatas['section_name'][$key];
                    $section->item_total_amount = $sectionDatas['item_total_amount'][$key];
                    $section->multiple_item = $sectionDatas['multiple_item'][$key];
                    if (!$section->save()) {
                        pr($section->getErrors());
                    }

                    $sectionItemsCount = (count($sectItemsDatas['description']));
                    for ($i = 0; $i < $sectionItemsCount; $i++) {
                        $itemId = isset($sectItemsDatas['id'][$i]) ? $sectItemsDatas['id'][$i] : '';

                        if ($itemId) {

                            $updatedItemIds[$itemId] = $itemId;
                            //
                            $items = QuotationItems::findOne($itemId);
                        } else {
                            $items = new QuotationItems();
                        }
                        $items->master_id = $quotationmasterId;
                        $items->section_id = $section->id;
                        $items->description = $sectItemsDatas['description'][$i];
                        $items->quantity = $sectItemsDatas['quantity'][$i];
                        $items->unit = $sectItemsDatas['unit'][$i];
                        $items->price = $sectItemsDatas['price'][$i];
                        $items->total_amount = $sectItemsDatas['total_amount'][$i];
                        $items->options = isset($sectItemsDatas['options'][$i]) ? $sectItemsDatas['options'][$i] : 0;
                        if (!$items->save()) {
                            pr($items->getErrors());
                        }
                    }
                    //pr($updatedItemIds);
                }

                // pr1($updatedItemIds);
                //Delete the removed items from table
                $this->deleteRecords($prevItemsIds, $updatedItemIds, 'items');
                return Yii::$app->response->redirect(Yii::$app->request->referrer . "#quotation");
            }
        } else {
            return $this->renderAjax('update', [
                'quotationMaster' => $quotationMaster
            ]);
        }


    }


    /**
     * Deletes an existing QuotationMaster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletes($id)
    {
        $this->findModel($id)->delete();
        QuotationSection::deleteAll(['quotation_master_id' => $id]);
        QuotationItems::deleteAll(['master_id' => $id]);

        return Yii::$app->response->redirect(Yii::$app->request->referrer . "#quotation");
    }

    public function actionExportPdf($id)
    {

        $query = QuotationMaster::find()
            ->joinWith('customer')
            ->where(['quotation_master.id' => $id])
            ->asArray()->one();

        $qouteID = $query['id'];
        $customerId = $query['client_id'];
        $quotationId = $query['quotation_number'];
        //pr1($customerId);

        $customerInfo = Customer::find()
            ->where(['customer.id' => $customerId])
            ->asArray()->one();

        $quotationSection = QuotationSection::find()->where(['quotation_master_id' => $qouteID])->asArray()->all();
        //pr1($quotationSection) ;
        $sectionUniqueIds = [];
        foreach ($quotationSection as $key => $sectionData) {
            $sectionUniqueIds[$sectionData['id']] = $sectionData;
        }
        $sectionKeysvalue = array_keys($sectionUniqueIds);
        $quotationItems = \app\models\QuotationItems::find()->where(['section_id' => $sectionKeysvalue])->asArray()->all();

        $quotationContainer = [];
        foreach ($quotationItems as $itemsData) {
            foreach ($sectionUniqueIds as $sectionId => $secData) {
                $quotationContainer[$sectionId]['Section'] = $secData;
            }
            $quotationContainer[$itemsData['section_id']]['Items'][] = $itemsData;
        }
        //pr($query['total_amount']);
        if ($query['total_amount'] != 0) {
            $word = \app\components\Helper::numtowords2($query['total_amount']);
        } else {
            $word = "-";
        }
        $homeurl = Yii::$app->params['domain_path'];

        $items = $quotationContainer;

        $rDate = explode(" ", $query['quotation_date']);
        $date = new \DateTime($rDate[0]);
        $dateQ = $date->format('d-m-Y');

        $content = $this->renderPartial('quotation_pdf', [
            'model' => $query,
            'customerInfo' => $customerInfo,
            'section' => $quotationSection,
            'items' => $quotationContainer,
            'homeurl' => $homeurl,
            'word' => $word,


        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8, // leaner size using standard fonts
            'content' => $content,
            'cssFile' => Yii::$app->params['domain_path'] . '/web/lib/css/pdf.css',
            //'defaultFontSize' => 5,
            'defaultFont' => 'Century Gothic',
            // any css to be embedded if required
            'marginTop' => 30,
            'cssInline' => 'table, .pdfquotes_data, .pdfquotes_ger {font-size:13px !important; font-family:"century";} .text-bold{font-family:"century-b";} strong{font-family:"century-b !important"; font-weight:"normal !important"} ',
            'format' => Pdf::FORMAT_A4,
            'destination' => Pdf::DEST_DOWNLOAD,
            'filename' => isset($quotationId) ? $quotationId . '.' . 'pdf' : 'BOQ' . '.' . 'pdf',
            'options' => [
                'defaultheaderline' => 0,  //for header

            ],
            'methods' => [
                'SetHeader' => ("<div style='width:60%;display:inline; text-align:left;' ><img src='" . $homeurl . "/web/lib/img/pdf_logo.png' width='100px'/></div><div style='display:inline;align=right'>" . $query['quotation_number'] . "<br>" . $dateQ . "</div>"),
                'SetFooter' => ('
<table >
    <tr>
       <td width="20%"  style="font-weight:bold; text-align:left">Amtrex Technical Service LLC</td>
        <td width="10%" style="text-align: right;font-weight:bold;">{PAGENO}</td>
        
        <td width="20%"  style="font-weight:bold; text-align:center"><span style=" color: white">&nbsp; Contenthide</span> Tel: +971 4 312 4859,+971 4 312 4860 Fax: +971 312 4802 P.Box : 128783, Dubai , UAE</td>
    </tr>
</table>')],

        ]);

        return $pdf->render();
    }

    /**
     * Finds the QuotationMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return QuotationMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = QuotationMaster::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function deleteRecords($previousIds, $currentIds, $table)
    {
        $deletedIds = array_diff_key($previousIds, $currentIds);
        // pr($deletedIds);
        if ($deletedIds) {
            if ($table == "items") {

                //Get all the Sections IDs before Delete the Items for the respective
                $sectionIdsbeforeDel = QuotationItems::find()->select('section_id')->where(['id' => $deletedIds])->asArray()->all();
                $sectionnIdsBD = [];
                foreach ($sectionIdsbeforeDel as $sectionIdsBDData) {
                    $sectionIdsBD[$sectionIdsBDData['section_id']] = $sectionIdsBDData['section_id'];
                }
                QuotationItems::deleteAll(['id' => $deletedIds]);

                //Get all the Sections IDs again again after Delete the Items now by section to see atleast one entry for each section
                $sectionIdsafterDel = QuotationItems::find()->select('section_id')->where(['section_id' => $sectionIdsBD])->asArray()->all();
                $sectionIdsAD = [];
                foreach ($sectionIdsafterDel as $sectionIdsADData) {
                    $sectionIdsAD[$sectionIdsADData['section_id']] = $sectionIdsADData['section_id'];
                }
                //Compare before after Sections ids to find the section ids for delete
                $sectionIdstoDelete = array_diff_key($sectionIdsBD, $sectionIdsAD);
                if ($sectionIdstoDelete) {
                    QuotationSection::deleteAll(['id' => $sectionIdstoDelete]);
                }

            } else if ($table == "section") {
                QuotationSection::deleteAll(['id' => $deletedIds]);
            }
        }
    }

}
