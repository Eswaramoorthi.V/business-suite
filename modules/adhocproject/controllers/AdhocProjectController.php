<?php

namespace app\modules\adhocproject\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use app\models\AdhocProject;
use app\models\Search\AdhocProjectSearch;
use app\models\Search\AwardedprojectSearch;
use app\models\QuotationMaster;
use app\models\QuotationSection;
use app\models\QuotationItems;
use app\models\Document;
use app\models\Drawing;
use app\controllers\BaseController;

/**
 * EquipmentGroupController implements the CRUD actions for EquipmentGroup model.
 */
class AdhocProjectController extends BaseController
{

    public function actionIndex()
    {

        $searchModel = new AdhocProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize' => 50];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAwardedIndex()
    {

        $searchModel = new AwardedprojectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize' => 50];
        return $this->render('awarded_index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


//     public function actionCopyQuote()
//     {
//         $projectId = $_GET['id'];
//         $model = Project::find()->where(['id' => $projectId])->one();
//         $clone = new Project();
//         $clone->attributes = $model->attributes;
//         $clone->isNewRecord = true;
//         $clone->id = null;
//         $clone->save();
//         $copyProjectId = $clone->id;

//         $quoteMaster = QuotationMaster::find()->where(['project_id' => $projectId])->one();
//         if (!empty($quoteMaster)) {

//             // $quoteMasterData = QuotationMaster::find()->where(['id'=>$quoteMasterVal->id])->one();

//             $clone1 = new QuotationMaster;
//             $clone1->setAttributes($quoteMaster->attributes);

//             $clone1->project_id = $copyProjectId;
//             $clone1->client_id = $quoteMaster['client_id'];
//             $clone1->quotation_number = $quoteMaster['quotation_number'];
//             $clone1->isNewRecord = true;
//             $clone1->id = null;
//             $clone1->save();
//             $quote_masterId = $clone1->id;
//             $quoteSec = QuotationSection::find()->where(['quotation_master_id' => $quote_masterId])->all();
//             // pr1($quoteMasterData->attributes);
//             if (!empty($quoteSec)) {
//                 foreach ($quoteSec as $key => $quoteSecVal) {

//                     $clone2 = new QuotationSection();
//                     $clone2->attributes = $quoteSecVal->attributes;
//                     $clone2->isNewRecord = true;
//                     $clone2->id = null;
//                     $clone2->quotation_master_id = $copyProjectId;
//                     $clone2->save();
//                     $sec_id = $clone2->id();
//                 }
//             }
//             $quoteItem = QuotationItems::find()->where(['master_id' => $projectId])->all();
//             if (!empty($quoteItem)) {
//                 foreach ($quoteItem as $key => $quoteItemVal) {

//                     $clone3 = new QuotationItems();
//                     $clone3->attributes = $quoteItemVal->attributes;
//                     $clone3->isNewRecord = true;
//                     $clone3->id = null;
//                     $clone3->master_id = $quote_masterId;
//                     $clone3->section_id = $sec_id;
//                     $clone3->save();

//                 }
//             }
//         }
//     }
// }


    public function actionCopyQuotedProject()
    {
        $projectId = $_GET['id'];

        $projectData = AdhocProject::find()->where(['id' => $projectId])->one();
        $cloneProject = new AdhocProject();
        $getLastRecord=AdhocProject::find()->orderBy(['id' => SORT_DESC])->asArray()->one();
        $PCode='PC/';
        if(!empty($getLastRecord)){
            $autoId=$getLastRecord['id']+1;
        }else{
            $autoId=1;
        }
        $strpadnumber=str_pad($autoId, 5, '0', STR_PAD_LEFT);
// pr($strpadnumber);
        $project_code=$PCode.$strpadnumber;

        foreach ($projectData->attributes as $key=>$value){
            if($key=='project_name'){
//                echo "ASd";
//                die();
                $cloneProject->$key = "Copy-".$value;
            }else if($key=='project_code'){
                $cloneProject->$key = $project_code;
            }
            else{
                $cloneProject->$key=$value;
            }

        }
        $cloneProject->isNewRecord = true;
        $cloneProject->id = Null;

        $cloneProject->save();
        $copyprojectId = $cloneProject->id;
        $documentdata = Document::find()->where(['project_id' => $projectId])->all();
        if(!empty($documentdata)){
            foreach ($documentdata as $documents) {

                $document = new Document();
                foreach ($documents->attributes as $dockey=>$docValue){
                    if($dockey=='project_id'){
                        $document->$dockey=$copyprojectId;
                    }else{
                        $document->$dockey=$docValue;
                    }
                }
                $document->isNewRecord = true;
                $document->id = Null;
                if (!$document->save()) {
                    pr($document->getErrors());
                }
            }
        }



        $drawingdata = Drawing::find()->where(['project_id' => $projectId])->all();
        if(!empty($drawingdata)){
            foreach ($drawingdata as $drawings) {

                $drawing = new Drawing();
                foreach ($drawings->attributes as $drawkey=>$drawValue){
                    if($drawkey=='project_id'){
                        $drawing->$drawkey=$copyprojectId;
                    }else{
                        $drawing->$drawkey=$docValue;
                    }
                }
                $drawing->isNewRecord = $drawValue;
                $drawing->id = Null;
                if (!$drawing->save()) {
                    pr($drawing->getErrors());
                }
            }
        }



        $sitePhotosdata = \app\models\SitePhotos::find()->where(['project_id' => $projectId])->all();
        if(!empty($sitePhotosdata)){
            foreach ($sitePhotosdata as $sitePhoto) {

                $sitePhotoObj = new \app\models\SitePhotos();
                foreach ($sitePhoto->attributes as $skey=>$svalue){
                    if($skey=='project_id'){
                        $sitePhotoObj->$skey=$copyprojectId;
                    }else{
                        $sitePhotoObj->$skey=$svalue;
                    }
                }
                $sitePhotoObj->isNewRecord = $drawValue;
                $sitePhotoObj->id = Null;
                if (!$sitePhotoObj->save()) {
                    pr($drawing->getErrors());
                }
            }
        }
        $quoteMaster = QuotationMaster::find()->where(['project_id' => $projectId])->all();
       //pr($quoteMaster);
        foreach ($quoteMaster as $quotes) {

            $quoteId=$quotes->id;
            $getLastRecord = QuotationMaster::find()->orderBy(['id' => SORT_DESC])->asArray()->one();
            if (!empty($getLastRecord)) {
                $quotationId = $getLastRecord['id'] + 1;
            } else {
                $quotationId = 1;
            }
            $quotationNmuber = 'QUO' . '/' . $quotationId . '/' . 'CI' . '/' . date('Y');

            $cloneQuote = new QuotationMaster();
            //pr($quotes);
            foreach ($quotes->attributes as $qKey=>$qvalue){
                if ($qKey == 'quotation_number') {
                    $cloneQuote->$qKey = $quotationNmuber;

                }elseif ($qKey=='project_id'){
                    $cloneQuote->project_id = $copyprojectId;
                } else {
                    $cloneQuote->$qKey = $qvalue;
                }
            }

            $cloneQuote->isNewRecord = true;
            $cloneQuote->id = "";
           // pr($cloneQuote);

            if(!$cloneQuote->save()){
                pr($cloneQuote->getErrors());
            }
            $cloneMasterId = $cloneQuote->id;

            $quoteSection = QuotationSection::find()->where(['quotation_master_id' => $quoteId])->all();
            if (!empty($quoteSection)) {
                foreach ($quoteSection as $quoteSectionVal) {
                    $qsecObj = new QuotationSection();
                    $sectionId = $quoteSectionVal->id;
                    foreach ($quoteSectionVal->attributes as $qkey => $qvalue) {
                        if ($qkey == 'quotation_master_id') {
                            $qsecObj->quotation_master_id = $cloneMasterId;
                        } else {
                            $qsecObj->$qkey = $qvalue;
                        }

                    }
                    $qsecObj->isNewRecord = true;
                    $qsecObj->id = Null;
                    $qsecObj->save();
                    $clonesectionId = $qsecObj->id;

                    $quoteItems = QuotationItems::find()->where(['master_id' => $quoteId, 'section_id' => $sectionId])->all();
                    // pr($quoteItems);
                    if (!empty($quoteItems)) {
                        foreach ($quoteItems as $quoteItemsVal) {
                            $qItemsObj = new QuotationItems();
                            foreach ($quoteItemsVal->attributes as $qkey => $qvalue) {
                                if ($qkey == 'master_id') {
                                    $qItemsObj->master_id = $cloneMasterId;
                                } else if ($qkey == 'section_id') {
                                    $qItemsObj->section_id = $clonesectionId;
                                } else {
                                    $qItemsObj->$qkey = $qvalue;
                                }

                            }
                            $qItemsObj->isNewRecord = true;
                            $qItemsObj->id = Null;
                            $qItemsObj->save();

                        }

                    }

                }
            }

        }

return $this->redirect(['index']);
    }


    public function actionDetailview()
    {
        $id = $_GET['id'];

        $loaInfo = \app\models\Loa::find()
            ->where(['project_id' => $id])
            ->groupBy('project_id')
            ->select('sum(loa_amount) as loaAmount')
            ->asArray()->all();

        $voInfo = \app\models\Variation::find()
            ->where(['project_id' => $id])
            ->groupBy('project_id')
            ->select('sum(vo_amount) as voAmount')
            ->asArray()->all();

        $invoiceInfo = \app\models\InvoiceMaster::find()
            ->where(['project_id' => $id])
            ->groupBy('project_id')
            ->select('sum(subtotal) as totalAmount')
            ->asArray()->all();
        $expenseInfo1 = \app\models\Services::find()
            ->where(['project_id' => $id])
            ->groupBy('project_id')
            ->select('sum(totalcost_working) as servicetotalAmount')
            ->asArray()->all();
        $expenseInfo2 = \app\models\Transaction::find()
            ->where(['project_id' => $id])
            ->andwhere(['type' => 2])
            ->groupBy('project_id')
            ->select('sum(expense_amount) as materialtotalAmount')
            ->asArray()->all();
        $expenseInfo3 = \app\models\SubContractor::find()
            ->where(['project_id' => $id])
            ->groupBy('project_id')
            ->select('sum(sub_total) as contractortotalAmount')
            ->asArray()->all();
        $discountInfo = \app\models\SubContractor::find()
            ->where(['project_id' => $id])
            ->groupBy('project_id')
            ->select('sum(discount_amount) as discounttotalAmount')
            ->asArray()->all();
        //pr($expenseInfo2);

        return $this->render('detail_view', [
            'model' => $this->findModel($id),
            'invoiceInfo' => $invoiceInfo,
            'expenseInfo1' => $expenseInfo1,
            'expenseInfo2' => $expenseInfo2,
            'expenseInfo3' => $expenseInfo3,
            'loaInfo' => $loaInfo,
            'discountInfo' => $discountInfo,
            'voInfo' => $voInfo,
        ]);
    }

    public function actionCreate()
    {
        $model = new AdhocProject();
        $ptype = Yii::$app->request->get('ptype');
        // $ptype = $_GET['ptype'];
        if ($model->load(Yii::$app->request->post())) {
            
            $request=Yii::$app->request->post();
            $model->project_name=$request['AdhocProject']['project_name'];
             $model->project_code=$request['AdhocProject']['project_code'];
              $model->customer_id=$request['AdhocProject']['customer_id'];
               $model->project_type=$request['AdhocProject']['project_type'];
                 $model->amount=$request['AdhocProject']['amount'];
            $model->lead_code=$request['AdhocProject']['lead_code'];
                $model->description=$request['AdhocProject']['description'];
            $ptype = Yii::$app->request->post('ptype');
            $model->start_date = date('Y-m-d', strtotime($model['start_date']));
            $model->end_date = date('Y-m-d', strtotime($model['end_date']));
            if ($ptype == 0) {
                $model->status = 0;
            }
            $model->save();
            if ($ptype == 0) {
                return $this->redirect(['index']);
            } else {
                return $this->redirect(['awarded-index']);
            }
        } else {
            $model->project_type = $ptype;
            return $this->renderAjax('create', [
                'model' => $model,
                'ptype' => $ptype,
            ]);
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();
            $ptype = Yii::$app->request->post('ptype');
            $Data = $request['AdhocProject'];
            $model->start_date = date('Y-m-d', strtotime($Data['start_date']));
            $model->end_date = date('Y-m-d', strtotime($Data['end_date']));
            $model->amount = $Data['amount'];

            
            $model->project_name=$Data['project_name'];
             $model->project_code=$Data['project_code'];
              $model->customer_id=$Data['customer_id'];
               $model->project_type=$Data['project_type'];
                
            $model->lead_code=$Data['lead_code'];
                $model->description=$Data['description'];
            if ($ptype == 0) {
                $model->status = 0;
            }
            if (!$model->save()) {
                pr($model->getErrors());
            }
            if ($ptype == 0) {
                return $this->redirect(['index']);
            } else {
                return $this->redirect(['awarded-index']);
            }
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeletes($id)
    {
        $ptype = Yii::$app->request->get('ptype');
        $this->findModel($id)->delete();
        if ($ptype == 0) {
            return $this->redirect(['index']);
        } else {
            return $this->redirect(['awarded-index']);
        }
    }

    protected function findModel($id)
    {
        if (($model = AdhocProject::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionProjectGroup()
    {
        $customerId = $_REQUEST['id'];
        $list = AdhocProject::find()->where(['customer_id' => $customerId])->asArray()->all();
        //pr($list);
        $details = ArrayHelper::map($list, 'id', 'project_name');
        echo "<option value=''>Select Project</option>";
        foreach ($details as $id => $name) {
            echo "<option value='$id'>$name</option>";
        }
    }

}
