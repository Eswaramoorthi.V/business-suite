<?php

namespace app\modules\adhocproject\controllers;

use app\models\Lead;
use app\models\Search\LeadSearch;
use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use app\models\Project;
use app\models\ProjectSearch;
use app\models\AwardedprojectSearch;
use app\models\QuotationMaster;
use app\models\QuotationSection;
use app\models\QuotationItems;
use app\models\Document;
use app\models\Drawing;
use app\controllers\BaseController;
/**
 * EquipmentGroupController implements the CRUD actions for EquipmentGroup model.
 */
class LeadController extends BaseController
{

    public function actionIndex()
    {

        $searchModel = new LeadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize' => 50];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

  
   public function actionDetailview()
    {
         $id = $_GET['id'];
        return $this->render('detail_view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {

        $model = new Lead();

        if ($model->load(Yii::$app->request->post())) {
  $request=Yii::$app->request->post();
  $model->lead_code=$request['Lead']['lead_code'];
  $model->lead_name=$request['Lead']['lead_name'];
  $model->lead_description=$request['Lead']['lead_description'];
  $model->lead_status_id=$request['Lead']['lead_status_id'];
  $model->lead_type_id=$request['Lead']['lead_type_id'];
  $model->first_name=$request['Lead']['first_name'];
  $model->last_name=$request['Lead']['last_name'];
  $model->phone=$request['Lead']['phone'];
  $model->mobile=$request['Lead']['mobile'];
  $model->fax=$request['Lead']['fax'];
$model->opportunity_amount=$request['Lead']['opportunity_amount'];


            $model->save();

                    if (!$model->save()) {
                        pr($model->getErrors());
                    }
           return $this->redirect(['index']);
          
        } else {

            return $this->renderAjax('create', [
                'model' => $model,

            ]);
        }
    }

    public function actionUpdate($id)
    {

        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {
            $postData=Yii::$app->request->post();

  $model->lead_code=$postData['Lead']['lead_code'];
  $model->lead_name=$postData['Lead']['lead_name'];
  $model->lead_description=$postData['Lead']['lead_description'];
  $model->lead_status_id=$postData['Lead']['lead_status_id'];
  $model->lead_type_id=$postData['Lead']['lead_type_id'];
  $model->first_name=$postData['Lead']['first_name'];
  $model->last_name=$postData['Lead']['last_name'];
  $model->phone=$postData['Lead']['phone'];
  $model->mobile=$postData['Lead']['mobile'];
  $model->fax=$postData['Lead']['fax'];
$model->opportunity_amount=$postData['Lead']['opportunity_amount'];

            if (!$model->save()) {
                pr($model->getErrors());
            }
           
     return $this->redirect(['index']);
          
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDeletes($id)
    {
       
        $this->findModel($id)->delete();
       return $this->redirect(['index']);
        
    }

    protected function findModel($id)
    {
        if (($model = Lead::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    

}
