<?php

use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;



$projectId=$_GET['id'];

$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
$name = $projectname['project_name'];
//pr($name);

?>

<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'class' => 'invoice-form form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
        ])
?>
<div id='msg' class='msg'></div>
<div id="document-form">
    <div class="row">

        
    <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo ucfirst(strtolower($name));?></small></legend></div>
        <div class="col-md-12" input-group-btn style="margin-top:21px">
            <button class="btn btn-success add-more" type="button"><i class="fa fa-gear"></i> Add More Drawings</button>

<div >&nbsp;</div>
        </div>
     <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
        <div class ="row">
             <div class="col-md-6" style="font-weight:bold">Description</div>
             <div class="col-md-6" style="font-weight:bold">Document</div>
            
            

        </div>
     <div >&nbsp;</div>
        <div id="drawingupload">
            <div class="row copyinvitems details after-add-more">
                <div class="control-group">
         <div class="col-md-6"><input type="text" class="form-control" name="Drawing[description][]"></div>
         <div class="col-md-5"><input type="file" class="form-control" name="Drawing[document][]"></div>
                   
                </div>

            </div>
        </div>



        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['id'=>'submit','class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
   
    <?php
    ActiveForm::end();
    ?>
</div>
 </div>
    <?php
    $js = <<< JS

    var counter = 1;
    $(".add-more").click(function () {
        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp;</div><div class="control-group">' +
        '<div class="col-md-6"><input type="text" class="form-control" name="Drawing[description][]"></div>'+
        '<div class="col-md-5"><input type="file" class="form-control" name="Drawing[document][]"></div>'+   
        '<div class="col-md-1 rmbtn" style="margin-top:0px"><button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove" style="font-size:10px; margin-right:0px;"></i></button></div>'+
        '</div>');
        newContainer.appendTo("#drawingupload");
        counter++;
    });

    $('body').on('click','.remove',function(){ 
        $(this).parents('.control-group').remove();
    });

JS;
    $this->registerJs($js);
    ?>

  
<script> 
$("form#active-form").submit(function(e) {
    e.preventDefault(); 
    event.stopPropagation();
    var formData = new FormData(this);
    
    var description = document.getElementsByName('Drawing[description][]');
    var documents = document.getElementsByName('Drawing[document][]');
               
                 $("#error-message").removeClass("hide");
                 for (k=0; k<description.length; k++) {
                    $(description[k]).removeClass("error-highlight");
                    $(documents[k]).removeClass("error-highlight");
                   
                    }
                    for (i=0; i<description.length; i++) {
                        if (description[i].value == '') {
                            $("#error-message").html("<p>Description can not be blank</p>");
                            $("decription[i]").addClass("error-highlight");
                            return false;
                        } 
                        else if (documents[i].value == '') {
                            $("#error-message").html("<p>Document cannot be empty</p>");
                            $(documents[i]).addClass("error-highlight");
                            return false;
                        }
                      }
                $("#error-message").addClass("hide");

    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('Drawing Uploaded');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();
                      window.location.href = window.location.href .split('#')[0] + "#drawings" ;
                      location.reload();


            },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>