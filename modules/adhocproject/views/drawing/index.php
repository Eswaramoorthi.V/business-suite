
<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;

$id=$_GET['id'];
$drawings = app\models\Drawing::find()->where(['id' => $id])->asArray()->one();
$projectId = $drawings['project_id'];

?>
<?php
            
    Modal::begin([
        'header' => '<h4>Update Drawing</h4>',
        'id' => 'updatedrawing',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modaldrawupdate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
Modal::begin([
        'header' => '<h4>Upload Drawings</h4>',
        'id' => 'uploaddrawings',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modaluploaddrawing'><div class='loader'></div></div>";
    Modal::end();
?>
<div class="row">
<div class="col-md-12">

    <p class="text-right" >
        <?=Html::a(Yii::t('app', ' {modelClass}', [
                   'modelClass' => 'Add Drawing',
                    ]), ['drawing/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalDrawingupload']);
?>  </p> 
      <legend class="text-info"><small>Drawings</small></legend>
 </div>
</div>
 <?php

if ($dataProvider) {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],

        [
            'attribute' => 'description',
            'header' => '<div style="width:220px;">Description</div>',
            'value' =>  'description',
           
        ],
        
//        [
//            'attribute' => 'document',
//            'header' => '<div style="width:120px;">Attachment</div>',
//            'value' => 'document',
//            
//        ],
        ['attribute'=>'Attachment',
        'header' => '<div style="width:30px;">Attachment</div>',
        'format'=>'raw',
        'value' => function($model)
        {
        return
        Html::a('<div style="text-align:center; font-size:18px;"><span class="fa fa-download"></span></div>', ['drawing/download', 'id' => $model->id]);

        }
        ],
  
[
                'class' => 'kartik\grid\ActionColumn',
                'mergeHeader' => false,
                'vAlign' => 'middle',
                'header' => '<div style="width:150px;">Actions</div>',
                'template' => ' {update}{deletes}',
                'buttons' => [
                   'update' => function ($url, $model, $key) {
                           
                                return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', Url::to(['drawing/update', 'id' => $model->id]),
                                    [
                                        'class'=>'updatedraw',
                                        'title' => 'Edit',
                                       // 'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                        },
                    'deletes' =>function ($url, $model, $key) {
                          
                                return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', Url::to(['drawing/deletes', 'id' => $model->id]),
                                    [
                                       // 'class'=>'delete',
                                        'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                                       // 'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                        },   
                ],
            ],
       ],         
        
       'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => false, // pjax is set to always true for this demo
    
    'summary' => '',
//    'panel' => [
//        'type' => GridView::TYPE_PRIMARY,
//        'heading' => '',
//    ],
    
    ]);
}

?>  


<?php
$this->registerJs("$(function() {
   $('#modalDrawingupload').click(function(e) {
     e.preventDefault();
     $('#uploaddrawings').modal('show').find('#modaluploaddrawing')
     .load($(this).attr('href'));
   });
});", View::POS_READY);
 ?>

<?= $this->registerJs(

  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updatedraw').click(function(e){
       e.preventDefault();      
       $('#updatedrawing').modal('show')
                  .find('#modaldrawupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
 ?>