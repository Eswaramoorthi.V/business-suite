<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PreventiveMaintenance */

$this->title = "Invoice";
//$this->params['breadcrumbs'][] = ['label' => 'Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-file';




?>
<div class="project-create">

    <?= $this->render('_form', [
        'invoiceMaster' => $invoiceMaster, 
        'invoice' => $invoice
        
    ]) ?>

</div>
