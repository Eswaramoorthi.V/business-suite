<?php


use yii\widgets\ActiveForm;
use yii\helpers\Html;

 $invoiceMaster->invoice_date = date('d-m-Y', strtotime($invoiceMaster['invoice_date']));
 $invoiceMaster->due_date = date('d-m-Y', strtotime($invoiceMaster['due_date']));
 //pr1($invoiceMaster->invoice_date);
$category = []; //ArrayHelper::map(\app\models\CategoryMaster::getPayement(), 'id', 'description');
$vatcategory=\Yii::$app->params['VATRate'];
$customInvoiceType=\Yii::$app->params['customInvoiceType'];
$getLastRecord=$invoiceMaster->find()->orderBy(['id' => SORT_DESC])->asArray()->one();
//pr1($getLastRecord);
if(!empty($getLastRecord)){
    $invoiceId=$getLastRecord['id']+1;
}else{
    $invoiceId=1;
}
  
?>


<div class="customer-form form-horizontal">
    <?php $form = ActiveForm::begin(['id'=>'invoice-form']); ?>
       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
         <div class="row">
        <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> Invoice </small></legend></div>
         </div>
   <div class ="row">   
   <div class="col-md-4">
           <?=
            $form->field($invoiceMaster, 'invoice_name', [
                'template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput()->label('Invoice Name')
            ?> 
        </div>
       
        <div class="col-md-2">
            <?=
            $form->field($invoiceMaster, 'invoice_date', ['template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput()->label('Date')
            ?> 
        </div>
       <div class="col-md-2">
            <?=
            $form->field($invoiceMaster, 'due_date', ['template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput([])->label('Due Date')
            ?> 
        </div>
        
        <div class="col-md-4">
        <?=
        $form->field($invoiceMaster, 'notes', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textarea(['rows'=>3])->label('Remarks')
        ?>
    </div>
       
    </div>

   





<div>&nbsp;</div>  
<div class="col-md-12"><legend class="text-info"><small>Items</small></legend></div>
<div class ="row">
    <div class="col-md-5" style="font-weight:bold">Description</div>
    <div class="col-md-1" style="font-weight:bold">Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Unit</div>
    <div class="col-md-2" style="font-weight:bold">Price</div>
    <div class="col-md-2" style="font-weight:bold">Total</div>
    <div class="col-md-1" style="font-weight:bold">&nbsp;</div>
</div>
<div id="product">
<?php foreach ($invoiceMaster->invoice as $key =>$invoice) { ?>



<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-5"><textarea id="invoice-description" class="form-control required descrip" name="Invoice[description][]"><?php echo $invoice->description;?></textarea></div>
    <div class="col-md-1"><input type="text" value="<?php echo $invoice->quantity; ?>" id="quantity-0<?php echo $invoice->id; ?>" class="form-control qnty" name="Invoice[quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" id="invoice-unit" class="form-control required unit" name="Invoice[unit][]"></div>
    <div class="col-md-2"><input type="text" value="<?php echo number_format($invoice->price, 2, '.' ,''); ?>" id="price-0<?php echo $invoice->id; ?>" class="form-control" name="Invoice[price][]" onchange="calc(this.id);"></div>
    <div class="col-md-2"><input type="text" class="form-control totalamount" readOnly = "true" value="<?php echo number_format($invoice->total_amount, 2, '.', '');?>" id="totalamount-0<?php echo $invoice->id; ?>" name="Invoice[total_amount][]" onchange="calc(this.id);"></div>
    <?php if($key!=0){ ?>
    <div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn  new-item-remove remove" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>
    <?php } ?>
    <input type="hidden" value="<?php echo $invoice->id;?>" name="Invoice[id][]"> 
  </div>
</div>

<div class="row">&nbsp;</div> 
<?php } ?>
</div>



   <div class="col-md-12" input-group-btn style="margin-top:21px">
       <div class="row">
 <div class="col-md-2">
        <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> ADD</button>
 </div>
            <div class="col-md-6">
                 <label>Custom Invoice
       <input type="hidden" class="performa" name="InvoiceMaster[custom_Invoice]" value="<?php if(isset($invoiceMaster->custom_Invoice)) { echo $invoiceMaster->custom_Invoice; }?>">
       <input type="checkbox" class="performa-Invoice"  id="status"  <?php if(isset($invoiceMaster->custom_Invoice)) { if(($invoiceMaster->custom_Invoice == 1)){  echo "checked"; } } ?>  onclick="this.previousSibling.value=<?php if(($invoiceMaster->custom_Invoice == 1)){  echo "0"; } else{ echo"1";}?>-this.previousSibling.value"  onchange="calc(this.id, '0');">
        </label></div> 
       </div> 
    </div>




<div class="status-box hide">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class='col-md-4 col-xs-12'>
                <?=
                $form->field($invoiceMaster, 'custom_Invoice_type', [
                    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
                ])->dropDownList($customInvoiceType, [
                    'class' => 'select from control',
                    'id' => 'customInvoicetype',
                    'onchange' => 'calc(this.id);',
                ])->label('Payment Mode')
                ?>
            </div>
            <div class='col-md-7 col-xs-12'>
                <div id="forWorkdoneAmount" class="show">
                    <?= $form->field($invoiceMaster, 'workdone_amount')->textInput(["class" => "form-control" , 'placeholder' => '0.00', 'id' => 'workdone_amount', 'onchange' => 'calc(this.id);',])->label('&nbsp;') ?>
                </div>
                <div id="forAdvancePercentage" class="hide">
                    <?= $form->field($invoiceMaster, 'advance_percentage')->textInput(["class" => "form-control" , 'placeholder' => '0.00' , 'id' => 'advance_percentage', 'onchange' => 'calc(this.id);'])->label('&nbsp;') ?>
                </div>
                <div id="forProgressPercentage" class="hide">
                    <?= $form->field($invoiceMaster, 'progress_percentage')->textInput(["class" => "form-control" , 'placeholder' => '0.00' , 'id' => 'progress_percentage', 'onchange' => 'calc(this.id);'])->label('&nbsp;') ?>
                </div>
            </div>
            <?=
                    $form->field($invoiceMaster, 'previous_certified_amount', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'id' => 'certified_amount',
                         'placeholder' => '0.00',
                'class' => 'form-control',
                'onchange' => 'calc(this.id);',
                    ])->label('Previous Certified')
                    ?>


            <?=
            $form->field($invoiceMaster, 'contra_charges', [
                'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
            ])->textInput([
                'id' => 'contra_charges',
                 'placeholder' => '0.00',
                'class' => 'form-control',
                'onchange' => 'calc(this.id);',
                
                ])->label('Contra Charges')
            ?>
                     <?=
            $form->field($invoiceMaster, 'retention_percentage', [
                'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
            ])->textInput([
                'id' => 'retention_amount',
                'placeholder' => '0.00',
                'class' => 'form-control',
                'onchange' => 'calc(this.id);',
                
                ])->label('Retention(%)')
            ?>
            <?=
            $form->field($invoiceMaster, 'advance_recovery_percentage', [
                'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
            ])->textInput([
                'id' => 'advance_recovery',
                'placeholder' => '0.00',
                'class' => 'form-control',
                'onchange' => 'calc(this.id);',

            ])->label('Advance Recovery(%)')
            ?>
        </div>
        <div>&nbsp;</div>

    </div>
</div>
<div>&nbsp;</div>
<div class="col-md-12"><legend class="text-info"><small></small></legend></div>


<div class="invoice-box">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <div class='col-md-12  padding-0'>
            <div class='col-md-3 col-xs-12 padding-0'>
                <?= $form->field($invoiceMaster, 'subtotal_label')->textInput(["class" => "form-control text-bold"])->label('&nbsp;') ?>
            </div>
            <div class='col-md-7 col-xs-12'>
            <?=
            $form->field($invoiceMaster, 'subtotal', [
                'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label',]
            ])->textInput(
                    [   
                        'id' => 'subtotal',
                        'readOnly' => true, 
                        'placeholder' => '0.00',
                        'class' => 'form-control subtotaltotal',
                        'onchange' => 'calc(this.id);',
                    ]
                    )->label('&nbsp;')
            ?>
            </div>
            </div>
            <div class="col-md-12"><legend class="text-info"><small></small></legend></div> 
            <?=
                    $form->field($invoiceMaster, 'vat', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        // 'class'=>'select from control',
                        'value'=>$vatcategory,
                        'readOnly' => true,
                        'placeholder' => '',
                        'id' => 'vat',
                        'onchange' => 'calc(this.id);',
                    ])->label('VAT(%)')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

            <?=
            $form->field($invoiceMaster, 'total_amount', [
                'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
            ])->textInput([
                'readOnly' => true, 
                'placeholder' => '0.00',
                'id' => 'totalamount',
                 //'class' => 'form-control total'
                
                ])->label('Grand Total')
            ?>
        </div>
        <div>&nbsp;</div>

    </div>
</div>
<div>&nbsp;</div>
<div class="col-md-12"><legend class="text-info"><small></small></legend></div>
<div class="col-md-12"> 
    <div class="form-group">
        <div class="col-md-12 col-xs-12">
<?= Html::submitButton($invoiceMaster->isNewRecord ? 'Submit' : 'Update', ['class' => $invoiceMaster->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
<?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
</div>
</div>
</div>
<?php
ActiveForm::end();
?>


<?php 
$js = <<< JS
        
    var counter = 1;
    $(".add-more").click(function () {
        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp</div><div class="control-group col-md-12 padding-0">' +
        '<div class="col-md-5"><textarea id="invoice-description" class="form-control" name="Invoice[description][]"></textarea></div>'+
        '<div class="col-md-1"><input type="text" value="0" id="quantity-' + counter + '" class="form-control" name="Invoice[quantity][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1"><input type="text" id="invoice-unit" class="form-control required unit" name="Invoice[unit][]"></div>'+
        '<div class="col-md-2"><input type="text" value="0.00" id="price-' + counter + '" class="form-control" name="Invoice[price][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-2"><input type="text" class="form-control totalamount" value="0.00" id="totalamount-' + counter + '" class="form-control" name="Invoice[total_amount][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn new-item-remove remove" onclick="calc(this.id);" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>'+
        '<div class="clear">&nbsp</div>'+
            '</div>');
        newContainer.appendTo("#product");
        counter++;
    });

    

     $('#invoicemaster-invoice_date').datepicker({
       format: 'dd-mm-yyyy'
    });
   
    $('#invoicemaster-due_date').datepicker({
       format: 'dd-mm-yyyy'
    });       
JS;
$this->registerJs($js);

?>

<script>
    $( document ).ready(function() {
        var customInvoiceType = $('#customInvoicetype').val();

        if(customInvoiceType == 1)
        {
            $('#forWorkdoneAmount').removeClass('show').addClass('hide');
            $('#forProgressPercentage').removeClass('show').addClass('hide');
            $('.field-certified_amount').hide();
            $('.field-contra_charges').hide();
            $('.field-retention_amount').hide();
            $('.field-advance_recovery').hide();
            $('#forAdvancePercentage').removeClass('hide').addClass('show');
        }
        else if(customInvoiceType == 2)
        {
            $('#forWorkdoneAmount').removeClass('show').addClass('hide');
            $('#forAdvancePercentage').removeClass('show').addClass('hide');
            $('#forProgressPercentage').removeClass('hide').addClass('show');
            $('.field-certified_amount').hide();
            $('.field-contra_charges').hide();
            $('.field-retention_amount').hide();
            $('.field-advance_recovery').hide();
        }
        else
        {
            $('#forWorkdoneAmount').removeClass('hide').addClass('show');
            $('#forAdvancePercentage').removeClass('show').addClass('hide');
            $('#forProgressPercentage').removeClass('show').addClass('hide');
            $('.field-certified_amount').show();
            $('.field-contra_charges').show();
            $('.field-retention_amount').show();
            $('.field-advance_recovery').show();
        }
    });
    $('#customInvoicetype').on('change',function() {
        var customInvoiceType = $(this).val();

        if(customInvoiceType == 1)
        {
            $('#forWorkdoneAmount').removeClass('show').addClass('hide');
            $('#forProgressPercentage').removeClass('show').addClass('hide');
            $('#workdone_amount').val('');
            $('#progress_percentage').val('');
            $('.field-certified_amount').hide().find(':input').val('');
            $('.field-contra_charges').hide().find(':input').val('');
            $('.field-retention_amount').hide().find(':input').val('');
            $('.field-advance_recovery').hide().find(':input').val('');
            $('#forAdvancePercentage').removeClass('hide').addClass('show');
            $('#invoicemaster-subtotal_label').val('Advance %');
        }
        else if(customInvoiceType == 2)
        {
            $('#forWorkdoneAmount').removeClass('show').addClass('hide');
            $('#forAdvancePercentage').removeClass('show').addClass('hide');
            $('#forProgressPercentage').removeClass('hide').addClass('show');
            $('#workdone_amount').val('');
            $('#advance_percentage').val('');
            $('.field-certified_amount').hide().find(':input').val('');
            $('.field-contra_charges').hide().find(':input').val('');
            $('.field-retention_amount').hide().find(':input').val('');
            $('.field-advance_recovery').hide().find(':input').val('');
            $('#invoicemaster-subtotal_label').val('Progress %');
        }
        else
        {
            $('#forWorkdoneAmount').removeClass('hide').addClass('show');
            $('#forAdvancePercentage').removeClass('show').addClass('hide');
            $('#forProgressPercentage').removeClass('show').addClass('hide');
            $('#advance_percentage').val('');
            $('#progress_percentage').val('');
            $('.field-certified_amount').show();
            $('.field-contra_charges').show();
            $('.field-retention_amount').show();
            $('.field-advance_recovery').show();
            $('#invoicemaster-subtotal_label').val('Net Amount');
        }
    });
    $('#updateinvoice').on('hidden.bs.modal', function () {
        location.reload();
        window.history.replaceState(null, null, '#invoice');
    });
</script>
<script> 
$('#invoicemaster-invoice_date').on('change',function() 
{ 
          var invoiceDate = $('#invoicemaster-invoice_date').val();
          var dateArray = invoiceDate.split("-"); //valuestart.replace(/-/g,"/");
          var formattedDate = dateArray[1]+"-"+dateArray[0]+"-"+dateArray[2];
          var newdate = new Date(formattedDate);
          
           newdate.setDate(newdate.getDate() + 7);
    
           var dd = newdate.getDate();
           var mm = newdate.getMonth() + 1;
           var y = newdate.getFullYear();

           var dueDate = (dd < 10 ? '0' + dd : '' + dd) + '-' + (mm < 10 ? '0' + mm : '' + mm)+ '-' + y;
         
          $('#invoicemaster-due_date').val(dueDate);
  
  });
  </script>  
<script>
    function calc(id) {
        if(id!='remove'){
            var idPosition = id.split('-');
            var price = Number($("#price-"+idPosition[1]).val()).toFixed(2)
            $("#price-"+idPosition[1]).val(price);
            var total = $("#quantity-"+idPosition[1]).val()*price;
            var tot = Number(total).toFixed(2); 
            $("#totalamount-"+idPosition[1]).val(tot)
        }

        if($('.performa-Invoice').is(':checked')) {
            var type = $("#customInvoicetype").val();
            var forpreamount = 0;
            $(".totalamount").each(function () {
                forpreamount += parseFloat($(this).val());
            });
            var presubtotal = Number(forpreamount).toFixed(2);

            var progressPercentage = $("#progress_percentage").val();
            var progressAmount = presubtotal * progressPercentage / 100;

            var advancePercentage = $("#advance_percentage").val();
            var advancAmount = presubtotal * advancePercentage / 100;

            var workdoneAmount = Number($("#workdone_amount").val()).toFixed(2);
            var certifiedAmount = Number($("#certified_amount").val()).toFixed(2);
            var contraAmount = Number($("#contra_charges").val()).toFixed(2);
            var rententionAmount = workdoneAmount * $("#retention_amount").val() / 100;
            var advancerecoveryAmount = workdoneAmount * $("#advance_recovery").val() / 100;
            var total = parseFloat(certifiedAmount) + parseFloat(contraAmount) + parseFloat(rententionAmount) + parseFloat(advancerecoveryAmount);
            var netAmount = workdoneAmount - total;
            if (type == 0){
                $("#subtotal").val(netAmount);
            }else if (type == 1){
                $("#subtotal").val(advancAmount);
            } else {
                $("#subtotal").val(progressAmount);
            }
        }
         else{
            var subTotal = 0;
            $(".totalamount").each(function () {
                subTotal += parseFloat($(this).val());
            });
            var subTotalFormatted = Number(subTotal).toFixed(2); 
            $("#subtotal").val(subTotalFormatted);
        }
            
            var subtotal = Number($("#subtotal").val()).toFixed(2)
            $("#subtotal").val(subtotal);
            var totamntbeforeVat;
            totamntbeforeVat = subtotal;
            var discrate = $("#discountrate").val();
            if(discrate>0) {
                var discamount = $("#discountrate").val()*subtotal/100;
                var discountformatted = Number(discamount).toFixed(2)
                $("#discountamount").val(discountformatted);
                totamntbeforeVat = subtotal- discountformatted;
            } 
            

            //console.log("Before vat amout",totamntbeforeVat);
            var vat = Number($("#vat").val()).toFixed(2)
            $("#vat").val(vat);
            var total, vatAmount;
            
            if(vat>0) {
                   vatAmount = totamntbeforeVat*(vat)/100;
            } else {
                   vatAmount = 0;
            }
            
            total = parseFloat(totamntbeforeVat) + parseFloat(vatAmount);
            //console.log("Vat value",vat);
            //console.log("Total amount",total);
            var tot = Number(total).toFixed(2);
            $("#totalamount").val(tot);
            
        }
        $('body').on('click','.remove',function(){ 
        $(this).parents('.control-group').parent().remove();
        calc('remove');
        });
          $("form#invoice-form").submit(function(e) {
                var decription = document.getElementsByName('Invoice[description][]');
                var quantity = document.getElementsByName('Invoice[quantity][]');
                var price = document.getElementsByName('Invoice[price][]');
                
           $("#invoicemaster-invoice_name").removeClass("error-highlight");
           $("#invoicemaster-invoice_date").removeClass("error-highlight");
                
            if($('#invoicemaster-invoice_name').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Invoice Name cannot be Empty</p>");
            $("#invoicemaster-invoice_name").addClass("error-highlight");
            return false;
            }
      
            else if($('#invoicemaster-invoice_date').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Date cannot be Blank</p>");
            $("#invoicemaster-invoice_date").addClass("error-highlight");
            return false;
            }
            else if($('.performa-Invoice').is(':checked')){

                if( $("#customInvoicetype").val()=="0" & $("#workdone_amount").val()==""){
                    $("#error-message").removeClass("hide");
                    $("#error-message").html("<p>Total Value Of Work Done cannot be Empty</p>");
                    $("#workdone_amount").addClass("error-highlight");

                    return false;
                } else if( $("#customInvoicetype").val()=="1" & $("#advance_percentage").val()==""){
                    $("#error-message").removeClass("hide");
                    $("#error-message").html("<p>Total Value Of Advance Percentage cannot be Empty</p>");
                    $("#advance_percentage").addClass("error-highlight");

                    return false;
                } else if( $("#customInvoicetype").val()==" "  ){
                    $("#error-message").removeClass("hide");
                    $("#error-message").html("<p>Total Value Of Custom Invoice type cannot be Empty</p>");
                    $("#customInvoicetype").addClass("error-highlight");

                    return false;
                }
            }
             
                
                $("#error-message").removeClass("hide");
                for (k=0; k<decription.length; k++) {
                    $(decription[k]).removeClass("error-highlight");
                    $(quantity[k]).removeClass("error-highlight");
                    $(price[k]).removeClass("error-highlight");
                }
                    for (i=0; i<decription.length; i++) {
                        if (decription[i].value == '') {
                            $("#error-message").html("<p>Description can not be blank</p>");
                            $(decription[i]).addClass("error-highlight");
                            return false;
                        } else if (quantity[i].value == 0) {
                            $("#error-message").html("<p>Quantity can not be 0 value</p>");
                            $(quantity[i]).addClass("error-highlight");
                            return false;
                        }else if (price[i].value == 0) {
                            $("#error-message").html("<p>Price can not be 0 value</p>");
                            $(price[i]).addClass("error-highlight");
                            return false;
                        }
                      }
                $("#error-message").addClass("hide");
        
            });


</script>

 <script>
        if($('.performa-Invoice').is(':checked')){
                $('.status-box').removeClass('hide').addClass('show');
                $('.add-more').hide();
             }
             else{
                  $('.status-box').removeClass('show').addClass('hide');
                    $('.add-more').show();
             }
       $(document).on("click", ".performa-Invoice", function () {
    
            if($(this).is(':checked')){
                $('.status-box').removeClass('hide').addClass('show');
                $('.add-more').hide();
                  $('.performa').val(1);
                
             }
             else{
                  $('.status-box').removeClass('show').addClass('hide');
                    $('.add-more').show();
                    $('.performa').val(0);
                     $('.status-box').find(':input').val('');
                $('#customInvoicetype').val(0);
             }
           
       });
       
       
    

      </script>