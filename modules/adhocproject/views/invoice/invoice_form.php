   <?php
  use yii\helpers\Html;
  use app\models\Country;
  $countryDetails = Country::find()->where(['id'=>$customerInfo['country']])->asArray()->one();
 //pr1($countryDetails);
 $country=$countryDetails['nicename'];
 //pr($country);
   ?>

        <div >
            <div class="text-center"> <?php //echo Html::img('@web/lib/img/pdf_logo.png') ?></div>
             <table width="100%" >
<!--                <tr>
                                <td class="text-center" >
                                    <?php echo Html::img('@web/lib/img/pdf_logo.png') ?> </td></tr>
                <tr>-->
                    <td>&nbsp;</td>
                </tr>
                
                <tr>
                                <td  class="text-center text-bold" style="color:#355879"> <?php echo "TRN:100389925700003" ?><br>
                                </td>
                            </tr>
                             <tr>
                    <td>&nbsp;</td>
                </tr>
                            <tr>
                                <td class="text-center text-bold" style="font-size: 20px;text-decoration:underline" >
                                        <?php    echo "TAX INVOICE"; ?>
                                </td>
                            </tr> 
                
            </table> 
            <div> &nbsp;</div>
                  <div class="new_head">
            <div class="left_top">
                <div class="text-bold">CLIENT:</div>

                <?php  $address = str_replace(",","<br>",$customerInfo['address']);
                   $salutation=['1'=>'Mr','2'=>'Mrs','3'=>'Ms'];
                ?>
                <div class="left_top_2"><div class="text-bold"><?php echo !empty($customerInfo['company_name'])?'M/S'.' '.$customerInfo['company_name']:($salutation[$customerInfo['salutation']]).' . '.($customerInfo['coustomer_fname'] . " " . $customerInfo['coustomer_lname']) ?></div>
                    <?php echo $address; ?><br>

                         <?php if(!empty($customerInfo['trn_number'])){?>
                    <span class="text-danger">TRN: <?php echo $customerInfo['trn_number']; }?></span></div>
            </div> 
    
                    <div class="right_top">
                         <table class="table table-bordered" cellspacing="0">
                <tbody>
                    <tr> <td class="text-center text-bold"  style="padding: 2px;width:122px"> Invoice No.
                                </td>
                                <td class="text-center text-bold"  style="padding: 2px;width:30px">
                                 <?php echo $model['invoice_number']; ?>
                                </td>
                            </tr>
                </tbody>
                        </table>
                                
                            <table class="table table-bordered" cellspacing="0">
                <tbody>
                            <tr>
                                <td class="text-center text-bold"  style="padding: 2px;width:17px">
                                    Transaction Date:
                                </td>
                                <td class="text-center text-bold"  style="padding: 2px;width:30px" >
                                   <?php $rDate = explode(" ", $model['invoice_date']);
                                 $date = new DateTime($rDate[0]);
                                 echo $date->format('d-m-Y');
                                ?>
                                </td>
                            </tr>
                </tbody>
                        </table>

                    </div> 
                    </div>

          
                        
                        
              
    <div>&nbsp;</div>
    
    <div class="text-center text-bold"> <?php echo $model['invoice_name']; ?> </div>
    
     <table class="table table-bordered " cellspacing="1">
                <tbody>
                    
                <tr class="rowheader">
                    <td width="5%" class="text-center text-bold" style="padding: 2px">SN</td>
                    <td width="50%" class="text-center text-bold" style="padding: 2px">DESCRIPTION</td>
                    <td width="5%" class="text-center text-bold" style="padding: 2px">QTY</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">UNIT</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">PRICE</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">AMOUNT</td>
                </tr>
                  <?php foreach($productInfo as $key => $product){ ?>
           <tr>
                <td class="text-center"> <?php echo $key+1;?> </td>
                <td class="text-left"> <?php echo $product['description'];?></td>
                <td class="text-center"> <?php echo $product['quantity'];?></td>
                <td class="text-center"> <?php echo $product['unit'];?></td>
                <td class="text-center"> <?php echo number_format($product['price'], 2, '.', ',');?></td>
                <td class="text-right"> <?php echo number_format($product['total_amount'], 2, '.', ',');?></td>
                 </tr>
               <?php } ?>
                 
                 <?php if($model['custom_Invoice']==1){?>

                     <?php if($model['custom_Invoice_type']== '0'){?>
                         <tr class="">
                             <td style="border: 0" colspan="3"> </td>
                             <td class="text-center text-bold" colspan="2">Work Executed Value</td>
                             <td class="text-right text-bold"><?php echo number_format($model['workdone_amount'], 2, '.', ','); ?></td>
                         </tr>
                     <?php } ?>


                <?php if($model['previous_certified_amount']!= ''){?>
                <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2">Previous Certified</td>
                    <td class="text-right"><?php echo number_format($model['previous_certified_amount'], 2, '.', ','); ?></td>
                </tr>
                     <?php } ?>
                <?php if($model['contra_charges']!= ''){?>
             <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2">Contra Charges</td>
                    <td class="text-right"><?php echo number_format($model['contra_charges'], 2, '.', ','); ?></td>
                </tr>
                     <?php } ?>
                <?php if($model['retention_amount']!= '0'){?>
                 <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2">Retention</td>
                    <td class="text-right"><?php echo number_format($model['retention_amount'], 2, '.', ','); ?></td>
                </tr>
                    <?php } ?>
                     <?php if($model['advance_recovery_amount']!= '0'){?>
                         <tr class="">
                             <td style="border: 0" colspan="3"> </td>
                             <td class="text-center" colspan="2">Advance Recovery</td>
                             <td class="text-right"><?php echo number_format($model['advance_recovery_amount'], 2, '.', ','); ?></td>
                         </tr>
                     <?php } ?>
                <tr class="">
                      <td style="border: 0" colspan="3"> </td>
                      <td class="text-center text-bold" colspan="2"><?php if($model['custom_Invoice_type']== '1'){?><?php echo  ""; ?> <?php } ?><?php if($model['custom_Invoice_type']== '2'){?><?php echo  $model['progress_percentage']; ?>% <?php } ?><?php if($model['subtotal_label']== 'Advance %'){ echo "Advance";}else{ echo $model['subtotal_label'];} ?></td>
                      <td class="text-right text-bold"><?php echo number_format($model['subtotal'], 2, '.', ','); ?></td>
                </tr>
                <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2">5% VAT</td>
                    <td class="text-right"><?php echo number_format($model['vat_amount'], 2, '.', ','); ?></td>
                </tr>
             <tr class="rowfooter">
                     <td class="text-center text-bold" colspan="3" style="padding: 1px"><?php echo ucfirst($word); ?></td>  
                      <td class="text-center text-bold" colspan="2" style="padding: 1px"> Grand Total </td>  
                      <td class="text-right text-bold" style="padding: 1px"><?php echo number_format($model['total_amount'], 2, '.', ','); ?></td>
                </tr>
                 <?php } else { ?>
                 <tr class="">
                      <td style="border: 0" colspan="3"> </td>
                      <td class="text-center text-bold" colspan="2"><?php if($model['custom_Invoice_type']== '1'){?><?php echo ""; ?> <?php } ?><?php if($model['custom_Invoice_type']== '2'){?><?php echo  $model['progress_percentage']; ?>% <?php } ?><?php if($model['subtotal_label']== 'Advance %'){ echo "Advance";}else{ echo $model['subtotal_label'];} ?></td>
                      <td class="text-right text-bold"><?php echo number_format($model['subtotal'], 2, '.', ','); ?></td>
                </tr>
                <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2">5% VAT</td>
                    <td class="text-right"><?php echo number_format($model['vat_amount'], 2, '.', ','); ?></td>
                </tr>
             <tr class="rowfooter">
                     <td class="text-center text-bold" colspan="3" style="padding: 1px"><?php echo ucfirst($word);?></td>  
                      <td class="text-center text-bold" colspan="2" style="padding: 1px"> Grand Total </td>  
                      <td class="text-right text-bold" style="padding: 1px"><?php echo number_format($model['total_amount'], 2, '.', ','); ?></td>
                </tr>
                 <?php } ?>
            
                </tbody>
              </table>
    
     <table>
                <tbody>
                      <tr><td class="text-left text-bold" colspan="6" >   Payment Information: </td> </tr>
                               
                            <tr>
                                <td class="text-center" colspan="6">
                                    Kindly make the payment by Tele Transfer or Cheque.
                                </td>

                            </tr>
                            <tr>
                                <td style="width:20%">
                                    Account Details
                                </td>
                                <td class="align" colspan="4">
                                    :  Jeyamcrm LLC
                                </td>
                                
                            </tr>
                            <tr>
                                <td style="width:20%">
                                    Bank 
                                </td>
                                <td class="align" colspan="4">
                                    :  RAK
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%">
                                    Branch    
                                </td>
                                <td class="align" colspan="4"> 
                                    :  Al-Qusais-Branch
                              </td>
                            </tr>
                            <tr>
                                <td style="width:20%">
                                    IBAN             
                                </td>
                                <td class="align" colspan="4">
                                    :  AE780409865653434<br>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:20%">
                                    Swift Code
                                </td>
                                <td class="align" colspan="4">
                                    :  NRAKAEAK
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:left;" colspan="5">
                                   
                                </td>

                            </tr>
                            <tr>
                              <td class="text-left" colspan="6"> <div  ><span style="visibility: hidden;"> We hope the above meets</span>
                                    If you have any question concerning this invoice,Kindly contact us within 5 days from the receipt of this invoice. 
                                  </div></td>

                            </tr>

                 </tbody>
            </table>

            <table width="100%"><tbody>
                   <tr width="100%">
                    <td class="text-left text-bold" colspan="6">
                        <br>
                       <div class="text-right text-bold"> </div><br>
                    <div > Prepared By: </div><br>
                    <div class="text-left text-bold"><?php echo  ' ' //Html::img('@web/lib/img/signature.png') ?> </div><br><br><br><br><br>
                       <div class="text-left text-bold">Accounts Department </div>
                        
                    </td> 
                    <td> </td>
                    <td class="text-right text-bold" colspan="6">
                        <br>
                       <div class="text-right text-bold"> </div><br>
                      <div > Received by:</div><br>
                      <div class="text-right text-bold">  </div><br><br><br><br><br>
                       <div class="text-right text-bold" ><?php echo!empty($customerInfo['company_name']) ? $customerInfo['company_name'] : $customerInfo['coustomer_fname'] . " " . $customerInfo['coustomer_lname'].'&nbsp;&nbsp;'; ; ?> </div>
                        
                    </td>
                   </tr>
                   
                  <!--  <tr >
                       <td colspan="6"><div class="pull-left" style="visibility: hidden">dgfdgk</div></td>
                        <td colspan="6"><div class="pull-right" style="visibility: hidden">ftytjj</div></td>
                   </tr> -->

            </tbody>
            </table>
       
</div>    
<!-- <link rel="stylesheet" href="../lib/css/pdf.css"> -->

<!-- <link rel="stylesheet" href="../lib/css/bootstrap/bootstrap.min.css"> -->   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
           