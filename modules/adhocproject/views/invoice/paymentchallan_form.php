
<?php
use yii\helpers\Html;
?>
    
    <html>  
  <body>
<div class="new_recipt">
<div class="new_recipt_head"><?php echo Html::img('@web/lib/img/pdf_logo.png',['width'=>'100']) ?></div>
<div class="new_recipt_head text-bold" style="text-decoration:underline">RECEIPT VOUCHER</div>
<div class="new_recipt_head_body_1"><div class="borderbox-text-1 text-bold"> RV # : </div> <div class="borderbox"><?php echo isset($model['receipt_number']) ? $model['receipt_number'] : $invoiceInfo['invoice_number']; ?></div></div>
<div class="new_recipt_head_body_1"><div class="borderbox-text-2 text-bold">Transaction Date: </div><div class="borderbox"><?php $rDate = explode(" ",$model['payment_date']);  $date = new DateTime($rDate[0]);
                                    echo $date->format('d-m-Y');?></div></div>
</div>
<table class='main-table' style='width:100%'>
          <tbody>
                <tr >
                           <td class="name text-bold " style='width:23%;'>
                                
                                Received from:
                               
                           </td>
                            
                             <td class="value"  style='text-align:left; width:77%;' colspan='2'>
                                
                                 <?php echo !empty($customerInfo['company_name'])?($customerInfo['company_name']):$customerInfo['coustomer_fname'] . " " . $customerInfo['coustomer_lname'] . " " . $customerInfo['coustomer_mid_name']; ?> 
                                 <div><hr></div> 
                             </td>
                        </tr>
          </tbody>
</table>
<table class='main-table' style='width:100%'>
    <tbody>
                        <tr  >
                            <td class="name text-bold " style='width:10%;'>
                             Amount:   
                            </td>
                            <td class="value"  style='text-align:left; width:35%;' colspan='2'>
                                <?php echo 'AED '.number_format($model['paid_amount'], 2, '.', ','); ?>
                                <div><hr></div> 
                            </td>

                            <td class="name text-bold " style='width:20%;'>Mode of Payment:</td>
                            <td class="value"  style='text-align:left; width:30%;' colspan='2'>   <?php $paymentTypes = \Yii::$app->params['paymentTypes']; echo $paymentTypes[$model['payment_method']]; ?>
                                <div><hr></div>
                            </td>
                        </tr>
    </tbody>
</table>
<table class='main-table' style='width:100%'>
    <tbody>
                        <tr  >
                            <td class="name text-bold " style='width:20%;'>
                             Amount in Words:
                            </td>
                            <td class="value"  style='text-align:left; width:77%;' colspan='2'>
                              <?php echo ucfirst($word)?>
                               <div><hr></div> 
                            </td>
                        </tr>
    </tbody>
</table>


<?php  if($model['payment_method'] == 2){ ?>
<table class='main-table' style='width:100%'>
    <tbody>
                       <tr>
                           <td class="name text-bold " style='width:10%;'>
                               Bank:
                           </td>

                           <td class="value"  style='text-align:left; width:50%;' colspan='2'>

                               <?php echo $model['bank_account']; ?>
                               <div><hr></div>
                           </td>

                           <td class="name text-bold " style='width:18%;text-align:right;'>
                               &nbsp;Cheque # / Date:
                           </td>
                           <td class="value"  style='text-align:left; width:50px;' colspan='2'>
                                
                               <?php echo $model['cheque_number']; ?> / <?php $rDate = explode(" ", $model['cheque_date']);
                               $date = new DateTime($rDate[0]);
                               echo $date->format('d-m-Y');?>
                                 <div><hr></div> 
                            </td>

                          </tr>

    </tbody>
</table>
                            <?php   } ?>

<?php  if($model['payment_method'] == 3){ ?>
<table class='main-table' style='width:100%'>
    <tbody>
                          <tr>

                              <td class="name text-bold " style='width:10%;'>
                              Bank:
                            </td>

                              <td class="value"  style='text-align:left; width:77%;' colspan='2'>
                                
                              <?php echo $model['bank_account']; ?>
                               <div><hr></div>   
                            </td>
                         
                           </tr>
    </tbody>
</table>
                            <?php   } ?>
<table class='main-table' style='width:100%'>
    <tbody>
                           <tr>
                               <td class="name text-bold " style='width:10%;'>
                              Towards:
                            </td >
                               <td class="value"  style='text-align:left; width:77%;' colspan='2'>&nbsp;
                                <?php echo ucfirst($model['description']); ?>
                                <div><hr></div> 
                             </td>
                             
                         
                           </tr>
<!--                           <tr>-->
<!--                               <td class="name text-bold " style='width:23%;'>-->
<!--                              -->
<!--                            </td>-->
<!--                               <td class="value"  style='text-align:left; width:77%;' colspan='2'>-->
<!--                                <div>&nbsp;</div>-->
<!--                                <div><hr></div> -->
<!--                             </td>-->
<!--                             -->
<!--                         -->
<!---->
<!--            </tr>-->
<!--            -->
     </tbody>
    
</table>

<table class="table-new">
    <tr>
        <?php  if($model['payment_method'] == 1){
            $heightpx = '90px';
        }  else {  $heightpx = '50px'; }?>

        <td height="<?php echo $heightpx; ?>" class="text-bold">&nbsp;</td>
    </tr>
</table>

<table  class="sign-table" >
    <tr>
        <td width ="500"></td>
        <td class="r-s">Authorised Signature<br><br>
            <b> Jeyamcrm LLC</b></td>
    </tr>

</table>
          

</body>
</html>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    