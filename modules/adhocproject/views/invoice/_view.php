<?php


use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\web\View;
?>

<div class="asset-master-view">
 
    

   
        <div class="col-sm-5">
            <table class="table table-bordered2 table-condensed small kv-table">
                <tbody><tr class="danger">
                    <th colspan="6" class="text-center text-danger">Invoice Details</th>
                </tr>
               
                 <tr>
                     
                     <td class="text-left" colspan="6" style="border: 0; border-width: 0;"> <div class="text-right">  <strong>Invoice #:</strong><?php echo $model['invoice_number']; ?><br>
                             Invoice Date: <?php $rDate = explode(" ", $model['invoice_date']);
                                 $date = new DateTime($rDate[0]);
                                 echo $date->format('d-m-Y');
                                ?></div>
                         <div class="left_top">
                <div class="text-bold">CLIENT:</div>

                <?php  $address = str_replace(",","<br>",$customerInfo['address']);
                   $salutation=['1'=>'Mr','2'=>'Mrs','3'=>'Ms'];
                ?>
                <div class="address_box"><div class="text-bold"><?php echo isset($customerInfo['company_name'])?'M/S'.' '.$customerInfo['company_name']:($salutation[$customerInfo['salutation']]).' . '.($customerInfo['coustomer_fname'] . " " . $customerInfo['coustomer_lname']) ?></div>
                    <?php echo $address; ?><br>

                         <?php if(!empty($customerInfo['trn_number'])){?>
                    <span class="text-danger">TRN: <?php echo $customerInfo['trn_number']; }?></span></div>
            </div> 
                         <div class="pull-right">
                   <button type="button" class="btn btn-default" onclick="location.href='<?php echo Url::to(['invoice/invoice', 'id' =>  $model['id']]);?>'"  title="Print Invoice" data-toggle="tooltip"><span class="fa fa-print"></span></button>
            </div>
                       </td>
                
                   
                </tr>
                <tr><td>
                  <table class="table table-bordered1 ">
                <tbody>
                    
                <tr class="rowheader">
                    <td width="5%" class="text-center text-bold" style="padding: 2px">S.No.</td>
                    <td width="50%" class="text-center text-bold" style="padding: 2px">DESCRIPTION</td>
                    <td width="5%" class="text-center text-bold" style="padding: 2px">QTY</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">UNIT</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">PRICE</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">AMOUNT</td>
                </tr>
                  <?php foreach($productInfo as $key => $product){ ?>
           <tr>
                <td class="text-center"> <?php echo $key+1;?> </td>
                <td class="text-left"> <?php echo $product['description'];?></td>
                <td class="text-center"> <?php echo $product['quantity'];?></td>
                <td class="text-center"> <?php echo $product['unit'];?></td>
                <td class="text-center"> <?php echo number_format($product['price'], 2, '.', ',');?></td>
                <td class="text-right"> <?php echo number_format($product['total_amount'], 2, '.', ',');?></td>
                 </tr>
               <?php } ?>
                <?php if($model['custom_Invoice']==1){?>
                    <?php if($model['custom_Invoice_type']== '0'){?>
                    <tr class="">
                        <td style="border: 0" colspan="3"> </td>
                        <td class="text-center text-bold" colspan="2">Work Executed Value</td>
                        <td class="text-right text-bold"><?php echo number_format($model['workdone_amount'], 2, '.', ','); ?></td>
                    </tr>
                    <?php } ?>



                    <?php if($model['previous_certified_amount']!= ''){?>
                    <tr class="">
                        <td style="border: 0" colspan="3"> </td>
                        <td class="text-center" colspan="2">Previous Certified</td>
                        <td class="text-right"><?php echo number_format($model['previous_certified_amount'], 2, '.', ','); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if($model['contra_charges']!= ''){?>
                    <tr class="">
                        <td style="border: 0" colspan="3"> </td>
                        <td class="text-center" colspan="2">Contra Charges</td>
                        <td class="text-right"><?php echo number_format($model['contra_charges'], 2, '.', ','); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if($model['retention_amount']!= '0'){?>
                    <tr class="">
                        <td style="border: 0" colspan="3"> </td>
                        <td class="text-center" colspan="2">Retention</td>
                        <td class="text-right"><?php echo number_format($model['retention_amount'], 2, '.', ','); ?></td>
                    </tr>
                    <?php } ?>
                    <?php if($model['advance_recovery_amount']!= '0'){?>
                        <tr class="">
                            <td style="border: 0" colspan="3"> </td>
                            <td class="text-center" colspan="2">Advance Recovery</td>
                            <td class="text-right"><?php echo number_format($model['advance_recovery_amount'], 2, '.', ','); ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="">
                        <td style="border: 0" colspan="3"> </td>
                        <td class="text-center text-bold" colspan="2"><?php if($model['custom_Invoice_type']== '1'){?><?php echo  $model['advance_percentage']; ?>% <?php } ?><?php if($model['custom_Invoice_type']== '2'){?><?php echo  $model['progress_percentage']; ?>% <?php } ?><?=$model['subtotal_label']; ?></td>
                        <td class="text-right text-bold"><?php echo number_format($model['subtotal'], 2, '.', ','); ?></td>
                    </tr>
                    <tr class="">
                        <td style="border: 0" colspan="3"> </td>
                        <td class="text-center" colspan="2">5% VAT</td>
                        <td class="text-right"><?php echo number_format($model['vat_amount'], 2, '.', ','); ?></td>
                    </tr>
                    <tr class="rowfooter">
                        <td class="text-center text-bold" colspan="3" style="padding: 1px"><?php echo ucfirst( (round($model['total_amount']))).' '.'dirhams only' ?></td>
                        <td class="text-center text-bold" colspan="2" style="padding: 1px"> Grand Total </td>
                        <td class="text-right text-bold" style="padding: 1px"><?php echo number_format($model['total_amount'], 2, '.', ','); ?></td>
                    </tr>
                <?php } else { ?>
                    <tr class="">
                        <td style="border: 0" colspan="3"> </td>
                        <td class="text-center text-bold" colspan="2"><?php if($model['custom_Invoice_type']== '1'){?><?php echo  $model['advance_percentage']; ?>% <?php } ?><?php if($model['custom_Invoice_type']== '2'){?><?php echo  $model['progress_percentage']; ?>% <?php } ?><?=$model['subtotal_label']; ?></td>
                        <td class="text-right text-bold"><?php echo number_format($model['subtotal'], 2, '.', ','); ?></td>
                    </tr>
                    <tr class="">
                        <td style="border: 0" colspan="3"> </td>
                        <td class="text-center" colspan="2">5% VAT</td>
                        <td class="text-right"><?php echo number_format($model['vat_amount'], 2, '.', ','); ?></td>
                    </tr>
                    <tr class="rowfooter">
                        <td class="text-center text-bold" colspan="3" style="padding: 1px"><?php echo ucfirst( (round($model['total_amount']))).' '.'dirhams only' ?></td>
                        <td class="text-center text-bold" colspan="2" style="padding: 1px"> Grand Total </td>
                        <td class="text-right text-bold" style="padding: 1px"><?php echo number_format($model['total_amount'], 2, '.', ','); ?></td>
                    </tr>
                <?php } ?>
            
                </tbody>
              </table>
                    </td></tr>   
            </tbody></table>
         
        </div>
        <div class="col-sm-7">
            <table class="table table-bordered table-condensed table-hover small kv-table">
                <tbody><tr class="success">
                    <th colspan="9" class="text-center text-success">Payment History</th>
                </tr>
                <tr class="active">
                    <th class="text-center">#</th>
                    <th> Payment Date</th>
                    <th class="text-right">Payment Method</th>
                    <th class="text-right">Cheque Number</th>
                    <th class="text-right">Cheque Date</th>
                    <th class="text-right">Bank</th>
                    <th class="text-right">Amount</th>
                    <th class="text-right">Balance</th>
                    <th class="text-right">Receipt</th>
                </tr>
                
                 <?php 
                 $paidtemp =0;
                 foreach($paymentInfo as $key=>$payment){ ?>
                <tr>
                    <td class="text-center"> <?php echo $key+1;?></td>
                    <td><?php $rDate = explode(" ", $payment['payment_date']); $payment_date = new DateTime($rDate[0]);echo $payment_date->format('d-m-Y'); ?></td>
                    <td class="text-right"><?php $paymentTypes = \Yii::$app->params['paymentTypes'];  $ptypes = (isset($paymentTypes[$payment['payment_method']]))? $paymentTypes[$payment['payment_method']]:''; echo $ptypes;?></td>
                    <td class="text-right"><?php echo !empty($payment['cheque_number'])? $payment['cheque_number']:'-'; ?></td>
                    <td class="text-right"><?php $rDate = explode(" ", $payment['cheque_date']);
                                    $date = new DateTime($rDate[0]);
                                    echo !empty($payment['cheque_number']) ?  $date->format('d-m-Y') : '-' ;?>
                                   </td>
                    <td class="text-right"><?php echo !empty($payment['cheque_number']) ?$payment['bank_account']:'-'; ?></td>
                   <?php $paidtemp=$paidtemp+ $payment['paid_amount'];
                         $balanceamt = $model['total_amount']-$paidtemp; ?>
                    <td class="text-right"><?php echo number_format($payment['paid_amount'], 2, '.', ','); ?></td>
                     <td class="text-right"><?php echo number_format($balanceamt, 2, '.', ','); ?></td>
                      <td class="text-right">
                          <button type="button"  class="btn recipitbtn btn-action"  onclick="location.href='<?php echo Url::to(['invoice/paymentchallan', 'id' =>  $payment['id']]);?>'" title='Print'><span class="fa fa-print"></span></button>
                      
                          <button type="button"  class="btn recipitbtn btn-action" onclick="location.href='<?php echo Url::to(['invoice/deletereceipt', 'id' =>  $payment['id']]);?>'" title='Delete'><span class="glyphicon glyphicon-trash"></span></button>
                          <?php $domainURL=Yii::$app->params['domain_url'];?>
                         <?= Html::button('<span class="fa fa-edit"></span>', ['value' => Url::to($domainURL.'/invoice/updatereceipt?id='.$payment['id']), 'class' => 'btn recipitbtn btn-action updaterecipt']) ?>
                          
                      </td>
                </tr>
                  <?php } ?>

            </tbody></table>
        </div>
        
    </div>

</body>
