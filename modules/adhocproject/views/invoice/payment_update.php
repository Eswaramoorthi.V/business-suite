<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\InvoiceMaster;
use yii\web\View;

$id = $_GET['id'];
 $model->payment_date = date('d-m-Y', strtotime($model['payment_date']));
 $model->cheque_date = date('d-m-Y', strtotime($model['cheque_date']));
$query = InvoiceMaster::find()->where(['id' => $id])->asArray()->one();
$pay = app\models\InvoicePayment::find()->where(['invoice_id' => $id])->asArray()->all();
//pr($pay);
// echo $paymentHistory;         

$paymentTypes = \Yii::$app->params['paymentTypes'];

?>
<div class="text-right" style="font-weight:bold;">
    <div class ="row"> 
        <table class="table table-bordered table-condensed table-hover small kv-table" id="paymenthistory">
            <tr>
                <td class="text-right" colspan="8"> Invoice.No <?php echo $invoicedetails['invoice_number']; ?> <br>
<?php $rDate = explode(" ", $invoicedetails['invoice_date']);
echo $rDate[0];
?> <br>
                    Total Amount :<?php echo $invoicedetails['total_amount']; ?> </td>
            </tr>

        </table>
    </div>
</div>
        <div id='msg' class='msg'></div>
<div class="customer-form form-vertical">
    <?php $form = ActiveForm::begin(['id' => 'active-form']); ?>
    <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
        
       <div class="col-md-2">
            <?=
            $form->field($model, 'payment_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
             ])->textInput()->label('Payment Date')
            ?>
        </div>

        <div class="col-md-3">
            <?=
            $form->field($model, 'payment_method', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->dropDownList($paymentTypes,['id'=>'paymentType' ,'class'=>'select from-control' ])->label('Payment Method')
            ?>
        </div>
         <div class="col-md-3"> 
     
    <?=
            $form->field($model, 'paid_amount', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
//                'inputOptions' => ['value' => Yii::$app->formatter->asDecimal($model->amount)]
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
            ])->textInput(['placeholder' => ""])->label('Paid Amount')
            ?>
 </div>
        <div class="col-md-4">
        <?=
        $form->field($model, 'description', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textarea(['rows'=>3])->label('Description')
        ?>
    </div>
       <div id="asonline">
<div class="col-md-4 ">     
      <?=
            $form->field($model, 'bank_account', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Bank Account')
            ?>
</div> </div> 
        <div id="ascheque">
        <div class="col-md-3 ">     
      <?=
            $form->field($model, 'cheque_number', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([])->label('Cheque Number')
            ?>
</div> 
    <div class="col-md-2">
            <?=
            $form->field($model, 'cheque_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
             ])->textInput()->label('Cheque Date')
            ?>
    </div></div>
        
         

<div class="form-group">
   <label class="col-md-3 col-xs-12 control-label" for=""></label>
    <div class="col-md-12 col-xs-6">
        <input type="hidden" value="0" id="empidcheck-val">
        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
    </div>
</div>
      <?php ActiveForm::end(); ?>  
</div>
</div>
<script>
    
    $('#invoicepayment-payment_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY'
        
    });
   $('#invoicepayment-cheque_date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
</script>
<script>
       $(document).ready(function() {
       if ($('#paymentType').val()== 1) {
        
                $('#ascheque').hide();
                $('#asonline').hide();
            } 
         else if($('#paymentType').val()== 3 ) {
        
                $('#ascheque').hide();
                 $('#asonline').show();
               
            } 
          

        $('#paymentType').click(function () {
                if ($('#paymentType').val()==='1') {
                $('#ascheque').hide();
                 $('#invoicepayment-bank_account').val("");
                $('#asonline').hide();
                 $('#invoicepayment-cheque_number').val("");
                             $('#invoicepayment-cheque_date').val("");
               
            } else { 
                        if ($('#paymentType').val()==='2') {
                             $('#ascheque').show();
                            $('#asonline').show();} 
                         else { 
                          $('#asonline').show();
                          $('#invoicepayment-cheque_number').val("");
                             $('#invoicepayment-cheque_date').val("");
                           $('#ascheque').hide();
                           
                         }
            }
            
        });
       
    });
  
</script>
<script> 
$("form#active-form").submit(function() {
 
        $("#invoicepayment-paid_amount").removeClass("error-highlight");
        $("#invoicepayment-cheque_date").removeClass("error-highlight");
        $("#invoicepayment-cheque_number").removeClass("error-highlight");
        $("#invoicepayment-bank_account").removeClass("error-highlight");

        if ($('#invoicepayment-paid_amount').val() === '') {
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Enter Paid Amount</p>");
            $("#invoicepayment-paid_amount").addClass("error-highlight");
            return false;
        } 
        else if (($('#paymentType').val() !== '1') && ($('#invoicepayment-bank_account').val() === '')) {
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Enter Bank Account</p>");
            $("#invoicepayment-bank_account").addClass("error-highlight");
            return false;
        }
        else if (($('#paymentType').val() ==='2') && ($('#invoicepayment-cheque_number').val() === '')) {
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Enter Cheque Number</p>");
            $("#invoicepayment-cheque_number").addClass("error-highlight");
            return false;
        }
         else if (($('#paymentType').val() ==='2') && ($('#invoicepayment-cheque_date').val() === '')) {
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Select Cheque Date</p>");
            $("#invoicepayment-cheque_date").addClass("error-highlight");
            return false;
        }
       else {
            $("#error-message").addClass("hide");
        }
  
});
</script>
