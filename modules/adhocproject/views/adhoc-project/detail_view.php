<?php


use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use app\models\Services;
use yii\helpers\ArrayHelper;

$codeList=\app\models\Lead::find()->asArray()->all();

$leadCodeList = ArrayHelper::map($codeList, 'id', 'lead_code');

if($model->project_type == '0'){
$this->params['breadcrumbs'][] = ['label' => 'Quoted', 'url' => ['index']];
}
else{
$this->params['breadcrumbs'][] = ['label' =>'Awarded', 'url' => ['awarded-index']];
}
$this->params['breadcrumbs'][] = $model->project_name;

$loa=isset($loaInfo[0]['loaAmount'])? number_format((float)$loaInfo[0]['loaAmount'], 2, '.', ''):0;
$vo=isset($voInfo[0]['voAmount'])? number_format((float)$voInfo[0]['voAmount'], 2, '.', ''):0;
$loavoAmount=number_format((float)($loa+$vo), 2, '.', '');

$invoice =isset($invoiceInfo[0]['totalAmount'])? number_format((float)$invoiceInfo[0]['totalAmount'], 2, '.', ''):0;
$serviceexpenseInfo = isset($expenseInfo1[0]['servicetotalAmount'])?$expenseInfo1[0]['servicetotalAmount']:0; 
$materialexpenseInfo= isset($expenseInfo2[0]['materialtotalAmount'])?$expenseInfo2[0]['materialtotalAmount']:0;
$contractorexpenseInfo= isset($expenseInfo3[0]['contractortotalAmount'])?$expenseInfo3[0]['contractortotalAmount']:0;
$contractordiscountexpenseInfo= isset($discountInfo[0]['discounttotalAmount'])?$discountInfo[0]['discounttotalAmount']:0;
////pr1($materialexpenseInfo);
$expense=$serviceexpenseInfo+$materialexpenseInfo+($contractorexpenseInfo-$contractordiscountexpenseInfo);
//$expense=$serviceexpenseInfo+$materialexpenseInfo+$contractorexpenseInfo;
$expense = number_format((float)$expense, 2, '.', '');

$profitamount=$invoice-$expense;
$profit = number_format((float)$profitamount, 2, '.', '');
//$pro=!empty($profit<0)?$profit:'0';

//pr($pro);
Modal::begin([
    'header' => '<h4>Update Project</h4>',
    'id' => 'update',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalupdate'><div class='loader'></div></div>";
Modal::end();
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-tasks" aria-hidden="true"></i> Project |  <small><?php echo $model['project_name']; ?></small> </h1>
        <ul class="breadcrumb">

        </ul>    </section>
</div>
<?php if($model->project_type == '1'){?>
<div class="row">
    <div class="col-md-3">
                            
                          
                            <div class="widget widget-info widget-padding-sm">
                                <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
                                    <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                                        <div class="widget-title">LOA</div>                                                                          
                                        <div class="widget-subtitle"><?php echo number_format($loavoAmount, 2 ,'.',',');  ?></div>
                                        <div class="widget-int"></div>
                                    </div></div></div></div>          
                               </div>                                                                                       
                            </div>         
                         
                        </div>
    
                        <div class="col-md-3">
                            
                          
                            <div class="widget widget-info widget-padding-sm">
                                <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
                                    <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                                        <div class="widget-title">Total Value</div>                                                                       
                                        <div class="widget-subtitle"><?php echo  number_format($invoice , 2 ,'.',',');   ?></div>
                                        <div class="widget-int"></div>
                                    </div></div></div></div>
                                    
                                    
                               </div>                            
                                                             
                            </div>         
                        
                            
                        </div>
                        <div class="col-md-3">
                            
                        
                            <div class="widget widget-info widget-padding-sm">
                                <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
                                    <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                                        <div class="widget-title">Expense</div>                                                                       
                                        <div class="widget-subtitle"><?php echo number_format($expense, 2 ,'.',',');   ?></div>
                                        <div class="widget-int"></div>
                                    </div></div></div></div>
                                   
                                </div>                             
                            </div>         
                        
                            
                        </div>
                        <div class="col-md-3">
                            
                      
                            <div class="widget widget-info widget-padding-sm">
                                <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
                                    <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1400px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                                                   <div class="widget-title" id="test">Profit</div>
                                                    <div class="widget-subtitle " id="profit"><?php echo number_format($profit, 2 ,'.',',');   ?></div>
                                        <div class="widget-int"></div>
                                       </div></div></div>
                                 </div>                             
                            </div>         
                            
                        </div>
                        
                    </div>

</div>
<?php }?>
<div class="nav-tab-custom">
 <ul class="nav nav-tabs">
        <li class="active" id="summarytab"><a href="#summary" data-toggle="tab">Project Summary</a></li>
        <li id="quotationtab"><a href="#quotation" data-toggle="tab">BOQ</a></li>
        <?php if($model->project_type == '1'){?>
            <li id="loatab"><a href="#loa" data-toggle="tab">LOA</a></li>
        <?php };?>
        <li id="documentstab"><a href="#documents" data-toggle="tab">Documents</a></li>
        <li id="drawingstab"><a href="#drawings" data-toggle="tab">Drawings</a></li>
        <?php if($model->project_type == '1'){?>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">Sub-Contractor's&Supplier's <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li id="servicestab"><a href="#services" data-toggle="tab">Man Power</a></li>
                <li id="materialtab"><a href="#material" data-toggle="tab">Material Procure</a></li>
                <li id="subcontracttab"><a href="#subcontract" data-toggle="tab">Sub Contractor</a></li>                        
            </ul>
        </li>
        <?php };?>
       <li id="sitephotostab"><a href="#sitephotos" data-toggle="tab">Site Photos</a></li>
       <?php if($model->project_type == '1'){?>
        <li id="invoicetab"><a href="#invoice" data-toggle="tab">Invoices</a></li>
           <!-- <li id="payment_applicationtab"><a href="#payment_application" data-toggle="tab">Payment Application</a></li> -->
         <?php };?>
    </ul>
    <div class="tab-content">
        <div class="tab-pane summary active" id="summary">

            <div class="box-body table-responsive">
                <p class="text-right" >
                    <?php $projectType = $model->project_type?>
                    <?=
                    Html::a(Yii::t('app', ' {modelClass}', [
                                'modelClass' => 'Edit',
                            ]), ['project/update', 'id' => $model->id,'ptype'=>$projectType], ['class' => 'btn btn-success', 'id' => 'modalUpdate']);
                    ?>  </p>                           
                <section class="content edusec-user-profile">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table">
                                <colgroup>
                                    <col style="width:15%">
                                    <col style="width:35%">
                                    <col style="width:15%">
                                    <col style="width:35%">
                                </colgroup>
                                <tbody><tr>
                                        <th> Project Name</th>
                                        <td><?php echo $model['project_name']; ?></td><th> Project Code</th>
                                        <td><?php echo $model['project_code']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Start Date</th>
                                        <td><?php $rDate = explode(" ", $model['start_date']);
                    echo $rDate[0]; ?></td>
                                        <th>End Date</th>
                                        <td><?php $rDate = explode(" ", $model['end_date']);
                    echo $rDate[0]; ?></td>
                                    </tr>
                                    <tr>
                                        <th>Project Value</th>
                                        <td><?php echo $model['amount']; ?></td>
                                        <th>Remarks</th>
                                        <td><?php echo $model['description']; ?></td>
                                    </tr>
                                     <tr>
                                        <th>Lead Code</th>
                                        <td><?php echo isset($leadCodeList[$model['lead_code']])?$leadCodeList[$model['lead_code']]:'-' ; ?></td>
                                        </tr>

                                </tbody></table>
                        </div>
                </section>               
            </div>
        </div>

        <div class="tab-pane" id="documents">
            <div class="box-body table-responsive">
                <?php
                $id = $_GET['id'];
                $dataProvider = new ActiveDataProvider([
                    'query' => \app\models\Document::find()->where(['project_id' => $id])->orderBy(['id' => SORT_DESC])
                ]);
                ?>
<?= $this->render('/document/index', ['model' => $model, 'dataProvider' => $dataProvider]) ?>

            </div>
        </div>
        <div class="tab-pane" id="loa">
            <div class="box-body table-responsive">
                <?php
                $id = $_GET['id'];
                $dataProvider = new ActiveDataProvider([
                    'query' => \app\models\Loa::find()->where(['project_id' => $id])->orderBy(['id' => SORT_DESC])
                ]);
                ?>
                <?php
                $id = $_GET['id'];
                $dataProvider2 = new ActiveDataProvider([
                    'query' => \app\models\Variation::find()->where(['project_id' => $id])->orderBy(['id' => SORT_DESC])
                ]);
                ?>
    <?= $this->render('/loa/index', ['model' => $model, 'dataProvider' => $dataProvider,'dataProvider2' => $dataProvider2]) ?>

            </div>
        </div>
        <div class="tab-pane" id="drawings">
            <div class="box-body table-responsive">
                <?php
                $id = $_GET['id'];
                $dataProvider = new ActiveDataProvider([
                    'query' => \app\models\Drawing::find()->where(['project_id' => $id])->orderBy(['id' => SORT_DESC])
                ]);
                ?>
               <?= $this->render('/drawing/index', ['model' => $model, 'dataProvider' => $dataProvider]) ?>

            </div>
        </div>
        <div class="tab-pane  servic" id="services">
            <div class="box-body table-responsive"> 

                <?php
                      $id=$_GET['id'];
                    
                  $dataProvider = [];
             
            $dataProvider = new ActiveDataProvider([
                'query' =>  app\models\Services::find()->where(['project_id'=>$id])->orderBy(['id' => SORT_DESC])
             ]);
           echo $this->render('/service/service_view', ['model'=> $model,'dataProvider' => $dataProvider]);
             ?>
             </div>
        </div> 

        <div class="tab-pane  material" id="material">
            <div class="box-body table-responsive">
                <?php
             
             $dataProvider = [];
             
            $dataProvider = new ActiveDataProvider([
                'query' => \app\models\Transaction::find()->where(['project_id'=>$id])->andwhere(['=','type',2])->orderBy(['id' => SORT_DESC])
            ]);
              echo $this->render('/product/material_view', ['model'=> $model,'dataProvider' => $dataProvider]);
                
              ?>

            </div>
        </div>


        <div class="tab-pane  subcontract" id="subcontract">
            <div class="box-body table-responsive">
                <?php
                 $id=$_GET['id'];
                    
                  $dataProvider = [];
             
            $dataProvider = new ActiveDataProvider([
                'query' =>  app\models\SubContractor::find()->where(['project_id'=>$id])->orderBy(['id' => SORT_DESC])
                           
            ]);
           
           echo $this->render('/sub-contractor/subcontractor_view', ['model'=> $model,'dataProvider' => $dataProvider]);
             ?> 
             	
            </div>
        </div>
             <div class="tab-pane  quotes" id="quotation">
               <div class="box-body table-responsive"> 
                   
                 <?php   
                 $id=$_GET['id'];
                 // $dataProvider = new ActiveDataProvider([
                 //    'query' => \app\models\QuotationMaster::find()->where(['project_id' => $id])->orderBy(['id' => SORT_DESC])
                 //  ]); 
                  $searchModel = new \app\models\Search\QuotationMasterSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$id);
                        
                 echo $this->render('/quotation/index', ['model'=> $model,'dataProvider' => $dataProvider,
              'qsearchModel' => $searchModel]);
             ?>    
        </div>
                    </div>  
            
                     <div class="tab-pane  invoice" id="invoice">
               <div class="box-body table-responsive"> 
                    <?php
                  $id=$_GET['id'];
                  $dataProvider = [];
                  $dataProvider = new ActiveDataProvider([
                'query' => \app\models\InvoiceMaster::find()->Where(['project_id'=>$id])->orderBy(['id' => SORT_DESC])
                           
                  ]);
                  echo $this->render('/invoice/index', ['model'=> $model,'dataProvider' => $dataProvider]);
         
        ?>  
                         </div>
                    </div>
        <div class="tab-pane  payment_application" id="payment_application">
            <div class="box-body table-responsive">
                <?php
                $id=$_GET['id'];
                $dataProvider = [];
                $dataProvider = new ActiveDataProvider([
                    'query' => \app\models\PaInvoiceMaster::find()->Where(['project_id'=>$id])->orderBy(['id' => SORT_DESC])

                ]);
                echo $this->render('/pa-invoice/index', ['model'=> $model,'dataProvider' => $dataProvider]);

                ?>
            </div>
        </div>
        <div class="tab-pane" id="sitephotos">
            <div class="box-body table-responsive">
                <?php
                $id = $_GET['id'];
                $dataProvider = new ActiveDataProvider([
                    'query' => \app\models\SitePhotos::find()->where(['project_id' => $id])->orderBy(['id' => SORT_DESC])
                ]);
                ?>
    <?= $this->render('/site-photos/index', ['model' => $model, 'dataProvider' => $dataProvider]) ?>

            </div>
        </div>

    </div>
</div>


<?php
$this->registerJs("$(function() {
   $('#modalUpdate').click(function(e) {
     e.preventDefault();
     $('#update').modal('show').find('#modalupdate')
     .load($(this).attr('href'));
   });
   

});");
?>
<?php
$js = <<< JS
$(document).ready(function(){
    
   var url_parts = location.href.split('#');
        
        
    var last_segment = url_parts[url_parts.length-1];
    if(url_parts.length==2) {
        try {
            $('.nav-tabs #summarytab').removeClass('active');
            $('.tab-content #summary').removeClass('active');
            $('.tab-content #'+last_segment).addClass('active');
            $('.nav-tabs #'+last_segment+'tab').addClass('active');
         } catch (e) {
            $('.nav-tabs #summarytab').addClass('active');
         }   
    }
            
});
JS;
$this->registerJs($js);
?>
<!--    <script>
        $(document).ready(function() {

    var url_parts = location.href.split('/');

    var last_segment = url_parts[url_parts.length-1];
    alert(last_segment);

    //$('.nav-tabs a[href="' + last_segment + '"]').parents('li').addClass('active');

  });
  </script>-->
<?php 
$js = <<< JS
$(document).ready(function() {

  var valuestart ='$profit';
 
	if(valuestart<0){

	document.getElementById('profit').style.color = 'red';
	$("#test").html("Loss");
}

		
 
});

JS;
$this->registerJs($js);
?>