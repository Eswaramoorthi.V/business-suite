<?php

use yii\widgets\ActiveForm;

use yii\helpers\Html;
use app\models\Customer;
use yii\web\View;
use yii\helpers\ArrayHelper;

$type = $_GET['ptype'];
$customerDetails = Customer::find()->andWhere(['customer_type' => '1'])->asArray()->all();

$customerlist = [];
foreach ($customerDetails as $customerData) {

    $customersname = ucfirst($customerData['customer_fname'] . ' ' . $customerData['customer_lname']);

    $customerlist[$customerData['id']] = $customersname;
}
$codeList=\app\models\Lead::find()->asArray()->all();
// pr($codeList);
$leadCodeList = ArrayHelper::map($codeList, 'id', 'lead_code');

$projectStatus=\Yii::$app->params['projectStatus'];
$projectType = ['1'=>'Awarded','0'=>'Quoted'];

$getLastRecord=app\models\Project::find()->orderBy(['id' => SORT_DESC])->asArray()->one();


$PCode='PC/';
if(!empty($getLastRecord)){
    $autoId=$getLastRecord['id']+1;
}else{
    $autoId=1;
}
$strpadnumber=str_pad($autoId, 5, '0', STR_PAD_LEFT);
// pr($strpadnumber);
$project_code=$PCode.$strpadnumber;


?>
<div id='msg' class='msg'></div>
<div class="customer-form form-vertical">
    <?php $form = ActiveForm::begin(['id'=>'active-form','action' => ['create']]); ?>
     <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    
    <div class="row">
<legend class="text-info"><small>New Project</small></legend>
        
         <div class="col-md-3">
            <?=
            $form->field($model, 'project_name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Project Name"])->label(' Project Name')
            ?>
         </div>
         <div class="col-md-3">
            <?=
            $form->field($model, 'project_code', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Project Code",'value'=>$project_code])->label(' Project Code')
            ?>
         </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'customer_id', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
            ])->dropDownList($customerlist, [
                'prompt' => 'Choose Client',
                'class'=>'select from control',
                    // 'id'=>'groupid',
            ])->label("Client Name")
            ?>
        </div>
           <div class="col-md-3">
            <?=
            $form->field($model, 'start_date', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
            ])->textInput([

                ])->label('Start Date')?>
        </div>
         </div>
         <div class="row">
          <div class="col-md-3">
            <?=
            $form->field($model, 'end_date', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
            ])->textInput([

                ])->label('End Date')?>
        </div>
        <?php if($type == 1){?>
        <div class="col-md-3">
            <?=
            $form->field($model, 'status', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
              //  'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->dropDownList($projectStatus, [
                //'prompt' => 'Select Status',
                'class'=>'select from control',
                    // 'id'=>'groupid',
            ])->label("Status")
            ?>
       
        </div>
        <?php } ?>
     <div class="col-md-3">
            <?=
            $form->field($model, 'project_type', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
              //  'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->dropDownList($projectType, [
                //'prompt' => 'status',
                'class'=>'select from control',
                    // 'id'=>'groupid',
            ])->label("Project Type")
            ?>
       
        </div>
             
      <div class="col-md-3">
            <?=
            $form->field($model, 'amount', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([
                'placeholder' => "amount",
                 'value' => "0",
                ])->label('Project Value')?>
      </div> 
      <div class="col-md-3">
            <?=
            $form->field($model, 'lead_code', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
              //  'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->dropDownList($leadCodeList, [
                //'prompt' => 'status',
                'class'=>'select from control',
                    // 'id'=>'groupid',
            ])->label("Lead Code")
            ?>
       
        </div>
          </div>
  <div class="row">
     
             <div class="col-md-5">
            <?=
            $form->field($model, 'description', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textarea(['placeholder' => "Description"])->label('Description')
            ?>
        
             </div>
              </div>
       
             
           <div class="row">
        <div class="form-group">
            <label class="col-md-12 col-xs-12 control-label" for=""></label>
            <div class="col-md-12 col-xs-12">
                  <input type="hidden" name="ptype" value="<?php echo $ptype; ?>">
<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>  
        </div>


 <?= $this->registerJs("
    
        $('#adhocproject-start_date').datetimepicker({
            defaultDate: new Date(),
            format: 'DD-MM-YYYY',
          });
        $('#adhocproject-end_date').datetimepicker({
            defaultDate: new Date(),
            format: 'DD-MM-YYYY'
        });

", View::POS_READY); ?>

<script> 
$("form#active-form").submit(function(e) {
    e.preventDefault();
    event.stopPropagation();
  
   $("#project-project_name").removeClass("error-highlight");
   $("#project-customer_id").removeClass("error-highlight");

   var formData = new FormData(this);
       if($('#project-project_name').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Project Name cannot be Empty</p>");
            $("#project-project_name").addClass("error-highlight");
            return false;
       }
       else if($('#project-customer_id').val()==='') {
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Choose Client</p>");
            $("#project-customer_id").addClass("error-highlight");
            return false;
       }
       else {
           $("#error-message").addClass("hide");
         }
    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('Project Created');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();

            },
        cache: false,
        contentType: false,
        processData: false
    });
   
     
});
</script>