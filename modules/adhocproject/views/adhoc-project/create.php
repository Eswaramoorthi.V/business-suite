<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PreventiveMaintenance */

$this->title = 'Projects';
//$this->params['breadcrumbs'][] = ['label' => 'Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-cubes';


?>
<div class="project-create">



    <?= $this->render('_form', [
        'model' => $model,
        'ptype' => $ptype,
        
    ]) ?>

</div>
