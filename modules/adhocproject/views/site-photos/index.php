
<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;

$id=$_GET['id'];
//$products = app\models\Products::find()->where(['project_id' => $id])->asArray()->all();
?>
<?php
            
    Modal::begin([
        'header' => '<h4>Update SitePhotos</h4>',
        'id' => 'updatephotos',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalphotoupdate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
Modal::begin([
        'header' => '<h4>Upload SitePhotos</h4>',
        'id' => 'uploadphoto',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modaluploadphoto'><div class='loader'></div></div>";
    Modal::end();
?>
<div class="row">
<div class="col-md-12">

    <p class="text-right" >
        <?=Html::a(Yii::t('app', ' {modelClass}', [
                   'modelClass' => 'Add Sitephotos',
                    ]), ['/site-photos/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalSitephotoupload']);
?>  </p> 
     <legend class="text-info"><small>Site Photos</small></legend>
 </div>
</div>
 <?php
 
if ($dataProvider) {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],

        [
            'attribute' => 'description',
            'header' => '<div style="width:120px;">Description</div>',
            'value' =>  'description',
           
        ],
        
       ['attribute'=>'Attachment',
        'header' => '<div style="width:30px;">Attachment</div>',
        'format'=>'raw',
        'value' => function($model)
        {
        return
        Html::a('<div style="text-align:center; font-size:18px;"><span class="fa fa-download"></span></div>', ['site-photos/download', 'id' => $model->id]);

        }
        ],
  
[
                'class' => 'kartik\grid\ActionColumn',
                'mergeHeader' => false,
                'vAlign' => 'middle',
                'header' => '<div style="width:150px;">Actions</div>',
                'template' => ' {update}{deletes}',
                'buttons' => [
                   'update' => function ($url, $model, $key) {
                           
                                return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', Url::to(['site-photos/update', 'id' => $model->id]),
                                    [
                                        'class'=>'updatephoto',
                                        'title' => 'Edit',
                                       // 'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                        },
                    'deletes' =>function ($url, $model, $key) {
                          
                                return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', Url::to(['site-photos/deletes', 'id' => $model->id]),
                                    [
                                       // 'class'=>'delete',
                                        'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                                       // 'data-pjax' => '0',
                                        
                                    ]
                                );
                           
                        },   
                ],
            ],
       ],         
        
       'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => false, // pjax is set to always true for this demo
    
    'summary' => '',
//    'panel' => [
//        'type' => GridView::TYPE_PRIMARY,
//        'heading' => '',
//    ],
    
    ]);
}

?>  


<?php
$this->registerJs("$(function() {
   $('#modalSitephotoupload').click(function(e) {
     e.preventDefault();
     $('#uploadphoto').modal('show').find('#modaluploadphoto')
     .load($(this).attr('href'));
   });
});", View::POS_READY);
 ?>

<?= $this->registerJs(

  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updatephoto').click(function(e){
       e.preventDefault();      
       $('#updatephotos').modal('show')
                  .find('#modalphotoupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
 ?>