<?php

use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Customer;
use dosamigos\ckeditor\CKEditor;


$contractorId=$_GET['id'];
$vatcategory=\Yii::$app->params['VATRate'];
$contrctor = app\models\SubContractor::find()->where(['id'=>$contractorId])->asArray()->one();
$projectId=$contrctor['project_id'];

$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
//pr($projectname);
$name = $projectname['project_name'];
$customer = Customer::find()->asArray()->all();
//pr($customer);
$customerlist = [];
        foreach ($customer as $custData) {
            if($custData['customer_type']==1) {
                continue;
            }
            $customerlist[$custData['id']] =  ($custData['type']==1)? $custData['coustomer_fname']." [".$custData['company_name']."]" : $custData['coustomer_fname'];
                    
        }

?>



       
 <div class="customer-form form-horizontal">
    <?php $form = ActiveForm::begin(['id'=>'active-form', 
        'options' => [
               'enctype' => 'multipart/form-data'
            ],]); ?>
       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
         <div class="row">
        <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo ucfirst(strtolower($name));?></small></legend></div>
         </div>
   <div class ="row">   
        <div class="col-md-3">
           <?=
            $form->field($model, 'name', [
                'template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->dropDownList($customerlist, [
                'prompt' => 'Choose Contractor',
                'class'=>'select from control',
                    // 'id'=>'groupid',
            ])->label("SubContractor Name")
            ?>
        </div>
         <div class="col-md-3">
            <?=
            $form->field($model, 'reference_number', ['template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput()->label('Reference Number')
            ?> 
        </div>
        
        <div class="col-md-3">
           <?=
            $form->field($model, 'date', [
                'template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput()->label('Date')
            ?> 
        </div>
        <div class="col-md-3">  
      <?php $documentpath=\Yii::$app->params['domain_url'].$model->document;?>
    <?php if(is_null($model->document) || empty($model->document)): ?>
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9'>
                {input}\n
                {hint}\n
                {error}
          </div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label','style'=>'text-align:left;' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput()->label('BOQ Attachment') ?>
    <?php else: ?>
  
 
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9 col-xs-9'>
                {input}\n
                <div class='file-preview'>
                  <strong> Current File </strong> : 
                    <div class='clearfix'></div>
                     <a base href='$documentpath'target='_blank'> $model->document</a>
                
                {hint}\n
                {error}
          </div></div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label','style'=>'text-align:left;'  ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput()->label('BOQ Attachment') ?>
    <?php endif; ?>
</div>
   </div>
        <div class="row">
       <div class="col-md-12">
                <?=
                $form->field($model, 'description')->widget(CKEditor::className(), [
        'options' => ['rows' => 2],
        'preset' => 'custom','clientOptions' => [
    //'extraPlugins' => 'pbckcode',
    'toolbarGroups' => [
        ['name' => 'undo'],
        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
        ['name' => 'colors'],
        [ 'name'=> 'editing',     'groups'=> [ 'find', 'selection', 'spellchecker' ] ],
        ['name'=> 'paragraph',   'groups'=> [ 'list', 'indent', 'blocks', 'align' ]],
        [   'name'=> 'styles' ] 
    ]],
                   
    ])->label('Notes')
                ?>
            </div>
       
    </div>
<div class="row">
       <div class="col-md-12">
                <?=
                $form->field($model, 'payment_terms')->widget(CKEditor::className(), [
        'options' => ['rows' => 2],
        'preset' => 'custom','clientOptions' => [
    //'extraPlugins' => 'pbckcode',
    'toolbarGroups' => [
        ['name' => 'undo'],
        ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
        ['name' => 'colors'],
        [ 'name'=> 'editing',     'groups'=> [ 'find', 'selection', 'spellchecker' ] ],
        ['name'=> 'paragraph',   'groups'=> [ 'list', 'indent', 'blocks', 'align' ]],
        [   'name'=> 'styles' ] 
    ]],
                   
    ])->label('Payment Terms')
                ?>
            </div>
       
      

        
        
       
    </div>
<div>&nbsp</div>  
<div class="col-md-12"><legend class="text-info"><small> LPO Items</small></legend></div>
<div class ="row">
    <div class="col-md-5" style="font-weight:bold">Description</div>
    <div class="col-md-1" style="font-weight:bold">Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Unit</div>
    <div class="col-md-2" style="font-weight:bold">Price</div>
    <div class="col-md-2" style="font-weight:bold">Total</div>
    <div class="col-md-1" style="font-weight:bold">&nbsp;</div>
</div>

<div id="product">
<?php foreach ($model->lpo as $lpodata) { ?>



<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-5"><textarea id="lpo-description" class="form-control required descrip" name="Lpo[description][]"><?php echo $lpodata->description;?></textarea></div>
    <div class="col-md-1"><input type="text" value="<?php echo $lpodata->quantity; ?>" id="quantity-0<?php echo $lpodata->id; ?>" class="form-control qnty" name="Lpo[quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" id="invoice-unit" class="form-control required unit" name="Lpo[unit][]"></div>
    <div class="col-md-2"><input type="text" value="<?php echo  number_format($lpodata->price, 2, '.', ''); ?>" id="price-0<?php echo $lpodata->id; ?>" class="form-control" name="Lpo[price][]" onchange="calc(this.id);"></div>
    <div class="col-md-2"><input type="text" class="form-control totalamount" readOnly = "true" value="<?php echo  number_format($lpodata->total_amount, 2, '.', '');?>" id="totalamount-0<?php echo $lpodata->id; ?>" name="Lpo[total_amount][]" onchange="calc(this.id);"></div>
    <div class="col-md-0 rmbtn" style="margin-top:5px"><button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove" style="font-size:10px; margin-right:0px;"></i></button></div>
    <div class="col-md-0"><input type="hidden" value="<?php echo $lpodata->id;?>" name="Lpo[id][]"> </div>
    <div class="clear">&nbsp;</div>
  </div>
</div>


<?php } ?>
</div>

  <div class="col-md-12" input-group-btn style="margin-top:21px">
        <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> ADD</button>
       
        
    </div>

<div class="invoice-box">
    <div class="row">
 <div class="col-md-6">
        </div> 
         <div class="col-md-6">
                    <?=
                    $form->field($model, 'sub_total', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput(
                            [
                                'id' => 'subtotal',
                                'readOnly' => true,
                                'placeholder' => '0.00',
                                'class' => 'form-control subtotaltotal text-bold',
                                'onchange' => 'calc(this.id);',
                            ]
                    )->label('Sub Total')
                    ?>
                    <?=
                    $form->field($model, 'discount_rate', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'placeholder' => '0%',
                        'id' => 'discountrate',
                        'onchange' => 'calc(this.id);',
                    ])->label('Discount Rate(%)')
                    ?>
                    <?=
                    $form->field($model, 'discount_amount', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'readOnly' => true,
                        'id' => 'discountamount',
                        'placeholder' => '0.00',
                        'class' => 'form-control discount text-bold'
                    ])->label('Discount Amount')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>
                     <div class="col-md-12"><legend class="text-info"><small></small></legend></div> 
                    <?=
                    $form->field($model, 'vat', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput(
                            [
                                
                                'id' => 'vat',
                                'value'=>$vatcategory,
                                 'readOnly' => true,
                                'class' => 'form-control subtotaltotal text-bold',
                                'onchange' => 'calc(this.id);',
                            ]
                    )->label('Vat')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>
                    <?=
                    $form->field($model, 'po_amount', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'readOnly' => true,
                        'placeholder' => '0.00',
                        'id' => 'totalamount',
                         'class' => 'form-control total text-bold'
                    ])->label('Total')
                    ?>
                </div> 
        

         </div>
         </div>

<div>&nbsp;</div>
     
       

        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12"> 
    <div class="form-group">
        <div class="col-md-12 col-xs-12">
            <input type="hidden" name="current_document" value="<?php echo $model->document; ?>">

<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
            <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>

        </div>
    </div>
</div>

   
</div>
</div>
    <?php
    ActiveForm::end();
    ?>

    
    <?php 
$js = <<< JS
        
    var counter = 1;
    $(".add-more").click(function () {
        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp</div><div class="control-group">' +
        '<div class="col-md-5"><textarea id="invoice-description" class="form-control" name="Lpo[description][]"></textarea></div>'+
        '<div class="col-md-1"><input type="text" value="0" id="quantity-' + counter + '" class="form-control" name="Lpo[quantity][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1"><input type="text" id="invoice-unit" class="form-control required unit" name="Lpo[unit][]"></div>'+
        '<div class="col-md-2"><input type="text" value="0.00" id="price-' + counter + '" class="form-control" name="Lpo[price][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-2"><input type="text" class="form-control totalamount" value="0.00" id="totalamount-' + counter + '" class="form-control" name="Lpo[total_amount][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-0 rmbtn" style="margin-top:5px"><button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove" style="font-size:10px; margin-right:0px;"></i></button></div>'+
        '<div class="col-md-0><input type="hidden" value="<?php echo $lpodata->id;?>" name="Lpo[id][]"> </div>'+
        '<div class="clear">&nbsp</div>'+
            '</div>');
        newContainer.appendTo("#product");
        counter++;
    });

      $('body').on('click','.remove',function(){ 
        $(this).parents('.control-group').remove();
    });

    $('#subcontractor-date').datepicker({
       format: 'dd-mm-yyyy'
    });
       
JS;
$this->registerJs($js);

?>
<script>
    $('#updatesubcont').on('hidden.bs.modal', function () {
        location.reload();
        window.history.replaceState(null, null, '#subcontract');
    });
</script>
<script>
    function calc(id) {
            var idPosition = id.split('-');
            var price = Number($("#price-"+idPosition[1]).val()).toFixed(2)
            $("#price-"+idPosition[1]).val(price);
            var total = $("#quantity-"+idPosition[1]).val()*price;
            var tot = Number(total).toFixed(2); 
            $("#totalamount-"+idPosition[1]).val(tot)
            
            var subTotal = 0;
            $(".totalamount").each(function () {
                subTotal += parseFloat($(this).val());
            });
            var subTotalFormatted = Number(subTotal).toFixed(2); 
            $("#subtotal").val(subTotalFormatted);
            
            var subtotal = Number($("#subtotal").val()).toFixed(2)
            $("#subtotal").val(subtotal);
            var totamntbeforeVat;
            totamntbeforeVat = subtotal;
            var discrate = $("#discountrate").val();
            if(discrate>0) {
                var discamount = $("#discountrate").val()*subtotal/100;
                var discountformatted = Number(discamount).toFixed(2)
                $("#discountamount").val(discountformatted);
                totamntbeforeVat = subtotal- discountformatted;
            } else {
            var discamount = 0 ;
            var discountformatted = Number(discamount).toFixed(2)
            $("#discountamount").val(discountformatted);
            totamntbeforeVat = subtotal - discountformatted;
        }
            

            //console.log("Before vat amout",totamntbeforeVat);
            var vat = Number($("#vat").val()).toFixed(2)
            $("#vat").val(vat);
            var total, vatAmount;
            
            if(vat>0) {
                   vatAmount = totamntbeforeVat*(vat)/100;
            } else {
                   vatAmount = 0;
            }
            
            total = parseFloat(totamntbeforeVat) + parseFloat(vatAmount);
            //console.log("Vat value",vat);
            //console.log("Total amount",total);
            var tot = Number(total).toFixed(2);
            $("#totalamount").val(tot);
            
        }


  $("form#active-form").submit(function(e) {
     // alert('submit');
                var decription = document.getElementsByName('Lpo[description][]');
                var quantity = document.getElementsByName('Lpo[quantity][]');
                var price = document.getElementsByName('Lpo[price][]');
                
           $("#subcontractor-name").removeClass("error-highlight");
           $("#subcontractor-date").removeClass("error-highlight");
                
            if($('#subcontractor-name').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Contractor Name cannot be Empty</p>");
            $("#subcontractor-name").addClass("error-highlight");
            return false;
            }
      
            else if($('#subcontractor-date').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Date cannot be Blank</p>");
            $("#subcontractor-date").addClass("error-highlight");
            return false;
            }
                
                $("#error-message").removeClass("hide");
                for (k=0; k<decription.length; k++) {
                    $(decription[k]).removeClass("error-highlight");
                    $(quantity[k]).removeClass("error-highlight");
                    $(price[k]).removeClass("error-highlight");
                }
                    for (i=0; i<decription.length; i++) {
                        if (decription[i].value == '') {
                            $("#error-message").html("<p>Description can not be blank</p>");
                            $(decription[i]).addClass("error-highlight");
                            return false;
                        } else if (quantity[i].value == 0) {
                            $("#error-message").html("<p>Quantity can not be 0 value</p>");
                            $(quantity[i]).addClass("error-highlight");
                            return false;
                        }else if (price[i].value == 0) {
                            $("#error-message").html("<p>Price can not be 0 value</p>");
                            $(price[i]).addClass("error-highlight");
                            return false;
                        }
                      }
                $("#error-message").addClass("hide");
       
       
});
</script>

   

