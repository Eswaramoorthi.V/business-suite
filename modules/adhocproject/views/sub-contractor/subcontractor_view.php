
<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use app\models\Customer;
use app\models\SubContractor;
use app\models\Lpo;

$id=$_GET['id'];
$products = app\models\SubContractor::find()->where(['project_id' => $id])->asArray()->all();

Modal::begin([
        'header' => '<h4>Add SubContractor</h4>',
        'id' => 'contractor',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalcontractor'><div class='loader'></div></div>";
    Modal::end();
    
   
   
?>
<?php
            
    Modal::begin([
        'header' => '<h4>Update Sub Contractor</h4>',
        'id' => 'updatesubcont',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalcontractupdate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
            
    Modal::begin([
        'header' => '<h4> Subcontractor Payment</h4>',
        'id' => 'payment',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='paymentcontent'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
Modal::begin([
    'header' => '<h4>Update</h4>',
    'id' => 'contractorrecepitmodal',
    'size' => 'modal-lg',
]);

echo "<div id ='contractormodalContent'><div class='loader'></div></div>";
Modal::end();
?>

   
                <div class="row">
                   

                    <div class="col-md-12">

                        <p class="text-right" >
                            <?=
                            Html::a(Yii::t('app', ' {modelClass}', [
                                        'modelClass' => 'Add SubContractors',
                                    ]), ['sub-contractor/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalContractors']);
                            ?>  </p>  
</div>
                   <legend class="text-info"><small>Sub-Contractors</small></legend>
</div>

 <?php
      
                        
 yii\widgets\Pjax::begin();
if ($dataProvider) {


     echo GridView::widget([
    'dataProvider' => $dataProvider,
    'responsiveWrap' =>false,
        'columns' => [
        ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
        [
                'class' => 'kartik\grid\ExpandRowColumn', 'mergeHeader' => false,
                'width' => '50px',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function ($model, $key, $index, $column) {
                 $query = SubContractor::find()
                        ->joinWith('customer')
                        ->joinWith('lpo')
                        ->joinWith('subcontractorpayment')
                        ->where(['sub_contractor.id' => $model->id])
                        ->asArray()->one();
        // pr($query);
        $customerId = $query['name'];
        $Id = $query['id'];
       
        $customerInfo = Customer::find()
                        ->where(['customer.id' => $customerId])
                        ->asArray()->one();

        $productInfo = Lpo::find()
                        ->where(['lpo.subcontractor_id' => $Id])
                        ->asArray()->all();
        $paymentInfo = \app\models\SubcontractorPayment::find()
                        ->where(['subcontractor_payment.subcontractor_id' => $Id])
                        ->asArray()->all();

        
                    return Yii::$app->controller->renderPartial('/sub-contractor/view', [
                        'model' => $query,
                        'customerInfo' => $customerInfo,
                        'productInfo' => $productInfo,
                        'paymentInfo'=>$paymentInfo
                            ]);
                },
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true
        ],
        [
                'attribute' => 'customer_id',
                'header' => '<div style="width:80px;">Name</div>',
                'value' =>  function($model, $key, $index) {
                 $client = Customer::find()->where(['id'=>$model->name])->asArray()->one();
                 $fname = $client['coustomer_fname'];
                 $lname = $client['coustomer_lname'];
                 return Html::a($fname.' '.$lname, ['/customer/view', 'id' => $model->name]);
               },
                'format' => 'raw',
            ],
        [
            'attribute' => 'Date',
            'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->date, 'php:d-m-Y');
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        [
            // 'attribute' => 'Ref.NO',
            'header' => '<div style="width:18x;">Ref.NO</div>',
            'value' => function($model, $key, $index) {
               return $model->reference_number;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        [
            // 'attribute' => 'Description',
             'header' => '<div style="width:10x;">Description</div>',
            'value' => function($model, $key, $index) {
               return $model->description;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        
         
//        [
//            'attribute' => 'Po Amount',
//            'value' => function($model, $key, $index) {
//               return $model->po_amount;
//                        
//            },
//            'vAlign' => 'middle',
//            'format' => 'raw',
//            //'width' => '150px',
//            'noWrap' => true
//        ],
        [
        'attribute'=>'document',
        'header' => '<div style="width:12x;">BOQ</div>',
        'format'=>'raw',
        'value' => function($model,$key,$index)
        {
        return !empty($model->document)?
        Html::a('<div style="text-align:center; font-size:16px;"><span class="fa fa-download"></span></div>', ['sub-contractor/download', 'id' => $model->id],['data-pjax'=>0]):'';

        }
        ],
                
        [
        'attribute'=>'po',
        'header' => '<div style="width:10px;">PO</div>',
        'format'=>'raw',
        'value' => function($model, $key, $index)
        {
        return 
        Html::a('<div style="text-align:center; font-size:16px;"><span class="fa fa-print"></span></div>', ['sub-contractor/po-download', 'id' => $model->id],['data-pjax'=>0]);

        }
        ],
        [   'attribute'=>'Payment',
                'header' => '<div style="width:32px;">Pymnt</div>',
                'format'=>'raw',
                'value' => function($model)
                {
                return
                Html::a('<div style="text-align:center; font-size:16px;"><span class="fa fa-euro"></span></div>', ['sub-contractor/payment', 'id' => $model->id],
                        [
                                      // 'data-pjax' => '0',
                                        'class'=>'subcontractorpayment',
                                        
                                    ]);

                }
            ],
        
    ['class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'template' => '{update}{deletes}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                          
                                return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', Url::to(['sub-contractor/update', 'id' => $model->id]),
                                    [
                                        'class'=>'updatecontract',
                                        'title' => 'edit',
      
                                    ]
                                );
                           
                            },
                        
                        
                        'deletes' => function ($url, $model, $key) {
                           
                                return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', Url::to(['sub-contractor/deletes', 'id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'Delete'),'data' => ['confirm' => 'Are you sure want to delete?'],
                                        //'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                        },
                    ]
    ],
    
],

      'pjax'=>false
      
]);
    
   
}

yii\widgets\pjax::end();
?>  


<?php
$this->registerJs("$(function() {
   $('#modalContractors').click(function(e) {
     e.preventDefault();
     $('#contractor').modal('show').find('#modalcontractor')
     .load($(this).attr('href'));
   });
});");
?>


<?= $this->registerJs(

  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updatecontract').click(function(e){
       e.preventDefault();      
       $('#updatesubcont').modal('show')
                  .find('#modalcontractupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
 ?>

<?= $this->registerJs(
  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.subcontractorpayment').click(function(e){
       e.preventDefault();      
       $('#payment').modal('show')
                  .find('#paymentcontent')
                  .load($(this).attr('href'));  
   });
});
");
?>

<?= $this->registerJs("
    $(function(){
$('.contractorrecipt').click(function() {
    $('#contractorrecepitmodal').modal('show')
    .find('#contractormodalContent')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>
