   <?php
  use yii\helpers\Html;

 
   ?>

        <div >
             <table width="100%" >
                <tr>
                                <td class="text-center" >
                                    <?php //echo Html::img('@web/lib/img/pdf_logo.png') ?> </td></tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                
               
                             <tr>
                    <td>&nbsp;</td>
                </tr>
                            <tr>
                                <td class="text-center text-bold" style="font-size: 20px;text-decoration:underline" >
                                   <?php echo "LOCAL PURCHASE ORDER" ?> 
                                </td>
                            </tr> 
                
            </table> 
            <div> &nbsp;</div>
                
                <table >
                    <tbody>
                    <tr>
                        <td class="text-left"> <span class="text-bold">Ref # : </span><?php echo !empty($model['lpo_number']) ? $model['lpo_number']:'LPO/'.$model['id'].'/CL/'.date('y'); ?></td>
                    
                    <td class="right_top  " style="padding-right:60px">  <span class="text-bold">Transaction Date :</span>  <?php $rDate = explode(" ", $model['date']);
                                 $date = new DateTime($rDate[0]);
                                 echo $date->format('d-m-Y');
                                ?></td>
                    
                    </tr>
                    </tbody>
                     </table> 
                    <table >
                        <tbody>
                     <tr>
                         <td class="text-left text-bold " style="margin-top:2px" >TO</td>
                     </tr>
                       <?php $salutation=['1'=>'Mr','2'=>'Mrs','3'=>'Ms'];?>
                     <tr>
                         <td ></td>
                         <td class="text-bold text-left"><?php echo !empty($customerInfo['company_name'])?'M/S'.' '.$customerInfo['company_name']:($salutation[$customerInfo['salutation']]).' . '.($customerInfo['coustomer_fname'] . " " . $customerInfo['coustomer_lname']) ?>
                      </tr>
                     <?php   $address = str_replace(",","<br>",$customerInfo['address']); ?>
                     <tr>
                          <td ></td>
                        <td><?php echo $address; ?></td>
                      </tr>
                        </tbody>
                </table> 
                 

          
                        
                        
              
    <div>&nbsp;</div>
    
  
    
     <table class="table table-bordered ">
                <tbody>
                    
                <tr class="rowheader">
                    <td width="5%" class="text-center text-bold" style="padding: 2px">S.No.</td>
                    <td width="50%" class="text-center text-bold" style="padding: 2px">DESCRIPTION</td>
                    <td width="5%" class="text-center text-bold" style="padding: 2px">QTY</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">UNIT</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">PRICE</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">AMOUNT</td>
                </tr>
                  <?php foreach($productInfo as $key => $product){ ?>
           <tr>
                <td class="text-center"> <?php echo $key+1;?> </td>
                <td class="text-left"> <?php echo $product['description'];?></td>
                <td class="text-center"> <?php echo $product['quantity'];?></td>
                <td class="text-center"> <?php echo $product['unit'];?></td>
                <td class="text-center"> <?php echo number_format($product['price'],2,'.',',');?></td>
                <td class="text-right"> <?php echo number_format($product['total_amount'],2,'.',',');?></td>
                 </tr>
               <?php } ?>
                    <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2"> Total Amount</td>
                    <td class="text-right"><?php echo number_format($model['sub_total'],2,'.',','); ?></td>
                </tr>
                 <?php if(!empty($model['discount_amount'])){?>
                  <tr class="">
                      <td style="border: 0" colspan="3"> </td>
                      <td class="text-center" colspan="2"> Discount </td>
                      <td class="text-right" style="font-weight:normal"><?php echo number_format($model['discount_amount'],2,'.',','); ?></td>
                </tr>
                <tr class="">
                      <td style="border: 0" colspan="3"> </td>
                      <td class="text-center" colspan="2">Net Total</td>
                      <td class="text-right" style="font-weight:normal"><?php echo number_format($model['sub_total']-$model['discount_amount'],2,'.',','); ?></td>
                </tr>
                 <?php } ?>
                
                <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2"> VAT 5%</td>
                    <td class="text-right"><?php echo number_format($model['vat_amount'],2,'.',','); ?></td>
                </tr>
               <tr class="rowfooter">
                     <td class="text-center text-bold" colspan="3" style="padding: 1px"><?php echo ucfirst($word); ?></td>  
                      <td class="text-center text-bold" colspan="2" style="padding: 1px"> Grand Total</td>  
                      <td class="text-right text-bold" style="padding: 1px"><?php echo number_format((float)$model['po_amount'],2,'.',','); ?></td>
                </tr>
                
            
                </tbody>
              </table>
    
    <table class="small kv-table" >
                <tbody>
                    <?php if(!empty($model['description'])){?>
                <tr>
                   
                    <td class="text-left text-bold" colspan="6"> Notes<td></tr>
                <tr>
                    <td></td>
                    <td></td>
                        <td ><?php  echo  $model['description']; ?>  </td></tr>
                     <?php }?>
                      <?php if(!empty($model['payment_terms'])){?> 
                
                         <tr><td class="text-left text-bold" colspan="6">     Payment Terms </td><tr>
                             <tr>
                                 <td></td>
                                 <td></td>
                                 <td  ><?php  echo  $model['payment_terms']; ?>  </td>
                     </tr>
                     <?php }?>
                         <tr><td></td></tr>

            </tbody>
            </table>
       
     <table >
              <tr>
                 <td height="100px" class="text-bold">&nbsp;</td>
                 </tr>
                </table>
     <table  class="sign-table">
                <tr>
                    <td width ="500"></td>
                   <td class="r-s">Authorised Signature<br><br>
                       <b> Jeyamcrm LLC</b></td>
                    </tr>
              
            </table>
               
    
</div>    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
           