<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PreventiveMaintenance */

$this->title = 'SubContractor';
//$this->params['breadcrumbs'][] = ['label' => 'Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-gear';


?>
<div class="product-create">



    <?= $this->render('/project/subcontractor_form', [
        'model' => $model,
        
    ]) ?>

</div>
