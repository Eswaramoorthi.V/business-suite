<?php

use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\web\View;
?>

<div class="asset-master-view">

    

   
        <div class="col-sm-5">
            <table class="table table-bordered2 table-condensed small kv-table">
                <tbody><tr class="danger">
                    <th colspan="6" class="text-center text-danger">Subcontractor Details</th>
                </tr>
                <tr class="active">
                    
                   
                    
                </tr>
                 <tr>
                     
                     <td class="text-left" colspan="6" style="border: 0; border-width: 0;"> <div class="text-right">  <strong>Ref # : </strong><?php echo !empty($model['lpo_number']) ? $model['lpo_number']:'CI/LPO/'.$model['id'].'/'.date('y'); ?><br>
                             Date:<?php $rDate = explode(" ",$model['date']);  echo $rDate[0]; ?></div>
                         
                      
                         
                          <div class="left_top">
                <div class="text-bold">TO:</div>

                <?php  $address = str_replace(",","<br>",$customerInfo['address']);
                   $salutation=['1'=>'Mr','2'=>'Mrs','3'=>'Ms'];
                ?>
                <div class="address_box"><div class="text-bold"><?php echo isset($customerInfo['company_name'])?'M/S'.' '.$customerInfo['company_name']:($salutation[$customerInfo['salutation']]).' . '.($customerInfo['coustomer_fname'] . " " . $customerInfo['coustomer_lname']) ?></div>
                    <?php echo $address; ?><br>

                         <?php if(!empty($customerInfo['trn_number'])){?>
                   TRN: <?php echo $customerInfo['trn_number']; }?></div>
            </div> 
                        
                         <div class="pull-right">
                   <button type="button" class="btn btn-default" onclick="location.href='<?php echo Url::to(['sub-contractor/po-download', 'id' =>  $model['id']]);?>'"  title="Print" data-toggle="tooltip"><span class="fa fa-print"></span></button>
            </div>
                       </td>
                
  
                </tr>
                <tr>
                    <td>
                         <table class="table table-bordered1 ">
                <tbody>
                    
                <tr class="rowheader">
                    <td width="5%" class="text-center text-bold" style="padding: 2px">S.No.</td>
                    <td width="50%" class="text-center text-bold" style="padding: 2px">DESCRIPTION</td>
                    <td width="5%" class="text-center text-bold" style="padding: 2px">QTY</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">UNIT</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">PRICE</td>
                    <td width="10%" class="text-center text-bold" style="padding: 2px">AMOUNT</td>
                </tr>
                  <?php foreach($productInfo as $key => $product){ ?>
           <tr>
                <td class="text-center"> <?php echo $key+1;?> </td>
                <td class="text-left"> <?php echo $product['description'];?></td>
                <td class="text-center"> <?php echo $product['quantity'];?></td>
                <td class="text-center"> <?php echo $product['unit'];?></td>
                <td class="text-center"> <?php echo number_format((float)$product['price'], 2, '.', ',');?></td>
                <td class="text-right"> <?php echo number_format($product['total_amount'], 2, '.', ',');?></td>
                 </tr>
               <?php } ?>
                  <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2"> Total Amount</td>
                    <td class="text-right"><?php echo number_format($model['sub_total'],2,'.',','); ?></td>
                </tr>
                 <?php if(!empty($model['discount_amount'])){?>
                  <tr class="">
                      <td style="border: 0" colspan="3"> </td>
                      <td class="text-center" colspan="2"> Discount <?php echo $model['discount_rate'].'%'; ?></td>
                      <td class="text-right" style="font-weight:normal"><?php echo number_format($model['discount_amount'],2,'.',','); ?></td>
                </tr>
                <tr class="">
                      <td style="border: 0" colspan="3"> </td>
                      <td class="text-center" colspan="2">Net Amount</td>
                      <td class="text-right" style="font-weight:normal"><?php echo number_format($model['sub_total']-$model['discount_amount'],2,'.',','); ?></td>
                </tr>
                 <?php } ?>
                
                <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2"> VAT 5%</td>
                    <td class="text-right"><?php echo number_format($model['vat_amount'],2,'.',','); ?></td>
                </tr>
               <tr class="rowfooter">
                     <td class="text-center text-bold" colspan="3" style="padding: 1px"><?php echo ucfirst(Yii::$app->formatter->asSpellout ((float)$model['po_amount'],2,'.',',').' '.'dirhams only') ?></td>  
                      <td class="text-center text-bold" colspan="2" style="padding: 1px"> Grand Total</td>  
                      <td class="text-right text-bold" style="padding: 1px"><?php echo number_format((float)$model['po_amount'],2,'.',','); ?></td>
                </tr>
                
            
                </tbody>
              </table>
                    </td>
                </tr>
                
            </tbody></table>
        </div>
        <div class="col-sm-7">
            <table class="table table-bordered table-condensed table-hover small kv-table">
                <tbody><tr class="success">
                    <th colspan="9" class="text-center text-success">Payment History</th>
                </tr>
                <tr class="active">
                    <th class="text-center">#</th>
                    <th> Payment Date</th>
                    <th class="text-right">Payment Method</th>
                    <th class="text-right">Cheque Number</th>
                    <th class="text-right">Cheque Date</th>
                    <th class="text-right">Bank</th>
                    <th class="text-right">Amount</th>
                    <th class="text-right">Balance</th>
                    <th class="text-right">Receipt</th>
                </tr>
                
                 <?php 
                 $paidtemp =0;
                 foreach($paymentInfo as $key=>$payment){ ?>
                <tr>
                    <td class="text-center"> <?php echo $key+1;?></td>
                    <td><?php $rDate = explode(" ", $payment['payment_date']); $payment_date = new DateTime($rDate[0]); echo $payment_date->format('d-m-Y'); ?></td>
                    <td class="text-right"><?php $paymentTypes = \Yii::$app->params['paymentTypes']; echo $paymentTypes[$payment['payment_method']]; ?></td>
                    <td class="text-right"><?php echo !empty($payment['cheque_number'])? $payment['cheque_number']:'-'; ?></td>
                    <td class="text-right"><?php $rDate = explode(" ", $payment['cheque_date']);
                                    $date = new DateTime($rDate[0]);
                                   echo !empty($payment['cheque_number']) ?  $date->format('d-m-Y') : '-' ;?>
                                   </td>
                    <td class="text-right"><?php echo !empty($payment['cheque_number']) ?$payment['bank_account']:'-'; ?></td>
                   <?php $paidtemp=$paidtemp+ $payment['paid_amount'];
                         $balanceamt = $model['po_amount']-$paidtemp; ?>
                   <td class="text-right"><?php echo number_format($payment['paid_amount'], 2, '.', ','); ?></td>
                     <td class="text-right"><?php echo number_format($balanceamt, 2, '.', ','); ?></td>
                      <td class="text-right">
                           <button type="button"  class="btn recipitbtn btn-action"  onclick="location.href='<?php echo Url::to(['sub-contractor/paymentchallan', 'id' =>  $payment['id']]);?>'" title='Print'><span class="fa fa-print"></span></button>
                          <button type="button"  class="btn recipitbtn btn-action" onclick="location.href='<?php echo Url::to(['sub-contractor/deletereceipt', 'id' =>  $payment['id']]);?>'" title='Delete'><span class="glyphicon glyphicon-trash"></span></button>
                          <?php $domainURL=Yii::$app->params['domain_url'];?>
                           <?= Html::button('<span class="fa fa-edit"></span>', ['value' => Url::to($domainURL.'/sub-contractor/updatereceipt?id='.$payment['id']), 'class' => 'btn recipitbtn btn-action contractorrecipt']) ?>
                          
                      </td>
                </tr>
                  <?php } ?>

            </tbody></table>
        </div>
        
    </div>

</body>
