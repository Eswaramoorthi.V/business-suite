<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\Customer;


//$this->title = 'Projects';
$this->params['breadcrumbs'][] = 'Awarded';
//$this->params['title_icon'] = 'fa-codepen';
?>



<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-codepen" aria-hidden="true"></i> Awarded
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
        <?= Html::button('Add Project',['value'=>Url::to('create?r=project/create&ptype=1'),'class'=>'btn btn-success','id'=>'modalButton'])?>
    </div>
</div>
<?php
            
    Modal::begin([
        'header' => '<h4>Create Project</h4>',
        'id' => 'create',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalcreate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
            
    Modal::begin([
        'header' => '<h4> Create Material Procure</h4>',
        'id' => 'productmodal',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalcontent'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
            
    Modal::begin([
        'header' => '<h4> Create Services</h4>',
        'id' => 'servicemodal',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalcontent'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
            
    Modal::begin([
        'header' => '<h4>Project Update</h4>',
        'id' => 'update',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalupdate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
 yii\widgets\Pjax::begin();
if ($dataProvider) {

echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
        
        [
                'attribute' => 'customer_id',
                'header' => '<div style="width:120px;">Client</div>',
                'value' =>  function($model, $key, $index) {
                 $client = Customer::find()->where(['id'=>$model->customer_id])->asArray()->one();
                 $companyName = $client['company_name'];
                 $fname = $client['coustomer_fname'];
                 $lname = $client['coustomer_lname'];
                 //return !empty($companyName) ? Html::a($companyName, ['/customer/view', 'id' => $model->customer_id]) :!empty($fname)?Html::a($fname.' '.$lname, ['/customer/view', 'id' => $model->customer_id]):'-';
                 return !empty($companyName) ? $companyName : $fname.' '.$lname;
                 },
                'format' => 'raw',
            ],
          [
            'attribute' => 'project_name',
            'header' => '<div style="width:120px;">Project Name</div>',
            'value' => function($model, $key, $index) {
               return Html::a($model->project_name, ['detailview', 'id' => $model->id], [ 'data-pjax'=>"0"]);

            },
           
            'vAlign' => 'middle',
            'format' => 'raw',
           // 'width' => '150px',
            'noWrap' => true
        ],               
                         
        [
            'attribute' => 'status',
            'header' => '<div style="width:120px;">Status</div>',
            'value' => function($model, $key, $index) {
             $projectStatus=\Yii::$app->params['projectStatus'];
             return $projectStatus[$model->status];
            },
            'vAlign' => 'middle',
            'format' => 'raw',
           // 'width' => '150px',
            'noWrap' => true
      ],
     
        [
                'attribute' => 'start_date',
                'header' => '<div style="width:180px;">Start Date</div>',
                'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->start_date, 'php:d-m-Y');
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd'
            ]
                ])
            ],
       [
                'attribute' => 'end_date',
                'header' => '<div style="width:180px;">End Date</div>',
                'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->end_date, 'php:d-m-Y');
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],
            [
            'attribute' => 'amount',
            'header' => '<div style="width:120px;">Project Value</div>',
            'value' => function($model, $key, $index) {
               return $model->amount; 
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
           ],

    [
    
    'class' => 'yii\grid\ActionColumn',
    'contentOptions' => ['style' => 'width:100px;text-align:center;','class'=>'skip-export'],
    'headerOptions' =>['class'=>'skip-export',],
     'header' => '<div style="width:80px;">Actions</div>',
    'template' => '{detailview}{update}{deletes} ',
    'buttons' => [
              
      'detailview' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-list icons"></span>',
                $url, 
                [
                    'title' => 'View',
                    'data-pjax' => 0,
                    //'target' => '_blank',
                ]
            );
        },
                
        'update' => function($url) {
                    return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url."&ptype=1", ['class' => 'update','title'=>'Edit']
                    );
                },
                
        'deletes' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-trash icons"></span>',
                $url."&ptype=1",
                [
                    'title' => 'delete','data' => ['confirm' => 'Are you sure want to delete?'],
                ]
            );
        },

            ],
],
    ],
        
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => 'List Of Projects', 'options' => ['colspan' => 5, 'class' => 'hide','style' => 'font-size:18px']],
                ],],
         ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => false, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar' =>  [
      '{export}',
   ],
    // set export properties
    'export' => [
        'fontAwesome' => true,
        'showConfirmAlert' => false
    ],
    // parameters from the demo form
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    //'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '',
    ],
    'persistResize' => false,
    'exportConfig' => [GridView::EXCEL => [
    'filename' => 'Project List',
    ],
            GridView::PDF => [
                'filename' => 'Project List',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'config' => [
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                ],
          ]
        
  
        ],
]);
    
   
}

yii\widgets\pjax::end();
?>  
    


<?= $this->registerJs("
    $(function(){
$('#modalButton').click(function() {
    $('#create').modal('show')
    .find('#modalcreate')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>
<?= $this->registerJs(

  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.update').click(function(e){
       e.preventDefault();      
       $('#update').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);