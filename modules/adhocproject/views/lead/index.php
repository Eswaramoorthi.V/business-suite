<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\Customer;
use yii\helpers\ArrayHelper;

//$this->title = 'Projects';
$this->params['breadcrumbs'][] = 'Lead';
//$this->params['title_icon'] = 'fa-codepen';
// pr("gfdgd");

?>

<!-- Custom Theme Style Start Here -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/style.bundle.css">
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/app-table.css">

<!-- Custom Theme Style End Here -->
<style type="text/css">
    table.kt-datatable__table thead th:nth-child(1){width: 3% !important;}
    table.kt-datatable__table thead th:nth-child(2){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(3){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(4){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(5){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(6){width: auto !important;}
    .btn.dropdown-toggle:after, .nav-link.dropdown-toggle:after{display: none;}
    .kt-datatable--brand.kt-datatable--scroll .grid-view.hide-resize{margin-bottom: 0;}
    .panel.panel-primary{border-top: unset;}
    .custom-margin-style{margin-right: 0;}
    .panel.panel-primary{    position: relative;}
    .panel-primary>.panel-heading .pull-right{    float: unset !important;}
    .panel-primary>.panel-heading{padding: 0;border:unset;}
    .panel.panel-default{background-color: #fff;}
</style>
<div class="row" style="margin-bottom:20px;">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-codepen" aria-hidden="true"></i> Lead
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
        <?= Html::button('Add Lead',['value'=>Url::to('create'),'class'=>'btn btn-success custom-main-btn custom-margin-style','id'=>'modalButton'])?>
    </div>
</div>
<?php
            
    Modal::begin([
        'header' => '<h4>Create Lead</h4>',
        'id' => 'create',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalcreate'><div class='loader'></div></div>";
    Modal::end();
    ?>

<?php
            
    Modal::begin([
        'header' => '<h4>Update Lead</h4>',
        'id' => 'update',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalupdate'><div class='loader'></div></div>";
    Modal::end();
    ?>

    <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile " style="margin-bottom: 0 !important;box-shadow: unset !important;">
<div class="kt-portlet__body kt-portlet__body--fit">
<div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded">
<?php
 yii\widgets\Pjax::begin();
if ($dataProvider) {

echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
        
                    [
                'attribute' => 'lead_code',
                'header' => '<div style="width:120px;">Lead Code</div>',
                'value' =>  function($model, $key, $index) {

                 return $model->lead_code;

                },
                'format' => 'raw',
            ],
                        [
            'attribute' => 'lead_name',
            'header' => '<div style="width:120px;">Lead Name</div>',
            'value' => function($model, $key, $index) {
               return Html::a($model->lead_name, ['detailview', 'id' => $model->id], [ 'data-pjax'=>"0"]);

            },
           
            'vAlign' => 'middle',
            'format' => 'raw',
           // 'width' => '150px',
            'noWrap' => true
        ],
         
      [
                'attribute' => 'lead_type_id',
                'header' => '<div style="width:120px;">Lead Type</div>',
                'value' => function($model, $key, $index) {
                    $leadType = \app\models\LeadType::find()->asArray()->all();

$leadTypeData = ArrayHelper::map($leadType, 'id', 'label');
                    return !empty($leadTypeData[$model['lead_type_id']])?$leadTypeData[$model['lead_type_id']]:'-';
                       
                    },
                
            ],
        [
            'attribute' => 'opportunity_amount',
            'header' => '<div style="width:120px;">Opportunity Amount</div>',
            'value' => function($model, $key, $index) {
               return $model->opportunity_amount;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],

    [
    
    'class' => 'yii\grid\ActionColumn',
    'contentOptions' => ['style' => 'width:100px;text-align:left;','class'=>'skip-export'],
    'headerOptions' =>['class'=>'skip-export',],
    'header'=>"Actions",
    'template' => '{detailview}{update}{deletes}',
    'buttons' => [
              
      'detailview' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-list icons"></span>',
                $url, 
                [
                    'title' => 'View',
                    'data-pjax' => 0,
                    //'target' => '_blank',
                ]
            );
        },
                
        'update' => function($url) {
                    return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'update','title'=>'Edit']
                    );
                },
                
        'deletes' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-trash icons"></span>',
                $url, 
                [
                    'title' => 'delete','data' => ['confirm' => 'Are you sure want to delete?'],
                ]
            );
        },
        

            ],
],
    ],
        
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => 'List Of Projects', 'options' => ['colspan' => 5, 'class' => 'hide','style' => 'font-size:18px']],
                ],],
         ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
    'pjax' => false, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar' =>  [
      '{export}',
   ],
    // set export properties
    'export' => [
        'fontAwesome' => true,
        'showConfirmAlert' => false
    ],
    // parameters from the demo form
    'bordered' => true,
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
    'hover' => true,
    //'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' => '',
    ],
    'persistResize' => false,
    'exportConfig' => [GridView::EXCEL => [
    'filename' => 'Project List',
    ],
            GridView::PDF => [
                'filename' => 'Project List',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'config' => [
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                ],
          ]
        
  
        ],
]);
    
   
}

yii\widgets\pjax::end();
?>  
    
</div>
</div>
</div> 

<?= $this->registerJs("
    $(function(){
        $('table').addClass('kt-datatable__table');
    $('tr').addClass('kt-datatable__row');
    $('th').addClass('kt-datatable__cell kt-datatable__cell--sort');
    $('td').addClass('kt-datatable__cell');

$('#modalButton').click(function() {
    $('#create').modal('show')
    .find('#modalcreate')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>
<?= $this->registerJs(

  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.update').click(function(e){
       e.preventDefault();      
       $('#update').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);