<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use app\models\LeadStatus;
use yii\helpers\ArrayHelper;
$leadStatus = LeadStatus::find()->asArray()->all();
$leadStatusData = ArrayHelper::map($leadStatus, 'id', 'label');

$leadType = \app\models\LeadType::find()->asArray()->all();

$leadTypeData = ArrayHelper::map($leadType, 'id', 'label');

$leadOwnerData=[];
?>
<div id='msg' class='msg'></div>
<div class="customer-form form-vertical">
    <?php $form = ActiveForm::begin(['id'=>'active-form']); ?>
  
   
<div class="row">

        <legend class="text-info"><small>Update</small></legend>
        <div class="col-md-4">
            <?=
            $form->field($model, 'lead_code', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                   // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Lead Code"])->label('Lead Code')
            ?>
        
        </div> 
        <div class="col-md-4 ">
           
           <?=
            $form->field($model, 'lead_name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                   // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " lead name"])->label('Lead Name')
            ?>
        </div>
  <div class="col-md-4 ">
            <?=
            $form->field($model, 'lead_description', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textarea(['placeholder' => "Lead Description"])->label('Comments')
            ?>
   </div>
    </div>
    <br>
    <div class="row">
      
         <div class="col-md-4">
            <?=
            $form->field($model, 'lead_status_id', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                //  'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->dropDownList($leadStatusData, [
                //'prompt' => 'Select Status',
                'class'=>'select from control',
                // 'id'=>'groupid',
            ])->label("Lead Status")
            ?>

        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'lead_type_id', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-lg-12 col-xs-12 control-label', 'style' => 'color:black'],
                //  'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->dropDownList($leadTypeData, [
                //'prompt' => 'Select Status',
                'class'=>'select from control',
                // 'id'=>'groupid',
            ])->label("Lead Type")
            ?>

        </div>
<div class="row">
                <div class="col-md-4">
            <?=
            $form->field($model, 'first_name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "First Name"])->label(' First Name')
            ?>
        </div>
         </div>
         <br>
          


       <div class="col-md-4">
            <?=
            $form->field($model, 'last_name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Last Name"])->label(' Last Name')
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'phone', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Phone"])->label('Phone')
            ?>
        </div>
<div class="col-md-4">
            <?=
            $form->field($model, 'mobile', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Mobile"])->label('Mobile')
            ?>
        </div>
         </div>
         <br>
            <div class="row">
         
   
     <div class="col-md-4">
            <?=
            $form->field($model, 'fax', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Fax"])->label('Fax')
            ?>
        </div>


        <div class="col-md-4">
            <?=
            $form->field($model, 'opportunity_amount', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Opportunity Amount"])->label('Opportunity Amount')
            ?>
        </div>
        </div>
 
<br>
 <div class="row">
     
        <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label" for=""></label>
            <div class="col-md-12 col-xs-6">
                        
<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
            </div>
        </div>
         </div>
<?php ActiveForm::end(); ?>  
    
</div>
 


