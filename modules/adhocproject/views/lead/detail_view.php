<?php


use yii\helpers\Html;
use yii\bootstrap\Modal;
use yii\data\ActiveDataProvider;
use app\models\Services;
use app\models\LeadStatus;
use yii\web\View;
use yii\helpers\ArrayHelper;

$this->params['breadcrumbs'][] = ['label' =>'Lead', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->lead_name;

Modal::begin([
    'header' => '<h4>Update Lead</h4>',
    'id' => 'update',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalupdate'><div class='loader'></div></div>";
Modal::end();


$leadStatus = LeadStatus::find()->asArray()->all();
$leadStatusData = ArrayHelper::map($leadStatus, 'id', 'label');

$leadType = \app\models\LeadType::find()->asArray()->all();

$leadTypeData = ArrayHelper::map($leadType, 'id', 'label');

?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-tasks" aria-hidden="true"></i> Lead |  <small><?php echo $model['lead_name']; ?></small> 
        </h1>
    </section>
</div>
<div>&nbsp</div>
<div>&nbsp</div>
<div>&nbsp</div>
<div class="nav-tab-custom">
   
    <div class="tab-content">
        
            <div class="box-body table-responsive">
                <p class="text-right" >
                   
                    <?=
                    Html::a(Yii::t('app', ' {modelClass}', [
                                'modelClass' => 'Edit',
                            ]), ['lead/update', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalUpdate']);
                    ?>  </p>
                <section class="content edusec-user-profile">
                    <div class="row">
                        <div class="table-responsive">
                            <table class="table">
                                <colgroup>
                                    <col style="width:15%">
                                    <col style="width:35%">
                                    <col style="width:15%">
                                    <col style="width:35%">
                                </colgroup>
                                <tbody><tr>
                                        <th> Lead Code</th>
                                        <td><?php echo $model['lead_code']; ?></td>
                                        <th>Lead Name</th>
                                          <td><?php echo $model['lead_name']; ?></td>
                                    </tr>
                                    <tr>
                                        
                                        <th>Comments</th>
                                         <td><?php echo $model['lead_description']; ?></td>
                                         <th>Lead Status</th>
                                        <td><?php echo $leadStatusData[$model['lead_status_id']]; ?></td>
                                    </tr>
                                    <tr>
                                       <th>Lead Type</th>
                                         <td><?php echo isset($leadTypeData[$model['lead_type_id']])? $leadTypeData[$model['lead_type_id']]: '-'; ?></td>
                                         <th>First Name</th>
                                        <td><?php echo isset($model['first_name'])? $model['first_name'] : '-'; ?></td>
                                       
                                    </tr>
                                     
                                    <tr>
                                        
                                        
                                         <th>Last Name</th>
                                         <td><?php echo isset($model['last_name'])? $model['last_name'] : '-'; ?></td>
                                         <th>Opportunity Amount</th>
                                     <td><?php echo isset($model['opportunity_amount'])? $model['opportunity_amount'] :'-'; ?></td>
                                        
                                    </tr>
                                    
                                    <tr>
                                        
                                        <th>Mobile</th>
                                        <td><?php echo isset($model['mobile'])? $model['mobile'] :'-'; ?></td>
                                        <th>Fax</th>
                                        <td><?php echo isset($model['fax'])? $model['fax'] :'-'; ?></td>
                                    </tr>
 <tr>
                                        
                                       
                                    </tr>

                                </tbody></table>
                        </div>
                </section>
            </div>
       
    </div>
</div>

<?php
$this->registerJs("$(function() {
   $('#modalUpdate').click(function(e) {
     e.preventDefault();
     $('#update').modal('show').find('#modalupdate')
     .load($(this).attr('href'));
   });
});");
?>
