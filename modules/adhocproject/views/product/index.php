<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use yii\web\View;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;


$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-gears';
?>
<div class="row">
    <div class="col-md-12 text-right add-label">
        <a href="<?= Url::toRoute([
            "/product/create"
        ]); ?>">
        <strong>Add Product</strong>
        </a>
    </div>
</div>
<?php
if ($dataProvider) {


    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false, 'hiddenFromExport' => false],
        [
            'attribute' => 'Product Name',
            'value' => function($model, $key, $index) {
                return $model->name;
            },
            'vAlign' => 'middle',
            'format' => 'raw',
           // 'width' => '150px',
            'noWrap' => true
        ],
        [
            'attribute' => 'Unit',
            'value' => function($model, $key, $index) {
               return $model->unit;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        
        [
            'attribute' => 'Description',
            'value' => function($model, $key, $index) {
               return$model->description;
            },
            'vAlign' => 'middle',
            'format' => 'raw',
           // 'width' => '150px',
            'noWrap' => true
      ],
//      [
//            'attribute' => 'Family',
//            'value' => function($model, $key, $index) {
//               return $model->family; 
//                        
//            },
//            'vAlign' => 'middle',
//            'format' => 'raw',
//            //'width' => '150px',
//            'noWrap' => true
//        ],

    [
    
    'class' => 'yii\grid\ActionColumn',
    'contentOptions' => ['style' => 'width:100px;'],
    'header'=>"Actions",
    'template' => '{view}{update}{deletes}',
    'buttons' => [
              
      'view' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-th-list icons"></span>',
                $url, 
                [
                    'title' => 'View',
                ]
            );
        },
                
        'update' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-edit icons"></span>',
                $url, 
                [
                    'title' => 'Edit',
                ]
            );
        },
                
        'deletes' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-trash icons"></span>',
                $url, 
                [
                    'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                ]
            );
        },
    ],
],
    ];


     echo GridView::widget([
    'dataProvider' => $dataProvider,
    'responsiveWrap' =>false,
    'columns' => $gridColumns, // check the configuration for grid columns by clicking button above
     'summary'=>'',
      
]);
    
   
}
?>                                   
