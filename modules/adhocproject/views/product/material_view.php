
<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;

$id=$_GET['id'];
$products = app\models\Products::find()->where(['project_id' => $id])->asArray()->all();

Modal::begin([
        'header' => '<h4>Add Material</h4>',
        'id' => 'products',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalproductss'><div class='loader'></div></div>";
    Modal::end();
    
    
    Modal::begin([
        'header' => '<h4> Update material</h4>',
        'id' => 'modalupdatematerial',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='updateMaterial'><div class='loader'></div></div>";
    Modal::end();
?>
  <div class="row">
                   <div class="col-md-12">

                        <p class="text-right" >
                            &nbsp;
                             </p>  
</div>
      <legend class="text-info"><small>Materials</small></legend>
</div>





 <?php

 yii\widgets\Pjax::begin();
if ($dataProvider) {
echo GridView::widget([
        
        'dataProvider' => $dataProvider,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
        
        [
            'attribute' => 'Description',
            'value' => function($model, $key, $index) {
               return $model->description;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        [
            'attribute' => 'Purchase Date',
            'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->date, 'php:d-m-Y');
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
         [
            'attribute' => 'Amount',
            'value' => function($model, $key, $index) {
               return $model->expense_total_amount;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        [
        'attribute'=>'Attachment',
        'header' => '<div style="width:30px;">Attachment</div>',
        'format'=>'raw',
        'value' => function($model)
        {
        return !empty($model->document)?
        Html::a('<div style="text-align:center; font-size:18px;"><span class="fa fa-download"></span></div>', ['product/download', 'id' => $model->id],['data-pjax'=>0]):'';

        }
        ],
        
   
    ],

// check the configuration for grid columns by clicking button above
     //'summary'=>'',
         'pjax'=>false
      
]);
    
   
}

yii\widgets\pjax::end();
?>  


<?php
$this->registerJs("$(function() {
   $('#modalProducts').click(function(e) {
     e.preventDefault();
     $('#products').modal('show').find('#modalproductss')
     .load($(this).attr('href'));
   });
});");
?>
<?= $this->registerJs(
  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.materialupdate').click(function(e){
       e.preventDefault();      
       $('#modalupdatematerial').modal('show')
                  .find('#updateMaterial')
                  .load($(this).attr('href'));  
   });
});
");
?>

