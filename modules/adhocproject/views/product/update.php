<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Block */

$this->title = 'Update ';
$this->params['breadcrumbs'][] = ['label' => 'Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$model->purchase_date=date('d-m-Y',strtotime($model->purchase_date));
?>
<div class="product-update">



    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>
