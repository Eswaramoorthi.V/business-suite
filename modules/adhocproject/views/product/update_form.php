<?php

use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$productId=$_GET['id'];

$product = app\models\Products::find()->where(['id'=>$productId])->asArray()->one();
$projectId=$product['project_id'];

$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
//pr($projectname);
$name = $projectname['project_name'];

?>

<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'class' => 'invoice-form form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
        ])
?>

         <div class="customer-form form-vertical">
    <?php $form = ActiveForm::begin(); ?>
  <div class="row">
       <div class="row">
          <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo ucfirst(strtolower($name));?></small></legend></div>
     </div>
     <div class="row">
         
         <div class="col-md-3">
            <?=
            $form->field($model, 'name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " "])->label('Material Name')
            ?>
         </div>
       
 
        
           <div class="col-md-3">
            <?=
            $form->field($model, 'item_number', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
            ])->textInput([
                
                ])->label('Invoice Number')?>
        </div>
          <div class="col-md-3">
            <?=
            $form->field($model, 'purchase_date', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
            ])->textInput([
                
                ])->label('Purchase Date')?>
        </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'price', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
              //  'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([
                
                ])->label('Amount')?>
       
        </div>
          <div class="col-md-6">
            <?=
            $form->field($model, 'description', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
           ])->textInput([
                
                ])->label('Description')?>
        </div>
      <div class="col-md-6">
           <?php $documentpath=\Yii::$app->params['domain_url'].$model->document;?>
    <?php if(is_null($model->document) || empty($model->document)): ?>
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9'>
                {input}\n
                {hint}\n
                {error}
          </div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php else: ?>
  
 
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9 col-xs-9'>
                {input}\n
                <div class='file-preview'>
                  <strong> Current File </strong> : 
                    <div class='clearfix'></div>
                     <a base href='$documentpath'target='_blank'> $model->document</a>
                
                {hint}\n
                {error}
          </div></div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php endif; ?>
        </div>
           </div>   
     
         
        <div class="form-group">
            <label class="col-md-12 col-xs-12 control-label" for=""></label>
            <div class="col-md-12 col-xs-12">
            <input type="hidden" name="current_document" value="<?php echo $model->document; ?>">
<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
            </div>
        </div>
  </div>
<?php ActiveForm::end(); ?>  
        </div>



<?= $this->registerJs("
    
        $('#products-purchase_date').datetimepicker({
            format: 'DD-MM-YYYY'
        });
      

", View::POS_READY); ?>

   
<script>  
   $(document).on('click', '#submit' , function() {
        $.ajax({
            type: 'POST',
            url: $('#active-form').attr('action'),
            data : $('#active-form').serialize(),
            beforeSend : function(){
                    //do anything you want before sending the form
            },
            success : function(data){
                    alert('save');
                    //We'll replace the old form with the new form widget  
                     document.getElementById("active-form").reset();

            },
            error : function(data){
                console.log('ops');
            },
        });
        return false;
    });
  </script>

