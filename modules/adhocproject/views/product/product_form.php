<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;


$projectId=$_GET['id'];

$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
$name = $projectname['project_name'];
//pr($name);

?>

<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'enctype' => 'multipart/form-data'
            ],
        ])
?>
<div id='msg' class='msg'></div>
<div id="invoice-form">
    <div class="row">

        
       <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo $name;?></small></legend></div>
        <div class="col-md-12" input-group-btn style="margin-top:21px">
            <button class="btn btn-success add-more" type="button"><i class="fa fa-plus"></i> Add More Material Procure</button>

<div >&nbsp;</div>
        </div>
    <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
        <div class ="row">
            <div class="col-md-3" style="font-weight:bold">Material Name</div>
            <div class="col-md-3" style="font-weight:bold">Invoice Number</div>
             <div class="col-md-3" style="font-weight:bold">Purchase Date</div>
            <div class="col-md-3" style="font-weight:bold">Amount</div>
           
        </div>
       
        <div id="product">
            <div class="row copyinvitems details after-add-more">
                <div class="control-group">
                     <div class ="row">
         <div class="col-md-3"><input type="text" class="form-control" name="Products[name][]"></textarea></div>
        <div class="col-md-3"><input type="text" class="form-control" name="Products[item_number][]"></textarea></div>
        <div class="col-md-3"><input type="date" class="form-control" name="Products[purchase_date][]"></div>
        <div class="col-md-3"><input type="text"  value="0.00" class="form-control" name="Products[price][]"></div>
        
                     </div>
        <div class="row">
              <div class="col-md-6" style="font-weight:bold">Description</div>
             <div class="col-md-5" style="font-weight:bold">Attachment</div>
        </div>
                    <div class="row">
         <div class="col-md-6"><input type="text" class="form-control" name="Products[description][]"></textarea></div>
         <div class="col-md-5"><input type="file" class="form-control" name="Products[document][]"></div>
                   
                </div>

            </div>
        </div>

        </div>

        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['id'=>'submit','class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
   
    <?php
    ActiveForm::end();
    ?>
</div>
 </div>
    <?php
    $js = <<< JS

    var counter = 1;
    $(".add-more").click(function () {
        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp;</div><div class="control-group">' +
        '<div><hr></div>'+'<div class = "row">'+
        '<div class ="row">'+
        '<div class="col-md-3" style="font-weight:bold">Material Name</div>'+
        '<div class="col-md-3" style="font-weight:bold">Invoice Number</div>'+
        '<div class="col-md-3" style="font-weight:bold">Purchase Date</div>'+
        '<div class="col-md-3" style="font-weight:bold">Amount</div></div>'+
        '<div class="col-md-3"><input type="text" class="form-control" name="Products[name][]"></textarea></div>'+
        '<div class="col-md-3"><input type="text" class="form-control" name="Products[item_number][]"></textarea></div>'+ 
        '<div class="col-md-3"><input type="date" class="form-control" name="Products[purchase_date][]"></div>'+
        '<div class="col-md-3"><input type="text"  value="0.00" class="form-control" name="Products[price][]"></textarea></div></div>'+ 
            '<div class ="row" style="margin-top: 20px;"><div class="col-md-6" style="font-weight:bold">Description</div>'+
        '<div class="col-md-5" style="font-weight:bold">Attachment</div></div>'+
        '<div class = "row">'+
        '<div class="col-md-6"><input type="text" class="form-control" name="Products[description][]"></textarea></div>'+ 
        '<div class="col-md-5"><input type="file" class="form-control" name="Products[document][]"></div>'+
          '<div class="clear">&nbsp;</div>'+
        '<div class="col-md-1 rmbtn" style="margin-top:0px"><button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove" style="font-size:10px; margin-right:0px;"></i></button></div></div>'+
        '</div>');
        newContainer.appendTo("#product");
        counter++;
    });

    $('body').on('click','.remove',function(){ 
        $(this).parents('.control-group').remove();
    });

    $('#products-purchase_date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
                
JS;
    $this->registerJs($js);
    ?>

  
<script> 
$("form#active-form").submit(function(e) {
    e.preventDefault();
    event.stopPropagation();
    var formData = new FormData(this);
    
               var name = document.getElementsByName('Products[name][]');
               var itemnumber = document.getElementsByName('Products[item_number][]');
               var purchasedate = document.getElementsByName('Products[purchase_date][]');
               var price = document.getElementsByName('Products[price][]');
               
                 $("#error-message").removeClass("hide");
                 for (k=0; k<name.length; k++) {
                    $(name[k]).removeClass("error-highlight");
                    $(itemnumber[k]).removeClass("error-highlight");
                    $(purchasedate[k]).removeClass("error-highlight");
                    $(price[k]).removeClass("error-highlight");
                   
                    }
                    for (i=0; i<name.length; i++) {
                        if (name[i].value == '') {
                            $("#error-message").html("<p>Enter Material Name</p>");
                            $(name[i]).addClass("error-highlight");
                             return false;
                        } 
                        else if (itemnumber[i].value == '') {
                            $("#error-message").html("<p>Enter Invoice Number</p>");
                            $(itemnumber[i]).addClass("error-highlight");
                            return false;
                        }
                        else if (purchasedate[i].value == '') {
                            $("#error-message").html("<p>Select Purchase date</p>");
                            $(purchasedate[i]).addClass("error-highlight");
                            return false;
                        }
                        else if (price[i].value == 0.00) {
                            $("#error-message").html("<p>Enter Amount</p>");
                            $(price[i]).addClass("error-highlight");
                            return false;
                        }
                      }
                $("#error-message").addClass("hide");

    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('Submitted');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();
                      window.location.href = window.location.href .split('#')[0] + "#material" ;
                      location.reload();

            },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>

