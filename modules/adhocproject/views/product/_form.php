<?php

use yii\widgets\ActiveForm;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\CategoryMaster;
use app\models\AssetMaster;

use yii\helpers\Url;
use yii\web\View;



$items=['yes'=>'Yes','no'=>'No'];
$units=ArrayHelper::map(\app\models\MeasurementUnit::find()->asArray()->all(), 'id', 'name');
$cost=['standard cost'=>'Standard Cost','weighted average price'=>'Weighted Average Price'];
$category = ArrayHelper::map(\app\models\CategoryMaster::getSales(), 'id', 'description');
$purchaseVAT = ArrayHelper::map(\app\models\CategoryMaster::getPurchaseVat(), 'id', 'description');
?>

<div class="customer-form form-vertical">
    <?php $form = ActiveForm::begin(['action' => ['product/create']]); ?>
    <div class="row">
        <legend class="text-info"><small>New Product</small></legend>
        <div class="col-md-3">

            <?=
            $form->field($model, 'name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "name"])->label(' Product Name')
            ?>
        </div>
       <div class="col-md-3">

            <?=
            $form->field($model, 'unit', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
           ])->dropDownList($units, [
                'prompt' => '  select unit',
                    // 'id'=>'groupid',
            ])->label("Unit")
            ?>
        </div>
         <div class="col-md-3">

            <?=
            $form->field($model, 'sales_price', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "sales price"])->label('Sales Price')
            ?>
        </div>
         <div class="col-md-3">
         <?=
            $form->field($model, 'purchase_price', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "purchase price"])->label('Purchase Price')
            ?>
        </div>
</div>
<div class="row">
    <div class="col-md-4">
         <?=
            $form->field($model, 'item_number', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "item number"])->label('Item Number')
            ?>
        </div> 
     <div class="col-md-6">
         <?=
            $form->field($model, 'description', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textarea(['placeholder' => "description"])->label('Description')
            ?>
        </div> 
    <div class="col-md-4">
         <?php
//            $form->field($model, 'family', [
//                'template' => "{label}\n
//      <div class='col-md-12 col-xs-12'>
//            {input}\n
//            {hint}\n
//            {error}
//      </div>",
//                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
//                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
//            ])->textinput(['placeholder' => "family"])->label('Family')
            ?>
        </div> 
</div>
    <hr>
    <div class="row">
         <div class="col-md-2">
        <?=
            $form->field($model, 'inventory', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
           ])->radiolist(
   $items,[
        'class' => 'select form-control',
        
       // 'onclick' => "$(#delivery_addr).show();",
       // 'id'=>'delvry_addr',
    ])->label("Track Inventory"); ?>
        </div> 
</div>
<div class="row">
     <div class="col-md-4">
    <?=
            $form->field($model, 'cost', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
           ])->dropDownList($cost, [
                'prompt' => ' cost value',
                    // 'id'=>'groupid',
            ])->label("Valuation costing")
            
            ?>
        </div> 
    <div class="col-md-4">
    <?=
            $form->field($model, 'standard_cost', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textinput(['placeholder' => "standard cost"])->label('Standard Cost')
            ?>
        </div> 
     <div class="col-md-4">
    <?=
            $form->field($model, 'stock', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
           ])->dropDownList($items, [
                'prompt' => ' stock',
                    // 'id'=>'groupid',
            ])->label("Authorize negative stock")
            
            
            ?>
        </div> 
</div>
    <div class="row">
         <div class="col-md-4">
    <?=
            $form->field($model, 'quantity', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textinput(['placeholder' => "quantity"])->label('Minimum stock quantity')
            ?>
        </div> 
     <div class="col-md-4">
    <?=
            $form->field($model, 'receive_location', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textinput(['placeholder' => "location"])->label('Receiving Location')
            ?>
        </div> 
        <div class="col-md-4">
    <?=
            $form->field($model, 'delivery_location', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textinput(['placeholder' => "location"])->label('Picking/Delivery Location')
            ?>
        </div> 
    </div>
     <legend class="text-info"><small>Advanced</small></legend>
    <div class="row">
        
        <div class="col-md-12">
    <?=
            $form->field($model, 'notes', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textarea(['placeholder' => "notes"])->label('Private Note')
            ?>
        </div> 
    </div>
    <div class="row">
         <div class="col-md-2">
    <?=
            $form->field($model, 'net_weight', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Net weight"])->label('Net Weight')
            ?>
        </div> 
        <div class="col-md-2">
    <?=
            $form->field($model, 'product_width', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "width"])->label('Width')
            ?>
        </div> 
        <div class="col-md-2">
    <?=
            $form->field($model, 'product_length', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "length"])->label('Length')
            ?>
        </div> 
           <div class="col-md-2">
    <?=
            $form->field($model, 'product_height', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "height"])->label('Height')
            ?>
        </div> 
          <div class="col-md-3">
    <?=
            $form->field($model, 'product_dimension', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "dimension"])->label('Dimension')
            ?>
        </div> 
    </div>
     <div class="row">
           <div class="col-md-6">
    <?=
            $form->field($model, 'purchase_vat', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->dropDownList($purchaseVAT, [
                'prompt' => '  select Vat Rated',
                    // 'id'=>'groupid',
            ])->label("Purchase VAT")
            ?>
        </div> 
            <div class="col-md-6">
    <?=
            $form->field($model, 'sales_vat', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->dropDownList($category, [
                'prompt' => '  select Vat Rated',
                    // 'id'=>'groupid',
            ])->label("VAT on Sales")
            ?>
        </div> 
     </div>
<div class="form-group">
   <label class="col-md-3 col-xs-12 control-label" for=""></label>
    <div class="col-md-12 col-xs-6">
        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
    </div>
</div>
      <?php ActiveForm::end(); ?>  
</div>

 
 