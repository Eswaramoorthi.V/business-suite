<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
//use dosamigos\ckeditor\CKEditor;
use mihaildev\ckeditor\CKEditor;
//use yii\helpers\Html;

$projectId=$_GET['id'];
$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
$name = $projectname['project_name'];
$vatcategory=\Yii::$app->params['VATRate'];

$optionsTypesFlip=array_flip(\Yii::$app->params['options']);
$optionsTypes=\Yii::$app->params['options'];
$optionsTypeDropdown = '<option value="0" selected >Select</option>';
foreach ($optionsTypesFlip as $opType) {
    $optionsTypeDropdown.= '<option value="'.$opType.'">'.$optionsTypes[$opType].'</option>';
}
?>


<div class="customer-form form-horizontal">
    <?php $form = ActiveForm::begin(['id'=>'quotation-form']); ?>
    <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    
<div id="snackbar">please fill out field</div>
    <div class="row">
         <div class="row">
        <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo ucfirst(strtolower($name));?></small></legend></div>
         </div>
        <div class ="row">   


            <div class="col-md-2">
                <?=
                $form->field($quotationMaster, 'quotation_date', ['template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
       </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
                ])->textInput()->label('Quotation Date')
                ?> 
            </div> 
            <div class="col-md-4">
                 <?=
                $form->field($quotationMaster, 'name', ['template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}\n
       </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
                ])->textInput(['placeholder' => "Name"])->label('Name')
                ?> 
                 
            </div> 
        </div>
        <br>
 <ul class="nav nav-pills new-nav-pills">
    <li><a data-toggle="pill" href="#Grettings"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Grettings</a></li>
    <li><a data-toggle="pill" href="#Exclusion"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Exclusion</a></li>
     <li><a data-toggle="pill" href="#CompletionPeriod"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Completion Period</a></li>
    <li><a data-toggle="pill" href="#Notes"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Notes</a></li>

    <li><a data-toggle="pill" href="#PaymentTerms"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Payment Terms</a></li>
     <li><a data-toggle="pill" href="#Validity"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Validity</a></li>
     <li><a data-toggle="pill" href="#Price"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Price</a></li>
     <li><a data-toggle="pill" href="#Signature"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Signature</a></li>

  </ul>     
<div class ="row tab-content"> 

            <div class="col-md-12 tab-pane fade " id="Grettings">
                <?=
               CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'description',
                    'editorOptions' => [
                            'name'=>'description',
                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-12 tab-pane fade" id="Exclusion">
                <?=
                CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'exclusion_text',
                    'editorOptions' => [

                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>
<div class="col-md-12 tab-pane fade" id="CompletionPeriod">
    <?=
    CKEditor::widget([
        'model' => $quotationMaster,
        'attribute' => 'completion_period',
        'editorOptions' => [

            'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
            'inline' => false, //по умолчанию false
        ]
    ]);
    ?>
</div>

            <div class="col-md-12 tab-pane fade" id="Notes">
                <?=
               CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'notes',
                    'editorOptions' => [
                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-12 tab-pane fade" id="PaymentTerms">
                <?=
               CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'payment_terms',
                    'editorOptions' => [
                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>
    <div class="col-md-12 tab-pane fade" id="Validity">
        <?=
        CKEditor::widget([
            'model' => $quotationMaster,
            'attribute' => 'validity',
            'editorOptions' => [
                'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ]
        ]);
        ?>
    </div>
    <div class="col-md-12 tab-pane fade" id="Price">
        <?=
        CKEditor::widget([
            'model' => $quotationMaster,
            'attribute' => 'price',
            'editorOptions' => [
                'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ]
        ]);
        ?>
    </div>
    <div class="col-md-12 tab-pane fade" id="Signature">
        <?=
        CKEditor::widget([
            'model' => $quotationMaster,
            'attribute' => 'signature',
            'editorOptions' => [
                'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                'inline' => false, //по умолчанию false
            ]
        ]);
        ?>
    </div>
        </div>

        <div>&nbsp</div>  
        <div class="col-md-12"><legend class="text-info"><small>Items</small></legend></div>
        <div class="input-group-btn  text-right add-label" > 
            <button  class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Section</button>
        </div>




        <div class="form-group fieldGroup0">
            <div class="input-group">
                <div id="items0">
                    <div class="row copyinvitems details after-add-more">
                        <div class="control-group col-md-12 quotationform_padding">
                            <div class="row">
                                <div class="col-md-4" style="font-weight:bold">Section 1</div>
                                <div class="col-md-3" style="font-weight:bold">Total Amount</div>
                                <div class="col-md-2" style="font-weight:bold">Multiple Item</div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 section sectnitems0" id="0">
                                    <input type="text"  id="sectnitems-0" class="form-control" name="quotationsection[section_name][]"  required>
                                </div>

                                <div class="col-md-3"><input type="text" class="form-control sectotalamount" value="0.00" readOnly = "true" id="sectiontotalamount-0" class="form-control" name="quotationsection[item_total_amount][]" onchange="calc(this.id);"></div>
<!--                                <div class="col-md-1" id="0"><input type="checkbox" class="secmultipleitem" value="1"  id="sectionmultipleitem-0" name="quotationsection[multiple_item][]"></div>-->
                                <div class="col-md-2" id="0"><input type="hidden" name="quotationsection[multiple_item][]" value="0"><input type="checkbox" class="secmultipleitem"  id="sectionmultipleitem-0"  onclick="this.previousSibling.value=1-this.previousSibling.value" onchange="calc(this.id, '0');"></div>
                                <div class="col-md-2 input-group-btn"><button class="btn btn-success add-more" id="0" type="button"><i class="glyphicon glyphicon-plus" style="font-size:10px; margin-right:0px;"></i>ADD Items</button></div>
              

                            </div></div></div>
                    <div class="clear">&nbsp</div>
                    <div class="row col-md-12 quotationform_padding">
                        <div class="col-md-4" style="font-weight:bold">Description</div>
                        <div class="col-md-1" style="font-weight:bold">Quantity</div>
                        <div class="col-md-1" style="font-weight:bold">Unit</div>
                        <div class="col-md-2" style="font-weight:bold">Price</div>
                        <div class="col-md-2" style="font-weight:bold">Total</div>
                        <div class="col-md-2" style="font-weight:bold">Options</div>
                        <div class="col-md-0" style="font-weight:bold">&nbsp;</div>
                        <div class="col-md-4"><textarea id="quotationitems-description"  class="form-control" name="quotationsection[items][0][description][]" ></textarea></div>
                        <div class="col-md-1"><input type="text" value="0" id="quantity-0" class="form-control" name="quotationsection[items][0][quantity][]" onchange="calc(this.id, '0');"></div>
                        <div class="col-md-1"><input type="text"  class="form-control" name="quotationsection[items][0][unit][]"></div>
                        <div class="col-md-2"><input type="text" value="0.00" id="price-0" class="form-control" name="quotationsection[items][0][price][]" onchange="calc(this.id, '0');"></div>
                        <div class="col-md-2"><input type="text" class="form-control totalamount0" readOnly = "true" value="0.00" id="totalamount-0" class="form-control" name="quotationsection[items][0][total_amount][]" onchange="calc(this.id, '0');"></div>
                                <div class="col-md-2" id="0">
                                    
                                 <select class="select from-control optionsection" id="options-0" name="quotationsection[items][0][options][]" onchange="calc(this.id, '0');">
                                  <?php echo $optionsTypeDropdown;?>

                                 </select>
                                </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <div class="invoice-box">
            <div class="row">
                <div class="col-md-6">
                </div> 
                <div class="col-md-6">
                    <?=
                    $form->field($quotationMaster, 'subtotal', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput(
                            [
                                'id' => 'subtotal',
                                'readOnly' => true,
                                'placeholder' => '0.00',
                                'class' => 'form-control subtotaltotal text-bold',
                                'onchange' => 'calc(this.id);',
                            ]
                    )->label('Sub Total')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div> 

                    <?=
                    $form->field($quotationMaster, 'discount_rate', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'placeholder' => '0%',
                        'id' => 'discountrate',
                        'onchange' => 'calc(this.id);',
                    ])->label('Discount Rate(%)')
                    ?>
                    <?=
                    $form->field($quotationMaster, 'discount_amount', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'readOnly' => true,
                        'id' => 'discountamount',
                        'placeholder' => '0.00',
                        'class' => 'form-control discount text-bold'
                    ])->label('Discount Amount')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

                    <?php
//                    $form->field($quotationMaster, 'vat', [
//                        'template' => "{label}\n
//            <div class='col-md-7 col-xs-12'>
//            {input}\n
//             </div>",
//                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
//                    ])->textInput([
//                        // 'class'=>'select from control',
//                        'value'=>$vatcategory,
//                        'placeholder' => '',
//                        'id' => 'vat',
//                        'onchange' => 'calc(this.id);',
//                    ])->label('VAT(%)')
                    ?>
<!--                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>-->

                    <?=
                    $form->field($quotationMaster, 'total_amount', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'readOnly' => true,
                        'placeholder' => '0.00',
                        'id' => 'totalamount',
                         'class' => 'form-control total text-bold'
                    ])->label('Total')
                    ?>
                </div>
                <div>&nbsp</div>

            </div>
        </div>
        <div>&nbsp</div>
        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($quotationMaster->isNewRecord ? 'Submit' : 'Update', ['class' => $quotationMaster->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                    <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
ActiveForm::end();
?>


<?php
$js = <<< JS
        
    var counter = 1;
    $('body').on('click','.add-more',function(){ 
        var sectnId = $(this).attr('id');
        var sectn = $('.sectnitems'+sectnId).closest('div');
        var sectionidPosition = sectn[0].id;
        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp</div><div class="control-group col-md-12 quotationform_padding" >' +
        '<div class="col-md-4"><textarea id="quotationitems-description" class="form-control" name="quotationsection[items]['+sectionidPosition+'][description][]"></textarea></div>'+
        '<div class="col-md-1 multi'+sectionidPosition+'" ><input type="text" value="0" id="quantity-' + counter + '"  class="form-control" name="quotationsection[items]['+sectionidPosition+'][quantity][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-1 multi'+sectionidPosition+'"><input type="text" value=""  class="form-control" name="quotationsection[items]['+sectionidPosition+'][unit][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2 multi'+sectionidPosition+'"><input type="text" value="0.00" id="price-' + counter + '" class="form-control" name="quotationsection[items]['+sectionidPosition+'][price][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2 multi'+sectionidPosition+'"><input type="text" class="form-control totalamount' + sectionidPosition + '" value="0.00" readOnly = "true" id="totalamount-' + counter + '"class="form-control" name="quotationsection[items]['+sectionidPosition+'][total_amount][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2 multi'+sectionidPosition+'" id="'+sectionidPosition+'"><select class="select from-control optionsection"  id="options-' + counter + '" name="quotationsection[items]['+sectionidPosition+'][options][]" onchange="calc(this.id,'+sectionidPosition+');">$optionsTypeDropdown</select></div>'+
        '<button class="btn new-item-remove remove" id="'+sectionidPosition+'" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button>'+
        '<div class="clear">&nbsp</div>'+
            '</div>');
        newContainer.appendTo('#items'+sectionidPosition+'');
        counter++;
        
        var rowid = $(this).attr('id');
        if($('#sectionmultipleitem-'+ rowid).is(':checked')){
        //alert(rowid);
                 $(".multi" + rowid).hide();
                 $(".multi" + rowid).find(':input').val('0');
                         } else {
                     $(".multi" + rowid).show();   
                  }
        return false;
    });

      $('body').on('click','.remove',function(){ 
        var sectnId = $(this).attr('id');
        // alert(sectnId);
        $(this).parents('.control-group').parent().remove();
        calc('remove',sectnId);
        
    });
      
    $('#quotationmaster-quotation_date').datetimepicker({
        defaultDate: new Date(),
        format: 'DD-MM-YYYY'
        
    });
                

    $(document).ready(function(){
        
    //add more fields group
    $('body').on('click','.addMore',function(){
        var sectn = $("div .section").last();
        var currentPosition = parseInt(sectn[0].id);
        var sectionidPosition = parseInt(sectn[0].id)+1;
        var sectionidPositionDisplay = parseInt(sectionidPosition)+1;
    
        var fieldHTML = '<div class="form-group fieldGroup'+sectionidPosition+'"> <div class="input-group"><div id="items'+sectionidPosition+'">' +
        '<div class="row copyinvitems details after-add-more"><hr/><div class="control-group col-md-12 quotationform_padding"><div class="row">'+
        '<div class="col-md-4" style="font-weight:bold">Section '+sectionidPositionDisplay+'</div>'+
        '<div class="col-md-3" style="font-weight:bold">Total Amount</div>'+
        '<div class="col-md-1" style="font-weight:bold">Multiple Items</div>'+
        '</div><div class="row ">'+
        '<div class="col-md-4 section sectnitems'+sectionidPosition+'" id='+sectionidPosition+'><input type="text" id="section-' + sectionidPosition + '-0" class="form-control" name="quotationsection[section_name][]" onchange="calc(this.id);"  required></div>'+
        '<div class="col-md-3"><input type="text" class="form-control sectotalamount" value="0.00" readOnly = "true" id="sectiontotalamount-' + sectionidPosition + '"  class="form-control" name="quotationsection[item_total_amount][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1" id="'+sectionidPosition+'"><input type="hidden" name="quotationsection[multiple_item][]" value="0"><input type="checkbox" class="secmultipleitem" value="1" id="sectionmultipleitem-' + sectionidPosition + '" onclick="this.previousSibling.value=1-this.previousSibling.value" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2 rmbtn"><button class="btn btn-success add-more" id="'+sectionidPosition+'" type="button"><i class="glyphicon glyphicon-plus" style="font-size:10px; margin-right:0px;"></i>ADD Items</button></div>'+
        '<div class="col-md-1 rmbtn"><button class="btn btn-success remove-section" id="'+sectionidPosition+'" type="button"><i class="glyphicon glyphicon-remove" style="font-size:10px; margin-right:0px;"></i>Remove Section</button></div>'+
        '</div></div></div><div class="clear">&nbsp</div><div class="row col-md-12 quotationform_padding">'+
        '<div class="col-md-4" style="font-weight:bold">Description</div>'+
        '<div class="col-md-1" style="font-weight:bold">Quantity</div>'+
        '<div class="col-md-1" style="font-weight:bold">Unit</div>'+
        '<div class="col-md-2" style="font-weight:bold">Price</div>'+
        '<div class="col-md-2" style="font-weight:bold">Total</div>'+
        '<div class="col-md-2" style="font-weight:bold">Options</div>'+
        '<div class="col-md-0" style="font-weight:bold">&nbsp;</div>'+
        '<div class="col-md-4"><textarea id="quotationitems-description" class="form-control" name="quotationsection[items]['+sectionidPosition+'][description][]"></textarea></div>'+
        '<div class="col-md-1"><input type="text" value="0" id="quantity-0' + sectionidPosition +'" class="form-control" name="quotationsection[items]['+sectionidPosition+'][quantity][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-1"><input type="text" value=""  class="form-control" name="quotationsection[items]['+sectionidPosition+'][unit][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2"><input type="text" value="0.00" id="price-0'+ sectionidPosition + '" class="form-control" name="quotationsection[items]['+sectionidPosition+'][price][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2"><input type="text" class="form-control totalamount'+ sectionidPosition + '" value="0.00" readOnly = "true" id="totalamount-0' + sectionidPosition + '" class="form-control" name="quotationsection[items]['+sectionidPosition+'][total_amount][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
       '<div class="col-md-2" id="'+sectionidPosition+'"><select class="select from-control optionsection"  id="options-0'+ sectionidPosition + '" name="quotationsection[items]['+sectionidPosition+'][options][]" onchange="calc(this.id,'+sectionidPosition+');">$optionsTypeDropdown</select></div>'+
        '</div></div></div></div>';
        $('body').find('.fieldGroup'+currentPosition+'').after(fieldHTML);
        return false;
    });
        
        
    //remove fields group
    $("body").on("click",".remove-section",function(){ 
         var sectnId = $(this).attr('id');
        $(this).parents('.fieldGroup'+sectnId+'').remove();
        calc('remove',sectnId);
    });
        
//    $('body').on('change','.optionsection',function(){ 
//        var sectionId = $(this).parents().attr('id');
//         var optionsectionid = $(this).attr('id');
//         var target = $("#" + optionsectionid).val();
//        if(target != "0") {
//          var sectionTotamount = $("#sectiontotalamount-" + sectionId).val();
//          var selectedItemTotamount = $("#totalamount-" + optionsectionid).val();
//          var deductedAmount = parseFloat(sectionTotamount) - parseFloat(selectedItemTotamount);
//          $("#sectiontotalamount-" + sectionId).val(deductedAmount);
//          $("#totalamount-" + optionsectionid).val('0.00');
//            }
//        });
       
   
        
    $('body').on('click','.secmultipleitem',function(){ 
        //var multiid = $(this).attr('id');
        var rowid = $(this).parents().attr('id');
            if($(this).is(':checked')){
                 //alert(rowid);
                 $(".multi" + rowid).hide();
                     $(".multi" + rowid).find(':input').val('0'); 
                         } else {
                     $(".multi" + rowid).show();   
                  }
     }); 
});
JS;
$this->registerJs($js);
?>
   
<script>
    function calc(id, sectnid) {
        alert(sectnid);
         //alert(sectnid);
        if(id!="remove"){
            // alert();
        var idPosition = id.split('-');
        var price = Number($("#price-" + idPosition[1]).val()).toFixed(2);
        var options = Number($("#options-" + idPosition[1]).val());
        $("#price-" + idPosition[1]).val(price);
        $("#totalamount-" + idPosition[1]).val("0.00");
        if(options==0) {
        var total = $("#quantity-" + idPosition[1]).val() * price;
        var tot = Number(total).toFixed(2);
        $("#totalamount-" + idPosition[1]).val(tot);
        } 
    }
        var secSubTotal = 0;
        $(".totalamount" + sectnid).each(function () {
            console.log('secSubTotal',$(this).val());
            secSubTotal += parseInt($(this).val());

        });
        
        var secsubTotalFormatted = Number(secSubTotal).toFixed(2);

        $("#sectiontotalamount-" + sectnid).val(secsubTotalFormatted);

 console.log('secsubTotalFormatted',secsubTotalFormatted);
        var SubTotal = 0;
        $(".sectotalamount").each(function () {
            var subtotall=$(this).val();
            // alert(subtotall);
            SubTotal += parseInt($(this).val());
        });
        var subTotalFormatted = Number(SubTotal).toFixed(2);
        console.log('subTotalFormatted',subTotalFormatted);
        $("#subtotal").val(subTotalFormatted);

        var subtotal = Number($("#subtotal").val()).toFixed(2);
        console.log('subtotal',subtotal);
        $("#subtotal").val(subtotal);
        var totamntbeforeVat;
        totamntbeforeVat = subtotal;
        var discrate = $("#discountrate").val();
        if (discrate > 0) {
            var discamount = $("#discountrate").val() * subtotal / 100;
            var discountformatted = Number(discamount).toFixed(2)
            $("#discountamount").val(discountformatted);
            totamntbeforeVat = subtotal - discountformatted;
        }
        else {
            var discamount = 0 ;
            var discountformatted = Number(discamount).toFixed(2)
            $("#discountamount").val(discountformatted);
            totamntbeforeVat = subtotal - discountformatted;
        }


        //console.log("Before vat amout",totamntbeforeVat);
        var vat = Number($("#vat").val()).toFixed(2)
        $("#vat").val(vat);
        var total, vatAmount;

        if (vat > 0) {
            vatAmount = totamntbeforeVat * (vat) / 100;
        } else {
            vatAmount = 0;
        }

        total = parseFloat(totamntbeforeVat) + parseFloat(vatAmount);
        //console.log("Vat value",vat);
        //console.log("Total amount",total);
        var tot = Number(total).toFixed(2);
        console.log('tot',tot);
        $("#totalamount").val(tot)

 }
 
</script>
<script>
 

 $('#quote').on('hidden.bs.modal', function () {
       location.reload();
       window.history.replaceState(null, null, '#quotation');
});
//
// $(document).ready(function(){
//    $("form#quotation-form").validate({
//    rules :{
//        "quotationsection[section_name][]" : {
//            required : true
//        },
//        "quotationsection[items][0][description][]" : {
//            required : true
//        },
//    },
//    messages :{
//        "quotationsection[section_name][]" : {
//            required : 'Enter section name'
//        },
//        "quotationsection[items][0][description][]" : {
//            required : 'Enter section description'
//        }
//    },
//    errorPlacement: function(error,element){ 
//                       
//                         $('#snackbar').html(error);
//                         var x = document.getElementById("snackbar");
//    x.className = "show";
//    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
//                     }
//    });
//});

 $("form#quotation-form").submit(function(e) {
                var section_name = document.getElementsByName('quotationsection[section_name][]');
                var description = document.getElementsByName('quotationsection[items][0][description][]');
               var x = document.getElementById("snackbar");
                
                for (k=0; k<description.length; k++) {
                    $(section_name[k]).removeClass("error-highlight");
                    $(description[k]).removeClass("error-highlight");
                    
                }
                    for (i=0; i<section_name.length; i++) {
                        if (section_name[i].value == '') {
                            $("#snackbar").html("<p>Please Enter Section Name</p>");
                            $(section_name[i]).addClass("error-highlight");
                             x.className = "show";
                             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                            return false;
                        } else if (description[i].value == 0) {
                            $("#snackbar").html("<p>Please Enter Description</p>");
                            $(description[i]).addClass("error-highlight");
                            x.className = "show";
                             setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                            return false;
                        }
                      }
              
        
            });

   </script>
<!--<script>
          $("form#quotation-form").submit(function(e) {
              
                var decription = document.getElementsByName('quotationsection[items]['+sectionidPosition+'][description][]');
                var quantity = document.getElementsByName('quotationsection[items]['+sectionidPosition+'][quantity][]');
                var price = document.getElementsByName('quotationsection[items]['+sectionidPosition+'][price][]');
          
                $("#error-message").removeClass("hide");
                for (k=0; k<decription.length; k++) {
                    $(decription[k]).removeClass("error-highlight");
                    $(quantity[k]).removeClass("error-highlight");
                    $(price[k]).removeClass("error-highlight");
                }
                    for (i=0; i<decription.length; i++) {
                        if (decription[i].value == '') {
                            $("#error-message").html("<p>Description can not be blank</p>");
                            $(decription[i]).addClass("error-highlight");
                            return false;
                        } else if (quantity[i].value == 0) {
                            $("#error-message").html("<p>Quantity can not be 0 value</p>");
                            $(quantity[i]).addClass("error-highlight");
                            return false;
                        }else if (price[i].value == 0) {
                            $("#error-message").html("<p>Price can not be 0 value</p>");
                            $(price[i]).addClass("error-highlight");
                            return false;
                        }
                      }
                $("#error-message").addClass("hide");
        
            });


</script>-->
