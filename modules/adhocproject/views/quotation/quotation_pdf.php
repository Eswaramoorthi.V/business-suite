 <?php
use yii\helpers\Html;

 
 ?>

         
<div>
         <div class="text-center"> <?php //echo Html::img('@web/lib/img/pdf_logo.png') ?></div>
          

                    <table >
                     <tr>
                         <td class="text-left text-bold " style="margin-top:2px" >TO</td>
                     </tr>
                    <?php $salutation=['1'=>'Mr','2'=>'Mrs','3'=>'Ms'];?>
                     <tr>
                         <td colspan="1">&nbsp;</td>  <td class="text-bold"><?php echo ($salutation[$customerInfo['salutation']]).' . ' .$customerInfo['coustomer_fname'] . " " . $customerInfo['coustomer_lname']; ?>
                      </tr>
                      <?php  if($customerInfo['company_name'] != '') { ?>
                    <tr>
                         <td colspan="1">&nbsp;</td>
                         <td>
                         <?php echo $customerInfo['company_name']; ?>  
                        </td></tr>-->
                     <?php } 
                     $address = str_replace(",","<br>",$customerInfo['address']);
                    ?>
                     <tr>
                          <td colspan="1" width="22%">&nbsp;</td>
                        <td><?php echo $address; ?></td>
                      </tr>
                </table> 
         <div class="pdfquotes_ger"><?php echo $model['description']; ?></div>
              <table class="table table-bordered">
                <tbody>
                 <tr class="rowheader">
                    <td width="10px" class="text-center text-bold" style="padding: 2px">ITEM</td>
                    <td width="50%" class="text-center text-bold" style="padding: 2px">DESCRIPTION</td>
                    <td width="5%" class="text-center text-bold" style="padding: 2px">QTY</td>
                    <td width="6%" class="text-center text-bold" style="padding: 2px">UNIT</td>
                    <td width="12%" class="text-center text-bold" style="padding: 2px">PRICE</td>
                    <td width="12%" class="text-center text-bold" style="padding: 2px">AMOUNT</td>
                </tr>
               </tbody>
               <tbody class="nobreak">
                <?php 
                 foreach($items as $section){

if(!isset($section['Items'])) { continue; }
               ?>
                     <?php if(!empty($section['Section']['section_name'])){?>




                         <tr >
                             <td> <?php echo ""; ?> </td>

                             <td colspan="5" ><strong> <?php echo $section['Section']['section_name'];?></strong></td>


                         </tr>




                     <?php } ?>
                <?php foreach($section['Items'] as $key=>$product){ ?>
                <?php if($section['Section']['multiple_item'] == 0){ ?>
                <tr>

                    
                <td class="text-center"> <?php echo $key+1;?></td>
                <td class="text-left"> <?php echo $product['description'];?></td>
                <td class="text-center"> <?php echo $product['quantity'];?></td>
                <td class="text-center"> <?php echo $product['unit'];?></td>
                <td class="text-center"> <?php echo number_format($product['price'], 2, '.', ',');?></td>
                <?php if($product['options']==0) { ?>
               <td class="text-right">  <?php  echo number_format($product['total_amount'], 2, '.', ','); ?></td>
                 <?php



                } else { ?>
                 <td class="text-right text-bold"> <?php   $projectStatus=\Yii::$app->params['options']; echo $projectStatus[$product['options']]; ?></td>
               <?php } ?>
                </tr>
                <?php } else {?>
                <tr>
                    
                    <td class="text-center"> <?php echo $key+1;?> </td>
                    <td class="text-left"> <?php echo $product['description'];?></td>
                    <?php if(($product['quantity']) != 0){ ?>
                    
                <td  rowspan="<?php echo count($section['Items']);?>" class="text-center"> <?php echo $product['quantity'];?></td>
                <td  rowspan="<?php echo count($section['Items']);?>" class="text-center"> <?php echo $product['unit'];?></td>
                <td  rowspan="<?php echo count($section['Items']);?>" class="text-center"> <?php echo number_format((float)$product['price'], 2, '.', ',');?></td>
                <td  rowspan="<?php echo count($section['Items']);?>" class="text-right"> <?php if($product['options']==0) { echo number_format($product['total_amount'], 2, '.', ','); } else {echo $product['options'];}?></td>
               
                    
                    <?php } ?>
                    
                    
                  
                </tr>

                <?php  } ?>
                <?php } ?>

                     <?php if(!empty($section['Section']['section_name'])){?>


                         <tr class="rowfooter">
                             <?php $amountnew= 0; ?>
                             <?php foreach($section['Items'] as $key=>$product){ ?>
                                 <?php if($section['Section']['multiple_item'] == 0){ ?>
                                     <?php
                                     $amountnew+= $product['total_amount'];
                                     ?>
                                 <?php } } ?>
                             <td class="text-center text-bold" colspan="5" style="padding: 1px">Subtotal <?php echo $section['Section']['section_name'];?></td>

                                     <td class="text-right text-bold" colspan="1" style="padding: 1px"><?php echo number_format((float)$amountnew,2,'.',','); ?></td>


                                </tr>





                     <?php } ?>


                 <?php } ?>

                    <tr class="">
                        <td class="text-center " colspan="6">&nbsp;</td>
                    </tr>


                <tr class="">
                    <td class="text-center " colspan="1">&nbsp;</td>
                    <td class="text-center text-bold" colspan="4"><?php echo 'Summary'; ?></td>
                    <td class="text-center " colspan="1">&nbsp;</td>

                </tr>

                <tr class="">
                    <td width="10px" class="text-center text-bold" style="padding: 2px">ITEM</td>
                    <td width="50%" colspan="4" class="text-center text-bold" style="padding: 2px">DESCRIPTION</td>
                    <td width="12%" class="text-right text-bold" style="padding: 2px">AMOUNT</td>
                </tr>
                <?php
                $sn=1;
                foreach($items as  $key=> $section){

                    if(!isset($section['Items'])) { continue; }
                    ?>
                    <?php $summaryamountnew= 0; ?>

                    <?php foreach($section['Items'] as $key=> $product){ ?>
                        <?php if($section['Section']['multiple_item'] == 0){ ?>
                            <?php
                            $summaryamountnew+= $product['total_amount'];
                            ?>
                        <?php } } ?>
                    <?php if(!empty($section['Section']['section_name'])){?>


                        <tr class="">
                            <td class="text-center"> <?php echo $sn++;?> </td>
                            <td class="text-center " colspan="4" style="padding: 1px"><?php echo $section['Section']['section_name'];?></td>
                            <td class="text-right " colspan="1" style="padding: 1px"><?php echo number_format((float)$summaryamountnew,2,'.',','); ?></td>
                        </tr>


                    <?php } }?>









                </tbody>
                <tbody>
                      <?php if(!empty($model['discount_amount'])){?>
                    <tr class="">
                    <td style="border: 0" colspan="3"> </td>
                    <td class="text-center" colspan="2"> Total Amount</td>
                    <td class="text-right"><?php echo number_format($model['subtotal'],2,'.',','); ?></td>
                </tr>
               
                  <tr class="">
                      <td style="border: 0" colspan="3"> </td>
                      <td class="text-center" colspan="2">Discount</td>
                      <td class="text-right" style="font-weight:normal"><?php echo number_format($model['discount_amount'],2,'.',','); ?></td>
                </tr>
             
                 <?php } ?>


              
                  <tr class="">
                        
                      <td class="text-center text-bold " colspan="3" style="padding: 1px"><?php echo ucfirst($word); ?></td>
                      <td class="text-center text-bold" colspan="2" style="padding: 1px">Grand Total</td>
                      <td class="text-right text-bold" style="padding: 1px"><?php echo number_format((float)$model['total_amount'],2,'.',','); ?></td>
                </tr>
                </tbody>
              </table>

         <?php if(!empty($model['exclusion_text'])){?>
         <div class="pdfquotes">
             <div class="text-bold"> Exclusion</div>
             <div class="pdfquotes_data"><?php  echo  $model['exclusion_text']; ?> </div>
         </div>
        <?php }?>
    <?php if(!empty($model['completion_period'])){?>
        <div class="pdfquotes">
            <div class="text-bold"> Completion Period</div>
            <div class="pdfquotes_data"><?php  echo  $model['exclusion_text']; ?> </div>
        </div>
    <?php }?>
        <?php if(!empty($model['notes'])){?>
         <div class="pdfquotes">
            <div class="text-bold"> Notes</div>
           <div class="pdfquotes_data"><?php  echo  $model['notes']; ?> </div>
         </div>
        <?php }?>
         <?php if(!empty($model['payment_terms'])){?>
         <div class="pdfquotes">
             <div class="text-bold"> Payment Terms </div>
             <div class="pdfquotes_data"><?php  echo  $model['payment_terms']; ?>  </div>
         </div>
        <?php }?>
    <?php if(!empty($model['validity'])){?>
        <div class="pdfquotes">
            <div class="text-bold"> Validity</div>
            <div class="pdfquotes_data"><?php  echo  $model['validity']; ?> </div>
        </div>
    <?php }?>
    <?php if(!empty($model['price'])){?>
        <div class="pdfquotes">
            <div class="text-bold"> Price</div>
            <div class="pdfquotes_data"><?php  echo  $model['price']; ?> </div>
        </div>
    <?php }?>

          <table class="table" >
                <tbody>
                         <tr><td class="text-left" > <div  ><span style=" color: white">&nbsp; We hope our above offer will be in line with your requirement and look forward to the pleasure of
receiving your valued order.
                                 </div><br>
                       <div class="text-left">Yours truly,  </div><br>
                                 <div class="pdfquotes_data"><?php  echo  $model['signature']; ?> </div>
                         <div><?php echo ' ' //Html::img('@web/lib/img/signature.png') ?>  </div><br>
                         <div class="text-bold">Operations - Head  </div><br>
                    </td> </tr>

            </tbody>
            </table>
       
</div>    


     
