<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\PreventiveMaintenance */

$this->title = 'Quotation';
//$this->params['breadcrumbs'][] = ['label' => 'Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa fa-file-code-o';


?>
<div class="customer-create">



    <?= $this->render('_form', [
        'projectId'=> $projectId ,
        'quotationMaster' => $quotationMaster, 
        'quotationsection' => $quotationsection, 
        'quotationitems' => $quotationitems
        
        
    ]) ?>

</div>
