<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use mihaildev\ckeditor\CKEditor;


//$vatcategory=\Yii::$app->params['VATRate'];
$optionsTypesFlip=array_flip(\Yii::$app->params['options']);
$optionsTypes=\Yii::$app->params['options'];
$optionsTypeDropdown = '<option value="0" >Select</option>';
foreach ($optionsTypesFlip as $opType) {
    $optionsTypeDropdown.= '<option value="'.$opType.'">'.$optionsTypes[$opType].'</option>';
}
$quotationMaster->quotation_date = date('d-m-Y', strtotime($quotationMaster['quotation_date']));


//$optionsTypesExist=\Yii::$app->params['options'];
// pr("vfg");
//pr($optionsTypeDropdown);
// pr($data['sections']['item_total_amount']);
// pr($quotationMaster);
?>


<div class="customer-form form-horizontal">
    <?php $form = ActiveForm::begin(); ?>
    <div id="newsnackbar">please fill out field</div>
    <div class="row">
        <div class="row">
            <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small>  Quotation</small></legend></div>
        </div>
        <div class ="row">


            <div class="col-md-2">
                <?=
                $form->field($quotationMaster, 'quotation_date', ['template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
       </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
                ])->textInput()->label('Quotation Date')
                ?>
            </div>
            <div class="col-md-4">
                 <?=
                $form->field($quotationMaster, 'name', ['template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}\n
       </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
                ])->textInput(['placeholder' => "Name"])->label('Name')
                ?> 
                 
            </div> 
        </div>
        <br>
        <ul class="nav nav-pills new-nav-pills">
            <li><a data-toggle="pill" href="#Grettings"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Grettings</a></li>
            <li><a data-toggle="pill" href="#Exclusion"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Exclusion</a></li>
            <li><a data-toggle="pill" href="#CompletionPeriod"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Completion Period</a></li>
            <li><a data-toggle="pill" href="#Notes"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Notes</a></li>

            <li><a data-toggle="pill" href="#PaymentTerms"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Payment Terms</a></li>
            <li><a data-toggle="pill" href="#Validity"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Validity</a></li>
            <li><a data-toggle="pill" href="#Price"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Price</a></li>
            <li><a data-toggle="pill" href="#Signature"><i class="fa fa-chevron-up nav-pills-open" aria-hidden="true"></i><i class="fa fa-chevron-down nav-pills-close" aria-hidden="true"></i>&nbsp;Signature</a></li>

        </ul>
        <div class ="row tab-content">

            <div class="col-md-12 tab-pane fade " id="Grettings">
                <?=
                CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'description',
                    'editorOptions' => [
                        'name'=>'description',
                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-12 tab-pane fade" id="Exclusion">
                <?=
                CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'exclusion_text',
                    'editorOptions' => [

                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-12 tab-pane fade" id="CompletionPeriod">
                <?=
                CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'completion_period',
                    'editorOptions' => [

                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-12 tab-pane fade" id="Notes">
                <?=
                CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'notes',
                    'editorOptions' => [
                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-12 tab-pane fade" id="PaymentTerms">
                <?=
                CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'payment_terms',
                    'editorOptions' => [
                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-12 tab-pane fade" id="Validity">
                <?=
                CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'validity',
                    'editorOptions' => [
                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-12 tab-pane fade" id="Price">
                <?=
                CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'price',
                    'editorOptions' => [
                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-12 tab-pane fade" id="Signature">
                <?=
                CKEditor::widget([
                    'model' => $quotationMaster,
                    'attribute' => 'signature',
                    'editorOptions' => [
                        'preset' => 'standard', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
                        'inline' => false, //по умолчанию false
                    ]
                ]);
                ?>
            </div>
        </div>

        <div>&nbsp</div>
        <div class="col-md-12"><legend class="text-info"><small>Items</small></legend></div>
        <div class="input-group-btn  text-right add-label" >
            <button  class="btn btn-success addMore"><span class="glyphicon glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Section</button>
        </div>


        <?php
        $sectionItems = [];


        foreach($quotationMaster->section as $sectionData) {
 // pr($sectionData);

            $sectionItems[$sectionData['id']]['sections']=$sectionData->attributes;
        }
        //pr($sectionItems);
        foreach($quotationMaster->items as $itemData) {
            $sectionItems[$itemData['section_id']]['sections']['items'][] = $itemData->attributes;
        }

       // pr($quotationMaster);
        ?>

        <?php
        $pos = 0;
        $rm = 0;
        foreach($sectionItems as $key=>$data) {

            if(!isset($data['sections']['items'])) {
                // pr($data['sections']['items']);
                continue;
            }
            
            ?>
            <div class="form-group fieldGroup<?php echo $pos;?>">
                <div class="input-group">
                    <div id="items<?php echo $pos;?>">

                        <div class="row copyinvitems details after-add-more">
                            <div class="control-group  col-md-12">

                                <div class="row">


                                    <div class="col-md-4" style="font-weight:bold">Section <?php echo $pos+1;?></div>

                                    <div class="col-md-3" style="font-weight:bold">Total Amount</div>

                                    <div class="col-md-1" style="font-weight:bold">Multiple Item</div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 section sectnitems<?php echo $pos;?>" id="<?php echo $pos;?>">

                                        <input type="text"  class="form-control" name="quotationsection[section_name][]" value="<?php if(isset($data['sections']['section_name'])) { echo $data['sections']['section_name']; }?>"  required />
                                    </div>

                                    <div class="col-md-3"><input type="text" class="form-control sectotalamount" value="<?php if(isset($data['sections']['item_total_amount'])) { echo $data['sections']['item_total_amount']; } ?>" readOnly = "true" id="sectiontotalamount-<?php echo $pos;?>" class="form-control" name="quotationsection[item_total_amount][]" onchange="calc(this.id);"></div>
                                    <div class="col-md-3"><input type="hidden" class="form-control sectotalamount" name="project_id" value="<?php echo $quotationMaster->project_id?>"></div>
                                    <div class="col-md-3"><input type="hidden" class="form-control sectotalamount" name="client_id" value="<?php echo $quotationMaster->client_id?>"></div>


                                    <div class="col-md-1" id="<?php echo $pos;?>"><input type="hidden" name="quotationsection[multiple_item][]" value="<?php if(isset($data['sections']['multiple_item'])) { echo $data['sections']['multiple_item']; }?>"><input type="checkbox" class="secmultipleitem"   id="sectionmultipleitem-<?php echo $pos;?>"  <?php if(isset($data['sections']['multiple_item'])) { if(($data['sections']['multiple_item'] == 1)){  echo "checked"; } } ?> onclick="this.previousSibling.value=1-this.previousSibling.value" onchange="calc(this.id, <?php echo $pos;?>);" ></div>

                                    <div class="col-md-2 rmbtn"><button class="btn btn-success add-more" id="<?php echo $pos;?>" type="button"><i class="glyphicon glyphicon-plus" style="font-size:10px; margin-right:0px;"></i>ADD Items</button></div>
                                    <div class="col-md-1 rmbtn"><button class="btn btn-success remove-section" id="<?php echo $pos;?>" type="button"><i class="glyphicon glyphicon-remove" style="font-size:10px; margin-right:0px;"></i>Remove Section</button></div>
                                    <input type="hidden" value="<?php if(isset($data['sections']['id'])) { echo $data['sections']['id']; }?>" name="quotationsection[id][]">
                                </div></div></div>

                        <div class="clear">&nbsp;</div>

                        <div class="row col-md-12">
                            <div class="col-md-4" style="font-weight:bold">Description</div>
                            <div class="col-md-1" style="font-weight:bold">Quantity</div>
                            <div class="col-md-1" style="font-weight:bold">Unit</div>
                            <div class="col-md-2" style="font-weight:bold">Price</div>
                            <div class="col-md-2" style="font-weight:bold">Total</div>
                            <div class="col-md-2" style="font-weight:bold">Options</div>
                            <!--                        <div class="col-md-0" style="font-weight:bold">&nbsp;</div>-->
                        </div>
                        <?php

                        foreach($data['sections']['items'] as $skey=>$items) {
                            ?>
                            <div>
                                <div class="clear">&nbsp;</div>
                                <div class="control-group col-md-12 quotationform_padding">
                                    <div class="row">
                                        <div class="col-md-4"><textarea id="quotationitems-description"  class="form-control" name="quotationsection[items][<?php echo $pos; ?>][description][]"><?php echo $items['description'];?></textarea></div>
                                        <div class="col-md-1 <?php if($items['quantity'] == 0){  echo "prehide"; } ?> <?php if($skey!= 0){  echo "multi".$pos; } ?>"><input type="text" value="<?php echo $items['quantity']; ?>" id="quantity-<?php echo $rm;?>" class="form-control" name="quotationsection[items][<?php echo $pos; ?>][quantity][]" onchange="calc(this.id,<?php echo $pos;?>);"></div>
                                        <div class="col-md-1 <?php if($items['quantity'] == 0){  echo "prehide"; } ?>  <?php if($skey!= 0){  echo "multi".$pos; } ?>"><input type="text"  class="form-control" name="quotationsection[items][<?php echo $pos; ?>][unit][]" value="<?php echo $items['unit'];?>"></div>
                                        <div class="col-md-2 <?php if($items['quantity'] == 0){  echo "prehide";} ?>  <?php if($skey!= 0){  echo "multi".$pos; } ?>"><input type="text" value="<?php echo number_format($items['price'], 2, '.', ''); ?>" id="price-<?php echo $rm;?>" class="form-control" name="quotationsection[items][<?php echo $pos; ?>][price][]" onchange="calc(this.id, <?php echo $pos;?>);"></div>
                                        <div class="col-md-2 <?php if($items['quantity'] == 0){  echo "prehide"; } ?>  <?php if($skey!= 0){  echo "multi".$pos; } ?>"><input type="text" class="form-control totalamount<?php echo $pos;?>" readOnly = "true" value="<?php echo  number_format($items['total_amount'], 2, '.', ''); ?>" id="totalamount-<?php echo $rm;?>" class="form-control" name="quotationsection[items][<?php echo $pos; ?>][total_amount][]" onchange="calc(this.id, <?php echo $pos;?>);"></div>
                                        <input type="hidden" value="<?php echo $items['id'];?>" name="quotationsection[items][<?php echo $pos; ?>][id][]">
                                        <div class="col-md-2 <?php if($items['quantity'] == 0){  echo "prehide"; } ?>  <?php if($skey!= 0){  echo "multi".$pos; } ?>" id="<?php echo $pos;?>">

                                            <select class="select from-control optionsection" id="options-<?php echo $rm;?>" name="quotationsection[items][<?php echo $pos; ?>][options][]" onchange="calc(this.id, <?php echo $pos;?>);" >

                                                <?php
                                                $optionsTypeDropdownExist = '<option value="0">Select</option>';
                                                foreach ($optionsTypesFlip as $opTypeExist) {
                                                    if($items['options'] == $opTypeExist ){
                                                        $optionsTypeDropdownExist.= '<option value="'.$opTypeExist.'" selected>'.$optionsTypes[$opTypeExist].'</option>';
                                                    }else {
                                                        $optionsTypeDropdownExist.= '<option value="'.$opTypeExist.'">'.$optionsTypes[$opTypeExist].'</option>';
                                                    }
                                                }
                                                echo $optionsTypeDropdownExist;
                                                ?>

                                            </select>
                                        </div>
                                        <!--                        <div class="col-md-1" style="font-weight:bold">&nbsp;</div>-->
                                        <?php if($rm!=0){ ?><button class="btn new-item-remove remove" id="<?php echo $rm;?>"  type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button><?php } ?>
                                    </div>
                                </div></div>
                            <?php $rm++;

                        } ?>

                    </div>
                </div>
            </div>

            <?php $pos++;

        } ?>
        <hr>

        <div class="invoice-box">
            <div class="row">
                <div class="col-md-6">
                </div>
                <div class="col-md-6">
                    <?=
                    $form->field($quotationMaster, 'subtotal', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput(
                        [
                            'id' => 'subtotal',
                            'readOnly' => true,
                            'placeholder' => '0.00',
                            'class' => 'form-control subtotaltotal text-bold',
                            'onchange' => 'calc(this.id);',
                        ]
                    )->label('Sub Total')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

                    <?=
                    $form->field($quotationMaster, 'discount_rate', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'placeholder' => '0%',
                        'id' => 'discountrate',
                        'onchange' => 'calc(this.id);',
                    ])->label('Discount Rate(%)')
                    ?>
                    <?=
                    $form->field($quotationMaster, 'discount_amount', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'readOnly' => true,
                        'id' => 'discountamount',
                        'placeholder' => '0.00',
                        'class' => 'form-control discount text-bold'
                    ])->label('Discount Amount')
                    ?>
                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

                    <?php
                    //                    $form->field($quotationMaster, 'vat', [
                    //                        'template' => "{label}\n
                    //            <div class='col-md-7 col-xs-12'>
                    //            {input}\n
                    //             </div>",
                    //                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    //                    ])->textInput([
                    //                        // 'class'=>'select from control',
                    //                        'value'=>$vatcategory,
                    //                        'placeholder' => '',
                    //                        'id' => 'vat',
                    //                        'onchange' => 'calc(this.id);',
                    //                    ])->label('VAT(%)')
                    ?>
                    <!--                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>-->

                    <?=
                    $form->field($quotationMaster, 'total_amount', [
                        'template' => "{label}\n
            <div class='col-md-7 col-xs-12'>
            {input}\n
             </div>",
                        'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
                    ])->textInput([
                        'readOnly' => true,
                        'placeholder' => '0.00',
                        'id' => 'totalamount',
                        'class' => 'form-control total text-bold'
                    ])->label('Total')
                    ?>
                </div>
                <div>&nbsp;</div>

            </div>
        </div>
        <div>&nbsp;</div>
        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12">
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($quotationMaster->isNewRecord ? 'Submit' : 'Update', ['class' => $quotationMaster->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right','value'=>'update' ,'name'=>'update']) ?>
                    <?= Html::submitButton( 'Update New', ['class' => 'btn btn-primary pull-right','value'=>'update_new' ,'name'=>'update']) ?>
                    <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
ActiveForm::end();
?>


<?php
$js = <<< JS
        
    var counter = $rm;
    $('body').on('click','.add-more',function(){ 
        var sectnId = $(this).attr('id');
        var sectn = $('.sectnitems'+sectnId).closest('div');
        var sectionidPosition = sectn[0].id;
        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp</div><div class="control-group col-md-12 quotationform_padding">' +
        '<div class="col-md-4"><textarea id="quotationitems-description" class="form-control" name="quotationsection[items]['+sectionidPosition+'][description][]"></textarea></div>'+
        '<div class="col-md-1 multi'+sectionidPosition+'"><input type="text" value="0" id="quantity-' + counter + '"  class="form-control" name="quotationsection[items]['+sectionidPosition+'][quantity][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-1 multi'+sectionidPosition+'"><input type="text" value=""  class="form-control" name="quotationsection[items]['+sectionidPosition+'][unit][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2 multi'+sectionidPosition+'"><input type="text" value="0.00" id="price-' + counter + '" class="form-control" name="quotationsection[items]['+sectionidPosition+'][price][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2 multi'+sectionidPosition+'"><input type="text" class="form-control totalamount' + sectionidPosition + '" value="0.00" readOnly = "true" id="totalamount-' + counter + '"class="form-control" name="quotationsection[items]['+sectionidPosition+'][total_amount][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2 multi'+sectionidPosition+'" id="'+ sectionidPosition +'"><select class="select from-control optionsection"  id="options-' + counter + '" name="quotationsection[items]['+sectionidPosition+'][options][]" onchange="calc(this.id,'+sectionidPosition+');">$optionsTypeDropdown</select></div>'+
        '<button class="btn new-item-remove remove" id="'+sectionidPosition+'" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button>'+
        '<div class="clear">&nbsp</div>'+
            '</div>');
        newContainer.appendTo('#items'+sectionidPosition+'');
        counter++;
         var rowid = $(this).attr('id');
        if($('#sectionmultipleitem-'+ rowid).is(':checked')){
        //alert(rowid);
                 $(".multi" + rowid).hide();
                 $(".multi" + rowid).find(':input').val('0');
                         } else {
                     $(".multi" + rowid).show();   
                  }
         return false;
    });

      $('body').on('click','.remove',function(){
         
        var sectnId = $(this).attr('id');
       
        var removetotal=$('#totalamount-'+sectnId).val();
        alert(removetotal);
        var subTotal=$('#subtotal').val();
        alert(subTotal);
        var reMamount=  parseInt(subTotal) - parseFloat(removetotal);
        alert(reMamount);
        $('.subtotaltotal').val(reMamount);
       
       
        $(this).parents('.control-group').parent().remove();
        calc('remove',sectnId);
    });
      
    $('#quotationmaster-quotation_date').datetimepicker({
        format: 'DD-MM-YYYY'
    });
                

    $(document).ready(function(){
        
    //add more fields group
    $('body').on('click','.addMore',function(){
        var sectn = $("div .section").last();
        var currentPosition = parseInt(sectn[0].id);
        var sectionidPosition = parseInt(sectn[0].id)+1;
        var sectionidPositionDisplay = parseInt(sectionidPosition)+1;
 
        var fieldHTML = '<div class="form-group fieldGroup'+sectionidPosition+'"> <div class="input-group"><div id="items'+sectionidPosition+'">' +
        '<div class="row copyinvitems details after-add-more"><hr/><div class="control-group col-md-12 quotationform_padding"><div class="row">'+
        '<div class="col-md-4" style="font-weight:bold">Section '+sectionidPositionDisplay+'</div>'+
        '<div class="col-md-3" style="font-weight:bold">Total Amount</div>'+
        '</div><div class="row">'+
         '<div class="col-md-0"><input type="hidden" name="quotationsection[id][]"> </div>'+ 
        '<div class="col-md-4 section sectnitems'+sectionidPosition+'" id='+sectionidPosition+'><input type="text" id="section-' + sectionidPosition + '-0" class="form-control" name="quotationsection[section_name][]" onchange="calc(this.id);" required ></div>'+
         '<div class="col-md-3"><input type="text" class="form-control sectotalamount" value="0.00" readOnly = "true" id="sectiontotalamount-' + sectionidPosition + '"  class="form-control" name="quotationsection[item_total_amount][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1" id="'+sectionidPosition+'"><input type="hidden" name="quotationsection[multiple_item][]" value="0"><input type="checkbox" class="secmultipleitem" value="1" id="sectionmultipleitem-' + sectionidPosition + '" onclick="this.previousSibling.value=1-this.previousSibling.value" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2 rmbtn"><button class="btn btn-success add-more" id="'+sectionidPosition+'" type="button"><i class="glyphicon glyphicon-plus" style="font-size:10px; margin-right:0px;"></i>ADD Items</button></div>'+
        '<div class="col-md-1 rmbtn"><button class="btn btn-success remove-section" id="'+sectionidPosition+'" type="button"><i class="glyphicon glyphicon-remove" style="font-size:10px; margin-right:0px;"></i>Remove Section</button></div>'+
        '</div></div></div><div class="clear">&nbsp</div><div class="row col-md-12 quotationform_padding">'+
        '<div class="col-md-4" style="font-weight:bold">Description</div>'+
        '<div class="col-md-1" style="font-weight:bold">Quantity</div>'+
        '<div class="col-md-1" style="font-weight:bold">Unit</div>'+
        '<div class="col-md-2" style="font-weight:bold">Price</div>'+
        '<div class="col-md-2" style="font-weight:bold">Total</div>'+
        '<div class="col-md-0" style="font-weight:bold">&nbsp;</div>'+
        '<div class="col-md-4"><textarea id="quotationitems-description" class="form-control" name="quotationsection[items]['+sectionidPosition+'][description][]"  required></textarea></div>'+
        '<div class="col-md-1"><input type="text" value="0" id="quantity-0' + sectionidPosition +'" class="form-control" name="quotationsection[items]['+sectionidPosition+'][quantity][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-1"><input type="text" value=""  class="form-control" name="quotationsection[items]['+sectionidPosition+'][unit][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2"><input type="text" value="0.00" id="price-0'+ sectionidPosition + '" class="form-control" name="quotationsection[items]['+sectionidPosition+'][price][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2"><input type="text" class="form-control totalamount'+ sectionidPosition + '" value="0.00" readOnly = "true" id="totalamount-0' + sectionidPosition + '" class="form-control" name="quotationsection[items]['+sectionidPosition+'][total_amount][]" onchange="calc(this.id,'+sectionidPosition+');"></div>'+
        '<div class="col-md-2" id="'+sectionidPosition+'"><select class="select from-control optionsection"  id="options-0'+ sectionidPosition + '" name="quotationsection[items]['+sectionidPosition+'][options][]"  onchange="calc(this.id,'+sectionidPosition+');" >$optionsTypeDropdown</select></div>'+
       
        '</div></div></div></div>';
        $('body').find('.fieldGroup'+currentPosition+'').after(fieldHTML);
        return false;
    });
        
        
    //remove fields group
    $("body").on("click",".remove-section",function(){ 
         var sectnId = $(this).attr('id');
        //  alert(sectnId);
        $(this).parents('.fieldGroup'+sectnId+'').remove();
        calc('remove',sectnId);
    });
    
//    $('body').on('change','.optionsection',function(){ 
//        var sectionId = $(this).parents().attr('id');
//         var optionsectionid = $(this).attr('id');
//         var target = $("#" + optionsectionid).val();
//        if(target != "0") {
//          var sectionTotamount = $("#sectiontotalamount-" + sectionId).val();
//          var selectedItemTotamount = $("#totalamount-" + optionsectionid).val();
//          var deductedAmount = parseFloat(sectionTotamount) - parseFloat(selectedItemTotamount);
//          $("#sectiontotalamount-" + sectionId).val(deductedAmount);
//          $("#totalamount-" + optionsectionid).val('0.00');
//            }
//        });
       
   
        
    $('body').on('click','.secmultipleitem',function(){ 
        //var multiid = $(this).attr('id');
        var rowid = $(this).parents().attr('id');
            if($(this).is(':checked')){
                 //alert(rowid);
                 $(".multi" + rowid).hide();
                 $(".multi" + rowid).find(':input').val('0'); 
                         } 
             else {
                  $(".multi" + rowid).show();
                  $(".multi" + rowid).find(':input').val('0'); 
                  }
     }); 
        
     
});
JS;
$this->registerJs($js);
?>

<script>
    function calc(id, sectnid) {

        // alert(sectnid);
        if(id!="remove"){
           
            var idPosition = id.split('-');
            var price = Number($("#price-" + idPosition[1]).val()).toFixed(2);
            var options = Number($("#options-" + idPosition[1]).val());
            $("#price-" + idPosition[1]).val(price);
            $("#totalamount-" + idPosition[1]).val("0.00");
            if(options==0) {
                var total = $("#quantity-" + idPosition[1]).val() * price;
                var tot = Number(total).toFixed(2);
                $("#totalamount-" + idPosition[1]).val(tot);
            }
        }
        var secSubTotal = 0;
        $(".totalamount" + 0).each(function () {
        
            secSubTotal += parseFloat($(this).val());
        });
        var secsubTotalFormatted = parseFloat(secSubTotal).toFixed(2);
        $("#sectiontotalamount-" + sectnid).val(secsubTotalFormatted);


        var SubTotal = 0;
        $(".sectotalamount").each(function () {
            var subtotall=$(this).val();
            // alert(subtotall);
            SubTotal += parseFloat($(this).val());
        });
        var subTotalFormatted = Number(SubTotal).toFixed(2);
        // console.log('subTotalFormatted',subTotalFormatted);
        // $("#subtotal").val(subTotalFormatted);

        var subtotal = Number($("#subtotal").val()).toFixed(2);
        // console.log('subtotal',subtotal);
        // $("#subtotal").val(subtotal);
        var totamntbeforeVat;
        totamntbeforeVat = subtotal;
        var discrate = $("#discountrate").val();
        if (discrate > 0) {
            var discamount = $("#discountrate").val() * subtotal / 100;
            var discountformatted = Number(discamount).toFixed(2)
            $("#discountamount").val(discountformatted);
            totamntbeforeVat = subtotal - discountformatted;
        }
        else {
            var discamount = 0 ;
            var discountformatted = Number(discamount).toFixed(2)
            $("#discountamount").val(discountformatted);
            totamntbeforeVat = subtotal - discountformatted;
        }


        //console.log("Before vat amout",totamntbeforeVat);
        var vat = Number($("#vat").val()).toFixed(2)
        $("#vat").val(vat);
        var total, vatAmount;

        if (vat > 0) {
            vatAmount = totamntbeforeVat * (vat) / 100;
        } else {
            vatAmount = 0;
        }

        total = parseFloat(totamntbeforeVat) + parseFloat(vatAmount);
        //console.log("Vat value",vat);
        //console.log("Total amount",total);
        var tot = Number(total).toFixed(2);
        console.log('tot',tot);
        $("#totalamount").val(tot)



    }
</script>
<script>


    $('#updateboq').on('hidden.bs.modal', function () {
        location.reload();
        window.history.replaceState(null, null, '#quotation');
    });

    $("form#w0").submit(function(e) {
        var section_name = document.getElementsByName('quotationsection[section_name][]');
        var description = document.getElementsByName('quotationsection[items][0][description][]');

        var x = document.getElementById("newsnackbar");

        for (k=0; k<description.length; k++) {
            $(section_name[k]).removeClass("error-highlight");
            $(description[k]).removeClass("error-highlight");

        }
        for (i=0; i<section_name.length; i++) {
            if (section_name[i].value == '') {
                $("#newsnackbar").html("<p>Please Enter Section Name</p>");
                $(section_name[i]).addClass("error-highlight");
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                return false;
            } else if (description[i].value == 0) {
                $("#newsnackbar").html("<p>Please Enter Description</p>");
                $(description[i]).addClass("error-highlight");
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                return false;
            }
        }


    });
</script>