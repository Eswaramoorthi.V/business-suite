
<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use yii\web\View;


$id=$_GET['id']; 
Modal::begin([
        'header' => '<h4>Add Quotation</h4>',
        'id' => 'quote',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalquote'><div class='loader'></div></div>";
    Modal::end();
  
?>
<?php
            
    Modal::begin([
        'header' => '<h4>Edit BOQ</h4>',
        'id' => 'updateboq',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalupdateboq'><div class='loader'></div></div>";
    Modal::end();
    ?>
<div class="row">
                   <div class="col-md-12" >

                        <p class="text-right" >
                            <?=
                            Html::a(Yii::t('app', ' {modelClass}', [
                                        'modelClass' => 'Add Quotation',
                                    ]), ['quotation/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalQuotes']);
                            ?>  </p>   
                    </div>
 <legend class="text-info"><small>Quotation</small></legend>
</div>

  
<?php
 yii\widgets\Pjax::begin();
if ($dataProvider) {
// pr($qsearchModel);
echo GridView::widget([
        'filterModel' => $qsearchModel,
        'dataProvider' => $dataProvider,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
         [
             'attribute' => 'quotation_number',
             'header' => '<div style="width:120px;">#NO.</div>',
            'value' => function($model, $key, $index) {
                return $model->quotation_number;
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '50px',
            //'noWrap' => true
        ],   
        [
             'attribute' => 'quotation_date',
            'header' => '<div style="width:120px;">Date</div>',
            'value' => function($model, $key, $index) {
                return Yii::$app->formatter->asDate($model->quotation_date, 'php:d-m-Y');
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '50px',
            //'noWrap' => true
        ],
         [
             'attribute' => 'name',
            'header' => '<div style="width:180px;">Name</div>',
            'value' => function($model, $key, $index) {
                 return $model->name;
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '50px',
            //'noWrap' => true
        ],
        [
            'attribute' => 'discount_amount',
            'header' => '<div style="width:180px;">Discount Amount</div>',
            'value' => function($model, $key, $index) {
               $discount= isset($model->discount_amount)?($model->discount_amount):'-';
               return number_format((float)$discount, 2, '.', ',');
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '30px',
            //'noWrap' => true
        ],
        
        [
            'attribute' => 'total_amount',
            'header' => '<div style="width:100px;">Amount</div>',
            'value' => function($model, $key, $index) {
               return number_format((float)$model->total_amount, 2, '.', ',');
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '10px',
            //'noWrap' => true
        ],
        
         
   [

  'class' => 'yii\grid\ActionColumn',
    'contentOptions' => ['style' => 'width:100px;text-align:center;','class'=>'skip-export'],
    'headerOptions' =>['class'=>'skip-export',],
     'header' => '<div style="width:80px;">Actions</div>',
    'template' => '{export-pdf}{detailview}{update}{deletes}{copy-quote}',
    'buttons' => [
                        'update' => function ($url, $model, $key) {
                           
                                return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', Url::to(['quotation/update', 'id' => $model->id]),
                                    [
                                        'class'=>'updateboqs',
                                       'title' => Yii::t('app', 'Edit'),
                                       // 'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                        },
                        'export-pdf' => function ($url, $model, $key) {
                                return Html::a('&nbsp;<span class="glyphicon glyphicon-print icons"></span>', Url::to(['quotation/export-pdf', 'id' => $model->id]),
                            
                              
                                    [
                                        'title' => Yii::t('app', 'Export'),
                                        'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                        },
                        'copy-quote' => function($url,$model) {
                            return Html::a('&nbsp;<span class="fa fa-copy"></span>', Url::to(['quotation/copy-quote', 'id' => $model->id])
                            );
                        },
                        
                        'deletes' => function ($url, $model, $key) {
                           
                                return Html::a('&nbsp;<span class="glyphicon glyphicon-trash icons"></span>', Url::to(['quotation/deletes', 'id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-pjax' => '0','data' => ['confirm' => 'Are you sure want to delete?'],
                                        
                                    ]
                                );
                            
                        },
                    ]
],
    ],

 // check the configuration for grid columns by clicking button above
  
         'pjax'=>false
      
]);
    
   
}

yii\widgets\pjax::end();
?>  

<?php
$this->registerJs("$(function() {
   $('#modalQuotes').click(function(e) {
     e.preventDefault();
     $('#quote').modal('show').find('#modalquote')
     .load($(this).attr('href'));
   });
});");
?>


<?= $this->registerJs(

  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updateboqs').click(function(e){
       e.preventDefault();      
       $('#updateboq').modal('show')
                  .find('#modalupdateboq')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);