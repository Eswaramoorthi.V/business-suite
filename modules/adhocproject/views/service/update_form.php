<?php

use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\models\Customer;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use app\models\InvoiceMaster;

$servicesId=$_GET['id'];

$service = app\models\Services::find()->where(['id'=>$servicesId])->asArray()->one();
$projectId=$service['project_id'];

$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
//pr($projectname);
$name = $projectname['project_name'];




?>

<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'class' => 'invoice-form form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
        ])
?>


       
 
         <div class="customer-form form-vertical">
    <?php $form = ActiveForm::begin(); ?>
  <div class="row">
     <div class="row">
          <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo ucfirst(strtolower($name));?></small></legend></div>
     </div>
          
          <div class="row">
          
          
          <div class="col-md-2">
            <?=
            $form->field($model, 'service_date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " "])->label('Date')
            ?>
         </div>
        <div class="col-md-3">
            <?=
            $form->field($model, 'description', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
           ])->textArea([
                
                ])->label('Description')?>
        </div>
 
        
           <div class="col-md-2">
            <?=
            $form->field($model, 'working_hours', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
            ])->textInput([
                'id'=>"wh-0",
                 'onchange'=>"calc(this.id)",
                
                ])->label('Working Hour')?>
        </div>
          <div class="col-md-2">
            <?=
            $form->field($model, 'rate_working', [
                'template' => "{label}\n
                    
         <div class='col-md-12 col-xs-12'>
             {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line margin:right'],
            ])->textInput([
                'id'=>"rh-0",
                ' onchange'=>"calc(this.id)",
                ])->label('Rate/hour')?>
        </div>
        <div class="col-md-2">
            <?=
            $form->field($model, 'totalcost_working', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
              //  'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput([
                'id'=>"totalamount-0" ,
                'onchange'=>"calc(this.id)",
                ])->label('Amount')?>
       
        </div>
     
           </div>   
     
         
        <div class="form-group">
            <label class="col-md-12 col-xs-12 control-label" for=""></label>
            <div class="col-md-12 col-xs-12">
<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
            </div>
        </div>
  </div>
<?php ActiveForm::end(); ?>  
        </div>



<?= $this->registerJs("
    
        $('#services-service_date').datetimepicker({
            format: 'DD-MM-YYYY'
        });
      

", View::POS_READY); ?>
<script>
        function calc(id) {
                var idPosition = id.split('-');
                var workinghour = Number($("#wh-"+idPosition[1]).val()).toFixed(2)
                $("#wh-"+idPosition[1]).val(workinghour);
                var total = $("#rh-"+idPosition[1]).val()*workinghour;
                var tot = Number(total).toFixed(2); 
                $("#totalamount-"+idPosition[1]).val(tot)
            
                




        }
    </script>
   
<script>  
   $(document).on('click', '#submit' , function() {
        $.ajax({
            type: 'POST',
            url: $('#active-form').attr('action'),
            data : $('#active-form').serialize(),
            beforeSend : function(){
                    //do anything you want before sending the form
            },
            success : function(data){
                    alert('save');
                    //We'll replace the old form with the new form widget  
                     document.getElementById("active-form").reset();

            },
            error : function(data){
                console.log('ops');
            },
        });
        return false;
    });
  </script>

