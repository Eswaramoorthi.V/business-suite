<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Block */

$this->title = 'Update ';
$this->params['breadcrumbs'][] = ['label' => 'Blocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$model->service_date=date('d-m-Y',strtotime($model->service_date));
?>
<div class="block-update">



    <?= $this->render('update_form', [
        'model' => $model,
    ]) ?>

</div>
