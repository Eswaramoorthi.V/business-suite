<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;


$units=ArrayHelper::map(\app\models\MeasurementUnit::find()->asArray()->all(), 'id', 'name');
$category = ArrayHelper::map(\app\models\CategoryMaster::getSales(), 'id', 'description');
//pr($category);
//die;


?>

<div class="area-form form-vertical">

    <?php $form = ActiveForm::begin(['action' => ['service/create']]); ?>
   
 <div class="row"> 

        <legend class="text-info"><small>New Services</small></legend>
       
  <div class="col-md-4">
            <?=
            $form->field($model, 'name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-312 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "name"])->label(' Service Name')
            ?>
  </div>
       <div class="col-md-4">
            <?=
            $form->field($model, 'unit', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
             {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
           ])->dropDownList($units, [
                'prompt' => '  select unit',
                    // 'id'=>'groupid',
            ])->label("Unit")
            ?>
       </div>
       <div class="col-md-4">
            <?=
            $form->field($model, 'sales_price', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Sales Price"])->label('Slaes Price')?>
       </div>
     <div class="row"> 
         <div class="col-md-4">
            <?=
            $form->field($model, 'item_number', [
                'template' => "{label}\n
          <div class='col-md-12 col-xs-12'>
                {input}\n
                {hint}\n
                {error}
          </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
              //  'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => "Item No"])->label('Item Number')
            ?>
         </div>
      <div class="col-md-4">
            <?=
            $form->field($model, 'description', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textarea(['placeholder' => " description"])->label('Description')
            ?>
      </div>
            <?php
//            $form->field($model, 'family', [
//                'template' => "{label}\n
//      <div class='col-md-6 col-xs-12'>
//            {input}\n
//            {hint}\n
//            {error}
//      </div>",
//                'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label', 'style' => 'color:black'],
//               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
//            ])->textInput(['placeholder' => "family"])->label('Family');
            ?>
          <div class="col-md-4"> 
      <?=  $form->field($model, 'sales_vat', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->dropDownList($category, [
                'prompt' => '  select Vat Rated',
                    // 'id'=>'groupid',
            ])->label("VAT on Sales")
            ?>
          </div>
     </div>
         <div class="row"> 
             <div class="col-md-12">
       <?=  $form->field($model, 'notes', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
               // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textarea(['placeholder' => "notes"])->label(' Private Note')
            ?>
             </div>
        <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label" for=""></label>
            <div class="col-md-12 col-xs-6">
<?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
            </div>
        </div>
<?php ActiveForm::end(); ?>  
    </div>
</div>

 