<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

$projectId=$_GET['id'];
//pr($projectId);

//$query = app\models\Project::find()->where(['id'=>$projectId]); // where `id` is your primary key
$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
$name = $projectname['project_name'];
//pr($name);

?>

<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'class' => 'invoice-form form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
        ])
?>
<div id='msg' class='msg'></div>
<div id="invoice">
    <div class="row">

        
    <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo $name;?></small></legend></div>
        <div class="col-md-12" input-group-btn style="margin-top:21px">
            <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> Add More Manpower</button>

<div >&nbsp;</div>
        </div>
    <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
        <div class ="row">
            <div class="col-md-2" style="font-weight:bold">Service Date</div>
            <div class="col-md-3" style="font-weight:bold">Description</div>
            <div class="col-md-2" style="font-weight:bold">Total Working Hours</div>
            <div class="col-md-2" style="font-weight:bold">Rate/hour</div>
            <div class="col-md-2" style="font-weight:bold">Amount</div>
          
        </div>
    
    <div >&nbsp;</div>
        <div id="service">
            <div class="row copyinvitems details after-add-more">
                <div class="control-group">
        <div class="col-md-2"><input type="text" class="form-control datepicker" name="Services[service_date][]"><span class="glyphicon glyphicon-calendar form-control-feedback"></span></div>
        <div class="col-md-3"><textarea class="form-control" name="Services[description][]"><?php //echo $model->description;?></textarea></div>
        <div class="col-md-2"><input type="text" value="0" id="wh-0" class="form-control" name="Services[working_hours][]" onchange="calc(this.id);"></div>
        <div class="col-md-2"><input type="text" value="0.00" id="rh-0"  class="form-control" name="Services[rate_working][]" onchange="calc(this.id);"></div>
        <div class="col-md-2"><input type="text" value="0.00" id="totalamount-0" class="form-control" name="Services[totalcost_working][]" onchange="calc(this.id);"></div>

                   
                </div>

            </div>
        </div>



        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['id'=>'submit','class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
    
    <?php
    ActiveForm::end();
    ?>
</div>
</div>


    <?php
    $js = <<< JS

    var counter = 1;
    $(".add-more").click(function () {
        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp;</div><div class="control-group">' +
        '<div class="col-md-2"><input type="text" class="form-control datepicker" name="Services[service_date][]"><span class="glyphicon glyphicon-calendar form-control-feedback"></span></div>'+
        '<div class="col-md-3"><textarea class="form-control" name="Services[description][]"></textarea></div>'+
        '<div class="col-md-2"><input type="text" value="0" id="wh-' + counter + '" class="form-control" name="Services[working_hours][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-2"><input type="text" value="0.00" id="rh-' + counter + '"  class="form-control" name="Services[rate_working][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-2"><input type="text" value="0.00" id="totalamount-' + counter + '" class="form-control" name="Services[totalcost_working][]" onchange="calc(this.id);"></div>'+
       
        '<div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove" style="font-size:10px; margin-right:0px;"></i></button></div>'+
        '</div>');
        newContainer.appendTo("#service");
        counter++;
    });

    $('body').on('click','.remove',function(){ 
        $(this).parents('.control-group').remove();
    });
       
JS;
    $this->registerJs($js);
    ?>

    <script>
        function calc(id) {
                var idPosition = id.split('-');
                var workinghour = Number($("#wh-"+idPosition[1]).val()).toFixed(2)
                $("#wh-"+idPosition[1]).val(workinghour);
                var total = $("#rh-"+idPosition[1]).val()*workinghour;
                var tot = Number(total).toFixed(2); 
                $("#totalamount-"+idPosition[1]).val(tot)
   
        }
    </script>

<script>


        $(document).on('focus',".datepicker", function(){
            $(this).datetimepicker({
                defaultDate: new Date(),
                format: 'DD-MM-YYYY'

            });
        });

$("form#active-form").submit(function(e) {
    e.preventDefault();
    event.stopPropagation();
    var formData = new FormData(this);
    
    var servicedate = document.getElementsByName('Services[service_date][]');
    var description = document.getElementsByName('Services[description][]');
    var workinghours = document.getElementsByName('Services[working_hours][]');
    var rateworking = document.getElementsByName('Services[rate_working][]');
    var totalcost = document.getElementsByName(' Services[totalcost_working][]');
    
                 $("#error-message").removeClass("hide");
                 for (k=0; k<servicedate.length; k++) {
                    $(servicedate[k]).removeClass("error-highlight");
                    $(description[k]).removeClass("error-highlight");
                    $(workinghours[k]).removeClass("error-highlight");
                    $(rateworking[k]).removeClass("error-highlight");
                   
                    }
                    for (i=0; i<servicedate.length; i++) {
                        if (servicedate[i].value == '') {
                            $("#error-message").html("<p>Select Date</p>");
                            $("service_date[i]").addClass("error-highlight");
                           return false;
                        } 
                        else if (description[i].value == '') {
                            $("#error-message").html("<p>Description cannot be empty</p>");
                            $(description[i]).addClass("error-highlight");
                            return false;
                        }
                         else if (workinghours[i].value == 0) {
                            $("#error-message").html("<p>Enter Working Hours</p>");
                            $(workinghours[i]).addClass("error-highlight");
                            return false;
                        }
                         else if (rateworking[i].value == 0.00) {
                            $("#error-message").html("<p>Enter Rate/Hour</p>");
                            $(rateworking[i]).addClass("error-highlight");
                            return false;
                        }
                       
                      }
                $("#error-message").addClass("hide");
    
    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('Submitted');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();
                      window.location.href = window.location.href .split('#')[0] + "#services" ;
                      location.reload();

            },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>
