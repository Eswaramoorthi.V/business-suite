
<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;

$id=$_GET['id'];
$services = app\models\Services::find()->where(['project_id' => $id])->asArray()->all();
//pr($services);

Modal::begin([
        'header' => '<h4>Add Manpower</h4>',
        'id' => 'servic',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalservice'><div class='loader'></div></div>";
    Modal::end();
  
            
    Modal::begin([
        'header' => '<h4> Update Manpower</h4>',
        'id' => 'modalupdateservices',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalUpdate'><div class='loader'></div></div>";
    Modal::end();
   
  
?>
<div class="row">
                   <div class="col-md-12" >

                        <p class="text-right" >
                            <?=
                            Html::a(Yii::t('app', ' {modelClass}', [
                                        'modelClass' => 'Add Manpower',
                                    ]), ['service/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalServices']);
                            ?>  </p>   
                    </div>
     
     <legend class="text-info"><small>Manpower</small></legend>
</div>

<?php
 yii\widgets\Pjax::begin();
if ($dataProvider) {


    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => true, 'hiddenFromExport' => false],
        [
             'attribute' => 'Date',
            'header' => '<div style="width:120px;">Date</div>',
            'value' => function($model, $key, $index) {
                return Yii::$app->formatter->asDate($model->service_date, 'php:d-m-Y');
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '50px',
            //'noWrap' => true
        ],
        [
            'attribute' => 'Description',
            'header' => '<div style="width:220px;">Description</div>',
            'value' => function($model, $key, $index) {
               return $model->description;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '150px',
           // 'noWrap' => true
        ],
        [
            'attribute' => 'Working Hours',
            'header' => '<div style="width:120px;">Working Hours</div>',
            'value' => function($model, $key, $index) {
               return $model->working_hours;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '30px',
            //'noWrap' => true
        ],
        
         [
            'attribute' => 'Rate/Hour',
             'header' => '<div style="width:120px;">Rate/Hour</div>',
            'value' => function($model, $key, $index) {
               return $model->rate_working;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '50px',
            //'noWrap' => true
        ],
        [
            'attribute' => 'Amount',
            'header' => '<div style="width:120px;">Amount</div>',
            'value' => function($model, $key, $index) {
               return $model->totalcost_working;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            'width' => '50px',
            //'noWrap' => true
        ],
         
    ['class' => 'kartik\grid\ActionColumn',
                    'dropdown' => false,
                    'vAlign'=>'middle',
                    'header' => '<div style="width:80px;">Actions</div>',
                    'template' => '{update}{deletes}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                           
                                return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', Url::to(['service/update', 'id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'Update'),
                                        'data-pjax' => '0',
                                        'class'=>'serviceupdate',
                                        
                                    ]
                                );
                            
                            },
                        
                        
                        'deletes' => function ($url, $model, $key) {
                           
                                return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', Url::to(['service/deletes', 'id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'Delete'),'data' => ['confirm' => 'Are you sure want to delete?'],
                                        'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                        },
                    ]
    ],
    ];


     echo GridView::widget([
    'dataProvider' => $dataProvider,
    'responsiveWrap' =>false,
    'columns' => $gridColumns, // check the configuration for grid columns by clicking button above
     'summary'=>'',
         'pjax'=>true
      
]);
    
   
}

yii\widgets\pjax::end();
?>  

<?php
$this->registerJs("$(function() {
   $('#modalServices').click(function(e) {
     e.preventDefault();
     $('#servic').modal('show').find('#modalservice')
     .load($(this).attr('href'));
   });
});");
?>
<?= $this->registerJs(
  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.serviceupdate').click(function(e){
       e.preventDefault();      
       $('#modalupdateservices').modal('show')
                  .find('#modalUpdate')
                  .load($(this).attr('href'));  
   });
});
");
?>
