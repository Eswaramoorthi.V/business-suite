<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

$this->title = 'Services';
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-list';

?>
<div class="row">
    <div class="col-md-12 text-right add-label">
        <a href="<?= Url::toRoute([
            "/service/create"
        ]); ?>">
        <strong>Add Service</strong>
        </a>
    </div>
</div>
<?php
            
    Modal::begin([
        'header' => '<h4> Invoice Payment</h4>',
        'id' => 'modalupdate',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalUpdate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
if ($dataProvider) {


    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false, 'hiddenFromExport' => false],
        [
            'attribute' => 'Service Name',
            'value' => function($model, $key, $index) {
                return $model->name;
            },
            'vAlign' => 'middle',
            'format' => 'raw',
           // 'width' => '150px',
            'noWrap' => true
        ],
        [
            'attribute' => 'Unit',
            'value' => function($model, $key, $index) {
               return $model->unit;
                        
            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],
        
        [
            'attribute' => 'Description',
            'value' => function($model, $key, $index) {
               return$model->description;
            },
            'vAlign' => 'middle',
            'format' => 'raw',
           // 'width' => '150px',
            'noWrap' => true
      ],
//      [
//            'attribute' => 'Family',
//            'value' => function($model, $key, $index) {
//               return $model->family; 
//                        
//            },
//            'vAlign' => 'middle',
//            'format' => 'raw',
//            //'width' => '150px',
//            'noWrap' => true
//        ],

    [
    
    'class' => 'yii\grid\ActionColumn',
    'contentOptions' => ['style' => 'width:66px;'],
    'header'=>"Actions",
    'template' => '{view} {update} {deletes}',
    'buttons' => [
              
      
                
       'update' => function ($url, $model, $key) {
                           
                                return Html::a('<span class="glyphicon glyphicon-edit"></span>', Url::to(['service/update', 'id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'view'),
                                        'id'=>'update',
                                        
                                    ]
                                );
                            
                            },
                
                
        'deletes' => function ($url) {
            return Html::a(
                '<span class="glyphicon glyphicon-trash"></span>',
                $url, 
                [
                    'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                ]
            );
        },
    ],
],
    ];


     echo GridView::widget([
    'dataProvider' => $dataProvider,
    'responsiveWrap' =>false,
    'columns' => $gridColumns, // check the configuration for grid columns by clicking button above
     'summary'=>'',
      
]);
    
   
}
?>                                   

 <?= $this->registerJs("
    $(function(){
$('#update').click(function() {
    $('#modalupdate').modal('show')
    .find('#modalUpdate')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>