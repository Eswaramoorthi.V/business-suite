
<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\web\View;
use app\models\Variation; 

$id=$_GET['id'];
$data = \app\models\Loa::find()->where(['project_id' => $id])->asArray()->one();

        $projectid = $data['project_id'];
      
       

?>
<?php
            
    Modal::begin([
        'header' => '<h4>Update LOA</h4>',
        'id' => 'updateloa',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalloaupdate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
Modal::begin([
        'header' => '<h4>Create LOA</h4>',
        'id' => 'uploadloa',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modaluploadloa'><div class='loader'></div></div>";
    Modal::end();
?>
<div class="row">
<div class="col-md-6">
 <legend class="text-info"> <small>LOA</small> </legend>
  <?php if (!isset($projectid)) {
            ?>
        
   <p class="text-right">
        <?=Html::a(Yii::t('app', ' {modelClass}', [
                   'modelClass' => ' LOA',
                    ]), ['/loa/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalLOAupload']);
  }else{?>
   <div> &nbsp;</div>
   <div> &nbsp;</div>
    
 <?php }
?>  </p> 
 

 <?php

if ($dataProvider) {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],


        [
            'attribute' => 'description',
            'header' => '<div style="width:120px;">Description</div>',
            'value' =>  'description',
           
        ],
         [
            'attribute' => 'loa_amount',
            'header' => '<div style="width:120px;">LOA Amount</div>',
            'value' =>  'loa_amount',
           
        ],
        ['attribute'=>'Attachment',
        'header' => '<div style="width:80px;">Attachment</div>',
        'format'=>'raw',
        'value' => function($model)
        {
        return !empty($model->document)?
        Html::a('<div style="text-align:center; font-size:18px;"><span class="fa fa-download"></span></div>', ['loa/download', 'id' => $model->id]):'';

        }
        ],
  
[
                'class' => 'kartik\grid\ActionColumn',
                'mergeHeader' => false,
                'vAlign' => 'middle',
                'header' => '<div style="width:50px;">Actions</div>',
                'template' => ' {update}{deletes}',
                'buttons' => [
                   'update' => function ($url, $model, $key) {
                            
                                return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', Url::to(['loa/update', 'id' => $model->id]),
                                    [
                                        'class'=>'updateLOA',
                                        'title' => 'Edit',
                                       // 'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                        },
                    'deletes' =>function ($url, $model, $key) {
                           
                                return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', Url::to(['loa/deletes', 'id' => $model->id]),
                                    [
                                       // 'class'=>'delete',
                                        'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                                       // 'data-pjax' => '0',
                                        
                                    ]
                                );
                           
                        },   
                ],
            ],
       ],         
        
       'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
   // 'pjax' => false, // pjax is set to always true for this demo
    
    'summary' => '',
//    'panel' => [
//        'type' => GridView::TYPE_PRIMARY,
//        'heading' => '',
//    ],
    
    ]);
}

?>  
 </div>

    <div class="col-md-6">
     <legend class="text-info"><small>VO</small></legend> 
    <p class="text-right" >
        <?=Html::a(Yii::t('app', ' {modelClass}', [
                   'modelClass' => ' VO',
                    ]), ['/variation/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalvOupload']);
?>  </p> 
    <?php
            
    Modal::begin([
        'header' => '<h4>Create VO</h4>',
        'id' => 'createvo',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalvocreate'></div>";
    Modal::end();
    ?>
    <?php
            
    Modal::begin([
        'header' => '<h4>Update VO</h4>',
        'id' => 'upddatevo',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalvoupdate'></div>";
    Modal::end();
    ?>
    <?php

if ($dataProvider2) {
    echo GridView::widget([
        'dataProvider' => $dataProvider2,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],

        [
            'attribute' => 'description',
            'header' => '<div style="width:120px;">Description</div>',
            'value' =>  'description',
           
        ],
         [
            'attribute' => 'vo_amount',
            'header' => '<div style="width:120px;">VO Amount</div>',
            'value' =>  'vo_amount',
           
        ],
        ['attribute'=>'vo_document',
        'header' => '<div style="width:80px;">Attachment</div>',
        'format'=>'raw',
        'value' => function($model)
        {
        return !empty($model->vo_document)?
        Html::a('<div style="text-align:center; font-size:18px;"><span class="fa fa-download"></span></div>', ['variation/download', 'id' => $model->id]):'';

        }
        ],
  
[
                'class' => 'kartik\grid\ActionColumn',
                'mergeHeader' => false,
                'vAlign' => 'middle',
                'header' => '<div style="width:50px;">Actions</div>',
                'template' => ' {update}{deletes}',
                'buttons' => [
                   'update' => function ($url, $model, $key) {
                           
                                return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', Url::to(['variation/update', 'id' => $model->id]),
                                    [
                                        'class'=>'updateVO',
                                        'title' => 'Edit',
                                       // 'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                        },
                    'deletes' =>function ($url, $model, $key) {
                         
                                return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', Url::to(['variation/deletes', 'id' => $model->id]),
                                    [
                                       // 'class'=>'delete',
                                        'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                                       // 'data-pjax' => '0',
                                        
                                    ]
                                );
                           
                        },   
                ],
            ],
       ],         
        
       'headerRowOptions' => ['class' => 'kartik-sheet-style'],
    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
   // 'pjax' => false, // pjax is set to always true for this demo
    
    'summary' => '',
//    'panel' => [
//        'type' => GridView::TYPE_PRIMARY,
//        'heading' => '',
//    ],
    
    ]);
}

?>      
    </div>
</div>

<?php
$this->registerJs("$(function() {
   $('#modalLOAupload').click(function(e) {
     e.preventDefault();
     $('#uploadloa').modal('show').find('#modaluploadloa')
     .load($(this).attr('href'));
   });
});", View::POS_READY);
 ?>

<?= $this->registerJs(

  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updateLOA').click(function(e){
       e.preventDefault();      
       $('#updateloa').modal('show')
                  .find('#modalloaupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
 ?>
<?php
$this->registerJs("$(function() {
   $('#modalvOupload').click(function(e) {
     e.preventDefault();
     $('#createvo').modal('show').find('#modalvocreate')
     .load($(this).attr('href'));
   });
});", View::POS_READY);
 ?>
<?php
$this->registerJs("$(function() {
   $('.updateVO').click(function(e) {
     e.preventDefault();
     $('#upddatevo').modal('show').find('#modalvoupdate')
     .load($(this).attr('href'));
   });
});", View::POS_READY);
 ?>