<?php

use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;



$projectId=$_GET['id'];

$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
$name = $projectname['project_name'];
//pr($name);

?>

<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'class' => 'invoice-form form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
        ])
?>
<div id='msg' class='msg'></div>
<div id="loa-form">
    <div class="row">

        
    <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo ucfirst(strtolower($name));?></small></legend></div>
       
    <div class ="row"> 
    <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
     <div class="col-md-4">
           <?=
            $form->field($loa, 'description', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput()->label('Description')
            ?> 
        </div>
       
       
   <div class="col-md-4">
        <?=
        $form->field($loa, 'loa_amount', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textInput([])->label('LOA Amount')
        ?>
    </div>
    <div class="col-md-4">  
      <?php $documentpath=\Yii::$app->params['domain_url'].$loa->document;?>
    <?php if(is_null($loa->document) || empty($loa->document)): ?>
        <?= $form->field($loa, 'document', [
          'template' => "{label}\n
          <div class='col-md-12'>
                {input}\n
                {hint}\n
                {error}
          </div>",
          'labelOptions' => [ 'class' => 'col-md-12 col-xs-12 control-label', 'style'=>'text-align:left'],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php else: ?>
  
    <?= $form->field($loa, 'document', [
          'template' => "{label}\n
          <div class='col-md-12 col-xs-12'>
                {input}\n
                <div class='file-preview'>
                  <strong> Current File </strong> : 
                    <div class='clearfix'></div>
                     <a base href='$documentpath'target='_blank'> $loa->document</a>
                
                {hint}\n
                {error}
          </div></div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php endif; ?>
</div>
    </div>
    
        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($loa->isNewRecord ? 'Submit' : 'Update', ['id'=>'submit','class' => $loa->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
   
    <?php
    ActiveForm::end();
    ?>
</div>
 </div>
   

  
<script> 
$("form#active-form").submit(function(e) {
    e.preventDefault(); 
    event.stopPropagation();
    
    $("#loa-description").removeClass("error-highlight");
    $("#loa-loa_amount").removeClass("error-highlight");
    
    var formData = new FormData(this);
    
        if($('#loa-description').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Description cannot be Empty</p>");
            $("#loa-description").addClass("error-highlight");
            return false;
        }
        else if($('#loa-loa_amount').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Amount cannot be Empty</p>");
            $("#loa-loa_amount").addClass("error-highlight");
            return false;
       }
      
       else {
           $("#error-message").addClass("hide");
        }

    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('LOA Created');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();
                      window.location.href = window.location.href .split('#')[0] + "#loa" ;
                      location.reload();

                     
            },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>