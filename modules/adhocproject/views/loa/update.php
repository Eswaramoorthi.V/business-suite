<?php

use yii\web\View;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$loaId=$_GET['id'];

$LOA = app\models\Loa::find()->where(['id'=>$loaId])->asArray()->one();
$projectId=$LOA['project_id'];

$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
$name = $projectname['project_name'];

?>

<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'class' => 'invoice-form form-horizontal',
                'enctype' => 'multipart/form-data'
            ],
        ])
?>
<div id='msg' class='msg'></div>
   <div class="customer-form form-vertical">
    <?php $form = ActiveForm::begin(); ?>
  <div class="row">
       <div class="row">
          <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo ucfirst(strtolower($name));?></small></legend></div>
     </div>
     <div class="row">
         
         <div class="col-md-4">
            <?=
            $form->field($model, 'description', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
              
            ])->textInput(['placeholder' => " "])->label('Description')
            ?>
         </div>
       <div class="col-md-4">
            <?=
            $form->field($model, 'loa_amount', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
              
            ])->textInput(['placeholder' => " "])->label('LOA Amount')
            ?>
         </div>
        <div class="col-md-4">
           <?php $documentpath=\Yii::$app->params['domain_url'].$model->document;?>
    <?php if(is_null($model->document) || empty($model->document)): ?>
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9'>
                {input}\n
                {hint}\n
                {error}
          </div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php else: ?>
  
 
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9 col-xs-9'>
                {input}\n
                <div class='file-preview'>
                  <strong> Current File </strong> : 
                    <div class='clearfix'></div>
                     <a base href='$documentpath'target='_blank'> $model->document</a>
                
                {hint}\n
                {error}
          </div></div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php endif; ?>
        </div>
     
           </div>   
     
         
        <div class="form-group">
            <label class="col-md-12 col-xs-12 control-label" for=""></label>
            <div class="col-md-12 col-xs-12">
             <input type="hidden" name="current_document" value="<?php echo $model->document; ?>">
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
            </div>
        </div>
  </div>
<?php ActiveForm::end(); ?>  
        </div>

   
<script> 
    
$("form#active-form").submit(function(e) {
    e.preventDefault();    
    var formData = new FormData(this);

    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('LOA Updated');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();

            },
        cache: false,
        contentType: false,
        processData: false
    });
});
</script>

