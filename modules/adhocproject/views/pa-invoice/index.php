<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\Customer;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\PaInvoiceMaster;
use app\models\PaInvoice;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;
use yii\web\View;
use app\components\Helper;



//$this->title = 'Invoice';
$this->params['breadcrumbs'][] = 'Payment Application';
//$this->params['title_icon'] = 'fa';
//$this->params['title_icon'] = 'fa-file-pdf-o';
$id=$_GET['id'];


?>
<div class="row">
                   <div class="col-md-12"  >

                        <p class="text-right"  >
                            <?=
                            Html::a(Yii::t('app', ' {modelClass}', [
                                        'modelClass' => 'Add Payment Application',
                                    ]), ['pa-invoice/create', 'id' => $model->id], ['class' => 'btn btn-success', 'id' => 'modalPaInvoice']);
                            ?>  </p>   
                    </div>
   
     <legend class="text-info"><small>Payment Application</small></legend>
</div>

<?php
 Modal::begin([
        'header' => '<h4>Create Payment Application</h4>',
        'id' => 'createPal',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalpacreate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
            
    Modal::begin([
        'header' => '<h4> Payment Receipt</h4>',
        'id' => 'parecepitform',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='parecepitContainer'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
            
    Modal::begin([
        'header' => '<h4>Edit</h4>',
        'id' => 'updatepapInvoice',
        'size' => 'modal-lg',
        'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
    ]);

    echo "<div id ='modalupdate'><div class='loader'></div></div>";
    Modal::end();
    ?>
<?php
Modal::begin([
    'header' => '<h4>Update</h4>',
    'id' => 'recepitmodalpa',
    'size' => 'modal-lg',
]);

echo "<div id ='parecepitContainer'><div class='loader'></div></div>";
Modal::end();
?>
  
<?php
 

 if ($dataProvider) {
   //  pr($dataProvider);
   
       
    echo GridView::widget([
        
        'dataProvider' => $dataProvider,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'class' => 'kartik\grid\ExpandRowColumn', 'mergeHeader' => false,
                'width' => '50px',
                'value' => function ($model, $key, $index, $column) {
                    return GridView::ROW_COLLAPSED;
                },
                'detail' => function ($model, $key, $index, $column) {
                    $query = \app\models\PaInvoiceMaster::find()
                        ->joinWith('customer')
                        ->joinWith('paInvoice')
                        ->joinWith('payment')
                        ->joinWith('project')
                        ->where(['pa_invoice_master.id' => $model->id])
                        ->asArray()->one();
        $customerId = $query['customer_id'];
        $projectId = $query['project_id'];
        $invoiceId = $query['id'];
        $papaymentId = $query['id'];

        $customerInfo = Customer::find()
                        ->where(['customer.id' => $customerId])
                        ->asArray()->one();

        $projectInfo = \app\models\Project::find()
                        ->where(['project.id' => $projectId])
                        ->asArray()->one();

        $papaymentInfo = \app\models\PaInvoicePayment::find()
                        ->where(['pa_invoice_payment.invoice_id' => $papaymentId])
                        ->asArray()->all();

        $productInfo = \app\models\PaInvoice::find()
                        ->where(['pa_invoice.invoice_id' => $invoiceId])
                        ->asArray()->all();

                    return Yii::$app->controller->renderPartial('/pa-invoice/_view', [
                        'model' => $query,
                        'customerInfo' => $customerInfo,
                        'projectInfo' => $projectInfo,
                       'papaymentInfo' => $papaymentInfo,
                        'productInfo'=>$productInfo,]);
                },
                'headerOptions' => ['class' => 'kartik-sheet-style'],
                'expandOneOnly' => true
            ],
            [
                'attribute' => 'invoice_number',
                'header' => '<div style="width:80px;">#No.</div>',
                'value' => 'invoice_number',
            ],
           

            [
                'attribute' => 'invoice_name',
                'header' => '<div style="width:100px;">Invoice Name</div>',
                'value' => 'invoice_name',
            ],
            [
                'attribute' => 'invoice_date',
                'header' => '<div style="width:50px;">Date</div>',
                'value' => function($model, $key, $index) {
                return Yii::$app->formatter->asDate($model->invoice_date, 'php:d-m-Y');
            },
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
                'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],
             [
                'attribute' => 'due_date',
                'header' => '<div style="width:200px;">Due Date</div>',
                'format'=>'html',
                'value' => function($model, $key, $index) {
                $papayment = app\models\PaInvoicePayment::find()->where(['invoice_id'=>$model['id']])->asArray()->all();
                //pr($payment);
                  $currentdate= date('y-m-d');
                  //pr($currentdate);
                  $duedate=Yii::$app->formatter->asDate($model['due_date'],'php:Y-m-d');
                  $date1= new DateTime($currentdate);
                  $date2= new DateTime($duedate);
                  $diffdate=$date1->diff($date2);
                  $day = ($diffdate->format('%a') > 1)?$diffdate->format('%a')." days":$diffdate->format('%a')." day";

               if ((strtotime($model->due_date) < strtotime('now')) and !empty($papayment) ){
                   return Yii::$app->formatter->asDate($model->due_date, 'php:d-m-Y');
                }
               elseif((strtotime($model->due_date) < strtotime('now')) ){
                   return Yii::$app->formatter->asDate($model->due_date, 'php:d-m-Y').' '.'<span style="color:#FF0000;font-size:10px;">Overdue on '.$day.'</span>';
                }
                else{
                    return Yii::$app->formatter->asDate($model->due_date, 'php:d-m-Y');

                }},
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
                'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],
            [
                'attribute' => 'subtotal',
                'header' => '<div style="width:100px;">Total Amount</div>',
                //'format' => ['currency', 'INR'],
                'format' => ['decimal', 2],
                'value' => 'subtotal',
            ],
            [   'attribute'=>'PaPayment',
                'header' => '<div style="width:60px;">Receipt</div>',
                'format'=>'raw',
                'value' => function($model)
                {
                return
                Html::a('<div style="text-align:center; font-size:16px;"><span class="fa fa-euro"></span></div>', ['pa-invoice/papayment', 'id' => $model->id],
                        [
                                       'data-pjax' => '0',
                                        'class'=>'papayment',

                                    ]);

                }
            ],
            [
                'class' => 'kartik\grid\ActionColumn',
                'mergeHeader' => false,
                'vAlign' => 'middle',
                'header' => '<div style="width:80px;">Actions</div>',
                'template' => '{painvoice}{update}{deletes}',
                'buttons' => [
                     'update' => function ($url, $model, $key) {
                            
                                return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', Url::to(['pa-invoice/update', 'id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'Edit'),
                                        'data-pjax' => '0',
                                        'class'=>'updatepinv',
                                        
                                    ]
                                );
                            
                            },
                    'deletes' => function ($url, $model, $key) {
                            
                                return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', Url::to(['pa-invoice/deletes', 'id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'Delete'),
                                        'data-pjax' => '0','data' => ['confirm' => 'Are you sure want to delete?'],
                                        
                                    ]
                                );
                            
                            },
                    'painvoice' => function ($url, $model, $key) {
                          
                                return Html::a('<span class="fa fa-print icons"></span>', Url::to(['pa-invoice/painvoice', 'id' => $model->id]),
                                    [
                                        'title' => Yii::t('app', 'PDF'),
                                        'data-pjax' => '0',
                                        
                                    ]
                                );
                            
                            },
                           
                ],
            ]],
'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [
                'columns' => [
                  
                   
                ],],

        ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => false, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '',
        ],
        'persistResize' => false,
        'exportConfig' => [GridView::EXCEL => [
                'filename' => 'Payment Application History',
            ],
            GridView::PDF => [
                'filename' => 'Payment Application History',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'config' => [
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                ],
            ]]
        


  // check the configuration for grid columns by clicking button above
    

      
]);
}

?>                                   
<?= $this->registerJs(
  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.papayment').click(function(e){
    console.log('inside');
       e.preventDefault();      
       $('#parecepitform').modal('show')
                  .find('#parecepitContainer')
                  .load($(this).attr('href'));  
   });
});
");
?>

<?php
$this->registerJs("$(function() {
   $('#modalPaInvoice').click(function(e) {
     e.preventDefault();
     $('#createPal').modal('show').find('#modalpacreate')
     .load($(this).attr('href'));
   });
});");
?>



<?= $this->registerJs(

  "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updatepinv').click(function(e){
       e.preventDefault();      
       $('#updatepapInvoice').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY); ?>

<?= $this->registerJs("
    $(function(){
$('.updatereciptpa').click(function() {
    $('#recepitmodalpa').modal('show')
    .find('#parecepitContainer')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>