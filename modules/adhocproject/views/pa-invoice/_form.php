<?php


use yii\widgets\ActiveForm;
use yii\helpers\Html;

$projectId=$_GET['id'];
$projectname = app\models\Project::find()->where(['id'=>$projectId])->asArray()->one();
$name = $projectname['project_name'];
$category = []; //ArrayHelper::map(\app\models\CategoryMaster::getPayement(), 'id', 'description');
//$vatcategory=\Yii::$app->params['VATRate'];
//$customInvoiceType=\Yii::$app->params['customInvoiceType'];

$getLastRecord=\app\models\PaInvoiceMaster::find()->orderBy(['id' => SORT_DESC])->asArray()->one();
//pr1($getLastRecord);
if(!empty($getLastRecord)){
    $paInvoiceId=$getLastRecord['id']+1;
}else{
    $paInvoiceId=1;
}

$date= date('dm');

?>


<div class="customer-form form-horizontal">
    <?php $form = ActiveForm::begin(['id'=>'paInvoice_form']); ?>
       <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
    <div class="row">
         <div class="row">
          <div class="col-md-12" style="font-size: 15px;font-weight: bold">  <legend class="text-info"><small> <?php   echo ucfirst(strtolower($name));?></small></legend></div>
         </div>
   <div class ="row">   
<!--         <div class="col-md-3">
           <?=
$form->field($paInvoiceMaster, 'invoice_number', [
                'template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput(['value'=>'CI'.$date ."-".$paInvoiceId])->label('Invoice Number')
            ?> 
        </div>-->
        
        <div class="col-md-4">
           <?=
            $form->field($paInvoiceMaster, 'invoice_name', [
                'template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput()->label('Invoice Name')
            ?> 
        </div>
       
        <div class="col-md-2">
            <?=
            $form->field($paInvoiceMaster, 'invoice_date', ['template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput()->label('Date')
            ?> 
        </div>
       <div class="col-md-2">
            <?=
            $form->field($paInvoiceMaster, 'due_date', ['template' => "{label}\n
      <div class='col-md-10 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput([])->label('Due Date')
            ?> 
        </div>

        
        <div class="col-md-4">
        <?=
        $form->field($paInvoiceMaster, 'notes', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textarea(['rows'=>3])->label('Remarks')
        ?>
    </div>
       
    </div>


<div>&nbsp</div>  
<div class="col-md-12"><legend class="text-info"><small>Items</small></legend></div>
<div class ="row">
    <div class="col-md-5" style="font-weight:bold">Description</div>
    <div class="col-md-1" style="font-weight:bold">Quantity</div>
    <div class="col-md-1" style="font-weight:bold">Unit</div>
    <div class="col-md-2" style="font-weight:bold">Price</div>
    <div class="col-md-2" style="font-weight:bold">Total</div>
    <div class="col-md-1" style="font-weight:bold">&nbsp;</div>
</div>
<div id="product">
<div class="row copyinvitems details after-add-more">
    <div class="control-group">
    <div class="col-md-5"><textarea id="PaymentApplication-description" class="form-control required descrip" name="PaymentApplication[description][]"></textarea></div>
    <div class="col-md-1"><input type="text" value="0" id="quantity-0" class="form-control qnty" name="PaymentApplication[quantity][]" onchange="calc(this.id);"></div>
    <div class="col-md-1"><input type="text" id="invoice-unit" class="form-control required unit" name="PaymentApplication[unit][]"></div>
    <div class="col-md-2"><input type="text" value="0.00" id="price-0" class="form-control" name="PaymentApplication[price][]" onchange="calc(this.id);"></div>
    <div class="col-md-2"><input type="text" class="form-control totalamount" readOnly = "true" value="0.00" id="totalamount-0" name="PaymentApplication[total_amount][]" onchange="calc(this.id);"></div>
   
  </div>
</div>
</div>
        <div class="col-md-12" input-group-btn style="margin-top:21px">
            <div class="row">
                <div class="col-md-2">
                    <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i> ADD</button>
                </div>
            </div>

                <div class="col-md-offset-5 col-md-6">
            <div class='col-md-12  '>
                <div class='col-md-3 col-xs-12 '>
                <?= $form->field($paInvoiceMaster, 'subtotal_label')->textInput(["class" => "form-control text-bold", "value" => "Net Amount"])->label('&nbsp;') ?>
            </div>
            <div class='col-md-7 col-xs-12'>
            <?=
            $form->field($paInvoiceMaster, 'subtotal', [
                'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label',]
            ])->textInput(
                    [
                        'id' => 'subtotal',
                        'readOnly' => true,
                        'placeholder' => '0.00',
                        'class' => 'form-control subtotal',
                        'onchange' => 'calc(this.id);',
                    ]
                    )->label('&nbsp;')
            ?>
                </div>
            </div>

<!--                    <div class="col-md-12"><legend class="text-info"><small></small></legend></div>-->

<!--            --><?php //echo
//            $form->field($pa_invoiceMaster, 'total_amount', [
//                'template' => "{label}\n
//            <div class='col-md-7 col-xs-12'>
//            {input}\n
//             </div>",
//                'labelOptions' => ['class' => 'col-md-3 col-xs-12 control-label']
//            ])->textInput([
//                'readOnly' => true,
//                'placeholder' => '0.00',
//                'id' => 'totalamount',
//                 //'class' => 'form-control total'
//
//                ])->label('Grand Total')
//            ?>
                </div>
<!--        </div>-->
<!--        <div>&nbsp;</div>-->
<!---->
<!--    </div>-->

<div>&nbsp;</div>
<div class="col-md-12"><legend class="text-info"><small></small></legend></div>
    <div class="row">
<div class="col-md-12"> 
    <div class="form-group">
        <div class="col-md-12 col-xs-12">
<?= Html::submitButton($paInvoiceMaster->isNewRecord ? 'Submit' : 'Update', ['class' => $paInvoiceMaster->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
<?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
</div>
    </div>
</div>
</div>
<?php
ActiveForm::end();
?>
</div>

<?php 
$js = <<< JS
        
    var counter = 1;
    $(".add-more").click(function () {
        var newContainer = $(document.createElement('div'));
        newContainer.after().html('<div class="clear">&nbsp</div><div class="control-group col-md-12 padding-0">' +
        '<div class="col-md-5"><textarea id="PaymentApplication-description" class="form-control" name="PaymentApplication[description][]"></textarea></div>'+
        '<div class="col-md-1"><input type="text" value="0" id="quantity-' + counter + '" class="form-control" name="PaymentApplication[quantity][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1"><input type="text" id="invoice-unit" class="form-control required unit" name="PaymentApplication[unit][]"></div>'+
        '<div class="col-md-2"><input type="text" value="0.00" id="price-' + counter + '" class="form-control" name="PaymentApplication[price][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-2"><input type="text" class="form-control totalamount" value="0.00" id="totalamount-' + counter + '" class="form-control" name="PaymentApplication[total_amount][]" onchange="calc(this.id);"></div>'+
        '<div class="col-md-1 rmbtn" style="margin-top:5px"><button class="btn  new-item-remove remove" type="button"><i class="glyphicon glyphicon-trash" style="font-size:10px; margin-right:0px;"></i></button></div>'+
        '<div class="clear">&nbsp</div>'+
            '</div>');
        newContainer.appendTo("#product");
        counter++;
    });

    
    $('#painvoicemaster-invoice_date').datepicker({
       format: 'dd-mm-yyyy'
    });
    $('#painvoicemaster-invoice_date').datepicker('setDate', new Date());
    $('#painvoicemaster-due_date').datepicker({
       format: 'dd-mm-yyyy'
    });
                
JS;
$this->registerJs($js);

?>


    <script>
    function calc(id) {
        // alert(id);
        if(id!="remove"){
            var idPosition = id.split('-');
            var price = Number($("#price-"+idPosition[1]).val()).toFixed(2)
            $("#price-"+idPosition[1]).val(price);
            var total = $("#quantity-"+idPosition[1]).val()*price;
            var tot = Number(total).toFixed(2);
            $("#totalamount-"+idPosition[1]).val(tot)
        }


            var subTotal = 0;
            $(".totalamount").each(function () {
                subTotal += parseFloat($(this).val());
            });
            var subTotalFormatted = Number(subTotal).toFixed(2);

            $("#subtotal").val(subTotalFormatted);


        var subtotal = Number($("#subtotal").val()).toFixed(2)
        $("#subtotal").val(subtotal);
        var totamntbeforeVat;
        totamntbeforeVat = subtotal;
        var discrate = $("#discountrate").val();
        if(discrate>0) {
            var discamount = $("#discountrate").val()*subtotal/100;
            var discountformatted = Number(discamount).toFixed(2)
            $("#discountamount").val(discountformatted);
            totamntbeforeVat = subtotal- discountformatted;
        }


        //console.log("Before vat amout",totamntbeforeVat);
        var vat = Number($("#vat").val()).toFixed(2)
        $("#vat").val(vat);
        var total, vatAmount;

        if(vat>0) {
            vatAmount = totamntbeforeVat*(vat)/100;
        } else {
            vatAmount = 0;
        }

        total = parseFloat(totamntbeforeVat) + parseFloat(vatAmount);
        //console.log("Vat value",vat);
        //console.log("Total amount",total);
        var tot = Number(total).toFixed(2);
        $("#totalamount").val(tot);

    }

    $('body').on('click','.remove',function (){
        $(this).parents('.control-group').parent().remove();
        calc('remove');

    });

</script>
<script>
    $("form#paInvoice_form").submit(function(e) {
        // alert("inside");
        var decription = document.getElementsByName('Payment Application[description][]');
        var quantity = document.getElementsByName('Payment Application[quantity][]');
        var price = document.getElementsByName('Payment Application[price][]');

        $("#paInvoicemaster-invoice_name").removeClass("error-highlight");
        $("#paInvoicemaster-invoice_date").removeClass("error-highlight");

        if($('#paInvoicemaster-invoice_name').val()==='') {
            //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Invoice Name cannot be Empty</p>");
            $("#paInvoicemaster-invoice_name").addClass("error-highlight");
            return false;
        }

        else if($('#paInvoicemaster-invoice_date').val()==='') {
            //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Date cannot be Blank</p>");
            $("#paInvoicemaster-invoice_date").addClass("error-highlight");
            return false;
        }



        $("#error-message").removeClass("hide");
        for (k=0; k<decription.length; k++) {
            $(decription[k]).removeClass("error-highlight");
            $(quantity[k]).removeClass("error-highlight");
            $(price[k]).removeClass("error-highlight");
        }
        for (i=0; i<decription.length; i++) {
            if (decription[i].value == '') {
                $("#error-message").html("<p>Description can not be blank</p>");
                $(decription[i]).addClass("error-highlight");
                return false;
            } else if (quantity[i].value == 0) {
                $("#error-message").html("<p>Quantity can not be 0 value</p>");
                $(quantity[i]).addClass("error-highlight");
                return false;
            }else if (price[i].value == 0) {
                $("#error-message").html("<p>Price can not be 0 value</p>");
                $(price[i]).addClass("error-highlight");
                return false;
            }
        }
        $("#error-message").addClass("hide");

    });


</script>

