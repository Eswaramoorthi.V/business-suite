<?php

namespace app\modules\estimate\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\mpdf\Pdf;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\controllers\BaseController;
use app\modules\estimate\models\Settings;
use app\models\SettingsTree;
use app\models\EstimateDetails;
use app\models\EstimateMaster;
use app\models\MaterialMaster;
use app\models\MaterialUnitCost;
use app\models\LabourMaster;
use app\models\LabourUnitCost;
use app\modules\project\models\Project;
use app\modules\estimate\models\Equipment;
use app\models\ProjectEstimateMaster;
use app\models\ProjectEstimate;
use app\models\Tree;
use app\components\MenuHelper;

class EstimateMasterController extends BaseController
{
      /**
     * Lists all EstimateMaster models.
     * @return mixed
     */
    public function actionMaster()
    {
        $route=$this->route;
        $actionList = MenuHelper::getActionHelper($route);
       // pr($actioList);
       $visibleList = MenuHelper::getActionVisibleHelper($route);

         $searchModel = new EstimateMaster();
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->pagination = ['PageSize'=>10];
        return $this->render('master', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'actionList'=>$actionList,
                    'visibleList'=>$visibleList
        ]);
 
    }

    public function actionCreate()
    {
       
       $activityId=isset($_REQUEST['activity_id'])?$_REQUEST['activity_id']:"";
        $subactivityId=isset($_REQUEST['sub_activity_id'])?$_REQUEST['sub_activity_id']:"";
        $subactivityData=[];
        $description="";
        if($activityId!=''){
            $list = \app\models\SettingsTree::find()
                ->select(['id','root','lft','rgt','lvl','name','code','description'])
                ->where(['!=', 'lvl', 0])->andWhere(['root'=>2])
                ->orderBy('root, lft')->asArray()->all();
        
            $dataformatChange=[];
            $parent="";
            foreach ($list as $d) {
                // pr($d);
                if ($d['lvl'] == 1) {
                    $dataformatChange[$d['id']]=$d;
                    $parent=$d['id'];
                }
                else {
                    $dataformatChange[$parent]['sublevel'][]=$d;
                }
            }
            foreach ($dataformatChange as $key => $valueData) {
                if($activityId==$key){
                    foreach ($valueData['sublevel'] as $value) {
                        $subactivityData[$value['id']]=$value['name'];
                        if($subactivityId==$value['id']){
                            //pr($value);
                           $description=$value['description'];
                        }
        
                    }
                }
            }
        }
        
 if (Yii::$app->request->post()){
            $request = Yii::$app->request->post();
            $activityId = $request['activity'];
            $subActivityId = $request['subActivity'];
           $unitId=$request['unit_id'];
            $description=$request['description'];
            $data= $request['formdata'];
            $estimateMaster = new EstimateMaster();
            $estimateMaster->activity_id=$activityId;
            $estimateMaster->sub_activity_id=$subActivityId;
          $estimateMaster->description=$description;
          $estimateMaster->unit_id=$unitId;
            if($estimateMaster->save()){
                $masterId = $estimateMaster->id;
                foreach ($data as $value1) {
                    // pr($value1);
                    $estimateDetails = new EstimateDetails;
                    $estimateDetails->estmate_id = $masterId;
                    if(!empty($value1[0])){
                        // pr($value1[0]);
                        $estimateDetails->resource_type = $value1[0];
                        $estimateDetails->resource_id=$value1[1];
                            $estimateDetails->last_purchased_cost = isset($value1[2]) ? $value1[2] : 0;
                            $estimateDetails->unit_type = isset($value1[3]) ? $value1[3] : '';
                            $estimateDetails->quantity = isset($value1[4]) ? $value1[4] : 1;
                            $estimateDetails->actual_cost = isset($value1[5]) ? $value1[5] : 0;
                            $estimateDetails->average_weighted_cost = isset($value1[6]) ? $value1[6] : 0;
                            $estimateDetails->formula = isset($value1[7]) ? $value1[7] : 0;
                            $estimateDetails->percentage = isset($value1[8]) ? $value1[8] : 0;
                       
                            if (!$estimateDetails->save()) {
                                pr($estimateDetails->getErrors());
                            }
                    }
                }
            }
            return $this->redirect(['master']);
        }
        else {
            $data=$this->GetResourceData();
            $getActivityData = \app\models\SettingsTree::find()->where(['=', 'root', 2])->andWhere(['=', 'lvl', 1])->asArray()->all();
            $activityList = ArrayHelper::map($getActivityData, 'id', 'name');
           

            $estimateType = Tree::find()->where(['lvl'=>0])->asArray()->all();
$estimateTypeList =[];
$listDataVal = [];
foreach ($estimateType as $value) {
    $eType =[];
    $eType['id'] = $value['id'];
    $eType['name'] = $value['name'];
    $estimateTypeList[]=$eType;
    $listDataVal[$value['id']]=$value['name'];
}

$itemType = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();
$itemTypeList =[];
foreach ($itemType as $value) {
    $eType1 =[];
    $eType1['id'] = $value['id'];
    $eType1['name'] = '['.$value['code'].'] '.$value['name'];
    $itemTypeList[]=$eType1;
}
$getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
$unitType=[];
       foreach($getUnitTypeList as $value){
        $eTypeList =[];
        $eTypeList['id'] = $value['id'];
        $eTypeList['name'] = $value['name'];
        $unitType[]=$eTypeList;
       }

       $unitTypeList = ArrayHelper::map($unitType, 'id', 'name');
// pr($unitTypeList);
            return $this->render('create',['resourcedata'=>$data,
            'unitType' => $unitType,
            'unitTypeList'=>$unitTypeList,
            'activityList'=>$activityList,'estimateTypeList'=>$estimateTypeList,
            'listDataVal'=>$listDataVal,'itemTypeList'=>$itemTypeList,'subactivityData'=>$subactivityData,'description'=>$description,'activityId'=>$activityId,'subactivityId'=>$subactivityId]);
           
        }
    }

    public function actionUpdate($id) 
    {
        $estimateType = Tree::find()->where(['lvl'=>0])->asArray()->all();

        $listDataVal = [];
        $updateVal = [];
        foreach ($estimateType as $value) {

            // $listDataVal[$value['id']]=$value['name'];
            $updateVal[$value['name']]=$value['id'];
        }
      
        // $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
        // $listDataValUnit = [];
        // $updatevalUnit = [];
        //         foreach ($getUnitTypeList as $value) {
        //             $listDataValUnit[$value['id']]=$value['name'];
        //             $updatevalUnit[$value['name']]=$value['id'];
        //         }
                // pr($updatevalUnit);

        $activityId=isset($_REQUEST['activity_id'])?$_REQUEST['activity_id']:"";
        $subActivityList = [];

if($activityId!=''){
    $list = \app\models\SettingsTree::find()
        ->select(['id','root','lft','rgt','lvl','name','code'])
        ->where(['!=', 'lvl', 0])->andWhere(['root'=>2])
        ->orderBy('root, lft')->asArray()->all();

    $dataformatChange=[];
    $parent="";
    foreach ($list as $d) {
        // pr($d);
        if ($d['lvl'] == 1) {
            $dataformatChange[$d['id']]=$d;
            $parent=$d['id'];
        }
        else {
            $dataformatChange[$parent]['sublevel'][]=$d;
        }
    }
    $subActivityList=[];
    foreach ($dataformatChange as $key => $valueData) {
        if($activityId==$key){
            foreach ($valueData['sublevel'] as $value) {
                $subActivityList[$value['id']]=$value['name'];

            }
        }
    }

    }
   

        //  pr($activityId);
        if (Yii::$app->request->post()){
            $request = Yii::$app->request->post();
            $activityId = $request['activity'];
            $subActivityId = $request['subActivity'];
            $unitId= $request['unit_id'];
            $description=$request['description'];
            $data= $request['formdata'];
            $estimateMaster = EstimateMaster::findOne($id);
            $estimateMaster->activity_id=$activityId;
            $estimateMaster->sub_activity_id=$subActivityId;
            $estimateMaster->unit_id=$unitId;
            $estimateMaster->description=$description;
           
            if($estimateMaster->save()){
                EstimateDetails::deleteAll(['estmate_id'=>$id]);
                foreach ($data as  $value1) {
                //    pr($value1);
                    $estimateDetails = new EstimateDetails;
                    $estimateDetails->estmate_id = $id;
                    if(!empty($value1[0])){
              $estimateDetails->resource_type = $value1[0];
                            $estimateDetails->resource_id=$value1[1];
                            $estimateDetails->last_purchased_cost = isset($value1[2]) ? $value1[2] : 0;
                            $estimateDetails->unit_type = isset($value1[3]) ? $value1[3] : '';
                            $estimateDetails->quantity = isset($value1[4]) ? $value1[4] : 1;
                            
                            $estimateDetails->actual_cost = isset($value1[5]) ? $value1[5] : 0;
                            $estimateDetails->average_weighted_cost = isset($value1[6]) ? $value1[6] : 0;
                            $estimateDetails->formula = isset($value1[7]) ? $value1[7] : '';
                            $estimateDetails->percentage = isset($value1[8]) ? $value1[8] : 0;
                            if (!$estimateDetails->save()) {
                                pr($estimateDetails->getErrors());
                            }
                    }
                }
            }
            return $this->redirect(['master']);
        }
        else {
            $data=$this->GetResourceData();
            $estimateMaster = EstimateMaster::find()->where(['id'=>$id])->asArray()->one();
            $estimateDetails = EstimateDetails::find()->where(['estmate_id'=>$id])->asArray()->all();
            $arrayData = [];
            $totalCost = 0;
            $materialCost = 0;
            $labourCost = 0;
            $equipmentCost = 0;
            $profitCost = 0;
            $overheadCost = 0;
            if(!empty($estimateDetails)){
                foreach ($estimateDetails as $value) {                   
                    $resData = [];

                    $resData[0]=$value['resource_type'];
                    $resData[1]=$value['resource_id'];
                    $resData[2]=$value['last_purchased_cost'];
                    $resData[3]=$value['unit_type'];
                    $resData[4]=$value['quantity'];
                    $resData[5]=$value['actual_cost'];
                    $resData[6]=$value['average_weighted_cost'];
                    $resData[7]=$value['formula'];
                    $resData[8]=$value['percentage'];

                    $arrayData[]=$resData;
                    
                    $totalCost+=$value['actual_cost'];
                    
                    if(strtolower($value['resource_type'])==1){
                        $materialCost+=$value['actual_cost'];
                    } else if(strtolower($value['resource_type'])==2){
                        $labourCost+=$value['actual_cost'];
                    } else if(strtolower($value['resource_type'])==3){
                        $equipmentCost+=$value['actual_cost'];
                    } else if(strtolower($value['resource_type'])==5){
                        $profitCost+=$value['actual_cost'];
                    } else if(strtolower($value['resource_type'])==4){
                        $overheadCost+=$value['actual_cost'];
                    }
                }
            }
            $list  = empty($arrayData) ? [ [ 1, '', '0.00', '0.00', '0.00' ] ] : $arrayData;
// pr($list);

            $getActivityData = \app\models\SettingsTree::find()->where(['=', 'root', 2])->andWhere(['=', 'lvl', 1])->asArray()->all();
            $activityList = ArrayHelper::map($getActivityData, 'id', 'name');
            $estimateType = \app\models\Tree::find()->where(['lvl'=>0])->asArray()->all();
            $estimateTypeList =[];
            $listDataVal = [];
            $activityId=isset($_REQUEST['activity_id'])?$_REQUEST['activity_id']:"";
            foreach ($estimateType as $value) {
                $eType =[];
                $eType['id'] = $value['id'];
                $eType['name'] = $value['name'];
                $estimateTypeList[]=$eType;
                $listDataVal[$value['id']]=$value['name'];
            }
            $itemType = Tree::find()->where(['!=', 'lvl', 0])->andWhere(['!=', 'lvl', 1])->asArray()->all();
            $itemTypeList =[];
            foreach ($itemType as $value) {
                $eType1 =[];
                $eType1['id'] = $value['id'];
                $eType1['name'] = '['.$value['code'].'] '.$value['name'];
                $itemTypeList[]=$eType1;
            }
            $getUnitTypeList = \app\models\SettingsTree::find()->where(['=', 'root', 1])->andWhere(['=', 'lvl', 1])->asArray()->all();
$unitType=[];
       foreach($getUnitTypeList as $value){
        $eTypeList =[];
        $eTypeList['id'] = $value['id'];
        $eTypeList['name'] = $value['name'];
        $unitType[]=$eTypeList;
       }
       $unitTypeList = ArrayHelper::map($unitType, 'id', 'name');
    //    pr($unitType);
         
    
            return $this->render('update',[ 'estimateMaster' => $estimateMaster, 'arrayData' => $list, 
            'overheadCost'=>$overheadCost,  'profitCost'=>$profitCost, 'equipmentCost'=>$equipmentCost,
            'labourCost'=>$labourCost, 'materialCost'=>$materialCost, 'totalCost'=>$totalCost,'resourcedata'=>$data,
            'activityList'=>$activityList,'estimateTypeList'=>$estimateTypeList,
            'unitTypeList'=>$unitTypeList,
            'listDataVal'=>$listDataVal,'itemTypeList'=>$itemTypeList,'subActivityList'=>$subActivityList,
            'unitType'=>$unitType
            ]);
        }
    }    

    public function actionDeletes($id) 
    {
        if($this->findModel($id)->delete()){
            EstimateDetails::deleteAll(['estmate_id'=>$id]);
        }
        return $this->redirect(['master']);
    }

    protected function findModel($id)
    {
        if (($model = EstimateMaster::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function reformat($arrayData) {
        $invKeysvalue = array_keys($arrayData);
        $dataVal = array_values($arrayData);
        $invTotalCount = count(array_values($dataVal));
        $container = [];
        for ($i = 0; $i < $invTotalCount; $i++) {
            $invTemp = [];
            foreach ($invKeysvalue as $key) {
                $invTemp[$key] = isset($arrayData[$key][$i])?$arrayData[$key][$i]:'';
            }
            $container[] = $invTemp;
        }
        return $container;
    }

    public function actionGetSubActivity()
    {
        $activityId = $_REQUEST['activity'];

        $list = \app\models\SettingsTree::find()
        ->select(['id','root','lft','rgt','lvl','name','code'])
        ->where(['!=', 'lvl', 0])->andWhere(['root'=>2])
        ->orderBy('root, lft')->asArray()->all();
      
        $dataformatChange=[];
        $parent="";
        foreach ($list as $d) {
            // pr($d);
            if ($d['lvl'] == 1) {
                $dataformatChange[$d['id']]=$d;
                $parent=$d['id'];
            } 
            else {
                $dataformatChange[$parent]['sublevel'][]=$d;
            }
        }
          $listData=[];
        foreach ($dataformatChange as $key => $valueData) {
            if($activityId==$key){
           foreach ($valueData['sublevel'] as $value) {
               $listData[$value['id']]=$value['name'];
         
            }
        }
        }
        echo "<option value=''>Select</option>";
        foreach ($listData as $key => $value) {
            echo "<option value='$key'>$value</option>";
        }

    }


    public function actionGetDescription()
    {
        $subActivityId = $_REQUEST['sub_activity'];
        $settingList = SettingsTree::find()->where(['id' => $subActivityId])->asArray()->one();
        echo $settingList['description'];
    }

    public function actionGetItems($id)
    {


        $list = \app\models\Tree::find()
        ->select(['id','root','lft','rgt','lvl','name','code'])
        ->where(['!=', 'lvl', 0])->andWhere(['root'=>$id])
        ->orderBy('root, lft')->asArray()->all();
        $dataformatChange=[];
        $parent="";
        foreach ($list as $d) {
            if ($d['lvl'] == 1) {
                $dataformatChange[$d['id']]=$d;
                $parent=$d['id'];
            } else {
                $dataformatChange[$parent]['sublevel'][]=$d;
            }
        }
        $listData=[];
        foreach ($dataformatChange as $key => $valueData) {
            if(isset($valueData['sublevel'])){
           foreach ($valueData['sublevel'] as $value) {
            $listData[]=['id'=>$value['id'], 'name'=> '['.$value['code'].'] '.$value['name'], 'group'=>$valueData['name']];
          }
        }
        }

        echo json_encode($listData);


        die();
    }

    public function GetResourceData()
    {

        $rooLevel = Tree::find()->select(['id'])->where(['lvl'=>0])->asArray()->all();

        $dataList=[];

        foreach ($rooLevel as $levelData){
            $levelid=$levelData['id'];
            $list = \app\models\Tree::find()
                ->select(['id','root','lft','rgt','lvl','name','code'])
                ->where(['!=', 'lvl', 0])->andWhere(['root'=>$levelid])
                ->orderBy('root, lft')->asArray()->all();

           // $levelCheck = \app\models\Tree::find()->select(max(['lvl']))->groupBy('lvl')->orderby(['id'=>'asc'])->asArray()->one();

            $levelCheck = Tree::find()->select('lvl')->where(['!=', 'lvl', 0])->andWhere(['root'=>$levelid])->orderBy("lvl DESC")->asArray()->one();


            $levelVal = isset($levelCheck['lvl'])?$levelCheck['lvl']:"";



            $dataformatChange=[];
            $parent="";
            $listData=[];
            if(!empty($list)){

                if($levelVal>1){
                    foreach ($list as $d) {
                        if ($d['lvl'] == 1) {
                            $dataformatChange[$d['id']]=$d;
                            $parent=$d['id'];
                        } else {
                            $dataformatChange[$parent]['sublevel'][]=$d;
                        }
                    }
                    foreach ($dataformatChange as $key => $valueData) {
                        if(isset($valueData['sublevel'])){
                            foreach ($valueData['sublevel'] as $value) {
                                $listData[]=['id'=>$value['id'], 'name'=> '['.$value['code'].'] '.$value['name'], 'group'=>$valueData['name']];
                            }
                        }
                    }
                }else{
                    foreach ($list as $d) {

                            $listData[]=['id'=>$d['id'], 'name'=> '['.$d['code'].'] '.$d['name'],'group'=>'Labour'];

                    }

                    //pr($listData);
                }

                //pr1($listData);
            }


            $dataList[$levelid]=$listData;

//  pr1($dataList);
        }

        return $dataList;

 }

 

    public function actionGetItemCost($id)
    {
        $tree =  \app\models\Tree::find()->where(['id'=>$id])->asArray()->one();
        echo $tree['cost'];
    }

    public function actionGetEstimateMasterData()
    {
       
      
        $activityId= $_REQUEST['activity_id'];
        $subActivityId= $_REQUEST['sub_activity_id'];
        $activity_list_unit = EstimateMaster::find()->where(['sub_activity_id'=>$subActivityId])->asArray()->one();
        return $activity_list_unit['id'];
       
    }

}