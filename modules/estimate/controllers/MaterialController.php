<?php

namespace app\modules\estimate\controllers;

use Yii;
use app\models\MaterialMaster;
use app\models\MaterialUnitCost;
use app\models\MaterialHistory;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

use app\controllers\BaseController;

/**
 * MaterialController implements the CRUD actions for MaterialMaster model.
 */
class MaterialController extends BaseController
{

    /**
     * Lists all MaterialMaster models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MaterialMaster();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize'=>50];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single MaterialMaster model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MaterialMaster model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new MaterialMaster();
        if (Yii::$app->request->post()){
            $material = new MaterialMaster();
            $request=Yii::$app->request->post();
            $description = $request['description'];
            $data= $request['formdata'];            
            $material->name=$request['name'];
            $material->description=$request['description'];
            $material->on_date = date_create($request['on_date'])->format('Y-m-d');
            $date=date_create();
            $material->created_by='1';
            $material->created_at=$date->format('Y-m-d');
            $material->updated_by='1';
            $material->updated_at=$date->format('Y-m-d');
            if($material->save()) {
                foreach ($data as $value) {
                    if(!empty($value[1])){
                        $materialUnitCost = new MaterialUnitCost();
                        $materialUnitCost->master_id=$material->id;
                        $materialUnitCost->unit_id=$value[0];
                        $materialUnitCost->cost=$value[1];
                        $materialUnitCost->created_by='1';
                        $materialUnitCost->created_at=$date->format('Y-m-d');
                        $materialUnitCost->updated_by='1';
                        $materialUnitCost->updated_at=$date->format('Y-m-d');
                        if (!$materialUnitCost->save()) {
                            pr($materialUnitCost->getErrors());
                        }
                    }    
                }
            }
            return $this->redirect(['index']);
        } else {            
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing MaterialMaster model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $materialUnitCost =MaterialUnitCost::find()->where(['master_id'=>$id])->asArray()->all();
        if (Yii::$app->request->post()){
            $material = $this->findModel($id);
            $request=Yii::$app->request->post();
            $description = $request['description'];
            $data= $request['formdata'];            
            $material->name=$request['name'];
            $material->description=$request['description'];
            $material->on_date = date_create($request['on_date'])->format('Y-m-d');
            $date=date_create();
            $material->updated_by='1';
            $material->updated_at=$date->format('Y-m-d');
            $mUC = MaterialUnitCost::find()->where(['master_id'=>$id])->asArray()->all();
            foreach ($mUC as $v1) {
                $materialHistory = new MaterialHistory();
                $materialHistory->master_id=$id;
                $materialHistory->on_date=$date->format('Y-m-d');
                        $materialHistory->unit_id=$v1['unit_id'];
                        $materialHistory->cost=$v1['cost'];
                        $materialHistory->created_by='1';
                        $materialHistory->created_at=$date->format('Y-m-d');
                        $materialHistory->save();
                            MaterialUnitCost::findOne($v1['id'])->delete();
            }
            if($material->save()) {
                foreach ($data as $value) {
                    if(!empty($value[1])){
                        $materialUnitCost = new MaterialUnitCost();
                        $materialUnitCost->master_id=$id;
                        $materialUnitCost->unit_id=$value[0];
                        $materialUnitCost->cost=$value[1];
                        $materialUnitCost->created_by='1';
                        $materialUnitCost->created_at=$date->format('Y-m-d');
                        $materialUnitCost->updated_by='1';
                        $materialUnitCost->updated_at=$date->format('Y-m-d');
                        if (!$materialUnitCost->save()) {
                            pr($materialUnitCost->getErrors());
                        }
                    }    
                }
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'materialUnitCost'=>$materialUnitCost,
            ]);
        }
    }

    /**
     * Deletes an existing MaterialMaster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionDeletes($id) {
        if($this->findModel($id)->delete()){
            $mUC = MaterialUnitCost::find()->where(['master_id'=>$id])->asArray()->all();
            foreach ($mUC as $v1) {
                MaterialUnitCost::findOne($v1['id'])->delete();
            }
        }
        return $this->redirect(['index']);
    }
    /**
     * Finds the MaterialMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MaterialMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MaterialMaster::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
