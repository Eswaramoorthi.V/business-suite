<?php

namespace app\modules\estimate\controllers;

use Yii;

use app\controllers\BaseController;

/**
 * ResourceMasterController implements the CRUD actions
 */
class SettingsController extends BaseController
{

    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionGetCodeCheck()
    {
        $code = $_REQUEST['code'];
        $codeList = \app\models\SettingsTree::find()->where(['code' => $code])->one();
        echo $codeList['code'];
    }

}
