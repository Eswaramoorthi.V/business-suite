<?php

namespace app\modules\estimate\controllers;

use Yii;

use app\controllers\BaseController;
use app\models\TblDirectory;
use app\models\CsvUpload;
use app\models\Tree;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use kartik\tree\TreeSecurity;
use kartik\tree\TreeView;
/**
 * ResourceMasterController implements the CRUD actions
 */
class ResourceMasterController extends BaseController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetCodeCheck()
    {
        $code = $_REQUEST['code'];
        $codeList = \app\models\Tree::find()->where(['code' => $code])->one();
        echo $codeList['code'];
    }

    public function actionImportDirectory()
    {

        $model = new TblDirectory();

        if ($model->load(Yii::$app->request->post())) {
            // pr($_FILES);
            $tmpName = $_FILES['TblDirectory']['tmp_name']['file'];
        //    pr($tmpName);
            $csvAsArray = array_map('str_getcsv', file($tmpName));
            // pr($csvAsArray);
            $directoryField = CsvUpload::getTableSchema()->getColumnNames();
            // pr($directoryField);
            unset($csvAsArray[0]);

            $header = array_map('str_getcsv', file($tmpName));
//   pr($header);
            $headerData = $header[0];
            $model->file = UploadedFile::getInstance($model, 'file');
            $diffresult = array_diff($headerData, $directoryField);
            // pr($diffresult);
            if (empty($diffresult)) {
                Yii::$app->db->createCommand()->truncateTable('csv_upload')->execute();

                $directoryPath = 'csv/';
                if (!file_exists($directoryPath)) {
                    mkdir($directoryPath);
                }
               
                if ($model->file) {
                    // pr("fgfd");
                    // pr($model);
                    $time = time();
                    $model->file->saveAs('csv/' . $time . '.' . $model->file->extension);
                    $model->file = 'csv/' . $time . '.' . $model->file->extension;
                    $model->save();
                    if (!$model->save()) {
                        pr($model->getErrors());
                    }
                }
// die();
                $csvdataArray = $csvAsArray; 
                //  pr($csvdataArray);
              

                foreach ($csvdataArray as $uploaddata) {
                   
                    $importtable = new CsvUpload();

                    foreach ($directoryField as $vk => $key) {
// pr($directoryField);
                        $importtable->$key = $uploaddata[$vk];
                       
                    }

                    if (!$importtable->save()) {
                        pr($model->getErrors());
                    }
                   
                }
            }
            $csvData = CsvUpload::find()->asArray()->all();


            foreach ($csvData as $data) {
// pr($csvData);
                $name=$data['name'];
                $description=$data['description'];
                $code=isset($data['code']) ? $data['code'] :'';
                $cost=isset($data['cost']) ? $data['cost'] :'';
                $date=isset($data['date']) ? $data['date'] :'';
                $parent=$data['parent'];
                if($parent==''){
                    $parent=$_REQUEST['id'];
                }else{
                    $parentData= Tree::find()->where(['name'=>$parent])->orderby(['id'=>SORT_DESC])->asArray()->one();
                    // pr($parentData);
                    $parent=$parentData['id'];
                }




                $data = ['nodeTitle' => 'node', 'nodeTitlePlural' => 'nodes', 'treeNodeModify' => 1, 'parentKey' => $parent, 'modelClass' => 'app\models\Tree', 'Tree' => ['id' => '(new)', 'name' => $name, 'date' => $date, 'icon_type' => 2, 'icon' => 1, 'description' => $description,'code'=>$code,'cost'=>$cost]];


                $post = $data;
                // pr($post);
                $parentKey = ArrayHelper::getValue($data, 'parentKey', null);
                $treeNodeModify = ArrayHelper::getValue($data, 'treeNodeModify', null);
                $currUrl = ArrayHelper::getValue($data, 'currUrl', '');
                $treeClass = TreeSecurity::getModelClass($data);

                $keyAttr = 1;
                $nodeTitles = TreeSecurity::getNodeTitles($data);


                if ($treeNodeModify) {
                    $node = new $treeClass;

                    $successMsg = Yii::t('kvtree', 'The {node} was successfully created.', $nodeTitles);
                    $errorMsg = Yii::t('kvtree', 'Error while creating the {node}. Please try again later.', $nodeTitles);
                } else {
                    $tag = explode("\\", $treeClass);
                    $tag = array_pop($tag);
                    $id = $post[$tag][$keyAttr];

                    $node = $treeClass::findOne($id);
                    $successMsg = Yii::t('kvtree', 'Saved the {node} details successfully.', $nodeTitles);
                    $errorMsg = Yii::t('kvtree', 'Error while saving the {node}. Please try again later.', $nodeTitles);
                }
                //  $node->activeOrig = $node->active;
                $isNewRecord = $node->isNewRecord;
                $node->load($post);
                //  pr($node);
                // $node->name = $post['Tree']['name'];
               
                // $node->remarks = $post['Tree']['description'];
                // $node->date = $post['Tree']['date'];
                // $node->parent = $post['parentKey'];
                // pr($post);
                $node->name = $post['Tree']['name'];
                $node->description =$post['Tree']['description'];
                $node->code = $post['Tree']['code'];
                $node->cost = $post['Tree']['cost'];
                $node->date = $post['Tree']['date'];
                $node->collapsed=1;

                $parentfolderData= Tree::find()->where(['id'=>$post['parentKey']])->asArray()->one();
            //    pr($parentfolderData);
                $relativePath = Yii::getAlias('@app');

                $absolutePath=$parentfolderData['absolute_path'];
                $pathFolder = $relativePath .'/web/'. $parentfolderData['absolute_path'];
                $folder = $pathFolder;




                $namevalue=$post['Tree']['name'];
                $folderNameValue=self::cleanString($namevalue);
                $folder = strip_tags($folder);



                $folderPath[] = self::cleanString($folder);



                $folderName = implode('/', $folderPath);



                $filePath= '/web/'.$absolutePath.'/'.$namevalue;
                $absolutePath=$absolutePath.'/'.$folderNameValue;
                $pathFolder=$relativePath.'/web/'.$absolutePath;




                if (!file_exists($pathFolder)) {

                    try {
                        mkdir(($pathFolder), 0777, true); // $path is a file

                    } catch (Exception $e) {
                        pr($e);
                    }
                }


                $node->folder_path = $filePath;
                $node->absolute_path = $absolutePath;
                $node->directory_name=$folderNameValue;

                if (!file_exists($filePath)) {

                    try {
                        mkdir(($filePath), 0777, true); // $path is a file

                    } catch (Exception $e) {
                        pr($e);
                    }
                }


                $errors = $success = false;

                if ($treeNodeModify) {

                    if ($parentKey == TreeView::ROOT_KEY) {
                        $node->makeRoot();
                    } else {

                        $parent = $treeClass::findOne($parentKey);
// pr($parent);
                        if ($parent->isChildAllowed()) {
                            $node->appendTo($parent);

                        }
                    }
                }
                if ($node->save()) {

                    // check if active status was changed
                    if (!$isNewRecord && $node->activeOrig != $node->active) {
                        if ($node->active) {
                            $success = $node->activateNode(false);
                            $errors = $node->nodeActivationErrors;
                        } else {
                            $success = $node->removeNode(true, false); // only deactivate the node(s)
                            $errors = $node->nodeRemovalErrors;
                        }
                    } else {
                        $success = true;
                    }
                    if (!empty($errors)) {
                        $success = false;
                        $errorMsg = "<ul style='padding:0'>\n";
                        foreach ($errors as $err) {
                            $errorMsg .= "<li>" . Yii::t('kvtree', "{node} # {id} - '{name}': {error}",
                                    $err + $nodeTitles) . "</li>\n";
                        }
                        $errorMsg .= "</ul>";
                    }
                } else {
                    echo $errorMsg = '<ul style="margin:0"><li>' . implode('</li><li>', $node->getFirstErrors()) . '</li></ul>';
                }

            }
            return $this->redirect('index');
        }
        else {
            return $this->render('import', [
                'model' => $model,
            ]);
        }

    }

    public static function cleanString($str)
    {
        $str = trim($str);
        $str = str_replace(array('\'', '"', ',', ';', '<', '>', ')', '('), '', $str);
        $str = str_replace(" ", "-", $str);
        $str = preg_replace('/-+/', '-', $str);
        //$str1 = strtolower($str);
        return $str;
    }

}
