<?php

namespace app\modules\estimate\controllers;

use Yii;
use app\models\LabourMaster;
use app\models\LabourUnitCost;
use app\models\LabourHistory;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

use app\controllers\BaseController;

/**
 * LabourController implements the CRUD actions for LabourMaster model.
 */
class LabourController extends BaseController
{

    /**
     * Lists all LabourMaster models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LabourMaster();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize'=>50];
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single LabourMaster model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LabourMaster model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new LabourMaster();
        if (Yii::$app->request->post()){
            $material = new LabourMaster();
            $request=Yii::$app->request->post();
            $description = $request['description'];
            $data= $request['formdata'];            
            $material->name=$request['name'];
            $material->description=$request['description'];
            $material->lrc=$request['lrc'];
            $material->src=$request['src'];
            $material->on_date = date_create($request['on_date'])->format('Y-m-d');
            $date=date_create();
            $material->created_by='1';
            $material->created_at=$date->format('Y-m-d');
            $material->updated_by='1';
            $material->updated_at=$date->format('Y-m-d');
            if($material->save()) {
                foreach ($data as $value) {
                    if(!empty($value[1])){
                        $labourUnitCost = new LabourUnitCost();
                        $labourUnitCost->master_id=$material->id;
                        $labourUnitCost->unit_id=$value[0];
                        $labourUnitCost->cost=$value[1];
                        $labourUnitCost->created_by='1';
                        $labourUnitCost->created_at=$date->format('Y-m-d');
                        $labourUnitCost->updated_by='1';
                        $labourUnitCost->updated_at=$date->format('Y-m-d');
                        if (!$labourUnitCost->save()) {
                            pr($labourUnitCost->getErrors());
                        }
                    }    
                }
            }
            return $this->redirect(['index']);
        } else {            
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing LabourMaster model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $labourUnitCost =LabourUnitCost::find()->where(['master_id'=>$id])->asArray()->all();
        if (Yii::$app->request->post()){
            $material = $this->findModel($id);
            $request=Yii::$app->request->post();
            $description = $request['description'];
            $data= $request['formdata'];            
            $material->name=$request['name'];
            $material->description=$request['description'];
            $material->on_date = date_create($request['on_date'])->format('Y-m-d');
            $material->lrc=$request['lrc'];
            $material->src=$request['src'];
            $date=date_create();
            $material->updated_by='1';
            $material->updated_at=$date->format('Y-m-d');
            $mUC = LabourUnitCost::find()->where(['master_id'=>$id])->asArray()->all();
            foreach ($mUC as $v1) {
                $materialHistory = new LabourHistory();
                $materialHistory->master_id=$id;
                $materialHistory->on_date=$date->format('Y-m-d');
                        $materialHistory->unit_id=$v1['unit_id'];
                        $materialHistory->cost=$v1['cost'];
                        $materialHistory->created_by='1';
                        $materialHistory->created_at=$date->format('Y-m-d');
                        $materialHistory->save();
                            LabourUnitCost::findOne($v1['id'])->delete();
            }
            if($material->save()) {
                foreach ($data as $value) {
                    if(!empty($value[1])){
                        $labourUnitCost = new LabourUnitCost();
                        $labourUnitCost->master_id=$id;
                        $labourUnitCost->unit_id=$value[0];
                        $labourUnitCost->cost=$value[1];
                        $labourUnitCost->created_by='1';
                        $labourUnitCost->created_at=$date->format('Y-m-d');
                        $labourUnitCost->updated_by='1';
                        $labourUnitCost->updated_at=$date->format('Y-m-d');
                        if (!$labourUnitCost->save()) {
                            pr($labourUnitCost->getErrors());
                        }
                    }    
                }
            }
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'labourUnitCost'=>$labourUnitCost,
            ]);
        }
    }

    /**
     * Deletes an existing LabourMaster model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */

    public function actionDeletes($id) {
        if($this->findModel($id)->delete()){
            $mUC = LabourUnitCost::find()->where(['master_id'=>$id])->asArray()->all();
            foreach ($mUC as $v1) {
                LabourUnitCost::findOne($v1['id'])->delete();
            }
        }
        return $this->redirect(['index']);
    }
    /**
     * Finds the LabourMaster model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LabourMaster the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LabourMaster::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
