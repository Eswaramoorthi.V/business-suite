<?php

namespace app\modules\estimate\controllers;

use Yii;
use app\modules\estimate\models\EstimateSetting\Relation;
use app\modules\estimate\models\EstimateSetting;
use app\modules\estimate\models\EstimateSetting\Search;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;

use app\controllers\BaseController;

/**
 * ConfigSettingController implements the CRUD actions for EstimateSetting model.
 */
class ConfigSettingController extends BaseController
{
    public function actions()
    {
        if(Yii::$app->request->get('draw') == 1) {
            $query = Relation::find()->where(['status' => 1, 'parent_id' => 1])->with('parentCategoryRelation');
        } else {
            $query = Relation::find()->where(['status' => 1])->with('parentCategoryRelation');
        }

        return [
            'datatables' => [
                'class' => 'app\datatable\DataTableAction',
                'query' => $query,
            ],
        ];
    }

    /**
     * Lists all EstimateSetting models.
     * @return mixed
     */
    public function actionIndex()
    {
         $searchModel = new Search();
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->pagination = ['PageSize'=>50];
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
 
    }

    /**
     * Displays a single EstimateSetting model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EstimateSetting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EstimateSetting();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EstimateSetting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EstimateSetting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
   
    public function actionDeletes($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the EstimateSetting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EstimateSetting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EstimateSetting::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
