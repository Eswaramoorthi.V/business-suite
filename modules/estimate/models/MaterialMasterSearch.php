<?php

namespace app\modules\estimate\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\estimate\models\MaterialMaster;

/**
 * Search represents the model behind the search form about `app\models\EstimateSetting`.
 */
class MaterialMasterSearch extends MaterialMaster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'last_purchased_price', 'average_weighted_price', 'unit_type', 'project_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MaterialMaster::find();
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
            
        return $dataProvider;
    }
    
}
