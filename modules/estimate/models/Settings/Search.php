<?php

namespace app\modules\estimate\models\Settings;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\estimate\models\Settings;

/**
 * Search represents the model behind the search form about `app\models\EstimateSetting`.
 */
class Search extends Settings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code','parent_id', 'description','name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Settings::find()
                ->where([
                    'status' => Settings::SETTINGS_STATUS_ENABLED
                ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'description', $this->description])
              -> andFilterWhere(['like', 'code', $this->code])
              -> andFilterWhere(['like', 'parent_id', $this->parent_id]);
            
        return $dataProvider;
    }
}
