<?php

namespace app\modules\estimate\models\EstimateSetting;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\estimate\models\EstimateSetting;

class Relation extends EstimateSetting
{
    public function fields()
    {
        return array_merge(parent::fields(), ['parentCategory']);
    }

    public function getParentCategory()
    {
        if(is_null($this->parentCategoryRelation)) {
            return 'NA';
        }

        return $this->parentCategoryRelation->description;;
    }

}
