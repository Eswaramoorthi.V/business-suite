<?php

namespace app\modules\estimate\models;

use Yii;

/**
 * This is the model class for table "labour_master".
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 * @property double $last_labour_cost
 * @property double $average_labour_cost
 * @property double $labour_related_cost
 * @property double $staff_related_cost
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property string $system_datetime
 */
class Equipment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'description', 'cost', 'status','created_by', 'updated_by','created_at','updated_at'], 'safe'],
            
            [['name','cost'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            
            'name' => 'Name',
            'description' => 'Description',
            'cost' => 'Cost',
            'status'=>'Status',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            
        ];
    }
}
