<?php

namespace app\modules\estimate\models;

use app\models\BaseModel;
/**
 * This is the model class for table "estimate_setting".
 *
 * @property integer $id
 * @property string $code
 * @property string $description
 * @property integer $status
 * @property integer $parent_id
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class EstimateSetting extends BaseModel
{
    // use SaveRelationsTrait;

    const ESTIMATE_SETTING_STATUS_ENABLED = 1;
    const ESTIMATE_SETTING_STATUS_DISABLED = 0;

    public function init()
    {
        $this->status = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%estimate_setting}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'parent_id', 'type'], 'integer'],
            [['code'], 'unique'],
            [['description', 'type'], 'required'],
            [['description1'], 'string'],
            [['description'], 'string', 'max' => 1000],
            [['code'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'description' => 'Name',
            'description1' => 'Description',
            'status' => 'Status',
            'type'=>'Config Type',
            'parent_id' => 'Parent',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'system_datetime' => 'System Datetime',
        ];
    }

    public function parentSettingList() {
        $categories = static::find()
                            ->where([
                                'parent_id' => null,
                                'status' => EstimateSetting::ESTIMATE_SETTING_STATUS_ENABLED
                            ]);
        if($this->id) {
            $categories->andWhere(['!=', 'id', $this->id]);
        }
        return $categories->all();
    }

    public function getParentCategoryRelation() {
        return $this->hasOne(EstimateSetting::className(), ['id' => 'parent_id']);
    }

    public function getChildCategories() {
        return $this->hasMany(EstimateSetting::className(), ['parent_id' => 'id'])->andOnCondition(['status'=>1]);
    }

    public function getCategoryList() {
        $categories = EstimateSetting::find()
                            ->where([
                                'parent_id' => null,
                                'type'=>'1',
                                'status' => EstimateSetting::ESTIMATE_SETTING_STATUS_ENABLED
                            ]);
        return $categories->asArray()->all();
    }
    
    public function getUnitTypeList() {
        $unitType = EstimateSetting::find()
                        ->where([
                            'description'=>'Unit Type',
                            'parent_id'=>null,
                            'type'=>2
                        ])->asArray()->one();
        $categories = EstimateSetting::find()
                            ->where([
                                'parent_id' => $unitType['id'],
                                'type'=>'2',
                                'status' => EstimateSetting::ESTIMATE_SETTING_STATUS_ENABLED
                            ])->asArray()->all();
        return $categories;
    }

}
