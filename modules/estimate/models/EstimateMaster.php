<?php

namespace app\modules\estimate\models;

use Yii;

/**
 * This is the model class for table "estimate_master".
 *
 * @property int $id
 * @property int $project_id
 * @property int $category_id
 * @property int $sub_category_id
 * @property int $quantity
 * @property int $unit_type
 * @property double $material_rate
 * @property double $labour_rate
 * @property double $profit
 * @property double $overhead
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property string $system_datetime
 */
class EstimateMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estimate_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'category_id', 'sub_category_id', 'quantity', 'unit_type', 'material_rate', 'labour_rate', 'profit', 'overhead'], 'required'],
            [['category_id', 'sub_category_id', 'quantity', 'unit_type'], 'integer'],
            [['material_rate', 'labour_rate', 'profit', 'overhead'], 'number'],
            [['created_on', 'updated_on', 'system_datetime','description'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project',
            'category_id' => 'Activity ID',
            'sub_category' => 'Sub Activity',
            'description'=>'Description',
            'quantity' => 'Quantity',
            'unit_type' => 'Unit Type',
            'material_rate' => 'Material Rate',
            'labour_rate' => 'Labour Rate',
            'profit' => 'Profit',
            'overhead' => 'Overhead',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'system_datetime' => 'System Datetime',
        ];
    }
    public function getSetting()
    {
        return $this->hasOne(EstimateSetting::className(), ['id' => 'sub_category_id']);
    }

}
