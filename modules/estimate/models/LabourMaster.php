<?php

namespace app\modules\estimate\models;

use Yii;

/**
 * This is the model class for table "labour_master".
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 * @property double $last_labour_cost
 * @property double $average_labour_cost
 * @property double $labour_related_cost
 * @property double $staff_related_cost
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property string $system_datetime
 */
class LabourMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'labour_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'last_labour_cost', 'average_labour_cost', 'project_id'], 'required'],
            [['last_labour_cost', 'average_labour_cost'], 'number'],
            [['labour_related_cost', 'staff_related_cost', 'created_on', 'updated_on', 'system_datetime'], 'safe'],
            [['name'], 'string', 'max' => 500],
            [['created_by', 'updated_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project',
            'name' => 'Name',
            'last_labour_cost' => 'Last Amount',
            'average_labour_cost' => 'Average Amount',
            'labour_related_cost'=>'Labour Related Cost',
            'staff_related_cost'=>'Staff Related Cost',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'system_datetime' => 'System Datetime',
        ];
    }
}
