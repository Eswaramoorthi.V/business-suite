<?php

namespace app\modules\estimate\models;

use Yii;
use yii\data\ActiveDataProvider;
use app\modules\estimate\models\EstimatePricing;

/**
 * This is the model class for table "estimate_pricing".
 *
 * @property int $id
 * @property int $project_id
 * @property int $master_id
 * @property string $type
 * @property string $description
 * @property double $cost
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property string $system_datetime
 */
class EstimatePricing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estimate_pricing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'master_id', 'type', 'cost'], 'required'],
            [['master_id','type'], 'integer'],
            [['cost'], 'number'],
            [['created_on', 'updated_on', 'system_datetime'], 'safe'],
            [['description'], 'string', 'max' => 255],
            [['created_by', 'updated_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project',
            'master_id' => 'Master ID',
            'type' => 'Estimate Type',
            'description' => 'Description',
            'cost' => 'Cost',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'system_datetime' => 'System Datetime',
        ];
    }

     public function search($params)
    {
        $query = EstimatePricing::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }            
        return $dataProvider;
    }
}
