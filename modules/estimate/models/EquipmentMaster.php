<?php

namespace app\modules\estimate\models;

use Yii;

/**
 * This is the model class for table "equipment_master".
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 * @property double $price
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property string $system_datetime
 */
class EquipmentMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipment_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'name', 'price'], 'required'],
            [['project_id'], 'integer'],
            [['price'], 'number'],
            [['created_on', 'updated_on', 'system_datetime'], 'safe'],
            [['name'], 'string', 'max' => 500],
            [['created_by', 'updated_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project',
            'name' => 'Name',
            'price' => 'Price',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'system_datetime' => 'System Datetime',
        ];
    }
}
