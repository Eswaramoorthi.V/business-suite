<?php

namespace app\modules\estimate\models;

use Yii;

/**
 * This is the model class for table "material_master".
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 * @property double $last_purchased_price
 * @property double $average_weighted_price
 * @property int $unit_type
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property string $system_datetime
 */
class MaterialMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'last_purchased_price', 'average_weighted_price', 'unit_type', 'project_id'], 'required'],
            [['last_purchased_price', 'average_weighted_price'], 'number'],
            [['unit_type'], 'integer'],
            [['created_on', 'updated_on', 'system_datetime'], 'safe'],
            [['name'], 'string', 'max' => 500],
            [['created_by', 'updated_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project',
            'name' => 'Name',
            'last_purchased_price' => 'Last Purchased Price',
            'average_weighted_price' => 'Average Weighted Price',
            'unit_type' => 'Unit Type',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'system_datetime' => 'System Datetime',
        ];
    }
}
