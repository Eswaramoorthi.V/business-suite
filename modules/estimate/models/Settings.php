<?php

namespace app\modules\estimate\models;

use Yii;

/**
 * This is the model class for table "labour_master".
 *
 * @property int $id
 * @property int $project_id
 * @property string $name
 * @property double $last_labour_cost
 * @property double $average_labour_cost
 * @property double $labour_related_cost
 * @property double $staff_related_cost
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property string $system_datetime
 */
class Settings extends \yii\db\ActiveRecord
{

    const SETTINGS_STATUS_ENABLED = 1;
    const SETTINGS_STATUS_DISABLED = 0;

    public function init()
    {
        $this->status = 1;
        parent::init();
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'code', 'description', 'status','parent_id','created_by', 'updated_by','created_at','updated_at'], 'safe'],
            
            [['name'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            
            'name' => 'Name',
            'description' => 'Description',
            'code' => 'Code',
            'status'=>'Status',
            'parent_id'=>'Parent',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            
        ];
    }

    public function getParentSettingList() {
        $categories = static::find()
                            ->where([
                                'parent_id' => null,
                                'status' => Settings::SETTINGS_STATUS_ENABLED
                            ]);
        if($this->id) {
            $categories->andWhere(['!=', 'id', $this->id]);
        }
        return $categories->all();
    }

    public function getParentCategoryRelation() {
        return $this->hasOne(Settings::className(), ['id' => 'parent_id']);
    }

    public function getChildCategories() {
        return $this->hasMany(Settings::className(), ['parent_id' => 'id'])->andOnCondition(['status'=>1]);
    }
}
