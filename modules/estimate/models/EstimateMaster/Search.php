<?php

namespace app\modules\estimate\models\EstimateMaster;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\estimate\models\EstimateMaster;
use app\modules\estimate\models\EstimateSetting;

/**
 * Search represents the model behind the search form about `app\models\EstimateSetting`.
 */
class Search extends EstimateMaster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code','parent_id', 'description','description1'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EstimateMaster::find()->with('setting');
       // pr($query);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // $query->andFilterWhere(['like', 'code', $this->code])
        //     ->andFilterWhere(['like', 'description', $this->description]);
            
        return $dataProvider;
    }
    
}
