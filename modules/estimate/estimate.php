<?php

namespace app\modules\estimate;

class estimate extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\estimate\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
