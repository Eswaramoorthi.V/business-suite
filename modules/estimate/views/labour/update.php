<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\LabourMaster */

$this->title = 'Update Labour';
$this->params['breadcrumbs'][] = ['label' => 'Labour', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="labour-master-update">

    <?= $this->render('_form_update', [
        'model' => $model, 'update'=>1,
        'labourUnitCost'=>$labourUnitCost
    ]) ?>

</div>
