<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\estimate\models\EstimateSetting;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\LabourMaster */
/* @var $form yii\widgets\ActiveForm */

$getUnitTypeList = \app\modules\estimate\models\Settings::find()->where(['parent_id' => 1])->asArray()->all();
$unitTypeList = ArrayHelper::map($getUnitTypeList, 'id', 'name');
$unitTypeListData=[];
if(!empty($unitTypeList)){
    $dataList = [];
    foreach ($unitTypeList as $key => $value) {
        $val = [];
        $val[0]=$key;
        $val[1]='0.00';
        $dataList[]=$val;
        $unitTypeListData[]=['id'=> $key,'name'=>$value];
    }
} else{
    $dataList = [ [] ];
}

?>
<div class="col-md-12 text-left add-label" style="font-size: 32px">
    Create Labour
</div>
<div class="material-master-form form-horizontal">
    <h4 class="error-msg hide text-danger"> </h4>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Name</label>
            <input type="text" class="form-control name" id = "name" placeholder="Enter Name" require>
        </div>
        <div class="col-md-6 text-left">
            <label class="control-label">Description</label>
            <textarea id="description" class="form-control" name="description" rows="2" placeholder="Enter Description" aria-required="true" aria-invalid="true"></textarea>
        </div>
    </div>
    <div class="row">    
        <div class="col-md-6 text-left">
            <?php echo '<label>On Date</label>';
            echo DatePicker::widget([
            'name' => 'check_issue_date',
            'value' => date('d-M-Y'),
            'options' => ['placeholder' => 'Select issue date ...'],
            'id'=>'on_date',
            'class'=>'on_date',
            'pluginOptions' => [
            'format' => 'dd-M-yyyy',
            'todayHighlight' => true
            ]
            ]); ?>
        </div>
        <div class="col-md-6">
            <label class="control-label">Labour Related Cost</label>
            <input type="number" class="form-control lrc" id = "lrc" placeholder="Enter LRC" require>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label class="control-label">Staff Related Cost</label>
            <input type="number" class="form-control src" id = "src" placeholder="Enter SRC" require>
        </div>
    </div>
    <div class="row">&nbsp</div>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="https://bossanova.uk/jexcel/v3/jexcel.js"></script>
    <script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script>
    <link rel="stylesheet" href="https://bossanova.uk/jexcel/v3/jexcel.css" type="text/css" />
    <link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

    <div class="row">
        <div class="col-md-12">
            <div id="spreadsheet"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 text-left add-label" style="padding-top: 12px">
            <input type='button' value='Add New Row' id="new"  class="btn btn-success btn-sm margin-left-10">
            <input type='button' value='Delete Last Row' id="delete"  class="btn btn-success btn-sm margin-left-10">
        </div>
        <div class="col-md-6 text-right add-label" style="padding-top: 12px">
            <input type='button' value='Save' id="getDataButton" class="btn btn-success btn-sm margin-right-10">
        </div>
    </div>
    <div>&nbsp</div>
</div>

<script>
    var data = <?=json_encode($dataList)?>;

    jexcel(document.getElementById('spreadsheet'), {
        data:data,
        columns: [
            { type: 'dropdown', title:'Unit Type', width:'300', source:<?=json_encode($unitTypeListData)?> },
            { type: 'number', title:'Cost', width:'200' },
        ],
        updateTable:function(instance, cell, col, row, val, label, cellName) {
            if ((col == 1)) {
                txt = numeral(val).format('0');
                $(cell).html(' ' + txt);
            }
        },
        columnSorting:false,
    });

    $('#new').on('click', function () {
        $('#spreadsheet').jexcel('insertRow'); event.preventDefault(); return false;
    });

    $('#delete').on('click', function () {
        $('#spreadsheet').jexcel('deleteRow'); event.preventDefault(); return false;
    });

    $('#getDataButton').on('click', function ()
    {
        $('#getDataButton').addClass("hide");
        var data = $('#spreadsheet').jexcel("getData");
        var name = $('#name').val();
        var description = $('#description').val();
        var lrc = $('#lrc').val();
        var src = $('#src').val();
        var on_date = $('#on_date').val();
        
        var formdata = JSON.stringify(data);
        if(name == ''){
            $('.error-msg').removeClass("hide");
            $('.error-msg').html("Please Enter Labour Name");
            $('#name').input('focus');
        } else if(description == ''){
            $('.error-msg').removeClass("hide");
            $('.error-msg').html("Please Enter Description");
            $('#name').input('focus');
        } else if(on_date == ''){
            $('.error-msg').removeClass("hide");
            $('.error-msg').html("Please Pick On Date");
            $('#on_date').input('focus');
        } else {
            $.ajax({
                url: './create',
                data: { 'formdata': data, 'name': name, 'description':description, 'on_date':on_date, 'src':src,'lrc':lrc},
                type: 'POST',
                success: function (response) {
                    console.log(response);
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }
    });

</script>