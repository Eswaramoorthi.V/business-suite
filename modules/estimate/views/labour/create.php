<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LabourMaster */

$this->title = 'Create Labour';
$this->params['breadcrumbs'][] = ['label' => 'Labour', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="labour-master-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
