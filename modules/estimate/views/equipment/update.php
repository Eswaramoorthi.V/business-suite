<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Equipment */

$this->title = 'Equipment';
$this->params['breadcrumbs'][] = ['label' => 'Equipment', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="labour-master-update">

    <?= $this->render('_form', [
        'model' => $model, 'update'=>1
    ]) ?>

</div>
