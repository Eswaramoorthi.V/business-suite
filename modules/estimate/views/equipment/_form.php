<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\LabourMaster */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="equipment-form form-horizontal">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'name', [
                'template' => "{label}\n
                <div class='col-md-6 col-xs-12'>
                    {input}\n
                    {hint}\n
                    {error}
                </div>",
                'labelOptions' => [ 'class' => 'col-md-3 col-xs-12 control-label' ]
            ])->textInput(['maxlength' => true,'placeholder' =>"Enter Name"]) ?>

            <?= $form->field($model, 'description', [
                'template' => "{label}\n
                <div class='col-md-6 col-xs-12'>
                    {input}\n
                    {hint}\n
                    {error}
                </div>",
                'labelOptions' => [ 'class' => 'col-md-3 col-xs-12 control-label' ]
            ])->textArea(['maxlength' => true,'placeholder' =>"Enter description"]) ?>
             <?= $form->field($model, 'cost', [
                'template' => "{label}\n
                <div class='col-md-6 col-xs-12'>
                    {input}\n
                    {hint}\n
                    {error}
                </div>",
                'labelOptions' => [ 'class' => 'col-md-3 col-xs-12 control-label' ]
            ])->textInput(['maxlength' => true,'placeholder' =>"Enter Cost"]) ?>
            </div>
        
    </div>
    <div>&nbsp</div>
    <div class="row">
        <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label" for=""></label>
            <div class="col-md-6 col-xs-12">
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
