<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LabourMaster */

$this->title = 'Create Equipment';
$this->params['breadcrumbs'][] = ['label' => 'Equipment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="equipment-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
