<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use dosamigos\ckeditor\CKEditor;
use yii\web\UploadedFile;
use kartik\file\FileInput;
$this->title = 'Import CSV';
$this->params['breadcrumbs'][] = ['label' => 'Resource Master', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$baseurl = Yii::$app->params['domain_url'] ;
$samplecsv = $baseurl.'web/samplecsv/DirectorySample.csv';
// pr($samplecsv);
$form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);
?>
<div class-ibox-content>


    <div class="row pull-right">

        <div class="col-md-4"> <a class="btn btn-primary" href="<?php echo $samplecsv; ?>"><i class="fa fa-upload"></i> Download sample csv</a>&nbsp;</div>
    </div>
    <div class="row">
        <div class="col-md-2"></div>
    <div class="col-md-6">
        <label>Import(CSV File)</label>


        <?php
        echo FileInput::widget([
            'model' => $model,
            'attribute' => 'file',
            'name' => 'file',
            'options'=>[
                'multiple'=>false,


            ],
            'pluginOptions' => [
                // 'uploadUrl' => Url::to(['upload']),

                'browseClass' => 'btn btn-success',
                'uploadClass' => 'btn btn-info',
                'removeClass' => 'btn btn-danger',
                'removeIcon' => '<i class="glyphicon glyphicon-trash"></i> ',
                'removeLabel' => 'Cancel',
            ],

        ]);


        ?>
    </div>
    </div>


</div>


<?php ActiveForm::end(); ?>
