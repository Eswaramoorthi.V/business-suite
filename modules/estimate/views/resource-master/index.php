<?php
use kartik\tree\TreeView;
use app\models\Tree;

//$t = new Tree(['name' => 'Level1']);
//$l=$t->makeRoot();
//$russia = new Tree(['name' => 'Level1.1']);
//$r=$russia->prependTo($t);
//pr($r);
 
echo TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => Tree::find()->addOrderBy('root, lft'), 
    'headingOptions' => ['label' => 'Resource Master'],
    'fontAwesome' => true,     // optional
    'isAdmin' => false,        // optional (toggle to enable admin mode)
    'displayValue' => 1,        // initial display value
    'softDelete' => false,
    'cacheSettings' => [        
        'enableCache' => true   // defaults to true
    ],
    'rootOptions' => ['label'=>'<span class="text-primary">Resource Master</span>'],
    'topRootAsHeading' => false, // this will override the headingOptions
    'fontAwesome' => true,
]);


?>