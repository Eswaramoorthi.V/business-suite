

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

$this->params['breadcrumbs'][] = 'Activity BOM';
?>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-etsy" aria-hidden="true"></i> Activity BOM (Bill Of Material)
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
  <?php echo Html::a(Yii::t ( 'app', 'Add' ), ['create'],['class' => 'btn btn-success ',$visibleList['create']]) ?>
    </div>
</div>
<?php
Modal::begin([
    'header' => '<h4>Add Activity BOM</h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Estimate Config Settings</h4>',
    'id' => 'update',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalupdate'><div class='loader'></div></div>";
Modal::end();
?>


<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'activity_id',
                'header' => '<div style="width:120px;">Activity</div>',
                'value' => function($model) {
                    return  isset($model->activity->name)?$model->activity->name:"";
                }
            ],
            [
                'attribute' => 'sub_activity_id',
                'header' => '<div style="width:120px;">Sub Activity</div>',
                'value' => function($model) {
                    return  isset($model->subactivity->name)?$model->subactivity->name:"";
                    //return  $model->subactivity->name;
                }
            ],
          
            [
                
                'class' => '\kartik\grid\ActionColumn',
                'mergeHeader' => false,
                    'contentOptions' => ['style' => 'width:50px;text-align:center','class'=>'skip-export'],
                    'headerOptions' =>['class'=>'skip-export'],
                    'header'=>"Actions",
                'template'=>$actionList,
                'dropdown' => true,
                'dropdownOptions'=>['class'=>'pull-right' ],
                'options' => ['style' => 'max-width:20px;'],
                'buttons' => [
                   
                    'update' => function($url,$model) {
                        $url=$url."&activity_id=".$model->activity_id;
                        return Html::a('Update',   $url,['class' => 'action-btn dropdown-item', 'title' => 'Update']
                        );
                    },
                    'deletes' => function($url) {
                       return Html::a('Delete',     $url,  ['class' => 'action-btn dropdown-item','data-method'=>'post', 'data-pjax'=>0,'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?']]
                       );
                   },
                 
                ]
                
                ],
        ],
        'summary'=>'',
        
       
        
    ]);
}
?>                                   
<?= $this->registerJs("
    $(function(){
$('#modalButton').click(function() {
    $('#modal').modal('show')
    .find('#modalContent')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>

<?=
$this->registerJs(
        "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.update').click(function(e){
       e.preventDefault();      
       $('#update').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
?>

<style>
a.action-btn.dropdown-item {
  display: block;
  width: 100%;
  padding: .25rem 1.5rem;
  clear: both;
  font-weight: normal;
  text-align: inherit;
  white-space: nowrap;
  background-color: transparent;
  border: 0;
  color: navy;
  font-size: small;
}
</style>