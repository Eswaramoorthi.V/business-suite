<?php

use execut\widget\TreeView;
use yii\web\JsExpression;
use kartik\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\editable\Editable;
use kartik\builder\Form;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\web\View;
use yii\helpers\Url; 
use yii\bootstrap\Modal;
use kartik\date\DatePicker;
use kartik\time\TimePicker;
use yii\data\ActiveDataProvider;
use app\modules\estimate\models\Settings;
use app\models\MaterialMaster;
use app\models\LabourMaster;
use app\modules\estimate\models\Equipment;
use app\models\Tree;

$this->title = 'Update Activity BOM';
$this->params['breadcrumbs'][] = ['label' => 'Activity BOM', 'url' => ['master']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';

?>
<style type="text/css">
    .margin-right-10 {
        margin-right: 10px;
    }
   warning.active:focus {
    background-color: #57b495;
    border-color: #57b495;
    color: #FFFFFF;
    }
    img.ObservationAttachments {
        padding: 4px;
    }
    .files{
        overflow: hidden;
    }
    .files .form-group{
        margin-bottom: 0px;
    }
    .files .help-block{
        display: none;
    }
    .draggable td select{
        padding-right: 18px !important;;
    }
</style>

<div class="col-md-12 text-left add-label" style="font-size: 32px">
    Update Activity BOM 
</div>
<h4 class="error-msg hide text-danger"> </h4>
 <div class ="row">
        <div class="col-md-6">
            <label class="control-label">Activity</label>
            <?=
            Select2::widget([
                'name' => 'activity_id',
                'data' => $activityList,
                'value'=>$estimateMaster['activity_id'],
                'options' => [
                    'id'=>'activity_id',
                    'placeholder'=>'Select Activity',
                    'class' => 'form-control activity_id',
                    // 'readonly'=>true
                ],
                // 'pluginOptions' => [
                //     'disabled' => true
                // ]
            ]);
            ?> 
        </div>
        <div class="col-md-6">
            <label class="control-label">Sub Activity</label>
            <?=
            Select2::widget([
                'name' => 'sub_activity_id',
                'data' => $subActivityList,
                'value'=>$estimateMaster['sub_activity_id'],
                'options' => [
                    'id'=>'sub_activity_id',
                    'placeholder'=>'Select Sub Activity',
                    'class' => 'form-control sub_activity_id',
                    
                ],
                // 'readonly'=>true
                // 'pluginOptions' => [
                //     'disabled' => true
                // ]
            ]);
            ?> 
        </div>
    </div>
    <div>&nbsp</div>
    <div class ="row">
       <div class="col-md-6">
       <label class="control-label">Unit</label>
           <?=
        Select2::widget([
            'name' => 'unit_id',
            'data' => $unitTypeList,
            'value'=>$estimateMaster['unit_id'],
            'options' => [
                'id'=>'unit_id',
                'placeholder'=>'Select Unit Type',
                'class' => 'form-control unit_id',
            ],
        ]);
            ?> 
        </div>  
        </div>
<div>&nbsp</div>

<div class="col-md-12 text-left">
            <label class="control-label">Description</label>
            <textarea id="description" class="form-control description"  rows="2" placeholder="Enter Description" aria-required="true" aria-invalid="true"><?=$estimateMaster['description']?></textarea>
        </div>
        <div>&nbsp</div>
<div class="row">
        <div class="col-md-2">
            <label class="control-label">Total Cost</label>
            <input type="text" value=<?=$totalCost?> class="form-control totalcost" readOnly="true" id = "totalcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">Material Cost</label>
            <input type="text" value=<?=$materialCost?> class="form-control materialcost" readOnly="true" id = "materialcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">Labour Cost</label>
            <input type="text" value=<?=$labourCost?> class="form-control labourcost" readOnly="true" id = "labourcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">Profit</label>
            <input type="text" value=<?=$profitCost?> class="form-control profitcost" readOnly="true" id = "profitcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">OverHead</label>
            <input type="text" value=<?=$overheadCost?> class="form-control overheadcost" readOnly="true" id = "overheadcost">
        </div>
        <div class="col-md-2">
            <label class="control-label">Equipment Cost</label>
            <input type="text" value=<?=$equipmentCost?> class="form-control equipmentcost" readOnly="true" id = "equipmentcost">
        </div>
    </div>

    <div>&nbsp</div>

<html>
<script src="<?= Url::base() ?>/web/lib/js/jexcel/jquery.min.js"></script>
<script src="<?= Url::base() ?>/web/lib/js/jexcel/jexcel.js"></script>
<!-- <script src="https://bossanova.uk/jsuites/v2/jsuites.js"></script> -->
<script src="<?= Url::base() ?>/web/lib/js/jexcel/jsuites.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jexcel/v3/jexcel.css" type="text/css" />
<link rel="stylesheet" href="https://bossanova.uk/jsuites/v2/jsuites.css" type="text/css" />
<script src="<?= Url::base() ?>/web/lib/js/jexcel/numeral.min.js"></script>
<script src="https://bossanova.uk/jexcel/v4/jexcel.js"></script>
<link rel="stylesheet" href="https://bossanova.uk/jexcel/v4/jexcel.css" type="text/css" />


<div class="row">
    <div class="col-md-12">
      <div id="spreadsheet"></div>
    </div>
</div>    

<div class="row">
    <div class="col-md-6 text-left add-label" style="padding-top: 12px">
        <input type='button' value='Add New Row' id="new"  class="btn btn-success btn-sm margin-left-10">
        <input type='button' value='Delete Last Row' id="delete"  class="btn btn-success btn-sm margin-left-10">
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
        <input type='button' value='Save' id="getDataButton" class="btn btn-success btn-sm margin-right-10">
    </div>
</div>
</html>

<script>
var data = <?= json_encode($arrayData) ?>;

dropdownFilter = function(instance, cell, c, r, source) {
    var value = instance.jexcel.getValueFromCoords(c - 1, r);
    if (value == 1) {
        return <?=json_encode($resourcedata[1])?>;
    } else if (value == 2) {

        return <?=json_encode($resourcedata[2])?>;
    } else if (value == 3){
        return <?=json_encode($resourcedata[3])?>;
    }
    else if (value == 4){
        return <?=json_encode($resourcedata[4])?>;
    }
    else {
        return <?=json_encode($resourcedata[5])?>;
    }

}

var SUMCOL = function(instance, columnId) {
    var total = 0;
    for (var j = 0; j < instance.options.data.length; j++) {
        if (Number(instance.records[j][columnId-1].innerHTML)) {
            // total += Number(instance.records[j][columnId-1].innerHTML);
            total += Math.ceil((Number(instance.records[j][columnId-1].innerHTML))*100)/100;
        }
    }    
    return total;
}

jexcel(document.getElementById('spreadsheet'), {
    data:data,
    columns: [
        { type: 'dropdown', title:'Resource Type', width:'110',source: <?=json_encode($estimateTypeList)?> },
        { type: 'dropdown', title:'Item Type', width:'300', filter:dropdownFilter,  source:<?php echo json_encode($itemTypeList)?>, autocomplete:true},
        { type: 'numeric', title:'Last Purchase Cost', width:'110', },
        { type: 'dropdown', title:'Unit Type', width:'100',class: 'unit_type', source: <?=json_encode($unitType)?> },
        { type: 'numeric', title:'Quantity', width:'90' },
        { type: 'numeric', title:'Actual Cost', width:'90' },
        { type: 'numeric', title:'Average Weighted Cost', width:'100', readOnly: true },
        { type: 'numeric', title:'Formula', width:'70' },
        { type: 'numeric', title:'Percentage', width:'100' },
    ],
    updateTable:function(instance, cell, col, row, val, label, cellName) {
        if (col == 2 ||  col == 4 ) {
            txt = numeral(val).format('0');
            $(cell).html(' ' + txt);
        }
    },
    footers: [['','','','Total','=ROUND((SUMCOL(TABLE(), COLUMN())),2)','','']],
    onchange:function(instance, cell, c, r, value) {
        if(c==1){
            var itemType = instance.jexcel.getValueFromCoords(c, r);
            var columnLPC= parseInt(c) + parseInt(1);
            var columnName = jexcel.getColumnNameFromId([columnLPC, r]);
            var quantityC= parseInt(c) + parseInt(3);
            var quantitycolumn = jexcel.getColumnNameFromId([quantityC, r]);
            var quantity=1;
            var resData = 0;
                $.ajax({
                    url: './get-item-cost?id='+itemType,
                    type: "post",
                    success: function (data) {
                    resData = data;
                    instance.jexcel.setValue(columnName, resData); 
                    instance.jexcel.setValue(quantitycolumn, quantity);   
                    }
                });
        }
        if(c==5) {
            var data = $('#spreadsheet').jexcel("getData");
        // console.log('data',data)
            var actualCostValue =instance.jexcel.getValueFromCoords(5, r);
           
            var matCount = 0;
            var labCount = 0;
            var equiCount= 0;
            var profitCount= 0;
            var overheadCount= 0;
            var estimateType = instance.jexcel.getValueFromCoords(0, r);
            var estimateTypeData = <?=json_encode($listDataVal)?>;
            // console.log('estimateTypeData',estimateTypeData);
            for(var i = 0; i < data.length; i++) {
                var val = data[i];
              
                if(estimateTypeData[val[0]].toLowerCase()=='material' ){
                    matCount = parseInt(matCount)+parseInt(val[5]);
                } else if(estimateTypeData[val[0]].toLowerCase()=='labour'){
                    labCount=parseInt(labCount)+parseInt(val[5]);
                } else if(estimateTypeData[val[0]].toLowerCase()=='equipment'){
                    equiCount=parseInt(equiCount)+parseInt(val[5]);
                } else if(estimateTypeData[val[0]].toLowerCase()=='overhead'){
                    overheadCount=parseInt(overheadCount)+parseInt(val[5]);
                } else if(estimateTypeData[val[0]].toLowerCase()=='profit'){
                    profitCount=parseInt(profitCount)+parseInt(val[5]);
                }
            }
            $('.materialcost').val(parseInt(matCount));
            $('.labourcost').val(parseInt(labCount));
            $('.equipmentcost').val(parseInt(equiCount));
            $('.overheadcost').val(parseInt(overheadCount));
            $('.profitcost').val(parseInt(profitCount));
            $('.totalcost').val(parseInt(profitCount)+ parseInt(matCount)+ parseInt(labCount) +parseInt(equiCount)+ parseInt(overheadCount));            
            var totalvalue  = $('.totalcost').val() ;
            var percentageC= parseInt(c) + parseInt(3);
            if(r==0) {
                var percentageColumn = jexcel.getColumnNameFromId([percentageC, r]);
                var percentage= (actualCostValue/totalvalue)*100;

                instance.jexcel.setValue(percentageColumn, percentage.toFixed(2));
            } else {
                var p;
                for(p=0;p<=r;p++){
                    var actualCostValue =instance.jexcel.getValueFromCoords(5, p);
                    var percentageColumn = jexcel.getColumnNameFromId([percentageC, p]);
                    var percentage= (actualCostValue/totalvalue)*100;
                    instance.jexcel.setValue(percentageColumn, percentage.toFixed(2));
                }
            }
        }
    },
    columnSorting:false,
});
</script>

<!-- <script src ="js/jexcel/jquery.min.js"> -->
<script>
$('#getDataButton').on('click', function () {
    var data = $('#spreadsheet').jexcel("getData");
    $('.error-msg').addClass("hide");
    var activity = $('#activity_id').val();
    var subActivity = $('#sub_activity_id').val();
    var unit_id = $('#unit_id').val();
   
    var description = $('#description').val();
    if(activity == ''){
        $('.error-msg').removeClass("hide");
        $('.error-msg').html("Please Choose Activity");
        $('#activity_id').select2('focus');
    } else if(subActivity == '') {
        $('.error-msg').removeClass("hide");
        $('.error-msg').html("Please Choose Sub Activity");
        $('#sub_activity_id').select2('focus');
    }  else if(description == ''){
        $('.error-msg').removeClass("hide");
        $('.error-msg').html("Please Enter Description");
        $('#description').input('focus');
    } else {
        var formdata = JSON.stringify(data);
      
        $.ajax({
            url: './update?id=<?=$_GET['id']?>',
            data: {'formdata': data,'activity': activity,'subActivity':subActivity,  'description':description,'unit_id':unit_id },
            type: 'POST',
            success: function (response) {
                console.log(response);
            },
            error: function (error) {
                console.log(error);
            }
        });

    }
});

$('#new').on('click', function () {
    $('#spreadsheet').jexcel('insertRow'); event.preventDefault(); return false;
});

$('#delete').on('click', function () {
    $('#spreadsheet').jexcel('deleteRow'); event.preventDefault(); return false;
});

$(function () {
    $('#activity_id').change(function () {
        var activityId = $(this).val();
        $.ajax({
            url: "./get-sub-activity",
            data: {'activity': activityId},
            type: "post",
            success: function (data) {
                $('#sub_activity_id').empty().append(data);
            }
        });
    });
});

$(function () {
    $('#sub_activity_id').change(function () {
        var sub_activity = $(this).val();
        $.ajax({
            url: "./get-description",
            data: {'sub_activity': sub_activity},
            type: "post",
            success: function (data) {
                $('#description').val(data);
            }
        });
    });
});

$(function () {
    $('#sub_activity_id').change(function () {
        var sub_activity = $(this).val();
        var activityId = $('.activity_id').val();    
        $.ajax({
            url: "./get-estimate-master-data",
            data: {  'activity_id' :activityId,'sub_activity_id':sub_activity},
            type: "post",
            success: function (data) {
                if( data && data !=''){
                    window.location = "/pricingcalculator/estimate/estimate-master/update?id=" + data+"&activity_id="+ activityId;
                }else{
                    window.location = "/pricingcalculator/estimate/estimate-master/create?activity_id=" + activityId+"&sub_activity_id="+sub_activity;
                }
            }
        });
    });
});

</script>
