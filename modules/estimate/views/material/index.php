

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use app\modules\estimate\models\EstimateSetting;

$this->params['breadcrumbs'][] = 'Material Master';
?>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-wrench" aria-hidden="true"></i> Material Master
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
       <?php echo Html::a(Yii::t ( 'app', 'Add' ), ['create'],['class' => 'btn btn-success btn-sm ']) ?>
    </div>
</div>
<?php
Modal::begin([
    'header' => '<h4>Add Material Master</h4>',
    'id' => 'addModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalAddContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Material Master</h4>',
    'id' => 'updateModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalUpdateContent'><div class='loader'></div></div>";
Modal::end();
?>


<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
          
            [
                'attribute' => 'name',
                'header' => '<div style="width:120px;">Name</div>',
                'value' => 'name',
            ],
            [
                'attribute' => 'description',
                'header' => '<div style="width:150px;">Description</div>',
                'value' => 'description',
            ],
            [
                'attribute' => 'on_date',
                'header' => '<div style="width:120px;">On Date</div>',
                'value' => 'on_date',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:80px;text-align:center','class'=>'skip-export'],
                'headerOptions' =>['class'=>'skip-export'],
                'header' => "Actions",
                'template' => ' {update}{deletes}',
                'buttons' => [
                  
                    'update' => function($url) {
                        return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'updateModalButton1', 'title' => 'Edit']);
                    },
                    'deletes' => function ($url,$model) {
                        return Html::a(Yii::t ( 'app', '<span class="glyphicon glyphicon-trash icons"></span>' ), ['deletes', 'id'=>$model->id],['class' => 'updateModalButton1', ]);
                    },
                ],
            ],
        ],
    ]);
}
?>                                   
<?= $this->registerJs("
$(function(){
$('#addModalButton').click(function() {
    $('#addModal').modal('show')
    .find('#modalAddContent')
    .load($(this).attr('value'));
});
});
", View::POS_READY); ?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updateModalButton').click(function(e){
        e.preventDefault();      
        $('#updateModal').modal('show')
        .find('#modalUpdateContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>
