<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MaterialMaster */

$this->title = 'Update Material';
$this->params['breadcrumbs'][] = ['label' => 'Material', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="material-master-update">

    <?= $this->render('_form_update', [
        'model' => $model, 'update'=>1,
        'materialUnitCost'=>$materialUnitCost
    ]) ?>

</div>
