<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MaterialMaster */

$this->title = 'Create Material';
$this->params['breadcrumbs'][] = ['label' => 'Material', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="material-master-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
