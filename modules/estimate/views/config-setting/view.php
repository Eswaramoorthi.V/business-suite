<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\EstimateSetting */

$this->title = $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Activity Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-master-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'description',
            'status',
            'parent_id',
            'created_by',
            'created_on',
            'updated_on',
            'updated_by',
            'system_datetime',
        ],
    ]) ?>

</div>
