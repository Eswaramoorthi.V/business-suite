<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstimateSetting */

$this->title = 'Update Activity Master: ' . $model->code;
$this->params['breadcrumbs'][] = ['label' => 'Activity Masters', 'url' => ['index']];
// $this->params['breadcrumbs'][] = ['label' => $model->code, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="category-master-update">

    <?= $this->render('_form', [
        'model' => $model, 'update'=>1
    ]) ?>

</div>
