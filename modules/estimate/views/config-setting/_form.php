<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\EstimateSetting */
/* @var $form yii\widgets\ActiveForm */

$configSettingTypeList = Yii::$app->params['configSettingType'];
$parentCategoryList = ArrayHelper::map($model->getParentSettingList(), 'id', 'description');

?>

<div class="category-master-form form-horizontal">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <?php 
            if(!isset($update)) {    
                $getLastRecord=$model->find()->orderBy(['id' => SORT_DESC])->asArray()->one();
            if(!empty($getLastRecord)){
                $catId=$getLastRecord['id']+1;
            } else{
                $catId=1;
            }
            $pmCode = "C_"  . str_pad($catId, 4, '0', STR_PAD_LEFT);
            $model->code=$pmCode;
        ?>
        <div class="col-md-12">
            <?= $form->field($model, 'code', [
                'template' => "{label}\n
                <div class='col-md-6 col-xs-12'>
                    {input}\n
                    {hint}\n
                    {error}
                </div>",
                'labelOptions' => [ 'class' => 'col-md-3 col-xs-12 control-label' ]
            ])->textInput(['maxlength' => true,'readonly'=>true]) ?>
        <?php } else { ?>
            <div class="form-group field-categorymaster-description">
                <label class="col-md-3  control-label">Code : </label>
                <label class="col-md-1  control-label"><?php echo $model->code;?></label>
            </div>
        <?php } ?>

            <?= $form->field($model, 'description', [
            'template' => "{label}\n
            <div class='col-md-6 col-xs-12'>
                    {input}\n
                    {hint}\n
                    {error}
            </div>",
            'labelOptions' => [ 'class' => 'col-md-3 col-xs-12 control-label' ]
            ])->textarea(['rows' => 2,'placeholder' =>"Enter Name"]) ?>  

            <?= $form->field($model, 'type', [
            'template' => "{label}\n
            <div class='col-md-6 col-xs-12'>
                    {input}\n
                    {hint}\n
                    {error}
            </div>",
            'labelOptions' => [ 'class' => 'col-md-3 col-xs-12 control-label' ]
            ])->dropDownList($configSettingTypeList, [
                'class' => 'select from control',
                // 'prompt' => 'Choose Config Type',
                'data-live-search' => 'true'
            ]) ?>
            
            <?= $form->field($model, 'parent_id', [
            'template' => "{label}\n
            <div class='col-md-6 col-xs-12'>
                    {input}\n
                    {hint}\n
                    {error}
            </div>",
            'labelOptions' => [ 'class' => 'col-md-3 col-xs-12 control-label' ]
            ])->dropDownList($parentCategoryList, [
                'class' => 'select from control',
                'prompt' => 'Choose Parent',
                'data-live-search' => 'true'
            ]) ?>
        
            <?= $form->field($model, 'description1', [
            'template' => "{label}\n
            <div class='col-md-6 col-xs-12'>
                    {input}\n
                    {hint}\n
                    {error}
            </div>",
            'labelOptions' => [ 'class' => 'col-md-3 col-xs-12 control-label' ]
            ])->textarea(['rows' => 3,'placeholder' =>"Enter Description"]) ?>
        </div>
        <div class="form-group">
            <label class="col-md-3 col-xs-12 control-label" for=""></label>
            <div class="col-md-6 col-xs-12">
            <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
