

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use app\modules\estimate\models\EstimateSetting;

$this->params['breadcrumbs'][] = 'Estimate Config Settings';
?>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-wrench" aria-hidden="true"></i> Estimate Config Settings
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
       <?= Html::button('Add', ['value' => Url::to('create?r=estimate/config/create'), 'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
    </div>
</div>
<?php
Modal::begin([
    'header' => '<h4>Add Estimate Config Settings</h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Estimate Config Settings</h4>',
    'id' => 'update',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalupdate'><div class='loader'></div></div>";
Modal::end();
?>


<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
          
            [
                'attribute' => 'description',
                'header' => '<div style="width:120px;">Name</div>',
                'value' => 'description',
            ],
            
            [
                'attribute' => 'parent_id',
                'header' => '<div style="width:120px;">Parent</div>',
                'value' => function($model) {
                 $category = ArrayHelper::map(EstimateSetting::find()->where(['status'=>1,'parent_id'=>null])->all(),'id','description');
//pr($category);                 
return !empty($model->parent_id)? (isset($category[$model->parent_id])?$category[$model->parent_id]:'-'):'-';            
                }
            ],
            [
                'attribute' => 'description1',
                'header' => '<div style="width:120px;">Description</div>',
                'value' => function($model) {
                 return !empty($model->description1)? $model->description1:'-';            
                }
            ],
           
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:80px;text-align:center','class'=>'skip-export'],
                'headerOptions' =>['class'=>'skip-export'],
                'header' => "Actions",
                'template' => ' {update}{deletes}',
                'buttons' => [
                  
                    'update' => function($url) {
                        return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'update', 'title' => 'Edit']
                        );
                    },
                    'deletes' => function ($url) {
                        return Html::a(
                                        '<span class="glyphicon glyphicon-trash icons"></span>', $url, [
                                    'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                                        ]
                        );
                    },
                ],
            ],
        ],
        'summary'=>'',
        
       
        
    ]);
}
?>                                   
<?= $this->registerJs("
    $(function(){
$('#modalButton').click(function() {
    $('#modal').modal('show')
    .find('#modalContent')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>

<?=
$this->registerJs(
        "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.update').click(function(e){
       e.preventDefault();      
       $('#update').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
?>
