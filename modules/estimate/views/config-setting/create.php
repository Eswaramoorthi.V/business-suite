<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CategoryMaster */

$this->title = 'Create Activity Master';
$this->params['breadcrumbs'][] = ['label' => 'Activity Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="category-master-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
