<?php
use kartik\tree\TreeView;
use app\models\SettingsTree;
 
echo TreeView::widget([
    // single query fetch to render the tree
    // use the Product model you have in the previous step
    'query' => SettingsTree::find()->addOrderBy('root, lft'), 
    'headingOptions' => ['label' => 'Settings'],
    'fontAwesome' => true,     // optional
    'isAdmin' => false,        // optional (toggle to enable admin mode)
    'displayValue' => 1,        // initial display value
    'softDelete' => false,
    'cacheSettings' => [        
        'enableCache' => true   // defaults to true
    ],
    'nodeView' => '@kvtree/views/_form1',
    'rootOptions' => ['label'=>'<span class="text-primary">Settings</span>'],
    'topRootAsHeading' => false, // this will override the headingOptions
    'fontAwesome' => true,
]);


?>