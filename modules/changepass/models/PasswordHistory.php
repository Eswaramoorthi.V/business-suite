<?php
namespace app\modules\changepass\models;
use Yii;
//use yii\base\Model;
use \dektrium\user\models\User;
use app\models\BaseModel;

class PasswordHistory extends BaseModel {

    public static function tableName() {
        return 'user_pass';
    }

    public function rules() {
        return [
               [['user_id', 'password_hash'], 'safe']             
        ];
    }

    public function getPasswords($limit=3) {
        $result = $this->find()->where([
                    'user_id' => Yii::$app->user->identity->id
                ])->all();
        
        return $result;
//        $password = $user->password_hash;
//        if (!Yii::$app->security->validatePassword($this->oldpass, $password))
//            $this->addError($attribute, 'Old password is incorrect');
    }



}
