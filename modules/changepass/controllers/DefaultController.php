<?php

namespace app\modules\changepass\controllers;

use Yii;
use yii\web\Controller;
use app\modules\changepass\models\PasswordForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use \dektrium\user\models\User;
use app\modules\changepass\models\PasswordHistory;

class DefaultController extends Controller {

    public $accessRole = '@';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                        [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                        [
                        'actions' => ['logout', 'index', 'changepassword'],
                        'allow' => true,
                        'roles' => [$this->accessRole],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $model = new PasswordForm;
        $modeluser = User::find()->where([
                    'username' => Yii::$app->user->identity->username
                ])->one();

        $getPasswords = PasswordHistory::find()->where([
                    'user_id' => Yii::$app->user->identity->id
                ])->all();

        $oldPasswords = [];
        if (!empty($getPasswords)) {
            foreach ($getPasswords as $passdata) {
                $oldPasswords[] = $passdata['password_hash'];
            }
        }

        if ($model->load(Yii::$app->request->post())) {
            // if ($model->validate()) {
                try {
                    $newPass = md5($_POST['PasswordForm']['newpass']);
                    $modeluser->password_hash = Yii::$app->security->generatePasswordHash($_POST['PasswordForm']['newpass']);
                    $modeluser->reset_password = 1;

                    if (!in_array($newPass, $oldPasswords)) {

                        if ($modeluser->save()) {
                            $passwordHistory = new PasswordHistory;
                            $passwordHistory->user_id = Yii::$app->user->identity->id;
                            $passwordHistory->password_hash = $newPass;
                            $passwordHistory->save();

                            // pr($passwordHistory);
                            Yii::$app->getSession()->setFlash(
                                    'success', 'Password changed'
                            );
                            return $this->redirect(['index']);
                        } else {
                            Yii::$app->getSession()->setFlash(
                                    'error', 'Password not changed'
                            );
                            return $this->redirect(['index']);
                        }
                    } else {
                        Yii::$app->getSession()->setFlash(
                                'error', 'Please do not use old password'
                        );
                        return $this->render('changepassword', [
                                    'model' => $model
                        ]);
                    }
                } catch (Exception $e) {
                    Yii::$app->getSession()->setFlash(
                            'error', "{$e->getMessage()}"
                    );
                    return $this->render('changepassword', [
                                'model' => $model
                    ]);
                }
            // } else {
            //     return $this->render('changepassword', [
            //                 'model' => $model
            //     ]);
            // }
        } else {
            return $this->render('changepassword', [
                        'model' => $model
            ]);
        }
    }

}
