<?php

namespace app\modules\changepass;

class changepass extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\changepass\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
