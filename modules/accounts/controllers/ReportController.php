<?php

namespace app\modules\accounts\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;

use app\models\Search\AwardedprojectSearch;

use app\controllers\BaseController;

/**
 * CategoryMasterController implements the CRUD actions for CategoryMaster model.
 */
class ReportController extends BaseController
{

    public function actionProject() {

        $searchModel = new AwardedprojectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination = ['PageSize'=>50];
        return $this->render('project', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


}
