<?php
namespace app\modules\accounts\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use app\models\PettyCash;
use app\models\Products;
use app\models\Employee;
use app\models\Search\PettyCashSearch;
use app\models\Search\BalanceHistorySearch;

use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use app\controllers\BaseController;

class PettyCashController extends BaseController {

    public function actionIndex() {
        if (empty(Yii::$app->session->get('sdate'))) {
            Yii::$app->session->set('sdate', date('Y-m-01'));
            Yii::$app->session->set('edate', date('Y-m-t'));
        }
        $startDate = Yii::$app->session->get('sdate');
        $endDate = Yii::$app->session->get('edate');
        $startDateDisp = Yii::$app->formatter->asDate($startDate, 'php:Y-m-d');
        $endDateDisp = Yii::$app->formatter->asDate($endDate, 'php:Y-m-d');
        $dataRange = $startDateDisp . " - " . $endDateDisp;                                  

   
        $searchModel = new PettyCashSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
          
       $dataProvider->pagination = ['pageSize' => 50];

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
            'dataRange' => $dataRange ,
        ]);
    }

    public function actionCreate() {

      $model = new PettyCash();
      $products = new Products();
        if ($model->load(Yii::$app->request->post())) {
            //pr(Yii::$app->request->post());

             $postData = Yii::$app->request->post();
            //pr($postData);
            // die;

//            $model->date = date('Y-m-d', strtotime($postData['model']['date']));
            $model->date = date('Y-m-d', strtotime($model['date']));
            $employeeId = $model['employee_id'];
            
            $employeeLastRecord = PettyCash::find()->orderBy(['id' => SORT_DESC])->andWhere(['employee_id'=>$employeeId])->asArray()->one();
            $lastbalance = $employeeLastRecord['cash_balance'];
           

            if($model['credit_expense'] == 'Credit') {
                
                $model->cash_balance = $lastbalance + $model['credit_amount'] ;
             }
            else {
                
                $model->cash_balance = $lastbalance - $model['expense_amount'] ;
            }
            
            
                     
            
           if($model->type == '1') {

//            $products = new Products();
//             $productDatas = $postData['Products'];
             
            //$projectID = $products->project_id;
                            $filenameDoc = '';
                            $upload = UploadedFile::getInstance($model, 'document');
                            if (!empty($upload)) {
              
                            $dirname = $postData['PettyCash']['project_id'];
                            $Subfolder = "material-procure";
                            $folder = 'uploads/project/' . $dirname . '/';
                            $filename = 'uploads/project/' . $dirname . '/'.$Subfolder.'/';
              
                    
                            if (file_exists($filename)) {
                                $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;

                            } else {
                                if (file_exists($folder)) {

                                mkdir('uploads/project/' . $dirname . '/'.$Subfolder.'/');
                                $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                               }
                                else{
                                mkdir('uploads/project/' . $dirname . '/');
                                mkdir('uploads/project/' . $dirname . '/'.$Subfolder.'/');
                                $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;

                                 } 

                            }
                            $upload->saveAs($filenameDoc); 
                            $fileName = $upload->baseName . '_' . time() . '.' . $upload->extension;
                              }        
           
       $model->document = isset($fileName)?$fileName:'';
        
       $vatpercentage = $postData['PettyCash']['vat'];
       $amount = $postData['PettyCash']['amount'];

       $model->vat = ($amount *$vatpercentage) / 100;
//                            $products->petty_cash_id = $model->id;
//                            $products->price = $model->expense_amount;
//                            $products->purchase_date = $model->date;
//                            $products->description = $model->description;
//                            $products->project_id = $productDatas['project_id'];
//                            $products->suppliers_id = $productDatas['suppliers_id'];
//                            $products->item_number = $productDatas['item_number'];
//                            $products->name = $productDatas['name'];
//                            $products->document = isset($fileName)?$fileName:'';
//
//                   if (!$products->save()) {
//                                pr($products->getErrors());
//                            }    
        }
          if (!$model->save()) {
                        pr($model->getErrors());
                    }  
                 return $this->redirect(yii::$app->request->referrer);
            
        } 
         else {

            return $this->renderAjax('create', ['model' => $model, 'products' => $products]);
        }
}

    public function actionUpdate($id) {

       
        $model = $this->findModel($id);
        if($model->type == '1') {
        $products = Products::find()->where(['petty_cash_id' => $id])->one();
        } else {
             $products = new Products();
        }
        //$products = new Products();
         //$REQUESTId = $_REQUEST['id'];
        
   
      
    
       
      //pr($model->type);
       
      // pr($_REQUEST);
        $lastEmployeeID = $model->employee_id;
        $lastTransaction = $model->credit_expense;
        $lastBalance = $model->cash_balance;
        $lastCredit = $model->credit_amount;
        $lastExpense = $model->expense_amount;
        
        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();
            $pettyCashData = $request['PettyCash'];
           
            $creditAmount = $pettyCashData['credit_amount'];
            $expenseAmount = $pettyCashData['expense_amount'];
            $updateEmployeeid = $pettyCashData['employee_id'];
            
            //incase of change in credit/expense amount alone
            if ($pettyCashData['credit_expense'] == 'Credit' && $lastCredit != $creditAmount && $lastEmployeeID == $updateEmployeeid) {
                 
                 $model->cash_balance = ($lastBalance - $lastCredit) + $creditAmount; 

            } else if($pettyCashData['credit_expense'] == 'Expense' && $lastExpense != $expenseAmount && $lastEmployeeID == $updateEmployeeid) {
                
                $model->cash_balance = ($lastBalance + $lastExpense) - $expenseAmount; 
            }
            
            //in case of change in credit/expense alone
            if ($pettyCashData['credit_expense'] == 'Expense' && !empty($lastCredit) && $lastEmployeeID == $updateEmployeeid) {
                 
                 $model->cash_balance = ($lastBalance - $lastCredit) - $expenseAmount;
                 $model->credit_amount = '';
                 
            } else if($pettyCashData['credit_expense'] == 'Credit'  && !empty($lastExpense) && $lastEmployeeID == $updateEmployeeid) {
                
               $model->cash_balance = ($lastBalance + $lastExpense) + $creditAmount;
               $model->expense_amount = '';
            }
            
            //in case of change in employee alone
            
            $UpdatedEmployeeRecord = PettyCash::find()->orderBy(['id' => SORT_DESC])->andWhere(['employee_id'=>$updateEmployeeid])->asArray()->one();
            $lastEmployeeBalance = $UpdatedEmployeeRecord['cash_balance'];
           
            if ($pettyCashData['credit_expense'] == 'Credit' &&  $lastEmployeeID != $updateEmployeeid) {
                 
                $model->cash_balance = $lastEmployeeBalance + $creditAmount; 
          
            } else if($pettyCashData['credit_expense'] == 'Expense' &&  $lastEmployeeID != $updateEmployeeid) {
                
                $model->cash_balance = $lastEmployeeBalance - $expenseAmount; 
            }
            
            
          $model->date = date('Y-m-d', strtotime($model['date']));
         
           
           
            if($pettyCashData['type'] == '1') {

           // $products = Products::find()->where(['petty_cash_id' => $id])->one();
             //$productDatas = $request['Products'];
             
            //$projectID = $products->project_id;
                            $filenameDoc = '';
                            $upload = UploadedFile::getInstance($model, 'document');
                            if (!empty($upload)) {
              
                            $dirname = $pettyCashData['project_id'];
                            $Subfolder = "material-procure";
                            $folder = 'uploads/project/' . $dirname . '/';
                            $filename = 'uploads/project/' . $dirname . '/'.$Subfolder.'/';
              
                    
                            if (file_exists($filename)) {
                                $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;

                            } else {
                                if (file_exists($folder)) {

                                mkdir('uploads/project/' . $dirname . '/'.$Subfolder.'/');
                                $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                               }
                                else{
                                mkdir('uploads/project/' . $dirname . '/');
                                mkdir('uploads/project/' . $dirname . '/'.$Subfolder.'/');
                                $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;

                                 } 

                            }
                            $upload->saveAs($filenameDoc); 
                            $fileName = $upload->baseName . '_' . time() . '.' . $upload->extension;
                              }        
           
       
                           
                            $model->document =isset($fileName)?$fileName:'';
                            $vatpercentage = $pettyCashData['vat'];
                            $amount = $pettyCashData['amount'];

                            $model->vat = ($amount *$vatpercentage) / 100;

                    
        }   
           
            if (!$model->save()) {
                pr($model->getErrors());
            }
            
       
            return $this->redirect(yii::$app->request->referrer);
        } else {
            //pr($model->type);
            return $this->renderAjax('update', [
                        'model' => $model,'products' => $products
            ]);
        }
    }

    public function actionDeletes($id) {
        //pr($id);
          
         
        $this->findModel($id)->delete();
         Products::deleteAll(['petty_cash_id' => $id]);
          //return $this->redirect('index');
        return $this->redirect(Yii::$app->request->referrer);
    }
     public function actionDaterange() {        
        $sDate = Yii::$app->request->post('sdate');
        $eDate = Yii::$app->request->post('edate');         
        Yii::$app->session->set('sdate', $sDate);
        Yii::$app->session->set('edate', $eDate);
                
    }
     public function actionBalanceHistory($id) {
     $id = $_GET['id'];
     if (empty(Yii::$app->session->get('sdate'))) {
            Yii::$app->session->set('sdate', date('Y-m-01'));
            Yii::$app->session->set('edate', date('Y-m-t'));
        }
        $startDate = Yii::$app->session->get('sdate');
        $endDate = Yii::$app->session->get('edate');
        $startDateDisp = Yii::$app->formatter->asDate($startDate, 'php:Y-m-d');
        $endDateDisp = Yii::$app->formatter->asDate($endDate, 'php:Y-m-d');
        $dataRange = $startDateDisp . " - " . $endDateDisp;                                  

     //echo $cashBalance;
//        $employeeId = $cashDetails['employee_id'];
        
//        $dataProvider = new ActiveDataProvider([
//            'query' => PettyCash::find()
//                    ->Where(['employee_id' => $id])
//                ->andwhere(['>=','date', $startDate])->andWhere(['<=','date', $endDate])
//                
//        ]);
       $searchModel = new BalanceHistorySearch();
       $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $startDate, $endDate);
       $dataProvider->pagination = ['pageSize' => 50];

      
        
        //$sum = new ActiveDataProvider();
        //$dataProvider->pagination = ['pageSize' => 10];
        //pr($dataProvider);
        //$dataProvider->pagination  = false;
           // pr($dataProvider);
        
            
        return $this->render('balance_history', ['dataProvider' => $dataProvider, 'dataRange' => $dataRange ,'searchModel' => $searchModel ] );
        
    }
    
      
    protected function findModel($id) {
        if (($model = PettyCash::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    

    
}
