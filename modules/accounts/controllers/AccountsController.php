<?php

namespace app\modules\accounts\controllers;

use Yii;
use app\models\Accounts;
use app\models\PettyCash;
use app\models\InvoiceMaster;
use yii\web\NotFoundHttpException;
use app\models\AccountsSearch;
use app\models\Transaction;
use app\models\TransactionSearch;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use app\controllers\BaseController;

class AccountsController extends BaseController {

    public function actionIndex() {

        $searchModel = new AccountsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDaterange() {
        $sDate = Yii::$app->request->post('sdate');
        $eDate = Yii::$app->request->post('edate');
        Yii::$app->session->set('sdate', $sDate);
        Yii::$app->session->set('edate', $eDate);
    }

    public function actionVat() {


        $dataProvider = [];
        $dataProvider2 = [];

        if (Yii::$app->request->post('vat')) {
            $post = Yii::$app->request->post('vat');

            if (empty(Yii::$app->session->get('sdate'))) {
                Yii::$app->session->set('sdate', date('Y-01-01'));
                Yii::$app->session->set('edate', date('Y-m-t'));
            }
            $startDate = Yii::$app->session->get('sdate');

            $endDate = Yii::$app->session->get('edate');
            $startDateDisp = Yii::$app->formatter->asDate($startDate, 'php:Y-m-d');
            $endDateDisp = Yii::$app->formatter->asDate($endDate, 'php:Y-m-d');
            $dataRange = $startDateDisp . " - " . $endDateDisp;
            $dataProvider = new ActiveDataProvider([
                'query' => InvoiceMaster::find()
                        ->joinwith('customer')
                        ->joinwith('project')
                        ->where(['between', 'invoice_date', $startDate, $endDate])
            ]);
            $dataProvider2 = new ActiveDataProvider([
                'query' => Transaction::find()
                        ->andwhere(['>=', 'date', $startDate])->andWhere(['<=', 'date', $endDate])
                        ->andwhere(['!=', 'vat', 0])
            ]);
            $vatAmount = InvoiceMaster::find()
                            ->select('sum(vat_amount) as vatTotalAmount')
                            ->where(['between', 'invoice_date', $startDate, $endDate])
                            ->asArray()->all();
            $dataProvider->pagination = ['PageSize' => 50];
            $dataProvider2->pagination = ['PageSize' => 50];
            $searchCntrl = ['startDate' => $post['start_date'], 'endDate' => $post['end_date']];
        } else {
            if (empty(Yii::$app->session->get('sdate'))) {
                Yii::$app->session->set('sdate', date('Y-01-01'));
                Yii::$app->session->set('edate', date('Y-m-t'));
            }
            $startDate = Yii::$app->session->get('sdate');
            $endDate = Yii::$app->session->get('edate');
            $startDateDisp = Yii::$app->formatter->asDate($startDate, 'php:Y-m-d');
            $endDateDisp = Yii::$app->formatter->asDate($endDate, 'php:Y-m-d');
            $dataRange = $startDateDisp . " - " . $endDateDisp;
            $dataProvider = new ActiveDataProvider([
                'query' => InvoiceMaster::find()
                        ->joinwith('customer')
                        ->joinwith('project')
                        ->where(['between', 'invoice_date', $startDate, $endDate])
            ]);
//            $dataProvider = new ActiveDataProvider([
//                'query' => PaInvoiceMaster::find()
//                    ->joinwith('customer')
//                    ->joinwith('project')
//                    ->where(['between', 'invoice_date', $startDate, $endDate])
//            ]);
            $dataProvider2 = new ActiveDataProvider([
                'query' => Transaction::find()
                        ->andwhere(['>=', 'date', $startDate])->andWhere(['<=', 'date', $endDate])
                        ->andwhere(['!=', 'vat', 0])
            ]);
            $vatAmount = InvoiceMaster::find()
                            ->select('sum(vat_amount) as vatTotalAmount')
                            ->where(['between', 'invoice_date', $startDate, $endDate])
                            ->asArray()->all();
            //pr($vatAmount);
            $vatFromPettuCashAmount = Transaction::find()
                            ->select('sum(vat) as vatFromPettuCashTotalAmount')
                            ->where(['between', 'date', $startDate, $endDate])
                            ->asArray()->all();
            // pr($vatFromPettuCashAmount);
            //pr($vatFromPettuCashAmount);
            $dataProvider->pagination = ['PageSize' => 50];
            $dataProvider2->pagination = ['PageSize' => 50];
            $searchCntrl = ['startDate' => $startDate, 'endDate' => $endDate];
        }
        // pr($dataProvider2);        
        echo $this->render('vat', ['dataProvider' => $dataProvider, 'dataProvider2' => $dataProvider2, 'searchCntrl' => $searchCntrl, 'vatAmount' => $vatAmount, 'vatFromPettuCashAmount' => $vatFromPettuCashAmount, 'dataRange' => $dataRange]);
    }

    public function actionCreate() {

        $model = new Accounts();

        if ($model->load(Yii::$app->request->post())) {

            $postData = Yii::$app->request->post();


            $model->date = date('Y-m-d', strtotime($model['date']));

            $getLastRecord = Accounts::find()->orderBy(['id' => SORT_DESC])->asArray()->one();
            $lastbalance = $getLastRecord['amount'];

            if ($model['credit_debit'] == 'Credit') {

                $model->amount = $lastbalance + $model['credit_amount'];
            } else {

                $model->amount = $lastbalance - $model['debit_amount'];
            }

            $model->save();
            return $this->redirect(yii::$app->request->referrer);
        } else {

            return $this->renderAjax('create', ['model' => $model]);
        }
    }

    public function actionUpdate($id) {


        $model = $this->findModel($id);

        $lastBalance = $model->amount;
        // pr($lastBalance);
        $lastCredit = $model->credit_amount;
        $lastDebit = $model->debit_amount;



        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();
            $AccountsData = $request['Accounts'];
            $creditAmount = $AccountsData['credit_amount'];
            $debitAmount = $AccountsData['debit_amount'];

            if ($AccountsData['credit_debit'] == 'Credit' && $lastCredit != $creditAmount) {

                $model->amount = ($lastBalance - $lastCredit) + $creditAmount;
            } else if ($AccountsData['credit_debit'] == 'Debit' && $lastDebit != $debitAmount) {

                $model->amount = ($lastBalance + $lastDebit) - $debitAmount;
            }

            if ($AccountsData['credit_debit'] == 'Debit' && !empty($lastCredit)) {

                $model->amount = ($lastBalance - $lastCredit) - $debitAmount;
                $model->credit_amount = '';
            } else if ($AccountsData['credit_debit'] == 'Credit' && !empty($lastDebit)) {

                $model->amount = ($lastBalance + $lastDebit) + $creditAmount;
                $model->debit_amount = '';
            }

            $model->date = date('Y-m-d', strtotime($model['date']));


            if (!$model->save()) {
                pr($model->getErrors());
            }
            return $this->redirect(yii::$app->request->referrer);
        } else {
            return $this->renderAjax('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionDeletes($id) {
        //pr($id);
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionTransaction() {
        $dateVariable      = strtotime(date('Y-m-d'));//your date variable goes here
        $before_29_days = date('Y-m-d', strtotime('-29 days', $dateVariable));
        $sDate = isset($_REQUEST['sdate'])?$_REQUEST['sdate']:$before_29_days;
        $eDate =  isset($_REQUEST['edate'])?$_REQUEST['edate']:date('Y-m-d');

        $startDate = $sDate;
//        pr1($startDate);
        $endDate = $eDate;

        $startDateDisp = Yii::$app->formatter->asDate($startDate, 'php:M d,Y');
        $endDateDisp = Yii::$app->formatter->asDate($endDate, 'php:M d,Y');
        $dataRange = $startDateDisp . " - " . $endDateDisp;
      //  pr1($dataRange);

        $searchModel = new TransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $startDate, $endDate);
        $dataProvider->pagination = ['PageSize' => 50];

        $previousCertified = InvoiceMaster::find()
            ->select('sum(previous_certified_amount) as previousTotalAmount')
            ->where(['between', 'invoice_date', $startDate, $endDate])
            ->asArray()->all();
        $revenueAmount = InvoiceMaster::find()
                        ->select('sum(total_amount) as revenueTotalAmount')
                        ->where(['between', 'invoice_date', $startDate, $endDate])
                        ->asArray()->all();
       // pr($revenueAmount);
        $creditAmount = \app\models\InvoicePayment::find()
                        ->select('sum(paid_amount) as creditTotalAmount')
                        ->where(['between', 'payment_date', $startDate, $endDate])
                        ->asArray()->all();
        $creditAmount = \app\models\PaInvoicePayment::find()
            ->select('sum(paid_amount) as creditTotalAmount')
            ->where(['between', 'payment_date', $startDate, $endDate])
            ->asArray()->all();
        $expenseAmount = \app\models\Transaction::find()
                        ->select('sum(expense_total_amount) as expenseTotalAmount')
                        ->where(['between', 'date', $startDate, $endDate])
                        ->andwhere(['!=', 'trans_mode', 3])
                        ->asArray()->all();
        $inwardvatAmount = InvoiceMaster::find()
                        ->select('sum(vat_amount) as inwardvatAmount')
                        ->where(['between', 'invoice_date', $startDate, $endDate])
                        ->asArray()->all();
       // pr1($inwardvatAmount);
        $outwardvatAmount = \app\models\Transaction::find()
                        ->select('sum(vat) as outwardvatAmount')
                        ->where(['between', 'date', $startDate, $endDate])
//                        ->andwhere(['!=', 'trans_mode', 3])
                        ->asArray()->all();
       // pr($outwardvatAmount);
        $previousCertifiedAmount = InvoiceMaster::find()
            ->select('sum(previous_certified_amount) as previousCertifiedTotalAmount')
            ->where(['between', 'invoice_date', $startDate, $endDate])
            ->asArray()->all();
//        $previousCertifiedAmount = PaInvoiceMaster::find()
//            ->select('sum(previous_certified_amount) as previousCertifiedTotalAmount')
//            ->where(['between', 'invoice_date', $startDate, $endDate])
//            ->asArray()->all();

        return $this->render('transaction', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'previousCertified' => $previousCertified,
                    'revenueAmount' => $revenueAmount,
            'previousCertifiedAmount' => $previousCertifiedAmount,
                    'creditAmount' => $creditAmount,
                    'expenseAmount' => $expenseAmount,
                    'inwardvatAmount' => $inwardvatAmount,
                    'outwardvatAmount' => $outwardvatAmount,
                    'dataRange' => $dataRange,
        ]);
    }

    public function actionCreatetransaction() {

        $model = new Transaction();

        if ($model->load(Yii::$app->request->post())) {

            $postData = Yii::$app->request->post();
             $firstName =$model->employeeName['first_name'];
             if($model->trans_mode != 1){
                    $name = '-'.ucfirst($firstName);
                    }
                 else if($model->payment_mode == 2){
                    $name = '-'.ucfirst($firstName);
                    }
                    else{ 
                        $name='';
                    }
                    
                $refcode = Yii::$app->params['refcode'];
                if ($model->type == 1 && $model->ref_code == 'PC-01' || $model->type == 1 && $model->ref_code == 'VM-06') {
                        $ref_code = $refcode[$model->ref_code];

                       $model->particulars =!empty($ref_code) ? $ref_code . '/' . $model->categoryMaster['description'].' - ' .$model->description : '-';
                    } 
                      else if ($model->trans_mode ==2  && $model->payment_mode ==3 && $model->type == 1 && $model->ref_code == 'PC-01' || 
                        $model->trans_mode ==2  && $model->payment_mode ==3 && $model->type == 1 && $model->ref_code == 'VM-06') {
                        $ref_code = $refcode[$model->ref_code];

                       $model->particulars =!empty($ref_code) ? $ref_code . '/' . $model->categoryMaster['description'].' - ' .$model->description.'-'.$name : '-';
                    }

                    else if ($model->type == 1) {
                        $ref_code = $refcode[$model->ref_code];

                       $model->particulars =!empty($ref_code) ? $ref_code .'-'.$model->description: '-';
                    } elseif ($model->description) {
                       $model->particulars = !empty($model->description) ? ($model->description)  . $name : '-';
                    } else {
                       $model->particulars = !empty($model->employee_id) ? $name : '-';
                    }

            $model->date = date('Y-m-d', strtotime($model['date']));
            if ($model->type == 3) {
               $lpo_num = $postData['Transaction']['lpo_no'];
              
         $po_amount = \app\models\SubContractor::find()->where(['id' => $lpo_num])->asArray()->one();

        $povatamount = $po_amount['vat_amount'];

             }
        
            if ($model->type == 2) {

//            $products = new Products();
//             $productDatas = $postData['Products'];
                //$projectID = $products->project_id;
                $filenameDoc = '';
                $upload = UploadedFile::getInstance($model, 'document');
                if (!empty($upload)) {

                    $dirname = $postData['Transaction']['project_id'];
                    $Subfolder = "material-procure";
                    $folder = 'uploads/project/' . $dirname . '/';
                    $filename = 'uploads/project/' . $dirname . '/' . $Subfolder . '/';


                    if (file_exists($filename)) {
                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                    } else {
                        if (file_exists($folder)) {

                            mkdir('uploads/project/' . $dirname . '/' . $Subfolder . '/');
                            $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                        } else {
                            mkdir('uploads/project/' . $dirname . '/');
                            mkdir('uploads/project/' . $dirname . '/' . $Subfolder . '/');
                            $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                        }
                    }
                    $upload->saveAs($filenameDoc);
                    $fileName = $upload->baseName . '_' . time() . '.' . $upload->extension;
                }

                $model->document = isset($fileName) ? $fileName : '';
            }
             
            $vatpercentage = $postData['Transaction']['vat_percentage'];
            $amount = $postData['Transaction']['expense_amount'];
         
            $vatcaluate = ($amount * $vatpercentage) / 100;
            if ($model->type == 3) {
              
                $model->vat=!empty($amount)?$vatcaluate:$povatamount;
                
            }else{
                        $model->vat = isset($amount) ? $vatcaluate : 0;
            }
            $model->vat_percentage=isset($vatpercentage)?$vatpercentage:0;
            $model->save();
            return $this->redirect(yii::$app->request->referrer);
        } else {

            return $this->renderAjax('create_transaction', ['model' => $model]);
        }
    }

    public function actionGetproject() {
        $client_id = $_POST['client'];
        $temp = [];
        $project = \app\models\AdhocProject::find()->where(['customer_id' => $client_id])->andwhere(['project_type' => 1])->asArray()->all();
        foreach ($project as $project_name) {
            $newid = $project_name['id'];
            $temp[$newid] = $project_name['project_name'];
        }
        //pr($temp);
        return $this->asJson($temp);
    }

    public function actionGetprojectlist() {
        $type = $_POST['type'];
        $temp = [];
        if ($type == 3) {

            $project = \app\models\AdhocProject::find()->where(['project_type' => 1])->asArray()->all();
            foreach ($project as $project_name) {
                $newid = $project_name['id'];
                $temp[$newid] = $project_name['project_name'];
            }
        }
        //pr($temp);
        return $this->asJson($temp);
    }

    public function actionGetsubcontractor() {
        $project_id = $_POST['project'];
        
        
        $subcontractor=\app\models\SubContractor::find()->joinwith('customer')->where(['project_id' => $project_id])->asArray()->all();
 
         foreach ($subcontractor as $subcontractorList) {
            $list[$subcontractorList['customer']['id']] = ($subcontractorList['customer']['type']==1)?$subcontractorList['customer']['company_name']:$subcontractorList['customer']['coustomer_fname']." ".$subcontractorList['customer']['coustomer_lname'];       
        }
  
       return $this->asJson($list);
      
    }

    public function actionGetlpoamount() {
        $lpo_no = $_POST['lpovalue'];

      

        $po_amount = \app\models\SubContractor::find()->where(['lpo_number' => $lpo_no])->asArray()->one();

        $po_id=  $po_amount ['id'];
        $popartexpense = \app\models\Transaction::find()
                        ->where(['lpo_no' => $po_id])
                        ->groupBy('lpo_no')
                        ->select('sum(expense_total_amount) as totalAmount')
                        ->asArray()->all();
        $partamount =isset($popartexpense[0]['totalAmount'])?$popartexpense[0]['totalAmount']:0; 
       
        $data['totalpoamount']=number_format($po_amount['po_amount'], 2, '.', ',') ;
        $data['partpayment']= isset($partamount)?number_format($partamount, 2, '.', ','):0;

        $data['amount'] =!empty($partamount)?number_format(($po_amount['po_amount']-$partamount), 2, '.', ','): $po_amount['po_amount'];
//        $vatamount = $po_amount['vat_amount'];
      
        return $this->asJson($data);
    }
    

    public function actionGetlpo() {
        $contractor = $_POST['contractor'];
        $lpo = [];
        $subcontractor = \app\models\SubContractor::find()->where(['name' => $contractor])->asArray()->all();

        foreach ($subcontractor as $subcontractor_lpo) {
            $newid = $subcontractor_lpo['id'];
            $lpo[$newid] = $subcontractor_lpo['lpo_number'];
        }

        // pr($lpo);
        return $this->asJson($lpo);
    }

    public function actionGetemployee() {
        $type = $_POST['type'];
        $name = [];
        if ($type == 4) {
            $employee = \app\models\Employee::find()->asArray()->all();

            foreach ($employee as $employee_name) {
                $newid = $employee_name['id'];
                $name[$newid] = $employee_name['first_name'] . ' ' . $employee_name['middle_name'] . $employee_name['last_name'];
            }
        } else {
            $employee_accounts = \app\models\Employee::find()->where(['accounts' => 1])->asArray()->all();
            foreach ($employee_accounts as $employee_name) {
                $newid = $employee_name['id'];
                $name[$newid] = $employee_name['first_name'] . ' ' . $employee_name['middle_name'] . $employee_name['last_name'];
            }
        }


        return $this->asJson($name);
    }

    public function actionCheckbalance() {
        $employee = $_POST['employee'];
        // $expense_amount = $_POST['expense_amount'];
        $credit_amount = Yii::$app->db->createCommand("SELECT sum(pettycash_amount) FROM transaction WHERE employee_id=$employee")->queryScalar();
    
        $expense_amount = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction WHERE employee_id=$employee and trans_mode=2 and payment_mode=3")->queryScalar();
      

        $balance1 = $credit_amount - $expense_amount;
       
        $balance = number_format($balance1, 2, '.', '');


        return $this->asJson($balance);
    }

    public function actionUpdatetransaction($id) {

        $model = Transaction::findOne($id);

         if ($model->load(Yii::$app->request->post())) {

            $postData = Yii::$app->request->post();
            $firstName =$model->employeeName['first_name'];
             if($model->trans_mode != 1){
                    $name = '-'.ucfirst($firstName);
                    }
                 else if($model->payment_mode == 2){
                    $name = '-'.ucfirst($firstName);
                    }
                    else{ 
                        $name='';
                    }
                    
                $refcode = Yii::$app->params['refcode'];
                if ($model->type == 1 && $model->ref_code == 'PC-01' || $model->type == 1 && $model->ref_code == 'VM-06') {
                        $ref_code = $refcode[$model->ref_code];

                       $model->particulars =!empty($ref_code) ? $ref_code . '/' . $model->categoryMaster['description'].' - ' .$model->description : '-';
                    }
                     else if ($model->trans_mode ==2  && $model->payment_mode ==3 && $model->type == 1 && $model->ref_code == 'PC-01' || 
                        $model->trans_mode ==2  && $model->payment_mode ==3 && $model->type == 1 && $model->ref_code == 'VM-06') {
                        $ref_code = $refcode[$model->ref_code];

                       $model->particulars =!empty($ref_code) ? $ref_code . '/' . $model->categoryMaster['description'].' - ' .$model->description.'-'.$name : '-';
                    }

                     else if ($model->type == 1) {
                        $ref_code = $refcode[$model->ref_code];

                       $model->particulars =!empty($ref_code) ? $ref_code .'-'.$model->description: '-';
                    } elseif ($model->description) {
                       $model->particulars = !empty($model->description) ? ($model->description)  . $name : '-';
                    } else {
                       $model->particulars = !empty($model->employee_id) ? $name : '-';
                    }


            $model->date = date('Y-m-d', strtotime($model['date']));
          
              if ($model->type == 3) {
               $lpo_num = $postData['Transaction']['lpo_no'];
              
         $po_amount = \app\models\SubContractor::find()->where(['id' => $lpo_num])->asArray()->one();

        $povatamount = $po_amount['vat_amount'];

             }
            if ($model->type == 2) {

//            $products = new Products();
//             $productDatas = $postData['Products'];
                //$projectID = $products->project_id;
                $filenameDoc = '';
                $upload = UploadedFile::getInstance($model, 'document');
                if (!empty($upload)) {

                    $dirname = $postData['Transaction']['project_id'];
                    $Subfolder = "material-procure";
                    $folder = 'uploads/project/' . $dirname . '/';
                    $filename = 'uploads/project/' . $dirname . '/' . $Subfolder . '/';


                    if (file_exists($filename)) {
                        $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                    } else {
                        if (file_exists($folder)) {

                            mkdir('uploads/project/' . $dirname . '/' . $Subfolder . '/');
                            $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                        } else {
                            mkdir('uploads/project/' . $dirname . '/');
                            mkdir('uploads/project/' . $dirname . '/' . $Subfolder . '/');
                            $filenameDoc = $filename . $upload->baseName . '_' . time() . '.' . $upload->extension;
                        }
                    }
                    $upload->saveAs($filenameDoc);
                    $fileName = $upload->baseName . '_' . time() . '.' . $upload->extension;
                }

                $model->document = isset($fileName) ? $fileName : '';
            }
             
            $vatpercentage = $postData['Transaction']['vat_percentage'];
            $amount = $postData['Transaction']['expense_amount'];
            $vatcaluate = ($amount * $vatpercentage) / 100;
            if ($model->type == 3) {
              
                $model->vat=!empty($amount)?$vatcaluate:$povatamount;
                
            }else{
                        $model->vat = isset($amount) ? $vatcaluate : 0;
            }

            $model->vat_percentage=isset($vatpercentage)?$vatpercentage:0;
            $model->save();
            return $this->redirect(yii::$app->request->referrer);
        } else {

            return $this->renderAjax('update_transaction', ['model' => $model]);
        }
    }
          

    public function actionDeletetransaction($id) {
        //pr($id);
        Transaction::findOne($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    protected function findModel($id) {
        if (($model = Accounts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
