<?php

namespace app\modules\accounts;

class accounts extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\accounts\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
