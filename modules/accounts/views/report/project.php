<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use app\models\Invoice;
use yii\helpers\ArrayHelper;
$logourl = Yii::$app->params['domain_url'];
$this->params['breadcrumbs'][] = 'Transation Reports';
$this->title = 'Reports';
$paymentMode =Yii::$app->params['paymentMode'];
?>


<div class="row">
    <div class="col-md-9 text-left add-label" style="font-size: 32px">

        <i class="fa fa-money" aria-hidden="true"></i> Transaction Reports
    </div>

    <div class="clearfix"></div>

</div>


<?php
if ($dataProvider) {


    $gridColumns = [
        ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false, 'hiddenFromExport' => false],
        [
            'attribute' => 'project_name',
            'header' => '<div style="width:120px;">Project</div>',
            'value' => function($model, $key, $index) {
                return Html::a($model->project_name, ['project/detailview', 'id' => $model->id], [ 'data-pjax'=>"0"]);

            },
            'vAlign' => 'middle',
            'format' => 'raw',
            //'width' => '150px',
            'noWrap' => true
        ],

        [

            'header' => '<div style="width:120px;">Total Revenue</div>',
            'value' => function($model, $key, $index) {


                $Revenue = Yii::$app->db->createCommand("SELECT sum(total_amount) FROM invoice_master  WHERE (project_id= $model->id  )")->queryScalar();



               return !empty($Revenue) ? $Revenue:'0';     },
            'vAlign' => 'middle',
            'hAlign'=>'right',
            'format' => ['decimal', 2],
            //'width' => '150px',
            'noWrap' => true
        ],
        [

            'header' => '<div style="width:120px;">Total Credited</div>',
            'value' => function($model, $key, $index) {
                $Invoice = \app\models\InvoiceMaster::find()->where(['project_id'=>$model->id])->asArray()->all();
                $Invoiceid = ArrayHelper::map($Invoice, 'id', 'id');
                $temp = [];
                foreach ($Invoiceid as $InvoiceID) {

//                    $temp[] = \app\models\InvoicePayment::find()->where(['invoice_id'=>$InvoiceID])->asArray()->all();
                    $temp[] = Yii::$app->db->createCommand("SELECT sum(paid_amount) FROM invoice_payment  WHERE (invoice_id= $InvoiceID  )")->queryScalar();

                }
                if(!empty($temp)) {
                    $Credited = array_sum($temp);


                } else {
                    $Credited =0;
                }


                    return !empty($Credited) ? $Credited:'0';     },
            'vAlign' => 'middle',
            'hAlign'=>'right',
            'format' => ['decimal', 2],
            //'width' => '150px',
            'noWrap' => true
        ],

        [

            'header' => '<div style="width:120px;">Outsatndng</div>',
            'value' => function($model, $key, $index) {

                $Revenue = Yii::$app->db->createCommand("SELECT sum(total_amount) FROM invoice_master  WHERE (project_id= $model->id  )")->queryScalar();

                $Invoice = \app\models\InvoiceMaster::find()->where(['project_id'=>$model->id])->asArray()->all();
                $Invoiceid = ArrayHelper::map($Invoice, 'id', 'id');
                foreach ($Invoiceid as $InvoiceID) {

//                    $temp[] = \app\models\InvoicePayment::find()->where(['invoice_id'=>$InvoiceID])->asArray()->all();
                    $temp[] = Yii::$app->db->createCommand("SELECT sum(paid_amount) FROM invoice_payment  WHERE (invoice_id= $InvoiceID  )")->queryScalar();

                }
                if(!empty($temp)) {
                    $Credited = array_sum($temp);


                } else {
                    $Credited =0;
                }

               $outsatndng = $Revenue - $Credited;

                return $outsatndng; },
            'vAlign' => 'middle',
            'hAlign'=>'right',
            'format' => ['decimal', 2],
            //'width' => '150px',
            'noWrap' => true
        ],

        [

            'header' => '<div style="width:120px;">Material Cost</div>',
            'value' => function($model, $key, $index) {


                $Material = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction  WHERE (project_id= $model->id AND type=2 )")->queryScalar();


                return !empty($Material) ? $Material:'0';     },
            'vAlign' => 'middle',
            'hAlign'=>'right',
            'format' => ['decimal', 2],
            //'width' => '150px',
            'noWrap' => true
        ],

        [

            'header' => '<div style="width:120px;">Sub-Contractor</div>',
            'value' => function($model, $key, $index) {


                $Subcontractor = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction  WHERE (project_id= $model->id AND type=3 )")->queryScalar();

                      return !empty($Subcontractor) ? $Subcontractor:'0';     },
            'vAlign' => 'middle',
            'hAlign'=>'right',
            'format' => ['decimal', 2],
            //'width' => '150px',
            'noWrap' => true
        ],

        [

            'header' => '<div style="width:120px;">Service</div>',
            'value' => function($model, $key, $index) {


                $Service = Yii::$app->db->createCommand("SELECT sum(totalcost_working) FROM service  WHERE (project_id= $model->id)")->queryScalar();


                return !empty($Service) ? $Service:'0';     },
            'vAlign' => 'middle',
            'hAlign'=>'right',
            'format' => ['decimal', 2],
            //'width' => '150px',
            'noWrap' => true
        ],

        [

            'header' => '<div style="width:120px;">Profit</div>',
            'value' => function($model, $key, $index) {

                $Revenue = Yii::$app->db->createCommand("SELECT sum(total_amount) FROM invoice_master  WHERE (project_id= $model->id  )")->queryScalar();

                $Material = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction  WHERE (project_id= $model->id AND type=2 )")->queryScalar();
                $Subcontractor = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction  WHERE (project_id= $model->id AND type=3 )")->queryScalar();
                $Service = Yii::$app->db->createCommand("SELECT sum(totalcost_working) FROM service  WHERE (project_id= $model->id)")->queryScalar();

                $Expenses = $Material + $Subcontractor + $Service;
                $Profit = $Revenue - $Expenses;
                return $Profit; },
            'vAlign' => 'middle',
            'hAlign'=>'right',
            'format' => ['decimal', 2],
            //'width' => '150px',
            'noWrap' => true
        ],



    ];


    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => $gridColumns, // check the configuration for grid columns by clicking button above

        'persistResize' => false,
        //'resizeableColumns'=>true,
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => '<img src="'.$logourl.'/lib/img/pdf_logo.png" >', 'options' => ['colspan' => 10, 'class' => ' hide text-center','style' => 'font-size:18px']],
                ],],
            [
                'columns' => [
                    ['content' => 'Transation Reports', 'options' => ['colspan' => 10, 'class' => 'hide text-center','style' => 'font-size:18px']],
                ],],

            [ 'columns' => [
                ['content' => '', 'options' => ['colspan' => 5, 'class' => '', 'style' => 'font-size:18px']],
                ['content' => ' Expenses', 'options' => ['colspan' => 3, 'class' => 'text-center', 'style' => 'font-size:18px']],

                ['content' => '', 'options' => ['colspan' => 2, 'class' => '', 'style' => 'font-size:18px']],

            ],],
        ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => false, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' =>  [
            '{export}'
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '',

        ],
        'exportConfig' => [GridView::EXCEL => [
            'filename' => 'Transation Reports',

        ],
            GridView::PDF => [
                'filename' => 'Transation Reports',
                'showHeader' => true,
                'showPageSummary' => false,
                'showFooter' => true,
                'showCaption' => true,


                'config' => [
                    //'cssInline' =>'table{direction:0}' ,
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                    //'contentBefore'=>'<div style="font-size:30px; text-align:center; text-decoration:underline;">List Of Clients</div>'
                ],
            ]


        ],
    ]);


}
?>
