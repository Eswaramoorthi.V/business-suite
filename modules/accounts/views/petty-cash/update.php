<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use app\models\Employee;
use app\models\Project;
use app\models\Products;
use app\models\Customer;

$VATRate =Yii::$app->params['VATRate'];
$type = ['Credit' => 'Credit', 'Expense' => 'Expense'];
$paymentMode = Yii::$app->params['paymentMode'];
$model->date=date('d-m-Y',strtotime($model->date));

$employeeDetails = Employee::find()->asArray()->all();

$employeelist = [];
        foreach ($employeeDetails as $employeeData) {
            
            $firstname = ucfirst($employeeData['first_name']);
            $lastname = ucfirst($employeeData['last_name']);
           
            $employeelist[$employeeData['id']] = $firstname.' '.$lastname;
                    
        }
$projectDetails = Project::find()->andWhere(['project_type'=>'1'])->asArray()->all();

$projectlist = [];
        foreach ($projectDetails as $projectData) {
            
            $projectname = ucfirst($projectData['project_name']); 
            
            $projectlist[$projectData['id']] = $projectname;        
        }
$suppliersDetails = Customer::find()->andWhere(['customer_type'=>'3'])->asArray()->all();

$supplierslist = [];
        foreach ($suppliersDetails as $suppliersData) {
            
            $suppliersname = ucfirst($suppliersData['coustomer_fname']. ' ' .$suppliersData['coustomer_lname']); 
            
            $supplierslist[$suppliersData['id']] = $suppliersname;        
        }         
?>

<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'enctype' => 'multipart/form-data'
            ],
        ])
?>
<div id='msg' class='msg'></div>
<div id="pettycash-form">
    <div class="row">
<legend class="text-info"><small>Cash Transaction</small></legend>
  <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
  
   <div class="col-md-4">
  <?=
            $form->field($model, 'employee_id', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->dropDownList($employeelist,[
                'prompt' => 'Select Employee',
                'class'=>'select from control',
            ])->label('Employee')
            ?> 
  </div> 
   <div class="col-md-4">
  <?=
            $form->field($model, 'payment_mode', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->dropDownList($paymentMode,[
                'class'=>'select from control',
            ])->label('Payment Mode')
            ?> 
  </div>
  <div class="col-md-4">
           <?=
            $form->field($model, 'date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput()->label('Date')
            ?> 
        </div>
     
  <div class="col-md-4">
  <?=
            $form->field($model, 'type', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->dropDownList(['0' => 'Other','1' => 'Material Cost',],[ 
                'class'=>'select from control',
            ])->label('Type')
            ?> 
  </div> 
  
  <?php //pr($products); ?>
    <div class="ForProducts">
<div class="col-md-4">
  <?=
            $form->field($model, 'project_id', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->dropDownList($projectlist,[
                'prompt' => 'Select project',
                'class'=>'select from control',
            ])->label('Projects')
            ?> 
  </div>
        <div class="col-md-4">
  <?=
            $form->field($model, 'supplier_id', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->dropDownList($supplierslist,[
                'prompt' => 'Select Supplier',
                'class'=>'select from control',
            ])->label('Supplier')
            ?> 
  </div>   
<div class="col-md-4">
        <?=
        $form->field($model, 'item_number', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textInput()->label('Invoice Number')
        ?>
    </div>
<div class="col-md-4">
        <?=
        $form->field($model, 'name', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textInput()->label('Material Name')
        ?>
    </div>  </div>   
   <div class="col-md-4">
        <?=
        $form->field($model, 'description', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textInput([])->label('Description')
        ?>
    </div>
   
    <div class="col-md-4">
           <?=
            $form->field($model, 'credit_expense', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->dropDownList($type,[
               // 'prompt' => 'Select',
                'class'=>'select from control',
            ])->label('Credit/Expense')
            ?> 
       </div>
  <div id ="credit">
       <div class="col-md-4">
           <?=
            $form->field($model, 'credit_amount',[
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput([])->label('Credit Amount')
            ?> 
        </div>
  </div>
    <div class="ForProducts">
    <div class="col-md-4">
        <?=
        $form->field($model, 'amount', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textInput(['onchange' => 'calculate()'])->label('Amount')
        ?>
    </div>     
    <div class="col-md-4">
        <?=
        $form->field($model, 'vat', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
         ])->textInput(['readonly' => true, 'value' => $VATRate,])->label('Vat %')
        ?>
    </div>  </div> 
<div id ="expense">
        <div class="col-md-4">
           <?=
            $form->field($model, 'expense_amount', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput([])->label('Expense Amount')
            ?> 
        </div>
  
   </div>
   <div class="ForProducts">
    <div class="col-md-6">
           <?php $documentpath=\Yii::$app->params['domain_url'].'web/uploads/project/'.$model->project_id.'/material-procure/'.$model->document;?>
    <?php if(is_null($model->document) || empty($model->document)): ?>
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9'>
                {input}\n
                {hint}\n
                {error}
          </div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php else: ?>
  
 
        <?= $form->field($model, 'document', [
          'template' => "{label}\n
          <div class='col-md-9 col-xs-9'>
                {input}\n
                <div class='file-preview'>
                  <strong> Current File </strong> : 
                    <div class='clearfix'></div>
                     <a base href='$documentpath'target='_blank'> $model->document</a>
                
                {hint}\n
                {error}
          </div></div>",
          'labelOptions' => [ 'class' => 'col-md-12 control-label' ],
          'inputOptions' => [ 'class' => 'file', 'data-show-upload' => "false",'data-show-preview'=>"false" ]
        ])->fileInput() ?>
    <?php endif; ?>
        </div>  
  </div>
  <div>&nbsp;</div>
  <div>&nbsp;</div>
  <div>&nbsp;</div>
  <div>&nbsp;</div>
  
        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['id'=>'submit','class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
   
    <?php
    ActiveForm::end();
    ?>
</div>
 </div>
 

  
<script> 
$("form#active-form").submit(function(e) {
    e.preventDefault();
   var formData = new FormData(this);
        $("#pettycash-employee_id").removeClass("error-highlight");
     $("#pettycash-description").removeClass("error-highlight");
     $("#pettycash-credit_amount").removeClass("error-highlight");
     $("#pettycash-expense_amount").removeClass("error-highlight");

   var formData = new FormData(this);
       
        if($('#pettycash-employee_id').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Choose Employee</p>");
            $("#pettycash-employee_id").addClass("error-highlight");
            return false;
       }
       else if($('#pettycash-description').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Description cannot be Blank</p>");
            $("#pettycash-description").addClass("error-highlight");
            return false;
       }
       else if(($('#pettycash-credit_amount').val()==='') && ($('#pettycash-credit_expense').val()=== 'Credit')) {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Enter Credit Amount</p>");
            $("#pettycash-credit_amount").addClass("error-highlight");
            return false;
       }
      else if(($('#pettycash-expense_amount').val()==='') && ($('#pettycash-credit_expense').val()==='Expense')) {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Enter Expense Amount</p>");
            $("#pettycash-expense_amount").addClass("error-highlight");
            return false;
       }
       else {
           $("#error-message").addClass("hide");
         
        }
    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('Cash Transaction Added');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();

            },
        cache: false,
        contentType: false,
        processData: false
    });
     
});
</script>


 
 <?= $this->registerJs("
    
        $('#pettycash-date').datetimepicker({
            format: 'DD-MM-YYYY'
        });
 ", View::POS_READY); ?>

<script> 
   $(document).ready(function() {
       calculate = function() {
   
    var amount = Number($("#pettycash-amount").val()).toFixed(2);
    $("#pettycash-amount").val(amount);
     var vat = Number($("#pettycash-vat").val()).toFixed(2);
    var vatamount = amount*(vat)/100;
    var totamount = parseFloat(amount)+parseFloat(vatamount);
    var tot = Number(totamount).toFixed(2);
    $("#pettycash-expense_amount").val(tot);
     }
    });
     $(document).ready(function() {
         var type = $('#pettycash-credit_expense').val();
            if (type == 'Credit') {
                $('#credit').show();
                $('#expense').hide();
              } 
              else {
                $('#credit').hide();
                $('#expense').show();
             }
    });
   
</script>


<script> 
        $('#pettycash-credit_expense').on('change',function(){
           var type = $('#pettycash-credit_expense').val();
             if (type == 'Credit') {
                $('#credit').show();
                $('#expense').hide();
              } 
              else if(type == 'Expense') {
                $('#credit').hide();
                $('#expense').show();
            }
        });
       
   
</script>

<script>
    
     $(document).ready(function() {
        var pettycashtype = $('#pettycash-type').val();
        
        if (pettycashtype == 0) {
                $('.ForOthers').show();
                $('.ForProducts').hide();
                $('#credit').show();
                $('#expense').hide();
                $("#pettycash-credit_expense").val('Credit');
              } else{
                $('.ForOthers').hide();
                $('.ForProducts').show();
                $('#credit').hide();
                $('#expense').show();
                $("#pettycash-credit_expense").val('Expense');
             }
        
       $('#pettycash-type').on('change',function(){
           var pettycashtype = $('#pettycash-type').val();
             if (pettycashtype == 1) {
               
                $('.ForProducts').show();
               $("#pettycash-credit_expense").val('Expense');
               $('#pettycash-credit_expense').attr("disabled", true);
               $('#credit').hide();
                $('#expense').show();
              } 
              else if(pettycashtype == 0) {
                $('.ForProducts').hide();
                $("#pettycash-credit_expense").val('Credit');
                $('#pettycash-credit_expense').attr("disabled", false);
                $('#credit').show();
                $('#expense').hide();
            }
        });
         });
   </script>