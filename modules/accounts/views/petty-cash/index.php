<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;

//$this->title = 'Petty Cash';
$this->params['breadcrumbs'][] = 'Petty Cash';
$paymentMode =Yii::$app->params['paymentMode'];
//$this->params['title_icon'] = 'fa-money';


$credit_amount1 = Yii::$app->db->createCommand("SELECT sum(pettycash_amount) FROM transaction WHERE employee_id!='' ")->queryScalar();
$credit_amount=number_format((float)($credit_amount1), 2, '.', ',');
$expense_amount1 = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction WHERE (employee_id!='' and trans_mode=2 and payment_mode=3)  ")->queryScalar();
$expense_amount=number_format((float)($expense_amount1), 2, '.', ',');
$balance1 =$credit_amount1-$expense_amount1;
$balance=number_format((float)($balance1), 2, '.', ',');

?>
<div class="row">
    <div class="col-md-9 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-money" aria-hidden="true"></i> Petty Cash
    </div>
   
    <div class="clearfix"></div> 
    
</div>



<div class="row">
<div class="col-md-3">
    <div class="widget widget-info widget-padding-sm">
        <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>
                            <div class="widget-title">Total Credit</div> <br><br>
                            <div class="widget-subtitle"><?php echo!empty($credit_amount) ? $credit_amount : '0'; ?></div>
                            <div class="widget-int"></div>
                        </div></div></div></div>
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="widget widget-info widget-padding-sm">
        <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>
                            <div class="widget-title">Total Expense</div> <br><br>
                            <div class="widget-subtitle"><?php echo!empty($expense_amount) ? $expense_amount : '0'; ?></div>
                            <div class="widget-int"></div>
                        </div></div></div></div>
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="widget widget-info widget-padding-sm">
        <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>
                            <div class="widget-title">Balance</div> <br><br>
                            <div class="widget-subtitle"><?php echo!empty($balance) ? $balance : '0.00'; ?></div>
                            <div class="widget-int"></div>
                        </div></div></div></div>
        </div>
    </div>
</div>
</div>






<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
           
            [
                'attribute' => '',
                'header' => '<div style="width:120px;">Employee</div>',
                 'value' => function($model, $key, $index) {
                    $EmployeeData = app\models\Employee::find()->where(['id'=>$model->employee_id])->asArray()->one();
                    $firstName = $EmployeeData['first_name'];
                    $lastName = $EmployeeData['last_name'];
                    $name = $firstName.' '.$lastName;
                   return Html::a($name, ['balance-history', 'id' => $model->employee_id], [ 'data-pjax'=>"0"]);

                },
                'format' => 'raw',
            ],
            
            

           
            [
                'attribute' => '',
                'header' => '<div style="width:120px;">Cash Balance</div>',
                'format' => ['decimal', 2],
                //'value' => 'cash_balance',
                'value' => function($model, $key, $index) {
                   $credit_amount = Yii::$app->db->createCommand("SELECT sum(pettycash_amount) FROM transaction WHERE employee_id=$model->employee_id ")->queryScalar();
                  
                    $expense_amount = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction WHERE (employee_id=$model->employee_id and trans_mode=2 and payment_mode=3) ")->queryScalar();
                   
                    //$credit_amount = $credits->queryScalar();
                    //$expense_amount = $expense->queryScalar();
                    $balance =$credit_amount-$expense_amount;
                    return $balance;
                },
            ],

           
        
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => 'Transaction History', 'options' => ['colspan' => 7, 'class' => 'hide', 'style' => 'font-size:18px']],
                ],],
            [
                'columns' => [
                    ['content' => '  &nbsp; <span style="font-weight:normal"> </span>', 'options' => ['colspan' => 1, 'class' => 'hide',]],
                    ['content' => 'Total Credit : <span style="font-weight:normal">' . $credit_amount . '</span>', 'options' => ['colspan' => 2, 'class' => 'hide',]],
                    ['content' => 'Total Expense : <span style="font-weight:normal">' . $expense_amount . '</span>', 'options' => ['colspan' => 2, 'class' => 'hide',]],
                    ['content' => 'Total Balance : <span style="font-weight:normal">' . $balance . '</span>', 'options' => ['colspan' => 2, 'class' => 'hide',]],
                ],],
            
        ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => false, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '',
        ],
        'persistResize' => false,
        'exportConfig' => [GridView::EXCEL => [
                'filename' => 'Transaction History',
            ],
            GridView::PDF => [
                'filename' => 'Transaction History',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'config' => [
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                ],
            ]]
    ]);
}
?>                                   


<?= $this->registerJs("
   // alert('hhjh');
    $('#reportrange').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM.DD.YYYY',
        separator: ' to ',
        startDate: moment().subtract('days', 29),
        endDate: moment()
    }, function (start, end) {
        console.log('inside');
        $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        var sdate = start.format('YYYY-MM-DD');
        var edate = end.format('YYYY-MM-DD');
        var id = $('#reportrange').attr('data-id');
        var url = $('#reportrange').attr('data-url');
         var daterangeurl = $('#reportrange').attr('data-daterangeurl');


                        $.ajax({
                        type : 'POST',
                        url: daterangeurl,
                        data: {'sdate':sdate,'edate':edate},
                        success  : function() {
                        //   console.log(response);
                        //$('.request-data').html(response);
                        location.href = url;
                       
                        },
                        error : function(response){
                        console.log(response);
                        }
                        });
                        
                        
                    });


                    $('#reportrange span').html($('#selectedDRange').val());
", View::POS_READY); ?>

