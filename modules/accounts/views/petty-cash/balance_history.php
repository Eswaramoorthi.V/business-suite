<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\Modal;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;

$id = $_GET['id'];
//pr($id);
$pettyCash = app\models\PettyCash::find()->where(['id' => $id])->asArray()->one();
$employeeId = $pettyCash['employee_id'];

$employeeData = app\models\Employee::find()->where(['id' => $id])->asArray()->one();
$employeeName = $employeeData['first_name'] . ' ' . $employeeData['last_name'];


$this->params['breadcrumbs'][] = ['label' => 'Petty Cash', 'url' => ['petty-cash/index']];
$this->params['breadcrumbs'][] = $employeeName;
?>
<?php

$credit_amount_date = 0;
$expense_amount_date = 0;
//$balance = 0;
if (!empty($dataProvider->getModels())) {
    foreach ($dataProvider->getModels() as $val) {
        $credit_amount_date += $val->pettycash_amount;
        $expense_amount_date += $val->expense_total_amount;
        //$balance = $credit_amount - $expense_amount;
    }
}
$credit_amount = Yii::$app->db->createCommand("SELECT sum(pettycash_amount) FROM transaction WHERE employee_id=$id ")->queryScalar();

$salary_advance_debit=number_format($credit_amount,2, '.', ',');

$expense_amount = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction WHERE employee_id=$id and trans_mode=2 and payment_mode=3 ")->queryScalar();

$salary_advance_credit=number_format($expense_amount,2, '.', ',');

$balance1= $credit_amount - $expense_amount;
$balance=number_format($balance1,2, '.', ',');
//echo $sum;
?>
<div class="content-wrapper">
    <section class="col-md-9">
        <h1>
            <i class="fa fa-eye" aria-hidden="true"></i> Petty Cash History | <small> <?php echo $employeeName; ?></small> </h1>
        <ul class="breadcrumb">

        </ul>    </section>
    <div class="col-md-3">
    <h3 class="text-center">Date Range</h3>
    <div id="reportrange" data-daterangeurl="<?php echo Url::toRoute("petty-cash/daterange"); ?>" data-url="<?php echo Url::toRoute("balance-history"); ?>" data-id="<?php echo $id; ?>"  class="dtrange  col-md-offset-1 col-md-10" >
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span> </span> <b class="caret"></b>
    </div>
</div>
    <div class="clearfix"></div> 
</div>
<?php
Modal::begin([
    'header' => '<h4>Update Petty Cash</h4>',
    'id' => 'update',
    'size' => 'modal-lg',
]);

echo "<div id ='modalupdate'></div>";
Modal::end();
?>
<div class="row">
<div class="col-md-3">
    <div class="widget widget-info widget-padding-sm">
        <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                            <div class="widget-title">Total Credit</div> <br><br>                                                                       
                            <div class="widget-subtitle"><?php echo!empty($salary_advance_debit) ? $salary_advance_debit : '0'; ?></div>
                            <div class="widget-int"></div>
                        </div></div></div></div>
        </div>                            
    </div>         
</div>
<div class="col-md-3">
    <div class="widget widget-info widget-padding-sm">
        <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                            <div class="widget-title">Total Expense</div> <br><br>                                                                       
                            <div class="widget-subtitle"><?php echo!empty($salary_advance_credit) ? $salary_advance_credit : '0'; ?></div>
                            <div class="widget-int"></div>
                        </div></div></div></div>
        </div>                            
    </div>         
</div>
<div class="col-md-3">
    <div class="widget widget-info widget-padding-sm">
        <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
            <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                            <div class="widget-title">Balance</div> <br><br>                                                                       
                            <div class="widget-subtitle"><?php echo!empty($balance) ? $balance : '0.00'; ?></div>
                            <div class="widget-int"></div>
                        </div></div></div></div>
        </div>                            
    </div>         
</div>
</div>



<input type="hidden" value="<?php echo $dataRange; ?>" id="selectedDRange">

<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'responsiveWrap' => false,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'date',
                'header' => '<div style="width:2px;">Date</div>',
                'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->date, 'php:d-m-Y');
           },
                //'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],

           [
                'attribute' => '',
                'vAlign' => 'middle',
                'header' => '<div style="width:200px;">Particulars</div>',
                 'value' => function($model) {
                $refcode = Yii::$app->params['refcode'];
                           if ($model->type==1){
                           $ref_code=$refcode[$model->ref_code];
                       
                 return !empty($ref_code) ?$ref_code:'-';
                 
                     }else{
                 return !empty($model->description)?($model->description):'-'; 
                 
                 }},


            ],
            [ 'attribute' => 'type', 
                'vAlign' => 'middle',
                'width' => '180px',
                 'header' => '<div style="width:180px;">Type</div>',
                'value' => function($model, $key, $index) { 
                 $mode= Yii::$app->params['type'];
              return !empty($mode[$model->type]) ? $mode[$model->type]: '';  },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Yii::$app->params['type'], 
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Select'],
   ],
            [
                'attribute' => '',
                'header' => '<div style="width:100px;">Credit</div>',
                'format' => ['decimal', 2],
                'value' => function ($model, $key, $index) {
            
                     if ($model->trans_mode==1 and $model->payment_mode==2 ){
                           $bank_withdraw=$model->pettycash_amount;
                            return !empty($bank_withdraw) ?$bank_withdraw:'';
                       }
                       else if ($model->trans_mode==2 and $model->payment_mode==1 ){
                           $cash_credit=$model->pettycash_amount;
                            return !empty($cash_credit) ?$cash_credit:'';
                       }
                       else{
                    return '0';
                      }
                         
                },
                 // 'footer'=>isset($credit_amount_date)?$credit_amount_date:'',
            ],
            [
                'attribute' => 'expense_total_amount',
                'header' => '<div style="width:100px;">Expense</div>',
                'format' => ['decimal', 2],
                'value' => function ($model, $key, $index) {
                   
                 if ($model->trans_mode==4 and $model->payment_mode==1){
                          return 0;
                     
                 }
                 else{
                     return   !empty($model->expense_total_amount) ? $model->expense_total_amount : ''; 
                 }
                },
             // 'footer'=>isset($expense_amount_date)?$expense_amount_date:'',
            ],

           
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => 'Petty Cash History', 'options' => ['colspan' => 6, 'class' => 'hide', 'style' => 'font-size:18px']],
                ],],
            [
                'columns' => [
                    ['content' => 'Name : <span style="font-weight:normal">' . $employeeName . '</span>', 'options' => ['colspan' => 5, 'class' => 'hide',]],
                    ['content' => 'Balance : <span style="font-weight:normal">' . $balance . '</span>', 'options' => ['colspan' => 1, 'class' => 'hide',]],
                ],],
        ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => false, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showFooter' => false,
        //'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '',
        ],
        'persistResize' => false,
        'exportConfig' => [GridView::EXCEL => [
                'filename' => $employeeName . ' ' . 'Petty Cash History',
            ],
            GridView::PDF => [
                'filename' => $employeeName . ' ' . 'Petty Cash History',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'config' => [
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                ],
            ]]
    ]);
}
?>                                   

<?=
$this->registerJs(
        "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.update').click(function(e){
       e.preventDefault();      
       $('#update').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
?>


<?= $this->registerJs("
   // alert('hhjh');
    $('#reportrange').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM.DD.YYYY',
        separator: ' to ',
        startDate: moment().subtract('days', 29),
        endDate: moment()
    }, function (start, end) {
        console.log('inside');
        $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        var sdate = start.format('YYYY-MM-DD');
        var edate = end.format('YYYY-MM-DD');
        var id = $('#reportrange').attr('data-id');
        var url = $('#reportrange').attr('data-url');
         var daterangeurl = $('#reportrange').attr('data-daterangeurl');


                        $.ajax({
                        type : 'POST',
                        url: daterangeurl,
                        data: {'sdate':sdate,'edate':edate},
                        success  : function() {
                        //   console.log(response);
                        //$('.request-data').html(response);
                        location.href = url + '?id=' + id;
                       
                        },
                        error : function(response){
                        console.log(response);
                        }
                        });
                        
                        
                    });


                    $('#reportrange span').html($('#selectedDRange').val());
", View::POS_READY); ?>