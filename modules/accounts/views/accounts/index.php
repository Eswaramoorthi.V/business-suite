<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;

//$this->title = 'Bank';
$this->params['breadcrumbs'][] = 'Bank';
//$this->params['title_icon'] = 'fa-credit-card';
?>
<div class="row">
     <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-bank" aria-hidden="true"></i> Bank
    </div>
    <div class="col-md-12 text-right add-label">
        <?= Html::button('Add Transaction', ['value' => Url::to('create?r=accounts/create'), 'class' => 'btn btn-success', 'id' => 'modalButton']) ?>
    </div>
</div>
<?php
Modal::begin([
    'header' => '<h4>Add Transaction</h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Transaction</h4>',
    'id' => 'update',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalupdate'><div class='loader'></div></div>";
Modal::end();
?>


<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
           [
                'attribute' => 'date',
                'header' => '<div style="width:180px;">Date</div>',
                'value' => 'date',
                'format' => 'date',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
                'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],
            [
                'attribute' => 'description',
                'header' => '<div style="width:120px;">Description</div>',
                'value' => function($model) {
                    return !empty($model->description) ? $model->description:'-';            
                }
            ],
             [
                'attribute' => 'reference_no',
                'header' => '<div style="width:120px;">Reference No</div>',
                 'value' => function($model) {
                    return !empty($model->reference_no) ? $model->reference_no:'-';            
                }
            ],
            [
                'attribute' => 'credit_amount',
                'header' => '<div style="width:120px;">Credit Amount</div>',
                'format' => ['decimal', 2],
                'value' => function($model) {
                    return !empty($model->credit_amount) ? $model->credit_amount:'';            
                }
            ],
            [
                'attribute' => 'debit_amount',
                'header' => '<div style="width:120px;">Debit Amount</div>',
                'format' => ['decimal', 2],
                'value' => function($model) {
                    return !empty($model->debit_amount) ? $model->debit_amount:'';            
                }
            ],
          
            [
                'attribute' => 'amount',
                'header' => '<div style="width:120px;">Balance</div>',
                'format' => ['decimal', 2],
                 'value' => function($model) {
                    return !empty($model->amount) ? $model->amount:'';            
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:80px;text-align:center;','class'=>'skip-export'],
                'headerOptions' =>['class'=>'skip-export'],
                'header' => "Actions",
                'template' => ' {update}{deletes}',
                'buttons' => [
                  
                    'update' => function($url) {
                        return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'update', 'title' => 'Edit']
                        );
                    },
                    'deletes' => function ($url) {
                        return Html::a(
                                        '<span class="glyphicon glyphicon-trash icons"></span>', $url, [
                                    'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                                        ]
                        );
                    },
                ],
            ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => 'Transaction History', 'options' => ['colspan' => 7, 'class' => 'hide', 'style' => 'font-size:18px']],
                ],],
            
        ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => false, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '',
        ],
        'persistResize' => false,
        'exportConfig' => [GridView::EXCEL => [
                'filename' => 'Transaction History',
            ],
            GridView::PDF => [
                'filename' => 'Transaction History',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'config' => [
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                ],
            ]]
    ]);
}
?>                                   
<?= $this->registerJs("
    $(function(){
$('#modalButton').click(function() {
    $('#modal').modal('show')
    .find('#modalContent')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>

<?=
$this->registerJs(
        "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.update').click(function(e){
       e.preventDefault();      
       $('#update').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
?>
