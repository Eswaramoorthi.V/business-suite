<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use app\models\Employee;
use app\models\Project;
use app\models\Customer;
use yii\helpers\ArrayHelper;
use app\models\CategoryMaster;

$VATRate = Yii::$app->params['VATRate'];
$vehicle = ArrayHelper::map(CategoryMaster::getVehicle(), 'id', 'description');
$transMode = Yii::$app->params['transMode'];
$paymentMode = Yii::$app->params['paymentModeOnTrans'];
$refcode = Yii::$app->params['refcode'];
$type = Yii::$app->params['type'];
$employeeDetails = Employee::find()->where(['accounts'=>1])->asArray()->all();
$customerDetails = Customer::find()->andWhere(['customer_type' => '1'])->asArray()->all();
$model->date = date('d-m-Y', strtotime($model['date']));
$customerlist = [];
foreach ($customerDetails as $customerData) {

    $customersname = isset  ($customerData['company_name'])?ucfirst($customerData['company_name']) :ucfirst($customerData['coustomer_fname'] . ' ' . $customerData['coustomer_lname']);

    $customerlist[$customerData['id']] = $customersname;
}

$employeelist = [];
foreach ($employeeDetails as $employeeData) {

    $firstname = ucfirst($employeeData['first_name']);
    $lastname = ucfirst($employeeData['last_name']);

    $employeelist[$employeeData['id']] = $firstname . ' ' . $lastname;
}


$suppliersDetails = Customer::find()->andWhere(['customer_type' => '3'])->asArray()->all();

$supplierslist = [];
foreach ($suppliersDetails as $suppliersData) {

    $suppliersname = ucfirst($suppliersData['coustomer_fname'] . ' ' . $suppliersData['coustomer_lname']);

    $supplierslist[$suppliersData['id']] = $suppliersname;
}

$projectDetails = Project::find()->andWhere(['project_type' => '1'])->asArray()->all();

$projectlist = [];
foreach ($projectDetails as $projectData) {

    $projectname = ucfirst($projectData['project_name']);

    $projectlist[$projectData['id']] = $projectname;
}

$select_client = $model->client_id;
$select_type = $model->type;
if (!empty($select_client)) {
    $projectDetails = Project::find()->where(['customer_id' => $select_client])->andWhere(['project_type' => '1'])->asArray()->all();
    $project = [];
    foreach ($projectDetails as $projectData) {

        $projectname = ucfirst($projectData['project_name']);

        $project[$projectData['id']] = $projectname;
    }
} elseif ($select_type == 3) {

    $projectDetails = Project::find()->andWhere(['project_type' => '1'])->asArray()->all();
    $project = [];
    foreach ($projectDetails as $projectData) {

        $projectname = ucfirst($projectData['project_name']);

        $project[$projectData['id']] = $projectname;
    }
} else {
    $project = [];
}

$select_project = $model->project_id;
$select_subcontractor = $model->subcontractor_id;
if (!empty($select_subcontractor)) {
    $temp = [];
    $subcontractor = \app\models\SubContractor::find()->where(['project_id' => $select_project])->asArray()->all();

    foreach ($subcontractor as $subcontractor_name) {

        $temp[] = $subcontractor_name['name'];
    }
    //pr($temp);
    $subcontractor = [];
    foreach ($temp as $subcontractor_temp) {

        $subcontractor[] = \app\models\Customer::find()->where(['id' => $subcontractor_temp])->asArray()->one();
        $list = \yii\helpers\ArrayHelper::map($subcontractor, 'id', 'coustomer_fname');
    }
} else {
    $list = [];
}


$select_contractort = $model->subcontractor_id;
$lpo = [];
$subcontractor_lpo = \app\models\SubContractor::find()->where(['name' => $select_contractort])->asArray()->all();

foreach ($subcontractor_lpo as $lpo_number) {
    $newid = $lpo_number['id'];
    $lpo[$newid] = $lpo_number['lpo_number'];
}
?>


<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'enctype' => 'multipart/form-data'
            ],
        ])
?>
<div id='msg' class='msg'></div>
<div id="pettycash-form">
    <div class="row">
        <legend class="text-info"><small>All
                Transaction</small></legend>
        <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
       <div class="col-md-4">
            <?=
            $form->field($model, 'date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}<span class='glyphicon glyphicon-calendar form-control-feedback'></span>\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
            ])->textInput()->label('Date')
            ?> 
        </div>
        <div  class="mode">
        <div class="col-md-4">
            <?=
            $form->field($model, 'trans_mode', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
            ])->dropDownList($transMode, [
                'prompt' => 'Select',
                'class' => 'select from control',
            ])->label('Transaction type')
            ?> 
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'payment_mode', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
            ])->dropDownList($paymentMode, [
                'class' => 'select from control',
            ])->label('Payment Mode')
            ?> 
        </div>
        <div class="type">
            <div class="col-md-4">
            <?=
            $form->field($model, 'type', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
            ])->dropDownList($type, [
                'prompt' => 'Select Type',
                'id'=>'type_id',
                'class' => 'select from control',
            ])->label('Type')
            ?>
            </div> </div>
       <div class="ref_code">
            <div class="col-md-4">
                <?=
                $form->field($model, 'ref_code', [
                    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
                ])->dropDownList($refcode, [
                    'prompt' => 'Select Reference Code',
                    'id'=>'refcode',
                    'class' => 'select from control',
                ])->label('Reference Code')
                ?> 
            </div>
        </div>
        <div class="vehicle">
            <div class="col-md-4">
                <?=
                $form->field($model, 'vehicle', [
                    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
                ])->dropDownList($vehicle, [
                    'prompt' => 'Select Vehicle',
                      'id'=>'vehicle_id',
                    'class' => 'select from control',
                ])->label('Vehicle No.')
                ?> 
            </div>
        </div>
 <div id="description">
        <div class="col-md-4">
<?=
$form->field($model, 'description', [
    'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->textInput()->label('Description')
?>
        </div> 
 </div>

        <div id="client_id">
            <div class="col-md-4">
<?=
$form->field($model, 'client_id', [
    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->dropDownList($customerlist, [
    'prompt' => 'Select Client',
    'id' => 'client',
    'class' => 'select from control',
])->label('Client Name')
?> 
            </div>   
        </div>
        <div class="project">
            <div class="col-md-4">
<?=
$form->field($model, 'project_id', [
    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->dropDownList($project, [
    // 'prompt' => 'Choose client',
    'id' => 'projectlist',
    'class' => 'select from control',
])->label('Projects')
?> 
            </div>
        </div>

        <div class="sub_contractor">
            <div class="col-md-4">
<?=
$form->field($model, 'subcontractor_id', [
    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->dropDownList($list, [
    'prompt' => 'Choose Sub-Contractor',
    'id' => 'contractor',
    'class' => 'select from control',
])->label('SubContractor Name')
?> 
            </div>
        </div>
        <div class="lpo">
            <div class="col-md-4">
<?=
$form->field($model, 'lpo_no', [
    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->dropDownList($lpo, [
    'prompt' => 'Choose LPO',
    'id' => 'lpo_id',
    'class' => 'select from control',
])->label('LPO NO.')
?> 
            </div>
        </div>

        <div class="formaterials">
            <div class="col-md-4">
<?=
$form->field($model, 'supplier_id', [
    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->dropDownList($supplierslist, [
    'prompt' => 'Select Supplier',
    'class' => 'select from control',
])->label('Supplier')
?> 
            </div>
            <div class="col-md-4">
                <?=
                $form->field($model, 'invoice_number', [
                    'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
                ])->textInput()->label('Invoice Number')
                ?>
            </div> 
        </div> 
        <div class="employee">
            <div class="col-md-4">
<?=
$form->field($model, 'employee_id', [
    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->dropDownList($employeelist, [
    'prompt' => 'Select Employee',
     'id'=>'employeelist',
    'class' => 'select from control',
])->label('Employee')
?> 
            </div>   
        </div>
        <div id ="credit">
            <div class="col-md-4">
<?=
$form->field($model, 'credit_amount', [
    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->textInput([])->label('Credit Amount')
?> 
            </div>
        </div>
        <div id ="petty_cash">
            <div class="col-md-4">
<?=
$form->field($model, 'pettycash_amount', [
    'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->textInput([])->label('Pettycash')
?> 
            </div>
        </div>
        <div class="formaterials_amount">
            <div class="col-md-4">
<?=
$form->field($model, 'expense_amount', [
    'template' => "{label}\n
            <div class='col-md-12 col-xs-12' onblur='calculate()'>
            {input}\n
             </div>",
    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
])->textInput(['onchange' => 'calculate()'])->label('Amount(Without VAT)')
?>
            </div> 
            <div class="col-md-4">
                <?=
                $form->field($model, 'vat_percentage', [
                    'template' => "{label}\n
            <div class='col-md-12 col-xs-12' onblur='calculate()' >
            {input}\n
             </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
               ])->textInput(['onchange' => 'calculate()'])->label('Vat(%)')
                ?>
            </div>    </div>


        <div id ="expense">
            <div class="col-md-4">
                <?=
                $form->field($model, 'expense_total_amount', [
                    'template' => "{label}\n
      <div class='col-md-12 col-xs-12' onblur='calculate2()'>
            {input}\n
       </div>",
                    'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'text-align:left']
                ])->textInput(['id' => 'total_amount','onchange' => 'calculate2()'])->label(' Total Amount(With VAT)')
                ?> 
            </div> 

        </div>
       <div class ="lopinfo">
            <div class="col-md-4">
                <table class="table table-lopinfo">

             
                <tbody>
                    <tr>
                    <td class="text-info">
    <b>LPO Amount Info</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             LPO Amount : <span  class="pull-right" id="lpototal">
                            </span>
                        
                </td>

                    </tr>
                    <tr>
                      <td>
                         Paid Amount :<span class="pull-right" id="lpototalpaid">
                            </span>
                    </td>
                </tr>

                </tbody>
                </table>
            </div>
        </div>

        <div class="document">
            <div class="col-md-4">
<?php $documentpath = \Yii::$app->params['domain_url'] . $model->document; ?>
<?php if (is_null($model->document) || empty($model->document)): ?>
    <?=
    $form->field($model, 'document', [
        'template' => "{label}\n
          <div class='col-md-9'>
                {input}\n
                {hint}\n
                {error}
          </div>",
        'labelOptions' => ['class' => 'col-md-12 control-label'],
        'inputOptions' => ['class' => 'file', 'data-show-upload' => "false", 'data-show-preview' => "false"]
    ])->fileInput()
    ?>
                <?php else: ?>


    <?=
    $form->field($model, 'document', [
        'template' => "{label}\n
          <div class='col-md-9 col-xs-9'>
                {input}\n
                <div class='file-preview'>
                  <strong> Current File </strong> : 
                    <div class='clearfix'></div>
                     <a base href='$documentpath'target='_blank'> $model->document</a>
                
                {hint}\n
                {error}
          </div></div>",
        'labelOptions' => ['class' => 'col-md-12 control-label'],
        'inputOptions' => ['class' => 'file', 'data-show-upload' => "false", 'data-show-preview' => "false"]
    ])->fileInput()
    ?>
                <?php endif; ?>
            </div>
        </div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>
        <div>&nbsp;</div>

        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['id' => 'submit', 'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                    <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                </div>
            </div>
        </div>

        <?php
        ActiveForm::end();
        ?>
    </div>
</div>







<?= $this->registerJs("
    
        $('#transaction-date').datetimepicker({
            format: 'DD-MM-YYYY'
        });

 ", View::POS_READY); ?>
<script>
  $(document).ready(function () {
        
        calculate = function () {

            var amount = Number($("#transaction-expense_amount").val()).toFixed(2);
            $("#transaction-expense_amount").val(amount);
            var vat = Number($("#transaction-vat_percentage").val()).toFixed(2);
//            alert(vat);
            var vatamount = amount * (vat) / 100;
            var totamount = parseFloat(amount) + parseFloat(vatamount);
            var tot = Number(totamount).toFixed(2);
            $("#total_amount").val(tot);
            
            
          
        }
          calculate2 = function () {

            
           var amount = Number($("#total_amount").val()).toFixed(2);
           var vat = Number($("#transaction-vat_percentage").val()).toFixed(2);
           var totalvatpercentage= parseFloat(vat) + parseFloat(100);
           
           var vatonepercentage = amount / totalvatpercentage;
           var vatamount=vatonepercentage*vat;
           var totamount = parseFloat(amount) - parseFloat(vatamount);
           var tot = Number(totamount).toFixed(2);
             console.log(totalvatpercentage);
            console.log(vatonepercentage);
              console.log(vatamount);
           
            console.log(totamount);
              console.log(vat);
            
            $("#transaction-expense_amount").val(tot);
            
            
        }
        
    
     
      
    });


     $(document).ready(function () {
         
     var transMode = $('#transaction-trans_mode').val();
               if (transMode == 4) {
                $("#transaction-payment_mode option[value='1']").show();
                $("#transaction-payment_mode option[value='2']").hide();
                $("#transaction-payment_mode option[value='3']").hide();
                $("#transaction-payment_mode option[value='4']").show();
                $("#type_id option[value='1']").show();
                $("#type_id option[value='2']").show();
                $("#type_id option[value='3']").show();
                  $('#type_id').val("");
                $('#transaction-payment_mode').val(1); 
            }
        $('#transaction-trans_mode').on('change', function () {
            var transMode = $('#transaction-trans_mode').val();
            if (transMode == 1) {
                $("#transaction-payment_mode option[value='2']").show();
                $("#transaction-payment_mode option[value='1']").show();
                $("#transaction-payment_mode option[value='3']").show();
                $("#transaction-payment_mode option[value='4']").hide();
                $("#type_id option[value='1']").show();
                $("#type_id option[value='2']").show();
                $("#type_id option[value='3']").show();
                $('#type_id').val("");
                $('#transaction-payment_mode').val(1);
            } else if (transMode == 2) {
                $("#transaction-payment_mode option[value='1']").show();
                $("#transaction-payment_mode option[value='2']").hide();
                $("#transaction-payment_mode option[value='3']").show();
                $("#transaction-payment_mode option[value='4']").hide();
                $("#type_id option[value='1']").show();
                $("#type_id option[value='2']").show();
                $("#type_id option[value='3']").show();
                  $('#type_id').val("");
                $('#transaction-payment_mode').val(1);
            } else if (transMode == 3) {
                $("#transaction-payment_mode option[value='1']").hide();
                $("#transaction-payment_mode option[value='2']").hide();
                $("#transaction-payment_mode option[value='3']").hide();
                $("#transaction-payment_mode option[value='4']").hide();
                $("#type_id option[value='1']").show();
                $("#type_id option[value='2']").show();
                $("#type_id option[value='3']").hide();
                  $('#type_id').val("");
                $('#transaction-payment_mode').val(3);
            } else if (transMode == 4) {
                $("#transaction-payment_mode option[value='1']").show();
                $("#transaction-payment_mode option[value='2']").hide();
                $("#transaction-payment_mode option[value='3']").hide();
                $("#transaction-payment_mode option[value='4']").show();
                $("#type_id option[value='1']").show();
                $("#type_id option[value='2']").show();
                $("#type_id option[value='3']").show();
                  $('#type_id').val("");
                $('#transaction-payment_mode').val(1);
            } else if (transMode == 5) {
                $("#transaction-payment_mode option[value='1']").hide();
                $("#transaction-payment_mode option[value='2']").hide();
                $("#transaction-payment_mode option[value='3']").hide();
                $("#transaction-payment_mode option[value='4']").hide();
                $("#type_id option[value='1']").show();
                $("#type_id option[value='2']").show();
                $("#type_id option[value='3']").show();  
                $('#type_id').val("");
                
                $('#transaction-payment_mode').val(3);
            }
        });
    });
  
  var refcode = $('#refcode').val();
             
              
    var paymentMode = $('#transaction-payment_mode').val();
    var transMode = $('#transaction-trans_mode').val();
    var refCode = $('#transaction-ref_code').val();
    var type = $('#type_id').val();
    if (transMode == 1 && paymentMode == 1) {
        $('.project').show();

        $('#client_id').show();
        $('.employee').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
        $('#credit').show();
        $('#petty_cash').hide();
        $('#expense').hide();
        $('.formaterials').hide();
         $('.lopinfo').hide();
          $('.formaterials_amount').hide();
        $('.type').hide();
        $('.document').hide();
        $('.ref_code').hide();
          $('.vehicle').hide();
           $('.vehicle').find(':input').val('');
    } else if (transMode == 1 && paymentMode == 2 || transMode == 2 && paymentMode == 1) {
        $('.employee').show();
        $('.project').hide();

        $('#credit').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
        $('#petty_cash').show();
        $('#client_id').hide();
        $('#expense').hide();
        $('.type').hide();
        $('.formaterials').hide();
         $('.lopinfo').hide();
         $('.formaterials_amount').hide();
        $('.document').hide();
        $('.ref_code').hide();
            $('.vehicle').hide();
             $('.vehicle').find(':input').val('');
    } else if ((transMode == 1 && paymentMode == 3 && type == 1  && refcode=='PC-01' ||(transMode == 1 && paymentMode == 3 && type == 1  && refcode=='VM-06'))|| (transMode == 3 && paymentMode == 3 && type == 1 && refcode=='PC-01'||(transMode == 3 && paymentMode == 3 && type == 1 && refcode=='VM-06'))|| (transMode == 5 && paymentMode == 3 && type == 1 && refcode=='PC-01' || (transMode == 5 && paymentMode == 3 && type == 1 && refcode=='VM-06'))) {

        $('.project').hide();

        $('#client_id').hide();
        $('.employee').hide();
        $('#credit').hide();
        $('#petty_cash').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
        $('#expense').show();
        $('#description').show();
        $('.formaterials').hide();
         $('.formaterials_amount').show();
         $('.lopinfo').hide();
        $('.document').hide();
        $('.ref_code').show();
            $('.vehicle').show();

    }
    
    else if ((transMode == 2 && paymentMode == 3 && type == 1 && refcode=='PC-01') || (transMode == 2 && paymentMode == 3 && type == 1 && refcode=='VM-06')) {

        $('.project').hide();

        $('#client_id').hide();
        $('.employee').show();
        $('#credit').hide();
        $('#petty_cash').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
        $('#expense').show();
        $('#description').show();
        $('.formaterials').hide();
         $('.formaterials_amount').show();
         $('.lopinfo').hide();
        $('.document').hide();
        $('.ref_code').show();
            $('.vehicle').show();

    }
    
    
    
   else if ((transMode == 1 && paymentMode == 3 && type == 1  && refcode!='PC-01' ||(transMode == 1 && paymentMode == 3 && type == 1  && refcode!='VM-06'))|| (transMode == 3 && paymentMode == 3 && type == 1 && refcode!='PC-01'||(transMode == 3 && paymentMode == 3 && type == 1 && refcode!='VM-06'))|| (transMode == 5 && paymentMode == 3 && type == 1 && refcode!='PC-01' || (transMode == 5 && paymentMode == 3 && type == 1 && refcode!='VM-06'))) {

        $('.project').hide();

        $('#client_id').hide();
        $('.employee').hide();
        $('#credit').hide();
        $('#petty_cash').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
        $('#expense').show();
        $('#description').show();
        $('.formaterials').hide();
         $('.lopinfo').hide();
             $('.formaterials_amount').show();
        $('.document').hide();
        $('.ref_code').show();
            $('.vehicle').hide();

    }
   
   
    else if ((transMode == 2 && paymentMode == 3 && type == 1 && refcode!='PC-01' || (transMode == 2 && paymentMode == 3 && type == 1 && refcode!='VM-06'))) {

        $('.project').hide();

        $('#client_id').hide();
        $('.employee').show();
        $('#credit').hide();
        $('#petty_cash').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
        $('#expense').show();
        $('#description').show();
        $('.formaterials').hide();
         $('.lopinfo').hide();
             $('.formaterials_amount').show();
        $('.document').hide();
        $('.ref_code').show();
            $('.vehicle').hide();

    }
   
       
    else if (transMode == 1 && paymentMode == 3 && type == 2 || transMode == 3 && paymentMode == 3 && type == 2|| transMode == 5 && paymentMode == 3 && type == 2) {
      
        $('.project').show();
          $('#description').show();
        $('#client_id').show();
        $('.employee').hide();
           $('#description').show();
        $('#credit').hide();
        $('#petty_cash').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
         $('.lopinfo').hide();
        $('#expense').show();
        $('.formaterials').show();
        $('.document').show();
        $('.ref_code').hide();
            $('.vehicle').hide();
                         $('.vehicle').find(':input').val('');
         } 
    
    else if (transMode == 2 && paymentMode == 3 && type == 2 ) {
      
        $('.project').show();
          $('#description').show();
        $('#client_id').show();
        $('.employee').show();
           $('#description').show();
        $('#credit').hide();
        $('#petty_cash').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
         $('.lopinfo').hide();
        $('#expense').show();
        $('.formaterials').show();
        $('.document').show();
        $('.ref_code').hide();
            $('.vehicle').hide();
                         $('.vehicle').find(':input').val('');


    }
    
    
    
    
    
    else if (transMode == 1 && paymentMode == 3 && type == 3 || transMode == 3 && paymentMode == 3 && type == 3|| transMode == 5 && paymentMode == 3 && type == 3) {
        $('.project').show();
          $('#description').show();
        $('#client_id').hide();
        $('.employee').hide();
           $('#description').show();
        $('#credit').hide();
        $('#petty_cash').hide();
        $('.sub_contractor').show();
        $('.lopinfo').show();
        $('.lpo').show();
        $('#expense').show();
        $('#total_amount').prop('readonly', false);
        $('.formaterials').hide();
                $('.formaterials_amount').show();

        $('.document').hide();
        $('.ref_code').hide();
            $('.vehicle').hide();
                         $('.vehicle').find(':input').val('');


    } 
  
   else if ( transMode == 2 && paymentMode == 3 && type == 3 ) {
        $('.project').show();
          $('#description').show();
        $('#client_id').hide();
        $('.employee').show();
           $('#description').show();
        $('#credit').hide();
        $('#petty_cash').hide();
        $('.sub_contractor').show();
        $('.lopinfo').show();
        $('.lpo').show();
        $('#expense').show();
        $('#total_amount').prop('readonly', false);
        $('.formaterials').hide();
                $('.formaterials_amount').show();

        $('.document').hide();
        $('.ref_code').hide();
            $('.vehicle').hide();
                         $('.vehicle').find(':input').val('');


    }
    
    
    
    else if (transMode == 4 && paymentMode == 1) {
        $('.project').hide();

        $('.employee').show();
        $('#client_id').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
        $('#petty_cash').hide();
             $('.formaterials_amount').hide();
        $('.type').hide();
        $('#credit').hide();
        $('#expense').show();
         $('.lopinfo').hide();
        $('.formaterials').hide();
        $('.document').hide();
        $('.ref_code').hide();
            $('.vehicle').hide();
             $('.vehicle').find(':input').val('');
    } else if (transMode == 4 && paymentMode == 4) {
        $('.project').hide();

        $('.employee').show();
        $('#client_id').hide();
        $('.type').hide();
        $('#petty_cash').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
         $('.formaterials_amount').hide();
        $('.lpo').hide();
        $('#credit').show();
        $('#expense').hide();
         $('.lopinfo').hide();
        $('.formaterials').hide();
        $('.document').hide();
        $('.ref_code').hide();
            $('.vehicle').hide();
             $('.vehicle').find(':input').val('');
    } 
    else if (transMode == 5 && paymentMode == 3 && type == 1) {
        $('.project').hide();

        $('.employee').hide();
        $('.type').show();
        $('#credit').hide();
        $('#expense').show();
         $('.lopinfo').hide();
        $('.formaterials').hide();
        $('.formaterials_amount').hide();
        $('.document').hide();
        $('.ref_code').show();
        $('.vehicle').hide();
    } 
    
 
    
    else if (transMode == "") {
        $('.project').hide();

        $('.employee').hide();
        $('.sub_contractor').hide();
          $('#total_amount').prop('readonly', false);
        $('.lpo').hide();
        $('#client_id').hide();
        $('.type').hide();
        $('#credit').hide();
        $('#expense').hide();
         $('.lopinfo').hide();
        $('.formaterials').hide();
             $('.formaterials_amount').hide();
        $('.document').hide();
        $('.ref_code').hide();
            $('.vehicle').hide();
             $('.vehicle').find(':input').val('');
        $('#petty_cash').hide();
        $('.lpo').hide();
    }




    $(document).ready(function () {
        $('#transaction-payment_mode').on('change', function () {
            var paymentMode = $('#transaction-payment_mode').val();
            var transMode = $('#transaction-trans_mode').val();
            var refCode = $('#transaction-ref_code').val();
            if (transMode == 1 && paymentMode == 1) {
                $('.project').show();

                $('#client_id').show();
                $('.employee').hide();
                $('.sub_contractor').hide();
                    $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.sub_contractor').find(':input').val('0');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('#credit').show();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('.type').hide();
                $('#expense').hide();
                  $('.formaterials_amount').hide();
                  $('#expense').find(':input').val('0');
                $('.formaterials').hide();
                 $('.lopinfo').hide();
                $('.document').hide();
                $('.ref_code').hide();
                    $('.vehicle').hide();
                     $('.vehicle').find(':input').val('');
                      $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 1 && paymentMode == 2 || transMode == 2 && paymentMode == 1) {
                $('.employee').show();

                $('.project').hide();
                $('#credit').hide();
                $('.sub_contractor').hide();
                   $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('#petty_cash').show();
                $('#client_id').hide();
                $('#expense').hide();
                 $('.formaterials_amount').hide();
                  $('#expense').find(':input').val('0');
                $('.type').hide();
                $('.formaterials').hide();
                 $('.lopinfo').hide();
                $('.document').hide();
                $('.ref_code').hide();
                    $('.vehicle').hide();
                     $('.vehicle').find(':input').val('');
                      $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 1 && paymentMode == 3) {
                $('.project').hide();

                $('.sub_contractor').hide();
                   $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('.employee').hide();
                   $('#description').show();
                $('.type').show();
                $('#credit').hide();
                $('#client_id').hide();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('#expense').show();
                $('#total_amount').val(0);
                $('.formaterials').hide();
                 $('.lopinfo').hide();
                $('.document').hide();
                $('.ref_code').show();
                    $('.vehicle').show();
                     $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 2 && paymentMode == 3) {
                $('.project').hide();

                $('.sub_contractor').hide();
                    $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('.employee').show();
                $('.type').show();
                $('#credit').hide();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('#expense').show();
                $('#total_amount').val(0);
                $('.formaterials').hide();
                 $('.lopinfo').hide();
                $('.document').hide();
                $('.ref_code').show();
                    $('.vehicle').show();
                     $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 3 && paymentMode == 3) {
                $('.project').hide();

                $('.sub_contractor').hide();
                   $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('.employee').show();
                $('.type').show();
                $('#credit').hide();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('#expense').show();
                $('#total_amount').val(0);
                $('.formaterials').hide();
                 $('.lopinfo').hide();
                $('.document').hide();
                $('.ref_code').show();
                    $('.vehicle').show();
                     $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 4 && paymentMode == 1) {
                $('.project').hide();

                $('.employee').show();
                $('#client_id').hide();
                 $('.formaterials_amount').hide();
                $('.type').hide();
                $('#credit').hide();
                $('#expense').show();
                $('#total_amount').val(0);
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').hide();
                    $('.vehicle').hide();
                     $('.vehicle').find(':input').val('');
                      $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 4 && paymentMode == 4) {
                $('.project').hide();

                $('.sub_contractor').hide();
                 $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('#client_id').hide();
                $('.employee').show();
                $('.type').hide();
                $('#credit').show();
                 $('.formaterials_amount').hide();
                  $('.lopinfo').hide();
                $('#expense').hide();
                 $('#expense').find(':input').val('0');
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').hide();
                    $('.vehicle').hide();
                     $('.vehicle').find(':input').val('');
                      $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            }
            else if (transMode == 5 && paymentMode == 3) {
                $('.project').hide();

                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                   $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();   
                $('.lpo').find(':input').val('');
                
                $('.employee').show();
                $('.type').show();
                $('#credit').hide();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('#expense').show();
                $('#total_amount').val(0);
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').show();
                    $('.vehicle').show();
                     $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            }

        });
        $('#transaction-trans_mode').on('change', function () {
            var paymentMode = $('#transaction-payment_mode').val();
            var transMode = $('#transaction-trans_mode').val();
            var refCode = $('#transaction-ref_code').val();
            if (transMode == 1 && paymentMode == 1) {
                $('.project').show();

                $('#client_id').show();
                $('.employee').hide();
                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                   $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('#credit').show();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('#expense').hide();
                  $('.formaterials_amount').hide();
                 $('#expense').find(':input').val('0');
                $('.type').hide();
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').hide();
                  $('.ref_code').find(':input').val('');
                    $('.vehicle').hide();
                     $('.vehicle').find(':input').val('');
                      $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 1 && paymentMode == 2 || transMode == 2 && paymentMode == 1) {
                $('.employee').show();

                $('.project').hide();
                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                   $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('#credit').hide();
                $('#client_id').hide();
                $('#petty_cash').show();
                $('#expense').hide();
                 $('.formaterials_amount').hide();
                 $('#expense').find(':input').val('0');
                $('.type').hide();
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').hide();
                  $('.ref_code').find(':input').val('');
                    $('.vehicle').hide();
                     $('.vehicle').find(':input').val('');
                      $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 1 && paymentMode == 3) {
                $('.project').hide();

                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                  $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('.employee').hide();
                   $('#description').show();
                $('.type').show();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('#client_id').hide();
                $('#credit').hide();
                $('#expense').show();
                 $('#expense').find(':input').val('0');
                $('#total_amount').val(0);
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').show();
                  $('.ref_code').find(':input').val('');
                    $('.vehicle').show();
                     $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 2 && paymentMode == 3) {
                $('.project').hide();

                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                   $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('.employee').show();
                $('.type').show();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('#credit').hide();
                $('#expense').show();
                 $('#expense').find(':input').val('0');
                $('#total_amount').val(0);
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').show();
                  $('.ref_code').find(':input').val('');
                    $('.vehicle').show();
                     $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 3 && paymentMode == 3) {
                $('.project').hide();

                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                   $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('.employee').show();
                $('.type').show();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('#credit').hide();
                $('#expense').show();
                 $('#expense').find(':input').val('0');
                $('#total_amount').val(0);
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').show();
                  $('.ref_code').find(':input').val('');
                    $('.vehicle').show();
                     $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 4 && paymentMode == 1) {
                $('.project').hide();

                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                  $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('#client_id').hide();
                $('.employee').show();
                $('.type').hide();
                $('#credit').hide();
                $('#expense').show();
                 $('.formaterials_amount').hide();
                 $('#expense').find(':input').val('0');
                $('#total_amount').val(0);
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').hide();
                  $('.ref_code').find(':input').val('');
                    $('.vehicle').hide();
                     $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (transMode == 4 && paymentMode == 4) {
                $('.project').hide();

                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                     $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('#client_id').hide();
              
                $('.employee').show();
                $('.type').hide();
                $('#credit').show();
                $('#expense').hide();
                 $('.formaterials_amount').hide();
                 $('#expense').find(':input').val('0');
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').hide();
                  $('.ref_code').find(':input').val('');
                    $('.vehicle').hide();
                     $('.vehicle').find(':input').val('');
                      $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            }
            else if (transMode == 5 && paymentMode == 3) {
                $('.project').hide();

                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                     $('#total_amount').prop('readonly', false);
                      $('.sub_contractor').find(':input').val('');
                $('.lpo').hide();
                   $('.lpo').find(':input').val('');
                $('.employee').show();
                $('.type').show();
                $('#petty_cash').hide();
                  $('#petty_cash').find(':input').val('0');
                $('#credit').hide();
                $('#expense').show();
                 $('#expense').find(':input').val('0');
                $('#total_amount').val(0);
                $('.formaterials').hide();
                $('.document').hide();
                $('.ref_code').show();
                  $('.ref_code').find(':input').val('');
                    $('.vehicle').show();
                     $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            }

        });

        $('#type_id').on('change', function () {

           var paymentMode = $('#transaction-payment_mode').val();
            var transMode = $('#transaction-trans_mode').val();
            var type_id = $('#type_id').val();
            if (type_id == 2 && transMode==1 && paymentMode==3 || type_id == 2 && transMode==2 && paymentMode==3 || type_id == 2 && transMode==3 && paymentMode==3 ||  type_id == 2 && transMode==5 && paymentMode==3) {
                $('.project').show();
                $('#client_id').show();

//                $('.employee').show();
                $('.employee').find(':input').val('');
                $('#credit').hide();
                $('#expense').show();
                $('#transaction-supplier_id').val('');
                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                $('#total_amount').prop('readonly', false);
                $('.formaterials').show();
                $('.formaterials_amount').show();
                $('.document').show();
                $('.ref_code').hide();
                $('.vehicle').hide();
                $('.lpo').hide();
                 $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } else if (type_id == 1 && transMode==5 && paymentMode==3  ) {
                $('.project').hide();
                     
                $('#client_id').hide();
//                $('.employee').show();
                $('.employee').find(':input').val('');
                $('#credit').hide();
                $('#description').show();
                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                $('#transaction-supplier_id').val('');
                $('#expense').show();
                $('#total_amount').prop('readonly', false);
                $('.formaterials').hide();
                $('.formaterials_amount').hide();
                $('.document').hide();
                $('.ref_code').show();
                $('.vehicle').hide();
                $('.lpo').hide();
                 $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            }
            else if (type_id == 1 && transMode==1 && paymentMode==3 || type_id == 1 && transMode==2 && paymentMode==3 || type_id == 1 && transMode==3 && paymentMode==3  ) {
                $('.project').hide();

                $('#client_id').hide();
//                $('.employee').show();
                $('.employee').find(':input').val('');
                $('#credit').hide();
                $('#description').show();
                $('.sub_contractor').hide();
                 $('.lopinfo').hide();
                $('#transaction-supplier_id').val('');
                $('#expense').show();
                $('#total_amount').prop('readonly', false);
                $('.formaterials').hide();
                $('.formaterials_amount').show();
                $('.document').hide();
                $('.ref_code').show();
                $('.vehicle').hide();
                $('.lpo').hide();
                 $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            }
        
            else if (type_id == 3 && transMode==1 && paymentMode==3 || type_id == 3 && transMode==2 && paymentMode==3 || type_id == 3 && transMode==3 && paymentMode==3 ||  type_id == 3 && transMode==5 && paymentMode==3) {
                $('.project').show();
                $('#transaction-supplier_id').val('');
                $('#client_id').hide();
//                $('.employee').show();
                $('.employee').find(':input').val('');
                $('#credit').hide();
                $('.sub_contractor').show();
                 $('.lopinfo').show();
                $('#expense').show();
                $('#total_amount').prop('readonly', false);
                $('.formaterials').hide();
                $('.formaterials_amount').show();
                $('.document').hide();
                $('.ref_code').hide();
                $('.vehicle').hide();
                $('.lpo').show();
                 $('#total_amount').val('');
                            $('#transaction-vat_percentage').val(0);
                   $('#transaction-expense_amount').val('');
            } 
        });
         $('#refcode').on('change', function () {
              var refcode = $(this).val();
              if (refcode =='PC-01' || refcode=='VM-06') {
                   $('.vehicle').show(); 
               }
               else{
                   $('.vehicle').hide(); 
                                $('.vehicle').find(':input').val('');

               }
            
             
         });
    });
</script>

<script>

    $(document).ready(function () {

        $(document).on("change", "#client", function () {
            var client = $(this).val();


            $.ajax({
                url: 'getproject',
                //data: $('form').serialize(),
                data: {'client': client},
                type: 'POST',
                success: function (response) {

                    console.log(response);

                    optionsData = '<option value="">Select</option>';
                    $.each(response, function (key, value) {
                        optionsData += '<option value="' + key + '">' + value + '</option>';
                    });

                    $("#projectlist").html(optionsData);



                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });


</script>
<script>

    $(document).ready(function () {
        $(document).on("change", "#type_id", function () {
            var type = $(this).val();
           


            $.ajax({
                url: 'getprojectlist',
                //data: $('form').serialize(),
                data: {'type': type},
                type: 'POST',
                success: function (response) {

                    console.log(response);

                    optionsData = '<option value="">Select</option>';
                    $.each(response, function (key, value) {
                        optionsData += '<option value="' + key + '">' + value + '</option>';
                    });

                    $("#projectlist").html(optionsData);



                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });


</script>
<script>

    $(document).ready(function () {

        $(document).on("change", "#projectlist", function () {

            var project = $(this).val();



            $.ajax({
                url: 'getsubcontractor',
                //data: $('form').serialize(),
                data: {'project': project},
                type: 'POST',
                success: function (response) {

                    console.log(response);

                    optionsData = '<option value="">Select</option>';
                    $.each(response, function (key, value) {
                        optionsData += '<option value="' + key + '">' + value + '</option>';
                    });

                    $("#contractor").html(optionsData);



                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });


</script>
<script>

    $(document).ready(function () {

        $(document).on("change", "#contractor", function () {
            var contractor = $(this).val();


            $.ajax({
                url: 'getlpo',
                //data: $('form').serialize(),
                data: {'contractor': contractor},
                type: 'POST',
                success: function (response) {

                    console.log(response);

                    optionsData = '<option value="">Select</option>';
                    $.each(response, function (key, value) {
                        optionsData += '<option value="' + key + '">' + value + '</option>';
                    });

                    $("#lpo_id").html(optionsData);



                },
                error: function (error) {
                    console.log(error);
                }
            });
        });
    });


</script>
<script>

    $(document).ready(function () {

        var lpovalue = $('.lpo :selected').text();


            $.ajax({
                url: 'getlpoamount',
                //data: $('form').serialize(),
                data: {'lpovalue': lpovalue},
                type: 'POST',
                success: function (response) {

                    console.log(response);

                    optionsData = response.amount;


                  
                            $("#lpototal").html(response.totalpoamount);
                     $("#lpototalpaid").html(response.partpayment);



                },
                error: function (error) {
                    console.log(error);
                }
            });
        $(document).on("change", "#lpo_id", function () {
            var lpovalue = $('.lpo :selected').text();


            $.ajax({
                url: 'getlpoamount',
                //data: $('form').serialize(),
                data: {'lpovalue': lpovalue},
                type: 'POST',
                success: function (response) {

                    console.log(response);

                    optionsData = response.amount;


                    $("#total_amount").val(optionsData);
                            $("#lpototal").html(response.totalpoamount);
                     $("#lpototalpaid").html(response.partpayment);



                },
                error: function (error) {
                    console.log(error);
                }
            });

        });
    });


</script>
<script>

    
        $("form#active-form").submit(function(e) {
    e.preventDefault();
    event.stopPropagation();
            var employee = $('#employeelist').val();
            var expense_amount=$('#total_amount').val();
             var trans_mode=$('#transaction-trans_mode').val();
            var payment_mode=$('#transaction-payment_mode').val();
            var type=$('#type_id').val();
    $("#transmode").removeClass("error-highlight");
    $("#client").removeClass("error-highlight");
   
    
    
   var formData = new FormData(this);
       if($('#transmode').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Transaction Type cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
       if(trans_mode==1 && payment_mode==1){
        if($('#client').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Client cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
       else if($('#projectlist').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Project cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
        else if($('#transaction-credit_amount').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Credit Amount cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
       }
     if(trans_mode==1 && payment_mode==2 || trans_mode==2 && payment_mode==1 ){
          if($('#employeelist').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Employee Name cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
        else if($('#transaction-pettycash_amount').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Amount cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }}
       if(trans_mode==1 && payment_mode==3 || trans_mode==2 && payment_mode==3 || trans_mode==3 && payment_mode==3 || trans_mode==5 && payment_mode==3){
          if($('#type_id').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Type cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
            
       }
      
       
       }


        if(trans_mode==2 && payment_mode==3 && type==1 || trans_mode==2 && payment_mode==3  && type==2 || trans_mode==2 && payment_mode==3  && type==3){
                   if($('#employeelist').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Employee Name cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
   }
        if(trans_mode==1 && payment_mode==3 && type==1 || trans_mode==2 && payment_mode==3  && type==1 || trans_mode==3 && payment_mode==3  && type==1|| trans_mode==5 && payment_mode==3  && type==1){
          if($('#refcode').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Ref-Code cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
       
           else if($('#total_amount').val()==='') {
               //alert("Name cannot be Empty ");
                $("#error-message").removeClass("hide");
                $("#error-message").html("<p>Amount cannot be Empty</p>");
                $("#employee-first_name").addClass("error-highlight");
                return false;
           }
        
       }
       if(trans_mode==1 && payment_mode==3 && type==2 || trans_mode==2 && payment_mode==3  && type==2 || trans_mode==3 && payment_mode==3  && type==2 || trans_mode==5 && payment_mode==3  && type==2){
          if($('#client').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Client cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
        else if($('#projectlist').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Project cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
       else if($('#transaction-expense_amount').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Amount cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
      
       }
        if(trans_mode==1 && payment_mode==3 && type==3 || trans_mode==2 && payment_mode==3  && type==3 || trans_mode==3 && payment_mode==3  && type==3 || trans_mode==5 && payment_mode==3  && type==3){
        if($('#projectlist').val()==='') {
            //alert('hhh');
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Project cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
        else  if($('#contractor').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Contractor Name cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
        else if($('#projectlist').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Project cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
       else if($('#lpo_id').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Lpo No. cannot be Empty</p>");
            $("#employee-first_name").addClass("error-highlight");
            return false;
       }
       }
       
       
       else {
           $("#error-message").addClass("hide");
           $("#empidcheck-val").val(0);
        }
        if (trans_mode == 2 && payment_mode == 3) {
        $.ajax({
            url: 'checkbalance',
            //data: $('form').serialize(),
            data: {'employee': employee},
            type: 'POST',
            success: function (response) {
              
                 var balance_amount = response; 
                   console.log('balance_amount'+balance_amount);
                  var expense = parseFloat(expense_amount); 
               console.log('expense'+expense);
             
             
                    if (balance_amount < expense) {
                        console.log('low')
                        $("#error-message").removeClass("hide");
                        $("#error-message").html("<p>Your Credit Balance Low</p>");
                        $("#total_amount").addClass("error-highlight");
                        return false;
                        console.log("After return false");

                    } else {
                         $("#total_amount").removeClass("error-highlight");
                            $.ajax({
                                url: $('#active-form').attr('action'),
                                type: 'POST',
                                data: formData,
                                success: function(){

                                },
                                cache: false,
                                contentType: false,
                                processData: false
                            });
                        
                    }
                

            },
            error: function (error) {
                console.log(error);
            }
        });
   }else{
   
    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('Your Transaction Submitted');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();

            },
        cache: false,
        contentType: false,
        processData: false
    });
   }


      

        });
  

</script>