<?php

use kartik\grid\GridView;
use app\models\Customer;
use app\models\Project;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use kartik\widgets\ActiveForm;
use kartik\builder\Form;
use kartik\builder\FormGrid;


//$this->title = 'VAT';
$this->params['breadcrumbs'][] = 'VAT';
//$this->params['title_icon'] = 'fa-credit-card';

$vat = isset($vatAmount[0]['vatTotalAmount'])? number_format((float)$vatAmount[0]['vatTotalAmount'], 2, '.', ''):0; 
$vatFromPettyCash = isset($vatFromPettuCashAmount[0]['vatFromPettuCashTotalAmount'])? number_format((float)$vatFromPettuCashAmount[0]['vatFromPettuCashTotalAmount'], 2, '.', ''):0; 
$haveToPay = $vat - $vatFromPettyCash;
//$haveToPay = $haveToPay>0?$haveToPay:0;
?>

<!-- Custom Theme Style Start Here -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/style.bundle.css" />
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/app-table.css" />

<!-- Custom Theme Style End Here -->
<style type="text/css">
    table.kt-datatable__table thead th:nth-child(1){width: 3% !important;}
    table.kt-datatable__table thead th:nth-child(2){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(3){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(4){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(5){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(6){width: auto !important;}
    .btn.dropdown-toggle:after, .nav-link.dropdown-toggle:after{display: none;}
    .kt-datatable--brand.kt-datatable--scroll .grid-view.hide-resize{margin-bottom: 0;}
    .panel.panel-primary{border-top: unset;}
    .custom-margin-style{margin-right: 0;}
    .panel.panel-primary{    position: relative;}
    .panel-primary>.panel-heading .pull-right{    float: unset !important;}
    .panel-primary>.panel-heading{padding: 0;border:unset;}
    .panel.panel-default{background-color: #fff;}
    table.kt-datatable__table thead th{border:1px solid #E5E5E5 !important;}
    .panel-default .panel-heading, .panel-primary .panel-heading, .panel-success .panel-heading, .panel-info .panel-heading, .panel-warning .panel-heading, .panel-danger .panel-heading{background: #ecf0ff !important; border-color: #bcc7ef !important; border-bottom: 2px solid #bcc7ef !important;}
</style>
<div class="row">
    <div class="col-md-9 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-credit-card" aria-hidden="true"></i> VAT
    </div>
    <div class="col-md-3">
    <h3 class="text-center">Date Range</h3>
    <div id="reportrange" data-daterangeurl="<?php echo Url::toRoute("accounts/daterange"); ?>" data-url="<?php echo Url::toRoute("vat"); ?>"  class="dtrange  col-md-offset-1 col-md-10" >
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span> </span> <b class="caret"></b>
    </div>
</div>
    <input type="hidden" value="<?php echo $dataRange; ?>" id="selectedDRange">
    <div class="clearfix"></div> 
</div>
<div class="row boxrow">
<div class="col-md-3">
   <div class="widget widget-info widget-padding-sm">
      <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
                  <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                       <div class="widget-title">Inward VAT Amount</div> <br><br>                                                                       
                          <div class="widget-subtitle"><?php echo number_format((float)$vat , 2, '.', ',')?></div>
                          <div class="widget-int"></div>
                       </div></div></div></div>
       </div>                            
       </div>         
</div>
<div class="col-md-3">
   <div class="widget widget-info widget-padding-sm">
      <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
                  <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                       <div class="widget-title">Outward VAT Amount </div> <br><br>                                                                       
                          <div class="widget-subtitle"><?php echo number_format((float)$vatFromPettyCash , 2, '.', ',')?></div>
                          <div class="widget-int"></div>
                       </div></div></div></div>
       </div>                            
       </div>         
</div>

<div class="col-md-3">
   <div class="widget widget-info widget-padding-sm">
      <div class="owl-carousel owl-theme" id="owl-example" style="opacity: 1; display: block;">
                  <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 1452px; left: 0px; display: block; transition: all 1000ms ease; transform: translate3d(0px, 0px, 0px);"><div class="owl-item" style="width: 242px;"><div>                                    
                       <div class="widget-title">Total Payable VAT</div> <br><br>                                                                       
                          <div class="widget-subtitle"><?php echo number_format((float)$haveToPay , 2, '.', ',')?></div>
                          <div class="widget-int"></div>
                       </div></div></div></div>
       </div>                            
       </div>         
</div>

</div>                              
<div class="clearfix"></div>
<div class="row">
<div class="col-md-12">
<div class="nav-tab-custom">
                    <ul class="nav nav-tabs">
                        <li class="active" id="summarytab"><a href="#summary" data-toggle="tab">Inward VAT</a></li>
                        <li id="timetab"><a href="#time" data-toggle="tab">OutWard VAT</a></li>
                      
                        
                    </ul>
                       <div class="tab-content">
        <div class="tab-pane summary active" id="summary">

            <div class="box-body table-responsive">
                                         
<section class="content edusec-user-profile">

    <div class="table-responsive">
      <div class="kt-portlet kt-portlet--height-fluid kt-portlet--mobile " style="margin-bottom: 0 !important;box-shadow: unset !important;">
<div class="kt-portlet__body kt-portlet__body--fit">
<div class="kt-datatable kt-datatable--default kt-datatable--brand kt-datatable--scroll kt-datatable--loaded">
    <?php
 if ($dataProvider) {
   //  pr($dataProvider);
   
       
    echo GridView::widget([
        
        'dataProvider' => $dataProvider,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'customer_id',
                'header' => '<div style="width:80px;">Client</div>',
                'value' =>  function($model, $key, $index) {
                 $client = Customer::find()->where(['id'=>$model->customer_id])->asArray()->one();
                 $companyName = $client['company_name'];
                 $fname = $client['coustomer_fname'];
                 $lname = $client['coustomer_lname'];
                 return !empty($companyName) ? Html::a($companyName, ['/customer/view', 'id' => $model->customer_id]) :Html::a($fname.' '.$lname, ['/customer/view', 'id' => $model->customer_id]);
               },
                'format' => 'raw',
            ],

            [
                'attribute' => 'invoice_date',
                'header' => '<div style="width:100px;">Date</div>',
                'value' => function($model, $key, $index) {
                return Yii::$app->formatter->asDate($model->invoice_date, 'php:Y-m-d');
            },
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
                'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],
           
            [
                'attribute' => 'invoice_number',
                'header' => '<div style="width:80px;">Invoice No</div>',
                'value' => function($model, $key, $index) {
                return isset($model->invoice_number)?$model->invoice_number:'-';
                }
            ],

            [
                'attribute' => 'total_amount',
                'header' => '<div style="width:100px;">Invoice Amount</div>',
                //'format' => ['currency', 'INR'],
                'format' => ['decimal', 2],
                'value' => function($model, $key, $index) {
                return isset($model->total_amount)?$model->total_amount:0;
                }
            ],
            
            [
              'header' => '<div style="width:100px;">Actual Cost</div>',
              'format' => ['decimal', 2],
              'value' => function($model, $key, $index) {
                $actualcost =  $model->total_amount - $model->vat_amount;
                return $actualcost;
            },
            ],
            [
                'attribute' => 'vat_amount',
                'header' => '<div style="width:100px;">VAT Amount</div>',
                //'format' => ['currency', 'INR'],
                'format' => ['decimal', 2],
                'value' => function($model, $key, $index) {
                return isset($model->vat_amount)?$model->vat_amount:0;
                }
            ], 
            // [
            //     'attribute' => 'vat',
            //     'header' => '<div style="width:100px;">VAT Percentage</div>',
           
            //     'format' => ['decimal', 2],
            //     'value' => function($model, $key, $index) {
            //     return isset($model->vat)?$model->vat:0;
            //     }
            // ],

        
],

  // check the configuration for grid columns by clicking button above
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => 'Inward VAT', 'options' => ['colspan' => 7, 'class' => 'hide', 'style' => 'font-size:18px']],
                ],],
             [
                'columns' => [
                    ['content' => '  &nbsp; <span style="font-weight:normal"> </span>', 'options' => ['colspan' => 1, 'class' => 'hide',]],
                    ['content' => 'Inward VAT Amount : <span style="font-weight:normal">' . $vat . '</span>', 'options' => ['colspan' => 2, 'class' => 'hide',]],
                    ['content' => 'Outward VAT Amount : <span style="font-weight:normal">' . $vatFromPettyCash . '</span>', 'options' => ['colspan' => 2, 'class' => 'hide',]],
                    ['content' => 'Total Payable VAT : <span style="font-weight:normal">' . $haveToPay . '</span>', 'options' => ['colspan' => 2, 'class' => 'hide',]],
                ],],
        ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => false, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '',
        ],
        'persistResize' => false,
        'exportConfig' => [GridView::EXCEL => [
                'filename' => 'Inward VAT',
            ],
           GridView::PDF => [
                'filename' => 'Inward VAT',
                'showHeader' => true,
                'showPageSummary' => false,
                'showFooter' => true,
                'showCaption' => true,
                
             
                'config' => [
                    //'cssInline' =>'table{direction:0}' ,
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                    //'contentBefore'=>'<div style="font-size:30px; text-align:center; text-decoration:underline;">List Of Clients</div>'
                ],
          ]]

      
]);
}
 
?>                                

</div>
</div>
</div>
</div>
    </section>               
</div>
  </div>
                          
              <div class="tab-pane" id="time">
               <div class="box-body table-responsive">
                   <?php
 if ($dataProvider2) {
   
       
    echo GridView::widget([
        
        'dataProvider' => $dataProvider2,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            [
                'attribute' => 'description',
                'vAlign' => 'middle',
                'header' => '<div style="width:150px;">Particulars</div>',
                 'value' => function($model) {
                $refcode = Yii::$app->params['refcode'];
                if ($model->type == 1 && $model->ref_code == 'PC-01' || $model->type == 1 && $model->ref_code == 'VM-06') {
                        $ref_code = $refcode[$model->ref_code];

                        return !empty($ref_code) ? $ref_code . '/' . $model->categoryMaster['description']  : '-';
                    } else if ($model->type == 1) {
                        $ref_code = $refcode[$model->ref_code];

                        return !empty($ref_code) ? $ref_code : '-';
                    } else {
                        return !empty($model->description) ? ($model->description) : '-';
                    } 
                 }
            ],
           [
                'attribute' => 'client_id',
                'vAlign' => 'middle',
                'header' => '<div style="width:180px;">Supplier/SubCont</div>',
                 
                 'value' => function($model) {
                   
                    if ($model->type==3){
                       
                           $subcontractor_Name= !empty($model->subcontractorName['company_name'])?$model->subcontractorName['company_name']:($model->subcontractorName['coustomer_fname'] . " " . $model->subcontractorName['coustomer_lname']);
                       
                 return !empty($subcontractor_Name) ?$subcontractor_Name:'-';
                 
                     }
                      else if ($model->type==2){
                         
                           $supplier_Name=  !empty($model->supplierName['company_name'])?$model->supplierName['company_name']:($model->supplierName['coustomer_fname'] . " " . $model->supplierName['coustomer_lname']) ;
                       
                 return !empty($supplier_Name) ?$supplier_Name:'-';
                 
                     } else {
                        return !empty($model->description) ? ($model->description) : '-';
                    } 
                     
                     },


            ], 
            [
                'attribute' => 'date',
                'header' => '<div style="width:100px;">Date</div>',
                'value' => function($model, $key, $index) {
                return Yii::$app->formatter->asDate($model->date, 'php:Y-m-d');
            },
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
                'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],
             [
                'attribute' => 'expense_total_amount',
                'header' => '<div style="width:140px;">Expense Amount</div>',
                //'format' => ['currency', 'INR'],
                'format' => ['decimal', 2],
                'value' => function($model, $key, $index) {
                return isset($model->expense_total_amount)?$model->expense_total_amount:0;
                }
            ],
            [
                'attribute' => 'expense_amount',
                'header' => '<div style="width:100px;">Actual Cost</div>',
                //'format' => ['currency', 'INR'],
                'format' => ['decimal', 2],
                'value' => function($model, $key, $index) {
                return isset($model->expense_amount)?$model->expense_amount:0;
                }
            ],
            [
                'attribute' => 'vat',
                'header' => '<div style="width:100px;"> VAT Amount</div>',
                //'format' => ['currency', 'INR'],
                'format' => ['decimal', 2],
                'value' => function($model, $key, $index) {
                
//                return isset($model->vat)?(number_format((float)($model->vat), 2, '.', '')):0;
              return isset($model->vat)?$model->vat:0;
                }
            ],
            
],

  // check the configuration for grid columns by clicking button above
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [   
                'columns' => [
                    ['content' => 'Outward VAT', 'options' => ['colspan' => 7, 'class' => 'hide', 'style' => 'font-size:18px']],
               ],],
             [
                'columns' => [
                    ['content' => '  &nbsp; <span style="font-weight:normal"> </span>', 'options' => ['colspan' => 1, 'class' => 'hide',]],
                    ['content' => 'Inward VAT Amount : <span style="font-weight:normal">' . $vat . '</span>', 'options' => ['colspan' => 2, 'class' => 'hide',]],
                    ['content' => 'Outward VAT Amount : <span style="font-weight:normal">' . $vatFromPettyCash . '</span>', 'options' => ['colspan' => 2, 'class' => 'hide',]],
                    ['content' => 'Total Payable VAT : <span style="font-weight:normal">' . $haveToPay . '</span>', 'options' => ['colspan' => 2, 'class' => 'hide',]],
                ],],
        ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => false, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '',
        ],
        'persistResize' => false,
        'exportConfig' => [GridView::EXCEL => [
                'filename' => 'Outward VAT',
            ],
           GridView::PDF => [
                'filename' => 'Outward VAT',
                'showHeader' => true,
                'showPageSummary' => false,
                'showFooter' => true,
                'showCaption' => true,
                
             
                'config' => [
                    //'cssInline' =>'table{direction:0}' ,
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                    //'contentBefore'=>'<div style="font-size:30px; text-align:center; text-decoration:underline;">List Of Clients</div>'
                ],
          ]]

      
]);
}
 
?>                            
              
             </div>
            </div>
            
           
            
            </div>
              </div>
</div></div>
<script>
function toggleDisplay() {
    var x = document.getElementById("toggleMe");
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}
</script>

<?= $this->registerJs("
   // alert('hhjh');
    $('#reportrange').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM.DD.YYYY',
        separator: ' to ',
        startDate: moment().subtract('days', 29),
        endDate: moment()
    }, function (start, end) {
        console.log('inside');
        $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        var sdate = start.format('YYYY-MM-DD');
        var edate = end.format('YYYY-MM-DD');
        var id = $('#reportrange').attr('data-id');
        var url = $('#reportrange').attr('data-url');
         var daterangeurl = $('#reportrange').attr('data-daterangeurl');


                        $.ajax({
                        type : 'POST',
                        url: daterangeurl,
                        data: {'sdate':sdate,'edate':edate},
                        success  : function() {
                        //   console.log(response);
                        //$('.request-data').html(response);
                        location.href = url;
                       
                        },
                        error : function(response){
                        console.log(response);
                        }
                        });
                        
                        
                    });


                    $('#reportrange span').html($('#selectedDRange').val());
                    $('table').addClass('kt-datatable__table');
    $('tr').addClass('kt-datatable__row');
    $('th').addClass('kt-datatable__cell kt-datatable__cell--sort');
    $('td').addClass('kt-datatable__cell');
", View::POS_READY); ?>
