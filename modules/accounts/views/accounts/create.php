<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

$type = ['Credit' => 'Credit', 'Debit' => 'Debit'];


?>

<?php
$form = ActiveForm::begin([
            'id' => 'active-form',
            'options' => [
                'enctype' => 'multipart/form-data'
            ],
        ])
?>
<div id='msg' class='msg'></div>
<div id="accounts-form">
    <div class="row">
  <legend class="text-info"><small>New Transaction</small></legend>
  <div class="row hide" id="error-message"><div class="col-md-12 warning"></div></div>
  <div class="col-md-2">
           <?=
            $form->field($model, 'date', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput()->label('Date')
            ?> 
        </div>
       
       
   <div class="col-md-5">
        <?=
        $form->field($model, 'description', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textInput([])->label('Description')
        ?>
    </div>
   <div class="col-md-3">
        <?=
        $form->field($model, 'reference_no', [
            'template' => "{label}\n
            <div class='col-md-12 col-xs-12'>
            {input}\n
             </div>",
            'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
        ])->textInput([])->label('Reference No')
        ?>
    </div>
    <div class="col-md-2">
           <?=
            $form->field($model, 'credit_debit', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->dropDownList($type,[
               // 'prompt' => 'Select',
                'class'=>'select from control',
            ])->label('Credit/Debit')
            ?> 
       </div>
  <div id ="credit">
       <div class="col-md-3">
           <?=
            $form->field($model, 'credit_amount',[
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput([])->label('Credit Amount')
            ?> 
        </div>
  </div>
<div id ="debit">
        <div class="col-md-3">
           <?=
            $form->field($model, 'debit_amount', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
       </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label','style'=>'text-align:left']
            ])->textInput([])->label('Debit Amount')
            ?> 
        </div>
  
   </div>
  
  <div>&nbsp;</div>
  <div>&nbsp;</div>
  <div>&nbsp;</div>
  <div>&nbsp;</div>
  
        <div class="col-md-12"><legend class="text-info"><small></small></legend></div>

        <div class="col-md-12"> 
            <div class="form-group">
                <div class="col-md-12 col-xs-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['id'=>'submit','class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                </div>
            </div>
        </div>
   
    <?php
    ActiveForm::end();
    ?>
</div>
 </div>
 

  
<script> 
$("form#active-form").submit(function(e) {
    e.preventDefault();
    event.stopPropagation();
     $("#accounts-description").removeClass("error-highlight");
     $("#accounts-credit_amount").removeClass("error-highlight");
     $("#accounts-debit_amount").removeClass("error-highlight");
     
   var formData = new FormData(this);
      if($('#accounts-description').val()==='') {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Description cannot be Blank</p>");
            $("#accounts-description").addClass("error-highlight");
            return false;
       }
      else if(($('#accounts-credit_amount').val()==='') && ($('#accounts-credit_debit').val()=== 'Credit')) {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Enter Credit Amount</p>");
            $("#accounts-credit_amount").addClass("error-highlight");
            return false;
       }
      else if(($('#accounts-debit_amount').val()==='') && ($('#accounts-credit_debit').val()==='Debit')) {
           //alert("Name cannot be Empty ");
            $("#error-message").removeClass("hide");
            $("#error-message").html("<p>Enter Debit Amount</p>");
            $("#accounts-debit_amount").addClass("error-highlight");
            return false;
       }
       else {
           $("#error-message").addClass("hide");
         
        }
    $.ajax({
        url: $('#active-form').attr('action'),
        type: 'POST',
        data: formData,
        success: function(){
                      $("#msg").html('Transaction Added');
                      $("#msg").show();
                      setTimeout(function() { $("#msg").fadeOut('slow'); }, 4000);
                      document.getElementById("active-form").reset();

            },
        cache: false,
        contentType: false,
        processData: false
    });
     
});
</script>


 
 <?= $this->registerJs("
    
        $('#accounts-date').datetimepicker({
            defaultDate: new Date(),
            format: 'DD-MM-YYYY'
        });
 ", View::POS_READY); ?>
<script> 
    $(document).ready(function() {
        var type = $('#accounts-credit_debit').val();
        
        if (type == 'Credit') {
                $('#credit').show();
                $('#debit').hide();
              }
        
       $('#accounts-credit_debit').on('change',function(){
           var type = $('#accounts-credit_debit').val();
             if (type == 'Credit') {
                $('#credit').show();
                $('#debit').hide();
              } 
              else if(type == 'Debit') {
                $('#credit').hide();
                $('#debit').show();
            }
        });
         });
</script>

