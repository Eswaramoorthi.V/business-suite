<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;

//$this->title = 'Bank';
$this->params['breadcrumbs'][] = 'Transaction';
//$this->params['title_icon'] = 'fa-credit-card';


$bankcredit = Yii::$app->db->createCommand("SELECT sum(credit_amount) FROM transaction  WHERE (trans_mode=1 AND payment_mode=1 )")->queryScalar();
$bankWithdrawn = Yii::$app->db->createCommand("SELECT sum(pettycash_amount) FROM transaction  WHERE (trans_mode=1 AND payment_mode=2 )")->queryScalar();
$bankExpense = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction  WHERE (trans_mode=1 AND payment_mode=3 )")->queryScalar();
$bankbalance1 = $bankcredit - ($bankWithdrawn + $bankExpense);
$bankbalance = number_format($bankbalance1,2, '.', ',');
//pr($bankbalance);
$cashcredit = Yii::$app->db->createCommand("SELECT sum(pettycash_amount) FROM transaction  WHERE (employee_id!='' and trans_mode=2 AND payment_mode=1 )")->queryScalar();
$bankwithdrawn=Yii::$app->db->createCommand("SELECT sum(pettycash_amount) FROM transaction  WHERE (employee_id!='' and trans_mode=1 AND payment_mode=2 )")->queryScalar();

$cashExpense = Yii::$app->db->createCommand("SELECT sum(expense_total_amount) FROM transaction  WHERE (employee_id!='' and trans_mode=2 AND payment_mode=3 )")->queryScalar();
$cashbalance1 =($bankwithdrawn+$cashcredit )- $cashExpense;
$cashbalance = number_format($cashbalance1,2, '.', ',');

//$previousCertified =  isset($previousCertified[0]['previousTotalAmount'])? number_format((float)$previousCertified[0]['previousTotalAmount'], 2, '.', ''):0;

$all_revenue = isset($revenueAmount[0]['revenueTotalAmount'])? number_format((float)$revenueAmount[0]['revenueTotalAmount'], 2, '.', ''):0;




$total_previousCertifiedAmount = isset($previousCertifiedAmount[0]['previousCertifiedTotalAmount']) ? number_format((float) $previousCertifiedAmount[0]['previousCertifiedTotalAmount'], 2, '.', '') : 0;

// $total_revenue =$all_revenue+ $total_previousCertifiedAmount;
$total_revenue =$all_revenue;

$total_credited=isset($creditAmount[0]['creditTotalAmount'])? number_format((float)$creditAmount[0]['creditTotalAmount'], 2, '.', ''):0; 


$Overall_Outstanding1=$total_revenue-$total_credited;
$Overall_Outstanding=number_format($Overall_Outstanding1,2, '.', ',');
//pr($Overall_Outstanding);

$expenses= isset($expenseAmount[0]['expenseTotalAmount'])? number_format((float)$expenseAmount[0]['expenseTotalAmount'], 2, '.', ''):0; 

$inwardvat= isset($inwardvatAmount[0]['inwardvatAmount'])? number_format((float)$inwardvatAmount[0]['inwardvatAmount'], 2, '.', ''):0; 
$outwardvat= isset($outwardvatAmount[0]['outwardvatAmount'])? number_format((float)$outwardvatAmount[0]['outwardvatAmount'], 2, '.', ''):0; 

$totalvat=number_format((float)($inwardvat-$outwardvat), 2, '.', '');
//$profit_loss1=$total_revenue-$expenses;
//$profit_loss1=$total_revenue-($expenses+$totalvat);
$profit_loss1=$total_revenue-($expenses);
$profit_loss=number_format($profit_loss1,2, '.', ',');


?>

<!-- Custom Theme Style Start Here -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/style.bundle.css">
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/app-table.css">
<!-- Custom Theme Style End Here -->
<style type="text/css">
    table.kt-datatable__table thead th:nth-child(1){width: 3% !important;}
    table.kt-datatable__table thead th:nth-child(2){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(3){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(4){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(5){width: 20% !important;}
    table.kt-datatable__table thead th:nth-child(6){width: auto !important;}
    .btn.dropdown-toggle:after, .nav-link.dropdown-toggle:after{display: none;}
    .kt-datatable--brand.kt-datatable--scroll .grid-view.hide-resize{margin-bottom: 0;}
    .panel.panel-primary{border-top: unset;}
    .custom-margin-style{margin-right: 0;}
    .panel.panel-primary{    position: relative;}
    .panel-primary>.panel-heading .pull-right{    float: unset !important;}
    .panel-primary>.panel-heading{padding: 0;border:unset;}
    .panel.panel-default{background-color: #fff;}
    table.kt-datatable__table thead th{border:1px solid #E5E5E5 !important;}
    .panel-default .panel-heading, .panel-primary .panel-heading, .panel-success .panel-heading, .panel-info .panel-heading, .panel-warning .panel-heading, .panel-danger .panel-heading{background: #ecf0ff !important; border-color: #bcc7ef !important; border-bottom: 2px solid #bcc7ef !important;}
</style>
<?php
Modal::begin([
    'header' => '<h4>Add Transaction</h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Transaction</h4>',
    'id' => 'update',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalupdate'><div class='loader'></div></div>";
Modal::end();
?>
<style>
    .revenue
    {
        background-color: #ab4293;
    }

</style>


<div class="row">
     <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-bank" aria-hidden="true"></i> Transaction
    </div>
    
</div>
<div class="row">

    <div class="col-md-3"></div>
    <div class="col-md-2"></div>
    
</div>
    <!-- START PROJECTS BLOCK -->
    <div class="row">
        <div class="col-md-8">
         <!-- START SALES BLOCK -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Activity</h3>
                                        <!-- <span> activity $dataRangeby period you selected</span> -->
                                        <span> activity data by period you selected</span>
                                    </div>                                     
                                    <ul class="panel-controls panel-controls-title">                                        
                                        <li>
                                            <div id="reportrange" data-daterangeurl="<?php echo Url::toRoute("transaction"); ?>" data-url="<?php echo Url::toRoute("transaction"); ?>" data-id=""  class="dtrange col-md-12" >
        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
        <span> </span> <b class="caret"></b>
        <input type="hidden" value="<?php echo $dataRange; ?>" id="selectedDRange">
    </div>                                    
                                        </li>                                
                                        <li><a href="#" class="panel-fullscreen rounded"><span class="fa fa-expand"></span></a></li>
                                    </ul>                                    
                                    
                                </div>
                                <div class="panel-body">                                    
                                    <div class="row stacked">
                                        <div class="col-md-12">                                            
                                            <div class="progress-list">                                               
                                                <div class="pull-left"><strong>Total Revenue</strong></div>
                                                <div class="pull-right"><?php echo!empty($total_revenue) ? $total_revenue : '0'; ?></div>
                                                <div class="progress progress-small progress-striped active">
                                                    <div class="revenue" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:  <?php if($total_revenue==0){echo 0;}else {echo 100;}?>%;color:mediumvioletred">  <?php if($total_revenue==0){echo 0;}else{ echo 100;}?>%</div>
                                                </div>
                                            </div>
                                            <div class="progress-list">                                               
                                                <div class="pull-left"><strong>Total Credited</strong></div>
                                                <div class="pull-right"><?php echo!empty($total_credited) ? $total_credited : '0'; ?></div>                                                
                                                <div class="progress progress-small progress-striped active">
                                                    <?php if($total_revenue!=0) {$creditpecentage=($total_credited/$total_revenue)*100;} ?>
                                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo isset($creditpecentage)?$creditpecentage:0;?>%;"> <?php echo isset($creditpecentage)?$creditpecentage:0;?>%</div>
                                                </div>
                                            </div>
                                            <div class="progress-list">                                               
                                                <div class="pull-left"><strong class="text-danger">Outstanding</strong></div>
                                                <div class="pull-right"><?php echo!empty($Overall_Outstanding) ? $Overall_Outstanding : '0.00'; ?></div>                                                
                                                <div class="progress progress-small progress-striped active">
                                                     <?php if($total_revenue!=0) {$outstandingpercentage=($Overall_Outstanding1/$total_revenue)*100;} ?>
                                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo isset($outstandingpercentage)?$outstandingpercentage:0;?>%;"> <?php echo isset($outstandingpercentage)?$outstandingpercentage:0;?>%</div>
                                                </div>
                                            </div>
                                            <div class="progress-list">                                               
                                                <div class="pull-left"><strong class="text-warning">Expenses</strong></div>
                                                <div class="pull-right"><?php echo!empty($expenses) ? $expenses : '0.00'; ?></div>                                                
                                                <div class="progress progress-small progress-striped active">
                                                     <?php if($total_revenue!=0) {$expensegpercentage=($expenses/$total_revenue)*100;} ?>
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo isset($expensegpercentage)?$expensegpercentage:0;?>%;"> <?php echo isset($expensegpercentage)?$expensegpercentage:0;?>%</div>
                                                </div>
                                            </div>
                                            <div class="progress-list">                                               
                                                <div class="pull-left"><strong class="text-warning">VAT</strong></div>
                                                <div class="pull-right"><?php echo!empty($totalvat) ? $totalvat : '0.00'; ?></div>                                                
                                                <div class="progress progress-small progress-striped active">
                                                     <?php if($total_revenue!=0) {$totalvatpercentage=($totalvat/$total_revenue)*100;} ?>
                                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo isset($totalvatpercentage)?$totalvatpercentage:0;?>%;"> <?php echo isset($totalvatpercentage)?$totalvatpercentage:0;?>%</div>
                                                </div>
                                            </div>
                                             <div class="progress-list">                                               
                                                <div class="pull-left" id="test"><strong class="text-warning">Profit</strong></div>
                                                <div class="pull-right"><?php echo $profit_loss;   ?></div>                                                
                                                <div class="progress progress-small progress-striped active">
                                                     <?php if($expenses!=0) { $profitpercentage=$profit_loss1/$expenses*100;
                                                     $per=abs($profitpercentage);
                                                     }  ?>
                                                    <div class="progress-bar progress-bar-warning" id="profit" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo isset($per)?$per:0;?>%;"> <?php echo isset($per)?$per:0;?>%</div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                      
                                    </div>                                    
                                </div>
                            </div>
        </div>
        <div class="col-md-4">
            <div class="col-md-12">
      <div class="widget widget-default widget-item-icon"  >
                                <div class="widget-item-left">
                                    <span class="fa fa-credit-card"></span>
                                </div>                             
                                <div class="widget-data">
                                    <div class="widget-int num-count">&nbsp;</div>
                                    <div class="widget-title">Bank Balance</div>
                                    <div class="widget-subtitle"><?php echo!empty($bankbalance) ? $bankbalance : '0'; ?></div>
                                </div>      
                               
                            </div>         
</div>
    <div class="col-md-12">
     <div class="widget widget-default widget-item-icon"  >
                                <div class="widget-item-left">
                                    <span class="fa fa-money"></span>
                                </div>                             
                                <div class="widget-data">
                                    <div class="widget-int num-count">&nbsp;</div>
                                    <div class="widget-title">Cash Balance</div>
                                    <div class="widget-subtitle"><?php echo!empty($cashbalance) ? $cashbalance : '0'; ?></div>
                                </div>      
                               
                            </div>   
                  
</div>
             
        
          <div class="col-md-12 text-right add-label">
        <?= Html::button('Add Transaction', ['value' => Url::to('createtransaction'), 'class' => 'btn btn-success custom-main-btn', 'id' => 'modalButton']) ?>
    </div>  
        </div>
                          
        
        
        

                            </div>
                          
       </div>     
                      
    
</div>

<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
//            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
           [
                'attribute' => '',
                'header' => '<div style="width:70px;">Date</div>',
                'value' => function($model, $key, $index) {
               return Yii::$app->formatter->asDate($model->date, 'php:d-m-Y');
           },
                //'format' => 'date',
                 'vAlign' => 'middle',
                'filterType' => GridView::FILTER_DATE,
                'filterWidgetOptions' => ([
                'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
            ]
                ])
            ],
            [
                'attribute' => 'particulars',
                'vAlign' => 'middle',
                'header' => '<div style="width:180px;">Particulars</div>',
                 'value' => function($model) {
                return !empty($model->particulars) ?$model->particulars:"-";
                },
            ],
                
                         
             [
                'attribute' => 'client_id',
                'vAlign' => 'middle',
                'header' => '<div style="width:180px;">Client/Supplier/SubCont</div>',
                 
                 'value' => function($model) {
                   
                     if ($model->trans_mode==1 and $model->payment_mode==1){
                         
                           $client_name=!empty($model->clientName['company_name'])?$model->clientName['company_name']:($model->clientName['coustomer_fname'] . " " . $model->clientName['coustomer_lname']);
                       
                 return !empty($client_name) ?$client_name:'-';
                 
                     }
                    else if ($model->type==3){
                       
                           $subcontractor_Name= !empty($model->subcontractorName['company_name'])?$model->subcontractorName['company_name']:($model->subcontractorName['coustomer_fname'] . " " . $model->subcontractorName['coustomer_lname']);
                       
                 return !empty($subcontractor_Name) ?$subcontractor_Name:'-';
                 
                     }
                      else if ($model->type==2){
                         
                           $supplier_Name=  !empty($model->supplierName['company_name'])?$model->supplierName['company_name']:($model->supplierName['coustomer_fname'] . " " . $model->supplierName['coustomer_lname']) ;
                       
                 return !empty($supplier_Name) ?$supplier_Name:'-';
                 
                     }

                     
                     else{
                        return '-'; 
                     }
                     
                     },


            ],    
            
            
            [
                'attribute' => 'project_id',
                'vAlign' => 'middle',
                'header' => '<div style="width:150px;">Project Name</div>',
                'value' => function($model) {
                   return !empty($model->projectName['project_name'])?($model->projectName['project_name']):'-'; },
            ],
                            
      
            [
                'attribute' => '',
                'hAlign'=>'right',
                'header' => '<div style="width:60px;">Credit</div>',
                'format' => ['decimal', 2],
                 'value' => function($model) {
                       if ($model->trans_mode==1 and $model->payment_mode==1){
                           $bank_credit=$model->credit_amount;
                       }
                    return !empty($bank_credit) ?$bank_credit:"";
                }
            ],


            [
                'attribute' => '',
                'hAlign'=>'right',
                'header' => '<div style="width:60px;">Debit</div>',
                'format' => ['decimal', 2],
                 'value' => function($model) {
                 if ($model->trans_mode==1 and $model->payment_mode==2){
                           $bank_withdrawn=$model->pettycash_amount;
                       }
                
                    return !empty($bank_withdrawn) ?$bank_withdrawn:'';
                }
            ],
                    [
                'attribute' => '',
                'hAlign'=>'right',
                'header' => '<div style="width:60px;">Expense</div>',
                'format' => ['decimal', 2],
                 'value' => function($model) {
                     if ($model->trans_mode==1 and $model->payment_mode==3){
                           $bank_expense=$model->expense_total_amount;
                       }
                
                    return !empty($bank_expense) ?$bank_expense:'';
                }
            ],
            [
                'attribute' => '',
                'hAlign'=>'right',
                'header' => '<div style="width:60px;">Credit</div>',
                'format' => ['decimal', 2],
                 'value' => function($model) {
                    if ($model->trans_mode==2 and $model->payment_mode==1){
                           $cash_credit=$model->pettycash_amount;
                       
                    return !empty($cash_credit) ?$cash_credit:'';
                    }
                    elseif($model->trans_mode==1 and $model->payment_mode==2){
                        $cash_credit=$model->pettycash_amount;
                       
                    return !empty($cash_credit) ?$cash_credit:'';
                    }
                    else{
                        return 0; 
                    }
                }
            ],
                     
                     [
                'attribute' => '',
                'hAlign'=>'right',
                'header' => '<div style="width:60px;">Expense</div>',
                'format' => ['decimal', 2],
                 'value' => function($model) {
                   if ($model->trans_mode==2 and $model->payment_mode==3){
                           $card_expense=$model->expense_total_amount;
                       }
                
                    return !empty($card_expense) ?$card_expense:'';
                }
            ],
                    [
                'attribute' => '',
                 'hAlign'=>'right',
                'header' => '<div style="width:60px;">Card</div>',
                'format' => ['decimal', 2],
                 'value' => function($model) {
                   if ($model->trans_mode==3 and $model->payment_mode==3){
                           $card_expense=$model->expense_total_amount;
                       }
                
                    return !empty($card_expense) ?$card_expense:'';
                }
            ],
           [
                'attribute' => '',
                 'hAlign'=>'right',
                'header' => '<div style="width:60px;">Cash</div>',
                'format' => ['decimal', 2],
                 'value' => function($model) {
                   if ($model->trans_mode==5 and $model->payment_mode==3){
                           $card_expense=$model->expense_total_amount;
                       }
                
                    return !empty($card_expense) ?$card_expense:'';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:55px;text-align:center;','class'=>'skip-export'],
                'headerOptions' =>['class'=>'skip-export'],
                'header' => "Actions",
                'template' => ' {updatetransaction}{deletetransaction}',
                'buttons' => [

                    'updatetransaction' => function($url) {
                        return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'update', 'title' => 'Edit']
                        );
                    },
                    'deletetransaction' => function ($url) {
                        return Html::a(
                                        '<span class="glyphicon glyphicon-trash icons"></span>', $url, [
                                    'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],
                                        ]
                        );
                    },
                ],
            ],
        ],
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => 'Transaction History', 'options' => ['colspan' => 7, 'class' => 'hide', 'style' => 'font-size:18px']],
                ],],
            [ 'columns' => [
                    ['content' => '', 'options' => ['colspan' => 4, 'class' => '', 'style' => 'font-size:18px']],
                    ['content' => 'Bank', 'options' => ['colspan' => 3, 'class' => 'text-center', 'style' => 'font-size:18px']],
                    ['content' => 'Petty Cash', 'options' => ['colspan' => 2, 'class' => 'text-center', 'style' => 'font-size:18px']],  
                    ['content' => 'Loan', 'options' => ['colspan' => 2, 'class' => 'text-center', 'style' => 'font-size:18px']],
                    ['content' => '', 'options' => ['colspan' => 1, 'class' => '', 'style' => 'font-size:18px']],
                   
                ],],

        ],
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        'filterRowOptions' => ['class' => 'kartik-sheet-style'],
        'pjax' => false, // pjax is set to always true for this demo
        // set your toolbar
        'toolbar' => [
            '{export}',
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true,
            'showConfirmAlert' => false
        ],
        // parameters from the demo form
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        //'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '',
        ],
        'persistResize' => false,
        'exportConfig' => [GridView::EXCEL => [
                'filename' => 'Transaction History',
            ],
            GridView::PDF => [
                'filename' => 'Transaction History',
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'config' => [
                    'methods' => [
                        'SetHeader' => [
                            ['odd' => '', 'even' => '']
                        ],
                        'SetFooter' => [
                            ['odd' => '', 'even' => '']
                        ],
                    ],
                ],
            ]]
    ]);
}
?>
<?= $this->registerJs("
    $(function(){
         $('table').addClass('kt-datatable__table');
    $('tr').addClass('kt-datatable__row');
    $('th').addClass('kt-datatable__cell kt-datatable__cell--sort');
    $('td').addClass('kt-datatable__cell');
$('#modalButton').click(function() {
    $('#modal').modal('show')
    .find('#modalContent')
    .load($(this).attr('value'));
   
});
});

 ", View::POS_READY); ?>

<?=
$this->registerJs(
        "$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.update').click(function(e){
       e.preventDefault();      
       $('#update').modal('show')
                  .find('#modalupdate')
                  .load($(this).attr('href'));  
   });
});
", View::POS_READY);
?>
<?php 
$js = <<< JS
$(document).ready(function() {

  var valuestart ='$profit_loss1';
 
	if(valuestart<0){

	document.getElementById('profit').style.color = 'red';
	$("#test").html("Loss");
        document.getElementById("test").className = "text-danger";
       document.getElementById("profit").className = "progress-bar progress-bar-danger";
}

		
 
});

JS;
$this->registerJs($js);
?>

<?= $this->registerJs("
   // alert('hhjh');
    $('#reportrange').daterangepicker({
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'left',
        buttonClasses: ['btn btn-default'],
        applyClass: 'btn-small btn-primary',
        cancelClass: 'btn-small',
        format: 'MM.DD.YYYY',
        separator: ' to ',
        startDate: moment().subtract('days', 29),
        endDate: moment()
    }, function (start, end) {
        console.log('inside');
        $('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'));
        var sdate = start.format('YYYY-MM-DD'); 
        var edate = end.format('YYYY-MM-DD');
        var id = $('#reportrange').attr('data-id');
        var url = $('#reportrange').attr('data-url');
         var daterangeurl = $('#reportrange').attr('data-daterangeurl');
   

                        $.ajax({
                        type : 'POST',
                        url: url,
                        data: {'sdate':sdate,'edate':edate},
                        success  : function() {
                        location.href = url + '?sdate=' + sdate +'&edate='+edate;
                       
                        },
                        error : function(response){
                        console.log(response);
                        }
                        });
                        
                        
                    });

                   $('#reportrange span').html($('#selectedDRange').val());
", View::POS_READY); ?>