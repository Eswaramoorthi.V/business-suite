<?php

namespace app\modules\role;

class role extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\role\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
