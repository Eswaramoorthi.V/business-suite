<?php

namespace app\modules\role\controllers;

use Yii;
use app\models\Customer;
use app\models\Search\RoleSearch;

use app\modules\role\models\AuthItemSearch;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\mpdf\Pdf;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\controllers\BaseController;
use app\modules\estimate\models\Settings;
use app\models\SettingsTree;
use app\models\EstimateDetails;
use app\models\EstimateMaster;
use app\models\MaterialMaster;
use app\models\MaterialUnitCost;
use app\models\LabourMaster;
use app\models\LabourUnitCost;
use app\modules\estimate\models\Equipment;
use app\models\ProjectEstimateMaster;
use app\models\ProjectEstimate;
use app\models\Tree;
use app\models\AuthItem;
use app\components\MenuHelper;
/**
 * ProjectController implements the CRUD actions for Project model.
 */
class RoleController extends BaseController
{
    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $route=$this->route;
        $actionList = MenuHelper::getActionHelper($route);
      
        $visibleList = MenuHelper::getActionVisibleHelper($route);
         $searchModel = new RoleSearch();
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->pagination = ['PageSize'=>10];
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'actionList'=>$actionList,
                    'visibleList'=>$visibleList
        ]);
       
 
    }

    public function actionCreate()
    {
        $model = new \app\models\AuthItem();
        if ($model->load(Yii::$app->request->post())) {
            // pr(Yii::$app->request->post());
       $request=Yii::$app->request->post();
    //    pr($request);
       $model->name = $request['AuthItem']['name'];
       $model->type =1;
       $model->description = $request['AuthItem']['description'];
            if(!$model->save()){
                pr($model->getErrors());
            }
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)

    {

        $model = $this->findModel($id);
// pr($model);
        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();
            $model->name = $request['AuthItem']['name'];
            $model->description = $request['AuthItem']['description'];
            if(!$model->save()){
                pr($model->getErrors());
            }
            return $this->redirect(['index']);
        } else {

            return $this->renderAjax('update', [

                'model' => $model,

            ]);

        }

    }
    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
   
    public function actionDeletes($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($name)
    {
        if (($model = AuthItem::findOne($name)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDetailview() {
        $id = $_GET['id'];
        return $this->render('detail_view', [
                    'model' => $this->findModel($id),
        ]);
    }

    
   

}