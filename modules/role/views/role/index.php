

<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;

$this->params['breadcrumbs'][] = 'Role';
?>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-wrench" aria-hidden="true"></i> User Role
    </div>
    <div class="col-md-6 text-right add-label" style="padding-top: 12px">
       <?= Html::button('Add', ['value' => Url::to('create'), 'class' => 'btn btn-success', 'id' => 'addModalButton']) ?>
    </div>
</div>
<?php
Modal::begin([
    'header' => '<h4>Add Role</h4>',
    'id' => 'addModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalAddContent'><div class='loader'></div></div>";
Modal::end();
?>
<?php
Modal::begin([
    'header' => '<h4>Update Role</h4>',
    'id' => 'updateModal',
    'size' => 'modal-lg',
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);

echo "<div id ='modalUpdateContent'><div class='loader'></div></div>";
Modal::end();
?>


<?php
if ($dataProvider) {
    echo GridView::widget([
        //'id' => 'kv-grid-demo',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' =>false,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn', 'mergeHeader' => false,],
            
            [
                'attribute' => 'name',
               'header' => '<div style="width:120px;">Role</div>',
                'value' => function($model, $key, $index) {
                   return !empty($model->name)?$model->name:'-';
                            
                },
                'vAlign' => 'middle',
                'format' => 'raw',
                //'width' => '150px',
                'noWrap' => true
            ],
            [
                'attribute' => 'description',
                'header' => '<div style="width:120px;">Description</div>',
                'value' => function($model, $key, $index) {
                    return !empty($model->description)?$model->description:'-';
                },
                'vAlign' => 'middle',
                'format' => 'raw',
               // 'width' => '150px',
                'noWrap' => true
          ],
         
            [
                'class' => 'yii\grid\ActionColumn',
                'contentOptions' => ['style' => 'width:80px;text-align:center','class'=>'skip-export'],
                'headerOptions' =>['class'=>'skip-export'],
                'header' => "Action",
                // 'template' => ' {update}{deletes}',
                'template'=>$actionList,
                'buttons' => [
                  
                    'update' => function($url) {
                        return Html::a('<span class="glyphicon glyphicon-edit icons"></span>', $url, ['class' => 'updateModalButton', 'title' => 'Edit']);
                    },
                    'deletes' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash icons"></span>', $url, [ 'title' => 'Delete','data' => ['confirm' => 'Are you sure want to delete?'],]);
                    },
                ],
            ],
        ],
    ]);
}
?>                                   
<?= $this->registerJs("
$(function(){
$('#addModalButton').click(function() {
    $('#addModal').modal('show')
    .find('#modalAddContent')
    .load($(this).attr('value'));
});
});
", View::POS_READY); ?>

<?=
$this->registerJs(
"$(document).on('ready pjax:success', function() {  // 'pjax:success' use if you have used pjax
    $('.updateModalButton').click(function(e){
        e.preventDefault();      
        $('#updateModal').modal('show')
        .find('#modalUpdateContent')
        .load($(this).attr('href'));  
    });
});
", View::POS_READY);
?>
