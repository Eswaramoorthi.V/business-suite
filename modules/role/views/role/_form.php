<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\web\View;
use kartik\date\DatePicker;

?>
<div class="role-form form-vertical">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
    <div class="col-md-6">
        <?=
            $form->field($model, 'name', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textInput(['placeholder' => " Please enter role"])->label(' Role')
            ?>
             <h5 class="error-msg2 hide text-danger"> </h5>
        </div>
        <div class="col-md-6">
            <?=
            $form->field($model, 'description', [
                'template' => "{label}\n
      <div class='col-md-12 col-xs-12'>
            {input}\n
            {hint}\n
            {error}
      </div>",
                'labelOptions' => ['class' => 'col-md-12 col-xs-12 control-label', 'style' => 'color:black'],
                    // 'inputOptions' => ['class' => 'col-md-12 col-xs-12 text-line'],
            ])->textarea(['placeholder' => " Please enter description"])->label('Description')
            ?>
        </div>
    </div>
    <br>
     <div class="row">
                <div class="form-group">
                    <label class="col-md-6 col-xs-12 control-label" for=""></label>
                    <div class="col-md-6 col-xs-12">
                        <?= Html::submitButton($model->isNewRecord ? 'Submit' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
                        <?= Html::resetButton('Cancel', ['class' => 'reset btn btn-success pull-right ' , 'data-dismiss' => 'modal']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>

  