<?php

namespace app\modules\home;

class home extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\home\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
