<?php
use yii\widgets\ListView;
?>

<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th width="40%">Type</th>
            <th width="20%">Activity</th>
        </tr>
        </thead>
        <tbody>
        <?= ListView::widget([
            'id'=>'jobgridview',
            'layout' => "{items}\n{pager}",
            'dataProvider' => $faultSummary,
            'itemView'=>'_myView',


        ]) ?>

        </tbody>
    </table>
</div>







  
