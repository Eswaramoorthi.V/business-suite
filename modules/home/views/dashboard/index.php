<?php
use app\widgets\charts\ChartWidget;
use yii\helpers\Url;
use yii\web\JsExpression;
?>

<!-- Custom Theme Style Start Here -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/style.bundle.css" />
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/app-table.css" />
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <?php
        echo ChartWidget::widget(
            [
                'title' => 'Work Order Monitoring',
                'icon' => 'fa-battery-empty',
                'chartClass' => 'dail2',
                'chartType' => 'bar'
            ]
        );
        ?>

    </div>
    <div class="row">
        <!-- START KNOB SLIDER -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Work Order Status</strong></h3>
            </div>
            <div class="panel-body dashboard">
                <div class="row">
                    <?php
                    foreach ($dountChartList as $dounchart) {
                        echo ChartWidget::widget(
                            [
                                'title' => $dounchart['chartTitle'],
                                'icon' => $dounchart['chartIcon'],
                                'chartClass' => $dounchart['chartClass'],
                                'chartType' => $dounchart['chartType'],
                                'chartName'=> $dounchart['chartName'],
                                'chartColor'=> $dounchart['chartColor'],
                            ]
                        );
                    }
                    ?>
                </div>


            </div>
        </div>
        <!-- END KNOB SLIDER -->

    </div>
    
         <div class="row">
        <!-- START KNOB SLIDER -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><strong>Schedule</strong></h3>
            </div>
            <div class="panel-body dashboard">
        <div class="row">
            <div class="col-md-12">
                
               
                <?= \app\widgets\yii2fullcalendar\yii2fullcalendar::widget(array(
                    'ajaxEvents' => Url::toRoute(['/schedule/jsoncalendar']),
                    'id' => 'calendar'
                ));
                ?>
            </div>
        </div>
                
                 </div>
        </div>
        <!-- END KNOB SLIDER -->

    </div>

</div>
<div class="hide" id="faulturl" data-url="<?= Url::toRoute(["index2"]); ?>"></div>

<?php


//pr($chartData);

$this->registerJsFile('@web/lib/js/demo_dashboard.js',['depends' => 'yii\web\JqueryAsset']);
?>



