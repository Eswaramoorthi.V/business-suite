<?php

use app\models\Services;
use app\models\Products;
use app\models\PettyCash;
use app\models\SubContractor;
use app\models\InvoiceMaster;
use app\components\DashboardHelper;

$homeUrl =Yii::$app->params['domain_url'];
$dateVariable      = strtotime(date('Y-m-d'));//your date variable goes here
$before_29_days = date('Y-m-d', strtotime('-29 days', $dateVariable));
        $startDate = isset($_GET['startDate'])?$_GET['startDate']:$before_29_days;


       
        $endDate = isset($_GET['endDate'])?$_GET['endDate']:date('Y-m-d');



        $previousCertifiedAmount = InvoiceMaster::find()
            ->select('sum(previous_certified_amount) as previousCertifiedTotalAmount')
            ->where(['between', 'invoice_date', $startDate, $endDate])
            ->asArray()->all();

        $revenueAmount = InvoiceMaster::find()
                            ->select('sum(total_amount) as revenueTotalAmount')
                            ->where(['between', 'invoice_date', $startDate, $endDate])
                            ->asArray()->all();

         $creditAmount = \app\models\InvoicePayment::find()
                            ->select('sum(paid_amount) as creditTotalAmount')
                            ->where(['between', 'payment_date', $startDate, $endDate])
                            ->asArray()->all();

$total_previousCertifiedAmount = isset($previousCertifiedAmount[0]['previousCertifiedTotalAmount']) ? number_format((float) $previousCertifiedAmount[0]['previousCertifiedTotalAmount'], 2, '.', '') : 0;
$total_revenueAmount = isset($revenueAmount[0]['revenueTotalAmount']) ? number_format((float) $revenueAmount[0]['revenueTotalAmount'], 2, '.', '') : 0;

// $total_revenueAmount = $total_previousCertifiedAmount + $total_revenueAmount; //
$total_revenueAmount = $total_revenueAmount;

$total_revenue = isset($total_revenueAmount) ? number_format((float) $total_revenueAmount, 2, '.', '') : 0;

$total_credited = isset($creditAmount[0]['creditTotalAmount']) ? number_format((float) $creditAmount[0]['creditTotalAmount'], 2, '.', '') : 0;
      

        $Overall_Outstanding1 = $total_revenue - $total_credited;
        $Overall_Outstanding = number_format($Overall_Outstanding1, 2, '.', '');
        //pr($Overall_Outstanding);
        $payment = app\models\InvoicePayment::find()->asArray()->all();
        $invoice=[];
        foreach ($payment as $pay){
            $invoice[]=$pay['invoice_id'];
          
        }
         //pr1($invoice );
        foreach($invoice as $id){
           
        $overdueamount=InvoiceMaster::find()
                            ->select('sum(total_amount) as overdueAmount')
                            ->where(['<=', 'due_date', $endDate])
                            ->andwhere(['!= ','id',$id])
                            ->asArray()->all();
        }
        // pr($overdueamount);


        $q1 = \app\models\Transaction::find()->select("sum(expense_total_amount) as servicetotalAmount")->where(['trans_mode'=>4])->andwhere('date > :startdate AND date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,]);
        $expenseInfo1 = $q1->asArray()->all();
        $serviceexpenseInfobefore = isset($expenseInfo1[0]['servicetotalAmount']) ? $expenseInfo1[0]['servicetotalAmount'] : 0;
        $serviceexpenseInfo = number_format((float) $serviceexpenseInfobefore, 2, '.', '');
//        pr($serviceexpenseInfo);
        $q2 = \app\models\Transaction::find()->select("sum(expense_total_amount) as materialtotalAmount")->where(['type'=>'2'])->andwhere(['!=','trans_mode',3])->andWhere('date > :startdate AND date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,]);
        $expenseInfo2 = $q2->asArray()->all();
        $materialexpenseInfobefore = isset($expenseInfo2[0]['materialtotalAmount']) ? $expenseInfo2[0]['materialtotalAmount'] : 0;
        $materialexpenseInfo = number_format((float) $materialexpenseInfobefore, 2, '.', '');

        
        $q2a = \app\models\Transaction::find()->select("sum(expense_total_amount) as othersmaterialtotalAmount")->where(['type'=>'1'])->andwhere(['!=','trans_mode',3])->andWhere('date > :startdate AND date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,]);
        $othersexpenseInfo2 = $q2a->asArray()->all();
        $othersmaterialexpenseInfobefore = isset($othersexpenseInfo2[0]['othersmaterialtotalAmount']) ? $othersexpenseInfo2[0]['othersmaterialtotalAmount'] : 0;
        $othersmaterialexpenseInfo = number_format((float) $othersmaterialexpenseInfobefore, 2, '.', '');
        $others=$othersmaterialexpenseInfo+$serviceexpenseInfo;
        
        $q3 = \app\models\Transaction::find()->select("sum(expense_total_amount) as contractortotalAmount")->where(['type'=>'3'])->andwhere(['!=','trans_mode',3])->andwhere('date > :startdate AND date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,]);
        $expenseInfo3 = $q3->asArray()->all();
        $contractorexpenseInfobefore = isset($expenseInfo3[0]['contractortotalAmount']) ? $expenseInfo3[0]['contractortotalAmount'] : 0;
        $contractorexpenseInfo = number_format((float) $contractorexpenseInfobefore, 2, '.', '');
        
        $q4a = InvoiceMaster::find()->select("sum(previous_certified_amount) as previousCertifiedTotalAmount")->where('invoice_date >= :startdate AND invoice_date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,]);
        $invoiceInfoA = $q4a->asArray()->all();

        $q4b = InvoiceMaster::find()->select("sum(total_amount) as totalAmount")->where('invoice_date >= :startdate AND invoice_date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,]);
        $invoiceInfoB = $q4b->asArray()->all();

$invoicePre = isset($invoiceInfoA[0]['previousCertifiedTotalAmount']) ? number_format((float) $invoiceInfoA[0]['previousCertifiedTotalAmount'], 2, '.', '') : 0;
$invoice = isset($invoiceInfoB[0]['totalAmount']) ? number_format((float) $invoiceInfoB[0]['totalAmount'], 2, '.', '') : 0;

// $invoiceCal = $invoicePre + $invoice ; //
$invoiceCal = $invoice ;

$invoiceGauge = isset($invoiceCal) ? number_format((float) $invoiceCal, 2, '.', '') : 0;

        $expenseinfo = $serviceexpenseInfo+ $materialexpenseInfo + $othersmaterialexpenseInfo + $contractorexpenseInfo;
        $expense = isset($expenseinfo) ? number_format((float) $expenseinfo, 2, '.', '') : 0;
        $profitbefore = $invoice - $expense;
        $profit = number_format((float) $profitbefore, 2, '.', '');
        $negativeValuebefore = ($profit > 0) ? 0 : abs($profit);
        $negativeValue= number_format((float) $negativeValuebefore, 2, '.', '');
        //pr($negativeValue);
        $salaryservice = \app\models\Transaction::find()->where(['trans_mode'=>4])->andwhere(['!=','trans_mode',3])->andwhere('date > :startdate AND date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,])->asArray()->all();
      
        $salaryContainer = [];
        foreach ($salaryservice as $salaryData) {
            $salarydateValue = strtotime($salaryData['date']);
            $salmonth = date('M', $salarydateValue);
            $salarytemp[$salmonth][] = $salaryData['expense_total_amount'];
            $salaryContainer = $salarytemp;
        }
        $salaryTotamount = DashboardHelper::monthDataPreparation($salaryContainer);
       // pr($salaryTotamount);

        $otherservice = \app\models\Transaction::find()->where(['type'=>1])->andwhere(['!=','trans_mode',3])->andwhere('date > :startdate AND date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,])->asArray()->all();
       
        $serviceContainer = [];
        foreach ($otherservice as $serviceData) {
            $servdateValue = strtotime($serviceData['date']);
            $servmonth = date('M', $servdateValue);
            $servtemp[$servmonth][] = $serviceData['expense_total_amount'];
            $serviceContainer = $servtemp;
        }
        $serviceTotamount = DashboardHelper::monthDataPreparation($serviceContainer);
      
        $product =\app\models\Transaction::find()->where(['type'=>2])->andwhere(['!=','trans_mode',3])->andWhere('date > :startdate AND date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,])->asArray()->all();
        $productContainer = [];
        foreach ($product as $productData) {
            $productdateValue = strtotime($productData['date']);
            $productmonth = date('M', $productdateValue);
            $producttemp[$productmonth][] = $productData['expense_total_amount'];
            $productContainer = $producttemp;
        }
        $productTotamount = DashboardHelper::monthDataPreparation($productContainer);
   

        $contract = \app\models\Transaction::find()->where(['type'=>3])->andwhere(['!=','trans_mode',3])->andwhere('date > :startdate AND date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,])->asArray()->all();
        $contractContainer = [];
        foreach ($contract as $contractData) {
            $contractdateValue = strtotime($contractData['date']);
            $contractmonth = date('M', $contractdateValue);
            $contracttemp[$contractmonth][] = $contractData['expense_total_amount'];
            $contractContainer = $contracttemp;
        }
        $contractTotamount = DashboardHelper::monthDataPreparation($contractContainer);
     
        $finalContainer = [$salaryTotamount,$serviceTotamount, $productTotamount, $contractTotamount];
        $expenseTotalamount = DashboardHelper::monthDataPreparation($finalContainer, $total = true);



$invMaster =InvoiceMaster::find()
   // ->select('sum(total_amount) as revenueTotalAmount')
    ->where(['between', 'invoice_date', $startDate, $endDate])
    ->asArray()->all();
       // $invMaster = InvoiceMaster::find()->where('invoice_date >= :startdate AND invoice_date <= :enddate', [':startdate' => $startDate, ':enddate' => $endDate,])->asArray()->all();
        $monthContainer = [];
        foreach ($invMaster as $invMasterData) {

            $dateValue = strtotime($invMasterData['invoice_date']);
            $month = date('M', $dateValue);
            // $temp[$month][] = $invMasterData['total_amount']+$invMasterData['previous_certified_amount']; //
            $temp[$month][] = $invMasterData['total_amount'];
            $monthContainer = $temp;
        }
//pr($monthContainer);
        $monthval = [];
        $chart_data = '';
        for ($i = 1; $i <= 12; $i++) {
            $totamount = 0;
            $allMonths = date("M", strtotime(date("Y") . "-" . $i . "-01"));
            if (array_key_exists($allMonths, $monthContainer)) {

                foreach ($monthContainer[$allMonths] as $amountData) {
                    $totamount = $totamount + $amountData;
                }

                $chart_data .= "{month:'" . $allMonths . "',invoice:'" . $totamount . "',expense:'" . $expenseTotalamount[$allMonths] . "'}, ";
            } else {

                $chart_data .= "{month:'" . $allMonths . "',invoice:'0',expense:'" . $expenseTotalamount[$allMonths] . "'}, ";
            }
        }
      //pr($chart_data);
?>        
 
 
         <div class="row" style="width: 100%">
             <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Invoice</h3>
                                    </div>
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body padding-0">
                                    <div class="chart-holder" id="morris-bar-example" style="height: 300px;"></div>
                                </div>
                            </div>
                        </div>
             <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Invoice</h3>
                                    </div>
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body padding-0">
                                    <div class="chart-holder" id="container44" style="height: 300px;"></div>
                                </div>
                            </div>
                        </div>
         </div>
                    <div class="row" style="width: 100%">
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Profit or Loss</h3>
                                    </div>
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                      
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                   </ul>
                                </div>
                                <div class="panel-body padding-0">
                                  
                                     <div id="container" style="width: 300px; height: 430px; margin: 0 auto"></div>
                                     
                              
                                </div>
                            </div>
                        </div>
                      <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Expenses</h3>
                                     </div>
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                    </ul>
                                </div>
                                <div class="panel-body padding-15">
                                    <div  id="dashboard-donut-2" style="width: 300px; height: 300px; margin: 0 auto"></div>
                                    <div id="legend" class="donut-legend" ></div>
                                </div>
                            </div>
                         </div>
                    </div>
       

<?php

$js = <<< JS
$(document).ready(function(){

        var others='$others';
        var material='$materialexpenseInfo';
        var contractor='$contractorexpenseInfo';

   
        var color_array = ['rgb(33, 150, 243)', 'rgb(255, 193, 7)', 'rgb(77, 216, 122)', 'rgb(255, 150, 0)'];
        var browsersChart = Morris.Donut({
                element: 'dashboard-donut-2',
                  margin:0.56,
                data   : [{label: "OverHead", value: others},
                         
                          {label: "Material Procure", value: material},
                          {label: "Sub Contractors", value: contractor}],
        colors: color_array
        });
        browsersChart.options.data.forEach(function(label, i){
         var legendItem = $('<span></span>').text( label['label'] + " ( " +label['value'] + " )" ).prepend('<br><span>&nbsp&nbsp&nbsp;</span>');
    legendItem.find('span')
      .css('backgroundColor', browsersChart.options.colors[i])
      .css('width', '15px')
      .css('display', 'inline-block')
      .css('margin', '5px');
    $('#legend').append(legendItem)
        })   
        
         
      var data=[ $chart_data];
Morris.Bar({
        element: 'morris-bar-example',
       data:data,
 xkey:'month',
 ykeys:['invoice','expense'],
  
 labels:[ 'Invoice Amount','Expense'],
  barColors: ['rgb(33, 150, 243)','rgb(255, 193, 7)'],    
        
    
 hideHover:'auto',

    });   


});

      
        
$(document).ready(function(){
$(function () {
            var invoiceamt='$invoiceGauge';
            var expense='$expense';
            var profit='$profit';
            var negative_val = '$negativeValue';
  
 
	
    Highcharts.setOptions({
    
            chart: {
                backgroundColor: 'white'
            },
            colors: ['rgb(33, 150, 243)', 'rgb(255, 193, 7)', 'rgb(77, 216, 122)']
            });
    // The speed gauge
    Highcharts.chart('container', {

        chart: {
            type: 'solidgauge',
            marginBottom: 100,
         spacingBottom: -70,
   spacingTop: -40,
          
                
        },

      title: {
    text: [ profit ]>= 0 ? 'Profit' : 'Loss',
   align: 'center',
     style:{
                        color:[ profit ]>= 0 ? 'rgb(77, 216, 122)' : 'red',
                    },
        y: 180 
  },
        subtitle: {
    text: profit,
       align: 'center',
        y: 220 
  
},
        
//        tooltip: {
//        	borderWidth: 0,
//            backgroundColor: 'white',
//            shadow: false,
//            style: {
//            	color: 'black',
//                fontSize: '10px'
//            },
//            pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y}</span>',
//             positioner: function (labelWidth, labelHeight) {
//            return {
//                x: 180 - labelWidth / 2,
//                y: 150
//            };
//        }
//        },

       pane: {
            startAngle: -140,
            endAngle: 140,
            background: [{ // Track for Move
                outerRadius: '112%',
                innerRadius: '88%',
                backgroundColor: 'white',
                borderWidth: 0,
               
            }, { // Track for Exercise
                outerRadius: '87%',
                innerRadius: '63%',
                backgroundColor: 'white',
                borderWidth: 0,
               
            }, { // Track for Stand
                outerRadius: '87%',
                innerRadius: '63%',
                backgroundColor:  'white',
                borderWidth: 0,
              
            }]
        },


        yAxis: {
            min: 0,
            max: 0,
       
            lineWidth: 0,
            tickPositions: []
        },

        plotOptions: {
            solidgauge: {
                borderWidth: '20px',
               dataLabels: {
            enabled: false
        }
                 
            }
        },
            
        legend: {
        align: 'left',
            verticalAlign: 'middle',
            x:30,
            y: 150,
            floating: true,
            itemWidth: 270, 
            itemStyle : '{ "word-wrap" "break-word"}',
         labelFormatter: function () {
            return '<span class="gauge-legend-item">' + this.name + '</span><span class="series-value">' + ' : '+(this.yData[0]) + '</span>';
        },
            },
            
        
        series: [{
            name: 'Invoice',
            borderColor: Highcharts.getOptions().colors[0],
            data: [{
	            color: Highcharts.getOptions().colors[0],
                radius: '100%',
                innerRadius: '100%',
                y: [invoiceamt]
            }],
       
             showInLegend: true,
             name:'Invoice',
             marker: { symbol: 'square', fillColor: 'rgb(33, 150, 243)', radius: 12 },
            
            
        }, {
            name: 'Expenses',
            borderColor: Highcharts.getOptions().colors[1],
            data: [{
                color: Highcharts.getOptions().colors[1],
                radius: '75%',
                innerRadius: '75%',
                y: [expense]
            }],
                showInLegend: true,
                name:'Expenses',
                marker: {  symbol: 'square',fillColor: 'rgb(255, 193, 7)', radius: 12 }
          
        }, 
        {
            name: [ profit ]>= 0 ? 'Profit' : 'Loss',
            borderColor: [ profit ]>= 0 ? 'rgb(77, 216, 122)' : 'red',
        
            data: [{
                color:[ profit ]>= 0 ? 'rgb(77, 216, 122)' : 'red',
                radius: '55%',
                innerRadius: '55%',
                y: [profit >=0 ? profit : negative_val]
            }],
            
                showInLegend: true,
                name:[ profit ]>= 0 ? 'Profit' : 'Loss',
                marker: {  symbol: 'square',fillColor: [ profit ]>= 0 ? 'rgb(77, 216, 122)' : 'red', radius: 12, }
        }]
    });


});

});
JS;
$this->registerJs($js);
?>

<script type="text/javascript" src="<?= $homeUrl; ?>web/lib/js/echarts.min.js"></script>
<!--<script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/echarts.min.js"></script>-->
<!--       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-gl/echarts-gl.min.js"></script>-->
<!--       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts-stat/ecStat.min.js"></script>-->
<!--       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/dataTool.min.js"></script>-->
<!--       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/china.js"></script>-->
<!--       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/map/js/world.js"></script>-->
<!--       <script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=ZUONbpqGBsYGXNIYHicvbAbM"></script>-->
<!--       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/echarts/extension/bmap.min.js"></script>-->
<!--       <script type="text/javascript" src="http://echarts.baidu.com/gallery/vendors/simplex.js"></script>-->
<script type="text/javascript">
var dom = document.getElementById("container44");
var myChart = echarts.init(dom);

var app = {};
option = null;


option = {
//    title: {
//        text: '天气情况统计',
//        subtext: '虚构数据',
//        left: 'center'
//    },
    tooltip : {
        trigger: 'item',
        formatter: ""
    },
    legend: {
         //orient: 'vertical',
         //top: 'middle',
        bottom: 10,
        left: 'center',
        data: ['Revenue', 'Credited','Outstanding','OverDue']
    },
     color: ['rgb(33, 150, 243)','rgb(77, 216, 122)','rgb(255, 193, 7)'],
    series : [
        {
            type: 'pie',
            radius : '65%',
            center: ['50%', '50%'],
            selectedMode: 'single',
            data:[

                {value:<?php echo $total_revenue ?>, name: 'Revenue'},
                {value:<?php echo $total_credited ?>, name: 'Credited'},
                {value:<?php echo $Overall_Outstanding ?>, name: 'Outstanding'},
                
            ],
    
    itemStyle : {
              normal : {
                  
                    
                         label : {
                                   show : false
                                  },
                         labelLine : {
                                       show : false
                                      }
                         }
              }
        }
    ]
};

if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
       </script>