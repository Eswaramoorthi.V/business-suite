    <!-- START WIDGET SLIDER -->
            <div class="widget widget-default widget-carousel">
                <div class="owl-carousel" id="owl-example">
                    <?php
                        foreach ($sliderData as $slider){
                                $label=$slider['label'];
                                $value=$slider['value'];
                                if($label=='Normal'){
                                    $class='normal';
                                }else if($label=='Low Battery'){
                                    $class='low';
                                }
                                 else if($label=='Critical'){
                                     $class='danger';
                                 }
                            ?>
                            <div>
                                <div class="widget-title <?php echo $class;?>">Battery Status</div>
                                <div class="widget-subtitle"><span class="<?php echo $class;?>"><?php echo $label;?></span></div>
                                <div class="widget-int"><span class="<?php echo $class;?>"><?php echo $value;?></span></div>
                            </div>
                        <?php }
                    ?>
                </div>                            

            </div>         
            <!-- END WIDGET SLIDER -->