<?php

use yii\helpers\Url;

?>
    <!-- Custom Theme Style Start Here -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/style.bundle.css" />
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/app-table.css" />
<div class="row">
    <div class="col-md-9">&nbsp;</div>
    <div id="reportrange" data-url="<?= Url::toRoute(["search-index"]); ?>" class="dtrange col-md-3">                                            
        <span></span><b class="caret"></b>
    </div>                                     
</div> 
 <div class="row dashboarddata"> 
<?php
            echo Yii::$app->controller->renderPartial("dashboard_data");
            ?>

 </div>        
<?php
$this->registerJsFile('@web/lib/js/demo_dashboard.js',['depends' => 'yii\web\JqueryAsset']);
?>
