<?php
namespace app\modules\home\controllers;
use app\controllers\BaseController;


class DashboardController extends BaseController {

    public function init() {
        parent::init();
        date_default_timezone_set('Asia/Dubai');
    }

    public function actions() {
        return [];
    }

    public function actionIndex() {
       
        return $this->render('dashboard_index');
    }
    
     public function actionSearchIndex() {
     
    
          return $this->renderAjax('dashboard_data',[
                    
        ]);
    }


}


