<?php

use yii\helpers\Html;

$this->title = 'Create Company';
$this->params['breadcrumbs'][] = ['label' => 'Company', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['title_icon'] = 'fa-tags';
?>
<div class="company-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
