<?php

namespace app\modules\company\controllers;

use Yii;
use app\models\Company;
use app\models\Search\CompanySearch;

use app\modules\role\models\AuthItemSearch;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\mpdf\Pdf;
use yii\web\View;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\controllers\BaseController;
use app\modules\estimate\models\Settings;
use app\models\SettingsTree;
use app\models\EstimateDetails;
use app\models\EstimateMaster;
use app\models\MaterialMaster;
use app\models\MaterialUnitCost;
use app\models\LabourMaster;
use app\models\LabourUnitCost;
use app\modules\estimate\models\Equipment;
use app\models\ProjectEstimateMaster;
use app\models\ProjectEstimate;
use app\models\Tree;
use app\models\AuthItem;
use app\components\MenuHelper;
use yii\web\UploadedFile;
/**
 * ProjectController implements the CRUD actions for Project model.
 */
class CompanyController extends BaseController
{
    /**
     * Lists all Project models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $route=$this->route;
        $actionList = MenuHelper::getActionHelper($route);
    //   pr($actionList);
        $visibleList = MenuHelper::getActionVisibleHelper($route);
         $searchModel = new CompanySearch();
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->pagination = ['PageSize'=>50];
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'actionList'=>$actionList,
                    'visibleList'=>$visibleList
        ]);
       
 
    }

    public function actionCreate()
    {
        $model = new Company();
        if ($model->load(Yii::$app->request->post())) {
            // pr(Yii::$app->request->post());
            $request = Yii::$app->request->post();

            $CompanyData = $request['Company'];

            $companyName = $CompanyData['company_name'];


            $filenameDoc = '';

            $directoryPath = 'web/uploads/company/';



            $logo = UploadedFile::getInstance($model, 'logo');
            if (!empty($logo)) {
                if ($model->logo = $logo) {
                    $webRoot= Yii::getAlias('@webroot');
                    $filename = $webRoot.'/uploads/company/';
                    $filenameDoc = $filename . $model->logo->baseName . '_' . time() . '.' . $model->logo->extension;
                    $model->logo->saveAs($filenameDoc);
                    $model->logo = $model->logo->baseName . '_' . time() . '.' . $model->logo->extension;
                }
                //    pr($request);
                $model->company_name = $request['Company']['company_name'];
                $model->address = $request['Company']['address'];
                $model->contact_person = $request['Company']['contact_person'];
                $model->country = $request['Company']['country'];
                $model->currency = $request['Company']['currency'];
                $model->fax_no = $request['Company']['fax_no'];
                $model->tel_no = $request['Company']['tel_no'];
                $model->email_id = $request['Company']['email_id'];
                $model->po_box_no = $request['Company']['po_box_no'];

                if (!$model->save()) {
                    pr($model->getErrors());
                }
                return $this->redirect(['index']);
            }
        }else {
                return $this->renderAjax('create', [
                    'model' => $model,
                ]);
            }

    }

    /**
     * Updates an existing Project model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)

    {

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $request = Yii::$app->request->post();
            $model->logo = $request['Company']['logo'];

            $companyName = $model['company_name'];
            $logoUpload = UploadedFile::getInstance($model, 'logo');

            if (!empty($logoUpload)) {
                if ($model->logo = $logoUpload) {

                    $webRoot = Yii::getAlias('@webroot');
                    $filename = $webRoot . '/uploads/company/';
                    $filenameDoc = $filename . $model->logo->baseName . '_' . time() . '.' . $model->logo->extension;
                    $model->logo->saveAs($filenameDoc);
                    $model->logo = $model->logo->baseName . '_' . time() . '.' . $model->logo->extension;
                }



            }

            $model->company_name = $request['Company']['company_name'];
            $model->address = $request['Company']['address'];
            $model->contact_person = $request['Company']['contact_person'];
            $model->country = $request['Company']['country'];
            $model->currency = $request['Company']['currency'];
            $model->fax_no = $request['Company']['fax_no'];
       $model->tel_no = $request['Company']['tel_no'];
       $model->email_id = $request['Company']['email_id'];
       $model->po_box_no = $request['Company']['po_box_no'];

            if(!$model->save()){
                pr($model->getErrors());
            }
            return $this->redirect(['index']);
        } else {

            return $this->renderAjax('update', [

                'model' => $model,

            ]);

        }

    }
    /**
     * Deletes an existing Project model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
   
    public function actionDeletes($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Company::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDetailview() {
        $id = $_GET['id'];
        return $this->render('detail_view', [
                    'model' => $this->findModel($id),
        ]);
    }

    
   

}