<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var dektrium\user\models\LoginForm $model
 * @var dektrium\user\Module $module
 */

$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<!-- Custom Theme Style Start Here -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/style.bundle.css">
<link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/login-6.css">
<!-- Custom Theme Style End Here -->



<!--New Code begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root kt-page">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                <div class="kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__body">
                            <div class="kt-login__logo">
                                <a href="#">
                                    <img src="<?= Url::base() ?>/web/lib/img/new/logo-2.png">
                                </a>
                            </div>
                            <div class="kt-login__signin">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Pricing Calculator</h3>
                                </div>
                                <div class="kt-login__form">
                                            <!-- <form class="kt-form" action="">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" placeholder="Username" name="email" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control form-control-last" type="password" placeholder="Password" name="password">
                                                </div>
                                                <div class="kt-login__extra">
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="remember"> Remember me
                                                        <span></span>
                                                    </label>
                                                    <a href="javascript:;" id="kt_login_forgot">Forget Password ?</a>
                                                </div>
                                                <div class="kt-login__actions">
                                                    <button id="kt_login_signin_submit" class="btn btn-brand btn-pill btn-elevate">Log In</button>
                                                </div>
                                            </form> -->
                                            <div class="form-horizontal">
                                                <?php $form = ActiveForm::begin([
                                                    'id' => 'login-form',
                                                    'enableAjaxValidation' => true,
                                                    'enableClientValidation' => false,
                                                    'validateOnBlur' => false,
                                                    'validateOnType' => false,
                                                    'validateOnChange' => false,
                                                ]) ?>

                                                <?= $form->field($model, 'login',
                                                    ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1', 'placeholder' => 'Username']]
                                                )->label(false);
                                                ?>

                                                <?= $form->field(
                                                    $model,
                                                    'password',
                                                    ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2', 'placeholder' => 'Password']])
                                                ->passwordInput()
                                                ->label(false);
                                                ?>

                                                <div class="form-group">
                                                   <div class="col-md-12 kt-login__extra" style="justify-content: flex-end;padding-right: 0;">
                                                       <a href="../site/forgot" class="" style="justify-content: flex-end;padding-right: 0;">Forgot password?</a>
                                                   </div>
                                                   <div class="col-md-12 kt-login__actions">
                                                       <button class="btn btn-brand btn-pill btn-elevate">Log In</button>
                                                   </div>
                                               </div>
                                               
                                               <?php ActiveForm::end(); ?>
                                           </div>
                                       </div>
                                   </div>
                                    <!-- <div class="kt-login__signup">
                                        <div class="kt-login__head">
                                            <h3 class="kt-login__title">Sign Up</h3>
                                            <div class="kt-login__desc">Enter your details to create your account:</div>
                                        </div>
                                        <div class="kt-login__form">
                                            <form class="kt-form" action="">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" placeholder="Fullname" name="fullname">
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control" type="password" placeholder="Password" name="password">
                                                </div>
                                                <div class="form-group">
                                                    <input class="form-control form-control-last" type="password" placeholder="Confirm Password" name="rpassword">
                                                </div>
                                                <div class="kt-login__extra">
                                                    <label class="kt-checkbox">
                                                        <input type="checkbox" name="agree"> I Agree the <a href="#">terms and conditions</a>.
                                                        <span></span>
                                                    </label>
                                                </div>
                                                <div class="kt-login__actions">
                                                    <button id="kt_login_signup_submit" class="btn btn-brand btn-pill btn-elevate">Sign Up</button>
                                                    <button id="kt_login_signup_cancel" class="btn btn-outline-brand btn-pill">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div> -->
                                    <!-- <div class="kt-login__forgot">
                                        <div class="kt-login__head">
                                            <h3 class="kt-login__title">Forgotten Password ?</h3>
                                            <div class="kt-login__desc">Enter your email to reset your password:</div>
                                        </div>
                                        <div class="kt-login__form">
                                            <form class="kt-form" action="">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" placeholder="Email" name="email" id="kt_email" autocomplete="off">
                                                </div>
                                                <div class="kt-login__actions">
                                                    <button id="kt_login_forgot_submit" class="btn btn-brand btn-pill btn-elevate">Request</button>
                                                    <button id="kt_login_forgot_cancel" class="btn btn-outline-brand btn-pill">Cancel</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="kt-login__account">
                                <span class="kt-login__account-msg">
                                    Jeyamsys© Copyright <?php echo date("Y"); ?>  All Rights Reserved.
                                </span>&nbsp;&nbsp;
                                <!-- <a href="javascript:;" id="kt_login_signup" class="kt-login__account-link">Sign Up!</a> -->
                            </div>
                        </div>
                    </div>
                    <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content" style="background-image: url(../web/lib/img/new/bg-4.jpg);">
                        <div class="kt-login__section">
                            <div class="kt-login__block">
                                <h3 class="kt-login__title">Join Our Community</h3>
                                <div class="kt-login__desc">
                                    Lorem ipsum dolor sit amet, coectetuer adipiscing
                                    <br>elit sed diam nonummy et nibh euismod
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--New Code end:: Page -->


        <!-- Old Code Here -->
<!-- <div class="login-container">
    <div class="login-box animated fadeInDown">
        
        <div class="text-center logo-text">Pricing Calculator</div>
        <div class="login-body">
        <div class="text-center logo-text">Pricing Calculator</div>
             <div class="login-title"><img src="<?php echo Yii::$app->params['domain_url']; ?>/lib/img/login_logo.png" width="100%"></div>
            <div class="login-title"><strong>Welcome</strong>, Please login</div>
           

            <div class="form-horizontal">
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                    'enableAjaxValidation' => true,
                    'enableClientValidation' => false,
                    'validateOnBlur' => false,
                    'validateOnType' => false,
                    'validateOnChange' => false,
                ]) ?>

                    <?= $form->field($model, 'login',
                        ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1', 'placeholder' => 'Username']]
                    )->label(false);
                    ?>

                    <?= $form->field(
                        $model,
                        'password',
                        ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2', 'placeholder' => 'Password']])
                        ->passwordInput()
                        ->label(false);
                     ?>

                     <div class="form-group">
                         <div class="col-md-6">
                             <a href="../site/forgot" class="btn btn-link btn-block">Forgot your password?</a>
                         </div>
                         <div class="col-md-6">
                             <button class="btn btn-success btn-block">Log In</button>
                         </div>
                     </div>
                    
                <?php ActiveForm::end(); ?>
            </div>
            <div class="col-md-12 text-right" style="color:#767676;"> Version v1.0.5</div>
        </div>
         <div class="row"><div class="col-md-12 text-center" style="clear:both; color:#767676; margin-top: 30px;">Jeyamsys© Copyright <?php echo date("Y"); ?>  All Rights Reserved.</div></div>
    </div>
</div> -->

