<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Url;

?>

<div class="faults-index">

    
    

    <div class="row">
            <div class="col-md-12 text-right add-label">
                <a href="<?= Url::toRoute(["create"]); ?>">
                    
                    <strong>Add New User</strong>
                </a>
            </div>
        </div>
    
</div>