<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;


/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */

//$this->title = Yii::t('user', 'Manage Users');
$this->params['breadcrumbs'][] = 'Manage Users';
//$this->params['title_icon'] = 'fa-users';
?>
<div class="row">
    <div class="col-md-6 text-left add-label" style="font-size: 32px">
      
            <i class="fa fa-users" aria-hidden="true"></i> Manage Users
    </div>
   <div class="col-md-6 text-right add-label" style="padding-top: 12px"> 
<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<?= $this->render('/admin/_menu') ?>
   </div>
</div>
<?php Pjax::begin() ?>
<div class="user-management">
<?= GridView::widget([
    'dataProvider' => $dataProvider,
   //'filterModel'  => $searchModel,
    'options' => ['class' => 'table-responsive kv-grid-container'], 
    'layout'       => "{items}\n{pager}",
    'columns' => [
//        [
//            'attribute' => 'User Id',
//            'value' => 'id',
//        ],
        [
             'header' => 'S/No',  
             'class' => 'yii\grid\SerialColumn',  
             'options' => ['style' => 'width:10%;'],             
        ],
        'username',
        'email:email',
        'primary_contact_no',
        [
            'attribute' => 'created On',
            'value' => function ($model) {
                if (extension_loaded('intl')) {
                    return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->created_at]);
                } else {
                    return date('d-m-Y', $model->created_at);
                }
            },
        ],

        [
          'attribute' => 'Last Login On',
          'value' => function ($model) {
            if (!$model->last_login_at || $model->last_login_at == 0) {
                return Yii::t('user', 'Never');
            } else if (extension_loaded('intl')) {
                return Yii::t('user', '{0, date, MMMM dd, YYYY HH:mm}', [$model->last_login_at]);
            } else {
                return date('d-m-Y', $model->created_at);
            }
          },
        ],
        [
            'header' => Yii::t('user', 'Confirmation'),
            'value' => function ($model) {
                if ($model->isConfirmed) {
                    return '<div class="text-center">
                                <span class="text-success">' . Yii::t('user', 'Confirmed') . '</span>
                            </div>';
                } else {
                    return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                    ]);
                }
            },
            'format' => 'raw',
            'visible' => Yii::$app->getModule('user')->enableConfirmation,
        ],
        
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{switch} {resend_password} {update} {delete}',
            'buttons' => [
                'resend_password' => function ($url, $model, $key) {
                    if (!$model->isAdmin) {
                        return '
                    <a data-method="POST" data-confirm="' . Yii::t('user', 'Are you sure?') . '" href="' . Url::to(['resend-password', 'id' => $model->id]) . '">
                    <span title="' . Yii::t('user', 'Generate and send new password to user') . '" class="glyphicon glyphicon-envelope">
                    </span> </a>';
                    }
                },
                
            ]
        ],
    ],
]); ?>
</div>
<?php Pjax::end() ?>

<style>
    .user-management th a {
        color: #56688A !important;
    } 
    
    .user-management td a {
       margin-right: 20px;
    }  
    
</style>
