<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var $this  yii\web\View
 * @var $model dektrium\rbac\models\Role
 */


use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="row">
            
            <div class="col-md-8">
<?php $form = ActiveForm::begin([
    'enableClientValidation' => false,
    'enableAjaxValidation'   => true,
]) ?>

<?= $form->field($model, 'name') ?>

<?= $form->field($model, 'description')->textarea() ?>





<?= Html::submitButton(Yii::t('rbac', 'Save'), ['class' => 'btn btn-primary text-center']) ?>
            </div></div>
<?php ActiveForm::end() ?>