<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var $dataProvider array
 * @var $filterModel  dektrium\rbac\models\Search
 * @var $this         yii\web\View
 */


use kartik\select2\Select2;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = Yii::t('rbac', 'Roles');
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $this->beginContent('@dektrium/rbac/views/layout.php') ?>

<div class="row">
            
            <div class="col-md-8 text-left"><span style="color:red"><strong><?php echo Yii::$app->getRequest()->getQueryParam('error'); ?></strong></span></div>
            <div class="col-md-4 text-right add-label">
                <a href="<?= Url::toRoute(["create"]); ?>">
                    <strong>Add New Role</strong>
                </a>
            </div>
        </div>


<?php Pjax::begin() ?>
<div class="role-management">
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel'  => $filterModel,
    'layout'       => "{items}\n{pager}",
    'formatter' => [
                        'class' => 'yii\i18n\Formatter',
                        'nullDisplay' => '-',
                    ],
    'columns'      => [
         [
             'header' => 'S/No',  
             'class' => 'yii\grid\SerialColumn',  
             'options' => ['style' => 'width:10%;'],             
        ],
        [
            'attribute' => 'name',
            'header'    => Yii::t('rbac', 'Name'),
            'options'   => [
                'style' => 'width: 30%'
            ],
            'filter' => Select2::widget([
                'model'     => $filterModel,
                'attribute' => 'name',
                'data'      => $filterModel->getNameList(),
                'options'   => [
                    'placeholder' => Yii::t('rbac', 'Select role'),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]),
        ],       
        [
            'attribute' => 'description',
            'header'    => Yii::t('rbac', 'Description'),
            'options'   => [
                'style' => 'width: 40%',
            ],
            'filterInputOptions' => [
                'class'       => 'form-control',
                'id'          => null,
                'placeholder' => Yii::t('rbac', 'Enter the description')
            ],
        ],
       /* [
            'attribute' => 'rule_name',
            'header'    => Yii::t('rbac', 'Rule name'),
            'options'   => [
                'style' => 'width: 20%'
            ],
            'filter' => Select2::widget([
                'model'     => $filterModel,
                'attribute' => 'rule_name',
                'data'      => $filterModel->getRuleList(),
                'options'   => [
                    'placeholder' => Yii::t('rbac', 'Select rule'),
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]),
        ],*/
         [
             'class'      => ActionColumn::className(),
             'template'   => '{update} {delete}',
             'urlCreator' => function ($action, $model) {
                 return Url::to(['/rbac/role/' . $action, 'name' => $model['name']]);
             },
             'options' => [
                 'style' => 'width: 10%'
             ],
         ]
    ],
]) ?>
</div>
<?php Pjax::end() ?>

<?php $this->endContent() ?>

<style>
    .role-management th a {
        color: #56688A !important;
    } 
    
    .role-management td a {
       margin-right: 20px;
    }  
    
</style>
