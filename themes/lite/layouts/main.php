<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\lite\AppAsset;
use yii\helpers\Url;
use app\widgets\noty\Wrapper;
use yii\widgets\Menu;
use app\components\MenuList;
use app\models\EmployeeCompany;
use app\models\Company;

AppAsset::register($this);
use yii\helpers\ArrayHelper;

// pr($assignCompany);

            

// pr($companylist);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <!-- META SECTION -->
        <title><?= Html::encode($this->title) ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- Custom Theme Style Start Here -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">
        <link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/style.bundle.css" />
        <!-- <link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/app-table.css" /> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/style.bundle.css"> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/login-6.css"> -->
<!-- Custom Theme Style End Here -->
        <?= Html::csrfMetaTags() ?>
        <!-- END META SECTION -->

        <!-- CSS INCLUDE -->
        <?php $this->head(); ?>

        <!-- EOF CSS INCLUDE -->
    </head>
    <body>
        <?php $this->beginBody() ?>

        <?php
        echo Wrapper::widget([
            'layerClass' => 'lo\modules\noty\layers\Growl',
            'options' => [
                'fixed' => false,
                'size' => 'medium',
                'location' => 'tr',
                'delayOnHover' => true,
                'duration' => 3000
            ]
        ]);

            $lastUpdateDate=date_create(Yii::$app->user->identity->password_update);
            $curDate=date_create(date("Y-m-d"));
            $passDiff=date_diff($lastUpdateDate,$curDate);
            $passwordexpired = \Yii::$app->params['passwordExpired'];
            $passwordexpireIn = \Yii::$app->params['passwordExpireIn'];
            if(Yii::$app->user->identity->reset_password==0 && Yii::$app->controller->module->id!='changepass'){
               return Yii::$app->response->redirect(['changepass'])->send();
            } else {
                  if($passDiff->days>$passwordexpired && Yii::$app->controller->module->id!='changepass') {
                      return Yii::$app->response->redirect(['changepass', 'expire' =>1])->send();
                  }
            }

        ?>

        <!-- START PAGE CONTAINER -->
        <div class="page-container">
 <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
            <?php

            $actionId = Yii::$app->controller->action->id;
            $controllerId = Yii::$app->controller->action->controller->id;
           // pr(Yii::$app->user->identity->username);
            //pr1($controllerId);
            echo Menu::widget(MenuList::getMenu($controllerId, $actionId));
        ?>
            </div>
 <!-- END PAGE SIDEBAR -->
            <!-- PAGE CONTENT -->
            <div class="page-content">

                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <li>&nbsp;</li>
                    <!-- <li><img src="<?php echo Yii::$app->params['domain_url']; ?>/lib/img/logo-a.jpg" style="height:50px;"></li> -->
<!--                    <li>&nbsp;&nbsp;&nbsp;</li>-->
                    <li class="hide"><span class="text-center" style="color:#ffff; font-weight: bold; font-size: 30px;">Clarity Interiors</span></li>



                    <!--START NEW SCHEDULE -->
<!--                    <li class="dropdown head-dpdn open pull-right" style="margin-right:25px;">&nbsp;</li>-->
                     <li class="dropdown head-dpdn open pull-right">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><i class="fa fa-user"></i>
                            Welcome <?php echo ucfirst(Yii::$app->user->identity->username); ?></a>
<!--                        <span class="fa fa-user"></span></a>&nbsp;-->

<!--                        <span class="glyphicon glyphicon-chevron-down"></span>-->

                        <ul class="" style="width:170px; background:#fff;">
                                <li> <a href="javascript://" data-changepassurl="<?= Url::toRoute('/changepass') ?>" id="change_pass" class="mb-control" ><i class="fa fa-cog"></i> Reset Password</a> </li>
<!--                                <li> <a href="#"><i class="fa fa-user"></i> Profile <?php //echo \Yii::$app->params['passwordExpiry'];?></a> </li> -->
                                <li> <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span> Logout</a> </li>
                        </ul>
                        </li>
                    <li class="dropdown  head-dpdn  open pull-right">
                        <?php
                            if(isset($_REQUEST['company_id'])){
                                $url=Yii::$app->request->url;
                                $session = Yii::$app->session;
                                $companyId=$_REQUEST['company_id'];
                                $companyDetails= Company::find()->where(['id'=>$companyId])->one();
                                $session['company_id'] = $companyDetails['id'];
                                $session['company_name'] = isset($companyDetails['company_name'])?$companyDetails['company_name']:"";

                                $pathurl=Yii::$app->homeUrl;

                                //return $this->redirect($pathurl);
                                return Yii::$app->response->redirect($pathurl);

                             }
                        ?>

                        <a class="dropdown-toggle" title="calculator" data-placement="bottom" href="#" data-toggle="dropdown"><?php echo isset($_SESSION['company_name'])?$_SESSION['company_name']:""?></i>
                        </a>
                        <?php

                        $currentUserId = Yii::$app->user->getId();
                        $assignCompany = EmployeeCompany::find()->with('company')->where(['user_id' => $currentUserId])->asArray()->all();


                        $assignCompany = ArrayHelper::map($assignCompany, 'company_id', 'company.company_name');

                        $url=Yii::$app->request->url;
                        $pathinfo=pathinfo($url);
                        if(!empty($assignCompany)){
                            $items=[];
                            foreach ($assignCompany as $id=>$name){
                                $item=[];
                                $item['label']=$name;
                                $item['options']=['class'=>'company_class'];
                                if(empty($_REQUEST)){
                                    $item['url']=$url."?company_id=".$id;
                                }else{
                                    $item['url']=$url."&company_id=".$id;
                                }


                                $items[]=$item;

                            }
                        }

                        use kartik\nav\NavX;
                        echo NavX::widget([
                            'options' => ['class' => 'mb-control'],
                            'items' => $items,
                            'encodeLabels' => false
                        ]);
                        ?>
                    </li>

                        <li class="dropdown  head-dpdn  open pull-right">
                        <a class="dropdown-toggle" title="calculator" data-placement="bottom" href="#" data-toggle="dropdown"><i class="fa fa-calculator margin-right-0"></i>
                        </a>
                        <ul class="xn-drop-left calc">
                            <li class="dropdown-content">
                                <span id="inlineCalc"></span>
                            </li>
                        </ul>
                    </li>
                    <?php
                     $fetchRole = Yii::$app->authManager->getRolesByUser(Yii::$app->user->getId());
                     reset($fetchRole);
                     $role = current($fetchRole);

                        ?>

                        <?php  if($passDiff->days>$passwordexpireIn) {
                            $daysDisp=($passDiff->days==1)?"day":"days";
                            ?>

                        <li class="xn-icon-button pull-right">
                            <a href="#"><span class="fa fa-bell" style="color:red;"></span></a>
                        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging ui-draggable">
                            <div class="panel-heading">
                                    <h3 class="panel-title"><span class="fa fa-exclamation-triangle"></span></h3>
                                    <div class="pull-left">
                                        <span class="label label-danger"> You Password Expire in <?php echo $passDiff->days." ".$daysDisp; ?>

                                        </span>
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <a href="<?= Url::toRoute(['/changepass']); ?>">Click here to change the password</a>
                                </div>

                        </div>
                    </li>
                        <?php } ?>



                </ul>
                <!-- END X-NAVIGATION VERTICAL -->

               

<?php if (!empty($this->title)) { ?>
                    <div class="page-title">
                        <h2>
                            <span class="fa <?= isset($this->params['title_icon']) ? $this->params['title_icon'] : '' ?>"></span>
    <?= Html::encode($this->title) ?>
                        </h2>
                    </div>
<?php } ?>

                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12" style="">
                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                       <!-- START BREADCRUMB -->
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <!-- END BREADCRUMB -->
      <div id="loading">   <div class="loader"></div></div>                              
<?= $content ?>
                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->
                

            </div>
            <!-- END PAGE CONTENT -->
            <div class="clearfix"></div>
            <div class="row" style="display: none;"><div style="margin-left: 265px;">&nbsp;</div><div class="text-center" style="color:#000;">Jeyamsys© Copyright <?php echo date("Y"); ?>  All Rights Reserved. Version v1.0.5 </div>
            <!-- <div class="col-md-3 text-right" style="color:#FFF;"> Version v1.0.5</div></div> -->

        </div>
        <!-- END PAGE CONTAINER -->

<?php if (!Yii::$app->user->isGuest): ?>
            <!-- MESSAGE BOX-->
            <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
                <div class="mb-container">
                    <div class="mb-middle">
                        <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                        <div class="mb-content">
                            <p>Are you sure you want to log out?</p>
                            <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                        </div>
                        <div class="mb-footer">
                            <div class="pull-right">
                                <?php
                                echo Html::beginForm(['/user/security/logout'], 'post', ['style' => 'display: inline;']);
                                echo Html::submitButton(
                                        'Yes', ['class' => 'btn btn-success btn-lg']
                                );
                                echo Html::endForm()
                                ?>
                                <button class="btn btn-default btn-lg mb-control-close">No</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MESSAGE BOX-->
<?php endif; ?>


        <!-- START PRELOADS -->
        <audio id="audio-alert" src="<?= Url::to('@web/') . 'lib/audio/alert.mp3'; ?>" preload="auto"></audio>
        <audio id="audio-fail" src="<?= Url::to('@web/') . 'lib/audio/fail.mp3'; ?>" preload="auto"></audio>
        <!-- END PRELOADS -->

<?php $this->endBody() ?>
<script type="text/javascript">
$(window).load(function() {
		// Animate loader off screen
		$("#loading").fadeOut("slow");

	$('#inlineCalc').calculator({layout: ['_%+-CABS','_7_8_9_/','_4_5_6_*','_1_2_3_-','_0_._=_+'], showFormula:true});
    $('.calc').click(function(e) { e.stopPropagation();});
    });
</script>
    </body>    
</html>

<?php $this->endPage() ?>
