<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\lite\AppAsset;

if(empty($this->title)) {
    $pageTitle = $this->title;
}
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="body-full-height">
<head>
    <!-- META SECTION -->
    <title><?= Html::encode($this->title) ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="favicon.ico" type="image/x-icon" />
    <?= Html::csrfMetaTags() ?>
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <?php $this->head(); ?>

    <link rel="stylesheet" type="text/css" id="theme" href="<?= Url::base() . '/lib/css/theme-default.css'; ?>"/>
    <link rel="stylesheet" type="text/css" href="<?= Url::base() ?>/web/lib/css/app-table.css">
    <!-- EOF CSS INCLUDE -->
</head>
<body>
    <?php $this->beginBody() ?>
    <?= $content; ?>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
