<?php

namespace app\controllers;

use app\models\Profile;
use app\models\UserInfo;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\modules\changepass\models\PasswordForm;
use \dektrium\user\models\User;
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'contact', 'about', 'dev', 'logout'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionDev()
    {
        return $this->render('dev');
    }

    public function actionForgot()
    {
        $model = new UserInfo();
        if (Yii::$app->request->isPost and isset($_REQUEST['UserInfo']['email'])) {
            $email=$_REQUEST['UserInfo']['email'];
            $model->load(Yii::$app->request->post());
            $userinfo=$model::find()->where(['email'=>$email])->one();
            if(empty($userinfo)){
                                Yii::$app->session->setFlash('msg', '
                     <div class="alert alert-danger alert-dismissable">
                     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                     <strong>Validation error! </strong> Email Doesn\'t Exsisit, Please Enter Correct Email.</div>'
                );
                Yii::$app->session->setFlash('success', 'Email doesnt Exisit, Please try Again');
                return $this->render('forgot', [
                    'model' => $model,
                ]);
            }else{
                $model = new Profile();
                $profile=$model::find()->where(['user_id'=>$userinfo->id])->asArray()->one();
                $model->hint_question=$profile['hint_question'];
                $model->user_id=$profile['user_id'];
                return $this->render('recover_question', [
                    'model' => $model,
                ]);
            }


        }else if (Yii::$app->request->isPost and isset($_REQUEST['Profile']['hint_answer'])) {
            //pr($_REQUEST);
            $profilehint=(string) $_REQUEST['Profile']['hint_answer'];
            $userId=$_REQUEST['Profile']['user_id'];
            $profile = new Profile();
            $profile=$profile::find()->where(['hint_answer'=>$profilehint,'user_id'=>$userId])->asArray()->one();

            if(empty($profile)){
                Yii::$app->session->setFlash('msg', '
                     <div class="alert alert-danger alert-dismissable">
                     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                     <strong>Validation error! </strong> Answer Doesn\'t Match, Please Enter Correct Answer.</div>'
                );
                Yii::$app->session->setFlash('success', 'Email doesnt Exisit, Please try Again');
                return $this->render('forgot', [
                    'model' => $model,
                ]);
            }else{
                $model = new PasswordForm;
                return $this->render('changepassword',[
                    'model'=>$model,'profile'=>$profile,
                    'userId'=>$userId
                ]);
            }


        }
        else if (Yii::$app->request->isPost and isset($_REQUEST['PasswordForm']['newpass'])) {
            pr($_REQUEST);
            $profilehint=(string) $_REQUEST['Profile']['hint_answer'];
            $userId=$_REQUEST['Profile']['user_id'];
            $profile = new Profile();
            $profile=$profile::find()->where(['hint_answer'=>$profilehint,'user_id'=>$userId])->asArray()->one();

            if(empty($profile)){
                Yii::$app->session->setFlash('msg', '
                     <div class="alert alert-danger alert-dismissable">
                     <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                     <strong>Validation error! </strong> Answer Doesn\'t Match, Please Enter Correct Answer.</div>'
                );
                Yii::$app->session->setFlash('success', 'Email doesnt Exisit, Please try Again');
                return $this->render('forgot', [
                    'model' => $model,
                ]);
            }else{
                $model = new PasswordForm;
                return $this->render('changepassword',[
                    'model'=>$model,'profile'=>$profile,
                    'userId'=>$userId
                ]);
            }


        }

        else{
            return $this->render('forgot', [
                'model' => $model,
            ]);
        }


    }
    public function actionChangePassword()
    {
        $model = new PasswordForm;
        $userId=$_POST['PasswordForm']['id'];
        $modeluser = User::find()->where([
            'id'=>$_POST['PasswordForm']['id']
        ])->one();

        if($model->load(Yii::$app->request->post()))
        {
                try
                {
                    $modeluser->password_hash = Yii::$app->security->generatePasswordHash($_POST['PasswordForm']['newpass']);
                    if($modeluser->save())
                    {
                        Yii::$app->getSession()->setFlash(
                            'success','Password changed'
                        );
                        return $this->redirect(['user/login']);
                    }
                    else
                    {
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
                        return $this->redirect(['user/login']);
                    }
                }
                catch(Exception $e)
                {
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->render('changepassword',[
                        'model'=>$model,
                        'userId'=>$userId,
                    ]);
                }

        }
        else
        {
            return $this->render('changepassword',[
                'model'=>$model,

            ]);
        }
    }
}
