<?php

namespace app\controllers;

use Yii;
use app\models\Menu\Relation;
use app\models\Menu\Search;
use app\models\Menu;
use yii\web\NotFoundHttpException;
use app\controllers\BaseController;
use yii\data\ActiveDataProvider;
use app\components\MenuHelper;
/**
 * MenuController implements the CRUD actions for Menu model.
 */
class MenuController extends BaseController {

    public function actions() {
        $query = Relation::find()->where(['status' => 1])->with('parentCategoryRelation');
        return [
            'datatables' => [
                'class' => 'app\datatable\DataTableAction',
                'query' => $query,
            ],
        ];
    }

    /**
     * Lists all Menu.
     * @return mixed
     */
    public function actionIndex() {
        
       
        $route=$this->route;
      $actionList = MenuHelper::getActionHelper($route);
     $visibleList = MenuHelper::getActionVisibleHelper($route);
          $searchModel = new Search();
         $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         $dataProvider->pagination = ['PageSize'=>10];
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'actionList' => $actionList,
                    'visibleList' => $visibleList
        ]);
    }

    /**
     * Displays a single Menu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Menu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Menu();
        $maxNum = Menu::find()->select(['max(position) as maxId'])->asArray()->one();
        $position = (empty($maxNum)) ? 1 : ($maxNum['maxId'] + 10);
        $model->position = $position;
        $model->level = 1;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Menu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->renderAjax('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Menu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeletes($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Menu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Menu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
