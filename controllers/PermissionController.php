<?php

namespace app\controllers;

use Yii;
use app\models\RoleChild;
use yii\web\NotFoundHttpException;
use app\controllers\BaseController;
use app\models\Menu;

/**
 * RoleController implements the CRUD actions for Role model.
 */
class PermissionController extends BaseController {

    const VD_PERMISSIONS = [
        'view',
        'datatables'
    ];
    const VDS_PERMISSIONS = [
        'view',
        'datatables',
        'statuslist',
        'jsoncalendar'
    ];
    
  

    /**
     * Lists all Permission models.
     * @return mixed
     */
    public function actionIndex2() {
        return $this->render('index');
    }

    // public function actionIndex() {
    //     //->andWhere(['!=', 'url', '#'])
    //     if (Yii::$app->request->get('role')) {
    //         $role = Yii::$app->request->get('role');
    //         $roleList = Menu::find()->where(['role_list_disp' => 1])->andWhere(['!=', 'type', 'report'])->andWhere(['=', 'status',1])->asArray()->all();
    //         $reportRoleList = Menu::find()->where(['role_list_disp' => 1])->andWhere(['=', 'type', 'report'])->andWhere(['=', 'status',1])->asArray()->all();
    //         $currentRoleList = RoleChild::find()->where(['parent' => $role])->asArray()->all();
    //         $childauthList = [];
    //         if (!empty($currentRoleList)) {
    //             foreach ($currentRoleList as $curData) {
    //                 $childauthList[] = $curData['child'];
    //             }
    //         }

    //         return $this->render('index', ['role' => $role, 'roleList' => $roleList, 'reportRoleList' => $reportRoleList, 'childauthList' => $childauthList]);
    //     } else {
    //         return $this->render('index', ['role' => '', 'roleList' => '']);
    //     }
    // }

    public function actionIndex($role = null)
    {

        //->andWhere(['!=', 'url', '#'])
        if (Yii::$app->request->get('role')) {

            $role = Yii::$app->request->get('role');
            // pr($role);
            $roleList = \app\models\Menu::find()->where(['role_list_disp' => 1])->andWhere(['!=', 'type', 'report'])->orderBy(['position'=>SORT_ASC])->asArray()->all();
            $reportRoleList = Menu::find()->where(['role_list_disp' => 1])->andWhere(['=', 'type', 'report'])->asArray()->all();
            $currentRoleList = RoleChild::find()->where(['parent' => $role])->asArray()->all();
            // pr($currentRoleList);
            // die();
            $childauthList = [];
            if (!empty($currentRoleList)) {

                foreach ($currentRoleList as $curData) {
                    $childauthList[] = isset($curData['child']) ? $curData['child'] :'';
                }
            }

            // pr($childauthList);

            return $this->render('index', ['role' => $role, 'roleList' => $roleList, 'reportRoleList' => $reportRoleList, 'childauthList' => $childauthList]);
        } else {
            // $role
            return $this->render('index', ['role' => '', 'roleList' => '']);
        }
    }



 
    public function actionRoleupdate()
    {
        if (!Yii::$app->request->isAjax) {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
        $action = Yii::$app->request->post('action');
        $child = Yii::$app->request->post('child');
        $parent = Yii::$app->request->post('parent');
        $menuId = Yii::$app->request->post('menuid');
        $item = Yii::$app->request->post('item');
        $controller = Yii::$app->request->post('controller');
        $authAddtional = Yii::$app->request->post('roleothers');
        $level = Yii::$app->request->post('level');
        $parentId = Yii::$app->request->post('parentid');

        if ($level == 3) {
            $getMainParentId = Menu::find()->where(['id' => $parentId])->asArray()->one();
            $ids = $menuId . "," . $parentId . "," . $getMainParentId['parent_id'];
        } else if ($level == 2) {
            $ids = $menuId . "," . $parentId;
        } else {
            $ids = $menuId;
        }

        $additonalauthlist = '';
        if (!empty($authAddtional)) {
            $additonalauthlist = explode(",", $authAddtional);
        }

        $model = new RoleChild();
        if ($action == 0) {
            $model->deleteAll(['AND', 'child = :child', ['IN', 'parent', $parent]], [':child' => $child]);
            if ($item == 'index') {
                $model->deleteAll(['AND', 'menu_others = :menu_others', ['IN', 'parent', $parent]], [':menu_others' => 1]);
            }
        } else if ($action == 1 && $item != 'index') {
            $model->child = $child;
            $model->parent = $parent;
            $model->menu_id = $menuId;
            $model->associated_ids = $ids;
            $model->save();
        } else {
            $model->child = $child;
            $model->parent = $parent;
            $model->menu_id = $menuId;
            $model->associated_ids = $ids;
            $model->save();
            if (!empty($additonalauthlist)) {
                foreach ($additonalauthlist as $additonalauthData) {
                    $additionalAuths = new RoleChild;
                    $additionalAuths->child = 'auth/' . $controller . '/' . $additonalauthData;
                    $additionalAuths->parent = $parent;
                    $additionalAuths->menu_id = $menuId;
                    $additionalAuths->menu_others = 1;
                    $additionalAuths->save();
                }
            }
        }
        $result = "Success";
        return json_encode($result);
    }

   

}
