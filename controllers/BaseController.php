<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class BaseController extends Controller
{
    /**
     * @inheritdoc
     */

    public $route;

    public function beforeAction($action)
    {
       $moduleId=$this->module->id;

        if (parent::beforeAction($action)) {



            $this->route = 'auth/' . Yii::$app->controller->id;
           
            return true; // or false if needed
        } else {
            return false;
        }
    }


    public function init() {
        date_default_timezone_set('Asia/Singapore');
        parent::init();
    }
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            $route = 'auth/';
                            // $route .= $action->controller->module->requestedRoute;
                            $route .= $action->controller->id."/".$action->id;
                            if(Yii::$app->user->can($route)){
                                return true;
                            }
                        }
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
}
