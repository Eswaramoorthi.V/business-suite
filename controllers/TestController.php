<?php

namespace app\controllers;
ob_start();
use yii\web\Controller;
// use Google\Spreadsheet\DefaultServiceRequest;
// use Google\Spreadsheet\ServiceRequestFactory;
// use Aspera\Spreadsheet\XLSX\Reader;
// use Aspera\Spreadsheet\XLSX\Worksheet;
use Krizalys\Onedrive\Onedrive;
use Yii;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use GuzzleHttp\ClientInterface;
class TestController extends Controller
{
    private $graph;
    private $httpClient;
    public $token;

     /**
     * @var string
     *      The base URL for authorization requests.
     */
    const AUTH_URL = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize';

    /**
     * @var string
     *      The base URL for token requests.
     */
    const TOKEN_URL = 'https://login.microsoftonline.com/common/oauth2/v2.0/token';

    /**
     * Constructor.
     *
     * @param string $clientId
     *        The client ID.
     * @param \Microsoft\Graph\Graph $graph
     *        The Microsoft Graph.
     * @param \GuzzleHttp\ClientInterface $httpClient
     *        The Guzzle HTTP client.
     * @param mixed $serviceDefinition
     *        The service definition. Not passing a
     *        \Krizalys\Onedrive\Definition\ServiceDefinitionInterface via this
     *        parameter is deprecated and will be disallowed in version 3.
     *        Passing a logger via this parameter is deprecated and will be
     *        disallowed in version 3.
     * @param mixed[string] $options
     *        The options to use while creating this object. Supported options:
     *          - `'state'` *(object)*: the OneDrive client state, as returned
     *            by `getState()`. Default: `[]`.
     *
     * @throws \Exception
     *         Thrown if `$clientId` is `null`.
     *
     * @since 1.0.0
     */
    
    public function init(){
        $this->enableCsrfValidation = false;
        $this->token='EwB4A8l6BAAUO9chh8cJscQLmU+LSWpbnr0vmwwAAdRY0TAvSAF0uz+wqTcN++PYI2jqvvciq5KirmwxFSEcHdKypE6ZOIWBYomkSc/HkUCJQ2vx0AOuX7/8z/A/rP1vIn9nNR5FIpju8BR+gYFDWir83Jf4wkD+AtHLrVn2RULvyDHr3EAH3WL0PXy0YUteSL9RAQeEtSyW8zQJsbMXKMmLfRBCV7ncEyaUviaxGNn4CPATXmfQebGEyAmqT/IlfJ2qeueOdnT4VxprjCj3911ES2wpp8hEp5HuGcaxvYtujdZc7tomQ4h9r32jvum0PdWqknUhWfNjhxtGXMouS3AFwSUgV/yU8y1heCYVh2NRwrKlgIvgNZhmiV8o+1wDZgAACCHEFQ0E38qYSAK3A3QaXcrPBZKncM8l7RuBPf03fDmS7XdXg30NgUeHZmchaSFc7r4sHUeh06eVuH8Uo9YfXGUhgMFEhi05jKJ2XBpF/l9GuF3TUs2Xn1ciqssYrP0rBXDldOPg/qaLzwMOM5y1n6h1A8vfXh3Q2l9dLSbXZX34L9T8MUOd4qLTCJ7Jks6Q7vrnJD81y+HrARKFv7zbCBVcCN4SCOFXUoceuiBE2nqP6jlw38si2TRwReALpbzgLI8RK1m6vTxiYKeA4rQHMEHJ2S+Ci+WyrfHlYwYBR/ogpGYS5XVEpjn/VlAmQ8F+fl4nn8YrddTquI9XJ2aPwfPUrz/arSL3AInj85izoaFUjEIP2732uaslcCeC+hqzZq1q88XuULV1exBRQX0gO490QdWePNOPufg55RCRnHL60/O8K41c/lQYRNIlMdcJzvFcSDXSgXXFs2G8DLcAqj+4OPusOrxZPVcZC5NKpADGviKbNcv+8D/zOFF6o8ystsbc/hCQj9s0gPkPBMQE3M7BHYW+avWTxvsQZyUdyE9xOX+kzsUW8alMwRZu9BnHwsLjW2exyHADxfmA8Xp5DMa+1JDGyhQkPn5t/CJATrCjtEe91C8A+5y9Md96trEVlx58HLgihASACW2adYsa1Vya6Tx8XogsYv3iKD6AfW5paHCJFbQg7V5310hPB0iuij9O6ZFFzNYvSyhwn23iLHsm2VYCv8OsrkU3sb1WMCWFpfGGxNEvLMe6kzn2cYzGZmLSxakF1KugwcisKCvXU7p9zJ8C';
    }
    public function actionIndex()
    {
        
        $serviceRequest = new DefaultServiceRequest("");
        ServiceRequestFactory::setInstance($serviceRequest);

        $spreadsheetService = new Google\Spreadsheet\SpreadsheetService();
        $worksheetFeed = $spreadsheetService->getPublicSpreadsheet("spreadsheet-id");
    }

    public function actionIndex2(){
        require('php-excel-reader/excel_reader2.php');

        require('SpreadsheetReader.php');

        $Reader = new SpreadsheetReader('example.xlsx');
        foreach ($Reader as $Row)
        {
            print_r($Row);
        }
    }
    public function actionIndex3(){

        $path= Yii::getAlias('@webroot').'/files/demo.xlsx';
       // die();
        //echo $url;


        $reader = new Reader();
        $reader->open($path);
        $sheets = $reader->getSheets();

        /** @var Worksheet $sheet_data */
        foreach ($sheets as $index => $sheet_data) {
            echo 'Sheet #' . $index . ': ' . $sheet_data->getName();

            $reader->changeSheet($index);


            foreach ($reader as $row) {
                pr1($row);
            }
        }

        $reader->close();
    }

    public function actionOneDrive(){
        $clientId="4211b374-6688-4219-9306-0ed22c14df25";
        $redirectURL="https://dev.jeyamsys.com/pricingcalculator/test/one-drive-response";
        $client = Onedrive::client($clientId);
        
        //pr($client);
        $url = $client->getLogInUrl([
            'files.read',
            'files.read.all',
            'files.readwrite',
            'files.readwrite.all',
            'offline_access',
        ], $redirectURL);

//session_start();

// Persist the OneDrive client' state for next API requests.
$_SESSION['onedrive.client.state'] = $client->getState();

// Redirect the user to the log in URL.
header('HTTP/1.1 302 Found', true, 302);
header("Location:$url");
die();
    }

        public function actionUpload(){
        
$file = $client->getRoot()->upload('hello.docx', 'Thanks for your updates JHello World!');
echo $file->download();

    }

    public function actionOneDriveResponse3(){
        $clientId="4211b374-6688-4219-9306-0ed22c14df25";
        $screet='6pkRt9AaO_hTc-L9mMdy_uS~57nKVG_tM.';
        $redirectURL="https://dev.jeyamsys.com/pricingcalculator/test/one-drive-response";
        echo "Asdasd";
        
        $state = (array) $_SESSION['onedrive.client.state'];
        $state['redirect_uri']=$redirectURL;
        $state1 = (object) $state;
        
        $client = Onedrive::client(
            $clientId,
            [
                // Restore the previous state while instantiating this client to proceed
                // in obtaining an access token.
                'state' => $state1,
                
            ]
        );
        // pr($client);
        $client->obtainAccessToken($screet,$_GET['code']);
        // pr($client->getState());
        
        //  die();

        $_SESSION['onedrive.client.state'] = $client->getState();
       $driveList=$client->fetchDocs();
       pr($driveList);
       die();
       foreach ($driveList as $key => $value) {
        
         echo $value->getName()."->".$value->getWebUrl();
         echo "<br>";

          

       }
       //die();
        $driveListData = $client->getDriveItemById('65ef573df7c99950');
        pr($driveListData);
        //die();
       // Past this point, you can start using file/folder functions from the SDK, eg:
            // Past this point, you can start using file/folder functions from the SDK, eg:
$file = $client->getRoot()->upload('hello.docx', 'Thanks for your updates JHello World!');
echo $file->download();

    }

    public function actionOneDriveResponse(){
        $clientId="4211b374-6688-4219-9306-0ed22c14df25";
        $screet='6pkRt9AaO_hTc-L9mMdy_uS~57nKVG_tM.';
        $redirectURL="https://dev.jeyamsys.com/pricingcalculator/test/one-drive-response";
        echo "Asdasd";
        
        $state = (array) $_SESSION['onedrive.client.state'];
        $state['redirect_uri']=$redirectURL;
        $state1 = (object) $state;
        
        $client = Onedrive::client(
            $clientId,
            [
                // Restore the previous state while instantiating this client to proceed
                // in obtaining an access token.
                'state' => $state1,
                
            ]
        );
        // pr($client);
        $client->obtainAccessToken($screet,$_GET['code']);
        pr($client->getState());
        
        //  die();

        $_SESSION['onedrive.client.state'] = $client->getState();
       $driveList=$client->getRecent();
       //pr($driveList);
       $token='EwB4A8l6BAAUO9chh8cJscQLmU+LSWpbnr0vmwwAASKGDs2bxLP2Yp3Rm8XEcoQ6bcCsXwWHh/KRyovChDEJWZ7yms9emHRdanc9APqmukLpz8+dTwL2kp7BSDEOCQhs3J5xLAYFe+vHMFrVAgZ+kY/8GMeP766Dtrw+ylWDC59Kw4HqViV0IE5evcxLEhhbiGuog8h+bMFoNJtsuypFhg9mlGMuuS5Qk6VepMSN+RNwv82Xp1FmgmmIXD6l0qbe8/x+geLn9KUzCm5R5inPPP7upfXSeuhChx7pxJnfStpAf4h4jw2c2B3N63E7edt2Ux1z128PFGkyJJOJeJttn4YvyqaU2y1FFO/zOXUZt/3r0Y8I8bChsPfHZ2FLAE0DZgAACOhwFfxg0AY+SAI9MTgvHgWVmOEauiG03km3kiSZdoU/O5epFRitjzmgL6aX6BKdeq5WMIkBrgt/oXwB/msfvwNrfZMN4eaF4z7hn/Qyd4TETp6aLAnPiWllpSyJmolsE7L8vSRYl2iN2/MVHGc7//aDuwRe708qDlEvPVH9JF7p49kYu7xzYltPBjNIZjnqInciwtAnFCmtNSwupx6m5qYgJOBoE3LbmovKlFfQguqy2GH/pJDTmbEjaELkVZKneutxrCC4KAHSdX5qxZFoIm2U7q+lO1hf1yj12YUfhRMtUyJRbQa9OEHFgPGci+QrmJMcZ29Xt3EN5Z4Bbf2eeiAmr6kaHKBdmLH5xvf35H/RXgKDsBW3G18wpvvh4mzP4qfdDcWrbieTkcKzS6pDJ8AyhxDxKNsMw2ED+LrjWWbvCDiAfbihEp31k6gZwyt/X45rdXVWXKejQ35A1tnh1TCdqJcDDA34pUGJ8tBiqHoIM75Z29IwUOCCqKpQ6+LrRqKnuCksUepWZzJ80RmmqEbriL/4YyaR7FeLJROTAc0/gfVq2RskU28g2ndpWtQDoeq5pwGt3z3qINWKQao+Tg65z3w5udtWVJ8PlSlaZ18D02ETJgWgF52UIwjwfLmK/llI+cjQA1J3JPcav8F3hkl2BOYjMrlr5ka8fqn9kGSwK84WE+kwJ88AFGBugRIPfbyrtwZliCFN9PYyjP8EdBGpMwbi0K2R9SpjRwdziOYuISQwgAyDeIEXuxSBC1+w+A6HxtTqi7ebT0sbp5u143lGaJ8C';
       foreach ($driveList as $key => $value) {
        // pr($value);
        // die();
        
         echo $value->getName()."->".$value->getWebUrl();
         echo "<br>";
         $url=$value->getWebUrl();
         $name=$value->getName();
        $url1=urlencode($url);
         echo "<iframe src='https://view.officeapps.live.com/op/embed.aspx?src=".$url1."' width='1366px' height='623px' frameborder='0'>This is an embedded <a target='_blank' href='http://office.com'>Microsoft Office</a> document, powered by <a target='_blank' href='http://office.com/webapps'>".$name."</a>.</iframe>";

          

       }
       die();
        $driveListData = $client->getDriveItemById('65ef573df7c99950');
        pr($driveListData);
        //die();
       // Past this point, you can start using file/folder functions from the SDK, eg:
            // Past this point, you can start using file/folder functions from the SDK, eg:
$file = $client->getRoot()->upload('hello.docx', 'Thanks for your updates JHello World!');
echo $file->download();

    }

    public function actionOneDrive1(){
        $clientId="4211b374-6688-4219-9306-0ed22c14df25";
        $screet='6pkRt9AaO_hTc-L9mMdy_uS~57nKVG_tM.';
        $redirectURL="https://jeyamsys.com/pricingcalculator/test/one-drive-response";
        echo "Asdasd";
        $code='M.R3_BL2.962ed004-7666-50e3-b801-08c9d63e501d';
        
        $state = (array) $_SESSION['onedrive.client.state'];
        $state['redirect_uri']=$redirectURL;
        $state1 = (object) $state;
        
        $client = Onedrive::client(
            $clientId,
            [
                // Restore the previous state while instantiating this client to proceed
                // in obtaining an access token.
                'state' => $state1,
                
            ]
        );
        // pr($client);
        $client->obtainAccessToken($screet,$_GET['code']);
        // pr($client->getState());
        
        //  die();

        $_SESSION['onedrive.client.state'] = $client->getState();
       $driveList=$client->getRecent();
       foreach ($driveList as $key => $value) {
           pr($value);
       }
       
        $driveListData = $client->getDriveItemById('65ef573df7c99950');
        pr($driveListData);
        die();
       // Past this point, you can start using file/folder functions from the SDK, eg:
            // Past this point, you can start using file/folder functions from the SDK, eg:
$file = $client->getRoot()->upload('hello.docx', 'Thanks for your updates JHello World!');
echo $file->download();

    }



    public function actionDrive(){
        
        $this->graph= new Graph();
        $this->graph->setAccessToken($this->token);
        //pr($this->graph);

        $driveLocator = '/me/drive';
        $endpoint     = "$driveLocator";

        $response = $this
            ->graph
            ->createRequest('GET', $endpoint)
            ->execute();

        $status = $response->getStatus();

        if ($status != 200) {
            throw new \Exception("Unexpected status code produced by 'GET $endpoint': $status");
        }

        $drive = $response->getResponseAsObject(Model\Drive::class);


    }
    public function actionDriveRoot(){
        
        $this->graph= new Graph();
        $this->graph->setAccessToken($this->token);
        //pr($this->graph);

        $driveLocator = '/drive/root/children';
        $endpoint     = "$driveLocator";
        $params=['viewType'=>'icons'];

        $response = $this
            ->graph
            ->createRequest('GET', $endpoint,$params)
            ->execute();

        $status = $response->getStatus();

        if ($status != 200) {
            throw new \Exception("Unexpected status code produced by 'GET $endpoint': $status");
        }

        $drive = $response->getResponseAsObject(Model\Drive::class);
        pr($drive);

    }
    public function actionIndex7(){
        $accessToken='EwB4A8l6BAAUO9chh8cJscQLmU+LSWpbnr0vmwwAAeGHHagczCaVNrzzqfRfjPSLKMskyKwBMg0I1ChDmYvCUDD2eks5cGKPkayXCHYlaVtAKR8JC6PmzHeZBnvUlZ+AtfE/wjmvuktTkkFGtUo88SOCDC7vXhf/uk3E6lGtL5lzrx4jUmPeRit3x4FWOWrtFU5m1lNGZYtB7YwP598cFLWxYqx9b8+PC1K3wKp9TglIS4g2rDxPbsDB+jUki1OOBxST9FPZjg6l+461lh4HNjFRuWBEUIxE2nWcyR43ylGSTffFSVGbJDcUZ/sHdjZl4S5xsD7XlVlqB2xDYzvGTmohHxk/PBBg0xqH6cQiI7okv8Fo7851fE/fiCsbqXcDZgAACKluqdy5lPjbSALri/FPmY+5JIrA9Z5Nz+CsMv7aVsLmWCyFn+KMhbX09Y2ZG9uvhHcGj5gr8i4+7milBnkeMmbEOHNSSIyXECX19wUdHOV8hE0wD+5R+MimSplhpXqSEfUKSkKzhr8BGJ51l+YDwKJ0P1q3uBJVMYcGlaTdcY/pq+KkWVIud3QKrO3zfgnwCmkXPIonj6LxIriT5e8SAulJije7DjOOBzt/YwrZSFYolQVu2GUGk9FFceBxIBVY8os8+b+rVj7Xr3zDiRElFYfb7LFgOL0CynIOq1s9eB9BZl8BbjqmIO9obqUhcKq/csnXdl4wVK/0pGbbXxfCqsO06bmbb0w6Q09RHmhGJJn5nuqGD7u8nytDNHztQj8WTv+oSg+Rs+z4NCrfoUZO1PfPFbAOkHpchfP87mI5ob8Tdzghdj4oylc6GyM9JVCb5iv0YYoaxKneZgl+8Pjj0r9j0AfrFFgReiNebShe1t5oNlh4PxQLC0JJOY1sXiOK3zkKDZ3MsCN4kxDAQAON01Y1d6Cqn13Q5h49BCZVMtT+C8fgDKHsvEPd1IWLx5sgjEQIcv14p6fJ8NTXaQfWvt8PUeWQLmI1acCkI8zQmbjkq67zsUsujxVBV8MgXjmNDoCPiSDrksQxVj1/jTTsoXsLGos7s05Kzcqtr5fkugiJ6IVBJAoKUyzvnRsI5SIIoGA8YQ/qIuSbg1FjXPd1vR9KH5IqF4k2UkFv4WZzGGONYcox6vUMAaT8bsv4mzerM3uxAPhT6vddC0nQDqf7k8YmYZ8C';
        $this->graph= new Graph();
        $this->graph->setAccessToken($accessToken);
        //pr($this->graph);
        $itemId='65EF573DF7C99950!113';
        $driveLocator = '/drive/items/'.$itemId."/children";
        $endpoint     = "$driveLocator";

        $response = $this
            ->graph
            ->createRequest('GET', $endpoint)
            ->execute();

        $status = $response->getStatus();

        if ($status != 200) {
            throw new \Exception("Unexpected status code produced by 'GET $endpoint': $status");
        }

        $drive = $response->getResponseAsObject(Model\Drive::class);
        pr($drive);

    }

    public function actionIndex8(){
        $accessToken='EwB4A8l6BAAUO9chh8cJscQLmU+LSWpbnr0vmwwAAeGHHagczCaVNrzzqfRfjPSLKMskyKwBMg0I1ChDmYvCUDD2eks5cGKPkayXCHYlaVtAKR8JC6PmzHeZBnvUlZ+AtfE/wjmvuktTkkFGtUo88SOCDC7vXhf/uk3E6lGtL5lzrx4jUmPeRit3x4FWOWrtFU5m1lNGZYtB7YwP598cFLWxYqx9b8+PC1K3wKp9TglIS4g2rDxPbsDB+jUki1OOBxST9FPZjg6l+461lh4HNjFRuWBEUIxE2nWcyR43ylGSTffFSVGbJDcUZ/sHdjZl4S5xsD7XlVlqB2xDYzvGTmohHxk/PBBg0xqH6cQiI7okv8Fo7851fE/fiCsbqXcDZgAACKluqdy5lPjbSALri/FPmY+5JIrA9Z5Nz+CsMv7aVsLmWCyFn+KMhbX09Y2ZG9uvhHcGj5gr8i4+7milBnkeMmbEOHNSSIyXECX19wUdHOV8hE0wD+5R+MimSplhpXqSEfUKSkKzhr8BGJ51l+YDwKJ0P1q3uBJVMYcGlaTdcY/pq+KkWVIud3QKrO3zfgnwCmkXPIonj6LxIriT5e8SAulJije7DjOOBzt/YwrZSFYolQVu2GUGk9FFceBxIBVY8os8+b+rVj7Xr3zDiRElFYfb7LFgOL0CynIOq1s9eB9BZl8BbjqmIO9obqUhcKq/csnXdl4wVK/0pGbbXxfCqsO06bmbb0w6Q09RHmhGJJn5nuqGD7u8nytDNHztQj8WTv+oSg+Rs+z4NCrfoUZO1PfPFbAOkHpchfP87mI5ob8Tdzghdj4oylc6GyM9JVCb5iv0YYoaxKneZgl+8Pjj0r9j0AfrFFgReiNebShe1t5oNlh4PxQLC0JJOY1sXiOK3zkKDZ3MsCN4kxDAQAON01Y1d6Cqn13Q5h49BCZVMtT+C8fgDKHsvEPd1IWLx5sgjEQIcv14p6fJ8NTXaQfWvt8PUeWQLmI1acCkI8zQmbjkq67zsUsujxVBV8MgXjmNDoCPiSDrksQxVj1/jTTsoXsLGos7s05Kzcqtr5fkugiJ6IVBJAoKUyzvnRsI5SIIoGA8YQ/qIuSbg1FjXPd1vR9KH5IqF4k2UkFv4WZzGGONYcox6vUMAaT8bsv4mzerM3uxAPhT6vddC0nQDqf7k8YmYZ8C';
        $this->graph= new Graph();
        $this->graph->setAccessToken($accessToken);
        //pr($this->graph);
        $itemId='65EF573DF7C99950!421';
        $driveLocator = '/me/drive/items/'.$itemId.'/content';
        $endpoint     = "$driveLocator";

        $response = $this
            ->graph
            ->createRequest('GET', $endpoint)
            ->execute();

        $status = $response->getStatus();

        if ($status != 200) {
            throw new \Exception("Unexpected status code produced by 'GET $endpoint': $status");
        }

        $drive = $response->getResponseAsObject(Model\Drive::class);
        pr($drive);

    }

    public function actionIndex6(){
       $accessToken='EwB4A8l6BAAUO9chh8cJscQLmU+LSWpbnr0vmwwAAeGHHagczCaVNrzzqfRfjPSLKMskyKwBMg0I1ChDmYvCUDD2eks5cGKPkayXCHYlaVtAKR8JC6PmzHeZBnvUlZ+AtfE/wjmvuktTkkFGtUo88SOCDC7vXhf/uk3E6lGtL5lzrx4jUmPeRit3x4FWOWrtFU5m1lNGZYtB7YwP598cFLWxYqx9b8+PC1K3wKp9TglIS4g2rDxPbsDB+jUki1OOBxST9FPZjg6l+461lh4HNjFRuWBEUIxE2nWcyR43ylGSTffFSVGbJDcUZ/sHdjZl4S5xsD7XlVlqB2xDYzvGTmohHxk/PBBg0xqH6cQiI7okv8Fo7851fE/fiCsbqXcDZgAACKluqdy5lPjbSALri/FPmY+5JIrA9Z5Nz+CsMv7aVsLmWCyFn+KMhbX09Y2ZG9uvhHcGj5gr8i4+7milBnkeMmbEOHNSSIyXECX19wUdHOV8hE0wD+5R+MimSplhpXqSEfUKSkKzhr8BGJ51l+YDwKJ0P1q3uBJVMYcGlaTdcY/pq+KkWVIud3QKrO3zfgnwCmkXPIonj6LxIriT5e8SAulJije7DjOOBzt/YwrZSFYolQVu2GUGk9FFceBxIBVY8os8+b+rVj7Xr3zDiRElFYfb7LFgOL0CynIOq1s9eB9BZl8BbjqmIO9obqUhcKq/csnXdl4wVK/0pGbbXxfCqsO06bmbb0w6Q09RHmhGJJn5nuqGD7u8nytDNHztQj8WTv+oSg+Rs+z4NCrfoUZO1PfPFbAOkHpchfP87mI5ob8Tdzghdj4oylc6GyM9JVCb5iv0YYoaxKneZgl+8Pjj0r9j0AfrFFgReiNebShe1t5oNlh4PxQLC0JJOY1sXiOK3zkKDZ3MsCN4kxDAQAON01Y1d6Cqn13Q5h49BCZVMtT+C8fgDKHsvEPd1IWLx5sgjEQIcv14p6fJ8NTXaQfWvt8PUeWQLmI1acCkI8zQmbjkq67zsUsujxVBV8MgXjmNDoCPiSDrksQxVj1/jTTsoXsLGos7s05Kzcqtr5fkugiJ6IVBJAoKUyzvnRsI5SIIoGA8YQ/qIuSbg1FjXPd1vR9KH5IqF4k2UkFv4WZzGGONYcox6vUMAaT8bsv4mzerM3uxAPhT6vddC0nQDqf7k8YmYZ8C';
        $this->graph= new Graph();
        $this->graph->setAccessToken($accessToken);
        //pr($this->graph);
        $ItemId="65ef573df7c99950";
        $driveLocator = '/me/drive/items/'.$ItemId.'/content';
        $endpoint     = "$driveLocator";

        $response = $this
            ->graph
            ->createRequest('GET', $endpoint)
            ->execute();

        $status = $response->getStatus();

        if ($status != 200) {
            throw new \Exception("Unexpected status code produced by 'GET $endpoint': $status");
        }

        $drive = $response->getResponseAsObject(Model\Drive::class);
        pr($drive);

    }
     public function actionPreview(){
       
        $this->graph= new Graph();
        $this->graph->setAccessToken($this->token);
        //pr($this->graph);
        $ItemId="65EF573DF7C99950!403";
        $driveId='65ef573df7c99950';
        $driveLocator = '/drives/'.$driveId.'/items/'.$ItemId.'/preview';
        $endpoint     = "$driveLocator";

        $response = $this
            ->graph
            ->createRequest('POST', $endpoint)
            ->execute();

                pr($response);
        $status = $response->getStatus();

        

        $drive = $response->getResponseAsObject(Model\Drive::class);
        pr($drive);

    }
    public function actionRenewToken()
    {
        $clientId="4211b374-6688-4219-9306-0ed22c14df25";
        $clientSecret='6pkRt9AaO_hTc-L9mMdy_uS~57nKVG_tM.';
        $refershToken='M.R3_BL2.CSJe1KSfwvrbBKC9FNo2A0DcHqVeyxRgkhLyOX*BzefIgOaIBW3SmoOFXau0xHEnOGdFWxOveRUtiNGercUBjBynB4rExT2dNh3mFstSFzi9nY6DY5JeN0!lZiP6wIH3rcRSX9zH6DsJT9snNbNTsbFpiP55eR*USbhW25ZrJE0bhXpFZcYWFzTC7i2DEruWaD6t!xeOxS02dUjXGR4l0nE7oJ2HVtSkckb91XQcaS9ztEQUAqc6dIZQMM0U1!EJDZveUu8gGbrnTE*WhmMgKt1069d0eClDGl9ajE9PJsYADz9817N*sItErABoN*0R9wZoXlrKQHl4ayFiIq3QvyBREJb!m1ceI4m5eMv5vtWR6WPsouG*fMeaa*aIQRTApuC0Asjf5*UfLg4r70oE1fLMffXL0P5rwqTbMP5miLSVnPsJ0fLbnfz9W2xY6bIymqMQjNysVFCmwVYuodt6JspKWj5Qp9ncfsZe74WLON9a';

        $values = [
            'client_id'     => $clientId,
            'client_secret' => $clientSecret,
            'grant_type'    => 'refresh_token',
            'refresh_token' => $refershToken,
        ];

        // //$this->httpClient =   ClientInterface;
        // $response = ClientInterface::post(
        //     self::TOKEN_URL,
        //     ['form_params' => $values]
        // );

        $client = new \GuzzleHttp\Client();
$response = $client->post(
    self::TOKEN_URL,
    ['form_params' => $values]
);

        $body = (string) $response->getBody();
        $data = json_decode($body);
        pr($data);

        
        //$this->graph->setAccessToken($this->_state->token->data->access_token);
    }

    public function actionKeyword(){
        $url="https://keywordtool.io/search/keywords/google/3273933?category=web&keyword=plants&country=IN&language=en-IN";
        $values=[];
        try{
            $client = new \GuzzleHttp\Client();
            $response = $client->get(
                $url
            // ['form_params' => $values]
            );

            $body = (string) $response->getBody();
            $data = json_decode($body);
            pr($data);
        }catch (\Exception $e){
            pr($e);
        }


    }


}
