<?php

namespace app\models;

use app\models\BaseModel;



class Drawing extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%drawing}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          
           
            [['description','document'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => 'Description',
            'document' => 'Document',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'system_datetime' => 'System Datetime',
        ];
    }


}
