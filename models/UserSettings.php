<?php

namespace app\models;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * Role Access.
 */
class UserSettings extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
    */
    public static function tableName() {
        return '{{%user}}';
    }
    
    public function checkOldPassword($role) {
        $roleCheck = Role::find()->where("id=1 and password_hash='$role'")->asArray(true)->one();
        return $roleCheck;
    }

}
