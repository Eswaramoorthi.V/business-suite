<?php

namespace app\models;

use app\models\BaseModel;



class Loa extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%loa}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
          
           
            [['description','document','loa_amount'], 'safe'],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => 'Description',
            'document' => 'Attachment',
            'loa_amount'=>'LOA Amount',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'system_datetime' => 'System Datetime',
        ];
    }


}
