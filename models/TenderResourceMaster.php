<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tender_resource_master".
 *
 * @property int $id
 * @property int $tender_id
 * @property int $resource_type
 * @property int $material_id
 * @property int $total_quantity
 * @property int $purchased_quantity
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_by
 * @property string $updated_at
 */
class TenderResourceMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tender_resource_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tender_id', 'resource_type','material_id', 'total_quantity', 'purchased_quantity'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by','updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tender_id' => 'Tender ID',
            'resource_type' => 'Resource Type',
            'material_id' => 'Material ID',
            'total_quantity' => 'Total Quantity',
            'purchased_quantity' => 'Purchased Quantity',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
