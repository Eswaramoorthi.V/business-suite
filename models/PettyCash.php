<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "petty_cash".
 *
 * @property integer $id

 * @property string $description
 * @property string $credit_expense
 * @property float $credit_amount
 * @property float $expense_amount
 * @property float $cash_balance
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class PettyCash extends BaseModel
{
  
    public static function tableName()
    {
        return '{{%petty_cash}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [[ 'employee_id','project_id','supplier_id','name','item_number','document','amount','vat','payment_mode','date','type','description','credit_expense','credit_amount','cash_balance','expense_amount',], 'safe'],
           
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id'=>'Employee',
            'payment_mode'=>'Payment Mode',
            'date'=>'Date',
            'description'=>'Description',
            'credit_expense'=>'Credit/Expense',
            'credit_amount'=>'Credited',
            'expense_amount'=>'Expense',
            'cash_balance'=>'Balance',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['petty_cash_id' => 'id']);
    }
     public function getEmployeeName()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

   
}
