<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property integer $area_code
 * @property string $area_name
 * @property integer $category_master_id
 * @property string $remark
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class SubContractor extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sub_contractor}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
           
             [['project_id','name','date','description','vat','reference_number','document','po','po_amount','payment_terms','sub_total','discount_amount','discount_rate'], 'safe'],
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    
    public function getSubcontractorpayment()
    {
        return $this->hasOne(SubcontractorPayment::className(), ['subcontractor_id' => 'id']);
    }
    
    public function getLpo()
    {
        return $this->hasMany(Lpo::className(), ['subcontractor_id' => 'id']);
    }
     public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'name']);
    }
}
