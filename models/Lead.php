<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_lead".
 *
 * @property int $id
 * @property string $lead_name
 * @property string $lead_description
 * @property int $lead_type_id

 * @property int $lead_status_id
 * @property string $lead_status_description
 * @property int $lead_source_id
 * @property string $lead_source_description
 * @property int $opportunity_amount
 * @property string $do_not_call
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property int $added_at
 * @property int $updated_at
 * @property int $customer_id
 */
class Lead extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_lead';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'lead_name', 'lead_description', 'lead_status_id',  'first_name', 'last_name', 'phone', 'mobile', 'fax'], 'required'],
            [['email','do_not_call', ],'safe'],
            [['id', 'lead_type_id',  'lead_status_id', 'lead_source_id', 'opportunity_amount', 'created_at', 'updated_at', 'customer_id'], 'integer'],
            [['lead_description', 'lead_status_description', 'lead_source_description'], 'string'],
            [['lead_name', 'email', 'first_name', 'last_name', 'phone', 'mobile', 'fax'], 'string', 'max' => 255],
            [['do_not_call'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lead_name' => 'Lead Name',
            'lead_description' => 'Lead Description',
            'lead_type_id' => 'Lead Type ID',
            
            'lead_status_id' => 'Lead Status ID',
            'lead_status_description' => 'Lead Status Description',
            'lead_source_id' => 'Lead Source ID',
            'lead_source_description' => 'Lead Source Description',
            'opportunity_amount' => 'Opportunity Amount',
            'do_not_call' => 'Do Not Call',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'fax' => 'Fax',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'customer_id' => 'Customer ID',
        ];
    }
    public function getLeadtype()
    {
        return $this->hasOne(LeadType::className(), ['id' => 'lead_type_id']);
    }
    public function getLeadStatus()
    {
        return $this->hasOne(LeadStatus::className(), ['id' => 'lead_status_id']);
    }
    public function getLeadSource()
    {
        return $this->hasOne(LeadSource::className(), ['id' => 'lead_source_id']);
    }
}
