<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_delivery".
 *
 * @property int $id
 * @property int $po_id
 * @property int $material_id
 * @property string $description
 * @property int $material_status
 * @property string $delivery_date
 * @property int $order_quantity
 * @property int $delivered_quantity
 * @property int $remaining_quantity
 * @property string $remarks
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 */
class PurchaseDelivery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchase_delivery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['delivery_status', 'delivery_date', 'order_quantity','project_id' ], 'required'],
            [[ 'material_id', 'delivery_status', 'order_quantity', 'delivered_quantity', 'remaining_quantity'], 'integer'],
            [['po_id','remarks', 'created_at', 'created_by', 'updated_at', 'updated_by','delivered_quantity', 'remaining_quantity','description','purchase_delivery_id','supplier_name','delivery_order_number'], 'safe'],
            [['description', 'remarks', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'po_id' => 'Purchase ID',
            'purchase_delivery_id' => 'Purchase Delivery ID',
            'material_id' => 'Material ID',
            'description' => 'Description',
            'supplier_name'=>'Supplier Name',
            'delivery_order_number'=>'Delivery Order Number',
            'delivery_status' => 'Delivery Status',
            'delivery_date' => 'Delivery Date',
            'oredr_quantity' => 'Order Quantity',
            'delivered_quantity' => 'Delivered Quantity',
            'remaining_quantity' => 'Remaining Quantity',
            'received_quantity' => 'Received Quantity',
            'remarks' => 'Remarks',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function getDeliveryDetails()
    {
        return $this->hasMany(PurchaseDeliveryStatus::className(), ['purchase_delivery_id' => 'id']);
    }
}
