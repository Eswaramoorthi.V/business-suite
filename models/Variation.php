<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property integer $area_code
 * @property string $area_name
 * @property integer $category_master_id
 * @property string $remark
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class Variation extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%variation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
           
             [['loa_id','project_id','description','vo_amount','vo_document'], 'safe'],
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project Id',
            'description'=> 'Description',
            'vo_amount'  => 'VO Amount',
            'vo_document'=> 'VO Document',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    
   
}
