<?php

namespace app\models;
/**
 * Role Child.
 */
class AuthAssignment extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_assignment';
    }
    
    public function getUser()
    {
        return $this->hasOne(UserInfo::className(), ['id' => 'user_id']);
    }
    
}
