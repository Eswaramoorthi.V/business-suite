<?php

namespace app\models;
/**
 * Role Child.
 */
class RoleChild extends \yii\db\ActiveRecord {
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item_child';
    }
    
}
