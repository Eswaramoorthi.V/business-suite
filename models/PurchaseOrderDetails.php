<?php

namespace app\models;

use app\models\PurchaseDelivery;
use Yii;

/**
 * This is the model class for table "purchase_master".
 *
 * @property int $id
 * @property int $purchase_id
 * @property string $material_id
 * @property string $description
 * @property double $quantity
 * @property string $unit
 * @property double $price
 * @property double $total_amount
 * @property int $created_by
 * @property string $created_on
 * @property int $updated_by
 * @property string $updated_on
 * @property int $project_id
 */
class PurchaseOrderDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchase_order_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['purchase_id',  'price', 'total_amount'], 'required'],
            [['purchase_id', 'created_by', 'updated_by', 'project_id'], 'integer'],
            [['quantity', 'price', 'total_amount'], 'number'],
            [['created_on', 'updated_on','project_id','description','material_id',], 'safe'],
            [[ 'description', 'unit'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchase_id' => 'Purchase ID',
            'material_id' => 'Material ID',
            'description' => 'Description',
            'quantity' => 'Quantity',
            'unit' => 'Unit',
            'price' => 'Price',
            'total_amount' => 'Total Amount',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'project_id' => 'Project ID',
        ];
    }

    public function getDelivery()
    {
        return $this->hasOne(PurchaseDelivery::className(), ['po_id' => 'purchase_id']);
    }



}
