<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_lead_type".
 *
 * @property int $id
 * @property string $type
 * @property string $label
 * @property int $active
 * @property int $sort_order
 * @property int $added_at
 * @property int $updated_at
 */
class LeadType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_lead_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'type', 'label', 'active', 'sort_order'], 'required'],
            [['id', 'active', 'sort_order', 'added_at', 'updated_at'], 'integer'],
            [['type', 'label'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'label' => 'Label',
            'active' => 'Active',
            'sort_order' => 'Sort Order',
            'added_at' => 'Added At',
            'updated_at' => 'Updated At',
        ];
    }
}
