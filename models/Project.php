<?php

namespace app\models;
use app\models\EmployeeCompany;
use Yii;

/**
 * This is the model class for table "project".
 *
 * @property int $id
 * @property string $project_name
 * @property string $project_code
 * @property string $location
 * @property string $start_date
 * @property string $end_date
 * @property int $status
 * @property int $project_type
 * @property double $amount
 * @property string $description
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property string $system_datetime
 */
class Project extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status','consultant','country'], 'safe'],
            [[ 'created_on', 'updated_on', 'system_datetime','property_account_type'], 'safe'],
            [['status', 'project_type'], 'safe'],
            [['amount'], 'number'],
            [['project_code','building_type','project_type','project_name','project_status','client_name','tender_due_date','total_area'], 'required'],
            [[ 'project_code',], 'string', 'max' => 500],
            [['description'], 'string', 'max' => 1000],

             
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_name' => 'Project Name',
            'project_code' => 'Project Code',
            'status' => 'Status',
            'project_type' => 'Project Type',
            'amount' => 'Amount',
            'description' => 'Description',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'system_datetime' => 'System Datetime',
        ];
    }


    public function assignCompany()
    {
        // pr("jhhvhj");
        $session = Yii::$app->session;
        // $currentUserId = Yii::$app->user->identity->id;
        $currentUserId=Yii::$app->user->getId();
        $assignCompany= EmployeeCompany::find()->with('company')->where(['user_id'=>$currentUserId])->orderBy(['id'=>SORT_ASC])->one();
        if (!empty($assignCompany)) {

            $session['company_id'] = $assignCompany['company_id'];
            $session['company_name'] = isset($assignCompany['company']['company_name'])?$assignCompany['company']['company_name']:"";

    }

    }

}
