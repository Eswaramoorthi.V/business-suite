<?php

namespace app\models;

use Yii;
use app\models\PurchaseOrderDetails;
/**
 * This is the model class for table "purchase_request_details".
 *
 * @property int $id
 * @property int $purchase_request_master_id
 * @property int $material_id
 * @property int $material_request_quantity
 * @property int $material_received_quantity
 * @property string $material_request_received_date
 * @property int $purchase_order_id
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_by
 * @property string $updated_at
 */
class PurchaseRequestDetails extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchase_request_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['purchase_request_master_id', 'material_id',  ], 'required'],
            [['purchase_request_master_id', 'material_id', 'material_request_quantity', 'material_total_quantity','material_received_quantity', 'purchase_order_id'], 'integer'],
            [['material_request_received_date', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchase_request_master_id' => 'Purchase Request Master ID',
            'material_id' => 'Material ID',
            'material_request_quantity' => 'Material Request Quantity',
            'material_received_quantity' => 'Material Received Quantity',
            'material_request_received_date' => 'Material Request Received Date',
            'purchase_order_id' => 'Purchase Order ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
    

    public function getPurchaseOrderDetails()
    {
        return $this->hasOne(PurchaseOrderDetails::className(), ['id' => 'request_order_id']);
    }

}
