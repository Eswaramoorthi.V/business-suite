<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tree".
 *
 * @property int $id
 * @property int $root
 * @property int $lft
 * @property int $rgt
 * @property int $lvl
 * @property string $name
 * @property string $date
 * @property string $fa_icons
 * @property int $rev
 * @property int $for
 * @property string $icon
 * @property int $icon_type
 * @property int $selected
 * @property int $disabled
 * @property int $readonly
 * @property int $visible
 * @property int $collapsed
 * @property int $movable_u
 * @property int $movable_d
 * @property int $movable_l
 * @property int $movable_r
 * @property int $removable
 * @property int $removable_all
 * @property int $child_allowed
 * @property int $status
 * @property int $added_at
 * @property int $updated_at
 * @property int $updated_by
 * @property int $added_by
 */
class Tree extends \kartik\tree\models\Tree
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_document_tree';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['root', 'lft', 'rgt', 'lvl', 'rev', 'for', 'icon_type', 'selected', 'disabled', 'readonly', 'visible', 'collapsed', 'movable_u', 'movable_d', 'movable_l', 'movable_r', 'removable', 'removable_all', 'child_allowed',  'added_at', 'updated_at', 'updated_by', 'added_by'], 'integer'],
            // [['name','code'], 'required'],
            [['fa_icons'], 'string'],
            [['name'], 'string', 'max' => 60],
            [['date'], 'string', 'max' => 256],
            [['icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root' => 'Root',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'lvl' => 'Lvl',
            'name' => 'Name',
            'date' => 'Date',
            'fa_icons' => 'Fa Icons',
            'rev' => 'Rev',
            'for' => 'For',
            'icon' => 'Icon',
            'icon_type' => 'Icon Type',
            'selected' => 'Selected',
            'disabled' => 'Disabled',
            'readonly' => 'Readonly',
            'visible' => 'Visible',
            'collapsed' => 'Collapsed',
            'movable_u' => 'Movable U',
            'movable_d' => 'Movable D',
            'movable_l' => 'Movable L',
            'movable_r' => 'Movable R',
            'removable' => 'Removable',
            'removable_all' => 'Removable All',
            'child_allowed' => 'Child Allowed',
            // 'status' => 'Status',
            'added_at' => 'Added At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'added_by' => 'Added By',
        ];
    }
}
