<?php

namespace app\models\Menu;
use app\models\Menu;

class Relation extends Menu
{
    public function fields()
    {
        return array_merge(parent::fields(), ['parentCategory']);
    }

    public function getParentCategory()
    {
        if(is_null($this->parentCategoryRelation)) {
            return '-';
        }

        return $this->parentCategoryRelation->name;
    }

}
