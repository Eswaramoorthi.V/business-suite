<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings_tree".
 *
 * @property int $id
 * @property int $root
 * @property int $lft
 * @property int $rgt
 * @property int $lvl
 * @property string $name
 * @property string $description
 * @property int $parent_id
 * @property string $code
 * @property string $fa_icons
 * @property int $rev
 * @property int $for
 * @property string $icon
 * @property int $icon_type
 * @property int $selected
 * @property int $disabled
 * @property int $readonly
 * @property int $visible
 * @property int $collapsed
 * @property int $movable_u
 * @property int $movable_d
 * @property int $movable_l
 * @property int $movable_r
 * @property int $removable
 * @property int $removable_all
 * @property int $child_allowed
 * @property int $active
 * @property int $status
 * @property int $added_at
 * @property int $updated_at
 * @property int $updated_by
 * @property int $added_by
 */
class SettingsTree extends \kartik\tree\models\Tree
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings_tree';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['root', 'lft', 'rgt', 'lvl', 'parent_id', 'rev', 'for', 'icon_type', 'selected', 'disabled', 'readonly', 'visible', 'collapsed', 'movable_u', 'movable_d', 'movable_l', 'movable_r', 'removable', 'removable_all', 'child_allowed', 'active', 'status', 'added_at', 'updated_at', 'updated_by', 'added_by'], 'integer'],
            [['lft', 'rgt', 'lvl', 'name'], 'safe'],
            [['fa_icons'], 'string'],
            [['name', 'code', 'icon'], 'string', 'max' => 256],
            [['description'], 'string', 'max' => 1000],
            [['name','code'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'root' => 'Root',
            'lft' => 'Lft',
            'rgt' => 'Rgt',
            'lvl' => 'Lvl',
            'name' => 'Name',
            'description' => 'Description',
            'parent_id' => 'Parent ID',
            'code' => 'Code',
            'fa_icons' => 'Fa Icons',
            'rev' => 'Rev',
            'for' => 'For',
            'icon' => 'Icon',
            'icon_type' => 'Icon Type',
            'selected' => 'Selected',
            'disabled' => 'Disabled',
            'readonly' => 'Readonly',
            'visible' => 'Visible',
            'collapsed' => 'Collapsed',
            'movable_u' => 'Movable U',
            'movable_d' => 'Movable D',
            'movable_l' => 'Movable L',
            'movable_r' => 'Movable R',
            'removable' => 'Removable',
            'removable_all' => 'Removable All',
            'child_allowed' => 'Child Allowed',
            'active' => 'Active',
            'status' => 'Status',
            'added_at' => 'Added At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'added_by' => 'Added By',
        ];
    }
}
