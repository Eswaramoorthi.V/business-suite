<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_lead_source".
 *
 * @property int $id
 * @property string $source
 * @property string $label
 * @property int $active
 * @property int $sort_order
 * @property int $added_at
 * @property int $updated_at
 */
class LeadSource extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_lead_source';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'source', 'label', 'active', 'sort_order'], 'required'],
            [['id', 'active', 'sort_order', 'added_at', 'updated_at'], 'integer'],
            [['source', 'label'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'source' => 'Source',
            'label' => 'Label',
            'active' => 'Active',
            'sort_order' => 'Sort Order',
            'added_at' => 'Added At',
            'updated_at' => 'Updated At',
        ];
    }
}
