<?php

namespace app\models;

use Yii;
use app\models\ProjectEstimate;

/**
 * This is the model class for table "project_estimate_master".
 *
 * @property int $id
 * @property int $project_id
 * @property int $activity_id
 * @property int $sub_activity_id
 * @property int $unit_id
 * @property int $quantity
 * @property double $unit_rate
 * @property double $total_cost
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class ProjectEstimateMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_estimate_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'activity_id', 'sub_activity_id', 'unit_id', 'quantity', 'unit_rate', 'total_cost','description'], 'safe'],
            [['project_id', 'activity_id', 'sub_activity_id', 'unit_id', 'quantity', 'status', 'created_by', 'updated_by'], 'safe'],
            // [['unit_rate', 'total_cost'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'activity_id' => 'Activity ID',
            'sub_activity_id' => 'Sub Activity ID',
            'unit_id' => 'Unit ID',
            'quantity' => 'Quantity',
            'unit_rate' => 'Unit Rate',
            'total_cost' => 'Total Cost',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

public function getProjectestimate() {
        return $this->hasMany(ProjectEstimate::className(), ['project_master_id'=>'id']);
    }
   
}
