<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "quotation_items".
 *
 * @property integer $id
 * @property string $description
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class QuotationItems extends BaseModel
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return '{{%quotation_items}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [['section_id','quantity','price','description','unit','total_amount','options'], 'safe'],
           
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            
        ];
    }
    
   
}
