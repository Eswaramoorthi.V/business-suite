<?php
namespace app\models;

use dektrium\user\models\Profile as BaseProfile;

class Profile extends BaseProfile{

    public function rules()
    {
        $rules = parent::rules();
        //pr($rules);
        // add some rules
        $rules['first_name'] = ['first_name', 'required'];
        $rules['middle_name'] = ['middle_name', 'safe'];
        $rules['last_name'] = ['last_name', 'required'];
        $rules['secondary_contact_no'] = ['secondary_contact_no', 'safe'];
        $rules['hint_question'] = ['hint_question', 'required'];
        $rules['hint_answer'] = ['hint_answer', 'required'];
        return $rules;
    }

//    public function save($runValidation = true, $attributeNames = null)
//    {
//
//        $attributeNames=$_REQUEST['Profile'];
//        return parent::save($runValidation, $attributeNames); // TODO: Change the autogenerated stub
//    }

}
