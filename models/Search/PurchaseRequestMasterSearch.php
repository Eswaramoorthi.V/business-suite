<?php

namespace app\models\Search;

use app\models\Project;
use app\models\PurchaseOrderDetails;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PurchaseRequestMaster;

/**
 * Search represents the model behind the search form about `app\models\Project`.
 */
class PurchaseRequestMasterSearch extends PurchaseRequestMaster
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id','reference_name','requested_date','requested_by'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PurchaseRequestMaster::find()->with('project')->orderBy(['id' => SORT_DESC]);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
          
        ]);

        $query->andFilterWhere(['like', 'reference_name', $this->reference_name]);
            
            
        return $dataProvider;
    }

    
}
