<?php


namespace app\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Lead;


/**
 * This is the model class for table "tbl_lead".
 *
 * @property int $id
 * @property string $lead_name
 * @property string $lead_description
 * @property int $lead_type_id

 * @property int $lead_status_id
 * @property string $lead_status_description
 * @property int $lead_source_id
 * @property string $lead_source_description
 * @property int $opportunity_amount
 * @property string $do_not_call
 * @property string $email
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $mobile
 * @property string $fax
 * @property int $added_at
 * @property int $updated_at
 * @property int $customer_id
 */
class LeadSearch extends Lead
{


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'lead_name', 'lead_description', 'lead_status_id', 'do_not_call', 'email', 'first_name', 'last_name', 'phone', 'mobile', 'fax','lead_code'], 'safe'],
            [['id', 'lead_type_id',  'lead_status_id', 'lead_source_id', 'opportunity_amount', 'created_at', 'updated_at', 'customer_id'], 'integer'],
            [['lead_description', 'lead_status_description', 'lead_source_description'], 'string'],
            [['lead_name', 'email', 'first_name', 'last_name', 'phone', 'mobile', 'fax'], 'string', 'max' => 255],
            [['do_not_call'], 'string', 'max' => 1],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lead_name' => 'Lead Name',
            'lead_description' => 'Lead Description',
            'lead_type_id' => 'Lead Type ID',
           
            'lead_status_id' => 'Lead Status ID',
            'lead_status_description' => 'Lead Status Description',
            'lead_source_id' => 'Lead Source ID',
            'lead_source_description' => 'Lead Source Description',
            'opportunity_amount' => 'Opportunity Amount',
            'do_not_call' => 'Do Not Call',
            'email' => 'Email',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'phone' => 'Phone',
            'mobile' => 'Mobile',
            'fax' => 'Fax',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'customer_id' => 'Customer ID',
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        // $query = Lead::find()->joinWith('leadtype');

        $query = Lead::find()->joinWith('leadtype')->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'invoice_date'=>$this->invoice_date,

        ]);
         $query->andFilterWhere(['like', 'lead_name', $this->lead_name])
                ->andFilterWhere(['like', 'lead_code', $this->lead_code])
                
                 ->andFilterWhere(['like', 'opportunity_amount', $this->opportunity_amount]);

          return $dataProvider;

    }
}
