<?php

namespace app\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Accounts;



class AccountsSearch extends Accounts
{
    /**
     * @inheritdoc
     */
   
    public function rules()
    {
        return [
          //  [['id', 'created_by',], 'integer'],
            [['date','description','credit_debit','amount','paid_by','reference_no','credit_amount','debit_amount'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Accounts::find();
    //    pr($query);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
           
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'invoice_date'=>$this->invoice_date,
            
        ]);

        $query->andFilterWhere(['like', 'date', $this->date])
              ->andFilterWhere(['like', 'description', $this->description])
              ->andFilterWhere(['like', 'credit_amount', $this->credit_amount])
              ->andFilterWhere(['like', 'debit_amount', $this->debit_amount])
              ->andFilterWhere(['like', 'amount', $this->amount]);
     
        return $dataProvider;
    }
}
