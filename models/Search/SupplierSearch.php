<?php

namespace app\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;


class SupplierSearch extends Customer
{
    /**
     * @inheritdoc
     */
   
    public function rules()
    {
        return [
            [['type','company_name','customer_fname','email','phone_number','customer_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Customer::find()->where(['customer_type'=>3])->orderBy(['id' => SORT_DESC]);
       
      

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
           
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
          
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
                ->andFilterWhere(['like', 'company_name', $this->company_name])
                ->andFilterWhere(['like', 'customer_fname', $this->customer_fname])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'phone_number', $this->phone_number])
                ->andFilterWhere(['like', 'customer_type', $this->customer_type]);
                
        return $dataProvider;
    }
}
