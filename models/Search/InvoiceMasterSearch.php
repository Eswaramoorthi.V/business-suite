<?php

namespace app\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\InvoiceMaster;
//use app\models\Invoice;

/**
 * WorkorderEvaluationSearch represents the model behind the search form about `app\models\WorkorderEvaluation`.
 */
class InvoiceMasterSearch extends InvoiceMaster
{
    /**
     * @inheritdoc
     */
    public $total_amount,$project_name;
    public function rules()
    {
        return [
            [['id', 'created_by'], 'integer'],
            [['invoice_date', 'created_on', 'updated_on','total_amount','project_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvoiceMaster::find()-> joinWith(['project']);
       
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
           
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'invoice_date'=>$this->invoice_date,
            
        ]);

        $query->andFilterWhere(['like', 'invoice_date', $this->invoice_date])
                ->andFilterWhere(['like', 'project.project_name', $this->project_name])
                ->andFilterWhere(['like', 'total_amount', $this->total_amount]);
                
        return $dataProvider;
    }
}
