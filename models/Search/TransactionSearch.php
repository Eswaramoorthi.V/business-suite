<?php

namespace app\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transaction;



class TransactionSearch extends Transaction
{
    /**
     * @inheritdoc
     */
   
    public function rules()
    {
        return [
          //  [['id', 'created_by',], 'integer'],
           [[ 'date','trans_mode','payment_mode','employee_id','project_id','invoice_number','credit_amount','particulars',
               'expense_amount','vat','expense_total_amount','document','subcontractor_id','lpo_no','pettycash_amount','vehicle'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$startDate, $endDate)
    {
        $query = Transaction::find()->joinWith('projectName')
                ->joinWith('employeeName')
                ->andwhere(['>=','date', $startDate])->andWhere(['<=','date', $endDate])
                ->andwhere(['!=','trans_mode',4])
            ->andwhere(['!=','trans_mode',6])
            ->andwhere(['!=','trans_mode',7])
                ->orderBy(['id' => SORT_DESC]);
     
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
           
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'invoice_date'=>$this->invoice_date,
            
        ]);

        $query->andFilterWhere(['like', 'date', $this->date])
                
                ->andFilterWhere(['like', 'project.project_name', $this->project_id])
                ->andFilterWhere(['like', 'particulars', $this->particulars])
                 ->andFilterWhere(['like', 'pettycash_amount', $this->pettycash_amount])
                ->andFilterWhere(['like', 'amount', $this->expense_amount])
                ->andFilterWhere(['like', 'expense_amount', $this->expense_total_amount]);
     
        return $dataProvider;
    }
}
