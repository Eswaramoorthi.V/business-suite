<?php

namespace app\models\Search;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PettyCash;
use app\models\Transaction;



class PettyCashSearch extends PettyCash
{
    /**
     * @inheritdoc
     */
   
    public function rules()
    {
        return [
          //  [['id', 'created_by',], 'integer'],
           [[ 'date','trans_mode','payment_mode','employee_id','project_id','invoice_number','credit_amount','expense_amount','vat','expense_total_amount','document','subcontractor_id','lpo_no','pettycash_amount'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
     public function search($params)
    {
        $query = Transaction::find()->where(['!=','employee_id',''])->andwhere(['!=','trans_mode',4])
                ->groupby(['employee_id']);
     
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
           
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'invoice_date'=>$this->invoice_date,
            
        ]);

        $query->andFilterWhere(['like', 'date', $this->date])
                
                ->andFilterWhere(['like', 'employee.first_name', $this->employee_id]);
               
     
        return $dataProvider;
    }
}
