<?php

namespace app\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AdhocProject;


/**
 * WorkorderEvaluationSearch represents the model behind the search form about `app\models\WorkorderEvaluation`.
 */
class AwardedprojectSearch extends AdhocProject
{
    /**
     * @inheritdoc
     */
   
    public function rules()
    {
        return [
             [['customer_id','project_name','project_code'], 'safe'],
             [['customer_contact','description','status','start_date','amount','probability','end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdhocProject::find()->andWhere(['project_type'=>'1'])->orderBy(['id' => SORT_DESC])
                ->joinwith('customer');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
           
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            //'invoice_date'=>$this->invoice_date,
            
        ]);

        $query->andFilterWhere(['like', 'project_name', $this->project_name])
        ->andFilterWhere(['like', 'customer.company_name', $this->customer_id])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'project_code', $this->project_code])
                ->andFilterWhere(['like', 'start_date', $this->start_date])
                 ->andFilterWhere(['like', 'end_date', $this->end_date])
                ->andFilterWhere(['like', 'status', $this->status])
                ->andFilterWhere(['like', 'amount', $this->amount]);
       // pr($query);
        return $dataProvider;
        
    }
}
