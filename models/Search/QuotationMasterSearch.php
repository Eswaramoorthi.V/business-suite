<?php

namespace app\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\QuotationMaster;


class QuotationMasterSearch extends QuotationMaster
{
    /**
     * @inheritdoc
     */
   
    public function rules()
    {
       
       
         return [
          
           [[ 'discount_amount','quotation_date','quotation_number','total_amount','name'], 'safe'],
        ];


    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    public function search($params,$id)
    {
        $query = QuotationMaster::find()->where(['project_id' => $id])->orderBy(['id' => SORT_DESC]);
       // $query = QuotationMaster::find()->where(['project_id' => $id]);

      // sql($query,1);
    // die();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
           
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
          
        ]);

        $query->andFilterWhere(['like', 'discount_amount', $this->discount_amount])
                ->andFilterWhere(['like', 'quotation_date', $this->quotation_date])
                ->andFilterWhere(['like', 'quotation_number', $this->quotation_number])
                 ->andFilterWhere(['like', 'total_amount', $this->total_amount]) ->andFilterWhere(['like', 'name', $this->name]);
                
        return $dataProvider;
    }
}
