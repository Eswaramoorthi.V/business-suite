<?php

namespace app\models\Search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PettyCash;
use Yii;

class BalanceHistorySearch extends PettyCash {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            //  [['id', 'created_by',], 'integer'],
            [['date', 'description', 'payment_mode', 'credit_amount', 'expense_amount', 'type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $startDate, $endDate) {
        $id = $_GET['id'];



        $query = Transaction::find()->Where(['employee_id' => $id])->andwhere(['in', ['trans_mode', 'payment_mode'], [
                                ['trans_mode' => 1, 'payment_mode' => 2],
                                ['trans_mode' => 2, 'payment_mode' => 3],
                                ['trans_mode' => 2, 'payment_mode' => 1],
                    ]])
                        ->andwhere(['>=', 'date', $startDate])->andWhere(['<=', 'date', $endDate])->orderBy(['date' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'date', $this->date])
                ->andFilterWhere(['like', 'type', $this->type])
                ->andFilterWhere(['like', 'description', $this->description])
                ->andFilterWhere(['like', 'credit_amount', $this->credit_amount])
                ->andFilterWhere(['like', 'expense_amount', $this->expense_amount]);

        return $dataProvider;
    }

}
