<?php

namespace app\models\Search;

use app\models\EmployeeProject;
use app\models\Tender;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

use yii\helpers\ArrayHelper;

/**
 * Search represents the model behind the search form about `app\models\Project`.
 */
class TenderSearch extends Tender
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tender_name', 'status', 'project_type', 'amount','total_area'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //pr($params);
             $project_type= isset($params['project_type']) ? $params['project_type'] :"";
             $total_area= isset($params['total_area']) ? $params['total_area'] :"";
             $total_amount= isset($params['total_amount']) ? $params['total_amount'] :"";
             $building_type=isset($params['building_type'])?$params['building_type']:"";

        // $userId=Yii::$app->user->id;

        // $rmpProjects=EmployeeProject::find()->where(['user_id'=>$userId])->asArray()->all();
        // $projectIds = ArrayHelper::getColumn($rmpProjects, 'project_id');

        // $query = Tender::find()->where(['id'=>$projectIds])->orderBy(['id' => SORT_DESC]);
        $query = Tender::find()->orderBy(['id' => SORT_DESC]);
       
        if($project_type !=''){

            $query = $query->andWhere(['project_type'=>$project_type]);


        } if( $total_area!=''){

        $query = $query->andWhere(['total_area'=>$total_area]);

        }
        if($building_type !='' ){

            $query = $query->andWhere(['building_type'=>$building_type]);


        }
        if($total_amount !='' ){
            $query = $query->andWhere(['amount'=>$total_amount]);

        }

        //sql($query);

        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }

        $query->andFilterWhere([ 'id' => $this->id, ]);

        $query->andFilterWhere(['like', 'tender_name', $this->tender_name])
            // ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'amount', $this->amount]);
            
        return $dataProvider;
    }
    
}
