<?php

namespace app\models\Search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GeneralSettings;

/**
 * Search represents the model behind the search form about `app\models\Project`.
 */
class SettingsSearch extends GeneralSettings
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tax_no', 'tel_no'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GeneralSettings::find()->orderBy(['id' => SORT_DESC]);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }


        $query->andFilterWhere(['like', 'fax_no', $this->fax_no])
            ->andFilterWhere(['like', 'tel_no', $this->tel_no]);
           
            
            
        return $dataProvider;
    }
    
}
