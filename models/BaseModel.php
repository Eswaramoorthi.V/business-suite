<?php

namespace app\models;


use yii\db\ActiveRecord;
use yii\db\Expression;
use dektrium\user\models\User;
use yii\helpers\ArrayHelper;
use Yii;
use app\models\Menu\Relation;

class BaseModel extends \yii\db\ActiveRecord
{

    var $primaryKey = 'id';
    var $serial_no;

    public function fields()
    {
        $fields = array_merge(parent::fields(), ['serial_no']);
        return $fields;

    }
    public   function getNotCompanyList(){
        $tableList=['company','tree','settings_tree','adhoc_project','quotation_master','menu'];
        return $tableList;
    }

    public static function find($findEnable = 1)
    {
        if ($findEnable != 1) {
            return parent::find();
        } else {

            $moduleName = Yii::$app->controller->module->id;
            $parentclass = get_called_class();

            $modelClass = substr(strrchr($parentclass, "\\"), 1);
            // pr($modelClass);
            $className =$parentclass;
            // pr($className);
            $tableName = $className::getTableSchema()->name;
            $roleName = isset($_SESSION['user_role_name']) ? $_SESSION['user_role_name'] : 0;
            //$role=Yii::$app->getUser()->identity->role;

// pr($_SESSION);
            $companyId=isset($_SESSION['company_id'])? $_SESSION['company_id'] :"";
            $tableList = self::getNotCompanyList();
            // pr($tableList);
           // if()
            if (class_exists($className) and !in_array($tableName,$tableList)) {
                return parent::find()->andWhere(['company_id' => $companyId]);
            } else {
                return parent::find();
            }


        }

//            if (in_array($modelClass, $IdRelatedTables)) {
//                if ((Yii::$app->params['user_role'] != 'admin')) {
//                    return parent::find()->andWhere(['IN', $tableName.'.id', $ProjectList])->andWhere([$tableName.'.delete_status' => 0]);
//                } else {
//                    return parent::find()->andWhere([$tableName. '.delete_status' => 0]);
//                }
//            } else if (in_array($modelClass, $projectIdRelatedTables)) {
//                if ((Yii::$app->params['user_role'] != 'admin')) {
//                    return parent::find()->andWhere(['IN', $tableName.'.project_id', $ProjectList])->andWhere([$tableName.'.delete_status' => 0]);
//                } else {
//                    return parent::find()->andWhere([$tableName.'.delete_status' => 0]);
//                }
//            } else if (in_array($modelClass, $projectsiteIdRelatedTables)) {
//
//                if ((Yii::$app->params['user_role'] == 'admin')) {
//                    return parent::find()->andWhere([$tableName.'.delete_status' => 0]);
//                }
//                else if (($_SESSION['manage_by']==true) ||  (in_array($roleName ,$specialRoles)))  {
//                    return parent::find()->andWhere(['IN', $tableName. '.project_id', $ProjectList])->andWhere([$tableName.'.delete_status' => 0]);
//                } else {
//                    return parent::find()->andWhere(['IN',$tableName. '.location_id', $SiteList])->andWhere([$tableName.'.delete_status' => 0]);
//                }
//            }
//            else if (in_array($modelClass, $siteIdRelatedTables)) {
//                if ((Yii::$app->params['user_role'] == 'admin') ||($_SESSION['manage_by']==true)||  (in_array($roleName ,$specialRoles)) ) {
//                    return parent::find()->andWhere([$tableName.'.delete_status' => 0]);
//
//                } else {
//                    return parent::find()->andWhere(['IN',$tableName. '.location_id', $SiteList])->andWhere([$tableName.'.delete_status' => 0]);
//
//                }
//            }
//            else if (in_array($modelClass, $locationRelatedTables)) {
//                if ((Yii::$app->params['user_role'] == 'admin')||($_SESSION['manage_by']==true) ||  (in_array($roleName ,$specialRoles))) {
//                    return parent::find()->andWhere([$tableName.'.delete_status' => 0]);
//
//                } else {
//                    return parent::find()->andWhere(['IN',$tableName. '.id', $SiteList])->andWhere([$tableName.'.delete_status' => 0]);
//
//                }
//            }
//            else if (in_array($modelClass, $basicTables)) {
//                return parent::find()->andWhere([$tableName.'.delete_status' => 0]);
//            }else{
//                return parent::find();
//            }
//        }

    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\BlameableBehavior',
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('CURDATE()'),
            ],
        ];
    }
    public function beforeSave($insert)
    {

        $tableList = $this->getNotCompanyList();

        if (parent::beforeSave($insert)) {
            $tableName=$this->tableName();
            pr($tableName);
            die();

            if(!in_array($tableName,$tableList)){
                // pr("if");
                $this->company_id=1;
                return true;
            }

        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function filterData($data)
    {
        $d = [];
        if (!empty($data)) {
            foreach ($data as $s) {
                if (!isset($d[$s['location_id']])) {
                    $d[$s['location_id']] = $s;
                } else {
                    if ($d[$s['location_id']]['time_stamp'] < $s['time_stamp']) {
                        $d[$s['location_id']] = $s;
                    }
                }
            }
        }
        $datavalues = array_values($d);
        return $datavalues;
    }

    /**
     * Creates data provider instance with search query applied
     * @return ActiveDataProvider
     */
    public function getPoleName()
    {
        return $this->hasOne(AssetMaster::className(), ['pole_id' => 'location_id']);
    }

    /**
     * Calculate Remaining Days
     *
     * @param string $startDate
     * @param string $endDate
     *
     * @return string $remaingHours
     */
    public static function getRemainingHours($startDate, $endDate)
    {
        $startdateValue = date_create($startDate);
        $endDateValue = date_create($endDate);
        $remainInterval = date_diff($startdateValue, $endDateValue);
        $remainingHours = (intval($remainInterval->format('%r%H')) + 1 > 0) ? intval($remainInterval->format('%r%a')) + 1 : 0;
        return $remainingHours;
    }

    public function applyIndex($data, $field)
    {
        $result = ArrayHelper::index($data, $field);
        return $result;
    }

    public function getYearDirectory()
    {
        $dateDirectory = [];
        for ($i = 1; $i <= 12; $i++) {
            $temp = [];
            if (strlen($i) == 1) {
                $i = "0" . $i;
            }
            $temp['start'] = date("Y-$i-01");
            $temp['end'] = date('Y-m-t', strtotime($temp['start']));
            $dateDirectory[] = $temp;
        }

        return $dateDirectory;
    }

    public function getMonthlyList()
    {
        $dateDirectory = [];
        for ($i = 1; $i <= 12; $i++) {
            if (strlen($i) == 1) {
                $i = "0" . $i;
            }

            $dateDirectory[] = $i;
        }

        return $dateDirectory;
    }

    public function getDateDiff($d1, $d2)
    {

//        $d1 = date_create($d1);
//        $d2 = date_create($d2);
//        $diff1 = date_diff($d2,$d1);
//        $hour = $diff1->h;
//        $temp['Response Time (hr)'] = $hour;

        $date1 = new \DateTime($d1);
        $date2 = new \DateTime($d2);
        $diff = $date2->diff($date1);
        $hours = $diff->h;
        $hours = $hours + ($diff->days * 24);
        return $hours;
    }

    public function getLasrThreeYears()
    {
        $yearsList = [];
        for ($i = 0; $i < 3; $i++) {
            $yearsList[] = date("Y", strtotime("-$i year"));
        }
        return $yearsList;
    }

}
