<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_resource".
 *
 * @property int $id
 * @property int $project_id
 * @property string $resource_type
 * @property int $material_id
 * @property int $resource_quantity
 * @property int $created_by
 * @property string $created_at
 * @property int $updated_by
 * @property string $updated_at
 */
class ProjectResource extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_resource_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'resource_type', 'material_id'], 'required'],
            [['project_id', 'material_id', 'total_quantity','purchased_quantity', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['resource_type'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'resource_type' => 'Resource Type',
            'material_id' => 'Material ID',
            'resource_quantity' => 'Resource Quantity',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
