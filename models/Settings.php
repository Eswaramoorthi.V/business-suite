<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "category_master".
 *
 * @property integer $id
 * @property string $code
 * @property string $description
 * @property integer $status
 * @property integer $parent_id
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class Settings extends BaseModel {

    const SETTINGS_STATUS_ENABLED = 1;
    const SETTINGS_STATUS_DISABLED = 0;

    public function init() {
        $this->status = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%settings}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['status', 'parent_id'], 'integer'],
                [['code', 'description', 'position','controller','action','icon', 'level'], 'safe'],
                //[['name','position'], 'unique'],
                [['name'], 'required'],
                // [['role_list','role_list_disp','menu_disp','type'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' =>'Description',
            'status' => 'Status',
            'parent_id' => 'Parent',
            'code' => 'Code',
            'menu_disp'=> 'Display In Menu',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSettings() {
        $categories = static::find()
                ->where([
            'status' => Settings::SETTINGS_STATUS_ENABLED
        ]);

        if ($this->id) {
            $categories->andWhere(['!=', 'id', $this->id]);
        }

        return $categories->all();
    }

    public function getParentCategoryRelation() {
        return $this->hasOne(Settings::className(), ['id' => 'parent_id']);
    }

    public function getChildCategories() {
        return $this->hasMany(Settings::className(), ['parent_id' => 'id']);
    }

}
