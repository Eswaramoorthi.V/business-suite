<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Employee;

/**
 * Search represents the model behind the search form about `app\models\Project`.
 */
class EmployeeSearch extends Employee
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name','employee_id','trade','accounts','mobile_number','basic_salary','allowance','hire_date','visa_expirydate','role'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Employee::find()->orderBy(['id' => SORT_DESC]);
        
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            return $dataProvider;
        }

        $query->andFilterWhere([ 'id' => $this->id, ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
        ->andFilterWhere(['like', 'employee_id', $this->employee_id])
        ->andFilterWhere(['like', 'trade', $this->trade])
        ->andFilterWhere(['like', 'role', $this->role])
        ->andFilterWhere(['like', 'mobile_number', $this->mobile_number])
        ->andFilterWhere(['like', 'basic_salary', $this->basic_salary])
        ->andFilterWhere(['like', 'allowance', $this->allowance])
        ->andFilterWhere(['like', 'hire_date', $this->hire_date])
        ->andFilterWhere(['like', 'visa_expirydate', $this->visa_expirydate]);
            
            
        return $dataProvider;
    }
    
}
