<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tender".
 *
 * @property int $id
 * @property string $tender_name
 * @property string $tender_code
 * @property int $status
 * @property int $project_type
 * @property int $amount
 * @property int $client_name
 * @property string $country
 * @property string $tender_due_date
 * @property int $building_type
 * @property string $consultant
 * @property int $total_area
 * @property string $reference
 * @property int $property_account_type
 * @property int $project_status
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property int $company_id
 */
class Tender extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tender';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tender_code', 'tender_name', 'tender_type','tender_status','building_type','total_area','client_name',], 'required'],
            [['status', 'amount',  'tender_account_type', 'company_id'], 'integer'],
            [['tender_due_date', 'created_on', 'updated_on'], 'safe'],
            [[  'country', 'consultant', 'reference'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tender_name' => 'Tender Name',
            'tender_code' => 'Tender Code',
            'status' => 'Status',
            'tender_type' => 'Tender Type',
            'amount' => 'Amount',
            'client_name' => 'Client Name',
            'country' => 'Country',
            'tender_due_date' => 'Tender Due Date',
            'building_type' => 'Building Type',
            'consultant' => 'Consultant',
            'total_area' => 'Total Area',
            'reference' => 'Reference',
            'tender_account_type' => 'Tender Account Type',
            'tender_status' => 'Tender Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'company_id' => 'Company ID',
        ];
    }
}
