<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "general_settings".
 *
 * @property int $id
 * @property string $email_id
 * @property string $fax_no
 * @property int $tel_no
 * @property string $po_box_no
 * @property string $logo
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $updated_at
 */
class GeneralSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'general_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tel_no'], 'integer'],
            [['email_id','company_name','fax_no','logo','po_box_no','tel_no','active'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['email_id', 'fax_no', 'po_box_no', 'logo', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email_id' => 'Email ID',
            'fax_no' => 'Fax No',
            'tel_no' => 'Tel No',
            'po_box_no' => 'Po Box No',
            'logo' => 'Logo',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
