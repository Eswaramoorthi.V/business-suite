<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tender_estimate".
 *
 * @property int $id
 * @property int $tender_id
 * @property int $tender_master_id
 * @property string $resource_type
 * @property double $resource_unit_cost
 * @property double $resource_total_cost
 * @property int $status
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_by
 * @property string $updated_at
 */
class TenderEstimate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tender_estimate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tender_id', 'tender_master_id', 'status'], 'integer'],
            [['resource_unit_cost', 'resource_total_cost','resource_type'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [[ 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tender_id' => 'Tender ID',
            'tender_master_id' => 'Tender Master ID',
            'resource_type' => 'Resource Type',
            'resource_unit_cost' => 'Resource Unit Cost',
            'resource_total_cost' => 'Resource Total Cost',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }
}
