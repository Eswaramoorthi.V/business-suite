<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "quotation_section".
 *
 * @property integer $id
 * @property string $section_name
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class QuotationSection extends BaseModel
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return '{{%quotation_section}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [['quotation_master_id','section_name','item_total_amount','multiple_item'], 'safe'],
           
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    public function getItems()
    {
        return $this->hasMany(QuotationItems::className(), ['section_id' => 'id']);
    }
   
}
