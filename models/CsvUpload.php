<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "csv_upload".
 *
 * @property int $id
 * @property string $parent_class
 * @property string $child_class
 * @property string $code
 * @property string $type
 * @property string $description
 * @property string $uom
 * @property string $cost_code
 * @property string $cost_head
 * @property string $detail
 * @property int $parent
 */
class CsvUpload extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'csv_upload';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'code', 'description',  'parent','name'], 'safe'],
            [['description'], 'string'],
            // [['parent'], 'integer'],
            [['code'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
           

            'code' => 'Code',
            
            'description' => 'Description',
            // 'uom' => 'Uom',
            // 'cost_code' => 'Cost Code',
            // 'cost_head' => 'Cost Head',
            // 'detail' => 'Detail',
            'parent' => 'Parent',
        ];
    }
}
