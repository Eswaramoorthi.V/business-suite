<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "labour_history".
 *
 * @property int $id
 * @property int $master_id
 * @property int $unit_id
 * @property double $cost
 * @property string $on_date
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 */
class LabourHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'labour_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['master_id', 'unit_id', 'cost', 'on_date'], 'required'],
            [['master_id', 'unit_id', 'status', 'created_by'], 'integer'],
            [['cost'], 'number'],
            [['on_date', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'master_id' => 'Master ID',
            'unit_id' => 'Unit ID',
            'cost' => 'Cost',
            'on_date' => 'On Date',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
