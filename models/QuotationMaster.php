<?php

namespace app\models;

use app\models\BaseModel;
use app\models\QuotationSection;

use app\models\QuotationItems;
/**
 * This is the model class for table "quotation_master".
 *
 * @property integer $id
 * @property string $description
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class QuotationMaster extends BaseModel
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return 'quotation_master';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['client_id','project_id'], 'required'],
            [['client_address','status','quotation_date','description','subtotal','discount_rate','quotation_number'], 'safe'],
             [['name'], 'required'],
            [['discount_amount','total_amount','vat','document','payment_terms','exclusion_text','notes','completion_period','validity','price','signature'], 'safe'],
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
     public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'client_id']);
    }
    public function getSection()
    {
        return $this->hasMany(QuotationSection::className(), ['quotation_master_id' => 'id']);
    }
    public function getItems()
    {
        return $this->hasMany(QuotationItems::className(), ['master_id' => 'id']);
    }
}
