<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "accounts".
 *
 * @property integer $id

 * @property string $description
 * @property string $credit_debit
 * @property integer $amount
 * @property string $paid_by
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class Accounts extends BaseModel
{
  
    public static function tableName()
    {
        return '{{%accounts}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [[ 'date','description','amount','credit_debit','credit_amount','debit_amount','reference_no'], 'safe'],
           
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date'=>'Date',
            'description'=>'Description',
            'reference_no'=>'Reference No',
            'credit_debit'=>'Credit/Debit',
            'credit_amount'=>'Credited',
            'debit_amount'=>'Debited',
            'amount'=>'Amount',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    
    
    

   
}
