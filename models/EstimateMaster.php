<?php

namespace app\models;

use app\models\SettingsTree;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EstimateMaster;
use app\modules\estimate\models\Settings;


/**
 * This is the model class for table "estimate_master".
 *
 * @property int $id
 * @property int $activity_id
 * @property int $sub_activity_id
 * @property int $unit_id
 * @property int $quantity
 * @property string $description
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class EstimateMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estimate_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activity_id', 'sub_activity_id', 'unit_id', 'quantity'], 'safe'],
            [['activity_id', 'sub_activity_id', 'unit_id', 'quantity', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activity_id' => 'Activity ID',
            'sub_activity_id' => 'Sub Activity ID',
            'unit_id' => 'Unit ID',
            'quantity' => 'Quantity',
            'description' => 'Description',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    public function search($params)
    {
        $query = EstimateMaster::find()->with('subactivity')->with('activity')->orderBy(['id' => SORT_DESC]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        return $dataProvider;
    }

    public function getActivity()
    {
        return $this->hasOne(SettingsTree::className(), ['id' => 'activity_id']);
    }

    public function getSubactivity()
    {
        return $this->hasOne(SettingsTree::className(), ['id' => 'sub_activity_id']);
    }

    public function getEstimateDetails()
    {
        return $this->hasMany(EstimateDetails::className(), ['estmate_id' => 'id']);
    }
}
