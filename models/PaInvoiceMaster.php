<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property integer $area_code
 * @property string $area_name
 * @property integer $category_master_id
 * @property string $remark
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class PaInvoiceMaster extends BaseModel
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return '{{%pa_invoice_master}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [['project_id','invoice_date','due_date','payment_terms','paid_type','subtotal','discount_rate','customer_address','invoice_name','invoice_number'], 'safe'],
            [['discount_amount','total_amount','delivery_contact','delivery_address','po_number','notes','document','subtotal_label','previous_certified_amount'], 'safe'],
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_date'=>'Invoice Date',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    
    public function getPaInvoice()
    {
        return $this->hasMany(PaInvoice::className(), ['invoice_id' => 'id']);
    }
    
     public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
    
    public function getCategory()
    {
        return $this->hasOne(CategoryMaster::className(), ['id' => 'payment_terms']);
    }
   
    public function getContact()
    {
        return $this->hasOne(Contacts::className(), ['id' => 'delivery_contact']);
    }
    
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
    public function getPayment()
    {
        return $this->hasOne(PaInvoicePayment::className(), ['invoice_id' => 'id']);
    }
    
//    public function getItem(){
//        $product =\app\models\Product::find()->asArray()->all();
//$productlist = [];
//foreach ($product as $productData) {
//    
//    $productlist[$productData['id']."-p"] = ($productData['name']) ? $productData['name'] . " [Product] ": '';
//    
//}
////pr($productlist);
//$services =\app\models\service::find()->asArray()->all();
//$serviceslist = [];
//foreach ($services as $servicesData) {
//    
//    $serviceslist[$servicesData['id']."-s"] = ($servicesData['name']) ? $servicesData['name'] . " [Services] ": '';
//    
//}
////pr($serviceslist);
// foreach ($serviceslist as $key=> $val) {
//   $productlist[$key] = $val;
//     return $productlist;
//}
////pr($productlist);
//    }
}
