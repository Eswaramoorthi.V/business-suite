<?php
namespace app\models;
use Yii;
use yii\data\ArrayDataProvider;
class UserInfo extends \dektrium\user\models\User
{
    // public  $usergroup;
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = 'user_group_id';
        $scenarios['update'][]   = 'user_group_id';
        $scenarios['register'][] = 'user_group_id';

        $scenarios['create'][]   = 'user_department_id';
        $scenarios['update'][]   = 'user_department_id';
        $scenarios['register'][] = 'user_department_id';
        
        $scenarios['create'][]   = 'primary_contact_no';
        $scenarios['update'][]   = 'primary_contact_no';
        $scenarios['register'][] = 'primary_contact_no';
        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        // add some rules
        $rules['userGroup'] = ['user_group_id', 'required'];
        $rules['userGroup'] = ['user_group_id', 'required'];
        $rules['userGroup1']   = ['user_group_id', 'integer', 'max' => 2];

        $rules['userDept'] = ['user_department_id', 'required'];
        $rules['userDept1']   = ['user_department_id', 'integer', 'max' => 10];
        
        $rules['primaryContact'] = ['primary_contact_no', 'required'];
        $rules['id'] = ['id', 'safe'];
        $rules['email'] = ['email', 'required'];
        $rules['password_hash']=['password', 'match', 'pattern' => '/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/', 'message' => 'Your password should contain atleast one number and one special character and it should be minimum 6'];
        
        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'user_group_id' => 'SMS Group',
            'user_department_id' => 'Department',
            'primary_contact_no'=>'Mobile',
            'id'=>'Id'

        ];
    }

    public function getUserActivity($startDate,$endDate,$periodType)
    {
        $startDate1= strtotime($startDate);
        $endDate1= strtotime($endDate);
        $connection = Yii::$app->getDb();
        $sql="SELECT [user].*,[audit_log].[requested_by] FROM [user] LEFT JOIN [audit_log] ON [user].[id] = [audit_log].[requested_by] WHERE ([last_login_at] > '$startDate1') AND ([last_login_at] < '$endDate1') and audit_log.action_name='login' AND audit_log.requested_on>'$startDate'";
        $command = $connection->createCommand($sql);
        $result = $command->queryAll(); //sql($q1,1);
        //pr($result);
        $userReformatData=self::reformatActivityData($result);
        $provider = new ArrayDataProvider([
            'allModels' => $userReformatData,
            'pagination' => false,
        ]);


        return $provider;

    }

    public function reformatActivityData($data){
        $userData=[];
        if(!empty($data)){
            $k=1;
            foreach ($data as $d){
                if(isset($userData[$d['id']])){
                    $userData[$d['id']]['number_of_time_login']=$userData[$d['id']]['number_of_time_login']+1;
                }else{
                    $temp=[];
                    $temp['S/No']=$k;
                    $temp['User Full Name']=$d['username'];
                    $temp['Last Login']=date('d/m/y',$d['last_login_at']);
                    $d1=date('Y-m-d',$d['last_login_at']);
                    $d2=date('Y-m-d');
                    $dStart = new \DateTime($d1);
                    $dEnd  = new \DateTime($d2);
                    $dDiff = $dStart->diff($dEnd);
                    //$dDiff->format('%R'); // use for point out relation: smaller/greater


                    $temp['Account Status']=($d['blocked_at']==0)?'Enable':'Disable';
                    $temp['number_of_time_login']=1;
                    $temp['Number of Days Since Last Logoin']=$dDiff->days;;
                    $userData[$d['id']]=$temp;
                    $k++;
                }
            }
        }

        $userData=array_values($userData);

        return $userData;

    }


}
