<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "petty_cash".
 *
 * @property integer $id

 * @property string $description
 * @property string $credit_expense
 * @property float $credit_amount
 * @property float $expense_amount
 * @property float $cash_balance
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class Transaction extends \yii\db\ActiveRecord
{
  
    public static function tableName()
    {
        return '{{%transaction}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [[ 'date','trans_mode','payment_mode','type','ref_code','description','client_id','employee_id','project_id','supplier_id','particulars',
                'invoice_number','credit_amount','expense_total_amount','vat','expense_amount','document','pettycash_amount','subcontractor_id','lpo_no','vehicle'], 'safe'],
           
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id'=>'Employee',
            'payment_mode'=>'Payment Mode',
            'date'=>'Date',
            'description'=>'Description',
            'credit_expense'=>'Credit/Expense',
            'credit_amount'=>'Credited',
            'expense_amount'=>'Expense',
            'cash_balance'=>'Balance',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    
   public function getProjectName()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
     public function getEmployeeName()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
    public function getSupplierName()
    {
        return $this->hasOne(Customer::className(), ['id' => 'supplier_id']);
    }
    public function getClientName()
    {
        return $this->hasOne(Customer::className(), ['id' => 'client_id']);
    }
    public function getSubcontractorName()
    {
        return $this->hasOne(Customer::className(), ['id' => 'subcontractor_id']);
    }
   public function getCategoryMaster()
    {
        return $this->hasOne(CategoryMaster::className(), ['id' => 'vehicle']);
    }
    public function getInvoiceMaster()
    {
        return $this->hasOne(InvoiceMaster::className(), ['project_id' => 'project_id']);
    }
   
}
