<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property integer $area_code
 * @property string $area_name
 * @property integer $category_master_id
 * @property string $remark
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class Services extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%service}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
           
             [['project_id','service_date','description','work_type','working_hours','ot_hours','rate_working','rate_otwork','totalcost_working','totalcost_otwork'], 'safe'],
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    
   
}
