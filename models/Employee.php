<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property int $confirmed_at
 * @property string $unconfirmed_email
 * @property int $blocked_at
 * @property string $registration_ip
 * @property string $primary_contact_no
 * @property string $user_department_id
 * @property string $user_group_id
 * @property int $password_notification
 * @property string $password_update
 * @property int $reset_password
 * @property int $flags
 * @property int $created_at
 * @property int $updated_at
 * @property int $last_login_at
 * @property string $role
 * @property int $gender
 * @property string $date_of_birth
 * @property string $first_name
 * @property double $basic_salary
 * @property string $last_name
 * @property string $trade
 * @property double $allowance
 * @property string $hire_date
 * @property string $visa_expirydate
 * @property string $notes
 * @property string $document
 * @property int $phone_number
 * @property int $mobile_number
 * @property string $employee_id
 *
 * @property Profile $profile
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'email', 'auth_key', 'created_at', 'updated_at','role'], 'required'],
            [['confirmed_at', 'blocked_at', 'password_notification', 'reset_password', 'flags', 'created_at', 'updated_at', 'last_login_at', 'gender', 'phone_number', 'mobile_number'], 'integer'],
            [['password_update', 'date_of_birth', 'hire_date', 'visa_expirydate'], 'safe'],
            [['basic_salary', 'allowance'], 'number'],
            [['username', 'email', 'unconfirmed_email', 'role', 'first_name', 'last_name', 'trade', 'notes', 'document', 'employee_id'], 'string', 'max' => 255],
            [['password_hash'], 'string', 'max' => 60],
            [['auth_key'], 'string', 'max' => 32],
            [['registration_ip'], 'string', 'max' => 45],
            [['primary_contact_no', 'user_department_id', 'user_group_id'], 'string', 'max' => 256],
            [['username'], 'unique'],
            [['password_hash'], 'required','message'=>'Password cannot be blank'],
            [['confirm_password'], 'safe','message'=>'Confirm password cannot be blank'],
            [['email'], 'unique'],
            [['first_name'], 'required'],
            [['last_name'], 'required'],
            // [['employee_id'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password_hash' => 'Password Hash',
            'auth_key' => 'Auth Key',
            'confirmed_at' => 'Confirmed At',
            'unconfirmed_email' => 'Unconfirmed Email',
            'blocked_at' => 'Blocked At',
            'registration_ip' => 'Registration Ip',
            'primary_contact_no' => 'Primary Contact No',
            'user_department_id' => 'User Department ID',
            'user_group_id' => 'User Group ID',
            'password_notification' => 'Password Notification',
            'password_update' => 'Password Update',
            'reset_password' => 'Reset Password',
            'flags' => 'Flags',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'last_login_at' => 'Last Login At',
            'role' => 'Role',
            'gender' => 'Gender',
            'date_of_birth' => 'Date Of Birth',
            'first_name' => 'First Name',
            'basic_salary' => 'Basic Salary',
            'last_name' => 'Last Name',
            'trade' => 'Trade',
            'allowance' => 'Allowance',
            'hire_date' => 'Hire Date',
            'visa_expirydate' => 'Visa Expirydate',
            'notes' => 'Notes',
            'document' => 'Document',
            'phone_number' => 'Phone Number',
            'mobile_number' => 'Mobile Number',
            'employee_id' => 'Employee ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }
}
