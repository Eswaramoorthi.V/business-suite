<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_estimate".
 *
 * @property int $id
 * @property int $project_id
 * @property int $project_master_id
 * @property string $resource_type
 * @property double $resource_unit_cost
 * @property double $resource_total_cost
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class ProjectEstimate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'project_estimate';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'project_master_id', 'resource_unit_cost', 'resource_total_cost'], 'safe'],
            [['project_id', 'project_master_id', 'status', 'created_by', 'updated_by','resource_type'], 'safe'],
            [['resource_unit_cost', 'resource_total_cost'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            // [['resource_type'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'project_master_id' => 'Project Master ID',
            'resource_type' => 'Resource Type',
            'resource_unit_cost' => 'Resource Unit Cost',
            'resource_total_cost' => 'Resource Total Cost',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

}
