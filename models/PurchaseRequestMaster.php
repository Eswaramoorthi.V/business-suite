<?php

namespace app\models;
use app\models\PurchaseRequestDetails;
use app\models\BaseModel;
use Yii;

/**
 * This is the model class for table "purchase_request_master".
 *
 * @property int $id
 * @property int $project_id
 * @property string $reference_name
 * @property string $requested_date
 * @property string $received_date
 * @property int $request_status
 * @property string $requested_by
 * @property int $purchase_order_id
 * @property string $created_by
 * @property string $created_at
 * @property string $updated_by
 * @property string $updated_at
 */
class PurchaseRequestMaster extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchase_request_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['project_id', 'reference_name', 'requested_date',  'request_status', 'requested_by'], 'required'],
            [['project_id', 'request_status', 'purchase_order_id'], 'integer'],
            [['requested_date', 'received_date', 'created_at', 'updated_at'], 'safe'],
            [['reference_name', 'requested_by',  'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project ID',
            'reference_name' => 'Reference Name',
            'requested_date' => 'Requested Date',
            'received_date' => 'Received Date',
            'request_status' => 'Request Status',
            'requested_by' => 'Requested By',
            'purchase_order_id' => 'Purchase Order ID',

            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
        ];
    }

    public function getRequest()
    {
        return $this->hasMany(PurchaseRequestDetails::className(), ['purchase_request_master_id' => 'id']);
    }
    public function getProject(){
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
