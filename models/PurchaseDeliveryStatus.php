<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "purchase_delivery_status".
 *
 * @property int $id
 * @property int $purchase_delivery_id
 * @property int $received_quantity
 * @property string $received_date
 * @property string $created_at
 * @property int $created_by
 */
class PurchaseDeliveryStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchase_delivery_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['purchase_delivery_id', 'received_quantity', 'received_date'], 'required'],
            [['purchase_delivery_id', 'received_quantity', 'created_by'], 'integer'],
            [['received_date', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'purchase_delivery_id' => 'Purchase Delivery ID',
            'received_quantity' => 'Received Quantity',
            'received_date' => 'Received Date',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
