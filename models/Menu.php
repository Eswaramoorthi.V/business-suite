<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "category_master".
 *
 * @property integer $id
 * @property string $code
 * @property string $description
 * @property integer $status
 * @property integer $parent_id
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class Menu extends BaseModel {

    const MENU_STATUS_ENABLED = 1;
    const MENU_STATUS_DISABLED = 0;

    public function init() {
        $this->status = 1;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%menu}}';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
                [['status', 'parent_id', 'position', 'level'], 'integer'],
                [['name', 'url', 'position','controller','action','icon', 'level'], 'required'],
                //[['name','position'], 'unique'],
                [['name'], 'unique'],
                [['role_list','role_list_disp','menu_disp','type'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
            'parent_id' => 'Parent',
            'role_list_disp' => 'Display in Map Roles',
            'menu_disp'=> 'Display In Menu',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'system_datetime' => 'System Datetime',
            
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus() {
        $categories = static::find()
                ->where([
            'status' => Menu::MENU_STATUS_ENABLED
        ]);

        if ($this->id) {
            $categories->andWhere(['!=', 'id', $this->id]);
        }

        return $categories->all();
    }

    public function getParentCategoryRelation() {
        return $this->hasOne(Menu::className(), ['id' => 'parent_id']);
    }

    public function getChildCategories() {
        return $this->hasMany(Menu::className(), ['parent_id' => 'id']);
    }

}
