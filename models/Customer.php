<?php


namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property int $type
 * @property string $customer_type
 * @property string $company_name
 * @property string $email
 * @property string $phone_number
 * @property string $mobile_number
 * @property string $postal_code
 * @property string $website
 * @property string $address
 * @property string $address_line2
 * @property string $trn_number
 * @property string $city
 * @property string $state
 * @property string $zip_code
 * @property string $country
 * @property string $same_address
 * @property string $delivery_address
 * @property string $delivery_address_line2
 * @property string $delivery_city
 * @property string $delivery_state
 * @property string $delivery_zip_code
 * @property string $delivery_country
 * @property string $salutation
 * @property string $customer_fname
 * @property string $customer_lname
 * @property string $customer_mid_name
 * @property string $description
 * @property int $created_by
 * @property string $created_on
 * @property int $updated_by
 * @property string $updated_on
 * @property string $system_datetime
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'phone_number'], 'safe'],
            [['type', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on', 'system_datetime'], 'safe'],
            [['customer_type', 'company_name', 'email', 'phone_number', 'mobile_number', 'postal_code', 'website', 'address', 'address_line2', 'trn_number', 'city', 'state', 'zip_code', 'country', 'same_address', 'delivery_address', 'delivery_address_line2', 'delivery_city', 'delivery_state', 'delivery_zip_code', 'delivery_country', 'salutation', 'customer_fname', 'customer_lname', 'customer_mid_name', 'description'], 'string', 'max' => 255],
            [['email', 'phone_number', 'customer_fname', 'customer_lname'],'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'customer_type' => 'Customer Type',
            'company_name' => 'Company Name',
            'email' => 'Email',
            'phone_number' => 'Phone Number',
            'mobile_number' => 'Mobile Number',
            'postal_code' => 'Postal Code',
            'website' => 'Website',
            'address' => 'Address',
            'address_line2' => 'Address Line2',
            'trn_number' => 'Trn Number',
            'city' => 'City',
            'state' => 'State',
            'zip_code' => 'Zip Code',
            'country' => 'Country',
            'same_address' => 'Same Address',
            'delivery_address' => 'Delivery Address',
            'delivery_address_line2' => 'Delivery Address Line2',
            'delivery_city' => 'Delivery City',
            'delivery_state' => 'Delivery State',
            'delivery_zip_code' => 'Delivery Zip Code',
            'delivery_country' => 'Delivery Country',
            'salutation' => 'Salutation',
            'customer_fname' => 'Customer Fname',
            'customer_lname' => 'Customer Lname',
            'customer_mid_name' => 'Customer Mid Name',
            'description' => 'Description',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'system_datetime' => 'System Datetime',
        ];
    }
}
