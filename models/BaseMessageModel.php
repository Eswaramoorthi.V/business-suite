<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use dektrium\user\models\User;
use yii\helpers\ArrayHelper;
class BaseMessageModel extends \yii\db\ActiveRecord {

    var $primaryKey = 'id';
    var $serial_no;

    public function fields() {
        return array_merge(parent::fields(), ['serial_no']);
    }

    public function behaviors() {
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy() {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy() {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    public function filterData($data) {
        $d = [];
        if (!empty($data)) {
            foreach ($data as $s) {
                if (!isset($d[$s['location_id']])) {
                    $d[$s['location_id']] = $s;
                } else {
                    if ($d[$s['location_id']]['time_stamp'] < $s['time_stamp']) {
                        $d[$s['location_id']] = $s;
                    }
                }
            }
        }
        $datavalues = array_values($d);
        return $datavalues;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function getPoleName() {
        return $this->hasOne(AssetMaster::className(), ['pole_id' => 'location_id']);
    }

    /**
     * Calculate Remaining Days
     *
     * @param string $startDate
     * @param string $endDate
     *
     * @return string $remaingHours
     */
    public static function getRemainingHours($startDate, $endDate) {
        $startdateValue = date_create($startDate);
        $endDateValue = date_create($endDate);
        $remainInterval = date_diff($startdateValue, $endDateValue);
        $remainingHours = (intval($remainInterval->format('%r%H')) + 1 > 0) ? intval($remainInterval->format('%r%a')) + 1 : 0;
        return $remainingHours;
    }
    public function applyIndex($data,$field){
        $result = ArrayHelper::index($data, $field);
        return $result;
    }
    public function getYearDirectory(){
        $dateDirectory=[];
        for($i=1;$i<=12;$i++){
           $temp=[];
           if(strlen($i)==1){
               $i="0".$i;
           }
           $temp['start']=date("Y-$i-01");
           $temp['end']=date('Y-m-t', strtotime($temp['start']));
           $dateDirectory[]=$temp;
        }
        
        return $dateDirectory;
    }
    public function getMonthlyList(){
        $dateDirectory=[];
        for($i=1;$i<=12;$i++){
           if(strlen($i)==1){
               $i="0".$i;
           }
           
           $dateDirectory[]=$i;
        }
        
        return $dateDirectory;
    }

}
