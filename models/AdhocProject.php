<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property integer $area_code
 * @property string $area_name
 * @property integer $category_master_id
 * @property string $remark
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class AdhocProject extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%adhoc_project}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
             [['customer_id','project_name','project_type','project_code'], 'safe'],
             [['customer_contact','description','status','start_date','amount','probability','end_date'], 'safe'],
      
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
   
    public function getContacts()
    {
        return $this->hasOne(Contacts::className(), ['customer_id' => 'customer_id']);
    }
    
    
    public function getServices()
    {
        return $this->hasOne(Services::className(), ['project_id' => 'id']);
    }
    
    public function getSubcontractor()
    {
        return $this->hasOne(SubContractor::className(), ['project_id' => 'id']);
    }
}
