<?php

namespace app\models;

use app\models\BaseModel;

/**
 * This is the model class for table "area".
 *
 * @property integer $id
 * @property integer $area_code
 * @property string $area_name
 * @property integer $category_master_id
 * @property string $remark
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_on
 * @property string $updated_by
 * @property string $system_datetime
 */
class PaInvoicePayment extends BaseModel
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return '{{%pa_invoice_payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [['receipt_number','invoice_id','payment_date','payment_method','paid_amount','bank_reference','bank_account','days_late','cheque_date','cheque_number','description'], 'safe'],
          ['paid_amount', 'number' ,'message' => 'Amount should be Numerical Value'],
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    
    public function getPaInvoice()
    {
        return $this->hasOne(PaInvoiceMaster::className(), ['id' => 'invoice_id']);
    }
    
    
    
   
}
