<?php

namespace app\models;

use app\models\BaseModel;


class SubcontractorPayment extends BaseModel
{
    /**
     * @inheritdoc
     */
    
    public static function tableName()
    {
        return '{{%subcontractor_payment}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           
            [['subcontractor_id','payment_date','description','payment_method','paid_amount','bank_account','cheque_date','cheque_number'], 'safe'],
          ['paid_amount', 'number' ,'message' => 'Amount should be Numerical Value'],
          
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            
        ];
    }
    
    public function getSubcontractor()
    {
        return $this->hasOne(SubContractor::className(), ['id' => 'subcontractor_id']);
    }
    
    
    
   
}
