<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estimate_details".
 *
 * @property int $id
 * @property int $estmate_id
 * @property string $resource_type
 * @property int $resource_id
 * @property double $last_purchased_cost
 * @property double $average_weighted_cost
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class EstimateDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estimate_details';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['estmate_id', 'resource_type', ], 'required'],
            [['estmate_id', 'resource_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['last_purchased_cost', 'average_weighted_cost'], 'number'],
            [['resource_id', 'created_at', 'updated_at'], 'safe'],
            [['resource_type','unit_type'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'estmate_id' => 'Estmate ID',
            'resource_type' => 'Resource Type',
            'resource_id' => 'Resource ID',
            'last_purchased_cost' => 'Last Purchased Cost',
            'average_weighted_cost' => 'Average Weighted Cost',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'unit_type'=>'Unit Type'
        ];
    }
}
