<?php

namespace app\models;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;

/**
 * Role Access.
 */
class Permission extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%auth_item}}';
    }

    public function getChild() {
        return $this->hasMany(RoleChild::className(), ['child' => 'name']);
    }

    /**
     * Creates data provider instance with search query applied
     * @param array $role
     * @param array $moduleType
     * @return ActiveDataProvider
     */
    public function getRoles($role, $moduleType) {
        $checkRole = self::checkRole($role);
        $authItem = [];
        if (!empty($checkRole)) {
            if (empty($moduleType)) {
                $query = Permission::find()->where('auth_item.type=2')->joinWith(['child'])->asArray(true)->all();
            } else {
                $query = Permission::find()->where("auth_item.type=2 and auth_item.description='$moduleType'")->joinWith(['child'])->asArray(true)->all();
            }
            
            foreach ($query as $key => $data) {
                $childTable = $data['child'];
                $childExist = 0;
                if (!empty($childTable)) {
                    foreach ($childTable as $childData) {
                        if ($childData['parent'] == $role && $childData['child'] == $data['name']) {
                            $childExist = 1;
                        }
                    }
                }

                $childVal = $data['name'];
                if ($childExist == 1) {
                    $actionLink = "<a href='#' data-action=0 data-child='$childVal' data-parent='$role' data-module='$moduleType' class='roleSettings'>Revoke</a>";
                } else {
                    $actionLink = "<a href='#' data-action=1 data-child='$childVal' data-parent='$role' data-module='$moduleType' class='roleSettings'>Assign</a>";
                }
                $authItem[$key]['sno'] = $key + 1;
                $authItem[$key]['role'] = $role;
                $authItem[$key]['module'] = ucfirst($data['description']);
                $authItem[$key]['section'] = $childVal;
                $authItem[$key]['action'] = $actionLink;
            }
        }
        $dataProvider = new ArrayDataProvider(['allModels' => $authItem, 'pagination' => false]);

        if (!$this->validate()) {
            return $dataProvider;
        }
        return $authItem;
    }

    public function checkRole($role) {
        $roleCheck = Permission::find()->where("type=1 and name='$role'")->asArray(true)->one();
        return $roleCheck;
    }

}
