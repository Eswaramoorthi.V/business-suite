<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "material_unit_cost".
 *
 * @property int $id
 * @property int $master_id
 * @property int $unit_id
 * @property double $cost
 * @property int $status
 * @property string $created_at
 * @property int $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class MaterialUnitCost extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'material_unit_cost';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['master_id', 'unit_id', 'cost'], 'required'],
            [['master_id', 'unit_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['cost'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'master_id' => 'Master ID',
            'unit_id' => 'Unit ID',
            'cost' => 'Cost',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
