<?php

namespace app\models;
use app\models\PurchaseOrderDetails;
use app\models\Customer;
use app\models\PurchaseRequestMaterial;
use Yii;

/**
 * This is the model class for table "project_resource_detail".
 *
 * @property int $id
 * @property string $project_date
 
 * @property int $cost
 * @property int $purchase_status
 * @property string $created_by
 * @property string $created_on
 * @property string $updated_by
 * @property string $updated_on
 * @property int $material_id
 * @property string $supplier_name
 * @property string $po_number
 * @property string $tax_number
 * @property string $date_created
 * @property string $due_date
 * @property int $quantity
 * @property string $unit
 * @property string $options
 * @property string $section_name
 * @property int $item_total_amount
 * @property int $multiple_item
 * @property string $project_name
 * @property string $description
 * @property double $sub_total
 * @property double $vat
 * @property double $discount_amount
 * @property double $grand_total
 * @property string $notes
 * @property double $total_amount
 * @property string $originator
 * @property string $reference
 * @property string $company_name
 * @property double $amount
 * @property string $currency
 * @property string $package
 * @property string $payment_terms
 * @property string $recommendation
 * @property double $discount
 * @property string $supplier_ref
 * @property double $provision_amount
 */
class Purchase extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'purchase_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'supplier_name',  'date_created', 'due_date',  'project_name',  'sub_total', 'notes', 'total_amount', 'originator', 'amount', 'currency', 'payment_terms','lpo_status','order_type','po_number' ], 'required'],
           
            [['project_date', 'created_on', 'updated_on', 'date_created', 'due_date','recommendation','package','vat','discount','reference','supplier_ref'], 'safe'],
            [[ 'sub_total',  'total_amount', 'amount', 'discount', 'provision_amount'], 'number'],
            [['created_by', 'updated_by'], 'string', 'max' => 256],
            [['supplier_name',  'project_name', 'notes', 'originator', 'reference','currency', 'package', 'payment_terms', 'recommendation', 'supplier_ref'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_date' => 'Project Date',
            
            'cost' => 'Cost',
            'purchase_status' => 'Purchase Status',
            'created_by' => 'Created By',
            'created_on' => 'Created On',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
            'material_id' => 'Material ID',
            'supplier_name' => 'Supplier Name',
            'po_number' => 'Po Number',
            'tax_number' => 'Tax Number',
            'date_created' => 'Date Created',
            'due_date' => 'Due Date',
            'quantity' => 'Quantity',
           
            'unit' => 'Unit',
            'options' => 'Options',
            'section_name' => 'Section Name',
            'item_total_amount' => 'Item Total Amount',
            'multiple_item' => 'Multiple Item',
            'project_name' => 'Project Name',
            'description' => 'Description',
            'sub_total' => 'Sub Total',
            'vat' => 'Vat',
            'discount_amount' => 'Discount Amount',
            'grand_total' => 'Grand Total',
            'notes' => 'Notes',
            'total_amount' => 'Total Amount',
            'originator' => 'Originator',
            'reference' => 'Reference',
            'company_name' => 'Company Name',
            'amount' => 'Amount',
            'currency' => 'Currency',
            'package' => 'Package',
            'payment_terms' => 'Payment Terms',
            'recommendation' => 'Recommendation',
            'discount' => 'Discount',
            'supplier_ref' => 'Supplier Ref',
            'provision_amount' => 'Provision Amount',
        ];
    }
    public function getPurchase()
    {
        return $this->hasMany(PurchaseOrderDetails::className(), ['purchase_id' => 'id']);
    }
    public function getRequest()
    {
        return $this->hasMany(PurchaseRequestMaterial::className(), ['purchase_request_id' => 'id']);
    }
    public function getPaymentTerms(){
        return $this->hasOne(SettingsTree::className(), ['id' => 'payment_terms']);
    }
    public function getSuplierList(){
        return $this->hasOne(Customer::className(), ['id' => 'supplier_name']);
    }
}
