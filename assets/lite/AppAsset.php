<?php

namespace app\assets\lite;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/lib/';
    public $baseUrl = '@web/lib/';
    public $js = [
        
        // 'js/jexcel/jquery4.min.js',
        'js/plugins.js',
        'js/actions.js',
        'js/jquery.plugin.min.js',
        'js/jquery.calculator.min.js',
        'js/plugins/bootstrap/bootstrap-colorpicker.js',
        'js/plugins/bootstrap/bootstrap-select.js',
        'js/plugins/bootstrap/bootstrap-datepicker.js',
        'js/plugins/bootstrap/bootstrap-timepicker.min.js',
        'js/plugins/icheck/icheck.min.js',

        'js/plugins/owl/owl.carousel.min.js',
        'js/plugins/moment_2_18_1.min.js',
        'js/plugins/daterangepicker/daterangepicker.js',

        'js/plugins/owl/owl.carousel.min.js',
        'js/plugins/moment_2_18_1.min.js',
        'js/plugins/daterangepicker/daterangepicker.js',

        'js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        'js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        'js/plugins/daterangepicker/daterangepicker.js',

        //'js/demo_dashboard1.js',
        'js/plugins/sparkline/jquery.sparkline.min.js',
        'js/plugins/knob/jquery.knob.min.js',
        'js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js',
        'js/plugins/datatables/jquery.dataTables.min.js',
        'js/plugins/fileinput/fileinput.min.js',
        'js/plugins/filetree/jqueryFileTree.js',
        'js/plugins/bootstrap/bootstrap-datetimepicker.min.js',
        'js/parallel/d3.min.js',
        'js/plugins/icheck/icheck.min.js',
        'js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js',
        'js/plugins/smartwizard/jquery.smartWizard-2.0.min.js',
        'js/plugins/jquery-validation/jquery.validate.js',
        'js/plugins/jquery/jquery-ui.min.js',
        'js/plugins/morris/raphael-min.js',
        'js/plugins/morris/morris.min.js',
//        'js/amcharts.js',
//        'js/gauge.js',
        'js/highcharts.js',
        'js/highcharts-more.js',
        'js/solid-gauge.js',
        'js/jexcel/jexcelv3.js',
        'js/jexcel/jsuites.js',
        
        //'css/jquery/jquery-ui.min.css',
        




    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'nullref\datatable\DataTableAsset'
    ];
}
