<?php

use dektrium\user\migrations\Migration;

class m171118_022510_menu extends Migration
{
    public function up()
    {
        $this->createTable('{{%menu}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(64)->notNull(),
            'url' => $this->string(80)->notNull(),
            'controller' => $this->string(1500)->notNull(),
            'action' => $this->string(1000)->notNull(),
            'icon' => $this->string(80)->notNull(),
            'position' => $this->integer()->notNull()->defaultValue(10),
            'status' => $this->integer()->notNull()->defaultValue(1),
            'level'  => $this->integer(2)->defaultValue(1),
            'parent_id'  => $this->integer()->defaultValue(0),
            'role_list' => $this->string(3000)->defaultValue('index,create,update,delete'),
            'role_list_disp' => $this->integer(1)->defaultValue(1),
            'menu_disp'=> $this->integer(1)->defaultValue(1),
            'type' => $this->string(20)->defaultValue('others'), // others- Operation, report - Report
            'created_by' => $this->string(50)->null(),
            'created_on' => $this->timestamp()->null(),
            'updated_by' => $this->string(50)->null(),
            'updated_on' => $this->timestamp()->null(),
            'system_datetime' => $this->timestamp()->null(),
        ], $this->tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%menu}}');
    }

   
}
