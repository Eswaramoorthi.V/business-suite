<?php

use yii\db\Migration;
use yii\db\Schema;

class m171203_121358_authitem_childtable_new_field extends Migration
{
  public function safeUp()
    {
        $this->addColumn('{{%auth_item_child}}', 'associated_ids', $this->string(1500)->null());
        $this->addColumn('{{%auth_item_child}}', 'menu_id', Schema::TYPE_INTEGER);
        $this->addColumn('{{%auth_item_child}}', 'menu_others', Schema::TYPE_INTEGER);

    }

    public function safeDown() {
        
        try {
            $this->dropColumn('{{%auth_item_child}}', 'associated_ids');
            $this->dropColumn('{{%auth_item_child}}', 'menu_id');
        $this->dropColumn('{{%auth_item_child}}', 'menu_others');
        } catch (Exception $ex) {
            $content = $ex->getMessage();
            if (preg_match_all("/The object '(.*?)'(.*?)is/is", $content, $match, PREG_SET_ORDER)) {
                $frValue = $match[0][1];
                $dropsql = "ALTER TABLE [auth_item_child] DROP CONSTRAINT $frValue";
                $this->execute($dropsql);
                $this->dropColumn('{{%auth_item_child}}', 'associated_ids');
            }
        }
    }
}

