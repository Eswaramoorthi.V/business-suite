<?php

use dektrium\user\migrations\Migration;

class m171203_165232_user_pass extends Migration
{
     public function up()
    {
        $this->createTable('{{%user_pass}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'password_hash' => $this->string(60)->notNull(),
            'password_notification' => $this->integer(1)->notNull()->defaultValue(0),
            'password_update' => $this->timestamp(),
            'created_by' => $this->string(50)->null(),
            'created_on' => $this->timestamp()->null(),
            'updated_by' => $this->string(50)->null(),
            'updated_on' => $this->timestamp()->null(),
            'system_datetime' => $this->timestamp()->null(),
        ], $this->tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user_pass}}');
    }
}
