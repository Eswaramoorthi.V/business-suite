<?php

use yii\db\Migration;

/**
 * Class m200930_125334_alter_table_purchase_order
 */
class m200930_125334_alter_table_purchase_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_order` CHANGE `purchase_status` `lpo_status` INT(11) NOT NULL;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200930_125334_alter_table_purchase_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200930_125334_alter_table_purchase_order cannot be reverted.\n";

        return false;
    }
    */
}
