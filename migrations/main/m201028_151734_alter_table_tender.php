<?php

use yii\db\Migration;

/**
 * Class m201028_151734_alter_table_tender
 */
class m201028_151734_alter_table_tender extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
 $sql="ALTER TABLE `tender` CHANGE `created_on` `created_at` DATETIME NULL DEFAULT NULL, CHANGE `updated_on` `updated_at` DATETIME NULL DEFAULT NULL;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201028_151734_alter_table_tender cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201028_151734_alter_table_tender cannot be reverted.\n";

        return false;
    }
    */
}
