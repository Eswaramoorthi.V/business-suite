<?php

use yii\db\Migration;

/**
 * Class m200925_155934_alter_table_purchase_request_master
 */
class m200925_155934_alter_table_purchase_request_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_request_master` CHANGE `created_by` `created_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `created_at` `created_at` DATETIME NULL, CHANGE `updated_by` `updated_by` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `updated_at` `updated_at` DATETIME NULL;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200925_155934_alter_table_purchase_request_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200925_155934_alter_table_purchase_request_master cannot be reverted.\n";

        return false;
    }
    */
}
