<?php

use yii\db\Migration;

/**
 * Class m201007_111441_craete_table_purchase_delivery
 */
class m201007_111441_craete_table_purchase_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`purchase_delivery` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `purchase_id` INT(11) NOT NULL , `material_id` INT(11) NOT NULL , `description` VARCHAR(255) NOT NULL , `material_status` INT(11) NOT NULL , `delivery_date` DATE NOT NULL , `total_quantity` INT(11) NOT NULL , `purchased_quantity` INT(11) NOT NULL , `remaining_quantity` INT(11) NOT NULL , `remarks` VARCHAR(255) NOT NULL , `created_at` DATETIME NOT NULL , `created_by` VARCHAR(255) NOT NULL , `updated_at` DATETIME NOT NULL , `updated_by` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201007_111441_craete_table_purchase_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201007_111441_craete_table_purchase_delivery cannot be reverted.\n";

        return false;
    }
    */
}
