<?php

use yii\db\Migration;

/**
 * Class m200915_113511_create_table_project_resource
 */
class m200915_113511_create_table_project_resource extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`project_resource` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `project_id` INT(11) NOT NULL , `resource_type` VARCHAR(256) NOT NULL , `resource_id` INT(11) NOT NULL , `resource_quantity` INT(11) NOT NULL , `created_by` INT(11) NULL , `created_at` DATETIME NULL , `updated_by` INT(11) NULL , `updated_at` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200915_113511_create_table_project_resource cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200915_113511_create_table_project_resource cannot be reverted.\n";

        return false;
    }
    */
}
