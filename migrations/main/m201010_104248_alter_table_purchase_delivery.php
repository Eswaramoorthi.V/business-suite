<?php

use yii\db\Migration;

/**
 * Class m201010_104248_alter_table_purchase_delivery
 */
class m201010_104248_alter_table_purchase_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $sql1 ="ALTER TABLE `purchase_delivery` CHANGE `material_id` `material_id` INT(11) NULL;";
        $this->execute($sql1);
       
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201010_104248_alter_table_purchase_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201010_104248_alter_table_purchase_delivery cannot be reverted.\n";

        return false;
    }
    */
}
