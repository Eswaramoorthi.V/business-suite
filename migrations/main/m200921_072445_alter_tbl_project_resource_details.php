<?php

use yii\db\Migration;

/**
 * Class m200921_072445_alter_tbl_project_resource_details
 */
class m200921_072445_alter_tbl_project_resource_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `project_resource_detail` ADD `originator` VARCHAR(255) NOT NULL AFTER `total_amount`, ADD `reference` VARCHAR(255) NOT NULL AFTER `originator`, ADD `company_name` VARCHAR(255) NOT NULL AFTER `reference`, ADD `amount` FLOAT NOT NULL AFTER `company_name`, ADD `currency` VARCHAR(255) NOT NULL AFTER `amount`, ADD `package` VARCHAR(255) NOT NULL AFTER `currency`, ADD `payment_terms` VARCHAR(255) NOT NULL AFTER `package`, ADD `recommendation` VARCHAR(255) NOT NULL AFTER `payment_terms`, ADD `discount` FLOAT NOT NULL AFTER `recommendation`, ADD `supplier_ref` VARCHAR(255) NOT NULL AFTER `discount`;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `project_resource_detail` ADD `provision_amount` FLOAT NOT NULL AFTER `supplier_ref`;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200921_072445_alter_tbl_project_resource_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200921_072445_alter_tbl_project_resource_details cannot be reverted.\n";

        return false;
    }
    */
}
