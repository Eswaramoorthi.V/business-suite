<?php

use yii\db\Migration;

/**
 * Class m200719_132029_project_estimate
 */
class m200719_132029_project_estimate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `project_estimate` (
            `id` int(11) NOT NULL,
            `project_id` int(11) NOT NULL,
            `project_master_id` int(11) NOT NULL,
            `resource_type` varchar(256) NOT NULL,
            `resource_unit_cost` float NOT NULL,
            `resource_total_cost` float NOT NULL,
            `status` int(11) NOT NULL DEFAULT '0',
            `created_at` datetime DEFAULT NULL,
            `created_by` int(11) DEFAULT NULL,
            `updated_at` datetime DEFAULT NULL,
            `updated_by` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->execute($sql);
        $sql1="ALTER TABLE `project_estimate` ADD PRIMARY KEY( `id`);";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `project_estimate` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%project_estimate}}');
    }
}
