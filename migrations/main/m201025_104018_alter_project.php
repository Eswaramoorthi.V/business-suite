<?php

use yii\db\Migration;

/**
 * Class m201025_104018_alter_project
 */
class m201025_104018_alter_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $sql="ALTER TABLE `project` ADD `created_at` DATE NOT NULL AFTER `location`;";
        // $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201025_104018_alter_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201025_104018_alter_project cannot be reverted.\n";

        return false;
    }
    */
}
