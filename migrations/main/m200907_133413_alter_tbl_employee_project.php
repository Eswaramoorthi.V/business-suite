<?php

use yii\db\Migration;

/**
 * Class m200907_133413_alter_tbl_employee_project
 */
class m200907_133413_alter_tbl_employee_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `employee_project` CHANGE `created_at` `created_at` DATETIME NULL;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `employee_project` CHANGE `created_by` `created_by` INT(11) NULL;";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `employee_project` CHANGE `updated_at` `updated_at` DATETIME NULL;";
        $this->execute($sql3);
        $sql4 ="ALTER TABLE `employee_project` CHANGE `updated_by` `updated_by` INT(11) NULL;";
        $this->execute($sql4);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200907_133413_alter_tbl_employee_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_133413_alter_tbl_employee_project cannot be reverted.\n";

        return false;
    }
    */
}
