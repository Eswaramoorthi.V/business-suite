<?php

use yii\db\Migration;

/**
 * Class m201105_105435_create_table_variation
 */
class m201105_105435_create_table_variation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `variation` (
            `id` int(11) NOT NULL,
            `loa_id` int(11) DEFAULT NULL,
            `project_id` int(11) DEFAULT NULL,
            `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `vo_amount` float DEFAULT NULL,
            `vo_document` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `variation` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `variation` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105435_create_table_variation cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105435_create_table_variation cannot be reverted.\n";

        return false;
    }
    */
}
