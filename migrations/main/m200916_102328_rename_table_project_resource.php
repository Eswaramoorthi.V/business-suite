<?php

use yii\db\Migration;

/**
 * Class m200916_102328_rename_table_project_resource
 */
class m200916_102328_rename_table_project_resource extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="RENAME TABLE `pricing_calculator`.`project_resource` TO `pricing_calculator`.`project_resource_master`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200916_102328_rename_table_project_resource cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200916_102328_rename_table_project_resource cannot be reverted.\n";

        return false;
    }
    */
}
