<?php

use yii\db\Migration;

/**
 * Class m201105_105552_create_table_invoice_payments
 */
class m201105_105552_create_table_invoice_payments extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `invoice_payment` (
            `id` int(11) NOT NULL,
            `receipt_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `invoice_id` int(11) NOT NULL,
            `payment_date` timestamp NULL DEFAULT NULL,
            `payment_method` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `paid_amount` float DEFAULT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `bank_reference` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `bank_account` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `cheque_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `cheque_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `days_late` int(11) DEFAULT NULL,
            `remark` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `invoice_payment` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `invoice_payment` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105552_create_table_invoice_payments cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105552_create_table_invoice_payments cannot be reverted.\n";

        return false;
    }
    */
}
