<?php

use yii\db\Migration;

/**
 * Class m201022_145325_create_tble_general_settings
 */
class m201022_145325_create_tble_general_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`general_settings` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `email_id` VARCHAR(255) NULL , `fax_no` VARCHAR(255) NULL , `tel_no` INT(11) NULL , `po_box_no` VARCHAR(255) NULL , `logo` VARCHAR(255) NULL , `created_at` DATETIME NULL , `created_by` VARCHAR(255) NULL , `updated_by` VARCHAR(255) NULL , `updated_at` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `general_settings` ADD `active` VARCHAR(255) NULL AFTER `logo`;";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `general_settings` ADD `company_name` VARCHAR(255) NULL AFTER `id`;";
        $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201022_145325_create_tble_general_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201022_145325_create_tble_general_settings cannot be reverted.\n";

        return false;
    }
    */
}
