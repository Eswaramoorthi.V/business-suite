<?php

use yii\db\Migration;

/**
 * Class m201020_143535_drop_table_purchase_delivery_received
 */
class m201020_143535_drop_table_purchase_delivery_received extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $sql1 ="DROP TABLE purchase_delivery_received";
        $this->execute($sql1);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201020_143535_drop_table_purchase_delivery_received cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201020_143535_drop_table_purchase_delivery_received cannot be reverted.\n";

        return false;
    }
    */
}
