<?php

use yii\db\Migration;

/**
 * Class m200904_032952_alter_tbl_user_remove
 */
class m200904_032952_alter_tbl_user_remove extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `user` DROP `employee_code`;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `user` DROP `division`, DROP `employee_type`, DROP `full_name`, DROP `father_name`, DROP `employees_previous_name`;";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `user` DROP `present_address`, DROP `emergency_address`, DROP `permanent_address`, DROP `present_tel_nos`, DROP `permanent_tel_nos`, DROP `emergency_tel_nos`, DROP `residence_status`, DROP `marital_status`, DROP `date_of_appointment`, DROP `place_of_birth`, DROP `blood_group`, DROP `sponsor_code`, DROP `sponser_name`, DROP `passport_details_number`, DROP `name_on_passport`, DROP `issue_date`, DROP `issue_place`, DROP `expiry_date`;";
        $this->execute($sql3);
       
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200904_032952_alter_tbl_user_remove cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200904_032952_alter_tbl_user_remove cannot be reverted.\n";

        return false;
    }
    */
}
