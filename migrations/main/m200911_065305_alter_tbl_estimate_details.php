<?php

use yii\db\Migration;

/**
 * Class m200911_065305_alter_tbl_estimate_details
 */
class m200911_065305_alter_tbl_estimate_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `estimate_details` ADD `quantity` INT(11) NOT NULL AFTER `last_purchased_cost`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200911_065305_alter_tbl_estimate_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200911_065305_alter_tbl_estimate_details cannot be reverted.\n";

        return false;
    }
    */
}
