<?php

use yii\db\Migration;

/**
 * Class m201105_110028_create_table_service
 */
class m201105_110028_create_table_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `service` (
            `id` int(11) NOT NULL,
            `project_id` int(11) DEFAULT NULL,
            `service_date` timestamp NULL DEFAULT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `work_type` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `working_hours` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `ot_hours` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `rate_working` float DEFAULT NULL,
            `rate_otwork` float DEFAULT NULL,
            `totalcost_working` float DEFAULT NULL,
            `totalcost_otwork` float DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `service` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `service` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_110028_create_table_service cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_110028_create_table_service cannot be reverted.\n";

        return false;
    }
    */
}
