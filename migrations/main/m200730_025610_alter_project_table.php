<?php

use yii\db\Migration;

/**
 * Class m200730_025610_alter_project_table
 */
class m200730_025610_alter_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `project` ADD `client_name` VARCHAR(256) NOT NULL AFTER `description`, ADD `country` VARCHAR(256) NOT NULL AFTER `client_name`, ADD `tender_due_date` DATE NOT NULL AFTER `country`, ADD `building_type` INT(11) NULL AFTER `tender_due_date`, ADD `consultant` VARCHAR(256) NULL AFTER `building_type`, ADD `total_area` INT(11) NOT NULL AFTER `consultant`, ADD `reference` VARCHAR(256) NOT NULL AFTER `total_area`, ADD `property_account_type` INT(11) NULL AFTER `reference`, ADD `project_status` INT(11) NULL AFTER `property_account_type`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%project}}', 'client_name');
        $this->dropColumn('{{%project}}', 'country');
        $this->dropColumn('{{%project}}', 'tender_due_date');
        $this->dropColumn('{{%project}}', 'building_type');
        $this->dropColumn('{{%project}}', 'consultant');
        $this->dropColumn('{{%project}}', 'total_area');
        $this->dropColumn('{{%project}}', 'reference');
        $this->dropColumn('{{%project}}', 'property_account_type');
        $this->dropColumn('{{%project}}', 'project_status');
    }
}

