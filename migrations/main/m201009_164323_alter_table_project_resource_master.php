<?php

use yii\db\Migration;

/**
 * Class m201009_164323_alter_table_project_resource_master
 */
class m201009_164323_alter_table_project_resource_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `project_resource_master` CHANGE `resource_quantity` `total_quantity` INT(11) NOT NULL;";
        //$this->execute($sql1);
        $sql2 ="ALTER TABLE `project_resource_master` ADD `purchased_quantity` INT(11) NOT NULL AFTER `total_quantity`;";
        //$this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201009_164323_alter_table_project_resource_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201009_164323_alter_table_project_resource_master cannot be reverted.\n";

        return false;
    }
    */
}
