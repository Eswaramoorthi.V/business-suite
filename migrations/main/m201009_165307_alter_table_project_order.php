<?php

use yii\db\Migration;

/**
 * Class m201009_165307_alter_table_project_order
 */
class m201009_165307_alter_table_project_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_order` ADD `vat` INT(11) NOT NULL AFTER `sub_total`;";
        //$this->execute($sql1);
        $sql2 ="ALTER TABLE `purchase_order` CHANGE `provision_amount` `provision_amount` FLOAT NULL;";
        //$this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201009_165307_alter_table_project_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201009_165307_alter_table_project_order cannot be reverted.\n";

        return false;
    }
    */
}
