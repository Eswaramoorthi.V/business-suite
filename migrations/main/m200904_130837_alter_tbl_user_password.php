<?php

use yii\db\Migration;

/**
 * Class m200904_130837_alter_tbl_user_password
 */
class m200904_130837_alter_tbl_user_password extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `user` ADD `confirm_password` VARCHAR(255) NOT NULL AFTER `employee_id`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200904_130837_alter_tbl_user_password cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200904_130837_alter_tbl_user_password cannot be reverted.\n";

        return false;
    }
    */
}
