<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%material_history}}`.
 */
class m200717_124354_create_material_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `material_history` (
        `id` int(11) NOT NULL,
        `master_id` int(11) NOT NULL,
        `unit_id` int(11) NOT NULL,
        `cost` float DEFAULT NULL,
        `on_date` date NOT NULL,
        `status` int(11) NOT NULL DEFAULT '0',
        `created_at` datetime DEFAULT NULL,
        `created_by` int(11) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->execute($sql);
        $sql1="ALTER TABLE `material_history` ADD PRIMARY KEY (`id`);";
        $this->execute($sql1);
        $sql2="ALTER TABLE `material_history` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%material_history}}');
    }
}
