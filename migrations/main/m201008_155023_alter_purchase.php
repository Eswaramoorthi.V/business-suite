<?php

use yii\db\Migration;

/**
 * Class m201008_155023_alter_purchase
 */
class m201008_155023_alter_purchase extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="ALTER TABLE `purchase_request_details` CHANGE `material_status` `material_status` INT(11) NULL DEFAULT NULL;";
        $this->execute($sql);
        $sql="ALTER TABLE `purchase_request_details` CHANGE `material_total_quantity` `material_total_quantity` INT(11) NULL DEFAULT NULL;";
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201008_155023_alter_purchase cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201008_155023_alter_purchase cannot be reverted.\n";

        return false;
    }
    */
}
