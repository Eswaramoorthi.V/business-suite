<?php

use yii\db\Migration;

/**
 * Class m201012_155835_alter_table_purchase_request_details
 */
class m201012_155835_alter_table_purchase_request_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
     $sql1 ="ALTER TABLE `purchase_request_details` CHANGE `material_status` `unit` INT(11) NOT NULL;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `purchase_request_details` ADD `remarks` VARCHAR(255) NOT NULL AFTER `description`;";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `purchase_request_details` ADD `mrv_status` INT(11) NULL AFTER `unit`;";
        $this->execute($sql3);
        $sql4 ="ALTER TABLE `purchase_request_details` CHANGE `unit` `unit` INT(11) NULL;";
        $this->execute($sql4);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201012_155835_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201012_155835_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }
    */
}
