<?php

use yii\db\Migration;

/**
 * Class m201008_165938_alter_purchase_order
 */
class m201008_165938_alter_purchase_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="ALTER TABLE `purchase_request_material` CHANGE `material_status` `material_status` INT(11) NULL DEFAULT NULL;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201008_165938_alter_purchase_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201008_165938_alter_purchase_order cannot be reverted.\n";

        return false;
    }
    */
}
