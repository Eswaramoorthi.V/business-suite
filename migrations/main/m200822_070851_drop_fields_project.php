<?php

use yii\db\Migration;

/**
 * Class m200822_070851_drop_fields_project
 */
class m200822_070851_drop_fields_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%project}}', 'location');
        $this->dropColumn('{{%project}}', 'start_date');
        $this->dropColumn('{{%project}}', 'end_date');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $sql="ALTER TABLE `project` ADD `location` VARCHAR(500) NULL DEFAULT NULL AFTER `project_code`, ADD `start_date` DATE NULL DEFAULT NULL AFTER `location`, ADD `end_date` DATE NULL DEFAULT NULL AFTER `start_date`;";
        $this->execute($sql1);
    }
}
