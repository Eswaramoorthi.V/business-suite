<?php

use yii\db\Migration;

/**
 * Class m200926_064103_alter_table_purchase_request_details
 */
class m200926_064103_alter_table_purchase_request_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_request_details` ADD `material_total_quantity` INT(11) NOT NULL AFTER `updated_at`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200926_064103_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200926_064103_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }
    */
}
