<?php

use yii\db\Migration;

/**
 * Class m200831_120705_alter_tbl_user_field
 */
class m200831_120705_alter_tbl_user_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `user` ADD `employee_code` INT(11) NOT NULL AFTER `role`, ADD `division` VARCHAR(255) NOT NULL AFTER `employee_code`, ADD `employee_type` INT(11) NOT NULL AFTER `division`, ADD `full_name` VARCHAR(255) NULL AFTER `employee_type`, ADD `father_name` VARCHAR(255) NOT NULL AFTER `full_name`, ADD `employees_previous_name` VARCHAR(255) NOT NULL AFTER `father_name`, ADD `present_address` VARCHAR(255) NOT NULL AFTER `employees_previous_name`, ADD `emergency_address` VARCHAR(255) NOT NULL AFTER `present_address`, ADD `permanent_address` VARCHAR(255) NOT NULL AFTER `emergency_address`, ADD `present_tel_nos` INT(11) NOT NULL AFTER `permanent_address`;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `user` ADD `permanent_tel_nos` INT(11) NOT NULL AFTER `present_tel_nos`, ADD `emergency_tel_nos` INT(11) NOT NULL AFTER `permanent_tel_nos`, ADD `residence_status` INT(11) NOT NULL AFTER `emergency_tel_nos`, ADD `gender` INT(11) NULL AFTER `residence_status`, ADD `marital_status` INT(11) NULL AFTER `gender`, ADD `date_of_birth` DATE NULL AFTER `marital_status`, ADD `date_of_appointment` DATE NULL AFTER `date_of_birth`, ADD `place_of_birth` VARCHAR(100) NOT NULL AFTER `date_of_appointment`, ADD `blood_group` VARCHAR(255) NOT NULL AFTER `place_of_birth`, ADD `sponsor_code` VARCHAR(255) NOT NULL AFTER `blood_group`, ADD `sponser_name` VARCHAR(255) NOT NULL AFTER `sponsor_code`, ADD `passport_details_number` VARCHAR(255) NULL AFTER `sponser_name`, ADD `name_on_passport` VARCHAR(255) NOT NULL AFTER `passport_details_number`, ADD `issue_date` DATE NULL AFTER `name_on_passport`, ADD `issue_place` VARCHAR(255) NULL AFTER `issue_date`, ADD `expiry_date` DATE NULL AFTER `issue_place`;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200831_120705_alter_tbl_user_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_120705_alter_tbl_user_field cannot be reverted.\n";

        return false;
    }
    */
}
