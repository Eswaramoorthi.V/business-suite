<?php

use yii\db\Migration;

/**
 * Class m201105_154516_create_table_petty_cash
 */
class m201105_154516_create_table_petty_cash extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `petty_cash` (
            `id` int(11) NOT NULL,
            `employee_id` int(11) DEFAULT NULL,
            `project_id` int(11) DEFAULT NULL,
            `supplier_id` int(11) DEFAULT NULL,
            `name` int(11) DEFAULT NULL,
            `item_number` int(11) DEFAULT NULL,
            `date` timestamp NULL DEFAULT NULL,
            `payment_mode` int(11) NOT NULL DEFAULT '1',
            `type` int(11) DEFAULT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `credit_expense` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `credit_amount` float DEFAULT NULL,
            `document` text COLLATE utf8_unicode_ci,
            `amount` float DEFAULT NULL,
            `vat` int(11) DEFAULT NULL,
            `expense_amount` float DEFAULT NULL,
            `cash_balance` float DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
             $this->execute($sql1);
             $sql2="ALTER TABLE `petty_cash` ADD PRIMARY KEY (`id`);";
             $this->execute($sql2);
             $sql3="ALTER TABLE `petty_cash` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
             $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_154516_create_table_petty_cash cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_154516_create_table_petty_cash cannot be reverted.\n";

        return false;
    }
    */
}
