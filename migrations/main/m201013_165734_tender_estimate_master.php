<?php

use yii\db\Migration;

/**
 * Class m201013_165734_tender_estimate_master
 */
class m201013_165734_tender_estimate_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$sql1 ="CREATE TABLE `pricing_calculator`.`tender_estimate_master` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `tender_id` INT(11) NULL , `activity_id` INT(11) NULL , `sub_activity_id` INT(11) NULL , `description` VARCHAR(255) NULL , `unit_id` VARCHAR(255) NULL , `quantity` INT(11) NULL , `unit_rate` FLOAT NULL , `total_cost` FLOAT NULL , `status` INT(11) NULL , `created_at` DATETIME NULL , `created_by` VARCHAR(255) NULL , `updated_at` DATETIME NULL , `updated_by` VARCHAR(255) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
        
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201013_165734_tender_estimate_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201013_165734_tender_estimate_master cannot be reverted.\n";

        return false;
    }
    */
}
