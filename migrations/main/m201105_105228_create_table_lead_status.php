<?php

use yii\db\Migration;

/**
 * Class m201105_105228_create_table_lead_status
 */
class m201105_105228_create_table_lead_status extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `tbl_lead_status` (
            `id` int(11) NOT NULL,
            `status` varchar(255) NOT NULL,
            `label` varchar(255) NOT NULL,
            `active` tinyint(1) NOT NULL,
            `sort_order` int(11) NOT NULL,
            `created_at` int(11) DEFAULT NULL,
            `updated_at` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            $this->execute($sql1);
            $sql2="ALTER TABLE `tbl_lead_status` ADD PRIMARY KEY (`id`);";
            $this->execute($sql2);
            $sql3="ALTER TABLE `tbl_lead_status` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;";
            $this->execute($sql3);
            $sql4="INSERT INTO `tbl_lead_status` (`id`, `status`, `label`, `active`, `sort_order`, `created_at`, `updated_at`) VALUES
            (1, 'New', 'New', 1, 1, 0, 0),
            (2, 'In Process', 'In Process', 1, 2, 0, 0),
            (3, 'Opportunity', 'Opportunity', 1, 3, 0, 0),
            (4, 'Converted', 'Converted', 1, 4, 0, 0),
            (5, 'Recycled', 'Recycled', 1, 5, 0, 0),
            (6, 'Dead', 'Dead', 1, 6, 0, 0);";
            $this->execute($sql4);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105228_create_table_lead_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105228_create_table_lead_status cannot be reverted.\n";

        return false;
    }
    */
}
