<?php

use yii\db\Migration;

/**
 * Class m201021_020024_create_delivery_order
 */
class m201021_020024_create_delivery_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `pricing_calculator`.`purchase_delivery_order` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `purchase_delivery_id` INT(11) NOT NULL , `suplier_number` INT NOT NULL , `purchase_delivery_number` INT NOT NULL , `created_at` INT NOT NULL , `created_by` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        //$this->execute($sql);
//        $sql1='TABLE purchase_delivery DROP delivery_order_number';
//        $this->execute($sql1);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201021_020024_create_delivery_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201021_020024_delivery_order_number_delivery_order cannot be reverted.\n";

        return false;
    }
    */
}
