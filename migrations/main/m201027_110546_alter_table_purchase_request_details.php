<?php

use yii\db\Migration;

/**
 * Class m201027_110546_alter_table_purchase_request_details
 */
class m201027_110546_alter_table_purchase_request_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="ALTER TABLE `purchase_request_details` ADD `document` VARCHAR(255) NULL AFTER `material_request_quantity`;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201027_110546_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201027_110546_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }
    */
}
