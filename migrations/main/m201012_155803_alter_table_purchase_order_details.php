<?php

use yii\db\Migration;

/**
 * Class m201012_155803_alter_table_purchase_order_details
 */
class m201012_155803_alter_table_purchase_order_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_order_details` CHANGE `unit` `unit` INT(11) NOT NULL;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `purchase_order_details` ADD `mrv_status` INT(11) NOT NULL AFTER `description`;";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `purchase_order_details` ADD `remaining_quantity` INT(11) NOT NULL AFTER `order_quantity`;";
        $this->execute($sql3);
        $sql4 ="ALTER TABLE `purchase_order_details` CHANGE `mrv_status` `mrv_status` INT(11) NULL;";
        $this->execute($sql4);
        $sql5 ="ALTER TABLE `purchase_order_details` CHANGE `unit` `unit` INT(11) NULL;";
        $this->execute($sql5);
        $sql6 ="ALTER TABLE `purchase_order_details` CHANGE `remaining_quantity` `remaining_quantity` INT(11) NULL;";
        $this->execute($sql6);
        $sql7 ="ALTER TABLE `purchase_order_details` CHANGE `quantity` `quantity` FLOAT NULL;";
        $this->execute($sql7);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201012_155803_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201012_155803_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }
    */
}
