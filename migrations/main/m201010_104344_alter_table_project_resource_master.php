<?php

use yii\db\Migration;

/**
 * Class m201010_104344_alter_table_project_resource_master
 */
class m201010_104344_alter_table_project_resource_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $sql1 ="ALTER TABLE `project_resource_master` CHANGE `resource_id` `material_id` INT(11) NOT NULL;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201010_104344_alter_table_project_resource_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201010_104344_alter_table_project_resource_master cannot be reverted.\n";

        return false;
    }
    */
}
