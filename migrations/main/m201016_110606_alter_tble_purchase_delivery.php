<?php

use yii\db\Migration;

/**
 * Class m201016_110606_alter_tble_purchase_delivery
 */
class m201016_110606_alter_tble_purchase_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_delivery` ADD `supplier_name` VARCHAR(255) NULL AFTER `project_id`;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `purchase_delivery` ADD `delivery_order_number` VARCHAR(255) NULL AFTER `supplier_name`;";
        $this->execute($sql2);
         $sql3 ="ALTER TABLE `purchase_delivery` CHANGE `material_status` `delivery_status` INT(11) NOT NULL;";
         $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201016_110606_alter_tble_purchase_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201016_110606_alter_tble_purchase_delivery cannot be reverted.\n";

        return false;
    }
    */
}
