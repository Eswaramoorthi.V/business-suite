<?php

use yii\db\Migration;

/**
 * Class m200922_114346_create_tbl_employee_company
 */
class m200922_114346_create_tbl_employee_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`employee_company` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `user_id` INT(11) NOT NULL , `company_id` INT(11) NOT NULL , `created_by` VARCHAR(255) NULL , `created_at` DATETIME NULL , `updated_by` VARCHAR(255) NULL , `updated_at` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200922_114346_create_tbl_employee_company cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200922_114346_create_tbl_employee_company cannot be reverted.\n";

        return false;
    }
    */
}
