<?php

use yii\db\Migration;

/**
 * Class m200719_151515_settings
 */
class m200719_151515_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `settings` (
            `id` int(11) NOT NULL,
            `code` varchar(256) NOT NULL,
            `name` text NOT NULL,
            `description` varchar(256) DEFAULT NULL,
            `parent_id` int(11) DEFAULT NULL,
            `status` int(11) NOT NULL DEFAULT '0',
            `created_at` datetime DEFAULT NULL,
            `created_by` int(11) DEFAULT NULL,
            `updated_at` datetime DEFAULT NULL,
            `updated_by` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->execute($sql);
        $sql1="ALTER TABLE `settings` ADD PRIMARY KEY( `id`);";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `settings` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql2);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
