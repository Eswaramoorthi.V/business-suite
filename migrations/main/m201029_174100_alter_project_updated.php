<?php

use yii\db\Migration;

/**
 * Class m201029_174100_alter_project_updated
 */
class m201029_174100_alter_project_updated extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201029_174100_alter_project_updated cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201029_174100_alter_project_updated cannot be reverted.\n";

        return false;
    }
    */
}
