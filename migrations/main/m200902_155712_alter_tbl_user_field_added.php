<?php

use yii\db\Migration;

/**
 * Class m200902_155712_alter_tbl_user_field_added
 */
class m200902_155712_alter_tbl_user_field_added extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `user` ADD `first_name` VARCHAR(255) NULL AFTER `expiry_date`, ADD `basic_salary` FLOAT NULL AFTER `first_name`, ADD `last_name` VARCHAR(255) NULL AFTER `basic_salary`, ADD `trade` VARCHAR(255) NULL AFTER `last_name`, ADD `allowance` FLOAT NULL AFTER `trade`, ADD `hire_date` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `allowance`, ADD `visa_expirydate` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `hire_date`, ADD `notes` VARCHAR(255) NULL AFTER `visa_expirydate`, ADD `document` VARCHAR(255) NOT NULL AFTER `notes`, ADD `phone_number` INT(11) NULL AFTER `document`;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `user` ADD `mobile_number` INT(11) NULL AFTER `phone_number`;";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `user` ADD `employee_id` VARCHAR(255) NULL AFTER `mobile_number`;";
        $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200902_155712_alter_tbl_user_field_added cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200902_155712_alter_tbl_user_field_added cannot be reverted.\n";

        return false;
    }
    */
}
