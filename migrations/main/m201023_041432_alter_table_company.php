<?php

use yii\db\Migration;

/**
 * Class m201023_041432_alter_table_company
 */
class m201023_041432_alter_table_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `company` ADD `email_id` VARCHAR(255) NOT NULL AFTER `logo`, ADD `fax_no` VARCHAR(255) NOT NULL AFTER `email_id`, ADD `tel_no` INT(11) NOT NULL AFTER `fax_no`, ADD `po_box_no` VARCHAR(255) NOT NULL AFTER `tel_no`;";
        //$this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201023_041432_alter_table_company cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201023_041432_alter_table_company cannot be reverted.\n";

        return false;
    }
    */
}
