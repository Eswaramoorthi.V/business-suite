<?php

use yii\db\Migration;

/**
 * Class m200907_132525_create_tbl_employee_project
 */
class m200907_132525_create_tbl_employee_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`employee_project` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `user_id` INT(11) NOT NULL , `project_id` INT(11) NOT NULL , `project_name` VARCHAR(256) NOT NULL , `created_at` DATETIME NOT NULL , `created_by` INT(11) NOT NULL , `updated_at` DATETIME NOT NULL , `updated_by` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200907_132525_create_tbl_employee_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_132525_create_tbl_employee_project cannot be reverted.\n";

        return false;
    }
    */
}
