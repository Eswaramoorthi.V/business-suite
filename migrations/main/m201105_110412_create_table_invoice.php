<?php

use yii\db\Migration;

/**
 * Class m201105_110412_create_table_invoice
 */
class m201105_110412_create_table_invoice extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `invoice` (
            `id` int(11) NOT NULL,
            `invoice_id` int(11) DEFAULT NULL,
            `product_id` int(11) NOT NULL,
            `service_id` int(11) NOT NULL,
            `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `quantity` float DEFAULT NULL,
            `unit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `price` float DEFAULT NULL,
            `discount_rate` float DEFAULT NULL,
            `total_amount` float DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `invoice` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `invoice` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_110412_create_table_invoice cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_110412_create_table_invoice cannot be reverted.\n";

        return false;
    }
    */
}
