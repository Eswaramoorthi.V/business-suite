<?php

use yii\db\Migration;

/**
 * Class m201105_105243_create_table_lead_source
 */
class m201105_105243_create_table_lead_source extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `tbl_lead_source` (
            `id` int(11) NOT NULL,
            `source` varchar(255) NOT NULL,
            `label` varchar(255) NOT NULL,
            `active` tinyint(1) NOT NULL,
            `sort_order` int(11) NOT NULL,
            `created_at` int(11) DEFAULT NULL,
            `updated_at` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
              $this->execute($sql1);
              $sql2="ALTER TABLE `tbl_lead_source` ADD PRIMARY KEY (`id`);";
              $this->execute($sql2);
              $sql3="ALTER TABLE `tbl_lead_source` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;";
              $this->execute($sql3);
              $sql4="INSERT INTO `tbl_lead_source` (`id`, `source`, `label`, `active`, `sort_order`, `created_at`, `updated_at`) VALUES
              (1, 'LinkedIn', 'LinkedIn', 1, 1, 0, 0),
              (2, 'Google', 'Google', 1, 2, 0, 0),
              (3, 'Misc', 'Misc', 1, 3, 0, 0);";
              $this->execute($sql4);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105243_create_table_lead_source cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105243_create_table_lead_source cannot be reverted.\n";

        return false;
    }
    */
}
