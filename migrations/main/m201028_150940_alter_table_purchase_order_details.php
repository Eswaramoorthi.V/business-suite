<?php

use yii\db\Migration;

/**
 * Class m201028_150940_alter_table_purchase_order_details
 */
class m201028_150940_alter_table_purchase_order_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="ALTER TABLE `purchase_order_details` CHANGE `material_id` `material_id` INT(11) NULL;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201028_150940_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201028_150940_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }
    */
}
