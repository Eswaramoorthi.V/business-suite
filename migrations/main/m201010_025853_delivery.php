<?php

use yii\db\Migration;

/**
 * Class m201010_025853_delivery
 */
class m201010_025853_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE IF NOT EXISTS `purchase_delivery_status` (
  `id` int(11) NOT NULL,
  `purchase_delivery_id` int(11) NOT NULL,
  `received_quantity` int(11) NOT NULL,
  `received_date` date NOT NULL,
  `created_at` date NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201010_025853_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201010_025853_delivery cannot be reverted.\n";

        return false;
    }
    */
}
