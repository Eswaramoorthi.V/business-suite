<?php

use yii\db\Migration;

/**
 * Class m200831_065130_alter_tbl_user_persona_details
 */
class m200831_065130_alter_tbl_user_persona_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`user_personal_details` ( `user_id` INT(11) NOT NULL , `employee_code` INT(11) NOT NULL , `division` VARCHAR(256) NOT NULL , `employee_type` INT(11) NOT NULL , `full_name` VARCHAR(256) NULL , `father_name` VARCHAR(256) NOT NULL , `employees_previous_name` VARCHAR(256) NOT NULL , `present_address` VARCHAR(256) NOT NULL , `emergency_address` VARCHAR(256) NOT NULL , `permanent_address` VARCHAR(256) NOT NULL , `present_tel_nos` INT(11) NOT NULL , `permanent_tel_nos` INT(11) NOT NULL , `emergency_tel_nos` INT(11) NOT NULL , `residence_status` INT(11) NULL , `gender` INT(11) NULL , `marital_status` INT(11) NULL , `date_of_birth` DATE NULL , `date_of_appointment` DATE NULL , `place_of_birth` VARCHAR(100) NOT NULL , `blood_group` VARCHAR(256) NULL , `sponsor_code` VARCHAR(256) NOT NULL , `sponser_name` VARCHAR(256) NOT NULL , `email_id` VARCHAR(100) NOT NULL , `passport_details_number` VARCHAR(256) NULL , `name_on_passport` VARCHAR(256) NOT NULL , `issue_date` DATE NULL , `issue_place` VARCHAR(256) NULL , `expiry_date` DATE NULL , `created_by` VARCHAR(256) NULL , `created_on` TIMESTAMP NULL , `updated_by` VARCHAR(256) NULL , `updated_on` TIMESTAMP NULL , PRIMARY KEY (`user_id`)) ENGINE = InnoDB;";
           $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200831_065130_alter_tbl_user_persona_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_065130_alter_tbl_user_persona_details cannot be reverted.\n";

        return false;
    }
    */
}
