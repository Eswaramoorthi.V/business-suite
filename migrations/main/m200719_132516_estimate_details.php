<?php

use yii\db\Migration;

/**
 * Class m200719_132516_estimate_details
 */
class m200719_132516_estimate_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `estimate_details` (
            `id` int(11) NOT NULL,
            `estmate_id` int(11) NOT NULL,
            `resource_type` varchar(256) NOT NULL,
            `resource_id` int(11) NOT NULL,
            `last_purchased_cost` float DEFAULT NULL,
            `average_weighted_cost` float DEFAULT NULL,
            `status` int(11) NOT NULL DEFAULT '0',
            `created_at` datetime DEFAULT NULL,
            `created_by` int(11) DEFAULT NULL,
            `updated_at` datetime DEFAULT NULL,
            `updated_by` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->execute($sql);
        $sql1="ALTER TABLE `estimate_details` ADD PRIMARY KEY( `id`);";
        $this->execute($sql1);
        $sql2="ALTER TABLE `estimate_details` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql2);
        $sql3="ALTER TABLE `estimate_details` CHANGE `resource_id` `resource_id` INT(11) NULL DEFAULT NULL;";
        $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%estimate_details}}');
    }
}
