<?php

use yii\db\Migration;

/**
 * Class m201002_141712_alter_table_purchase_order_details
 */
class m201002_141712_alter_table_purchase_order_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $sql1 ="ALTER TABLE `purchase_order_details` ADD `material_status` INT(11) NULL AFTER `description`, ADD `expected_date` DATE NOT NULL AFTER `material_status`;";
        // $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201002_141712_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201002_141712_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }
    */
}
