<?php

use yii\db\Migration;

/**
 * Class m200719_132727_estimate_master
 */
class m200719_132727_estimate_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `estimate_master` (
            `id` int(11) NOT NULL,
            `activity_id` int(11) NOT NULL,
            `sub_activity_id` int(11) NOT NULL,
            `unit_id` int(11) NOT NULL,
            `quantity` int(11) NOT NULL,
            `description` varchar(256) DEFAULT NULL,
            `status` int(11) NOT NULL DEFAULT '0',
            `created_at` datetime DEFAULT NULL,
            `created_by` int(11) DEFAULT NULL,
            `updated_at` datetime DEFAULT NULL,
            `updated_by` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
          ";
        $this->execute($sql);
        $sql1="ALTER TABLE `estimate_master` ADD PRIMARY KEY( `id`);";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `estimate_master` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%estimate_master}}');
    }

}
