<?php

use yii\db\Migration;

/**
 * Class m200925_155847_create_table_purchase_request_master
 */
class m200925_155847_create_table_purchase_request_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`purchase_request_master` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `project_id` INT(11) NOT NULL , `reference_name` VARCHAR(255) NOT NULL , `requested_date` DATE NOT NULL , `received_date` DATE NOT NULL , `request_status` INT NOT NULL , `requested_by` VARCHAR(255) NOT NULL , `purchase_order_id` INT(11) NOT NULL , `created_by` VARCHAR(255) NOT NULL , `created_at` DATETIME NOT NULL , `updated_by` VARCHAR(255) NOT NULL , `updated_at` DATETIME NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200925_155847_create_table_purchase_request_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200925_155847_create_table_purchase_request_master cannot be reverted.\n";

        return false;
    }
    */
}
