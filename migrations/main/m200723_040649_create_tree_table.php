<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tree}}`.
 */
class m200723_040649_create_tree_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `tree` (
            `id` int(11) NOT NULL,
            `root` int(11) DEFAULT NULL,
            `lft` int(11) NOT NULL,
            `rgt` int(11) NOT NULL,
            `lvl` smallint(5) NOT NULL,
            `name` varchar(60) NOT NULL,
            `date` varchar(256) DEFAULT NULL,
            `description` varchar(256) DEFAULT NULL,
            `on_date` date DEFAULT NULL,
            `unit_type` varchar(256) DEFAULT NULL,
            `cost` float DEFAULT NULL,
            `fa_icons` text,
            `rev` int(11) DEFAULT '0',
            `for` int(11) DEFAULT NULL,
            `icon` varchar(255) DEFAULT NULL,
            `icon_type` tinyint(1) NOT NULL DEFAULT '1',
            `selected` tinyint(1) NOT NULL DEFAULT '0',
            `disabled` tinyint(1) NOT NULL DEFAULT '0',
            `readonly` tinyint(1) NOT NULL DEFAULT '0',
            `visible` tinyint(1) NOT NULL DEFAULT '1',
            `collapsed` tinyint(1) NOT NULL DEFAULT '0',
            `movable_u` tinyint(1) NOT NULL DEFAULT '1',
            `movable_d` tinyint(1) NOT NULL DEFAULT '1',
            `movable_l` tinyint(1) NOT NULL DEFAULT '1',
            `movable_r` tinyint(1) NOT NULL DEFAULT '1',
            `removable` tinyint(1) NOT NULL DEFAULT '1',
            `removable_all` tinyint(1) NOT NULL DEFAULT '0',
            `child_allowed` tinyint(1) NOT NULL DEFAULT '1',
            `active` int(11) DEFAULT NULL,
            `status` int(11) NOT NULL DEFAULT '0',
            `added_at` int(6) DEFAULT NULL,
            `updated_at` int(6) DEFAULT NULL,
            `updated_by` int(11) DEFAULT NULL,
            `added_by` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $this->execute($sql);
        $sql1="ALTER TABLE `tree` ADD PRIMARY KEY( `id`);";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `tree` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `tree` ADD `code` VARCHAR(256) NULL DEFAULT NULL AFTER `cost`;";
        $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tree}}');
    }
}
