<?php

use yii\db\Migration;

/**
 * Class m200922_123256_alter_tbl_company
 */
class m200922_123256_alter_tbl_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `company` ADD `logo` VARCHAR(255) NOT NULL AFTER `currency`;";
        $this->execute($sql1);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200922_123256_alter_tbl_company cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200922_123256_alter_tbl_company cannot be reverted.\n";

        return false;
    }
    */
}
