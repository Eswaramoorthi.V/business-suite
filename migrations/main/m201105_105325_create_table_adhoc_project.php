<?php

use yii\db\Migration;

/**
 * Class m201105_105325_create_table_adhoc_project
 */
class m201105_105325_create_table_adhoc_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `adhoc_project` (
            `id` int(11) NOT NULL,
            `customer_id` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `customer_contact` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `project_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `project_code` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `lead_code` int(11) DEFAULT NULL,
            `start_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            `end_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `status` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `project_type` int(11) NOT NULL DEFAULT '1',
            `closing_date` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `amount` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `probability` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `adhoc_project` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `adhoc_project` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105325_create_table_adhoc_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105325_create_table_adhoc_project cannot be reverted.\n";

        return false;
    }
    */
}
