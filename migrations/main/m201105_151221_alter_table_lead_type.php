<?php

use yii\db\Migration;

/**
 * Class m201105_151221_alter_table_lead_type
 */
class m201105_151221_alter_table_lead_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1="INSERT INTO `tbl_lead_type` (`id`, `type`, `label`, `active`, `sort_order`, `created_at`, `updated_at`) VALUES
        (1, 'Information Qualified Lead (IQL)', 'Information Qualified Lead (IQL)', 1, 1, 0, 0),
        (2, 'Marketing Qualified Lead (MQL)', 'Marketing Qualified Lead (MQL)', 1, 2, 0, 0),
        (3, 'Sales Qualified Lead (SQL)', 'Sales Qualified Lead (SQL)', 1, 3, 0, 0);";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_151221_alter_table_lead_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_151221_alter_table_lead_type cannot be reverted.\n";

        return false;
    }
    */
}
