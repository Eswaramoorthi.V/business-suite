<?php

use yii\db\Migration;

/**
 * Class m201009_164159_alter_table_purchase_delivery
 */
class m201009_164159_alter_table_purchase_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $sql1 ="ALTER TABLE `purchase_delivery` CHANGE `total_quantity` `order_quantity` INT(11) NOT NULL;";
       $this->execute($sql1);
       $sql2 ="ALTER TABLE `purchase_delivery` CHANGE `purchase_id` `po_id` INT(11) NOT NULL;";
       $this->execute($sql2);
        $sql3 ="ALTER TABLE `purchase_delivery` ADD `po_details_id` INT(11) NOT NULL AFTER `po_id`;";
        $this->execute($sql3);
        $sql4 ="ALTER TABLE `purchase_delivery` ADD `project_id` INT(11) NOT NULL AFTER `po_details_id`;";
        $this->execute($sql4);
        $sql5 ="ALTER TABLE `purchase_delivery` CHANGE `purchased_quantity` `delivered_quantity` INT(11) NOT NULL;";
        $this->execute($sql5);
       

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201009_164159_alter_table_purchase_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201009_164159_alter_table_purchase_delivery cannot be reverted.\n";

        return false;
    }
    */
}
