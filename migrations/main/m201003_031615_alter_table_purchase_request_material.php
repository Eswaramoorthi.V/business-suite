<?php

use yii\db\Migration;

/**
 * Class m201003_031615_alter_table_purchase_request_material
 */
class m201003_031615_alter_table_purchase_request_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_request_material` ADD `expected_date` DATE NOT NULL AFTER `material_status`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201003_031615_alter_table_purchase_request_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201003_031615_alter_table_purchase_request_material cannot be reverted.\n";

        return false;
    }
    */
}
