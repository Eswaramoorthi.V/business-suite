<?php

use yii\db\Migration;

/**
 * Class m200906_175739_alter_master
 */
class m200906_175739_alter_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `project_estimate_master` CHANGE `unit_id` `unit_id` VARCHAR(256) NULL DEFAULT NULL;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200906_175739_alter_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200906_175739_alter_master cannot be reverted.\n";

        return false;
    }
    */
}
