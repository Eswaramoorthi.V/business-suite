<?php

use yii\db\Migration;

/**
 * Class m200901_043840_alter_drop_tbl_user_personal_details
 */
class m200901_043840_alter_drop_tbl_user_personal_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="DROP TABLE user_personal_details";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200901_043840_alter_drop_tbl_user_personal_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200901_043840_alter_drop_tbl_user_personal_details cannot be reverted.\n";

        return false;
    }
    */
}
