<?php

use yii\db\Migration;

/**
 * Class m201028_151923_alter_table_tree
 */
class m201028_151923_alter_table_tree extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
 $sql1="ALTER TABLE `tree` ADD `absolute_path` VARCHAR(255) NULL DEFAULT NULL AFTER `status`;";
        $this->execute($sql1);
 $sql2="ALTER TABLE `tree` ADD `folder_path` VARCHAR(255) NULL DEFAULT NULL AFTER `absolute_path`;";
        $this->execute($sql2);
 $sql3="ALTER TABLE `tree` ADD `directory_name` VARCHAR(255) NULL AFTER `folder_path`;";
        $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201028_151923_alter_table_tree cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201028_151923_alter_table_tree cannot be reverted.\n";

        return false;
    }
    */
}
