<?php

use yii\db\Migration;

/**
 * Class m200730_110002_add_new_fields_estimate_details
 */
class m200730_110002_add_new_fields_estimate_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $sql1 ="ALTER TABLE `estimate_details` ADD `formula` VARCHAR(256) NULL AFTER `actual_cost`, ADD `percentage` FLOAT NULL AFTER `formula`;";
        // $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%estimate_details}}', 'formula');
        $this->dropColumn('{{%estimate_details}}', 'percentage');
    }
}
