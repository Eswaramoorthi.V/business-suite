<?php

use yii\db\Migration;

/**
 * Class m200925_155824_rename_table_purchase_master
 */
class m200925_155824_rename_table_purchase_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="RENAME TABLE `pricing_calculator`.`project_resource_detail` TO `pricing_calculator`.`purchase_order`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200925_155824_rename_table_purchase_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200925_155824_rename_table_purchase_master cannot be reverted.\n";

        return false;
    }
    */
}
