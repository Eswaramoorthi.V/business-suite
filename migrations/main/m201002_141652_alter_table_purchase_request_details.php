<?php

use yii\db\Migration;

/**
 * Class m201002_141652_alter_table_purchase_request_details
 */
class m201002_141652_alter_table_purchase_request_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_request_details` ADD `description` VARCHAR(255) NOT NULL AFTER `material_id`, ADD `material_status` INT(11) NOT NULL AFTER `description`, ADD `expected_date` DATE NOT NULL AFTER `material_status`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201002_141652_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201002_141652_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }
    */
}
