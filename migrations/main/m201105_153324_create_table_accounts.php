<?php

use yii\db\Migration;

/**
 * Class m201105_153324_create_table_accounts
 */
class m201105_153324_create_table_accounts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `accounts` (
            `id` int(11) NOT NULL,
            `date` timestamp NULL DEFAULT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `reference_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `credit_amount` float DEFAULT NULL,
            `debit_amount` float DEFAULT NULL,
            `credit_debit` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `amount` float NOT NULL,
            `paid_by` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `document` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NULL DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
             $this->execute($sql1);
             $sql2="ALTER TABLE `accounts` ADD PRIMARY KEY (`id`);";
             $this->execute($sql2);
             $sql3="ALTER TABLE `accounts` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
             $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_153324_create_table_accounts cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_153324_create_table_accounts cannot be reverted.\n";

        return false;
    }
    */
}
