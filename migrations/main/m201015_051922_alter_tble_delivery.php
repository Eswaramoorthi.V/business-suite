<?php

use yii\db\Migration;

/**
 * Class m201015_051922_alter_tble_delivery
 */
class m201015_051922_alter_tble_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`delivery` ( `id` INT NOT NULL , `project_id` INT(11) NULL , `supplier_id` INT(11) NULL , `delivery_date` DATE NULL , `po_number` VARCHAR(255) NULL , `delivery_order_number` VARCHAR(255) NULL , `created_by` VARCHAR(255) NULL , `created_at` DATETIME NULL , `updated_at` DATETIME NULL , `updated_by` VARCHAR(255) NULL ) ENGINE = InnoDB;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `delivery` ADD PRIMARY KEY(`id`);";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `delivery` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql3);
        $sql4 ="ALTER TABLE `delivery` CHANGE `supplier_id` `supplier_id` VARCHAR(255) NULL DEFAULT NULL;";
        $this->execute($sql4);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201015_051922_alter_tble_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201015_051922_alter_tble_delivery cannot be reverted.\n";

        return false;
    }
    */
}
