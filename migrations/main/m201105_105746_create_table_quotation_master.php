<?php

use yii\db\Migration;

/**
 * Class m201105_105746_create_table_quotation_master
 */
class m201105_105746_create_table_quotation_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `quotation_master` (
            `id` int(11) NOT NULL,
            `version_id` int(11) NOT NULL DEFAULT '0',
            `parent_master` int(11) NOT NULL DEFAULT '0',
            `client_id` int(11) NOT NULL,
            `project_id` int(11) NOT NULL,
            `quotation_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `client_address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `status` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `quotation_date` timestamp NULL DEFAULT NULL,
            `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `payment_terms` text COLLATE utf8_unicode_ci,
            `description` text COLLATE utf8_unicode_ci,
            `exclusion_text` text COLLATE utf8_unicode_ci,
            `notes` text COLLATE utf8_unicode_ci,
            `signature` text COLLATE utf8_unicode_ci NOT NULL,
            `price` text COLLATE utf8_unicode_ci NOT NULL,
            `validity` text COLLATE utf8_unicode_ci NOT NULL,
            `completion_period` text COLLATE utf8_unicode_ci NOT NULL,
            `vat` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `subtotal` float DEFAULT NULL,
            `discount_rate` float DEFAULT NULL,
            `discount_amount` float DEFAULT NULL,
            `vat_amount` float DEFAULT NULL,
            `total_amount` float DEFAULT NULL,
            `document` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `quotation_master` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `quotation_master` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105746_create_table_quotation_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105746_create_table_quotation_master cannot be reverted.\n";

        return false;
    }
    */
}
