<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%material_unit_cost}}`.
 */
class m200717_124217_create_material_unit_cost_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `material_unit_cost` (
        `id` int(11) NOT NULL,
        `master_id` int(11) NOT NULL,
        `unit_id` int(11) NOT NULL,
        `cost` float DEFAULT NULL,
        `status` int(11) NOT NULL DEFAULT '0',
        `created_at` datetime NOT NULL,
        `created_by` int(11) DEFAULT NULL,
        `updated_at` datetime DEFAULT NULL,
        `updated_by` int(11) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->execute($sql);
        $sql1="ALTER TABLE `material_unit_cost` ADD PRIMARY KEY (`id`);";
        $this->execute($sql1);
        $sql2="ALTER TABLE `material_unit_cost` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%material_unit_cost}}');
    }
}
