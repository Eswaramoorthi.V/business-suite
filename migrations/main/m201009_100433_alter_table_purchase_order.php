<?php

use yii\db\Migration;

/**
 * Class m201009_100433_alter_table_purchase_order
 */
class m201009_100433_alter_table_purchase_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_order` DROP `project_id`, DROP `project_master_id`, DROP `resource_id`, DROP `supplier_id`, DROP `project_date`, DROP `purchase_count`, DROP `cost`, DROP `material_id`, DROP `tax_number`, DROP `quantity`, DROP `price`, DROP `unit`, DROP `options`, DROP `section_name`, DROP `item_total_amount`, DROP `multiple_item`, DROP `description`, DROP `discount_rate`, DROP `discount_amount`;";
        //$this->execute($sql1);
        // $sql2 ="ALTER TABLE `purchase_order` CHANGE `provision_amount` `provision_amount` FLOAT NULL;";
        // $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201009_100433_alter_table_purchase_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201009_100433_alter_table_purchase_order cannot be reverted.\n";

        return false;
    }
    */
}
