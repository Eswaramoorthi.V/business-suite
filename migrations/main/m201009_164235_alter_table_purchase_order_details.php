<?php

use yii\db\Migration;

/**
 * Class m201009_164235_alter_table_purchase_order_details
 */
class m201009_164235_alter_table_purchase_order_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       // $sql1 ="ALTER TABLE `purchase_order_details` ADD `request_id` INT(11) NULL DEFAULT NULL AFTER `project_id`;";
       // $this->execute($sql1);
       // $sql2 ="ALTER TABLE `purchase_order_details` ADD `order_quantity` INT(11) NOT NULL AFTER `quantity`;";
       // $this->execute($sql2);
        $sql3 ="ALTER TABLE `purchase_order_details` ADD `purchase_delivery_id` INT(11) NOT NULL AFTER `request_id`;";
        $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201009_164235_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201009_164235_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }
    */
}
