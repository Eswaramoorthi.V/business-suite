<?php

use yii\db\Migration;

/**
 * Class m201002_065924_alter_table_purchase_request_material
 */
class m201002_065924_alter_table_purchase_request_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_request_material` ADD `material_status` INT(11) NOT NULL AFTER `description`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201002_065924_alter_table_purchase_request_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201002_065924_alter_table_purchase_request_material cannot be reverted.\n";

        return false;
    }
    */
}
