<?php

use yii\db\Migration;

/**
 * Class m200916_104740_create_table_project_resource_details
 */
class m200916_104740_create_table_project_resource_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`project_resource_detail` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `project_id` INT(11) NOT NULL , `project_master_id` INT(11) NOT NULL , `resource_id` INT(11) NOT NULL , `project_date` DATE NOT NULL , `purchase_count` INT(11) NOT NULL , `cost` INT(11) NOT NULL , `purchase_status` INT(11) NOT NULL , `created_by` VARCHAR(256) NULL , `created_on` TIMESTAMP NULL , `updated_by` VARCHAR(256) NULL , `updated_on` TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `project_resource_detail` ADD `supplier_id` INT(11) NOT NULL AFTER `resource_id`;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200916_104740_create_table_project_resource_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200916_104740_create_table_project_resource_details cannot be reverted.\n";

        return false;
    }
    */
}
