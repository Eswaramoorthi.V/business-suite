<?php

use yii\db\Migration;

/**
 * Class m201105_105827_create_table_transaction
 */
class m201105_105827_create_table_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `transaction` (
            `id` int(11) NOT NULL,
            `date` timestamp NULL DEFAULT NULL,
            `trans_mode` int(11) DEFAULT NULL,
            `payment_mode` int(11) DEFAULT NULL,
            `type` int(11) DEFAULT NULL,
            `ref_code` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `vehicle` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `employee_id` int(11) DEFAULT NULL,
            `client_id` int(11) DEFAULT NULL,
            `project_id` int(11) DEFAULT NULL,
            `subcontractor_id` int(11) DEFAULT NULL,
            `lpo_no` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `supplier_id` int(11) DEFAULT NULL,
            `invoice_number` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `credit_amount` float DEFAULT NULL,
            `pettycash_amount` float DEFAULT NULL,
            `expense_amount` float DEFAULT NULL,
            `vat` float DEFAULT NULL,
            `vat_percentage` float DEFAULT NULL,
            `expense_total_amount` float DEFAULT NULL,
            `document` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `particulars` text COLLATE utf8_unicode_ci,
            `created_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `transaction` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `transaction` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105827_create_table_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105827_create_table_transaction cannot be reverted.\n";

        return false;
    }
    */
}
