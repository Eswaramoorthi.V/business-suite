<?php

use yii\db\Migration;

/**
 * Class m201018_153512_drop_tble_purchase_request_material
 */
class m201018_153512_drop_tble_purchase_request_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$sql1 ="DROP TABLE purchase_request_material";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201018_153512_drop_tble_purchase_request_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201018_153512_drop_tble_purchase_request_material cannot be reverted.\n";

        return false;
    }
    */
}
