<?php

use yii\db\Migration;

/**
 * Class m201105_105616_create_table_sub_contractor
 */
class m201105_105616_create_table_sub_contractor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `sub_contractor` (
            `id` int(11) NOT NULL,
            `lpo_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `project_id` int(11) DEFAULT NULL,
            `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `date` timestamp NULL DEFAULT NULL,
            `sub_total` float DEFAULT NULL,
            `vat` int(64) DEFAULT NULL,
            `vat_amount` float DEFAULT NULL,
            `discount_rate` float DEFAULT NULL,
            `discount_amount` float DEFAULT NULL,
            `po_amount` float DEFAULT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `payment_terms` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `reference_number` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `document` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `po` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `sub_contractor` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `sub_contractor` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105616_create_table_sub_contractor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105616_create_table_sub_contractor cannot be reverted.\n";

        return false;
    }
    */
}
