<?php

use yii\db\Migration;

/**
 * Class m201013_165758_tender_resource_master
 */
class m201013_165758_tender_resource_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$sql1 ="CREATE TABLE `pricing_calculator`.`tender_resource_master` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `tender_id` INT(11) NULL , `resource_type` INT(11) NULL , `material_id` INT(11) NULL , `total_quantity` INT(11) NULL , `purchased_quantity` INT(11) NULL , `created_by` VARCHAR(255) NULL , `created_at` DATETIME NULL , `updated_by` VARCHAR(255) NULL , `updated_at` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `tender_resource_master` CHANGE `resource_type` `resource_type` VARCHAR(255) NULL DEFAULT NULL;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201013_165758_tender_resource_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201013_165758_tender_resource_master cannot be reverted.\n";

        return false;
    }
    */
}
