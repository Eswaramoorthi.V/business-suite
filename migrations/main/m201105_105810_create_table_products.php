<?php

use yii\db\Migration;

/**
 * Class m201105_105810_create_table_products
 */
class m201105_105810_create_table_products extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `products` (
            `id` int(11) NOT NULL,
            `project_id` int(11) DEFAULT NULL,
            `suppliers_id` int(11) DEFAULT NULL,
            `petty_cash_id` int(11) DEFAULT NULL,
            `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `purchase_date` timestamp NULL DEFAULT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `item_number` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `price` float DEFAULT NULL,
            `tax_amount` int(11) DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `document` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `products` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `products` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105810_create_table_products cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105810_create_table_products cannot be reverted.\n";

        return false;
    }
    */
}
