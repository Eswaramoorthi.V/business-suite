<?php

use yii\db\Migration;

/**
 * Class m201013_125208_delivery
 */
class m201013_125208_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201013_125208_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201013_125208_delivery cannot be reverted.\n";

        return false;
    }
    */
}
