<?php

use yii\db\Migration;

/**
 * Class m201009_162432_craete_table_purchase_delivery_received
 */
class m201009_162432_craete_table_purchase_delivery_received extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`purchase_delivery_received` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `purchase_delivery_id` INT(11) NOT NULL , `received_quantity` INT(11) NOT NULL , `received_date` DATE NOT NULL , `created_at` INT NOT NULL , `created_by` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201009_162432_craete_table_purchase_delivery_received cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201009_162432_craete_table_purchase_delivery_received cannot be reverted.\n";

        return false;
    }
    */
}
