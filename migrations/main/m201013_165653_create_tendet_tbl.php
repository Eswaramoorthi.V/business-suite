<?php

use yii\db\Migration;

/**
 * Class m201013_165653_create_tendet_tbl
 */
class m201013_165653_create_tendet_tbl extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`tender` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `tender_name` VARCHAR(255) NULL , `tender_code` VARCHAR(255) NULL , `status` INT(11) NULL , `project_type` INT(11) NULL , `amount` INT(11) NULL , `client_name` INT(11) NULL , `country` VARCHAR(255) NULL , `tender_due_date` DATE NULL , `building_type` INT(11) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `tender` ADD `consultant` VARCHAR(255) NULL AFTER `building_type`, ADD `total_area` INT(11) NULL AFTER `consultant`, ADD `reference` VARCHAR(255) NULL AFTER `total_area`, ADD `property_account_type` INT(11) NULL AFTER `reference`, ADD `project_status` INT(11) NULL AFTER `property_account_type`, ADD `created_by` VARCHAR(255) NULL AFTER `project_status`, ADD `created_on` DATETIME NULL AFTER `created_by`, ADD `updated_by` VARCHAR(255) NULL AFTER `created_on`, ADD `updated_on` DATETIME NULL AFTER `updated_by`, ADD `company_id` INT(11) NULL AFTER `updated_on`;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201013_165653_create_tendet_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201013_165653_create_tendet_tbl cannot be reverted.\n";

        return false;
    }
    */
}
