<?php

use yii\db\Migration;

/**
 * Class m201013_165720_tender_estimate
 */
class m201013_165720_tender_estimate extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$sql1 ="CREATE TABLE `pricing_calculator`.`tender_estimate` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `tender_id` INT(11) NULL , `tender_master_id` INT(11) NULL , `resource_type` VARCHAR(255) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `tender_estimate` ADD `resource_unit_cost` FLOAT NULL AFTER `resource_type`, ADD `resource_total_cost` FLOAT NULL AFTER `resource_unit_cost`, ADD `status` INT(11) NULL AFTER `resource_total_cost`, ADD `created_at` DATETIME NULL AFTER `status`, ADD `created_by` VARCHAR(255) NULL AFTER `created_at`, ADD `updated_by` VARCHAR(255) NULL AFTER `created_by`, ADD `updated_at` DATETIME NULL AFTER `updated_by`;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201013_165720_tender_estimate cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201013_165720_tender_estimate cannot be reverted.\n";

        return false;
    }
    */
}
