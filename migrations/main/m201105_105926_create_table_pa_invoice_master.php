<?php

use yii\db\Migration;

/**
 * Class m201105_105926_create_table_pa_invoice_master
 */
class m201105_105926_create_table_pa_invoice_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pa_invoice_master` (
            `id` int(11) NOT NULL,
            `customer_id` int(11) NOT NULL,
            `project_id` int(11) NOT NULL,
            `invoice_date` timestamp NULL DEFAULT NULL,
            `invoice_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `invoice_number` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `due_date` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `payment_terms` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `paid_type` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `subtotal` float DEFAULT NULL,
            `discount_rate` float DEFAULT NULL,
            `discount_amount` float DEFAULT NULL,
            `total_amount` float DEFAULT NULL,
            `delivery_contact` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `delivery_address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `po_number` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `notes` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `document` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_on` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_on` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `subtotal_label` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `previous_certified_amount` float DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `pa_invoice_master` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `pa_invoice_master` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105926_create_table_pa_invoice_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105926_create_table_pa_invoice_master cannot be reverted.\n";

        return false;
    }
    */
}
