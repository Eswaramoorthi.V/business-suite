<?php

use yii\db\Migration;

/**
 * Class m200929_034811_alter_table_purchase_request_material
 */
class m200929_034811_alter_table_purchase_request_material extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`purchase_request_material` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `reference_id` INT(11) NOT NULL , `material_id` INT(11) NOT NULL , `description` VARCHAR(255) NOT NULL , `total_quantity` INT(11) NOT NULL , `purchased_quantity` INT(11) NOT NULL , `order_quantity` INT(11) NOT NULL , `total_amount` FLOAT NOT NULL , `price` FLOAT NOT NULL , `created_at` DATETIME NOT NULL , `updated_at` DATETIME NOT NULL , `created_by` VARCHAR(255) NOT NULL , `updated_by` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200929_034811_alter_table_purchase_request_material cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200929_034811_alter_table_purchase_request_material cannot be reverted.\n";

        return false;
    }
    */
}
