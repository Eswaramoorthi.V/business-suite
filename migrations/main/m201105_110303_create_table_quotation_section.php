<?php

use yii\db\Migration;

/**
 * Class m201105_110303_create_table_quotation_section
 */
class m201105_110303_create_table_quotation_section extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `quotation_section` (
            `id` int(11) NOT NULL,
            `quotation_master_id` int(11) DEFAULT NULL,
            `section_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `item_total_amount` float DEFAULT NULL,
            `multiple_item` int(11) DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `quotation_section` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `quotation_section` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_110303_create_table_quotation_section cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_110303_create_table_quotation_section cannot be reverted.\n";

        return false;
    }
    */
}
