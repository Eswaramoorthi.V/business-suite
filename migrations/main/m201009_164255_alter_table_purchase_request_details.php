<?php

use yii\db\Migration;

/**
 * Class m201009_164255_alter_table_purchase_request_details
 */
class m201009_164255_alter_table_purchase_request_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_request_details` ADD `request_order_id` INT(11) NULL DEFAULT NULL AFTER `material_total_quantity`;";
       // $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201009_164255_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201009_164255_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }
    */
}
