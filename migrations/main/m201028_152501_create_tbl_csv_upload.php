<?php

use yii\db\Migration;

/**
 * Class m201028_152501_create_tbl_csv_upload
 */
class m201028_152501_create_tbl_csv_upload extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1="CREATE TABLE `pricing_calculator`.`csv_upload` ( `name` VARCHAR(255) NULL DEFAULT NULL , `description` VARCHAR(255) NULL DEFAULT NULL , `code` VARCHAR(255) NULL DEFAULT NULL , `date` DATE NOT NULL , `cost` FLOAT NULL DEFAULT NULL ) ENGINE = InnoDB;";
                $this->execute($sql1);
         $sql2="ALTER TABLE `csv_upload` ADD `parent` TEXT NULL DEFAULT NULL FIRST;";
        $this->execute($sql2);
        $sql3="ALTER TABLE `csv_upload` CHANGE `date` `date` DATE NULL;";
    $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201028_152501_create_tbl_csv_upload cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201028_152501_create_tbl_csv_upload cannot be reverted.\n";

        return false;
    }
    */
}
