<?php

use yii\db\Migration;

/**
 * Class m200907_132859_alter_tbl_project
 */
class m200907_132859_alter_tbl_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `project` ADD `employee` VARCHAR(255) NULL AFTER `system_datetime`;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `project` CHANGE `amount` `amount` FLOAT NULL;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200907_132859_alter_tbl_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200907_132859_alter_tbl_project cannot be reverted.\n";

        return false;
    }
    */
}
