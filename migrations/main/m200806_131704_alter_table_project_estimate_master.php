<?php

use yii\db\Migration;

/**
 * Class m200806_131704_alter_table_project_estimate_master
 */
class m200806_131704_alter_table_project_estimate_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `project_estimate_master` ADD `description` VARCHAR(256) NULL AFTER `sub_activity_id`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%project_estimate_master}}', 'description');
    }
}
