<?php

use yii\db\Migration;

/**
 * Class m200923_033019_create_table_purchase_master
 */
class m200923_033019_create_table_purchase_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`purchase_master` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `purchase_id` INT(11) NOT NULL , `resource_id` VARCHAR(255) NOT NULL , `description` VARCHAR(255) NOT NULL , `quantity` FLOAT NOT NULL , `unit` VARCHAR(255) NOT NULL , `price` FLOAT NOT NULL , `total_amount` FLOAT NOT NULL , `created_by` INT(11) NULL , `created_on` TIMESTAMP NULL , `updated_by` INT(11) NULL , `updated_on` TIMESTAMP NULL , `project_id` INT(11) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200923_033019_create_table_purchase_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200923_033019_create_table_purchase_master cannot be reverted.\n";

        return false;
    }
    */
}
