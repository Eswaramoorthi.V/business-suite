<?php

use yii\db\Migration;

/**
 * Class m200929_094132_alter_table_estimate_details
 */
class m200929_094132_alter_table_estimate_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `estimate_details` ADD `unit_type` VARCHAR(255) NOT NULL AFTER `quantity`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200929_094132_alter_table_estimate_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200929_094132_alter_table_estimate_details cannot be reverted.\n";

        return false;
    }
    */
}
