<?php

use yii\db\Migration;

/**
 * Class m201021_065139_alter_tble_purchase_order_details
 */
class m201021_065139_alter_tble_purchase_order_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $sql1 ="ALTER TABLE `purchase_order_details` ADD `company_id` INT(11) NULL AFTER `purchase_delivery_id`;";
        // $this->execute($sql1);
        // $sql2 ="ALTER TABLE `purchase_request_details` ADD `company_id` INT(11) NULL AFTER `request_order_id`;";
        // $this->execute($sql2);
        // $sql3 ="ALTER TABLE `purchase_request_master` ADD `company_id` INT(11) NULL AFTER `purchase_order_id`;";
        // $this->execute($sql3);
        // $sql4 ="ALTER TABLE `purchase_delivery` ADD `company_id` INT(11) NULL AFTER `purchase_delivery_id`;";
        // $this->execute($sql4);
        $sql5 ="ALTER TABLE `purchase_delivery_status` ADD `company_id` INT(11) NULL AFTER `received_date`;";
        $this->execute($sql5);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201021_065139_alter_tble_purchase_order_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201021_065139_alter_tble_purchase_order_details cannot be reverted.\n";

        return false;
    }
    */
}
