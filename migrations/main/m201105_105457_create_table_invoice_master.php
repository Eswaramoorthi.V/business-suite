<?php

use yii\db\Migration;

/**
 * Class m201105_105457_create_table_invoice_master
 */
class m201105_105457_create_table_invoice_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `invoice_master` (
            `id` int(11) NOT NULL,
            `invoice_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `invoice_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `customer_id` int(11) NOT NULL,
            `project_id` int(11) NOT NULL,
            `customer_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
            `invoice_date` timestamp NULL DEFAULT NULL,
            `due_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
            `payment_terms` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `paid_type` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `description` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `vat` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `subtotal_label` varchar(250) COLLATE utf8_unicode_ci DEFAULT 'Net Amount',
            `subtotal` float DEFAULT NULL,
            `vat_amount` float DEFAULT NULL,
            `discount_rate` float DEFAULT NULL,
            `discount_amount` float DEFAULT NULL,
            `total_amount` float DEFAULT NULL,
            `delivery_contact` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `delivery_address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `po_number` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `custom_Invoice` int(11) DEFAULT '0',
            `custom_Invoice_type` int(11) DEFAULT '0',
            `advance_percentage` float DEFAULT NULL,
            `progress_percentage` int(15) DEFAULT NULL,
            `workdone_amount` float DEFAULT NULL,
            `previous_certified_amount` float DEFAULT NULL,
            `contra_charges` float DEFAULT NULL,
            `retention_percentage` float DEFAULT NULL,
            `retention_amount` float DEFAULT NULL,
            `advance_recovery_percentage` float DEFAULT NULL,
            `advance_recovery_amount` float DEFAULT NULL,
            `notes` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `document` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
            `created_by` int(11) NOT NULL,
            `created_at` timestamp NULL DEFAULT NULL,
            `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
            `updated_at` timestamp NULL DEFAULT NULL,
            `system_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";
           $this->execute($sql1);
           $sql2="ALTER TABLE `invoice_master` ADD PRIMARY KEY (`id`);";
           $this->execute($sql2);
           $sql3="ALTER TABLE `invoice_master` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
           $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_105457_create_table_invoice_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_105457_create_table_invoice_master cannot be reverted.\n";

        return false;
    }
    */
}
