<?php

use yii\db\Migration;

/**
 * Class m201025_104449_alter_project_updated
 */
class m201025_104449_alter_project_updated extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $sql="ALTER TABLE `project` ADD `updated_at` DATE NOT NULL AFTER `location`;";
        // $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201025_104449_alter_project_updated cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201025_104449_alter_project_updated cannot be reverted.\n";

        return false;
    }
    */
}
