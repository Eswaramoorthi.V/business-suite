<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%project}}`.
 */
class m200716_071341_create_project_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%project}}', [
            'id' => $this->primaryKey(),
            'project_name' => $this->string(500)->notNull(),
            'project_code' => $this->string(500)->null(),
            'location' => $this->string(500)->notNull(),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->notNull(),
            'status' => $this->integer()->notNull(),            
            'project_type' => $this->integer()->notNull(),
            'amount'=>$this->float()->notNull(),
            'description' => $this->string(1000)->null(),
            'created_by' => $this->string(50)->null(),
            'created_on' => $this->timestamp()->null(),
            'updated_by' => $this->string(50)->null(),
            'updated_on' => $this->timestamp()->null(),
            'system_datetime' => $this->timestamp()->null(),            
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%project}}');
    }
}
