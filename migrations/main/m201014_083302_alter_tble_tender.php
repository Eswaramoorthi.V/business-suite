<?php

use yii\db\Migration;

/**
 * Class m201014_083302_alter_tble_tender
 */
class m201014_083302_alter_tble_tender extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `tender` CHANGE `project_type` `tender_type` INT(11) NULL DEFAULT NULL;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `tender` CHANGE `project_status` `tender_status` INT(11) NULL DEFAULT NULL;";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `tender` CHANGE `property_account_type` `tender_account_type` INT(11) NULL DEFAULT NULL;";
        $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201014_083302_alter_tble_tender cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201014_083302_alter_tble_tender cannot be reverted.\n";

        return false;
    }
    */
}
