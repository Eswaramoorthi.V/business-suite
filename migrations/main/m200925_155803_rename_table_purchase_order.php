<?php

use yii\db\Migration;

/**
 * Class m200925_155803_rename_table_purchase_order
 */
class m200925_155803_rename_table_purchase_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="RENAME TABLE `pricing_calculator`.`purchase_master` TO `pricing_calculator`.`purchase_order_details`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200925_155803_rename_table_purchase_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200925_155803_rename_table_purchase_order cannot be reverted.\n";

        return false;
    }
    */
}
