<?php

use yii\db\Migration;

/**
 * Class m200925_155902_create_table_purchase_request_details
 */
class m200925_155902_create_table_purchase_request_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`purchase_request_details` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `purchase_request_master_id` INT(11) NOT NULL , `material_id` INT(11) NOT NULL , `material_request_quantity` INT(11) NOT NULL , `material_received_quantity` INT(11) NOT NULL , `material_request_received_date` DATE NOT NULL , `purchase_order_id` INT(11) NOT NULL , `created_by` VARCHAR(255) NULL , `created_at` DATETIME NULL , `updated_by` VARCHAR(255) NULL , `updated_at` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200925_155902_create_table_purchase_request_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200925_155902_create_table_purchase_request_details cannot be reverted.\n";

        return false;
    }
    */
}
