<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%labour_master}}`.
 */
class m200715_092351_create_labour_master_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `labour_master` (
        `id` int(11) NOT NULL,
        `name` varchar(256) NOT NULL,
        `description` varchar(256) NOT NULL,
        `on_date` date NOT NULL,
        `lrc` float DEFAULT NULL,
        `src` float DEFAULT NULL,
        `status` int(11) NOT NULL DEFAULT '0',
        `created_at` datetime DEFAULT NULL,
        `created_by` int(11) DEFAULT NULL,
        `updated_at` datetime DEFAULT NULL,
        `updated_by` int(11) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->execute($sql);
        $sql1="ALTER TABLE `labour_master` ADD PRIMARY KEY (`id`);";
        $this->execute($sql1);
        $sql2="ALTER TABLE `labour_master` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%labour_master}}');
    }
}
