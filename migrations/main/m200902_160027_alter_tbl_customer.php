<?php

use yii\db\Migration;

/**
 * Class m200902_160027_alter_tbl_customer
 */
class m200902_160027_alter_tbl_customer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`customer` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `type` INT(11) NOT NULL , `customer_type` VARCHAR(255) NULL , `company_name` VARCHAR(255) NULL , `email` VARCHAR(255) NULL , `phone_number` VARCHAR(255) NOT NULL , `mobile_number` VARCHAR(255) NULL , `postal_code` VARCHAR(255) NULL , `website` VARCHAR(255) NULL , `address` VARCHAR(255) NULL , `address_line2` VARCHAR(255) NULL , `trn_number` VARCHAR(255) NULL , `city` VARCHAR(255) NULL , `state` VARCHAR(255) NULL , `zip_code` VARCHAR(255) NULL , `country` VARCHAR(255) NULL , `same_address` VARCHAR(255) NULL , `delivery_address` VARCHAR(255) NULL , `delivery_address_line2` VARCHAR(255) NULL , `delivery_city` VARCHAR(255) NULL , `delivery_state` VARCHAR(255) NULL , `delivery_zip_code` VARCHAR(255) NULL , `delivery_country` VARCHAR(255) NULL , `salutation` VARCHAR(255) NULL , `customer_fname` VARCHAR(255) NULL , `customer_lname` VARCHAR(255) NULL , `customer_mid_name` VARCHAR(255) NULL , `description` VARCHAR(255) NULL , `created_by` INT(11) NULL , `created_on` TIMESTAMP NULL , `updated_by` INT(11) NULL , `updated_on` TIMESTAMP NULL , `system_datetime` TIMESTAMP NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200902_160027_alter_tbl_customer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200902_160027_alter_tbl_customer cannot be reverted.\n";

        return false;
    }
    */
}
