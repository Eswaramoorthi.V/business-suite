<?php

use yii\db\Migration;

/**
 * Class m200929_034559_alter_table_purchase_order
 */
class m200929_034559_alter_table_purchase_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_order` ADD `order_type` VARCHAR(255) NOT NULL AFTER `provision_amount`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200929_034559_alter_table_purchase_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200929_034559_alter_table_purchase_order cannot be reverted.\n";

        return false;
    }
    */
}
