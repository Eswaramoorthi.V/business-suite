<?php

use yii\db\Migration;

/**
 * Class m201015_052010_alter_tble_delivery_order_details
 */
class m201015_052010_alter_tble_delivery_order_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
  $sql1 ="CREATE TABLE `pricing_calculator`.`delivery_order_details` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `delivery_id` INT(11) NULL , `po_number` VARCHAR(255) NULL , `material_id` INT(11) NULL , `description` VARCHAR(255) NULL , `delivery_status` INT(11) NULL , `order_quantity` INT(11) NULL , `delivered_quantity` INT(11) NULL , `received_quantity` INT(11) NULL , `created_by` VARCHAR(255) NULL , `created_at` DATETIME NULL , `updated_by` VARCHAR(255) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `delivery_order_details` ADD `updated_at` DATETIME NULL AFTER `updated_by`;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201015_052010_alter_tble_delivery_order_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201015_052010_alter_tble_delivery_order_details cannot be reverted.\n";

        return false;
    }
    */
}
