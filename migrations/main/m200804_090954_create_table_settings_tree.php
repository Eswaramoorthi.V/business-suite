<?php

use yii\db\Migration;

/**
 * Class m200804_090954_create_table_settings_tree
 */
class m200804_090954_create_table_settings_tree extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `settings_tree` ( 
        `id` INT(11) NOT NULL AUTO_INCREMENT , 
        `root` INT(11) NULL , 
        `lft` INT(11) NOT NULL ,
        `rgt` INT(11) NOT NULL , 
        `lvl` SMALLINT(5) NOT NULL , 
        `name` VARCHAR(256) NOT NULL ,
        `description` VARCHAR(1000) NULL ,
        `parent_id` INT(11) NULL ,
        `code` VARCHAR(256) NULL ,
        `fa_icons` TEXT NULL ,
        `rev` INT(11) NULL , 
        `for` INT(11) NULL DEFAULT '0' ,
        `icon` VARCHAR(256) NULL ,
        `icon_type` TINYINT(1) NOT NULL DEFAULT '1' ,
        `selected` TINYINT(1) NOT NULL DEFAULT '0' , 
        `disabled` TINYINT(1) NOT NULL DEFAULT '0' ,
        `readonly` TINYINT(1) NOT NULL DEFAULT '0' ,
        `visible` TINYINT(1) NOT NULL DEFAULT '1' ,
        `collapsed` TINYINT(1) NOT NULL DEFAULT '0' ,
        `movable_u` TINYINT(1) NOT NULL DEFAULT '1' ,
        `movable_d` TINYINT(1) NOT NULL DEFAULT '1' , 
        `movable_l` TINYINT(1) NOT NULL DEFAULT '1' ,
        `movable_r` TINYINT(1) NOT NULL DEFAULT '1' ,
        `removable` TINYINT(1) NOT NULL DEFAULT '1' ,
        `removable_all` TINYINT(1) NOT NULL DEFAULT '0' ,
        `child_allowed` TINYINT(1) NOT NULL DEFAULT '1' ,
        `active` INT(11) NULL ,
        `status` INT(11) NOT NULL DEFAULT '0' ,
        `added_at` INT(6) NULL , 
        `updated_at` INT(6) NULL , 
        `updated_by` INT(11) NULL ,
        `added_by` INT(11) NULL ,
        PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings_tree}}');
    }
}
