<?php

use yii\db\Migration;

/**
 * Class m201001_174214_alter_table_purchase_request_details
 */
class m201001_174214_alter_table_purchase_request_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_request_material` ADD `purchase_request_id` INT(11) NOT NULL AFTER `id`;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `purchase_request_material` ADD `project_id` INT(11) NOT NULL AFTER `reference_id`;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201001_174214_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201001_174214_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }
    */
}
