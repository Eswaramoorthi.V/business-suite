<?php

use yii\db\Migration;

/**
 * Class m200922_114540_alter_tbl_project
 */
class m200922_114540_alter_tbl_project extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
       $sql1 ="ALTER TABLE `project` ADD `company_id` INT(11) NULL AFTER `updated_on`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200922_114540_alter_tbl_project cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200922_114540_alter_tbl_project cannot be reverted.\n";

        return false;
    }
    */
}
