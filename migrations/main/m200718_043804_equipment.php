<?php

use yii\db\Migration;

/**
 * Class m200718_043804_equipment
 */
class m200718_043804_equipment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `equipment` (
            `id` int(11) NOT NULL,
            `name` varchar(256) NOT NULL,
            `description` varchar(256) DEFAULT NULL,
            `cost` float NOT NULL,
            `status` int(11) NOT NULL DEFAULT '0',
            `created_at` datetime DEFAULT NULL,
            `created_by` int(11) DEFAULT NULL,
            `updated_at` datetime DEFAULT NULL,
            `updated_by` int(11) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->execute($sql);
        $sql1="ALTER TABLE `equipment` ADD PRIMARY KEY( `id`);";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `equipment` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%equipment}}');
    }
}
