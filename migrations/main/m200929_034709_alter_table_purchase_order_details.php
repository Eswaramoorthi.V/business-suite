<?php

use yii\db\Migration;

/**
 * Class m200929_034709_alter_table_purchase_order_details
 */
class m200929_034709_alter_table_purchase_order_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $sql1 ="ALTER TABLE `purchase_order_details` ADD `material_status` VARCHAR(255) NOT NULL AFTER `description`;";
        // $this->execute($sql1);
        // $sql2 ="ALTER TABLE `purchase_order_details` ADD `total_quantity` INT(11) NOT NULL AFTER `material_status`, ADD `purchased_quantity` INT(11) NOT NULL AFTER `total_quantity`, ADD `order_quantity` INT(11) NOT NULL AFTER `purchased_quantity`;";
        // $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200929_034709_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200929_034709_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }
    */
}
