<?php

use yii\db\Migration;

/**
 * Class m201027_154757_alter_table_purchase_order_details
 */
class m201027_154757_alter_table_purchase_order_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="ALTER TABLE `purchase_order_details` ADD `document` VARCHAR(255) NULL AFTER `expected_date`;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201027_154757_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201027_154757_alter_table_purchase_order_details cannot be reverted.\n";

        return false;
    }
    */
}
