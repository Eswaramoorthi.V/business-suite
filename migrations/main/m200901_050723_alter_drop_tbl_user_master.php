<?php

use yii\db\Migration;

/**
 * Class m200901_050723_alter_drop_tbl_user_master
 */
class m200901_050723_alter_drop_tbl_user_master extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $sql1 ="DROP TABLE user_master";
        // $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200901_050723_alter_drop_tbl_user_master cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200901_050723_alter_drop_tbl_user_master cannot be reverted.\n";

        return false;
    }
    */
}
