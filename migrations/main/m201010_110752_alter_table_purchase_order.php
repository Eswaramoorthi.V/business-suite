<?php

use yii\db\Migration;

/**
 * Class m201010_110752_alter_table_purchase_order
 */
class m201010_110752_alter_table_purchase_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $sql1 ="ALTER TABLE `purchase_order` CHANGE `vat` `vat` INT(11) NULL, CHANGE `originator` `originator` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `reference` `reference` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `company_name` `company_name` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `amount` `amount` FLOAT NULL, CHANGE `currency` `currency` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `package` `package` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `payment_terms` `payment_terms` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `recommendation` `recommendation` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `discount` `discount` FLOAT NULL, CHANGE `supplier_ref` `supplier_ref` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;";
        // $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201010_110752_alter_table_purchase_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201010_110752_alter_table_purchase_order cannot be reverted.\n";

        return false;
    }
    */
}
