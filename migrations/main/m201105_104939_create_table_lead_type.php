<?php

use yii\db\Migration;

/**
 * Class m201105_104939_create_table_lead_type
 */
class m201105_104939_create_table_lead_type extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `tbl_lead_type` (
            `id` int(11) NOT NULL,
            `type` varchar(255) NOT NULL,
            `label` varchar(255) NOT NULL,
            `active` tinyint(1) NOT NULL,
            `sort_order` int(11) NOT NULL,
            `created_at` int(11) DEFAULT NULL,
            `updated_at` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            $this->execute($sql1);
            $sql2="ALTER TABLE `tbl_lead_type` ADD PRIMARY KEY (`id`);";
            $this->execute($sql2);
            $sql3="ALTER TABLE `tbl_lead_type` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;";
            $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_104939_create_table_lead_type cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_104939_create_table_lead_type cannot be reverted.\n";

        return false;
    }
    */
}
