<?php

use yii\db\Migration;

/**
 * Class m200921_072205_create_tbl_company
 */
class m200921_072205_create_tbl_company extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`company` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `company_name` VARCHAR(255) NOT NULL , `address` VARCHAR(255) NOT NULL , `contact_person` VARCHAR(255) NOT NULL , `country` VARCHAR(255) NOT NULL , `currency` VARCHAR(255) NOT NULL , `created_by` VARCHAR(255) NOT NULL , `created_at` TIMESTAMP NOT NULL , `updated_at` TIMESTAMP NOT NULL , `updated_by` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `company` CHANGE `updated_at` `updated_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200921_072205_create_tbl_company cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200921_072205_create_tbl_company cannot be reverted.\n";

        return false;
    }
    */
}
