<?php

use yii\db\Migration;

/**
 * Class m200918_134957_alter_table_project_resource_details
 */
class m200918_134957_alter_table_project_resource_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `project_resource_detail` ADD `material_id` INT(11) NOT NULL AFTER `updated_on`, ADD `supplier_name` VARCHAR(255) NOT NULL AFTER `material_id`, ADD `po_number` VARCHAR(255) NOT NULL AFTER `supplier_name`, ADD `tax_number` VARCHAR(255) NOT NULL AFTER `po_number`, ADD `date_created` DATE NOT NULL AFTER `tax_number`, ADD `due_date` DATE NOT NULL AFTER `date_created`, ADD `quantity` INT(11) NOT NULL AFTER `due_date`, ADD `price` FLOAT NOT NULL AFTER `quantity`, ADD `unit_id` VARCHAR(255) NOT NULL AFTER `price`, ADD `options` VARCHAR(255) NOT NULL AFTER `unit_id`, ADD `section_name` VARCHAR(255) NOT NULL AFTER `options`, ADD `item_total_amount` INT(11) NOT NULL AFTER `section_name`, ADD `multiple_item` INT(11) NOT NULL AFTER `item_total_amount`, ADD `project_name` VARCHAR(255) NOT NULL AFTER `multiple_item`, ADD `description` VARCHAR(255) NOT NULL AFTER `project_name`, ADD `sub_total` FLOAT NOT NULL AFTER `description`, ADD `discount_rate` FLOAT NOT NULL AFTER `sub_total`, ADD `discount_amount` FLOAT NOT NULL AFTER `discount_rate`, ADD `grand_total` FLOAT NOT NULL AFTER `discount_amount`, ADD `notes` VARCHAR(255) NOT NULL AFTER `grand_total`;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `project_resource_detail` ADD `total_amount` FLOAT NOT NULL AFTER `notes`;";
        $this->execute($sql2);
        $sql3 ="ALTER TABLE `project_resource_detail` CHANGE `unit_id` `unit` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;";
        $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200918_134957_alter_table_project_resource_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200918_134957_alter_table_project_resource_details cannot be reverted.\n";

        return false;
    }
    */
}
