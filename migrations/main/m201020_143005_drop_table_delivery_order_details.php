<?php

use yii\db\Migration;

/**
 * Class m201020_143005_drop_table_delivery_order_details
 */
class m201020_143005_drop_table_delivery_order_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="DROP TABLE delivery_order_details";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201020_143005_drop_table_delivery_order_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201020_143005_drop_table_delivery_order_details cannot be reverted.\n";

        return false;
    }
    */
}
