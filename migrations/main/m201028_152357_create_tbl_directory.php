<?php

use yii\db\Migration;

/**
 * Class m201028_152357_create_tbl_directory
 */
class m201028_152357_create_tbl_directory extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
 $sql="CREATE TABLE `pricing_calculator`.`tbl_directory` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `file` VARCHAR(255) NULL DEFAULT NULL , `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP() , `created_by` VARCHAR(255) NULL DEFAULT NULL , `updated_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP() , `updated_by` VARCHAR(255) NULL DEFAULT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201028_152357_create_tbl_directory cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201028_152357_create_tbl_directory cannot be reverted.\n";

        return false;
    }
    */
}
