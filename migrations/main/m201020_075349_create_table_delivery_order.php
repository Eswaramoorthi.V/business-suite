<?php

use yii\db\Migration;

/**
 * Class m201020_075349_create_table_delivery_order
 */
class m201020_075349_create_table_delivery_order extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `pricing_calculator`.`delivery_order` ( `id` INT(11) NOT NULL AUTO_INCREMENT , `project_id` INT(11) NULL , `po_id` INT(11) NULL , `supplier_delivery_number` VARCHAR(255) NULL , `created_at` DATETIME NULL , `created_by` INT(11) NULL , `updated_at` DATETIME NULL , `updated_by` INT(11) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201020_075349_create_table_delivery_order cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201020_075349_create_table_delivery_order cannot be reverted.\n";

        return false;
    }
    */
}
