<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%material_master}}`.
 */
class m200715_092309_create_material_master_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql="CREATE TABLE `material_master` (
        `id` int(11) NOT NULL,
        `name` varchar(256) NOT NULL,
        `description` varchar(256) NOT NULL,
        `on_date` date NOT NULL,
        `status` int(11) NOT NULL DEFAULT '0',
        `created_at` datetime DEFAULT NULL,
        `created_by` int(11) DEFAULT NULL,
        `updated_at` datetime DEFAULT NULL,
        `updated_by` int(11) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        $this->execute($sql);
        $sql1="ALTER TABLE `material_master` ADD PRIMARY KEY (`id`);";
        $this->execute($sql1);
        $sql2="ALTER TABLE `material_master` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%material_master}}');
    }
}
