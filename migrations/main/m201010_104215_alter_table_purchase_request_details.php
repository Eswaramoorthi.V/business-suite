<?php

use yii\db\Migration;

/**
 * Class m201010_104215_alter_table_purchase_request_details
 */
class m201010_104215_alter_table_purchase_request_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_request_details` CHANGE `material_received_quantity` `material_received_quantity` INT(11) NULL;";
        //$this->execute($sql1);
        $sql2 ="ALTER TABLE `purchase_request_details` CHANGE `material_request_quantity` `material_request_quantity` INT(11) NULL;";
       // $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201010_104215_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201010_104215_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }
    */
}
