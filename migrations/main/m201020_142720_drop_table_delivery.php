<?php

use yii\db\Migration;

/**
 * Class m201020_142720_drop_table_delivery
 */
class m201020_142720_drop_table_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
$sql1 ="DROP TABLE delivery";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201020_142720_drop_table_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201020_142720_drop_table_delivery cannot be reverted.\n";

        return false;
    }
    */
}
