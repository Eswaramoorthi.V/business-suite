<?php

use yii\db\Migration;

/**
 * Class m200831_064605_alter_tbl_user
 */
class m200831_064605_alter_tbl_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

         $sql1 ="ALTER TABLE `user` ADD `role` VARCHAR(255) NOT NULL AFTER `last_login_at`;";
        $this->execute($sql1);
         $sql2 ="ALTER TABLE `user` CHANGE `role` `role` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;";
        $this->execute($sql2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200831_064605_alter_tbl_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200831_064605_alter_tbl_user cannot be reverted.\n";

        return false;
    }
    */
}
