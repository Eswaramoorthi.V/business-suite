<?php

use yii\db\Migration;

/**
 * Class m201020_074507_alter_table_purchase_delivery
 */
class m201020_074507_alter_table_purchase_delivery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_delivery` ADD `lpo_status` INT(11) NULL AFTER `delivery_order_number`;";
        $this->execute($sql1);
        $sql2 ="ALTER TABLE `purchase_delivery` ADD `originator` VARCHAR(255) NULL AFTER `lpo_status`;";
        $this->execute($sql2);
         $sql3 ="ALTER TABLE `purchase_delivery` ADD `currency` INT(11) NULL AFTER `originator`;";
         $this->execute($sql3);
         $sql4 ="ALTER TABLE `purchase_delivery` ADD `payment_terms` INT(11) NULL AFTER `currency`;";
         $this->execute($sql4);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201020_074507_alter_table_purchase_delivery cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201020_074507_alter_table_purchase_delivery cannot be reverted.\n";

        return false;
    }
    */
}
