<?php

use yii\db\Migration;

/**
 * Class m201105_104001_create_table_lead
 */
class m201105_104001_create_table_lead extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="CREATE TABLE `tbl_lead` (
            `id` int(11) NOT NULL,
            `lead_code` varchar(256) NOT NULL,
            `lead_name` varchar(255) NOT NULL,
            `lead_description` longtext NOT NULL,
            `lead_type_id` int(11) DEFAULT NULL,
            `lead_status_id` int(11) NOT NULL,
            `lead_status_description` longtext,
            `lead_source_id` int(11) DEFAULT NULL,
            `lead_source_description` longtext,
            `opportunity_amount` int(11) DEFAULT NULL,
            `do_not_call` char(1) DEFAULT NULL,
            `email` varchar(255) DEFAULT NULL,
            `first_name` varchar(255) DEFAULT NULL,
            `last_name` varchar(255) DEFAULT NULL,
            `phone` varchar(255) DEFAULT NULL,
            `mobile` varchar(255) DEFAULT NULL,
            `fax` varchar(255) DEFAULT NULL,
            `created_at` int(11) DEFAULT NULL,
            `updated_at` int(11) DEFAULT NULL,
            `customer_id` int(11) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
            $this->execute($sql1);
            $sql2="ALTER TABLE `tbl_lead` ADD PRIMARY KEY (`id`);";
            $this->execute($sql2);
            $sql3="ALTER TABLE `tbl_lead` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
            $this->execute($sql3);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201105_104001_create_table_lead cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201105_104001_create_table_lead cannot be reverted.\n";

        return false;
    }
    */
}
