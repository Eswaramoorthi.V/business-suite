<?php

use yii\db\Migration;

/**
 * Class m201005_104822_alter_table_purchase_request_details
 */
class m201005_104822_alter_table_purchase_request_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `purchase_request_details` ADD `project_id` INT(11) NOT NULL AFTER `purchase_request_master_id`;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201005_104822_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201005_104822_alter_table_purchase_request_details cannot be reverted.\n";

        return false;
    }
    */
}
