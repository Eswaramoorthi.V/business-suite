<?php

use yii\db\Migration;

/**
 * Class m201010_104323_alter_table_purchase_order_detaiils
 */
class m201010_104323_alter_table_purchase_order_detaiils extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
      $sql1 ="ALTER TABLE `purchase_order_details` CHANGE `resource_id` `material_id` INT(11) NOT NULL;";
        $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201010_104323_alter_table_purchase_order_detaiils cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201010_104323_alter_table_purchase_order_detaiils cannot be reverted.\n";

        return false;
    }
    */
}
