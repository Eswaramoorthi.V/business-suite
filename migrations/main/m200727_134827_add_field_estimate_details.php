<?php

use yii\db\Migration;

/**
 * Class m200727_134827_add_field_estimate_details
 */
class m200727_134827_add_field_estimate_details extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $sql1 ="ALTER TABLE `estimate_details` ADD `actual_cost` FLOAT NULL AFTER `average_weighted_cost`;";
       $this->execute($sql1);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%estimate_details}}', 'actual_cost');
    }
}
