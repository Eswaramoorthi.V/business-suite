<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use dektrium\user\migrations\Migration;

/**
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */
class m140209_132017_init extends Migration
{
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id'                   => $this->primaryKey(),
            'username'             => $this->string(255)->notNull(),
            'email'                => $this->string(255)->notNull(),
            'password_hash'        => $this->string(60)->notNull(),
            'auth_key'             => $this->string(32)->notNull(),
            'confirmed_at'         => $this->integer()->null(),
            'unconfirmed_email'    => $this->string(255)->null(),
            'blocked_at'           => $this->integer()->null(),
            'registration_ip'      => $this->string(45),
            'primary_contact_no'      => $this->string(256),
            'user_department_id'      => $this->string(256),
            'user_group_id'      => $this->string(256),
            'password_notification' => $this->integer(1)->notNull()->defaultValue(0),
            'password_update' => $this->timestamp(),
            'reset_password' => $this->integer(1)->notNull()->defaultValue(0),
            'flags'                => $this->integer()->notNull()->defaultValue(0),
            'created_at'           => $this->integer()->notNull(),
            'updated_at'           => $this->integer()->notNull(),
            'last_login_at'        => $this->integer(),
        ], $this->tableOptions);

        $this->createIndex('{{%user_unique_username}}', '{{%user}}', 'username', true);
        $this->createIndex('{{%user_unique_email}}', '{{%user}}', 'email', true);

        $this->createTable('{{%profile}}', [
            'user_id'        => $this->integer()->notNull()->append('PRIMARY KEY'),
            'name'           => $this->string(255)->null(),
            'public_email'   => $this->string(255)->null(),
            'gravatar_email' => $this->string(255)->null(),
            'gravatar_id'    => $this->string(32)->null(),
            'location'       => $this->string(255)->null(),
            'website'        => $this->string(255)->null(),
            'bio'            => $this->text()->null(),
            'timezone'     => $this->string(40)->null(),
        ]);

        $this->addForeignKey('{{%fk_user_profile}}', '{{%profile}}', 'user_id', '{{%user}}', 'id', $this->cascade, $this->restrict);

         $this->insert('{{%user}}', array(
                "username" => 'demo',
                "email" => 'demo2@testmail.com',
                "password_hash" => '$2y$13$0Aa.K0AV1eiQyE6Wmds6OuEwCK0YOpy1qm.0GfYgQUJSD39Hc8DkW', //demo@123
                "auth_key" => 'OYQYRUXnw_AI-m20a_pB_tnuU_sdbsZb',
                "confirmed_at" => '1514101022',
                "created_at" => '1514101022',
                "updated_at" => '1514101022',
                "password_update" => date('yy-m-d'),
                "reset_password" =>1
            ));
    }

    public function down()
    {
        $this->dropTable('{{%profile}}');
        $this->dropTable('{{%user}}');
    }
}
