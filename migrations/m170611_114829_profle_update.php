<?php

use yii\db\Migration;

class m170611_114829_profle_update extends Migration
{
   public function up()
    {
       $this->addColumn('{{%profile}}', 'nric', $this->string("256")); 
        $this->addColumn('{{%profile}}', 'salutation', $this->string("50"));
        $this->addColumn('{{%profile}}', 'first_name', $this->string("256"));
        $this->addColumn('{{%profile}}', 'middle_name', $this->string("256"));
        $this->addColumn('{{%profile}}', 'last_name', $this->string("256"));
        $this->addColumn('{{%profile}}', 'secondary_contact_no', $this->string("256"));
        $this->addColumn('{{%profile}}', 'hint_question', $this->string("256"));
        $this->addColumn('{{%profile}}', 'hint_answer', $this->string("256"));     
    }

    public function down()
    {
        $this->dropColumn('{{%profile}}', 'salutation');
        $this->dropColumn('{{%profile}}', 'first_name');
        $this->dropColumn('{{%profile}}', 'nric');
        $this->dropColumn('{{%profile}}', 'middle_name');
        $this->dropColumn('{{%profile}}', 'last_name');
        $this->dropColumn('{{%profile}}', 'secondary_contact_no');
        $this->dropColumn('{{%profile}}', 'hint_question');
        $this->dropColumn('{{%profile}}', 'hint_answer');
    }
}
