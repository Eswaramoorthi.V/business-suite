<?php

namespace app\modules\tender\models;

use Yii;

/**
 * This is the model class for table "tender_estimate_master".
 *
 * @property int $id
 * @property int $tender_id
 * @property int $activity_id
 * @property int $sub_activity_id
 * @property string $description
 * @property string $unit_id
 * @property int $quantity
 * @property double $unit_rate
 * @property double $total_cost
 * @property int $status
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property string $updated_by
 */
class TenderEstimateMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tender_estimate_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tender_id', 'activity_id', 'sub_activity_id', 'quantity', 'status'], 'integer'],
            [['unit_rate', 'total_cost'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['description', 'unit_id', 'created_by', 'updated_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tender_id' => 'Tender ID',
            'activity_id' => 'Activity ID',
            'sub_activity_id' => 'Sub Activity ID',
            'description' => 'Description',
            'unit_id' => 'Unit ID',
            'quantity' => 'Quantity',
            'unit_rate' => 'Unit Rate',
            'total_cost' => 'Total Cost',
            'status' => 'Status',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
