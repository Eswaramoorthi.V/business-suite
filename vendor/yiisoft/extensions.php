<?php

$vendorDir = dirname(__DIR__);

return array (
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => '2.1.8.0',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.10.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'dektrium/yii2-rbac' => 
  array (
    'name' => 'dektrium/yii2-rbac',
    'version' => '1.0.0.0-alpha',
    'alias' => 
    array (
      '@dektrium/rbac' => $vendorDir . '/dektrium/yii2-rbac',
    ),
    'bootstrap' => 'dektrium\\rbac\\Bootstrap',
  ),
  'skeeks/yii2-widget-highcharts' => 
  array (
    'name' => 'skeeks/yii2-widget-highcharts',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@skeeks/widget/highcharts' => $vendorDir . '/skeeks/yii2-widget-highcharts',
    ),
  ),
  'moonlandsoft/yii2-phpexcel' => 
  array (
    'name' => 'moonlandsoft/yii2-phpexcel',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@moonland/phpexcel' => $vendorDir . '/moonlandsoft/yii2-phpexcel',
    ),
  ),
  'kartik-v/yii2-bootstrap4-dropdown' => 
  array (
    'name' => 'kartik-v/yii2-bootstrap4-dropdown',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/bs4dropdown' => $vendorDir . '/kartik-v/yii2-bootstrap4-dropdown/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid/src',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform/src',
    ),
  ),
  'kartik-v/yii2-helpers' => 
  array (
    'name' => 'kartik-v/yii2-helpers',
    'version' => '1.3.9.0',
    'alias' => 
    array (
      '@kartik/helpers' => $vendorDir . '/kartik-v/yii2-helpers/src',
    ),
  ),
  'kartik-v/yii2-builder' => 
  array (
    'name' => 'kartik-v/yii2-builder',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/builder' => $vendorDir . '/kartik-v/yii2-builder/src',
    ),
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf/src',
    ),
  ),
  '2amigos/yii2-ckeditor-widget' => 
  array (
    'name' => '2amigos/yii2-ckeditor-widget',
    'version' => '2.1.0.0',
    'alias' => 
    array (
      '@dosamigos/ckeditor' => $vendorDir . '/2amigos/yii2-ckeditor-widget/src',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.12.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient/src',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.2.7.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'dektrium/yii2-user' => 
  array (
    'name' => 'dektrium/yii2-user',
    'version' => '0.9.14.0',
    'alias' => 
    array (
      '@dektrium/user' => $vendorDir . '/dektrium/yii2-user',
    ),
    'bootstrap' => 'dektrium\\user\\Bootstrap',
  ),
  'nullref/yii2-datatables' => 
  array (
    'name' => 'nullref/yii2-datatables',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@nullref/datatable' => $vendorDir . '/nullref/yii2-datatables/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'philippfrenzel/yii2fullcalendar' => 
  array (
    'name' => 'philippfrenzel/yii2fullcalendar',
    'version' => '4.0.2.0',
    'alias' => 
    array (
      '@yii2fullcalendar' => $vendorDir . '/philippfrenzel/yii2fullcalendar',
    ),
  ),
  'loveorigami/yii2-notification-wrapper' => 
  array (
    'name' => 'loveorigami/yii2-notification-wrapper',
    'version' => '6.7.2.0',
    'alias' => 
    array (
      '@lo/modules/noty' => $vendorDir . '/loveorigami/yii2-notification-wrapper/src',
    ),
    'bootstrap' => 'lo\\modules\\noty\\Bootstrap',
  ),
  'la-haute-societe/yii2-save-relations-behavior' => 
  array (
    'name' => 'la-haute-societe/yii2-save-relations-behavior',
    'version' => '1.7.2.0',
    'alias' => 
    array (
      '@lhs/Yii2SaveRelationsBehavior' => $vendorDir . '/la-haute-societe/yii2-save-relations-behavior/src',
    ),
  ),
  'rmrevin/yii2-fontawesome' => 
  array (
    'name' => 'rmrevin/yii2-fontawesome',
    'version' => '2.17.1.0',
    'alias' => 
    array (
      '@rmrevin/yii/fontawesome' => $vendorDir . '/rmrevin/yii2-fontawesome',
    ),
  ),
  'yiister/yii2-gentelella' => 
  array (
    'name' => 'yiister/yii2-gentelella',
    'version' => '1.3.1.0',
    'alias' => 
    array (
      '@yiister/gentelella' => $vendorDir . '/yiister/yii2-gentelella',
    ),
  ),
  'yii2mod/yii2-c3-chart' => 
  array (
    'name' => 'yii2mod/yii2-c3-chart',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@yii2mod/c3/chart' => $vendorDir . '/yii2mod/yii2-c3-chart',
    ),
  ),
  'phpnt/yii2-export' => 
  array (
    'name' => 'phpnt/yii2-export',
    'version' => '0.0.7.0',
    'alias' => 
    array (
      '@phpnt/exportFile' => $vendorDir . '/phpnt/yii2-export',
    ),
  ),
  'kartik-v/yii2-widget-typeahead' => 
  array (
    'name' => 'kartik-v/yii2-widget-typeahead',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/typeahead' => $vendorDir . '/kartik-v/yii2-widget-typeahead/src',
    ),
  ),
  'kartik-v/yii2-widget-touchspin' => 
  array (
    'name' => 'kartik-v/yii2-widget-touchspin',
    'version' => '1.2.3.0',
    'alias' => 
    array (
      '@kartik/touchspin' => $vendorDir . '/kartik-v/yii2-widget-touchspin/src',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker/src',
    ),
  ),
  'kartik-v/yii2-widget-switchinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-switchinput',
    'version' => '1.3.1.0',
    'alias' => 
    array (
      '@kartik/switchinput' => $vendorDir . '/kartik-v/yii2-widget-switchinput',
    ),
  ),
  'kartik-v/yii2-widget-spinner' => 
  array (
    'name' => 'kartik-v/yii2-widget-spinner',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/spinner' => $vendorDir . '/kartik-v/yii2-widget-spinner/src',
    ),
  ),
  'kartik-v/yii2-widget-sidenav' => 
  array (
    'name' => 'kartik-v/yii2-widget-sidenav',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/sidenav' => $vendorDir . '/kartik-v/yii2-widget-sidenav',
    ),
  ),
  'kartik-v/yii2-widget-rating' => 
  array (
    'name' => 'kartik-v/yii2-widget-rating',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/rating' => $vendorDir . '/kartik-v/yii2-widget-rating/src',
    ),
  ),
  'kartik-v/yii2-widget-rangeinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-rangeinput',
    'version' => '1.0.2.0',
    'alias' => 
    array (
      '@kartik/range' => $vendorDir . '/kartik-v/yii2-widget-rangeinput/src',
    ),
  ),
  'kartik-v/yii2-widget-growl' => 
  array (
    'name' => 'kartik-v/yii2-widget-growl',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@kartik/growl' => $vendorDir . '/kartik-v/yii2-widget-growl',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.0.9.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput/src',
    ),
  ),
  'kartik-v/yii2-widget-depdrop' => 
  array (
    'name' => 'kartik-v/yii2-widget-depdrop',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@kartik/depdrop' => $vendorDir . '/kartik-v/yii2-widget-depdrop/src',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '1.4.9.0',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker/src',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => '1.4.7.0',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker/src',
    ),
  ),
  'kartik-v/yii2-widget-colorinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-colorinput',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/color' => $vendorDir . '/kartik-v/yii2-widget-colorinput/src',
    ),
  ),
  'kartik-v/yii2-widget-alert' => 
  array (
    'name' => 'kartik-v/yii2-widget-alert',
    'version' => '1.1.1.0',
    'alias' => 
    array (
      '@kartik/alert' => $vendorDir . '/kartik-v/yii2-widget-alert',
    ),
  ),
  'kartik-v/yii2-widget-affix' => 
  array (
    'name' => 'kartik-v/yii2-widget-affix',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@kartik/affix' => $vendorDir . '/kartik-v/yii2-widget-affix',
    ),
  ),
  'kartik-v/yii2-widgets' => 
  array (
    'name' => 'kartik-v/yii2-widgets',
    'version' => '3.4.1.0',
    'alias' => 
    array (
      '@kartik/widgets' => $vendorDir . '/kartik-v/yii2-widgets/src',
    ),
  ),
  'kartik-v/yii2-detail-view' => 
  array (
    'name' => 'kartik-v/yii2-detail-view',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/detail' => $vendorDir . '/kartik-v/yii2-detail-view/src',
    ),
  ),
  'kartik-v/yii2-date-range' => 
  array (
    'name' => 'kartik-v/yii2-date-range',
    'version' => '1.7.2.0',
    'alias' => 
    array (
      '@kartik/daterange' => $vendorDir . '/kartik-v/yii2-date-range/src',
    ),
  ),
  'kartik-v/yii2-popover-x' => 
  array (
    'name' => 'kartik-v/yii2-popover-x',
    'version' => '1.3.5.0',
    'alias' => 
    array (
      '@kartik/popover' => $vendorDir . '/kartik-v/yii2-popover-x/src',
    ),
  ),
  'kartik-v/yii2-editable' => 
  array (
    'name' => 'kartik-v/yii2-editable',
    'version' => '1.7.8.0',
    'alias' => 
    array (
      '@kartik/editable' => $vendorDir . '/kartik-v/yii2-editable/src',
    ),
  ),
  'kartik-v/yii2-slider' => 
  array (
    'name' => 'kartik-v/yii2-slider',
    'version' => '1.3.2.0',
    'alias' => 
    array (
      '@kartik/slider' => $vendorDir . '/kartik-v/yii2-slider',
    ),
  ),
  'kartik-v/yii2-sortable' => 
  array (
    'name' => 'kartik-v/yii2-sortable',
    'version' => '1.2.2.0',
    'alias' => 
    array (
      '@kartik/sortable' => $vendorDir . '/kartik-v/yii2-sortable/src',
    ),
  ),
  'kartik-v/yii2-money' => 
  array (
    'name' => 'kartik-v/yii2-money',
    'version' => '1.2.3.0',
    'alias' => 
    array (
      '@kartik/money' => $vendorDir . '/kartik-v/yii2-money/src',
    ),
  ),
  'kartik-v/yii2-checkbox-x' => 
  array (
    'name' => 'kartik-v/yii2-checkbox-x',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/checkbox' => $vendorDir . '/kartik-v/yii2-checkbox-x/src',
    ),
  ),
  'kartik-v/yii2-label-inplace' => 
  array (
    'name' => 'kartik-v/yii2-label-inplace',
    'version' => '1.2.3.0',
    'alias' => 
    array (
      '@kartik/label' => $vendorDir . '/kartik-v/yii2-label-inplace/src',
    ),
  ),
  'kartik-v/yii2-tabs-x' => 
  array (
    'name' => 'kartik-v/yii2-tabs-x',
    'version' => '1.2.7.0',
    'alias' => 
    array (
      '@kartik/tabs' => $vendorDir . '/kartik-v/yii2-tabs-x/src',
    ),
  ),
  'kartik-v/yii2-icons' => 
  array (
    'name' => 'kartik-v/yii2-icons',
    'version' => '1.4.5.0',
    'alias' => 
    array (
      '@kartik/icons' => $vendorDir . '/kartik-v/yii2-icons/src',
    ),
  ),
  'kartik-v/yii2-ipinfo' => 
  array (
    'name' => 'kartik-v/yii2-ipinfo',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/ipinfo' => $vendorDir . '/kartik-v/yii2-ipinfo/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.14.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.8.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.4.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'codemix/yii2-excelexport' => 
  array (
    'name' => 'codemix/yii2-excelexport',
    'version' => '2.7.2.0',
    'alias' => 
    array (
      '@codemix/excelexport' => $vendorDir . '/codemix/yii2-excelexport/src',
    ),
  ),
  'creocoder/yii2-nested-sets' => 
  array (
    'name' => 'creocoder/yii2-nested-sets',
    'version' => '0.9.0.0',
    'alias' => 
    array (
      '@creocoder/nestedsets' => $vendorDir . '/creocoder/yii2-nested-sets/src',
    ),
  ),
  'kartik-v/yii2-tree-manager' => 
  array (
    'name' => 'kartik-v/yii2-tree-manager',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/tree' => $vendorDir . '/kartik-v/yii2-tree-manager/src',
    ),
  ),
  'perminder-klair/yii2-dropzone' => 
  array (
    'name' => 'perminder-klair/yii2-dropzone',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kato' => $vendorDir . '/perminder-klair/yii2-dropzone',
    ),
  ),
  'kartik-v/yii2-dropdown-x' => 
  array (
    'name' => 'kartik-v/yii2-dropdown-x',
    'version' => '1.2.0.0',
    'alias' => 
    array (
      '@kartik/dropdown' => $vendorDir . '/kartik-v/yii2-dropdown-x',
    ),
  ),
  'kartik-v/yii2-nav-x' => 
  array (
    'name' => 'kartik-v/yii2-nav-x',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/nav' => $vendorDir . '/kartik-v/yii2-nav-x/src',
    ),
  ),
  'mihaildev/yii2-ckeditor' => 
  array (
    'name' => 'mihaildev/yii2-ckeditor',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@mihaildev/ckeditor' => $vendorDir . '/mihaildev/yii2-ckeditor',
    ),
  ),
);
