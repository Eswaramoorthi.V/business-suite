<?php

namespace app\commands;

use app\models\SiteDataMigration;
use livefactory\components\TaskHelper;
use livefactory\models\HistoryModel;
use livefactory\models\Location;
use livefactory\models\LocationGroup;
use livefactory\models\Organization;
use livefactory\models\ProjectOrg;
use livefactory\models\ProjectSites;
use livefactory\models\Project;
use livefactory\models\ProjectLocationContractor;
use livefactory\models\TaskAttachments;
use livefactory\models\TaskTemplates;
use livefactory\models\TaskTemplatesDocuments;
use Yii;
use yii\console\Controller;
use yii\db\DataReader;
use yii\db\Exception;
use livefactory\models\Milestone;
use livefactory\models\Task;
use livefactory\models\TaskStatus;
use yii\helpers\ArrayHelper;
use livefactory\models\TaskOrg;
use livefactory\models\CodeType;
use livefactory\models\Code;
use livefactory\models\InventorySitesResarved;
use livefactory\models\TaskLastId;
use yii\helpers\Html;

use livefactory\models\Datamigration;

class DataMigrationController extends Controller
{


    public function actionIndex()
    {

        echo $filepath = Yii::$app->basePath . "/resource_master.csv";



//        die();
        $handle = fopen($filepath, "r");
        $r = 1;
        $i = 0;
        while ($data = fgetcsv($handle, 1000, "\n")) {

            pr($data);

            $i++;

        }
    }

    public function actionExecute()
    {

        $this->actionLocationUpdate();
        //$this->actionSiteInventoryUpdate();
        $this->actionOrg();
        $this->actionProjectSite();
        $this->actionProjectSiteContractor();
        $this->actionProjectOrg();
//         $this->actionTaskUpdates();

    }

    public function actionSiteInventoryUpdate()
    {
        echo "Site Inventory Updates";

        $data = $this->getSitemigration();
        $module = CodeType::find()->where(['prefix' => 'Module'])->one();
        $invertor = CodeType::find()->where(['prefix' => 'Inverter'])->one();

        $moduleassetTypeId = !empty($module) ? $module->id : "";

        $invertorassetTypeId = !empty($invertor) ? $invertor->id : "";


        $moduleList = CodeType::findByCodeReIndex('Module');
        $inverterList = CodeType::findByCodeReIndex('Inverter');

        $invertorList = ['12_ktl', '20ktl', '36ktl', '60ktl'];


        foreach ($data as $d) {
            $postalCode = isset($d['postal_code']) ? $d['postal_code'] : "";
            //$postalCode=isset($d['postal_code'])?$d['postal_code']:"";
            $moduleType = isset($d['watt_class']) ? $d['watt_class'] : "";
            $qty = isset($d['qty']) ? $d['qty'] : "";

            $moduleaAssetInfo = isset($moduleList[$moduleType]) ? $moduleList[$moduleType] :
                "";

            $moduleassetId = !empty($moduleaAssetInfo) ? $moduleaAssetInfo['id'] : "";

            $siteInvertor = [];
            foreach ($invertorList as $key => $inv) {
                if (isset($d[$inv]) and $d[$inv] != 0) {
                    $invName = 'SUN2000-' . strtoupper($inv);
                    $siteInvertor[$invName] = $d[$inv];
                }

            }

            if ($postalCode != '') {
                $locationObj = Location::find()->where(['postal_code' => $postalCode])->one();
                if (!empty($locationObj)) {
                    $locationId = $locationObj->id;
                    if ($moduleassetId != '' and $moduleassetTypeId != '') {

                        $siteInventory = InventorySitesResarved::find()->where(['asset_category' => $moduleassetTypeId, 'asset_type' => $moduleassetId, 'location_id' => $locationId])->one();
                        if (empty($siteInventory)) {
                            $siteInventory = new InventorySitesResarved();
                        }
                        $siteInventory->location_id = $locationId;
                        $siteInventory->asset_category = $moduleassetTypeId;
                        $siteInventory->asset_type = $moduleassetId;
                        $siteInventory->qty = $qty;
                        //  pr($siteInventory);
                        if (!$siteInventory->save()) {
                            pr($siteInventory->getErrors());
                        }

                        if (!empty($siteInvertor)) {

                            foreach ($siteInvertor as $keyName => $value) {
                                echo "Key Name-->" . $keyName;
                                echo "\n";
                                // pr($inverterList);
                                $invId = isset($inverterList[$keyName]) ? $inverterList[$keyName]['id'] : '';
                                // echo "sasdasd--->".$invId;
                                // die();
                                if ($invId != '' and $invertorassetTypeId != '') {
                                    $siteInverterInventory = InventorySitesResarved::find()->where(['asset_category' => $invertorassetTypeId, 'asset_type' => $invId, 'location_id' => $locationId])->one();
                                    if (empty($siteInverterInventory)) {
                                        $siteInverterInventory = new InventorySitesResarved();
                                    }
                                    $siteInverterInventory->location_id = $locationId;
                                    $siteInverterInventory->asset_category = $invertorassetTypeId;
                                    $siteInverterInventory->asset_type = $invId;
                                    $siteInverterInventory->qty = $value;
//                                    //  pr($siteInverterInventory);
                                    if (!$siteInverterInventory->save()) {
                                        pr($siteInverterInventory->getErrors());
                                    }


                                }


                            }


                        }


                    }


                }


            }
        }

    }

    public function actionLocationUpdate()
    {
        echo "Location Update";
        $data = $this->getSitemigration();
        $project=$this->getProjects();
        $projectName=$project['project_name'];

        foreach ($data as $d) {
            $locationGroup = $d['location_group'];
            $locationGroupObj = LocationGroup::find()->where(['name' => $locationGroup])->one();
            if (empty($locationGroupObj)) {
                $locationGroupObj = new LocationGroup();
                $locationGroupObj->name = $locationGroup;
                $locationGroupObj->description = $locationGroup;
                $locationGroupObj->active = 1;


                try {
                    if (!$locationGroupObj->save()) {

                        pr($locationGroupObj->getErrors());
                    }

                } catch (Exception $e) {
                    pr($e);
                }
            }

            $postalCode = $d['postal_code'];

            $locationObj = Location::find()->where(['postal_code' => $postalCode])->one();
            //pr($locationObj);

            if (empty($locationObj)) {
                echo $d['postal_code'];
                echo "\n";
                $locationObj = new Location();


                //  pr1($d);
                $siteName = $projectName . "_" . $d['postal_code'];
                $locationGroupId = $locationGroupObj->id;
                $locationObj->name = $siteName;
                $locationObj->group_id = $locationGroupId;
                $locationObj->address = ucwords(strtolower($d['site_address']));
                $locationObj->country = 197;
                $locationObj->postal_code = $d['postal_code'];
                $locationObj->block_id = $d['blk_no'];
                $locationObj->module_quantity = $d['qty'];
                $locationObj->module_capacity = $d['watt_class'];
                $locationObj->meter_serial_num = $d['module_brand'];
                $total_capcity = ((int)$d['qty'] * (int)$d['watt_class']) / 1000;
                $locationObj->total_capcity = $total_capcity;
                $locationObj->added_by = (bool) 1;
//                $locationObj->active = 1;
                $locationObj->migration_update=1;

                // pr1($d);

                $startDate = ($d['ms1_start_date'] != '') ? $d['ms1_start_date'] : $d['ms2_start_date'];
                $endDate = $d['ms5_end_date'];
                $db_startdate = '';
                $db_enddate = '';
                if ($startDate != '') {
                    $startDate = str_replace("/", "-", $startDate);
                    $db_startdate = date('Y-m-d', strtotime($startDate));

                }
                if ($endDate != '') {
                    $endDate = str_replace("/", "-", $endDate);
                    $db_enddate = date('Y-m-d', strtotime($endDate));

                }
                $locationObj->start_date = $db_startdate;
                $locationObj->end_date = $db_enddate;
                // pr1($locationObj);
                try {
                    if (!$locationObj->save()) {
                        pr1($locationObj->getErrors());
                    }
                } catch (Exception $e) {
                    pr1($e->getMessage());
                }


            }
        }

    }

    public function actionOrg()
    {
        $projects = $this->getProjects();
        $projectId = $projects['project_id'];
        $projectName = $projects['project_name'];
        $data = SiteDataMigration::find()->groupBy('contractor')->asArray()->all();
        foreach ($data as $d) {
            $contractor = $d['contractor'];
            $model = Organization::find()
                ->where(['LIKE', 'name', $contractor])->one();
            if (empty($model)) {
                $model = new Organization();
                echo "Contractor Not Exisit--->" . $contractor . "-->" . $d['ms1_status'];
                echo "\n";
                echo "\n";

                if ('Not Assigned' != $contractor) {
                    $model->name = $contractor;
                    $model->country = 197;
                    $model->postal_code = $d['postal_code'];
                    $model->mobile_number = 1111111;
                    $model->primary_contact = 111111;
                    $model->code = 111111;
                    $model->abbreviation = $contractor;
                    $model->uen = "1";

                    $model->migration_update = 1;
                    //pr1($model);
                    try {
                        if (!$model->save()) {
                            pr($model);
                            pr1($model->getErrors());
                        }
                    } catch (Exception $e) {
                        pr($e->getMessage());
                    }

                }
                //$d['m1_status'];


            } else {
                //pr1($model);
//            echo "Contractor  Exisit--->". $contractor;
//            echo "\n";
//            echo "\n";
            }


        }
    }

    public function actionProjectSite()
    {
        $projects = $this->getProjects();
        $projectid = $projects['id'];
        $projectName = $projects['project_name'];
        $data = $this->getSitemigration();

        foreach ($data as $d) {
            $locationObj = Location::find()->where(['postal_code' => $d['postal_code']])->one();

            if (!empty($locationObj)) {
                $siteId = $locationObj->id;
                $locationObj = ProjectSites::find()->where(['project_id' => $projectid, 'location_id' => $siteId])->one();
                // $migationUpdate=0;
                if (empty($locationObj)) {
                    $locationObj = new ProjectSites();
                    $migationUpdate = 1;

                    try {
                        $locationObj->project_id = $projectid;
                        $locationObj->location_id = $siteId;
                        $locationObj->is_group = 0;
                        $locationObj->status_id = 1;
                        $locationObj->migration_update = $migationUpdate;
                        #$locationObj->save();
                        if (!$locationObj->save()) {
                            pr($locationObj);
                            pr1($locationObj->getErrors());
                        }


                    } catch (Exception $e) {
                        pr($e->getMessage());
                        die();
                    }
                }

            } else {
                echo "Postal Code Not Exisit" . $d['postal_code'];
                echo "\n";
            }
        }

    }

    public function actionProjectSiteContractor()
    {   $data = $this->getSitemigration();
        $projects = $this->getProjects();
        $projectid = $projects['id'];
        foreach ($data as $d) {
            $locationObj = Location::find()->where(['postal_code' => $d['postal_code']])->one();
            $contractor = $d['contractor'];
            $contractorModel = Organization::find()
                ->where(['LIKE', 'name', $contractor])->one();
            if (!empty($locationObj) && !empty($contractorModel)) {
                $siteId = $locationObj->id;
                $contractorId = $contractorModel->id;
                echo $siteId . "--" . $contractorId . "---" . $projectid;
                echo "\n";
                // die();
                $locationObj = ProjectLocationContractor::find()->where(['project_id' => $projectid, 'location_id' => $siteId, 'contractor_id' => $contractorId])->one();
                $updateOn=0;
                if (empty($locationObj)) {
                    $locationObj = new ProjectLocationContractor();
                    $updateOn = 1;


                    try {
                        $locationObj->project_id = $projectid;
                        $locationObj->location_id = $siteId;
                        $locationObj->contractor_id = $contractorId;
                        $locationObj->migration_update = $updateOn;
                        $locationObj->save();


                    } catch (Exception $e) {
                        pr($e->getMessage());

                    }
                }

            } else {
                echo "Postal Code Not Exisit" . $d['postal_code'];
                echo "\n";
            }


        }

    }

    public function actionProjectOrg()
    {
        $projects = $this->getProjects();
        $projectid = $projects['id'];
        $data = $this->getSitemigration();
        foreach ($data as $d) {
            $contractor = $d['contractor'];
            $model = Organization::find()
                ->where(['LIKE', 'name', $contractor])->one();
            if (!empty($model)) {
                $contractorName = $model->name;
                $contractorId = $model->id;
                echo $contractorName . "---" . $contractorId;
                echo "\n";
                $contractorObJ = ProjectOrg::find()->where(['project_id' => $projectid, 'org_id' => $contractorId])->one();
                $updateOn=0;
                if (empty($contractorObJ)) {
                    $contractorObJ = new ProjectOrg();
                    $updateOn = 1;

                    try {
                        $contractorObJ->project_id = $projectid;
                        $contractorObJ->org_id = $contractorId;
                        $contractorObJ->migration_update = $updateOn;
                        $contractorObJ->save();
                    } catch (Exception $e) {
                        pr($e->getMessage());

                    }
                }


            } else {
                echo "Not Exisit" . $contractor;
                echo "\n";
            }

        }

    }


    public function actionTaskUpdates()
    {
        $data = $this->getSitemigration();
        $taskStatusList = ArrayHelper::map(TaskStatus::find()->where("active=1")->orderBy('sort_order')->asArray()->all(), 'id', 'label');
        $projects = $this->getProjects();
        $projectId = $projects['id'];

        $milesotnelist = [
            'ms1' => ['name' => 'milestone_1', 'start_date' => 'ms1_start_date', 'end_date' => 'ms1_end_date', 'status' => 'ms1_status'],
            'ms2' => ['name' => 'milestone_2', 'start_date' => 'ms2_start_date', 'end_date' => 'ms2_end_date', 'status' => 'ms2_status'],
            'ms3' => ['name' => 'milestone_3', 'start_date' => 'ms3_start_date', 'end_date' => 'ms3_end_date', 'status' => 'ms3_status'],
            'ms4' => ['name' => 'milestone_4', 'start_date' => 'ms4_start_date', 'end_date' => 'ms4_end_date', 'status' => 'ms4_status'],
            'ms5' => ['name' => 'milestone_5', 'start_date' => 'ms5_start_date', 'end_date' => 'ms5_end_date', 'status' => 'ms5_status'],

        ];
        $codeList = \livefactory\models\Code::getcodeTypes();
        $codeType = $codeList['task'];
        $codeTypeName = \livefactory\models\CodeType::find()->where(['name' => $codeType])->one();

        foreach ($data as $d) {
            echo "\n";
            echo  Yii::getLogger()->getElapsedTime()."-->".$d['sno'] . "---->" . $d['postal_code'];
            echo "\n";


            $locationObj = Location::find()->where(['postal_code' => $d['postal_code']])->one();

            $contractor = $d['contractor'];

            $contractorModel = Organization::find()
                ->where(['LIKE', 'name', $contractor])->one();

            if (!empty($contractorModel)) {
                $taskOrg = $contractorModel->id;

            } else {
                $taskOrg = "";

            }


            foreach ($milesotnelist as $m) {

                $name = $m['name'];
                $stDate = $m['start_date'];
                $enDate = $m['end_date'];
                $status = $m['status'];
                $mileStoneName = trim($d[$name]);
                $startdate = isset($d[$stDate]) ? trim($d[$stDate]) : "";
                $enddate = isset($d[$enDate]) ? trim($d[$enDate]) : "";
                $taskStatus = isset($d[$status]) ? $d[$status] : "";

                $taskStatuskey = array_search($taskStatus, $taskStatusList); // $key = 2;
                if ($taskStatuskey == 0) {
                    $taskStatuskey = 8;
                }

                $model = Milestone::find()
                    ->where(['LIKE', 'name', $mileStoneName])->one();

                if (!empty($model)) {

                    $mileStoneId = $model->id;
                    $mileStoneName = $model->name;
                    $startDate = '';
                    $endDate = "";
                    if ($startdate != '') {
                        $startdate = str_replace("/", "-", $startdate);
                        $startDate = date('Y-m-d', strtotime($startdate));
                    }
                    if ($enddate != '') {
                        $enddate = str_replace("/", "-", $enddate);
                        $endDate = date('Y-m-d', strtotime($enddate));
                    }


                    $milestonesql = Task::find()->where(['project_id' => $projectId, 'location_id' => $locationObj->id, 'org_id' => $taskOrg, 'milestone_id' => $mileStoneId, 'parent_id' => 0]);
                    $milestoneObj = $milestonesql->one();


                    if (empty($milestoneObj)) {
                        $milestoneObj = new Task();
                    }


                    $orgId = $this->getLastInsertid('milstone');

                    $stringId = "Milestone_" . str_pad($orgId, 6, "0", STR_PAD_LEFT);

                    //$stringId = "Milestone_" . $stringId;

                    $milestoneObj->task_name = $mileStoneName;
                    $milestoneObj->task_description = $mileStoneName;
                    $milestoneObj->task_progress = ($taskStatuskey == 3) ? 100 : '';
                    $milestoneObj->task_status_id = $taskStatuskey;
                    $milestoneObj->task_source = 2;
                    $milestoneObj->project_id = $projectId;
                    if (!empty($locationObj)) {
                        $milestoneObj->location_id = $locationObj->id;
                    } else {
                        $milestoneObj->location_id = "";
                    }


                    $milestoneObj->parent_id = 0;
                    $milestoneObj->milestone_id = $mileStoneId;
                    $milestoneObj->task_id = $stringId;
                    $milestoneObj->expected_start_datetime = $startDate;
                    $milestoneObj->expected_end_datetime = $endDate;
                    $milestoneObj->actual_start_datetime = $startDate;
                    $milestoneObj->actual_end_datetime = $endDate;
                    $milestoneObj->sort_order = 1;
                    $milestoneObj->user_assigned_id = 1;
                    if (!empty($taskOrg)) {
                        $milestoneObj->org_id = $taskOrg;
                    } else {

                        $milestoneObj->org_id = "";
                    }

                    try {
                        if (!$milestoneObj->save()) {
                            pr($milestoneObj->getErrors());
                        } else {

                            $taskMilestoneId = $milestoneObj->id;
                            $taskOrgObj = new TaskOrg();
                            $taskOrgObj->project_id = $projectId;
                            $taskOrgObj->task_id = $taskMilestoneId;
                            $taskOrgObj->org_id = $taskOrg;
                            $taskOrgObj->save();

                        }

                        $taskList = TaskTemplates::find()->where(['milestone' => $mileStoneId])->asArray()->all();
                        $parentId = $milestoneObj->id;
                        $taskData = [];
                        $taskAutoIncrement = $this->getLastInsertid('task');
                        foreach ($taskList as $k => $taskname) {
                            $mileStoneId = $model->id;


                            $stringId = 'Task_' . str_pad($taskAutoIncrement, 5, "0", STR_PAD_LEFT);
                            $temp['task_name'] = $taskname['name'];
                            $temp['task_description'] = $taskname['name'];
                            $temp['task_template_id'] = $taskname['id'];
                            $temp['task_progress'] = ($taskStatuskey == 3) ? 100 : '';
                            $temp['task_status_id'] = $taskStatuskey;
                            $temp['task_source'] = 2;
                            $temp['project_id'] = $projectId;
                            if (!empty($locationObj)) {
                                $temp['location_id'] = $locationObj['id'];
                            } else {
                                $temp['location_id'] = "";
                            }
                            if (!empty($contractorModel)) {
                                $temp['org_id'] = $contractorModel['id'];
                            } else {
                                $temp['org_id'] = "";
                            }

                            $temp['parent_id'] = $parentId;
                            $temp['milestone_id'] = $mileStoneId;
                            $temp['task_id'] = $stringId;
                            $temp['expected_start_datetime'] = $startDate;
                            $temp['expected_end_datetime'] = $endDate;
                            $temp['sort_order'] = $k + 1;
                            $temp['user_assigned_id'] = 1;
                            $temp['actual_start_datetime'] = $startDate;
                            $temp['actual_end_datetime'] = $endDate;
                            $taskData[] = $temp;


//                           try {
//                               if (!$taskObj->save()) {
//                                   pr($taskObj->getErrors());
//                               } else {
//
//                                   $taskId = $taskObj->id;
//                                   $taskOrgObj = new TaskOrg();
//                                   $taskOrgObj->project_id = $projectId;
//                                   $taskOrgObj->task_id = $taskId;
//                                   $taskOrgObj->org_id = $taskOrg;
//                                   $taskOrgObj->save();
//
//                               }
//
//                           } catch (Exception $e) {
//                               pr($e);
//                           }

                            $taskAutoIncrement=$taskAutoIncrement+1;

                        }

                        $connection = \Yii::$app->db;
                        try {
                            $connection->createCommand()->batchInsert('tbl_task',
                                ['task_name', 'task_description', 'task_template_id', 'task_progress', 'task_status_id', 'task_source', 'project_id', 'location_id', 'org_id', 'parent_id', 'milestone_id', 'task_id', 'expected_start_datetime', 'expected_end_datetime', 'sort_order', 'user_assigned_id', 'actual_start_datetime', 'actual_end_datetime'],
                                $taskData
                            )
                                ->execute();

                            $this->updateTaskLastId($taskAutoIncrement,'task');

                        } catch (Exception $e) {
                            pr($e);
                        }
//

                    } catch (Exception $e) {
                        pr($e);
                    }


                }


            }

        }


    }

    public function getSitemigration()
    {
//        $sql=$data = SiteDataMigration::find()->where(['ms2_status' => 'Completed'])->andWhere(['>','sno',20]);
//        sql($sql);
//        die();
        //$data = SiteDataMigration::find()->andWhere(['>', 'sno', 370])->asArray()->all();
        $data = SiteDataMigration::find()->limit(1000)->asArray()->all();
        echo Yii::getLogger()->getElapsedTime();
        // pr($data);
        // die();
        return $data;

    }

    public function actionUpdateSiteStatus()
    {

        $data = Task::find()->where(['milestone_id' => 12, 'parent_id' => 0, 'task_status_id' => 3])->asArray()->all();

        foreach ($data as $d) {
            $locationId = $d['location_id'];
            $projectid = $d['project_id'];
            $locationObj = ProjectSites::find()->where(['project_id' => $projectid, 'location_id' => $locationId])->one();
            try {
                if (!empty($locationObj)) {
                    $locationObj->status_id = 3;
                    $locationObj->save();
                } else {
                    echo "Location id" . $locationId;
                }


            } catch (Exception $e) {
                pr($e);
                die();
            }

        }
    }

    public function actionClear()
    {
        $list = ['tbl_task_org', 'tbl_task', 'tbl_project_org', 'tbl_project_location_contractor', 'tbl_project_sites', 'tbl_location_group', 'tbl_location'];
        foreach ($list as $l) {
            try {
                $l = (string)$l;
                $q1 = \Yii::$app->db->createCommand()->truncateTable($l)->execute();

//                  }
            } catch (Exception $ex) {
                pr($ex);
            }
        }
    }

    public function actionProjectCapacityUpdate()
    {
        $getallProjects = Project::find()->all();
        // pr($getallProjects);

        foreach ($getallProjects as $project) {
            $projectid = $project->id;
            $locationData = ProjectSites::find()->with('location')->where(['project_id' => $projectid, 'status_id' => 3])->asArray()->all();
            $totalGeneratedValue = 0;
            foreach ($locationData as $locationValues) {

                if (isset($locationValues['location']['total_capcity']) and !empty($locationValues['location']['total_capcity'])) {
                    $siteCapacity = $locationValues['location']['total_capcity'];
                    $totalGeneratedValue = $totalGeneratedValue + $siteCapacity;
                }

            }
            $totalMw = ($totalGeneratedValue != 0) ? ($totalGeneratedValue / 1000) : 0;

            $project->capacity = round($totalMw, 2);
            try {
                if (!$project->save()) {
                    pr($project->getErrors());
                }
            } catch (Exception $e) {
                pr($e);
            }

        }

    }

    public function actionProjectProgressUpdate()
    {
        $getallProjects = Project::find()->all();
        // pr($getallProjects);

        foreach ($getallProjects as $project) {
            $projectid = $project->id;
            $CompletedLocation = ProjectSites::find()->where(['project_id' => $projectid, 'status_id' => 3])->count();
            $assignedLocation = ProjectSites::find()->where(['project_id' => $projectid])->count();
            $progress = 0;

            $progress = ($CompletedLocation / $assignedLocation) * 100;


            $project->project_progress = round($progress, 2);
            try {
                if (!$project->save()) {
                    pr($project->getErrors());
                }
            } catch (Exception $e) {
                pr($e);
            }

        }

    }

    public function actionPostalUpdate()
    {

        $filepath = Yii::$app->basePath . "\sn2_postal_code.csv";
        $handle = fopen($filepath, "r");
        $r = 1;

        $i = 0;
        $c = 0;

        while ($data = fgetcsv($handle, 1000, "\n")) {
            if ($i != 0) {
                $datavalues = $data[0];
                $splitValue = explode(",", $datavalues);
                //pr($splitValue);
                $postalCode = $splitValue[0];
                $long = $splitValue[1];
                $lat = $splitValue[1];
                if (is_numeric($long) and is_numeric($lat)) {
                    $locationGroupObj = Location::find()->where(['postal_code' => $postalCode])->one();
                    if (!empty($locationGroupObj)) {
                        // pr1($locationGroupObj);
                        $locationGroupObj->latitude = $lat;
                        $locationGroupObj->longitude = $long;
                        $locationGroupObj->save();
                        echo "Updated" . $locationGroupObj->postal_code;
                        echo "";
                        $c++;

                    }

                } else {

                }
                //die();

            }
            $i++;

        }
        echo "Total Updated Coune" . $c++;
    }

    public function getProjects()
    {
        $projects = [];
        $projects = Project::find()->where(['project_id' => 'SN3'])->asArray()->one();

        return $projects;
    }

    public function actionSiteUpdate()
    {
        echo "Site Updates";
        $projectid = 2;
//        pr(Yii::$app->db);
//        die();
        $projectSites = ProjectSites::find()->with('location')->where(['project_id' => $projectid])->asArray()->all();
        foreach ($projectSites as $projectSite) {
            // pr($projectSite);
            $locationId = $projectSite['location_id'];

            $query = "UPDATE tbl_location SET name = REPLACE(name, 'SN3_', 'SN2_') where id=" . $locationId;
            $command = Yii::$app->db->createCommand($query);
            $command->execute();


        }

    }

    public function actionIndex2()
    {
        echo $filepath = Yii::$app->basePath . "\punchlist.csv";
        echo "Punch List\n";
        $handle = fopen($filepath, "r");
        $r = 1;
        $i = 0;
        while ($data = fgetcsv($handle, 1000, "\n")) {
            if ($i != 0) {
                $datavalues = $data[0];
                $splitDataValues = explode(",", $datavalues);
                $ownnerId = $splitDataValues[1];
                $contractorId = $splitDataValues[7];
                $projectName = $splitDataValues[6];
//                pr($splitDataValues);
//                die();

                // echo $projectId;


                $ownnerData = Organization::find()
                    ->where(['LIKE', 'abbreviation', $ownnerId])->one();

                $OwnerId = $ownnerData->id;

                echo $projectName;
                die();

                $project = Project::find()->where(['name' => $projectName])->one();

//                $project = Project::find()
//                    ->where(['LIKE', 'name', $projectName])->one();

                pr($project);

                $contractorData = Organization::find()
                    ->where(['LIKE', 'abbreviation', $contractorId])->one();
                $contractorId = $contractorData->id;

                $contractorData = Organization::find()
                    ->where(['LIKE', 'abbreviation', $contractorId])->one();
                $contractorId = $contractorData->id;
            }

            $i++;
        }
    }

    public function actionTest2()
    {


        $codeList = Code::findByAllCode();
        pr($codeList);


    }

    public function checkTaskExisit()
    {
        echo "Task Exisit";
    }

    public function actionMilestone()
    {
        echo "SSS";

        $milestone = Milestone::find()->asArray()->all();
        $monthCount = [];
        $reformatData = [];
        foreach ($milestone as $m) {
            $milestoneId = $m['id'];
            $data = Task::find()->with('location')->where(['milestone_id' => $milestoneId, 'parent_id' => 0])->asArray()->all();

            foreach ($data as $key => $d) {
                $refDate = date('Y-m', strtotime($d['expected_end_datetime']));
                $reformatData[$refDate][] = $d;
                if (isset($monthCount[$milestoneId][$refDate])) {
                    $monthCount[$milestoneId][$refDate] = $monthCount[$milestoneId][$refDate] + 1;
                } else {
                    $monthCount[$milestoneId][$refDate] = 1;
                }
            }




            $undefiendDatesList=$reformatData['1970-01'];
            foreach ($undefiendDatesList as $list){
//                pr($list);
//                die();
                $taskId=$list['id'];
                $postalCode=isset($list['location']['postal_code'])?$list['location']['postal_code']:"";
                $siteData=$data = SiteDataMigration::find()->where(['postal_code' => $postalCode])->one();
                //sql($sql);

                $hiostingDate=$siteData->ms2_start_date;

                $startDate = str_replace("/", "-", $hiostingDate);
                $dbStartdate = date('Y-m-d', strtotime($startDate));
                $dbEnddate = date('Y-m-d', strtotime($startDate));
                $endDate=$this->getDateSubtract($dbStartdate);
                $stDate=$this->getDateSubtract($dbEnddate,21);
                echo $startDate."---->".$stDate."--->".$endDate;
                $taskObj = Task::find()->where(['id' => $taskId])->one();

                $taskObj->expected_start_datetime=$stDate;
                $taskObj->expected_end_datetime=$endDate;
                $taskObj->actual_start_datetime=$stDate;
                $taskObj->actual_end_datetime=$endDate;
                if(!$taskObj->save()){
                    pr($taskObj->getErrors());
                }



            }
            die();

        }


        foreach ($monthCount as $monthVal) {
            ksort($monthVal);
            pr($monthVal);
        }


    }

    public function getLastInsertid($type)
    {

        $obj = TaskLastId::find()->where(['type' => $type])->one();
        if (!empty($obj)) {
            $lastId = $obj->last_insert_id + 1;
        } else {
            $obj = new TaskLastId();
            $lastId = 1;
            $obj->type = $type;
        }

        $obj->last_insert_id = $lastId;
        if (!$obj->save()) {
            pr($obj->getErrors());
        }

        return $lastId;


    }
    public function updateTaskLastId($lastId,$type)
    {

        $obj = TaskLastId::find()->where(['type' => $type])->one();
        $obj->last_insert_id = $lastId;
        if (!$obj->save()) {
            pr($obj->getErrors());
        }

        return $lastId;


    }


    public function getDateSubtract($dateval, $day = 1)
    {
        $currentDate = new \DateTime($dateval);

//Subtract a day using DateInterval
        $interVal = 'P' . $day . 'D';
        $yesterdayDT = $currentDate->sub(new \DateInterval($interVal));

//Get the date in a YYYY-MM-DD format.
        $yesterday = $yesterdayDT->format('Y-m-d');
//Print it out.
        return $yesterday;
    }


    public function actionUpdateLocationName()
    {
        $project=Project::find()->where(['project_name'=>'SN3'])->one();
        $projectSites=ProjectSites::find()->where(['project_id'=>$project['id']])->asArray()->all();
        $AssignSites=ArrayHelper::map($projectSites,'id','location_id');


        foreach($AssignSites as $site){
            $sql="UPDATE tbl_location 
    SET name = REPLACE(name,'SN2_', 'SN3') WHERE id=$site";


            $command = yii::$app->db->createCommand($sql);
            $command->execute();
        }


        echo "DONE";
        die();
    }

    public function actionMigration(){
        echo "Data Migration";
        echo $filepath = Yii::$app->basePath . "\\datamigration_V1.csv";

        $fields= Datamigration::getTableSchema()->getColumnNames();


//        echo "Hello\n";
//
//        die();
        $handle = fopen($filepath, "r");
        $r = 1;
        $i = 0;
        $task="task";
        while ($data = fgetcsv($handle, 2000, "\n")) {
            $obj = new Datamigration();
            $splitData=explode(",",$data[0]);
            foreach ($fields as $k=>$key){
                $pattern = "/date/i";
                if(preg_match($pattern, $key)){
                    if($splitData[$k]!=''){
                        $value =  Yii::$app->formatter->asDate($splitData[$k], 'php:Y-m-d');
                    }else{
                        $value = date('Y-m-d');
                    }

                }else{
                    if($key=='capacity' && $splitData[$k]==''){
                        $splitData[$k]=0;
                    }
                    $value=$splitData[$k];
                }
                $obj->$key=$value;
            }

            try{
               if(!$obj->save()){
                   echo "\n\n";
                   pr($obj->getErrors());
               }
            }catch (Exception $e){
                echo "\n\n";
                    pr($e->getMessage());
            }



//IFC Uploaded
//Structures Ordered
//LIfting Plan and Approvals Uploaded
//Notices For Hoisting Uploaded
//            Transmittal  and Delivery Order Uploaded
//Hoisting Completed
//SPPG Checklist and Declaration Uploaded (CS1)
//Installation Completed
//As-Built Drawings Uploaded
//Notices for Shutdown Uploaded
//            Shut Down and TIe-In Completed
//QAQC Checklist Completed
//T&C Checklist Completed
//SPPG Documents Uploaded (SP Reply)
//COC Uploaded
//SPPG Turn-On Letter Uploaded
//Turn-On Completed
//COPS Uploaded
//Handover Documents Uploaded




//            $datamigration->postal_code=$splitData[1];
//            $datamigration->contractor=$splitData[2];
//            $datamigration->site_unit=$splitData[3];
//            $datamigration->task1='IFA Uploaded';
//            $datamigration->task1_startdate=$splitData[4];
//            $datamigration->task1_enddate=$splitData[5];
//            $datamigration->task1_status=$splitData[6];
//            $datamigration->task2='Site Survey Completed';
//            $datamigration->task2_startdate=$splitData[7];
//            $datamigration->task2_enddate=$splitData[8];
//            $datamigration->task2_status=$splitData[9];
//
//
//
//
//            die();



        }
    }

    public function actionTaskSave()
    {
//        $Data = $_REQUEST;
//        $projectId = $Data['id'];
//        $taskData = json_decode($Data['task'], TRUE);
//        $siteData = json_decode($Data['site'], TRUE);
//        $contractor = $Data['contractor'];
//
        $projectId=1;

        $missingContractor=[];
        $siteData = Datamigration::find()->asArray()->all();
        $siteIdList=[];

        foreach ($siteData as $siteval) {
            $milestoneIds = [];
            $contractor=$siteval['contractor'];

//            $sql=\livefactory\models\search\Location::find()->where(['postal_code'=>$siteval['postal_code']]);
//            //sql($sql);
            $locationDetails= \livefactory\models\search\Location::find()->where(['postal_code'=>$siteval['postal_code']])->one();
            $contractorData = \livefactory\models\search\Organization::find()->where(['LIKE', 'name', $contractor])->asArray()->one();
            $locationId=$locationDetails['id'];
            if(empty($contractorData)){
                $missingContractor[]=$contractor;
            }
            for ($i=1;$i<=21;$i++) {
                $taskName = 'task'.$i;
                $startDate=$taskName."_startdate";
                $endDate=$taskName."_enddate";
                $taskStatus = $taskName."_status";
                $taskValue=$siteval[$taskName];
                $taskStart=$siteval[$startDate];
                $taskend=$siteval[$endDate];
                $taskStatusVal=$siteval[$taskStatus];
                $getTaskData = TaskStatus::find()->where(['status'=>$taskStatusVal])->asArray()->one();



                echo $taskValue."--".$taskStart."--".$taskend;
                echo "\n";


                $taskTemplate = TaskTemplates::find()->where(['name' => $taskValue])->asArray()->one();
               // pr($checkparent);
                $tasktemplateId=$taskTemplate['id'];
//
                $TemplateDoc = TaskTemplatesDocuments::find()->where(['template' => $tasktemplateId])->asArray()->all();
//

                $startDate = $taskStart;
                $ednDate= $taskend;
                $milestoneID = $taskTemplate['milestone'];
                $approval = $taskTemplate['is_approval'];
                $approval_user = $taskTemplate['approved_by'];
                $milestoneName = Milestone::find()->where(['id' => $milestoneID])->one();

                $milestone = Task::find()->where(['project_id' => $projectId])->andWhere(['location_id' => $locationDetails['id']])->andWhere(['milestone_id'=>$milestoneName['id']])->one();

                if (empty($milestone)) {
                    $milestone = new Task();
                    $milestone->project_id = $projectId;
                    $milestone->milestone_id = $milestoneName['id'];
                    $milestone->sort_order = 0;
                    $milestone->task_source = 1;

                    $milestone->expected_start_datetime = '';
                    $milestone->expected_end_datetime = '';
                    $milestone->actual_start_datetime = '';
                    $milestone->actual_end_datetime = '';
                    $milestone->approval_required = $approval;
                    $milestone->approval_user = $approval_user;
                    $milestone->task_name = $milestoneName['name'];
                    $milestone->task_status_id = $getTaskData['id'];
                    $milestone->delete_status=0;
                    $milestone->user_assigned_id=1;
                    $milestone->org_id=$contractorData['id'];
                    $milestone->added_by_user_id=1;
                    $milestone->location_id = $locationDetails['id'];
                    $milestone->task_priority_id = 1;
                    $milestone->task_priority_id = 1;
                    $milestone->added_by=1;

                    try {
                        if (!$milestone->save()) {
                            echo "Row".$i;

                            pr($milestone->getErrors());
                            die();
                        }else{
                            $milestoneIdvalue=$milestone->id;
                            $milestoneIds[]=$milestoneIdvalue;

                            $stringId12 ='Task' . str_pad($milestoneIdvalue, 5, "0", STR_PAD_LEFT);
                            $milestone->task_id = $stringId12;
                            $milestone->save();
                        }
                    } catch (Exception $e) {
                        pr($e->getMessage());
                        die();
                    }
                }
                else{
                    $milestoneIdvalue=$milestone->id;
                    $milestoneIds[]=$milestoneIdvalue;
                }
                $task = Task::find()->where(['project_id' => $projectId])->andWhere(['location_id' => $locationDetails['id']])->andWhere(['task_template_id'=>$tasktemplateId])->one();

                if(empty($task)){
                    $task = new Task();
                }
                $task->project_id = $projectId;
                $task->milestone_id = $milestoneName['id'];
                $task->task_template_id = $tasktemplateId;
                $task->sort_order = $taskTemplate['sort_order'];
                $task->approval_required = $approval;
                $task->approval_user = $approval_user;
                $task->task_source = 1;
                $task->expected_start_datetime = $startDate;
                $task->expected_end_datetime = $ednDate;
                $task->actual_start_datetime = $startDate;
                $task->actual_end_datetime = $ednDate;
                $task->parent_id = $milestone['id'];
                $task->task_status_id = $getTaskData['id'];
                $task->task_name = $taskTemplate['name'];
                $task->location_id = $locationDetails['id'];
                if($getTaskData['id']==3){
                    $task->task_progress = 100;
                }

                try {
                    if (!$task->save()) {
                       // echo \Yii::$app->getSession()->setFlash('error', '  Tasks are Not Assigned');
                       // pr($task->getErrors());

                        echo "Row".$i;

                        pr($milestone->getErrors());
                        die();
                    } else {
                        $orgId1=$task->id;
                        $stringId12 ='Task' . str_pad($orgId1, 5, "0", STR_PAD_LEFT);
                        $task->task_id = $stringId12;
                        $task->save();

                       // echo \Yii::$app->getSession()->setFlash('success', ' Sucessfully Tasks are Assigned');

                    }
                } catch (Exception $e) {
                    pr($e->getMessage());
                    die();
                }
                if ($task->save()) {

                    foreach ($TemplateDoc as $docval) {
                        $taskdoc = new TaskAttachments();
                        $taskdoc->task = $task['id'];
                        $taskdoc->project_id = $projectId;
                        $taskdoc->location_id = $locationDetails['id'];
                        $taskdoc->is_approval = $docval['is_approval'];
                        $taskdoc->classification = $docval['classification'];
                        $taskdoc->contractor_id = $contractorData['id'];
                        $taskdoc->approval_role = $taskTemplate['doc_approved_by'];
                        try {
                            if (!$taskdoc->save()) {
                                pr($taskdoc->getErrors());
                            }
                        } catch (Exception $e) {
                            pr($e->getMessage());
                            die();
                        }
                    }


                    $org = new TaskOrg();
                    $org->project_id = $projectId;
                    $org->task_id = $task['id'];
                    $org->org_id = $contractorData['id'];;
                    try {
                        if (!$org->save()) {
                            pr($org->getErrors());
                        }
                    } catch (Exception $e) {
                        pr($e->getMessage());
                        die();
                    }

//                    $taskParent=$task['parent_id'];
////                    pr($taskParent);
//                    $milestonestatus = Task::find()->andwhere(['id' =>$taskParent])->one();
//                    $status=1;
//                    $sql = "select min(expected_start_datetime) as stdate,max(expected_end_datetime) as endate from tbl_task where parent_id=$taskParent";
//                    $dateList = Task::findBySql($sql)->asArray()->one();
//
//                    $milestonestatus->expected_start_datetime =  $dateList['stdate'];
//                    $milestonestatus->expected_end_datetime =  $dateList['endate'];
//                    try {
//                        if (!$milestonestatus->save()) {
//                            pr($milestonestatus->getErrors());
//
//                        }
//                    } catch (Exception $e) {
//                        pr($e);
//                    }
//                    $date=$dateList['endate'];
//                    TaskHelper::projectMileStoneUpdates($taskParent,$projectId,$siteval['value'],$milestonestatus['milestone_id'],$milestonestatus['id'],$date,$status);

                }
//
//                $date1->modify('+1 day');
            }
           // pr($milestoneIds);
            $milestoneIds=array_values(array_unique($milestoneIds));
            //pr($milestoneIds);
            TaskHelper::upsertTaskMileStone($milestoneIds);
            TaskHelper::upsertTaskSite($projectId,$locationId);
            TaskHelper::upsertSiteUpdate($projectId,$locationId);


        }

        pr($missingContractor);
    }

    public function actionTaskProgressSave()
    {

        $projectId=1;
        $projecttask = Task::find(0)->where(['project_id' => $projectId]);
        $tasksum = $projecttask->sum('task_progress');
        $taskCount = $projecttask->count();
        $projectprogress = $tasksum / $taskCount;
//        pr($projectprogress);
//        die();
        $projectTaskSave = Project::find(0)->where(['id' => $projectId])->one();

        $projectTaskSave->project_progress = $projectprogress;
        try {
            if (!$projectTaskSave->save()) {
                pr($projectTaskSave->getErrors());

            }
        } catch (Exception $e) {
            pr($e);
        }
        pr($projectTaskSave);
        die();
        echo "Project Progress Saved";
        die();


    }

    public function actionDataMaping(){

        echo "Data Migration";
        echo $filepath = Yii::$app->basePath . "\\SN3_Sites_EPC_Tag.csv";

        $fields= Datamigration::getTableSchema()->getColumnNames();

        $handle = fopen($filepath, "r");
        $r = 1;
        $i = 0;
        $task="task";
        while ($data = fgetcsv($handle, 2000, "\n")) {
            if($i!=0){
                   $restData=reset($data);
                   $splitData=explode(",",$restData);
                   //pr($splitData);
                   $postalCode=$splitData[0];
                   $contractor1=$splitData[1];
                   $contractor2=$splitData[2];
                    $getLocation= Location::find(0)->where(['postal_code'=>$postalCode])->asArray()->one();
                    $getContractor = Organization::find(0)->where(['name'=>$contractor2])->asArray()->one();
//                   pr($getLocation);
//                   die();
                    echo "Location".$locationId = $getLocation['id'];
                    echo "\n";
                    $contractorId = $getContractor['id'];
                   if(!empty($getLocation) and !empty($getContractor)){
                       $projectLocation = ProjectLocationContractor::find(0)->where(['location_id'=>$locationId])->one();
                       if(empty($projectLocation)){
                           $projectLocation = new ProjectLocationContractor();
                           $proLocationId ="";
                           $projectLocation->added_by=1;
                       }else{
                           $proLocationId= $projectLocation->contractor_id;

                       }

                       echo "Project Id". $proLocationId;
                       echo "\n";

                       $projectLocation->contractor_id = $contractorId;
                       $projectLocation->old_contractor_id = $proLocationId;
//                       pr($projectLocation->attributes);
//                    die();
                       $projectLocation->save();


                   }

            }
            $i++;

        }
    }
    public function actionTaskUpdate(){
        echo "SSSSS";
        $taskOrg = TaskOrg::find(0)->with('task')->asArray()->all();
        pr($taskOrg);
        foreach ($taskOrg as $orgData){
            pr($orgData);
            die()   ;

        }

    }

}

