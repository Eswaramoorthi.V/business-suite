<?php

namespace app\commands;
use yii\console\Controller;
use app\models\Settings;

class SettingsController extends Controller {

    public function actionSync() {
        \Yii::$app->db->createCommand()->truncateTable('settings')->execute();
        \Yii::$app->db->createCommand("
INSERT INTO `settings` (`id`, `code`, `name`, `description`, `parent_id`, `status`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '001', 'Unit', 'desc', NULL, 1, NULL, NULL, NULL, NULL),
(2, '002', 'Activity', 'desc', NULL, 1, NULL, NULL, NULL, NULL),
(5, '005', 'Sq.Mtr', 'desc', 1, 1, NULL, NULL, NULL, NULL),
(6, '006', 'Item', 'desc2', 1, 1, NULL, NULL, NULL, NULL),
(7, '006', 'Mtrs', 'mtres description', 1, 1, NULL, NULL, NULL, NULL),
(8, '008', 'Nos', 'Nos description', 1, 1, NULL, NULL, NULL, NULL),
(10, '0099', 'Set', 'set description', 1, 1, NULL, NULL, NULL, NULL),
(11, '007', 'Lot', 'lot desc', 1, 1, NULL, NULL, NULL, NULL),
(12, '02', 'Demolition', 'demo desc', 2, 1, NULL, NULL, NULL, NULL),
(13, '03', 'Partition', 'partition 3', 2, 1, NULL, NULL, NULL, NULL),
(14, '04', 'Flooring', 'floring desc', 2, 1, NULL, NULL, NULL, NULL),
(15, '005', 'Wall Finishes', 'wall desc', 2, 1, NULL, NULL, NULL, NULL),
(16, '05', 'Ceiling', 'desc ceiling', 2, 1, NULL, NULL, NULL, NULL),
(17, '06', 'Ceiling Finishes', 'desc ceiling', 2, 1, NULL, NULL, NULL, NULL),
(18, '007', 'Steel Doors', 'doors desc', 2, 1, NULL, NULL, NULL, NULL),
(19, '004', 'Glass Works', 'desc work', 2, 1, NULL, NULL, NULL, NULL),
(20, '07', 'Joinery Works', 'Joinery works', 2, 1, NULL, NULL, NULL, NULL),
(21, '007', 'Lift', 'lift desc', 2, 1, NULL, NULL, NULL, NULL),
(22, '', 'd1', 'Demolishing, sorting and transporting of existing block wall  and disposing the debris as per DM norms.', 12, 1, NULL, NULL, NULL, NULL),
(23, '323', 'd2', 'Builders work for MEP including cutting, chasing, make good of the same, Foundations, false boxes, Grill openings with wooden frames, undercuts, air tight partition etc', 12, 1, NULL, NULL, NULL, NULL);

")->execute();
    }

}
