<?php

namespace app\commands;

use app\models\Tree1;
//use kartik\tree\models\Tree;
use kartik\tree\TreeSecurity;
use kartik\tree\TreeView;
use Yii;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use app\models\CsvUpload;
use app\models\Tree;

class DataMigrationController extends Controller
{
    public $treeData =[];
    public $level;
    public function actionIndex()
    {
        echo $filepath = Yii::$app->basePath . "/importTree1.csv";
        $handle = fopen($filepath, "r");
        $i = 0;
        \Yii::$app->db->createCommand()->truncateTable('tree')->execute();
        while ($data = fgetcsv($handle, 1000, "\n")) {
            if ($i != 0) {
                $obj = new Tree1();
                $arrFields = array_keys($obj->attributes);
                $datavalues = $data[0];
                $splitDataValues = explode(",", $datavalues);
                // pr($splitDataValues);
                // die();
                try {
                    foreach ($arrFields as $k => $v) {
                        $d1 = isset($splitDataValues[$k]) ? $splitDataValues[$k] : "";
                        $obj->$v = Html::encode($d1);
                        $obj->selected=0;
                        $obj->disabled=0;
                        $obj->readonly=0;
                        $obj->collapsed=0;
                        $obj->status=0;
                        $obj->removable_all=0;
                        $obj->icon_type=1;
                        $obj->visible=1;
                        $obj->movable_u=1;
                        $obj->movable_d=1;
                        $obj->movable_l=1;
                        $obj->movable_r=1;
                        $obj->removable=1;
                        $obj->child_allowed=1;
                        $obj->date=NULL;
                        $obj->description=NULL;
                        $obj->on_date=NULL;
                        $obj->unit_type=NULL;
                        $obj->cost=NULL;
                    }
                //   pr($obj);
                // die();
                if (!$obj->save()) {
                    pr($obj->getErrors());
                }
                } catch (Exception $e) {
                    pr($e->getMessage());
                }
            }
            $i++;
        }
    }

    public function actionRun()
    {
        echo $filepath = Yii::$app->basePath . "/tree1.csv";
        $handle = fopen($filepath, "r");
        $i = 0;
        $res=[];
        // \Yii::$app->db->createCommand()->truncateTable('tree')->execute();
        while ($data = fgetcsv($handle, 1000, "\n")) {
            if ($i != 0) {
                $obj = new Tree1();
                $arrFields = array_keys($obj->attributes);
                $datavalues = $data[0];
                $splitDataValues = explode(",", $datavalues);
                if($splitDataValues[0] &&  !empty($splitDataValues[0])){
                    $name = $splitDataValues[0];
                } else if($splitDataValues[1] &&  !empty($splitDataValues[1])){
                    $name = $splitDataValues[1];
                } else if($splitDataValues[2] &&  !empty($splitDataValues[2])){
                    $name = $splitDataValues[2];
                } else{
                    $name = '';
                }
                $tmp= [];
                $tmp['root']=1;
                $tmp['lft']=$i+1;
                $tmp['lvl']=!empty($splitDataValues[0]) ? 1 : (!empty($splitDataValues[1]) ? 2 : 3);
                $tmp['name']=$name;
                $tmp['code']=!empty($splitDataValues[3]) ? $splitDataValues[3] : (rand(1111,9999));
                $res[]=$tmp;
            }
            $i++;
        }
        pr($res);
        die();
    }
    public function actionData(){
        echo $filepath = Yii::$app->basePath . "\\resource_list1.csv";
        $csvAsArray = array_map('str_getcsv', file($filepath));
        $arrayKey=$csvAsArray[0];

        foreach ($csvAsArray as $keyval=>$csvData){
            if($keyval!=0){
                $csvObj = new CsvUpload();
                $fieldNam=CsvUpload::getTableSchema()->getColumnNames();
                $data = array_combine($arrayKey, $csvData);

                if($data['parent_code']==''){
                    $parentId=0;

                }
                else{
                    $code=$data['parent_code'];
                    $getId=  CsvUpload::find()->where(['code'=>$code])->one();
                    $parentId = $getId->id;

                }



                foreach ($data as $k=>$d){
                    $k=strtolower($k);
                    $csvObj->$k=$d;
                }
                $csvObj->parentid=$parentId;
                //pr1($csvObj);
                try{
                    if($csvObj->save()){
                        pr($csvObj->getErrors());

                    }else{
                        echo "here else";
                        die();
                    }
                }catch (\Exception $e){
                    pr($e);
                }

            }
        }


    }

    public function actionTree(){

        $csvData = CsvUpload::find()->where(['parentid'=>0])->asArray()->all();

        $filedName=Tree1::getTableSchema()->getColumnNames();
        //pr($filedName);
        $m=2;



        foreach ($csvData as $d){
            $treeObj =  new Tree1();
            $treeObj->code = $d['code'];
            $treeObj->name = $d['description'];
            $treeObj->root = 1;
            $treeObj->lft = $m;
            $treeObj->lvl = 1;
            try {
                $treeObj->save();
                $lvelid=$treeObj->id;
                $treeObj->save();

            }catch (\Exception $e){
                pr($e);
            }

           echo  $id = $d['id'];



            $level1Data = CsvUpload::find()->where(['parentid'=>$id])->asArray()->all();


            foreach ($level1Data as $d1){

                $treeObj1 =  new Tree1();
                $treeObj1->code = $d1['code'];
                $treeObj1->name = $d1['description'];
                $treeObj1->root = 1;
                $m = $m+1;
                $treeObj1->lft = $m;
                $m = $m+1;
                $treeObj1->rgt = $m;
                $treeObj1->lvl = 2;
                $treeObj1->save();
                $level1id=$d1['id'];
                $level2Data = CsvUpload::find()->where(['parentid'=>$level1id])->asArray()->all();

                foreach ($level2Data as $d2){
                    $treeObj2 =  new Tree1();
                    $treeObj2->code = $d2['code'];
                    $treeObj2->name = $d2['description'];
                    $treeObj2->root = 1;
                    $m = $m+1;
                    $treeObj2->lft = $m;
                    $m = $m+1;
                    $treeObj2->rgt = $m;
                    $treeObj2->lvl = 3;
                    $treeObj2->save();
                }
                $m = $m+1;
                $treeObj1->rgt=$m;
                $treeObj1->save();



            }
            $m = $m+1;
            $treeObj->rgt=$m;
            $treeObj->save();
        }


    }

    public function actionTree1($parentId=0){
        if($parentId==0){
            $csvData = CsvUpload::find()->where(['parentid'=>$parentId])->asArray()->all();
            foreach ($csvData as $d){
                $this->treeData[$d['id']][]=$d;
                $this->childExist($d['id']);

            }

        }else{
            $csvData = CsvUpload::find()->where(['parentid'=>$parentId])->asArray()->all();
           // pr($csvData);
//            pr($csvData);
            foreach ($csvData as $d){
                $this->level=0;
                $this->childExist($d['id']);
                $this->levelCheck($d['id']);
                echo "\n";
                echo "Level--->".$this->level;
                //die();
            }





        }




    }

    public function childExist($parentId){

        $count = CsvUpload::find()->where(['parentid'=>$parentId])->count();
        if($count!=0){
            $this->actionTree1($parentId);
        }

    }

    public function actionTreesave()
    {

        $csvData = CsvUpload::find()->asArray()->all();

        foreach ($csvData as $data) {

            $name=$data['description'];
            $code=$data['code'];
            $uom=$data['uom'];
            $parentid=$data['parentid'];
            if($parentid==0){
                $parentid=1;
            }else{
                $paretCode = CsvUpload::find()->where(['id'=>$parentid])->one();
                $parentcodeValues=$paretCode->code;
                $parentData= Tree::find()->where(['code'=>$parentcodeValues])->asArray()->one();
                $parentid=$parentData['id'];
            }





            $data = ['nodeTitle' => 'node', 'nodeTitlePlural' => 'nodes', 'treeNodeModify' => 1, 'parentKey' => $parentid, 'modelClass' => 'app\models\Tree', 'Tree' => ['id' => '(new)', 'name' => $name, 'code' => $code, 'on_date' => '', 'icon_type' => 2, 'icon' => '', 'unit_type' => $uom, 'cost' => '', 'description' => $name]];
            $post = $data;
            $parentKey = ArrayHelper::getValue($data, 'parentKey', null);
            $treeNodeModify = ArrayHelper::getValue($data, 'treeNodeModify', null);
            $currUrl = ArrayHelper::getValue($data, 'currUrl', '');
            $treeClass = TreeSecurity::getModelClass($data);

            //$module = kartik\base\Config::getModule('treemanager');
            $keyAttr = 1;
            $nodeTitles = TreeSecurity::getNodeTitles($data);


            if ($treeNodeModify) {
                $node = new $treeClass;
                // pr($node);
                // die();
                $successMsg = Yii::t('kvtree', 'The {node} was successfully created.', $nodeTitles);
                $errorMsg = Yii::t('kvtree', 'Error while creating the {node}. Please try again later.', $nodeTitles);
            } else {
                $tag = explode("\\", $treeClass);
                $tag = array_pop($tag);
                $id = $post[$tag][$keyAttr];
                $node = $treeClass::findOne($id);
                $successMsg = Yii::t('kvtree', 'Saved the {node} details successfully.', $nodeTitles);
                $errorMsg = Yii::t('kvtree', 'Error while saving the {node}. Please try again later.', $nodeTitles);
            }
            $node->activeOrig = $node->active;
            $isNewRecord = $node->isNewRecord;
            $node->load($post);
            $node->description = $post['Tree']['description'];
            $node->on_date = $post['Tree']['on_date'];
            $node->unit_type = $post['Tree']['unit_type'];
            $node->cost = $post['Tree']['cost'];
            $node->code = $post['Tree']['code'];

            $errors = $success = false;

            if ($treeNodeModify) {

                if ($parentKey == TreeView::ROOT_KEY) {
                    echo 'sssss';
                    $node->makeRoot();
                } else {

                    $parent = $treeClass::findOne($parentKey);

                    if ($parent->isChildAllowed()) {
                        echo 'mmmmmm';
                        $node->appendTo($parent);

                    }
                }
            }
            if ($node->save()) {

                // check if active status was changed
                if (!$isNewRecord && $node->activeOrig != $node->active) {
                    if ($node->active) {
                        $success = $node->activateNode(false);
                        $errors = $node->nodeActivationErrors;
                    } else {
                        $success = $node->removeNode(true, false); // only deactivate the node(s)
                        $errors = $node->nodeRemovalErrors;
                    }
                } else {
                    $success = true;
                }
                if (!empty($errors)) {
                    $success = false;
                    $errorMsg = "<ul style='padding:0'>\n";
                    foreach ($errors as $err) {
                        $errorMsg .= "<li>" . Yii::t('kvtree', "{node} # {id} - '{name}': {error}",
                                $err + $nodeTitles) . "</li>\n";
                    }
                    $errorMsg .= "</ul>";
                }
            } else {
                echo $errorMsg = '<ul style="margin:0"><li>' . implode('</li><li>', $node->getFirstErrors()) . '</li></ul>';
            }

        }
    }


}

