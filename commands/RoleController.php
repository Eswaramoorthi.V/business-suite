<?php
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\RoleChild;
class RoleController extends Controller
{
    
      public $roles = [
        'Super Admin' => [
            'admin' =>  [
                'ConstructionManager' => true,
                'Estimator' => true,
                'Project Manager'=>true,
                'Purchase Officer'=>true,
                'Site Engineer'=>true,
                'Site Supervisor'=>true,
                'Quality Surveyor'=>true
                ]
        ]
    ];
      
    public function actionInit()
    {
        \Yii::$app->db->createCommand('set foreign_key_checks=0')->execute();
        \Yii::$app->db->createCommand("TRUNCATE TABLE  `auth_assignment`")->execute();
        \Yii::$app->db->createCommand('set foreign_key_checks=0')->execute();
        \Yii::$app->db->createCommand("TRUNCATE TABLE  `auth_item`")->execute();
        \Yii::$app->db->createCommand('set foreign_key_checks=0')->execute();
        \Yii::$app->db->createCommand("TRUNCATE TABLE `auth_item_child`")->execute();
        $auth = Yii::$app->authManager;
        $this->_role($auth, $this->roles);
        $t=time();
        \Yii::$app->db->createCommand("INSERT INTO `auth_assignment` (`item_name`, `user_id`,`created_at`) VALUES ('admin', 1,'$t')")->execute();
    }

    private function _role($auth, $roles, $parent=false)
    {   
        foreach ($roles as $role => $child) {
            $roleObject = $auth->createRole($role);
            $auth->add($roleObject);
            if(is_array($child)) {
                $this->_role($auth, $child, $roleObject);
            }
            if($parent) {
                $auth->addChild($parent, $roleObject);
            }
        }
    }
    
    public function actionDefaultRoles() {
        $rolelist = $this->roles['Super Admin']['admin'];
           foreach($rolelist as $parent =>$val){
            $defaultPermissions = [];
            foreach ($defaultPermissions as $controller => $authlist) {
                foreach ($authlist as $data) {
                    $model = new RoleChild();
                    $child = 'auth/' . $controller . '/' . $data;
                    $model->child = $child;
                    $model->parent = $parent;
                    $checkrecordExist = RoleChild::find()->where(['parent' => $parent])->andWhere(['=', 'child', $child])->asArray()->one();
                    if (empty($checkrecordExist)) {
                        $model->save();
                    }
                }
            }
        }
    }
}
