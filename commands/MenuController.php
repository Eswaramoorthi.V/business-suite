<?php

namespace app\commands;
use yii\console\Controller;
use app\models\Menu;

class MenuController extends Controller {

    public function actionSyncmenu() {
        \Yii::$app->db->createCommand()->truncateTable('menu')->execute();
        \Yii::$app->db->createCommand("
        INSERT INTO `menu` (`id`, `name`, `url`, `controller`, `action`, `icon`, `position`, `status`, `level`, `parent_id`, `role_list`, `role_list_disp`, `menu_disp`, `type`, `company_id`, `created_by`, `created_at`, `updated_by`, `updated_at`, `system_datetime`) VALUES
        (1, 'Admin', '#', 'location,area,block,equipment-group,category-master,asset-contacts,alarm-config,admin,role,permission,userdepartment,usergroup,menu,workorder-evaluation,asset-master', 'index', 'fa fa-wrench', 670, 1, 1, NULL, 'index,create,update,delete', 1, 1, 'others', NULL, NULL, '2017-12-16 00:30:00', '1', '2020-10-14 13:00:00', '0000-00-00 00:00:00'),
        (2, 'User Settings', '#', 'admin,role,permission,userdepartment,usergroup', '-', 'fa fa-user', 290, 1, 2, 1, 'index,create,update,delete', 1, 1, 'others', NULL, NULL, '2017-12-16 00:30:00', '1', '2020-09-13 07:30:00', '0000-00-00 00:00:00'),
        (4, 'Roles', '/rbac/role/index', 'rbac', 'role', 'fa fa-users', 310, 0, 3, 2, 'calendar,create,update,delete', 0, 1, 'others', NULL, NULL, '2017-12-16 00:30:00', '1', '2018-03-18 16:30:00', '0000-00-00 00:00:00'),
        (5, 'Role Management', '/permission/index', 'permission', 'index', 'fa fa-lock', 320, 1, 3, 2, 'index', 1, 1, 'others', NULL, NULL, '2017-12-16 00:30:00', '1', '2020-09-27 07:30:00', '0000-00-00 00:00:00'),
        (8, 'Activity BOM', '/estimate/estimate-master/master', 'estimate/estimate-master', 'master', 'fa fa-etsy', 50, 1, 2, 10, 'create,update,deletes,master', 1, 1, 'others', NULL, '1', '2020-07-04 02:30:00', '1', '2020-09-02 15:00:00', NULL),
        (10, 'Master', '#', 'material,labour,equipment,estimate-master', 'index', 'fa fa-cogs', 630, 1, 1, NULL, 'index,create,update,delete', 1, 1, 'others', NULL, NULL, '2017-12-16 00:30:00', '1', '2020-10-14 13:00:00', '0000-00-00 00:00:00'),
        (14, 'Settings', '/estimate/settings/index', 'estimate/settings', 'index', 'fa fa-cog', 550, 1, 2, 10, 'index', 1, 1, 'others', NULL, '1', '2020-07-16 00:30:00', '1', '2020-09-02 15:00:00', NULL),
        (15, 'Project', '/project/project/index', 'project/project', 'index', 'fa fa-product-hunt', 500, 1, 1, NULL, 'update,deletes,master,index,copy', 1, 1, 'others', NULL, '1', '2020-07-16 00:30:00', '1', '2020-09-13 07:30:00', NULL),
        (16, 'Resource Master', '/estimate/resource-master/index', 'estimate/resource-master', 'index', 'fa fa-sitemap', 540, 1, 2, 10, 'index', 1, 1, 'others', NULL, '1', '2020-07-21 11:30:00', '1', '2020-08-29 17:00:00', NULL),
        (18, 'User Management', '/employee/employee/index', 'employee/employee', 'index', 'fa fa-users', 570, 1, 3, 2, 'index,create,deletes,update,view', 1, 1, 'others', NULL, '1', '2020-08-30 22:30:00', '1', '2020-09-27 07:30:00', NULL),
        (19, 'Client', '/customer/customer/index', 'customer/customer', 'index', 'glyphicon glyphicon-user', 580, 1, 2, 1, 'index,create,deletes,update,view', 1, 1, 'others', NULL, '1', '2020-08-30 22:30:00', '1', '2020-09-27 07:30:00', NULL),
        (21, 'Suppliers', '/customer/customer/supplier-index', 'customer/customer', 'supplier-index', 'fa fa-handshake-o', 600, 1, 2, 1, 'supplier-index', 1, 1, 'others', NULL, '1', '2020-09-09 02:00:00', '1', '2020-09-27 07:30:00', NULL),
        (28, 'LPO', '/procurement/purchase-order/index', 'procurement/purchase-order', 'index', 'fa fa-product-hunt', 650, 1, 2, 29, 'index,create,update,deletes', 1, 1, 'others', NULL, '1', '2020-09-17 07:30:00', '1', '2020-10-14 13:00:00', NULL),
        (29, 'Procurement ', '#', 'purchase/purchase', 'index', 'fa fa-users', 510, 1, 1, NULL, 'index', 1, 1, 'others', NULL, '1', '2020-09-17 07:30:00', '1', '2020-10-14 13:00:00', NULL),
        (30, 'Company', '/company/company/index', 'company/company', 'index', 'fa fa-users', 640, 1, 2, 1, 'index,create,update,deletes', 1, 1, 'others', NULL, '1', '2020-09-19 07:30:00', '1', '2020-09-27 07:30:00', NULL),
        (31, 'MRV', '/procurement/purchase-request/index', 'procurement/purchase-request', 'index', 'fa fa-product-hunt', 620, 1, 2, 29, 'index,create,update,deletes', 1, 1, 'others', NULL, '1', '2020-09-23 07:30:00', '1', '2020-10-14 13:00:00', NULL),
        (32, 'Tender', '/tender/tender/index', 'tender/tender', 'index', 'fa fa-download', 100, 1, 1, NULL, 'index,create,deletes,update', 1, 1, 'others', NULL, '1', '2020-10-12 13:00:00', '1', '2020-10-16 13:00:00', NULL),
        (33, 'GRN', '/procurement/delivery-order/index', 'procurement/delivery-order', 'index', 'fa fa-users', 670, 1, 2, 29, 'index,create,update,deletes', 1, 1, 'others', NULL, '1', '2020-10-13 13:00:00', '1', '2020-10-13 13:00:00', NULL),
        (34, 'Adhoc Project', '#', 'adhocproject/adhoc-project', 'index', 'fa fa-building-o', 10, 1, 1, NULL, 'index,create,update,deletes', 1, 1, 'others', 1, '1', '2020-11-04 18:30:00', '1', '2020-11-04 18:30:00', NULL),
        (35, 'Enquiry', 'adhocproject/lead/index', '/adhocproject/lead', 'index', 'fa fa-etsy', 20, 1, 2, 34, 'create,index,deletes,update,detailview', 1, 1, 'others', 1, '1', '2020-11-04 18:30:00', '1', '2020-11-04 18:30:00', NULL),
        (36, 'Quoted', 'adhocproject/adhoc-project/index', 'adhocproject/adhoc-project', 'index', 'fa fa-cog', 30, 1, 2, 34, 'index,create,update,deletes,export-pdf,copy-quote', 1, 1, 'others', 1, '1', '2020-11-04 18:30:00', '1', '2020-11-04 18:30:00', NULL),
        (37, 'Awarded', '/adhocproject/adhoc-project/awarded-index', 'adhocproject/adhoc-project', 'awarded-index', 'fa fa-cogs', 40, 1, 2, 34, 'awarded-index', 1, 1, 'others', 1, '1', '2020-11-04 18:30:00', '1', '2020-11-04 18:30:00', NULL),
        (38, 'Accounts', '#', 'accounts,petty-cash', 'index', 'fa fa-list-alt', 50, 1, 1, NULL, 'index,create,update,delete', 1, 1, 'others', 1, '1', '2020-11-04 18:30:00', '1', '2020-11-04 18:30:00', NULL),
        (39, 'Transaction', '/accounts/accounts/transaction', '/accounts/accounts/', 'transaction', 'fa fa-exchange', 60, 1, 2, 38, 'transaction', 1, 1, 'others', 1, '1', '2020-11-04 18:30:00', '1', '2020-11-04 18:30:00', NULL),
        (40, 'Petty Cash', '/accounts/petty-cash/index', 'accounts/petty-cash', 'index', 'fa fa-money', 70, 1, 2, 38, 'create,index,update,balance-history,deletes', 1, 1, 'others', 1, '1', '2020-11-04 18:30:00', '1', '2020-11-04 18:30:00', NULL),
        (41, 'VAT', '/accounts/accounts/vat', 'accounts/accounts', 'vat', 'fa fa-credit-card', 80, 1, 2, 38, 'vat', 1, 1, 'others', 1, '1', '2020-11-04 18:30:00', '1', '2020-11-04 18:30:00', NULL),
        (42, 'Home', '/home/dashboard/index', 'home/dashboard', 'index', 'fa fa-home', 90, 1, 1, NULL, 'index', 1, 1, 'others', 1, '1', '2020-11-04 18:30:00', '1', '2020-11-05 18:30:00', NULL),
        (43, 'Reports', '#', 'accounts/report', 'project', 'fa fa-file', 100, 1, 1, NULL, 'project', 1, 1, 'others', 1, '1', '2020-11-05 18:30:00', '1', '2020-11-05 18:30:00', NULL),
        (44, 'Project Transaction', '/accounts/report/project', 'accounts/report', 'project', 'fa fa-file', 110, 1, 2, 43, 'project', 1, 1, 'others', 1, '1', '2020-11-05 18:30:00', '1', '2020-11-05 18:30:00', NULL);
")->execute();
    }

}
