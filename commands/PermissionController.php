<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class PermissionController extends Controller
{
   
    // public $route;

    public $permissions = [
        'admin' => [
            'index'=>['index'],
            'user'=>['create','index','deletes','view','update','datatables','admin'],
            'permission'=>['index','rolelist','create','default-roles','roleupdate'],
            'menu'=>['create','index','deletes','view','update'],
            'config-setting'=>['create','index','deletes','view','update'],
            'estimate-master'=>['create','index','deletes','view','update','get-sub-activity','master','get-description', 'get-items', 'get-item-cost','get-estimate-master-data'],
            'material'=>['create','index','deletes','view','update'],
            'labour'=>['create','index','deletes','view','update'],
            'equipment-master'=>['create','index','deletes','view','update'],
            'equipment'=>['create','index','deletes','view','update'],
            'settings'=>['create','index','deletes','view','update','get-code-check'],
            'resource-master'=>['create','index','deletes','view','update','get-code-check','import-directory'],
            'customer'=>['create','index','deletes','view','update','subcontractor-index','supplier-index'],
            'role'=>['create','index','view','update','deletes'],
            'company'=>['create','index','view','update','deletes'],
            'settings'=>['create','index','view','update','deletes'],
            'users/users'=>['create','index','delete','view','update','datatables','admin'],
            'delivery-order'=>['create','index','deletes','view','update','get-supplier','get-material-list','suplier-list','delivery-order-number','po-list','lpo-status','currency','payment-terms','ponumber_view','delivery_date_view','get-received-quantity','get-ponumber-list','get-delivery-data','request','delivery','index1','updatedelivery','company','get-document-status','get-currency','get-payment-terms'],
            'employee'=>['create','index','deletes','view','update','detailview','changepassword','employee-id-check','download','get-username-check','get-email-check','assignproject','assigncompany' ],
            'project'=>['create','index','deletes','view','update','detailview','master','get-activity-list','get-total-quantity','get-description', 'get-estimate-data','get-material-data','create-estimate', 'export-excel', 'export-pdf','get-project-code-check','export-full-excel','copy','company' ],
            'tender'=>['create','index','deletes','view','update','detailview','master','get-activity-list','get-total-quantity','get-description', 'get-estimate-data','get-material-data','create-estimate', 'export-excel', 'export-pdf','get-tender-code-check','export-full-excel','convert-project' ],
            'purchase-request'=>['create','index','deletes','view','update','datatables','amount','get-material-list','export-pdf','get-total-quantity','get-purchase-quantity','get-project-code','get-project-type','detailview'],
            'purchase-order'=>['create1','create','index','deletes','view','update','datatables','amount','get-material-list','get-material-list-drop','export-pdf','get-total-quantity','get-purchase-quantity','get-reference-name','get-total-material-data','request','delivery','index1'],
            'lead'=>['create','index','deletes','update','detailview'],
            'dashboard' => ['index', 'datatables', 'search-index'],
            'report' =>['project'],
              'adhoc-project'=>['create','index','deletes','view','update','project-group','services','products','detailview','contractor','awarded-index','copy-quoted-project','copy-quote'],
              'petty-cash'=>['index','create','update','deletes','balance-history','daterange'],
            'pa-invoice'=>['create','view','painvoice','index','update','pdf','deletes','detailview','papayment','pa-paymentchallan','deletereceipt','updatereceipt'],
            'document'=>['index','create','update','deletes','download'],
            'quotation'=>['index','create','update','deletes','export-pdf','copy-quote'],
            'drawing'=>['index','create','update','deletes','download'],
            'loa'=>['index','create','update','deletes','download'],
            'site-photos'=>['index','create','update','deletes','download'],
            'variation'=>['update','deletes','download','create'],
              'accounts'=>['index','create','update','deletes','vat','daterange','transaction','Createtransaction','Updatetransaction','Deletetransaction','payment-modes','getproject',
            'getsubcontractor','getlpo','getlpoamount','getprojectlist','getemployee','checkbalance'],
             'petty-cash'=>['index','create','update','deletes','balance-history','daterange'],
        ]
    ];

    // public $route;
    
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $this->_permissions($auth, $this->permissions);
        parent::init();
    }

    private function _permissions($auth, $permissionList)
    {
        foreach ($permissionList as $roleName => $permissions) {
            foreach ($permissions as $requestName => $actions) {
                foreach ($actions as $action) {
                    $name = 'auth/'.$requestName.'/'.$action;
                    $description = $requestName;
                    $permission = $auth->getPermission($name);
                    if(is_null($permission)) {
                        $permission = $auth->createPermission($name);
                        $permission->description = $description;
                        $auth->add($permission);
                        $auth->addChild($auth->getRole($roleName), $permission);
                    }
                }
            }
        }
    }

}