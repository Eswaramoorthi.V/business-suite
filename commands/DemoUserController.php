<?php

namespace app\commands;

use yii\console\Controller;
use app\models\AuthAssignment;

class DemoUserController extends Controller {

    public function actionInit() {        
        $model = new AuthAssignment();
        $model->item_name = 'admin';
        $model->user_id = '1';
        $model->save();
    }

}
