<?php

return [
    'sub_folder_path' => '/pricingcalculator',
    'domain_url'=>'http://localhost/pricingcalculator/',
    'passwordExpireIn' => 15, //In Days
    'passwordExpired' => 30, //In Days
    'VATRate'=>5,
    'projectStatus'=>['0'=>'Pending','1'=>'Progress','2'=>'Completed'],
    'estimateType'=>['1' =>'Material','2' =>'Labour','3'=>'Profit','4'=>'OverHead'],
    'configSettingType'=>['2' =>'Others','1' =>'Activity',],
    'options'=>['1'=>'Option','2'=>'Excluded','3'=>'NA','4'=>'Rate Only','5'=>'Included','6'=>'By Other'],
];
