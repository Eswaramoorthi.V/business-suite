<?php
//error_reporting(E_ALL);
//ini_set('display_errors', 1);
$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'baseUrl' => $params['sub_folder_path'],
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ab8dr7VjKggUIvpOdVjgGI2oPq7zZJMV',
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
                'kvgrid' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ]]],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'dektrium\user\models\User',
            // 'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        '/' => 'project/project/',
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => $params['sub_folder_path'],
            'rules' => [
                 '/' => 'project/project/index',
                 'school' => 'school/index',
                 'scheduler' => 'schedule/scheduler',
                'schedule/jsoncalendar/<schoolId:\d+>' => 'schedule/jsoncalendar',
                'schedule/session/<schoolId:\d+>' => 'schedule/session'
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@app/views' => '@app/themes/lite',
                    '@dektrium/user/views' => '@app/themes/lite/vendor/user',
                    '@dektrium/rbac/widgets/views' => '@app/themes/lite/vendor/rbac/widgets',
                    '@dektrium/rbac/views' => '@app/themes/lite/vendor/rbac/views'

                ],
                'baseUrl' => '@web/../themes/lite',
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'sourcePath' => '@webroot/lib/js/plugins/jquery/',
                    'js'=>[
                        'jquery.min.js',
                        'jquery-ui.min.js'
                    ],
                    'css' => []
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => '@webroot/lib/js/plugins/bootstrap/',
                    'js'=>[
                        'bootstrap.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => '@webroot/lib/',
                    'css' => [
                        'css/theme-default.css',
                        'css/custom.css'
                    ]
                ],
                'nullref\datatable\DataTableAsset' => [
                    'styling' => false,
                ],
                'yii2fullcalendar\CoreAsset' => [
                    'sourcePath' => '@webroot/lib/',
                    'css' => [],
                    'js' => ['js/plugins/fullcalendar/fullcalendar.min.js'],
                    'depends' => [
                        'yii\web\JqueryAsset',
                        'yii2fullcalendar\MomentAsset'
                    ]
                ]
            ],
        ],
        'formatter' => [
            'dateFormat' => 'yyyy-MM-dd',
            'timeFormat' => 'php:H:i',
        ],
    ],
    'controllerMap' => [
            // declares "account" controller using a class name
            //'location' => 'app\controllers\SchoolController',
            // declares "article" controller using a configuration array
            'location' => [
                'class' => 'app\controllers\SchoolController',
                'viewPath' => '@app/views/school',
                'enableCsrfValidation' => false,
            ],

        ],
    
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableRegistration' => false,
            'enableConfirmation' => false,
            'enableUnconfirmedLogin' => true,
            'modelMap' => [
                'User' => 'app\models\UserInfo',
                'Profile' => 'app\models\Profile',
            ],
            'adminPermission' => 'auth/user/admin',
            'admins'=>['demo']
        ],
        'reports' => [

            'class' => 'app\modules\reports\Reports',

        ],        
        'changepass' => [ 'class' => 'app\modules\changepass\changepass'],
        'rbac' => 'dektrium\rbac\RbacWebModule',
        'noty' => [
            'class' => 'lo\modules\noty\Module',
        ],
        'debug' => [
            'class' => 'yii\debug\Module',
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module',
            // your other grid module settings
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module'

            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'estimate' => [ 'class' => 'app\modules\estimate\estimate'],
        'project' => [ 'class' => 'app\modules\project\project'],
        'tender' => [ 'class' => 'app\modules\tender\tender'],
        'users' => [ 'class' => 'app\modules\users\users'],
        'employee' => [ 'class' => 'app\modules\employee\employee'],
        'customer' => [ 'class' => 'app\modules\customer\customer'],
        'role' => [ 'class' => 'app\modules\role\role'],
        'purchase' => [ 'class' => 'app\modules\purchase\purchase'],
        'company' => [ 'class' => 'app\modules\company\company'],
        'request' => [ 'class' => 'app\modules\request\request'],
        'procurement' => [ 'class' => 'app\modules\procurement\procurement'],
        'settings' => [ 'class' => 'app\modules\settings\settings'],
          'adhocproject' => [ 'class' => 'app\modules\adhocproject\adhocproject'],
            'accounts' => [ 'class' => 'app\modules\accounts\accounts'],
            'home' => [ 'class' => 'app\modules\home\home'],
        'treemanager' => [
            'class' => '\kartik\tree\Module',
        ],
    ],

    'params' => $params,
    // 'defaultRoute' => 'dashboard',
    'on beforeRequest' => array('\app\components\LogEventComponent', 'init'),
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    // $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
        'generators' => [
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'myCrud' => '@app/mms/crud/default',
                ]
            ]
        ],
    ];
}

return $config;
