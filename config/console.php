<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'baseUrl' => $params['sub_folder_path'],
            'rules' => [
                '/' => 'project/project/index',
                'school' => 'school/index',
                'scheduler' => 'schedule/scheduler',
                'schedule/jsoncalendar/<schoolId:\d+>' => 'schedule/jsoncalendar',
                'schedule/session/<schoolId:\d+>' => 'schedule/session'
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
         'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            //'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => gethostbyname('smtp.gmail.com'),
                'username' => 'testmms81@gmail.com',
                'password' => 'testmms@1',
                'port' => '465',
                'encryption' => 'ssl',
                'streamOptions' => [ 
                    'ssl' => [ 
                        'allow_self_signed' => true,
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                    ],
                ]
                ],
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'rbac' => 'dektrium\rbac\RbacConsoleModule',
    ],
    'modules' => [

        'treemanager' => [
            'class' => '\kartik\tree\Module',
        ],
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}
function sql($q,$off=0) {
    echo "\n";
    echo $q->createCommand()->rawsql;
    echo "\n";
    if($off==1){
        die();
    }
}
function pr1($d){
    echo "<pre>",print_r($d),"</pre>";
    die();
}
return $config;
