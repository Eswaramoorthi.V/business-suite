<?php

return [
    // 'sub_folder_path' => '/businesssuites',
    // 'domain_url'=>'https://dev.jeyamsys.com/',
    'sub_folder_path' => '/eswar/my-git/business-suite',

    'domain_url'=>'https://localhost/eswar/my-git/business-suite',

    'passwordExpireIn' => 15, //In Days
    'passwordExpired' => 30, //In Days
    'options'=>['1'=>'Chiller A/c','2'=>'Normal Ac','3'=>'NA','4'=>'Rate Only','5'=>'Included','6'=>'Byother'],

    'projectStatus'=>['0'=>'Pending','1'=>'Progress','2'=>'Completed'],
    'estimateType'=>['1' =>'Material','2' =>'Labour','3'=>'Profit','4'=>'OverHead','5'=>'Equipment'],
    'configSettingType'=>['2' =>'Others','1' =>'Category',],
    'VATRate'=>5
];
