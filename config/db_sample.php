<?php
$serverName = "localhost";
$database = "businesssuite";
return [
    'class' => 'yii\db\Connection',
    'dsn' => "mysql:host=$serverName;dbname=$database",
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];