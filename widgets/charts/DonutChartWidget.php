<?php
namespace app\widgets\charts;
use yii\base\Widget;
class DonutChartWidget extends Widget
{
         public $title;
         public $icon;
         public $chart_class;


         public function init(){
		parent::init();
		
	}
	
	public function run(){
            
             return $this->render('donutChart',[
                        'title'=>$this->title,
                        'icon'=>$this->icon,
                        'chart_class'=>$this->chart_class,
                     ]);
	}
  
}
