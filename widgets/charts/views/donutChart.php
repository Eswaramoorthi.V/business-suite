<?php
//use kartik\icons\Icon;
//Icon::map($this);
?>
<div class="col-md-3 text-center">
     <h5><?php //echo Icon::show('empire');?><?php echo $title; ?></h5>
    <div id="<?php echo $chartClass;?>" style="height: 300px;"><svg></svg></div>
</div>


<?php


//pr($chartData);
$this->registerJsFile('@web/lib/js/plugins/rickshaw/d3.v3.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/lib/js/plugins/nvd3/nv.d3.min.js',['depends' => 'yii\web\JqueryAsset']);
$this->registerJsFile('@web/lib/js/charts_nvd3.js',['depends' => 'yii\web\JqueryAsset']);
?>
 <?= $this->registerJs("nvd3Charts.init('$chartClass','$chartData','$chartTotal','$chartColor')"); ?>
    
 