<div class="col-md-3 text-center">
    <h5><i class="" aria-hidden="true"></i> <?php echo $title; ?></h5>
    <input class="<?php echo $chartClass;?>" data-width="170" data-spacewidth="150" data-skin='tron' data-bgColor="<?php echo $bgcolor;?>" data-fgColor="<?php echo $fgcolor; ?>" data-displayInput=true value="<?php echo $chartValue; ?>" data-max="<?php echo $chartTotal;?>"  readOnly="true" lineCap='round=rounded' data-entryid="<?=$chartTotal;?>" data-detailedview-url="<?=$url;?>" >
</div>

<?php

//echo $chartClass;
//echo $chartValue ."---".$chartTotal;
//echo "<br>";
$this->registerJsFile('@web/rms/js/charts_knob.js',['depends' => 'yii\web\JqueryAsset']);
?>