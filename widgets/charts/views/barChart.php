<?php

use app\models\WorkOrder;

$workOrderData=WorkOrder::prepareWorkOrderData();

?>
<div class="col-md-" height="670px">

            <!-- START USERS ACTIVITY BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3><strong><?php echo $title;?></strong></h3>
                       <!--  <span>Communication failure VS PV System failure</span> -->
                    </div>
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li><a href="#" class="panel-fullscreen" data-toggle="tooltip" title="Fullscreen"><span class="fa fa-expand"></span></a></li>
                        <li><a href="#" class="panel-refresh" data-toggle="tooltip" title="Refresh"><span class="fa fa-refresh"></span></a></li>


                    </ul>
                </div>
                <div class="panel-body padding-0">
                    <?php echo \yii2mod\c3\chart\Chart::widget([
                        'options' => [
                            'id' => 'popularity_chart'
                        ],
                        'clientOptions' => [
                            'data' => [
                                'x' => 'x',
                                'columns' => $workOrderData,
                                'type' => 'bar',
                                'types'=>['Work Order Completion'=>'line'],
                                //'groups'=>[['Work Order Request','Work Order Evalution','Work Order Procesing']],
                                'colors' => [
                                    'Total Count' => '#4EB269',
//                                    'Work Order Creation'=>'#a9a9a9',
//                                    'Work Order Evalution'=>'',
//                                    'Work Order Process'=>'',
                                    'Work Order Completion'=>'#2ca02c',
                                    'Work Order Request'=>'#ff7f0e',
                                    'Work Order Evalution'=>'#1f77b4',
                                    'Work Order Procesing'=>'#17becf',

                                ],
                            ],
                            'axis' => [
                                'x' => [
                                    //'label' => 'Month',
                                    'type' => 'timeseries',
                                    'tick'=>['format'=>'%d-%m-%Y'],
                                    'ticks'=>20,


                                ],
                                'y' => [
                                    'label' => [
                                        'text' => 'Total Count',
                                        'position' => 'outer-top'
                                    ],
                                    'min' => 0,
                                    'max' => 30,
                                    'padding' => ['top' => 10, 'bottom' => 0],
                                    'ticks'=>29,
                                ]
                            ]
                        ]
                    ]); ?>
                </div>
            </div>
            <!-- END USERS ACTIVITY BLOCK -->

        </div>
<style>
    .c3-line {
        stroke-width: 3px;
    }

    .c3-circle {
        fill: #ff4500 !important;
    }
    .c3-bar {
        stroke-width: 8px;
    }
    
</style>