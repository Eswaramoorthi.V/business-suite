<?php

namespace app\widgets\charts;
use app\components\DashboardSettings;
use app\models\AssetMaster;

use yii\base\Widget;


class ChartWidget extends Widget {

    public $title;
    public $icon;
    public $chartClass;
    public $chartType;
    public $chartName;
    public $chartTotal;
    public $chartColor;
    public $url;

    public function init() {
        parent::init();
    }

    public function run() {

        switch ($this->chartType) {
            case DashboardSettings::donutCHART: return $this->donutChart();
                break;
            case DashboardSettings::KNOBCHART: return $this->knobChart();
                break;
             case DashboardSettings::BARCHART: return $this->barChart();
                break;
            default:
                break;
                
        }
    }

    private function donutChart() {
        $dashBoarSettings=new DashboardSettings();
        $domainClass=$dashBoarSettings->getChartDataSettings($this->chartName);
        $chartData=$domainClass->getStatus();    
        $chartTotal=0;
        if(!empty($chartData)){
            $chartTotal = array_sum(array_column($chartData, 'value'));
            
        }
        return $this->render('donutChart', [
                    'title' => $this->title,
                    'icon' => $this->icon,
                    'chartClass' => $this->chartClass,
                    'chartData'=> json_encode($chartData),
                    'chartTotal'=>$chartTotal,
                    'chartColor'=>json_encode($this->chartColor),
        ]);
    }
     private function knobChart() {
         $dashBoarSettings=new DashboardSettings();
         $domainClass=$dashBoarSettings->getChartDataSettings($this->chartName);
         $colorSettings=$dashBoarSettings::$knobChartGroupColorSettings;
         $chartValue=$domainClass->getStatus();
         $totalCount=$domainClass->getTotalValue();
         $detailedViewURL=$domainClass->getdetailedViewURL();
         $statusColorSettings=isset($colorSettings[$this->chartName])?$colorSettings[$this->chartName]:"";
         if($statusColorSettings==$dashBoarSettings::LOWTOHIGH){
             $statusColor = ($chartValue < 200 || $chartValue==0) ? '#00B000' : '#00B000';
         }else{
             $statusColor = ($chartValue > 350) ? '#00B000' : '#FF0000';
         }
        return $this->render('knobChart', [
                    'title' => $this->title,
                    'icon' => $this->icon,
                    'bgcolor'=>$this->chartColor[0],
                    'fgcolor'=>$this->chartColor[1],
                    'chartClass' => $this->chartClass,
                    'chartValue'=>$chartValue,
                    'chartTotal'=>$totalCount,
                    'url'=>$detailedViewURL,
        ]);
    }
    
     private function barChart() {
        return $this->render('barChart', [
                    'title' => $this->title,
                    'icon' => $this->icon,
                    'chartClass' => $this->chartClass,
        ]);
    }
    
     public $depends = [
        'yii\web\YiiAsset',
    ];

}
