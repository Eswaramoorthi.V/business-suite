<?php

namespace app\widgets\yii2fullcalendar;

use yii\web\View;
use yii\helpers\Html;
use yii2fullcalendar\yii2fullcalendar as base;
use yii2fullcalendar\CoreAsset;
use yii2fullcalendar\ThemeAsset;

class yii2fullcalendar extends base
{
    /**
     * internal marker for the name of the plugin
     * @var string
     */
    private $pluginName = 'fullCalendar';

    public $dragable;

    public $header = [
        'center'=>'title',
        'left'=>'prev,next today',
        'right'=>'month,agendaWeek,agendaDay'
    ];

    /**
     * Renders the widget.
     */
         /**
          * internal marker for the name of the plugin
          * @var string
          */
         private $_pluginName = 'fullCalendar';

    public function run()
    {
        $this->options['data-plugin-name'] = $this->pluginName;

        if (!isset($this->options['class'])) {
            $this->options['class'] = 'fullcalendar';
        }

        /*echo '<div class="rows">
            <div class="pull-right">
                <select id="schedulemaster-filter" class="select form-control" name="" aria-required="true">
                    <option value="all">All</option>
                    <option value="off">Holidays</option>
                    <option value="on">On Events</option>
                    <option value="group">By Groups</option>
                    <option value="school">By Schools</option>
                </select>
            </div>
            <div class="pull-right">
                <select id="schedulemaster-fevent" class="select form-control" name="" aria-required="true">
                    <option value="all">All</option>
                    <option value="drafted">Drafted</option>
                    <option value="committed">Committed</option>
                </select>
            </div>
        </div>
        <div class="clearfix"></div><br/>';*/
        echo  '<div class="clearfix"></div><br/>';
        echo Html::beginTag('div', $this->options) . "\n";
            echo Html::beginTag('div',['class'=>'fc-loading','style' => 'display:none;']);
                echo Html::encode($this->loading);
            echo Html::endTag('div')."\n";
        echo Html::endTag('div')."\n";
        $this->registerPlugin();
    }


    /**
    * Registers the FullCalendar javascript assets and builds the requiered js  for the widget and the related events
    */
    protected function registerPlugin()
    {
        $id = $this->options['id'];
        $view = $this->getView();

        /** @var \yii\web\AssetBundle $assetClass */
        $assets = CoreAsset::register($view);

        //by default we load the jui theme, but if you like you can set the theme to false and nothing gets loaded....
        if($this->theme == true)
        {
            ThemeAsset::register($view);
        }

        $js = array();

        if($this->ajaxEvents != NULL){
            $this->clientOptions['events'] = $this->ajaxEvents;
        }

        if(!isset($this->options['dragable'])){
            $this->options['dragable'] = true;
        }

    	$js[] = "var loading_container = jQuery('#$id .fc-loading');"; // take backup of loading container
    	$js[] = "jQuery('#$id').empty().append(loading_container);"; // remove/empty the calendar container and append loading container bakup

        $js[] = "var curSource = ''";
        $js[] = "curSource = '$this->ajaxEvents';";

        $js[] = "var fullCalendar = function(){
            var calendar = function(){
                if($('#calendar').length > 0){

                    var date = new Date();
                    var d = date.getDate();
                    var m = date.getMonth();
                    var y = date.getFullYear();

                    var calendar = $('#calendar').fullCalendar({
                        header: {
                            left: 'prev,next today',
                            center: 'title',
                            right: 'month,agendaWeek,agendaDay'
                        },
                        editable: true,
                        events: curSource,
                        droppable: ".$this->options['dragable'].",
                        selectable: true,
                        selectHelper: true,
                        eventRender: function(event, element) {
                            $(element).find('.fc-time').remove();
                            if(event.editable) {
                                element.addClass('fc-all-editable');
                                if(event.className[0] == 'schedule') {
                                    element.click(function() {
                                        $('#modal_schedule .modal-content #ajax-model').remove();
                                        $('#modal_schedule').removeData('bs.modal');
                                        var update = $('#modal_schedule').data('update') + '?id='+event.id;
                                        $('#modal_schedule').modal({remote: update });
                                    });
                                } else if(event.className[0] == 'session') {
                                    element.click(function() {
                                        if(confirm('Are you sure you want off this session?')) {
                                            var sessionOff = $('#modal_schedule').data('session');
                                            var sessionUrl = sessionOff + '?id='+event.id;
                                            console.log({ start_date: event.start, end_date: event.end});
                                            $.ajax({
                                                type: 'POST',
                                                url: sessionUrl,
                                                data: { 'data': JSON.stringify({title: event.title, start_date: event.start, end_date: event.end}) },
                                                success: function (data) {
                                                    $('#modal_schedule .modal-header button').trigger('click');
                                                    $('#calendar').fullCalendar('refetchEvents');
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        },
                        select: function(start, end, allDay) {
                            var check = moment(start, 'DD.MM.YYYY').format('YYYY-MM-DD');
                            var today = moment().format('YYYY-MM-DD');
                            if(check >= today) {
                                $('#modal_schedule').removeData('bs.modal');
                                var create = $('#modal_schedule').data('create');
                                $('#modal_schedule').modal({remote: create });
                                $('#modal_schedule').data('from', 'select');
                                $('#modal_schedule').data('start', start);
                            } else {
                                $.growl.error({\"title\": \"\", \"message\":\"Can't add schedule for past days!\"});
                            }
                        },
                        drop: function(date, allDay) {
                            var check = moment(date, 'DD.MM.YYYY').format('YYYY-MM-DD');
                            var today = moment().format('YYYY-MM-DD');
                            if(check >= today) {
                                // $('#modal_schedule .modal-content #ajax-model').remove();
                                var originalEventObject = $(this).data('eventObject');
                                var copiedEventObject = $.extend({}, originalEventObject);
                                copiedEventObject.start = date;
                                copiedEventObject.allDay = allDay;

                                $('#modal_schedule').removeData('bs.modal');
                                var create = $('#modal_schedule').data('create');
                                $('#modal_schedule').modal({remote: create+'?holiday=1' });
                                $('#modal_schedule').data('from', 'drop');
                                $('#modal_schedule').data('object', copiedEventObject);
                            } else {
                                $.growl.error({\"title\": \"\", \"message\":\"Can't add schedule for past days!\"});
                            }
                        }
                    });
                }
            }

            return {
                init: function(){
                    calendar();
                }
            }
        }();";

        $js[] = "fullCalendar.init(); $('calendar').fullCalendar();";

        $js[] = 'jQuery("#schedulemaster-fevent").change(function() {
            var event = $(this).val();
            var filter = jQuery("#schedulemaster-filter").val();
            var newSource = "'. $this->ajaxEvents .'?event="+event+"&filter="+filter;
            jQuery("#'.$id.'").fullCalendar("removeEventSource", curSource);
            jQuery("#'.$id.'").fullCalendar("addEventSource", newSource);

            curSource = newSource;
        })';

        $js[] = 'jQuery("#schedulemaster-filter").change(function() {
            var event =jQuery("#schedulemaster-fevent").val();
            var filter = $(this).val();
            var newSource = "'. $this->ajaxEvents .'?event="+event+"&filter="+filter;
            jQuery("#'.$id.'").fullCalendar("removeEventSource", curSource);
            jQuery("#'.$id.'").fullCalendar("addEventSource", newSource);

            curSource = newSource;
        })';

        $view->registerJs(implode("\n", $js),View::POS_READY);
    }
}
