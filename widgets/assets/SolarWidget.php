<?php

namespace app\widgets\assets;
use yii\base\Widget;
use app\models\AssetMaster\Solar;
use app\models\CategoryMaster;

class SolarWidget extends Widget
{
    public $signageId;
    public $title = 'Solar Information';

    public function init() {
        parent::init();
    }

    public function run()
    {
        $model = Solar::find()->joinWith('categoryMaster')->where([
            '[category_master].code' => CategoryMaster::CATEGORY_MASTER_ASSET_SOLAR_CODE
        ])->andWhere(['[asset_master].parent_id' => $this->signageId])->one();

        if(is_null($model)) {
            $model = new Solar();
        }
        return $this->render('solar', [
            'title' => $this->title,
            'model' => $model
        ]);
    }
}
