<?php

namespace app\widgets\assets;

use yii\base\Widget;
use app\models\NetworkConfigurationMaster\SchoolView;

class NetworkWidget extends Widget
{
    public $signageId;
    public $title = 'Network Information';

    public function init() {
        parent::init();
    }

    public function run()
    {
        $model = SchoolView::find()->joinWith(['asset'])->where(['asset_master_id' => $this->signageId])->one();
        if(is_null($model)) {
            $model = new SchoolView();
        }
        return $this->render('network', [
            'title' => $this->title,
            'model' => $model
        ]);
    }
}
