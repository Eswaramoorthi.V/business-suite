<?php

namespace app\widgets\assets;
use yii\base\Widget;

class SignageWidget extends Widget
{
    public $signage;
    public $title = '';


    public function init() {
        parent::init();
    }

    public function run()
    {
        return $this->render('signage', [
            'title' => $this->title,
            'model' => $this->signage
        ]);
    }
}
