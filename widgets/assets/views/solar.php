<?php
use yii\widgets\DetailView;

?>
<div class="header">
    <i class="fa fa-sun-o"></i>
    <span class="tab-subheading"><?= $title; ?></span>
</div>
<div class="ui divider"></div>
<?= DetailView::widget([
    'model' => $model,
    'formatter' => [
        'class' => 'yii\i18n\Formatter',
        'nullDisplay' => '-',
    ],
    'attributes' => [
        'PartNumber',
        'SerialNumber',
        'InstalledDate',
        'currentChargingStatus',
        'Remarks'
    ],
    'options' => [
        'class' => 'ui padded striped table'
    ]
]) ?>
