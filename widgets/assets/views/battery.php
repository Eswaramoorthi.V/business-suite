<?php
use yii\widgets\DetailView;

?>
<div class="header">
     <i class="fa fa-signal"></i>
    <span class="tab-subheading"><?= $title; ?></span>
</div>
<div class="ui divider"></div>
<?= DetailView::widget([
    'model' => $model,
     'formatter' => [
        'class' => 'yii\i18n\Formatter',
        'nullDisplay' => '-',
    ],
    'attributes' => [
        'part_no',
        'serial_number',
        'installation_date',
        'currentBatteryLevel',
        'Remarks'
    ],
    'options' => [
        'class' => 'ui padded striped table'
    ]
]) ?>
