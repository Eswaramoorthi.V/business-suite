<?php
use yii\widgets\DetailView;

?>
<div class="header">
    <i class="fa fa-sitemap"></i>
    <span class="tab-subheading"><?= $title; ?></span>
</div>
<div class="ui divider"></div>
<?= DetailView::widget([
    'model' => $model,
     'formatter' => [
        'class' => 'yii\i18n\Formatter',
        'nullDisplay' => '-',
    ],
    'attributes' => [
        'HostName',
        'IpAddress',        
        'Subnet',
        'ServerIP',
        'ServerPort',
        'ServerSubnet',
        'ImieID',
        'SimCardNumber',
        'currentConnectionStatus',
        'CurrentBandwidthUtilisation',
        'lastCommunicationTime',
        'currentServerTime',
        'Remarks'
    ],
    'options' => [
        'class' => 'ui padded striped table'
    ]
]) ?>
