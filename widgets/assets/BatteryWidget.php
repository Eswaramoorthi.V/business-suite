<?php

namespace app\widgets\assets;

use yii\base\Widget;
use app\models\AssetMaster\Battery;
use app\models\CategoryMaster;

class BatteryWidget extends Widget
{
    public $signageId;
    public $title = 'Battery Information';

    public function init() {
        parent::init();
    }

    public function run()
    {
        $model = Battery::find()->joinWith('categoryMaster')->where([
            '[category_master].code' => CategoryMaster::CATEGORY_MASTER_ASSET_BATTERY_CODE
        ])->andWhere(['[asset_master].parent_id' => $this->signageId])->one();
        // echo $model->createCommand()->sql; exit;
        if(is_null($model)) {
            $model = new Battery();
        }
        return $this->render('battery', [
            'title' => $this->title,
            'model' => $model
        ]);
    }
}
