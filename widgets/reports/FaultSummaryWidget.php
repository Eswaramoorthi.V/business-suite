<?php

namespace app\widgets\reports;

use yii\base\Widget;

class FaultSummaryWidget extends Widget {
  
    public $title;

    public function init() {
        parent::init();
    }

    public function run() {
             return $this->render('faultSummary', [
                   'title'=>$this->title,
                 ]);
     
    }

    

}
