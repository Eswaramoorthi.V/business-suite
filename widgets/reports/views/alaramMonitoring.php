
<div class="schedule-holidays-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
        //pr($dataProvider);
    ?>

    <?= \nullref\datatable\DataTable::widget([
        'data' => $dataProvider->getModels(),
        'paging' => true,
        'pageLength'=>5,        
        'columns' => [
            'S/No',
            'Sign_ID',
            'Equipment_Type',
            'School',
            'Address',
            'alarm_name',
            'alarm_date',
            
        ],
        'tableOptions' => [
            'class' => 'table dataTable table-bordered'
        ],
        'assetManager' => [
            'bundles' => [
                'nullref\datatable\DataTableAsset' => [
                    'styling' => \nullref\datatable\DataTableAsset::STYLING_BOOTSTRAP,
                ]
            ],
        ],
    ]) ?>
</div>
