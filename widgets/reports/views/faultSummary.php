<div class="col-md-6">

            <!-- START PROJECTS BLOCK -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title-box">
                        <h3><?php echo $title;?></h3>

                    </div>                                    
                    <ul class="panel-controls" style="margin-top: 2px;">
                        <li>
                            <div id="reportrange" class="dtrange">                                            
                                <span></span><b class="caret"></b>
                            </div>                                     
                        </li> 



                        <li><a href="#" class="panel-fullscreen" data-toggle="tooltip" title="Fullscreen"><span class="fa fa-expand"></span></a></li>


                    </ul>
                </div>
                <div class="panel-body panel-body-table">

                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="40%">Type</th>
                                    <th width="20%">Activity</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a href="link">Door Opened</a> (60)</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 65%;">85%</div>

                                        </div>
                                    </td>

                                </tr>

                                <tr>

                                    <td><a href="link">Door Not Closed</a> (40)</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 72%;">72%</div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td><a href="link">Battery voltage threshold fall-below</a> (40)</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 95%;">85%</div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td><a href="link">Battery not charging / Charger failed</a> (20)</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td><a href="link">Charger Alarm</a> (20)</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                        </div>
                                    </td>

                                </tr>

                                <tr>

                                    <td><a href="link">Controller Alarm</a> (80)</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                        </div>
                                    </td>

                                </tr>     

                                <tr>


                                    <td><a href="link">Monitor and alert for SMS utilization per location</a> (60)</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                        </div>
                                    </td>

                                </tr>   

                                <tr>


                                    <td><a href="link">LED Failure Alarm - OA -2</a> (60)</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>


                                    <td><a href="link">LED Failure Alarm - OA-1</a> (60)</td>
                                    <td>
                                        <div class="progress progress-small progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">100%</div>
                                        </div>
                                    </td>

                                </tr>                                            

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <!-- END PROJECTS BLOCK -->

        </div>