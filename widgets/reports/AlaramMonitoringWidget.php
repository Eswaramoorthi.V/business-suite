<?php

namespace app\widgets\reports;

use yii\base\Widget;

class AlaramMonitoringWidget extends Widget {
  
    public $title;
    public $alarmSummary;

    public function init() {
        parent::init();
    }

    public function run() {
             return $this->render('alaramMonitoring', [
                   'title'=>$this->title,
                    'dataProvider'=>$this->alarmSummary
                 ]);
     
    }

    

}
