<?php

namespace app\widgets\menu;
use yii\base\Widget;



use app\models\Menu;


class MenuWidget extends Widget {

    public $title;
    public $icon;
    public $chartClass;
    public $chartType;
    public $chartName;
    public $chartTotal;
    public $chartColor;
    public $url;

    public function init() {
        parent::init();
    }

    public function run() {        
        $model = Menu::find()->where(['status' => 1])->asArray()->all();
        return $this->render('index', [
            'title' => $this->title,
            'result' => $model
        ]);
        
//       return $this->render('index', [
//                    'title' => $this->title,
//                    'icon' => $this->icon,
//                    //'chartClass' => $this->chartClass,
//        ]);
    }
    
    

}
