<?php
use yii\helpers\Url;
$actionId = Yii::$app->controller->action->id;
$controllerId = Yii::$app->controller->action->controller->id;
//pr1($actionId);
//pr($controllerId);
?>
<!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                        <a href="<?= Url::toRoute(['/']); ?>">40WLFS</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-title"></li>
                   <?php foreach($result as $key=>$menuval) { 
                       if($menuval['parent_id']==0 && $menuval['url']!='#') {
                       ?>
                     <li class="<?= ($controllerId==$menuval['controller'] && $actionId==$menuval['action']) ? "active" : ""; ?>">
                        <a href="<?= Url::toRoute([$menuval['url']]); ?>"><span class="fa <?php echo $menuval['icon']; ?>"></span> 
                        <span class="xn-text"><?php echo $menuval['name']; ?></span></a>
                    </li>
                       <?php } else { ?>
                         <li class="<?= ($controllerId==$menuval['controller'] && $actionId==$menuval['action']) ? "active" : ""; ?>">
                        <a href="<?= Url::toRoute([$menuval['url']]); ?>"><span class="fa <?php echo $menuval['icon']; ?>"></span> 
                        <span class="xn-text"><?php echo $menuval['name']; ?> FIXED HERERE</span></a>
                    </li>
                      <?php }
                       } ?>
                    
                    

                     
                    <?php if (Yii::$app->user->identity->getIsAdmin()): ?>
                        <li class="xn-openable <?= (in_array(\Yii::$app->controller->id, ['schedule', 'project-phase', 'holidays', 'session', 'asset-contacts', 'school', 'area', 'asset-master', 'signage', 'permission', 'role', 'user', 'network', 'group', 'admin', 'category-master', 'sessions', 'backend', 'faultstype', 'faults-cause', 'alarm-config', 'usergroup', 'userdepartment', 'fault-report', 'performance-report']) && \Yii::$app->controller->action->id != 'scheduler') ? "active" : ""; ?>">
                            <a href="#">
                                <span class="fa fa-wrench"></span>
                                <span class="xn-text">Settings</span>
                            </a>
                            <ul>

                                <li class="xn-openable <?= (in_array(\Yii::$app->controller->id, ['school', 'project-phase', 'area', 'backend'])) ? "active" : ""; ?>">
                                    <a href="#">
                                        <span class="fa fa-map-marker"></span>
                                        <span class="xn-text">School Settings</span>
                                    </a>
                                    <ul>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['area'])) ? "active" : ""; ?>">
                                            <a href="<?= Url::toRoute(['/area/index']); ?>"><i class="fa fa-indent"></i>Area Management</a>
                                        </li>

                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['school'])) ? "active" : ""; ?>">
                                            <a href="<?= Url::toRoute(['/school/manage']); ?>"><i class="fa fa-university"></i>School Management</a>
                                        </li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['project-phase'])) ? "active" : ""; ?>">
                                            <a href="<?= Url::toRoute(['/project-phase']); ?>"><i class="fa fa-film"></i>Project Phase</a>
                                        </li>

        <!--                          <li class="<? = (in_array(\Yii::$app->controller->id, ['backend']))? "active" : ""; ?>">
                                    <a href="<? = Url::toRoute(['/backend/manage']); ?>"><i class="fa fa-indent"></i>Backend Management</a>
                                </li>-->

                                    </ul>
                                </li>

                                <li class="xn-openable <?= (in_array(\Yii::$app->controller->id, ['category-master', 'asset-contacts', 'signage', 'network', 'asset-master', 'asset-contacts'])) ? "active" : ""; ?>">
                                    <a href="#">
                                        <span class="fa fa-gear"></span>
                                        <span class="xn-text">Asset Setings</span>
                                    </a>
                                    <ul>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['category-master'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/category-master/index']); ?>"><i class="fa fa-file"></i>Category Management</a></li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['asset-contacts'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/asset-contacts/index']); ?>"><i class="fa fa-bullseye"></i>Vendor Configuration</a></li>

                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['signage'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/signage/index']); ?>"><i class="fa fa-gear"></i>Signage Configuration</a></li>                                         
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['network'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/network/index']); ?>"><i class="fa fa-sitemap"></i>Network Configuration</a></li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['asset-master'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/asset-master/index']); ?>"><i class="fa fa-bullhorn"></i>Asset Management</a></li>

                                    </ul>
                                </li>
                                <li class="xn-openable <?= (in_array(\Yii::$app->controller->id, ['faultstype', 'alarm-config', 'faults-cause'])) ? "active" : ""; ?>">
                                    <a href="#">
                                        <span class="fa fa-exclamation-triangle"></span>
                                        <span class="xn-text">Fault Settings</span>
                                    </a>
                                    <ul>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['faults-cause'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/faults-classification/index']); ?>"><i class="fa fa-gear"></i>Fault Type</a></li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['faultstype'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/faultstype/index']); ?>"><i class="fa fa-tags"></i>Fault Cause</a></li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['alarm-config'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/alarm-config/index']); ?>"><i class="fa fa-bullseye"></i>SMS/Alarm Management</a></li>                                         



                                    </ul>
                                </li>


                                <li class="xn-openable <?= (in_array(\Yii::$app->controller->id, ['session', 'holidays', 'schedule', 'group'])) ? "active" : ""; ?>">
                                    <a href="#">
                                        <span class="fa fa-calendar"></span>
                                        <span class="xn-text">Schedule Settings </span>
                                    </a>
                                    <ul>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['holidays'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/holidays/index']); ?>"><i class="fa fa-reorder"></i>Holiday Configuration</a></li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['session'])) ? "active" : ""; ?>">
                                            <a href="<?= Url::toRoute(['/session/index']); ?>"><i class="fa fa-calendar-o"></i>Session Configuration</a>
                                        </li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['group'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/group/index']); ?>"><i class="fa fa-ticket"></i>Schedule Management</a></li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['schedule'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/schedule/calendar']); ?>"><i class="fa fa-calendar"></i>Ad-hoc Scheduler</a></li>
                                    </ul>
                                </li>

                                <li class="xn-openable <?= (in_array(\Yii::$app->controller->id, ['permission', 'role', 'admin', 'userdepartment', 'usergroup'])) ? "active" : ""; ?>">
                                    <a href="#">
                                        <span class="fa fa-users"></span>
                                        <span class="xn-text">User Settings </span>
                                    </a>
                                    <ul>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['admin'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/user/admin']); ?>"><i class="fa fa-users"></i>Manage Users</a></li>      
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['role'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/rbac/role/index']); ?>"><i class="fa fa-user"></i>Roles</a></li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['permission'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/permission/index']); ?>"><i class="fa fa-lock"></i>Map Roles</a> </li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['userdepartment'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/userdepartment/index']); ?>"><i class="fa fa-columns"></i>Department</a></li>
                                        <li class="<?= (in_array(\Yii::$app->controller->id, ['usergroup'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/usergroup/index']); ?>"><i class="fa fa-mobile"></i>SMS Group</a></li>                                

                                    </ul>
                                </li>

                            </ul></li>


                        <li class="<?= (in_array(\Yii::$app->controller->id, ['sitemap'])) ? "active" : ""; ?>"><a href="<?= Url::toRoute(['/sitemap/index']); ?>"><i class="fa fa-sitemap"></i>Sitemap</a></li>
                    <?php endif; ?>
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->